#include "Global.h"
#include "Player.h"

Player::Player()
{
	mmr = -1;
	mmServerID = -1;
	lobbyServerID = -1;
	lobbyID = -1;

	is_matching = false;
	is_active = false;

	is_synchro = true;
}
Player::~Player()
{
}

void Player::SetPlayer( int _mmr, short _lobbyServerID, short _lobbyID )
{
	mmr = _mmr;
	lobbyServerID = _lobbyServerID;
	lobbyID = _lobbyID;
}

void Player::SetmmID( int id )
{
	mmServerID = id;
}
int Player::GetmmID()
{
	return mmServerID;
}

int Player::Getmmr()
{
	return mmr;
}
short Player::GetLobbyServerID()
{
	return lobbyServerID;
}
short Player::GetLobbyID()
{
	return lobbyID;
}

void Player::SetTimeBegin()
{
	beginTime = std::chrono::steady_clock::now();
}
std::chrono::steady_clock::time_point Player::GetTimeBegin()
{
	return beginTime;
}

bool Player::SetMatchingReverse()
{
	is_matching = !is_matching;
	return is_matching;
}

bool Player::isMatching()
{
	return is_matching;
}

bool Player::isActive()
{
	return is_active;
}

bool Player::isSynchro()
{
	return is_synchro;
}

void Player::PrintPlayer()
{
	std::cout << "mmSID : " << setw( 5 ) << mmServerID << ", lobbySID : " << setw( 5 ) << lobbyServerID << ", mmr : " << setw( 5 ) << mmr << std::endl;
}

bool Player::operator <( const Player& lh )const
{
	return mmr > lh.mmr;
}

bool Player::operator ==( const Player& lh ) const
{
	return ( ( lobbyID == lh.lobbyID ) && ( lobbyServerID == lh.lobbyServerID ) );
}