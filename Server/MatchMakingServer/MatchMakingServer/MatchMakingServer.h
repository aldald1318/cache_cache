#pragma once
#define _WINSOCK_DEPRECATED_NO_WARNINGS
#include "pch.h"
#include "Global.h"
#include "Player.h"
#include "Match.h"

const static int MAX_SERVER = 2;	//(LOBBY + BATTLE)

enum SERVER_TYPE { SV_BATTLE, SV_LOBBY };	//로비가 추가되면 여기에도 추가

struct OVER_EX {
	WSAOVERLAPPED	over;
	WSABUF			wsabuf[1];
	char			net_buf[BUF_SIZE];
	bool			is_recv;
};

struct SOCKETINFO
{
	OVER_EX			recv_over;
	SOCKET			socket;
	SERVER_TYPE		server_type;
};

struct MATCH
{
	bool is_match = false;
	std::vector<Player>::iterator player_iter[4];
};

class MatchMakingServer
{
public:
	MatchMakingServer();
	~MatchMakingServer();

	void ConnectServer( SERVER_TYPE server_type, int port, std::string ip );

	void DoWorker();

	void ProcessPacket( int id, void* buff );

	void FindMatch( const boost::system::error_code&, boost::asio::deadline_timer* t );
	std::vector<Player>::iterator FindNextPlayer( std::vector<Player>::iterator begin );
	MATCH FindFourPlayer( std::vector<Player>::iterator begin );
	bool IsAcceptableMatch( const MATCH& match );

	void ProcessMatch( int roomNum );

	void SendMatchPacket( short id, short lobbyID, short roomNum, char is_host );//M -> L
	void SendCancelAutoMatchSuccess( short id, short lobbyID );
	void SendRequestRoomPacket();// M -> B

	void SendPacket( SERVER_TYPE id, void* buff );

	void error_display( const char* msg, int err_no );

private:
	HANDLE iocp;
	WSADATA WSAData;
	SOCKADDR_IN serverAddr;
	DWORD flags;
	int addrLen = sizeof( SOCKADDR_IN );

	std::array <SOCKETINFO, MAX_SERVER> servers;

	std::array<Player, MAX_PLAYER> playerArray;
	std::vector<Player> playerHeap;

	std::queue<Match> matchQueue;
	std::mutex matchQueueLock;
};
