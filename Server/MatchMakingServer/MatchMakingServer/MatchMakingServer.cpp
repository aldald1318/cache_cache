#include "MatchMakingServer.h"
#include "../../Protocol.h"

MatchMakingServer::MatchMakingServer()
{
	wcout.imbue( std::locale( "korean" ) );
	WSAStartup( MAKEWORD( 2, 2 ), &WSAData );
	iocp = CreateIoCompletionPort( INVALID_HANDLE_VALUE, 0, NULL, 0 );
	// INITIALIZE IOCP
}
MatchMakingServer::~MatchMakingServer()
{
	WSACleanup();
}

void MatchMakingServer::DoWorker()
{
	while ( true ) {
		DWORD num_byte;
		ULONG key;
		PULONG p_key = &key;
		WSAOVERLAPPED* p_over;

		GetQueuedCompletionStatus( iocp, &num_byte, ( PULONG_PTR )p_key, &p_over, INFINITE );
		//여기서 에러 코드를 받아야된다 우리의 클라는 시도때도없이 뻗는다

		SOCKET client_s = servers[key].socket;

		if ( num_byte == 0 ) {
			closesocket( client_s );
			std::cout << key << "servers disconnect!" << std::endl;
			continue;
		}  // 서버가 closesocket을 했을 경우

		OVER_EX* over_ex = reinterpret_cast< OVER_EX* > ( p_over );

		if ( true == over_ex->is_recv )
		{
			ProcessPacket( key, over_ex->net_buf );

			DWORD flags = 0;
			memset( &over_ex->over, 0x00, sizeof( WSAOVERLAPPED ) );
			WSARecv( client_s, over_ex->wsabuf, 1, 0, &flags, &over_ex->over, 0 );
		}
		else
		{
			delete over_ex;
		}
	}
}

void MatchMakingServer::ProcessPacket( int server_type, void* buff )
{
	char* recv = reinterpret_cast< char* >( buff );
	switch ( ( int )recv[1] )
	{
	case BM_ROOMREADY:
	{
		bm_packet_room_ready packet;
		memcpy( &packet, recv, sizeof( packet ) );
		ProcessMatch( packet.roomNum );
		break;
	}
	case LM_AUTOMATCH:
	{
		lm_packet_automatch packet;
		memcpy( &packet, recv, sizeof( packet ) );
		std::cout << "recv auto match packet " << packet.id << " : " << packet.mmr << std::endl;
		Player new_player;
		new_player.SetPlayer( packet.mmr, packet.id, server_type );


		for ( int i = 0; i < MAX_PLAYER; ++i )
		{
			if ( !playerArray[i].isActive() )
			{
				playerArray[i] = new_player;
				playerArray[i].is_active = true;
				playerArray[i].is_synchro = false;
				playerArray[i].SetmmID( i );
				playerArray[i].SetTimeBegin();
				if ( !playerArray[i].SetMatchingReverse() )
					std::cout << "matching 이 true가 되지 않는다" << std::endl;
				return;
			}
		}
		break;
	}
	case LM_CANCEL_AUTOMATCH:
	{
		lm_packet_cancel_automatch packet;
		memcpy( &packet, recv, sizeof( packet ) );
		std::cout << "recv cancel auto match packet " << packet.id << std::endl;

		for ( int i = 0; i < MAX_PLAYER; ++i )
		{
			if ( playerArray[i].isActive() )
				if ( playerArray[i].GetLobbyServerID() == packet.id )
				{
					playerArray[i].is_synchro = false;
					if ( playerArray[i].SetMatchingReverse() )
						playerArray[i].SetTimeBegin();
					SendCancelAutoMatchSuccess( playerArray[i].GetLobbyServerID(), playerArray[i].GetLobbyID() );
					return;
				}
		}
		break;
	}
	case  LM_CLIENT_DISCONNECT:
	{
		lm_packet_client_disconnect packet;
		memcpy( &packet, recv, sizeof( packet ) );
		std::cout << "recv client disconnect packet " << packet.id << std::endl;

		for ( int i = 0; i < MAX_PLAYER; ++i )
		{
			if ( playerArray[i].isActive() )
				if ( playerArray[i].GetLobbyServerID() == packet.id )
				{
					playerArray[i].is_synchro = false;
					if ( playerArray[i].SetMatchingReverse() )
						playerArray[i].SetTimeBegin();
					return;
				}
		}
		break;
	}
	default:
	{
		std::cout << "unknown key input" << std::endl;
	}
	}
}

void MatchMakingServer::ProcessMatch( int roomNum )
{
	matchQueueLock.lock();
	if ( matchQueue.empty() )
	{
		matchQueueLock.unlock();
		std::cout << "match Queue is empty" << std::endl;//비었으면 음 그거에 관한 처리
		return;
	}
	matchQueueLock.unlock();

	Match match;
	matchQueueLock.lock();
	match = matchQueue.front();
	matchQueue.pop();
	matchQueueLock.unlock();

	for ( int i = 0; i < 4; ++i )
	{
		char ishost { 1 };
		if ( i != 0 )
		{
			ishost = 0;
		}
		SendMatchPacket( match.LobbyServerID[i], match.LobbyID[i], roomNum, ishost );
	}
}

void MatchMakingServer::FindMatch( const boost::system::error_code&, boost::asio::deadline_timer* t )
{
	for ( int i = 0; i < MAX_PLAYER; ++i )
	{
		if ( !playerArray[i].isSynchro() )
		{
			playerArray[i].is_synchro = true;
			Player new_player = playerArray[i];
			auto iter = std::find( playerHeap.begin(), playerHeap.end(), new_player );
			if ( iter != playerHeap.end() )
			{
				*iter = new_player;
			}
			else
			{
				playerHeap.emplace_back( new_player );
			}
		}
	}
	std::make_heap( playerHeap.begin(), playerHeap.end() );
	std::sort_heap( playerHeap.begin(), playerHeap.end() );

	for ( auto iter = playerHeap.begin(); iter != playerHeap.end(); )
	{
		MATCH getMatch = FindFourPlayer( iter );
		if ( getMatch.is_match == true )	//4명 매치 찾음
		{
			Match new_match;
			for ( int i = 0; i < 4; ++i )
			{
				getMatch.player_iter[i]->SetMatchingReverse();

				int id = getMatch.player_iter[i]->GetmmID();
				new_match.LobbyID[i] = getMatch.player_iter[i]->GetLobbyID();
				new_match.LobbyServerID[i] = getMatch.player_iter[i]->GetLobbyServerID();

				playerArray[id].is_synchro = false;
				playerArray[id].SetMatchingReverse();
			}
			matchQueueLock.lock();
			matchQueue.emplace( new_match );
			matchQueueLock.unlock();

			iter = ++getMatch.player_iter[3];
			SendRequestRoomPacket();
		}
		else	//매치 못찾음
		{
			for ( int i = 0; i < 4; ++i )
			{
				if ( getMatch.player_iter[i] == playerHeap.end() )
				{
					goto FIN;
				}
				else
				{
					iter = getMatch.player_iter[i + 1];
					break;
				}
			}
		}
	}

FIN:

	t->expires_from_now( boost::posix_time::seconds( 1 ) );
	t->async_wait( boost::bind( &MatchMakingServer::FindMatch, this, boost::asio::placeholders::error, t ) );
}

MATCH MatchMakingServer::FindFourPlayer( std::vector<Player>::iterator begin )
{
	std::vector<Player>::iterator iter = begin;
	MATCH match;
	match.is_match = true;
	for ( int i = 0; i < 4; ++i )
	{
		iter = FindNextPlayer( iter );
		if ( iter != playerHeap.end() )
		{
			match.player_iter[i] = iter;
		}
		else
		{
			match.is_match = false;
			match.player_iter[i] = iter;
			return match;
		}
		++iter;
	}

	if ( !IsAcceptableMatch( match ) )
	{
		match.is_match = false;
	}
	return match;
}

std::vector<Player>::iterator MatchMakingServer::FindNextPlayer( std::vector<Player>::iterator begin )
{
	std::vector<Player>::iterator iter;
	for ( iter = begin; iter != playerHeap.end(); ++iter )
	{
		if ( ( *iter ).isActive() && ( *iter ).isMatching() )
		{
			return iter;
		}
	}
	return iter;
}

bool MatchMakingServer::IsAcceptableMatch( const MATCH& match )
{
	float avgmmr = ( match.player_iter[0]->Getmmr() + match.player_iter[1]->Getmmr() + match.player_iter[2]->Getmmr() + match.player_iter[3]->Getmmr() ) / 4.f;
	avgmmr = ( abs( avgmmr - match.player_iter[0]->Getmmr() ) + abs( avgmmr - match.player_iter[1]->Getmmr() ) + abs( avgmmr - match.player_iter[2]->Getmmr() ) + abs( avgmmr - match.player_iter[3]->Getmmr() ) ) / 4.f;

	std::chrono::steady_clock::time_point cur_time = std::chrono::steady_clock::now();
	std::chrono::duration<double> avgtime = ( cur_time - match.player_iter[0]->GetTimeBegin() + cur_time - match.player_iter[1]->GetTimeBegin() + cur_time - match.player_iter[2]->GetTimeBegin() + cur_time - match.player_iter[3]->GetTimeBegin() ) / 4;

	if ( avgtime.count() > ( avgmmr ) )// * 10.f ) )
	{
		std::cout << "player : " << match.player_iter[0]->GetmmID() << ", " << match.player_iter[1]->GetmmID() << ", " << match.player_iter[2]->GetmmID() << ", " << match.player_iter[3]->GetmmID() << std::endl;
		std::cout << "avgtime : " << avgtime.count() << ", avgmmr : " << avgmmr << std::endl;
		return true;
	}
	else
		return false;
}

void MatchMakingServer::SendMatchPacket( short id, short lobbyID, short roomNum, char is_host )
{
	ml_packet_match packet;
	packet.size = sizeof( packet );
	packet.type = ML_MATCH;
	packet.roomNum = roomNum;
	packet.id = id;
	packet.is_host = is_host;
	SendPacket( ( SERVER_TYPE )lobbyID, &packet );
}

void MatchMakingServer::SendCancelAutoMatchSuccess( short id, short lobbyID )
{
	ml_packet_cancel_automatch_success packet;
	packet.size = sizeof( packet );
	packet.type = ML_CANCEL_AUTOMATCH_SUCCESS;
	packet.id = id;
	SendPacket( ( SERVER_TYPE )lobbyID, &packet );
}

void MatchMakingServer::SendRequestRoomPacket()
{
	mb_packet_request_room packet;
	packet.size = sizeof( packet );
	packet.type = MB_REQUEST_ROOM;
	SendPacket( SV_BATTLE, &packet );
}

void MatchMakingServer::SendPacket( SERVER_TYPE id, void* buff )
{
	char* packet = reinterpret_cast< char* >( buff );
	int packet_size = packet[0];

	OVER_EX* send_over = new OVER_EX;
	memset( send_over, 0x00, sizeof( OVER_EX ) );
	send_over->is_recv = false;
	memcpy( send_over->net_buf, packet, packet_size );
	send_over->wsabuf[0].buf = send_over->net_buf;
	send_over->wsabuf[0].len = packet_size;

	int ret = WSASend( servers[id].socket, send_over->wsabuf, 1, 0, 0, &send_over->over, 0 );
	if ( 0 != ret ) {
		int err_no = WSAGetLastError();
		error_display( "WSASend Error :", err_no );
		if ( WSA_IO_PENDING != err_no )
			error_display( "WSASend Error :", err_no );
	}
}

void MatchMakingServer::ConnectServer( SERVER_TYPE server_type, int port, std::string ip )
{
	servers[server_type].socket = WSASocketW( AF_INET, SOCK_STREAM, 0, NULL, 0, WSA_FLAG_OVERLAPPED );

	//BOOL optval = TRUE;
	//setsockopt( servers[server_type].socket, IPPROTO_TCP, TCP_NODELAY, ( char* )&optval, sizeof( optval ) );

	ZeroMemory( &serverAddr, sizeof( SOCKADDR_IN ) );
	serverAddr.sin_family = AF_INET;
	serverAddr.sin_port = htons( port );
	serverAddr.sin_addr.s_addr = inet_addr( ip.c_str() );

	int Result = WSAConnect( servers[server_type].socket, ( sockaddr* )&serverAddr, sizeof( serverAddr ), NULL, NULL, NULL, NULL );
	if ( 0 != Result ) std::cout << server_type << "SERVER CONNECTION FAIL\n";
	else std::cout << server_type << " SERVER CONNECTION!\n";

	servers[server_type].recv_over.wsabuf[0].len = BUF_SIZE;
	servers[server_type].recv_over.wsabuf[0].buf = servers[server_type].recv_over.net_buf;
	servers[server_type].recv_over.is_recv = true;
	memset( &servers[server_type].recv_over.over, 0x00, sizeof( WSAOVERLAPPED ) );

	CreateIoCompletionPort( reinterpret_cast< HANDLE >( servers[server_type].socket ), iocp, server_type, 0 );

	int ret = WSARecv( servers[server_type].socket, servers[server_type].recv_over.wsabuf, 1, NULL, &flags, &( servers[server_type].recv_over.over ), NULL );
	if ( SOCKET_ERROR == ret ) {
		int err_no = WSAGetLastError();
		if ( err_no != WSA_IO_PENDING )
		{
			error_display( "RECV ERROR", err_no );
		}
	}
}

void MatchMakingServer::error_display( const char* msg, int err_no )
{
	WCHAR* lpMsgBuf;
	FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER |
		FORMAT_MESSAGE_FROM_SYSTEM,
		NULL, err_no,
		MAKELANGID( LANG_NEUTRAL, SUBLANG_DEFAULT ),
		( LPTSTR )&lpMsgBuf, 0, NULL );
	cout << msg;
	wcout << L"error " << lpMsgBuf << endl;
	while ( true );
	LocalFree( lpMsgBuf );
}