#include "pch.h"
#include "../../Protocol.h"
#include "Global.h"
#include "MatchMakingServer.h"
#include "Player.h"

MatchMakingServer* g_matchMakingServer;

int new_playerID { 0 };

int main()
{
	g_matchMakingServer = new MatchMakingServer();
	g_matchMakingServer->ConnectServer( SV_LOBBY, LOBBY_SERVER_PORT, LOBBY_SERVER_IP_PRIVATE );
	g_matchMakingServer->ConnectServer( SV_BATTLE, BATTLE_SERVER_PORT, BATTLE_SERVER_IP_PRIVATE );

	boost::asio::io_context io;
	boost::asio::deadline_timer timer( io, boost::posix_time::seconds( 1 ) );

	timer.async_wait( boost::bind( &MatchMakingServer::FindMatch, g_matchMakingServer, boost::asio::placeholders::error, &timer ) );
	
	std::thread worker_thread( &MatchMakingServer::DoWorker, g_matchMakingServer );

	io.run();

	worker_thread.join();

	delete g_matchMakingServer;
}