//#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <iomanip>
#include <random>

#include <algorithm>
#include <string>
#include <vector>
#include <queue>
#include <array>
#include <thread>
#include <mutex>
#include <chrono>

#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

using namespace std;
#include <WS2tcpip.h>
#pragma comment(lib, "Ws2_32.lib")