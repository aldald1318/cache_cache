#pragma once
#include "pch.h"

class Player
{
public:
	Player();
	~Player();

	void SetPlayer( int _mmr, short _lobbyServerID, short _lobbyID );
	void SetmmID( int id );
	int GetmmID();

	int Getmmr();
	short GetLobbyServerID();
	short GetLobbyID();

	void SetTimeBegin();
	std::chrono::steady_clock::time_point GetTimeBegin();

	bool SetMatchingReverse();

	bool isMatching();
	bool isActive();
	bool isSynchro();

	void PrintPlayer();

	bool operator <( const Player& lh )const;
	bool operator ==( const Player& lh )const;

private:
	int				mmr;
	short			mmServerID;
	short			lobbyServerID;	//로비서버에서의 아이디
	short			lobbyID;		//로비아이디 0or1 정도
	bool			is_matching;
	std::chrono::steady_clock::time_point beginTime;

public:
	bool			is_active = false;	// 플레이어가 접속을 종료하기 전까지는 false가 되지 않는다
	bool			is_synchro = false;
};

