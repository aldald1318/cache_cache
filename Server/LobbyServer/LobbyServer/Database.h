#pragma once
#include <iostream>
#include <string>
#include <windows.h>  
#include <sqlext.h>  
#include <codecvt>
#include "Singleton.h"

constexpr auto NAMELEN = 11;

class Database : public Singleton<Database>
{
public:
	Database();
	~Database();

	bool ConfirmPlayer( std::string id, std::string password, int* mmr );
	bool IsExistID( std::string id ,int* is_exist);
	bool SignUpPlayer( std::string id, std::string password );

	bool GetUserInfo( std::string id, int& mmr, int& killCnt, float& W_rate, int& totalCnt, int& winCnt );
	void UpdateUserInfo( std::string& id, int& mmr, int& killCnt, float& WRate, int& TCnt, int& WCnt );
	// 밑에 두개 아직 저장 프로시저는 없음

	void HandleDiagnosticRecord( SQLHANDLE hHandle, SQLSMALLINT hType, RETCODE RetCode );
	void show_error();
	std::wstring StringToWstring( const std::string& str );

private:
	SQLHENV henv;
	SQLHDBC hdbc;
	SQLHSTMT hstmt = 0;
	SQLRETURN retcode;

	SQLINTEGER nMmr;
	SQLLEN cbMmr;
	SQLINTEGER nCount;
	SQLLEN cbCount;

	SQLINTEGER  nKillCnt{}, nTotalCnt{}, nWinCnt{};
	SQLREAL nWinRate{};
	SQLLEN cbKillCnt{}, cbWinRate{}, cbTotalCnt{}, cbWinCnt{};
};

