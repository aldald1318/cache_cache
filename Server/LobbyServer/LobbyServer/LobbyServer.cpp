#include "LobbyServer.h"
#include "Database.h"
#include "../../Protocol.h"

LobbyServer::LobbyServer( short lobby_id )
{
	lobbyID = lobby_id;

	wcout.imbue( std::locale( "korean" ) );

	WSAStartup( MAKEWORD( 2, 2 ), &WSAData );
	listenSocket = WSASocket( AF_INET, SOCK_STREAM, 0, NULL, 0, WSA_FLAG_OVERLAPPED );

	memset( &serverAddr, 0, sizeof( SOCKADDR_IN ) );
	serverAddr.sin_family = AF_INET;
	serverAddr.sin_port = htons( LOBBY_SERVER_PORT );
	serverAddr.sin_addr.S_un.S_addr = htonl( INADDR_ANY );
	::bind( listenSocket, ( struct sockaddr* ) & serverAddr, sizeof( SOCKADDR_IN ) );
	listen( listenSocket, 5 );

	memset( &clientAddr, 0, addrLen );
	memset( &mmServerAddr, 0, addrLen );

	iocp = CreateIoCompletionPort( INVALID_HANDLE_VALUE, NULL, NULL, 0 );
}
LobbyServer::~LobbyServer()
{
	closesocket( listenSocket );
	WSACleanup();
}

void LobbyServer::MMServerAccept()
{
	// 문제점 : 이렇게하면 순서가 중요해짐 클라 전에 mm서버가 연결되어야 함
	// 나중에 bind를 따로 해서 지정된 mmrAddr로만 접속 가능하도록 수정?
	mmServerSocket = accept( listenSocket, ( struct sockaddr* ) & mmServerAddr, &addrLen );

	SOCKETINFO* new_mmServer = new SOCKETINFO;
	new_mmServer->id = 0;
	new_mmServer->socket = mmServerSocket;
	new_mmServer->recv_over.wsabuf[0].len = BUF_SIZE;
	new_mmServer->recv_over.wsabuf[0].buf = new_mmServer->recv_over.net_buf;
	new_mmServer->recv_over.is_recv = true;
	new_mmServer->is_active = true;
	new_mmServer->curr_packet_size = 0;
	new_mmServer->prev_packet_data = 0;

	players[0] = new_mmServer;	//player의 0번째를 mm서버로 하자...!

	CreateIoCompletionPort( reinterpret_cast< HANDLE >( mmServerSocket ), iocp, 0, 0 );

	memset( &players[0]->recv_over.over, 0x00, sizeof( WSAOVERLAPPED ) );
	flags = 0;
	int ret = WSARecv( mmServerSocket, players[0]->recv_over.wsabuf, 1, NULL, &flags, &( players[0]->recv_over.over ), NULL );
	if ( 0 != ret ) {
		int err_no = WSAGetLastError();
		if ( WSA_IO_PENDING != err_no )
			error_display( "WSARecv Error :", err_no );
	}

	std::cout << "MMSERVER CONNECT!" << std::endl;
}

void LobbyServer::ClientAccept( int id )
{
	clientSocket = accept( listenSocket, ( struct sockaddr* ) & clientAddr, &addrLen );

	SOCKETINFO* new_player = new SOCKETINFO;
	new_player->id = id;
	new_player->socket = clientSocket;
	new_player->recv_over.wsabuf[0].len = BUF_SIZE;
	new_player->recv_over.wsabuf[0].buf = new_player->recv_over.net_buf;
	new_player->recv_over.is_recv = true;
	new_player->player_info = new Player();
	new_player->is_active = true;
	new_player->curr_packet_size = 0;
	new_player->prev_packet_data = 0;

	players[id] = new_player;

	CreateIoCompletionPort( reinterpret_cast< HANDLE >( clientSocket ), iocp, id, 0 );

	memset( &players[id]->recv_over.over, 0x00, sizeof( WSAOVERLAPPED ) );
	flags = 0;
	int ret = WSARecv( clientSocket, players[id]->recv_over.wsabuf, 1, NULL, &flags, &( players[id]->recv_over.over ), NULL );
	if ( 0 != ret ) {
		int err_no = WSAGetLastError();
		if ( WSA_IO_PENDING != err_no )
			error_display( "WSARecv Error :", err_no );
	}
}

void LobbyServer::DoWorker()
{
	while ( true ) {
		DWORD num_byte;
		ULONG key;
		PULONG p_key = &key;
		WSAOVERLAPPED* p_over;

		BOOL ret = GetQueuedCompletionStatus( iocp, &num_byte, ( PULONG_PTR )p_key, &p_over, INFINITE );

		SOCKET client_s = players[key]->socket;

		if ( FALSE == ret )//여기서 에러 코드를 받아야된다 우리의 클라는 시도때도없이 뻗는다
		{
			int err_no = WSAGetLastError();
			if ( 64 == err_no ) closesocket( client_s );
			else error_display( "GQCS : ", WSAGetLastError() );
		}

		if ( num_byte == 0 ) {
			closesocket( client_s );
			players[key]->is_active = false;
			players[key]->player_info->SetPlayerLoginOK( "" );
			if ( players[key]->player_info->is_matching == true )
			{
				SendClientDisconnectPacket( key );
			}
			delete players[key]->player_info;
			continue;
		}  // 클라이언트가 closesocket을 했을 경우

		OVER_EX* over_ex = reinterpret_cast< OVER_EX* > ( p_over );

		if ( true == over_ex->is_recv )
		{
			char* buf = players[key]->recv_over.net_buf;
			unsigned psize = players[key]->curr_packet_size;
			unsigned pr_size = players[key]->prev_packet_data;

			while ( num_byte > 0 )
			{
				if ( 0 == psize ) psize = (BYTE)buf[0];
				if ( num_byte + pr_size >= psize )
				{
					// 지금 패킷 완성 가능
					unsigned char packet[MAX_PACKET_SIZE];
					memcpy( packet, players[key]->packet_buf, pr_size );
					memcpy( packet + pr_size, buf, psize - pr_size );
					ProcessPacket( static_cast< int >( key ), packet );
					num_byte -= psize - pr_size;
					buf += psize - pr_size;
					psize = 0; pr_size = 0;
				}
				else
				{
					memcpy( players[key]->packet_buf + pr_size, buf, num_byte );
					pr_size += num_byte;
					num_byte = 0;
				}
			}
			players[key]->curr_packet_size = psize;
			players[key]->prev_packet_data = pr_size;

			DWORD flags = 0;
			memset( &over_ex->over, 0x00, sizeof( WSAOVERLAPPED ) );
			WSARecv( client_s, over_ex->wsabuf, 1, 0, &flags, &over_ex->over, 0 );
		}
		else
		{
			delete over_ex;
		}
	}
}

void LobbyServer::ProcessPacket( int id, void* buff )
{
	char* packet = reinterpret_cast< char* >( buff );
	switch ( packet[1] )
	{
	case CL_LOGIN:
	{
		cl_packet_login* login_packet = reinterpret_cast< cl_packet_login* >( packet );
		//if ( Database::GetInstance()->ConfilmPlayer( "frog", "ffrroogg", &mmr ) )


		int mmr{};
		if ( Database::GetInstance()->ConfirmPlayer( login_packet->id, login_packet->password, &mmr ) )
		{

			for ( int i = 1; i < new_playerID ; ++i )	//0은 mmr server
			{
				if ( players[i]->player_info->ComparePlayerID( login_packet->id ) )
				{
					std::cout << "client deny : already login" << std::endl;
					SendLoginDenyPacket( id );
					return;
				}
			}
			std::cout << "client login ok : " << login_packet->id << std::endl;

			players[id]->player_info->SetPlayerLoginOK( login_packet->id );
			players[id]->player_info->SetPlayerMMR( mmr );

			SendLoginOKPacket( id );
		}
		else
		{
			std::cout << "client deny : not exist" << std::endl;
			SendLoginDenyPacket( id );
		}
		break;
	}
	case CL_SIGNUP:
	{
		cl_packet_signup* signup_packet = reinterpret_cast<cl_packet_signup* >( packet );
				
		int is_exist;
		Database::GetInstance()->IsExistID( signup_packet->id, &is_exist );
		if ( is_exist == 0 )	//회원가입 가능
		{
			if ( Database::GetInstance()->SignUpPlayer( signup_packet->id, signup_packet->password ) )
			{
				SendSignUpOkPacket( id, START_MMR );
				break;
			}
		}
		SendSignUpDenyPacket( id );
		break;
	}
	case CL_AUTOMATCH:
	{
		cl_packet_automatch recvpacket;
		memcpy( &recvpacket, packet, sizeof( recvpacket ) );

		players[id]->player_info->is_matching = true;

		SendAutoMatchPacket( id, players[id]->player_info->GetPlayerMMR() );
		break;
	}
	case CL_CANCEL_AUTOMATCH:
	{
		cl_packet_cancel_automatch recvpacket;
		memcpy( &recvpacket, packet, sizeof( recvpacket ) );

		SendCancelAutoMatchPacket( id );

		break;
	}
	case ML_MATCH:
	{
		ml_packet_match recvpacket;
		memcpy( &recvpacket, packet, sizeof( recvpacket ) );
		SendMatchPacket( recvpacket.id, recvpacket.roomNum, recvpacket.is_host );
		break;
	}
	case ML_CANCEL_AUTOMATCH_SUCCESS:
	{
		ml_packet_cancel_automatch_success recvpacket;
		memcpy( &recvpacket, packet, sizeof( recvpacket ) );
		SendCancelAutoMatchSuccess( recvpacket.id );

		players[recvpacket.id]->player_info->is_matching = false;
		break;
	}
	case CL_DUMMY_LOGIN:
	{
		players[id]->is_active = true;
		players[id]->id = id;
		Player* new_player = new Player();
		new_player->is_matching = false;
		new_player->SetPlayerLoginOK( "dummy" );

		SendLoginOKPacket( id );
		break;
	}

	case CL_REQUEST_USERINFO:
	{
		int mmr, killCnt, totalCnt, winCnt;
		float winRate;
		Database::GetInstance()->GetUserInfo( players[id]->player_info->GetPlayerName(), mmr, killCnt, winRate, totalCnt, winCnt );

		SendUserInfoPacket( id, players[id]->player_info->GetPlayerName().c_str(), mmr, killCnt, winRate, totalCnt, winCnt );
		break;
	}

	case CL_UPDATED_USER_INFO:
	{
		cl_packet_updated_user_info* packet = reinterpret_cast<cl_packet_updated_user_info*>(buff);
		std::string name{ players[id]->player_info->GetPlayerName() };
		Database::GetInstance()->UpdateUserInfo( name, packet->mmr, packet->catched_student_count, packet->winning_rate,
			packet->total_game_cnt, packet->winning_game_cnt );
		break;
	}

	default:
	{
		std::cout << "Wrong packet type error!" << std::endl;
		while( 1 );
		break;
	}
	}
}

void LobbyServer::SendLoginOKPacket( int id )

{
	lc_packet_login_ok packet;
	packet.size = sizeof( packet );
	packet.type = LC_LOGINOK;
	packet.id = id;

	SendPacket( id, &packet );
}

void LobbyServer::SendLoginDenyPacket( int id )
{
	lc_packet_login_deny packet;
	packet.size = sizeof( packet );
	packet.type = LC_LOGINDENY;
	SendPacket( id, &packet );
}

void LobbyServer::SendSignUpOkPacket( int id, int mmr )
{
	lc_packet_signup_ok packet;
	packet.size = sizeof( packet );
	packet.type = LC_SIGNUPOK;
	SendPacket( id, &packet );
}
void LobbyServer::SendSignUpDenyPacket( int id )
{
	lc_packet_signup_deny packet;
	packet.size = sizeof( packet );
	packet.type = LC_SIGNUPDENY;
	SendPacket( id, &packet );
}

void LobbyServer::SendCancelAutoMatchSuccess( int id )
{
	lc_packet_cancel_automatch_success packet;
	packet.size = sizeof( packet );
	packet.type = LC_CANCEL_AUTOMATCH_SUCCESS;
	SendPacket( id, &packet );
}

void LobbyServer::SendMatchPacket( int id, short roomNum, char is_host )
{
	lc_packet_match packet;
	packet.size = sizeof( packet );
	packet.type = LC_MATCH;
	packet.roomNum = roomNum;
	packet.is_host = is_host;
	SendPacket( id, &packet );
}

void LobbyServer::SendUserInfoPacket( int id, const char* id_str, int mmr, int killCnt, float winRate, int totalCnt, int winCnt )
{
	lc_packet_userinfo packet;
	packet.size = sizeof( lc_packet_userinfo );
	packet.type = LC_USERINFO;
	memcpy( packet.id_str, id_str, 10 );
	packet.mmr = mmr;
	packet.catched_student_count = killCnt;
	packet.total_game_cnt = totalCnt;
	packet.winning_game_cnt = winCnt;
	packet.winning_rate = winRate;
	SendPacket( id, &packet );
}

void LobbyServer::SendAutoMatchPacket( int id, int mmr )
{
	lm_packet_automatch packet;
	packet.size = sizeof( packet );
	packet.type = LM_AUTOMATCH;
	packet.id = id;
	packet.mmr = mmr;
	SendPacket( 0, &packet );	//무조건 매치메이킹 서버로 보낸다
}

void LobbyServer::SendCancelAutoMatchPacket( int id )
{
	lm_packet_cancel_automatch packet;
	packet.size = sizeof( packet );
	packet.type = LM_CANCEL_AUTOMATCH;
	packet.id = id;
	SendPacket( 0, &packet );
}

void LobbyServer::SendClientDisconnectPacket( int id )
{
	lm_packet_client_disconnect packet;
	packet.size = sizeof( packet );
	packet.type = LM_CLIENT_DISCONNECT;
	packet.id = id;
	SendPacket( 0, &packet );
}

void LobbyServer::SendPacket( int id, void* buff )
{
	char* packet = reinterpret_cast< char* >( buff );
	int packet_size = packet[0];

	OVER_EX* send_over = new OVER_EX;
	memset( send_over, 0x00, sizeof( OVER_EX ) );
	send_over->is_recv = false;
	memcpy( send_over->net_buf, packet, packet_size );
	send_over->wsabuf[0].buf = send_over->net_buf;
	send_over->wsabuf[0].len = packet_size;

	int ret = WSASend( players[id]->socket, send_over->wsabuf, 1, 0, 0, &send_over->over, 0 );
	if ( 0 != ret ) {
		int err_no = WSAGetLastError();
		if ( WSA_IO_PENDING != err_no )
			error_display( "WSARecv Error :", err_no );
	}
}

void LobbyServer::error_display( const char* msg, int err_no )
{
	WCHAR* lpMsgBuf;
	FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER |
		FORMAT_MESSAGE_FROM_SYSTEM,
		NULL, err_no,
		MAKELANGID( LANG_NEUTRAL, SUBLANG_DEFAULT ),
		( LPTSTR )&lpMsgBuf, 0, NULL );
	cout << msg;
	wcout << L"error " << lpMsgBuf << endl;
	while ( true );
	LocalFree( lpMsgBuf );
}