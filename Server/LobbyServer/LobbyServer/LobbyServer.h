#pragma once
#include "pch.h"
#include "Global.h"
#include "Player.h"

extern std::atomic_int new_playerID;

struct OVER_EX {
	WSAOVERLAPPED	over;
	WSABUF			wsabuf[1];
	char			net_buf[BUF_SIZE];
	bool			is_recv;
};

struct SOCKETINFO
{
	OVER_EX			recv_over;
	SOCKET			socket;
	bool			is_active = false;
	int				id;

	unsigned char packet_buf[MAX_PACKET_SIZE];
	int prev_packet_data;
	int curr_packet_size;

	Player* player_info = NULL;
};

class LobbyServer
{
public:
	LobbyServer( short lobby_id );
	~LobbyServer();

	void ClientAccept( int id );
	void MMServerAccept();

	void DoWorker();

	void ProcessPacket( int id, void* buff );

	// L -> C
	void SendLoginOKPacket( int id );
	void SendLoginDenyPacket( int id );
	void SendSignUpOkPacket( int id ,int mmr);
	void SendSignUpDenyPacket( int id );
	void SendMatchPacket( int id, short roomNum, char is_host );
	void SendUserInfoPacket( int id, const char* id_str,int mmr, int killCnt, float winRate, int totalCnt, int winCnt );
	void SendCancelAutoMatchSuccess( int id );

	// L -> M
	void SendAutoMatchPacket( int id, int mmr );
	void SendCancelAutoMatchPacket( int id );
	void SendClientDisconnectPacket( int id );

	void SendPacket( int id, void* buff );

	void error_display( const char* msg, int err_no );

	std::uniform_int_distribution<int> uid { 0, 5000 };
	std::default_random_engine dre;

private:
	HANDLE iocp;
	WSADATA WSAData;
	SOCKET listenSocket;
	SOCKADDR_IN serverAddr;
	DWORD flags;
	int addrLen = sizeof( SOCKADDR_IN );

	SOCKADDR_IN mmServerAddr;
	SOCKET mmServerSocket;

	SOCKADDR_IN clientAddr;
	SOCKET clientSocket;


	short lobbyID;
	std::array<SOCKETINFO*, MAX_PLAYER + 1> players;
};
