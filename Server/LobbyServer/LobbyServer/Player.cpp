#include "Global.h"
#include "Player.h"

Player::Player()
{
	is_matching = false;
	is_allow = false;
	lobbyID = LOBBY_ID;
	name = "notYET";
	mmr = -1;
}
Player::~Player()
{
}

void Player::SetPlayerMMR( int _mmr )
{
	mmr = _mmr;
}

int Player::GetPlayerMMR()
{
	return mmr;
}

void Player::SetPlayerLoginOK( std::string _name )
{
	name = _name;
	is_allow = true;
}

bool Player::ComparePlayerID( std::string _name )
{
	return _name == name;
}