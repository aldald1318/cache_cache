#include "pch.h"
#include "Global.h"
#include "LobbyServer.h"
#include "Database.h"
#include "Player.h"

LobbyServer* g_lobbyServer;
Database* g_database;

std::atomic_int new_playerID { 0 };	//0번은 무조건 mmr서버다!

int main()
{
	g_lobbyServer = new LobbyServer( LOBBY_ID );
	g_database = Database::GetInstance();

	g_lobbyServer->MMServerAccept();

	std::thread worker_thread( &LobbyServer::DoWorker, g_lobbyServer );

	while ( true )
	{
		g_lobbyServer->ClientAccept( new_playerID + 1 );
		++new_playerID;
	}

	worker_thread.join();

	delete g_lobbyServer;
	Database::DeleteInstance();
}