#pragma once
#include "pch.h"

class Player
{
public:
	Player();
	~Player();

	void SetPlayerLoginOK( std::string _name );
	void SetPlayerMMR( int mmr );
	int GetPlayerMMR();
	std::string GetPlayerName(){
		return name;
	}
	bool ComparePlayerID( std::string _name );

private:
	std::string name;
	int mmr;
	short lobbyID;
	bool is_allow;

public:
	bool is_matching;
};

