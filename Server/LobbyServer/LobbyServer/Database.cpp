#include "Database.h"

Database::Database() 
{
	setlocale( LC_ALL, "korean" );

	retcode = SQLAllocHandle( SQL_HANDLE_ENV, SQL_NULL_HANDLE, &henv );
	retcode = SQLSetEnvAttr( henv, SQL_ATTR_ODBC_VERSION, ( SQLPOINTER* )SQL_OV_ODBC3, 0 );
	retcode = SQLAllocHandle( SQL_HANDLE_DBC, henv, &hdbc );	//명령어 저장할 핸들
	SQLSetConnectAttr( hdbc, SQL_LOGIN_TIMEOUT, ( SQLPOINTER )5, 0 );
	retcode = SQLConnect( hdbc, (SQLWCHAR*)L"CacheCache_DB", SQL_NTS, (SQLWCHAR*)L"admin", SQL_NTS, (SQLWCHAR*)L"cachecache1!", SQL_NTS );
	if ( retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO )
	{
		std::cout << "DB connect!\n";
	}
	else
	{
		std::cout << "DB connect fail!\n";
	}
}
Database::~Database()
{
	SQLCancel( hstmt );
	SQLFreeHandle( SQL_HANDLE_STMT, hstmt );
	SQLDisconnect( hdbc );
	SQLFreeHandle( SQL_HANDLE_DBC, hdbc );
	SQLFreeHandle( SQL_HANDLE_ENV, henv );
}

bool Database::SignUpPlayer( std::string id, std::string password )
{
	std::wstring qu {};

	qu += L"EXEC SignUp_Player ";
	qu += StringToWstring(id);
	qu += L", ";
	qu += StringToWstring( password );

	retcode = SQLAllocHandle( SQL_HANDLE_STMT, hdbc, &hstmt );
	retcode = SQLExecDirect( hstmt, ( SQLWCHAR* )qu.c_str(), SQL_NTS );
	if ( retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO )
		return true;
	else
		return false;

}

bool Database::GetUserInfo( std::string id, int& mmr, int& killCnt, float& W_rate, int& totalCnt, int& winCnt )
{
	std::wstring qu{};
	qu += L"EXEC Get_User_Info ";
	std::wstring tmp{};
	tmp.assign( id.begin(), id.end() );
	qu += tmp;
	//qu += StringToWstring( id );

	retcode = SQLAllocHandle( SQL_HANDLE_STMT, hdbc, &hstmt );

	retcode = SQLExecDirect( hstmt, (SQLWCHAR*)qu.c_str(), SQL_NTS );
	if( retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO )
	{
		retcode = SQLBindCol( hstmt, 1, SQL_C_LONG, &nMmr, 4, &cbMmr );
		retcode = SQLBindCol( hstmt, 2, SQL_INTEGER, &nKillCnt, 4, &cbKillCnt );
		retcode = SQLBindCol( hstmt, 3, SQL_REAL, &nWinRate, 4, &cbWinRate );
		retcode = SQLBindCol( hstmt, 4, SQL_INTEGER, &nTotalCnt, 4, &cbTotalCnt );
		retcode = SQLBindCol( hstmt, 5, SQL_INTEGER, &nWinCnt, 4, &cbWinCnt );
		if( retcode != SQL_SUCCESS )
			HandleDiagnosticRecord( hstmt, SQL_HANDLE_STMT, retcode );

		retcode = SQLFetch( hstmt );	//SQLFetch를 사용해서 리턴값을 받는다
		if( retcode == SQL_ERROR )
			show_error();
		if( retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO )
		{
			mmr = nMmr;
			killCnt = nKillCnt;
			W_rate = nWinRate;
			totalCnt = nTotalCnt;
			winCnt = nWinCnt;
			if( W_rate <= 0.0f )
				W_rate = 0.0f;
			SQLFreeHandle( SQL_HANDLE_STMT, hstmt );
			return true;
		}
		else
		{
			std::cout << "Get None\n";
			return false;
		}
	}
	else{
		HandleDiagnosticRecord( hstmt, SQL_HANDLE_STMT, retcode );
		return false;
	}
}

void Database::UpdateUserInfo( std::string& id, int& mmr, int& killCnt, float& WRate, int& TCnt, int& WCnt )
{
	std::wstring qu{ L"EXEC UpdateUserInfo " + StringToWstring( id ) };
	qu += L", " + std::to_wstring( mmr ) + L", " + std::to_wstring( killCnt ) + L", " + std::to_wstring( WRate )
		+ L", " + std::to_wstring( TCnt ) + L", " + std::to_wstring( WCnt );

	retcode = SQLAllocHandle( SQL_HANDLE_STMT, hdbc, &hstmt );

	retcode = SQLExecDirect( hstmt, (SQLWCHAR*)qu.c_str(), SQL_NTS );
	if( retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO )
	{
		std::cout << "[LOG] Update " << id << "'s Information Successful!" << std::endl;
		if( retcode != SQL_SUCCESS )
			HandleDiagnosticRecord( hstmt, SQL_HANDLE_STMT, retcode );

		if( retcode == SQL_ERROR )
			show_error();
		if( retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO )
			SQLFreeHandle( SQL_HANDLE_STMT, hstmt );
	}

	else HandleDiagnosticRecord( hstmt, SQL_HANDLE_STMT, retcode );
}

bool Database::IsExistID( std::string id, int* is_exist )
{
	//db에서 있으면 true 없으면 false 반환해주는 내장함수 만들어야 한다
	std::wstring qu {};
	qu += L"EXEC CheckID_Exist ";
	qu += StringToWstring( id );
	std::cout << "id : " << id << std::endl;

	retcode = SQLAllocHandle( SQL_HANDLE_STMT, hdbc, &hstmt );

	retcode = SQLExecDirect( hstmt, ( SQLWCHAR* )qu.c_str(), SQL_NTS );
	if ( retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO )
	{
		retcode = SQLBindCol( hstmt, 1, SQL_C_LONG, &nCount, 10, &cbCount );

		retcode = SQLFetch( hstmt );	//SQLFetch를 사용해서 리턴값을 받는다
		if ( retcode == SQL_ERROR )
			show_error();
		if ( retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO )
		{
			*is_exist = nCount;
			SQLFreeHandle( SQL_HANDLE_STMT, hstmt );
			return true;
		}
		else
		{
			return false;
		}
	}
	return false;
}

bool Database::ConfirmPlayer( std::string id, std::string password,int* mmr )
{
	std::wstring qu {};
	qu += L"EXEC User_Login ";
	qu += StringToWstring( id );
	qu += L", ";
	qu += StringToWstring( password );

	
	retcode = SQLAllocHandle( SQL_HANDLE_STMT, hdbc, &hstmt );
	
	retcode = SQLExecDirect( hstmt, ( SQLWCHAR* )qu.c_str(), SQL_NTS ); 
	if ( retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO )
	{
		retcode = SQLBindCol( hstmt, 1, SQL_C_LONG, &nMmr, 10, &cbMmr );
	
		retcode = SQLFetch( hstmt );	//SQLFetch를 사용해서 리턴값을 받는다
		if ( retcode == SQL_ERROR )
			show_error();
		if ( retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO )
		{
			*mmr = nMmr;
			SQLFreeHandle(SQL_HANDLE_STMT, hstmt);
			return true;
		}
		else
		{
			std::cout << "Get NoneID\n";
			return false;
		}
	}
	return false;
}

void Database::HandleDiagnosticRecord( SQLHANDLE hHandle, SQLSMALLINT hType, RETCODE RetCode )
{
	SQLSMALLINT iRec = 0;
	SQLINTEGER iError;
	WCHAR wszMessage[1000];
	WCHAR wszState[SQL_SQLSTATE_SIZE + 1];
	if ( RetCode == SQL_INVALID_HANDLE ) {
		fwprintf( stderr, L"Invalid handle!\n" );
		return;
	}
	while ( SQLGetDiagRec( hType, hHandle, ++iRec, wszState, &iError, wszMessage,
		( SQLSMALLINT )( sizeof( wszMessage ) / sizeof( WCHAR ) ), ( SQLSMALLINT* )NULL ) == SQL_SUCCESS ) {
		// Hide data truncated..
		if ( wcsncmp( wszState, L"01004", 5 ) ) {
			fwprintf( stderr, L"[%5.5s] %s (%d)\n", wszState, wszMessage, iError );
		}
	}
}

std::wstring Database::StringToWstring( const std::string& str )
{
	typedef std::codecvt_utf8<wchar_t> convert_type;
	std::wstring_convert<convert_type, wchar_t> converter;

	return converter.from_bytes( str );
}

void Database::show_error()
{
	printf( "error\n" );
}