#pragma once
#include "BattleServer/BattleServer/DX_Math.h"
//------------------------------------------------------------------
#define LOBBY_SERVER_PORT		3500
#define MATCHMAKING_SERVER_PORT	5959
#define BATTLE_SERVER_PORT		8888
#define BATTLE_SERVER_UDP_PORT	9105

//#define LOBBY_SERVER_IP			"13.209.64.231"
//#define MATCHMAKING_SERVER_IP	"192.168.103.210"
//#define BATTLE_SERVER_IP		"192.168.103.210"

// 우리 서버 PC에서 사용할 때.
//#define LOBBY_SERVER_IP_PRIVATE			"119.207.192.237"
//#define MATCHMAKING_SERVER_IP_PRIVATE	"119.207.192.237"
//#define BATTLE_SERVER_IP_PRIVATE		"119.207.192.237"
//
//#define LOBBY_SERVER_IP_PUBLIC			"119.207.192.237"
//#define MATCHMAKING_SERVER_IP_PUBLIC	"119.207.192.237"
//#define BATTLE_SERVER_IP_PUBLIC			"119.207.192.237"


// 디버깅 할 때 사용하면 됩니다.
//#define LOBBY_SERVER_IP_PRIVATE			"5.0.145.72"
//#define MATCHMAKING_SERVER_IP_PRIVATE	"5.0.145.72"
//#define BATTLE_SERVER_IP_PRIVATE		"172.31.45.180"
//
//#define LOBBY_SERVER_IP_PUBLIC			"13.125.79.46"
//#define MATCHMAKING_SERVER_IP_PUBLIC	"13.125.79.46"
//#define BATTLE_SERVER_IP_PUBLIC			"54.180.113.27"

#define LOBBY_SERVER_IP_PRIVATE			"192.168.102.22"
#define MATCHMAKING_SERVER_IP_PRIVATE	"192.168.102.22"
#define BATTLE_SERVER_IP_PRIVATE		"192.168.20.143"
#define LOBBY_SERVER_IP_PUBLIC			"192.168.102.22"
#define MATCHMAKING_SERVER_IP_PUBLIC	"192.168.102.22"
#define BATTLE_SERVER_IP_PUBLIC			"192.168.20.143"

//#define LOBBY_SERVER_IP			"127.0.0.1"
//#define MATCHMAKING_SERVER_IP	"127.0.0.1"
//#define BATTLE_SERVER_IP		"127.0.0.1"
//------------------------------------------------------------------

// 패킷 전송 타입
#define PACKET_TYPE_NON_BUFFERED	0
#define PACKET_TYPE_BUFFERED		1

// 매치 타입
#define MATCH_TYPE_AUTO				0
#define MATCH_TYPE_MANUAL			1

// ROOM 입장 거부 사유
#define ROOM_DENY_CODE_ROOM_DOSENT_EXIST	0
#define ROOM_DENY_CODE_ROOM_HAS_STARTED		1
#define ROOM_DENY_CODE_ROOM_IS_FULL			2

// 타입 번호 나중에 한 번 정리하자..
#define LC_MATCH					100
#define LC_LOGINOK					101
#define LC_LOGINDENY				102
#define LC_SIGNUPOK					103
#define LC_SIGNUPDENY				104
#define LC_USERINFO					105
#define LC_CANCEL_AUTOMATCH_SUCCESS	106

#define CL_LOGIN					1
#define CL_AUTOMATCH				2
#define CL_DUMMY_LOGIN				3
#define CL_SIGNUP					4
#define ML_MATCH					5
#define CL_REQUEST_USERINFO			6
#define CL_CANCEL_AUTOMATCH			7
#define ML_CANCEL_AUTOMATCH_SUCCESS	8
#define CL_UPDATED_USER_INFO		9

#define LM_AUTOMATCH				1
#define BM_ROOMREADY				2
#define LM_CANCEL_AUTOMATCH			3
#define LM_CLIENT_DISCONNECT		4

#define MB_REQUEST_ROOM				1

// c -> b
#define CB_LOGIN					2
#define CB_JOIN						3
#define CB_ROOM_LEAVE				4
#define CB_READY					5
#define CB_START					6
#define CB_CHAT						7
#define CB_REQUEST_ROOM_LIST		8
#define CB_REQUSET_ROOM				9	//manual match 방만들기
#define CB_SELECT_CHARACTER			10
#define CB_CANCEL_SELECT_CHARACTER	11
#define CB_MAP_TYPE					12
#define CB_KEY_W_UP					13
#define CB_KEY_W_DOWN				14
#define CB_KEY_A_UP					15
#define CB_KEY_A_DOWN				16
#define CB_KEY_S_UP					17
#define CB_KEY_S_DOWN				18
#define CB_KEY_D_UP					19
#define CB_KEY_D_DOWN				20
#define CB_JUMP						21
#define CB_THUNDER_BOLT				22
#define CB_BULLET_HIT				23
#define CB_TRANSFORM				24
#define CB_TRANSFORM_CHARACTER		25
#define CB_LOOK_VECTOR				26
#define CB_TEST_MINUS_TIME			27
#define CB_TEST_PLUS_TIME			28
#define CB_TEST_ZERO_HP				29

// b -> c
#define BC_MANUAL_ACCEPT_OK			0
#define BC_AUTO_ACCEPT_OK			1
#define BC_ACCEPT_DEINED			2
#define BC_JOIN_OK					4
#define BC_MAP_TYPE					5
#define BC_JOIN_DENY				6
#define BC_ROOM_ENTERED				7
#define BC_ROOM_LEAVED				8
#define BC_NEW_ROOM_MANAGER			9
#define BC_SELECT_CHARACTER			10
#define BC_CANCEL_SELECT_CHARACTER	11
#define BC_SELECT_CHARACTER_FAIL	12
#define BC_READY					13
#define BC_GAME_START				14
#define BC_GAMESTART_AVAILABLE		15
#define BC_GAME_OVER				16

#define BC_LEFT_TIME				17
#define BC_ROUND_START				18
#define BC_CHAT						19

#define BC_ROOM_LIST				20	// 가변길이 패킷으로 수정
#define BC_ROOM_MAKED				21

#define BC_PLAYER_POS				22
#define BC_PLAYER_ROT				23
#define BC_OBJECT_POS				24
#define BC_OBJECT_ROT				25

#define BC_PUT_THUNDERBOLT			26
#define BC_REMOVE_THUNDERBOLT		27
#define BC_TRANSFORM				28
#define BC_TRANSFORM_CHARACTER		29
#define BC_BULLET_NEAR				30
#define BC_HIT						31
#define BC_DIE						32
#define BC_PENALTY_WARNING			33
#define BC_PENALTY_WARNING_END		34
#define BC_PENALTY_BEGIN			35
#define BC_PENALTY_END				36
#define BC_ANIM						37
#define BC_UPDATED_USER_INFO		38

//role
#define ROLE_MASTER			0
#define ROLE_STUDENT		1
#define ROLE_OBSERVER		2

//MAP TYPE
// Gameplay 맵
// 맵 디파인값이 연속적이여야함
#define MAP_TOWN			0
#define TEMP_SECOND_MAP		1 /*두번째 맵*/

//start mmr
#define START_MMR			1500

// player type / id
// type과 id는 동일함
#define CHARACTER_NONE "NO"
#define CHARACTER_WIZARD "WI"
#define CHARACTER_DRUID "DR"
#define CHARACTER_BAIRD "BA"
#define CHARACTER_FEMALE_PEASANT "FP"
#define CHARACTER_MALE_PEASANT "MP"
#define CHARACTER_SORCERER "SO"

// 프랍이외의 오브젝트 타입
#define OBJECT_TYPE_CHARACTER -1
#define OBJECT_TYPE_THUNDERBOLT -2

#define OBJECT_START_INDEX_THUNDERBOLT 1000
#define MAX_THUNDERBOLT_COUNT 30

// ANIMATION TYPES
#define	ANIM_IDLE			0
#define ANIM_FORWARD		1
#define ANIM_BACKWARD		2
#define ANIM_LEFT_STRAFE	3
#define ANIM_RIGHT_STRAFE	4
#define ANIM_ATTACK			5
#define ANIM_RUNNING		6
#define ANIM_JUMP			7

#pragma pack (push, 1)

/*========================CLIENT to LOBBY==========================*/
struct cl_packet_login {
	BYTE size;
	BYTE type;

	char id[10];
	char password[10];
};

struct cl_packet_dummy_login {
	BYTE size;
	BYTE type;
};

struct cl_packet_automatch {
	BYTE size;
	BYTE type;
};

struct cl_packet_signup {
	BYTE size;
	BYTE type;

	char id[10];
	char password[10];
};

struct cl_packet_request_userinfo{
	BYTE size;
	BYTE type;
};

struct cl_packet_cancel_automatch {
	BYTE size;
	BYTE type;
};

struct cl_packet_updated_user_info{
	BYTE size;
	BYTE type;

	int mmr;
	int catched_student_count;
	float winning_rate;
	int total_game_cnt;
	int winning_game_cnt;
};

/*=================================================================*/


/*========================LOBBY to CLIENT==========================*/
struct lc_packet_match {
	BYTE size;
	BYTE type;

	short roomNum;
	char is_host;
};

struct lc_packet_login_ok {
	BYTE size;
	BYTE type;

	int id;
};

struct lc_packet_login_deny {
	BYTE size;
	BYTE type;
};

struct lc_packet_signup_ok {
	BYTE size;
	BYTE type;
};

struct lc_packet_signup_deny {
	BYTE size;
	BYTE type;
};

struct lc_packet_userinfo{
	BYTE size;
	BYTE type;

	char id_str[10];
	int mmr;
	int catched_student_count;
	float winning_rate;
	int total_game_cnt;
	int winning_game_cnt;
};

struct lc_packet_cancel_automatch_success {
	BYTE size;
	BYTE type;
};
/*=================================================================*/


/*========================LOBBY to MATCH===========================*/
struct lm_packet_automatch {
	BYTE size;
	BYTE type;

	short id;
	int mmr;
};

struct lm_packet_cancel_automatch {
	BYTE size;
	BYTE type;

	short id;
};

struct lm_packet_client_disconnect {
	BYTE size;
	BYTE type;

	short id;
};
/*=================================================================*/


/*========================BATTLE to MATCH==========================*/
struct bm_packet_room_ready {
	BYTE size;
	BYTE type;

	int roomNum;
};
/*=================================================================*/


/*========================MATCH to BATTLE==========================*/
struct mb_packet_request_room {
	BYTE size;
	BYTE type;
};	//only auto match
/*=================================================================*/


/*========================BATTLE to CLIENT=========================*/
struct PTC_Start_Info {
	int id;
	char role;
	char spawn_pos;
};

struct PTC_Vector {
	float x;
	float y;
	float z;
};

struct PTC_Room {
	short id;
	wchar_t name[11];
	char participant;
	char joinable;
};

struct bc_packet_bb{
	BYTE size;
	BYTE type;

	XMFLOAT4X4 mats;
	XMFLOAT3 sizes;
};

struct bc_packet_accept_ok {
	BYTE size;
	BYTE type;

	int id;
};

struct bc_packet_accept_deny{
	BYTE size;
	BYTE type;
};

struct bc_packet_join_ok{
	BYTE size;
	BYTE type;
	char playerNo;
};

struct bc_packet_join_deny{
	BYTE size;
	BYTE type;
	char code;
};

struct bc_packet_map_type{
	BYTE size;
	BYTE type;
	char mapType;
};

struct bc_packet_room_entered {
	BYTE size;
	BYTE type;

	int id;
	char ready;
	char playerNo;
	char char_type[3];

	char name[10];
	int mmr;
	int catched_student_count;
	float winning_rate;
	int total_game_cnt;
	int winning_game_cnt;
	char isManager;
};

struct bc_packet_room_leaved {
	BYTE size;
	BYTE type;

	int id;
};

struct bc_packet_new_room_manager{
	BYTE size;
	BYTE type;

	int id;
};

struct bc_packet_select_character{
	BYTE size;
	BYTE type;

	int id;
	char character[3];
};

struct bc_packet_cancel_select_character{
	BYTE size;
	BYTE type;

	int id;
	char character[3];
};

struct bc_packet_select_character_fail{
	BYTE size;
	BYTE type;
};

struct bc_packet_ready {
	BYTE size;
	BYTE type;

	int id;
	char ready;
};

struct bc_packet_game_start {
	BYTE size;
	BYTE type;

	char map_type;
	char penalty_duration;
	PTC_Start_Info info[4];
};

struct bc_packet_gamestart_available{
	BYTE size;
	BYTE type;
	char available;
};

struct bc_packet_game_over {
	BYTE size;
	BYTE type;

	char win_team;	//role재활용
};

struct bc_packet_left_time {
	BYTE size;
	BYTE type;
	unsigned char left_time;
};

struct bc_packet_round_start {
	BYTE size;
	BYTE type;
};//이거 보내면 울타리 문 없애자.

struct bc_packet_room_list { // 67바이트 크기
	BYTE size;
	BYTE type;
	char totalRoomNum;

	PTC_Room room_list[5];
};

struct bc_packet_room_maked {
	BYTE size;
	BYTE type;

	int roomNum;
};

struct bc_packet_player_pos {
	BYTE size;
	BYTE type;

	int id;
	PTC_Vector pos;
};

struct bc_packet_player_rot {
	BYTE size;
	BYTE type;

	int id;
	PTC_Vector look;
};

struct bc_packet_object_pos {
	BYTE size;
	BYTE type;

	short type_id;
	int obj_id;
	PTC_Vector pos;
};

struct bc_packet_object_rot {
	BYTE size;
	BYTE type;

	short type_id;
	float rot_y;
};

struct bc_packet_put_thunderbolt {
	BYTE size;
	BYTE type;

	char bolt_id;
	PTC_Vector pos;
};

struct bc_packet_remove_thunderbolt{
	BYTE size;
	BYTE type;

	char bolt_id;
	PTC_Vector pos;	// 충돌한 위치. ( 이팩트 출력해야할 위치 )
};

struct bc_packet_transform {
	BYTE size;
	BYTE type;

	int id;
	short object_type;
};

struct bc_packet_transform_character{
	BYTE size;
	BYTE type;

	int id;
	char character[3];
};

struct bc_packet_chat {
	BYTE size;
	BYTE type;

	int id;
	wchar_t chat[100];
};

struct bc_packet_bullet_near {
	BYTE size;
	BYTE type;

	PTC_Vector pos;
};

struct bc_packet_hit {
	BYTE size;
	BYTE type;

	int id;
	float hp;
};

struct bc_packet_die {
	BYTE size;
	BYTE type;

	int id;
};

struct bc_packet_penalty_warning{
	BYTE size;
	BYTE type;
};

struct bc_packet_penalty_warning_end{
	BYTE size;
	BYTE type;
};

struct bc_packet_penalty_begin{
	BYTE size;
	BYTE type;
	int id;
};

struct bc_packet_penalty_end{
	BYTE size;
	BYTE type;

	int id;
};

struct bc_packet_anim_type{
	BYTE size;
	BYTE type;

	int id;
	char anim_type;
};

struct bc_packet_updated_user_info{
	BYTE size;
	BYTE type;

	int mmr;
	int catched_student_count;
	float winning_rate;
	int total_game_cnt;
	int winning_game_cnt;
};
/*=================================================================*/


/*========================CLIENT to BATTLE=========================*/
struct cb_packet_login {
	BYTE size;
	BYTE type;

	char is_automatch;
	char name[10];
	int mmr;
	int catched_student_count;
	float winning_rate;
	int total_game_cnt;
	int winning_game_cnt;
};

struct cb_packet_join {
	BYTE size;
	BYTE type;

	int roomNum;
	char is_roomManager;
};

struct cb_packet_room_leave {
	BYTE size;
	BYTE type;
};

struct cb_packet_ready {
	BYTE size;
	BYTE type;
};

struct cb_packet_start {
	BYTE size;
	BYTE type;
};

struct cb_packet_chat {
	BYTE size;
	BYTE type;
	wchar_t chat[100];
};

struct cb_packet_request_room_list {
	BYTE size;
	BYTE type;
};

struct cb_packet_request_room {
	BYTE size;
	BYTE type;
};

struct cb_packet_request_manual_room{
	BYTE size;
	BYTE type;

	wchar_t room_title[11];
};

struct cb_packet_select_character{
	BYTE size;
	BYTE type;

	int id;
	char character[3];
};

struct cb_packet_cancel_select_character{
	BYTE size;
	BYTE type;

	int id;
	char character[3];
};

struct cb_packet_map_type{
	BYTE size;
	BYTE type;

	char map_type;
};

struct cb_packet_move_key_status {
	BYTE size;
	BYTE type;
};

struct cb_packet_thunder_bolt {
	BYTE size;
	BYTE type;

	PTC_Vector dir; // 쏠 방향
};

struct cb_packet_bullet_hit {
	BYTE size;
	BYTE type;

	char damage;
};

struct cb_packet_transform {
	BYTE	size;
	BYTE	type;

	short	obj_type;
};

struct cb_packet_transform_character{	// 원래 모습으로 돌아갈 때, 보내야함.
	BYTE size;
	BYTE type;
};

struct cb_packet_look_vector {
	BYTE size;
	BYTE type;

	int id;
	PTC_Vector look;	//순서대로 look up right
};

struct cb_test_packet_minus_time{
	BYTE size;
	BYTE type;
};

struct cb_test_packet_plus_time{
	BYTE size;
	BYTE type;
};

struct cb_test_packet_zero_hp{
	BYTE size;
	BYTE type;
};

/*=================================================================*/


/*========================MATCH to LOBBY===========================*/
struct ml_packet_match {
	BYTE size;
	BYTE type;

	short id;
	short roomNum;
	char is_host;
};

struct ml_packet_room_ready {
	BYTE size;
	BYTE type;

	short id;
	short roomNum;
};

struct ml_packet_cancel_automatch_success {
	BYTE size;
	BYTE type;

	short id;
};
/*=================================================================*/

#pragma pack (pop)