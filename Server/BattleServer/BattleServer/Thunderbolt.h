#pragma once
#include "Object.h"
class Thunderbolt :
	public Object
{
public:
	Thunderbolt() : Object::Object(){ Initialize(); };
	~Thunderbolt();

public:
	void Initialize();
	void Shoot(const XMFLOAT3& pos, const XMFLOAT3& look, XMFLOAT3& dir, double eTime );

public:
	unsigned short GetDamage() const;
	void SetDamage( const unsigned short& damage );

	void SetUse();
	void SetNotUse();
	bool isUsed();
	void SetCollidable();
	void SetNotCollidable();
	bool isCollidable();

	//안에 있음 true 아니면 false;
	bool CheckBulletInsideMap();

private:
	unsigned short	m_damage;
	bool			m_isUsed;	// 썬더볼트 발사패킷 오면 true로 바꾼다.
	bool			m_isCollidable;
};