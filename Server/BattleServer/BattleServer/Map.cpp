#include "pch.h"
#include "extern.h"
#include "Map.h"

Map::~Map()
{
	for ( int i = 0; i < ID_COUNT; ++i )
	{
		size_t list_size = m_ObjList[i].size();

		for ( int j = 0; j < list_size; ++j )
		{
			if ( m_ObjList ) delete m_ObjList[i][j];
		}
	}

	for ( int i = 0; i < NUM_OF_SECTOR * 2 - 1; ++i )
	{
		for ( int j = 0; j < NUM_OF_SECTOR * 2 - 1; ++j )
		{
			if ( ( i & 1 ) == 0 && ( j & 1 ) == 0 )
			{
				for ( int k = 0; k < ID_COUNT; ++k )
				{
					for ( auto iter = m_SectorObjList[i][j][k].begin(); iter != m_SectorObjList[i][j][k].end(); ++iter )
					{
						if ( *iter ) delete( *iter );
					}
				}
			}
			else
			{
				m_SectorObjList[i][j]->clear();
			}
		}
	}
}

void Map::LoadMap( int mapNo )
{
	for ( int i = 0; i < ID_COUNT; ++i )
	{
		size_t list_size = SHARED_RESOURCE::g_map[mapNo].m_ObjList[i].size();
		m_ObjList[i].resize( list_size );

		for ( int j = 0; j < list_size; ++j )
		{
			m_ObjList[i][j] = new Object( *SHARED_RESOURCE::g_map[mapNo].m_ObjList[i][j] );
			//m_ObjList[i][j] = SHARED_RESOURCE::g_map[mapNo].m_ObjList[i][j]->clone();
			//m_ObjList[i][j].
		}
	}
}

void Map::LoadSectorMap( int mapNo )
{
	for ( int i = 0; i < NUM_OF_SECTOR; ++i )
	{
		for ( int j = 0; j < NUM_OF_SECTOR; ++j )
		{
			for ( int k = 0; k < ID_COUNT; ++k )
			{
				size_t list_size = SHARED_RESOURCE::g_map[mapNo].m_SectorObjList[i*2][j*2][k].size();
				m_SectorObjList[i*2][j*2][k].resize( list_size );

				auto mIter = m_SectorObjList[i*2][j*2][k].begin();
				for ( std::list<Object*>::iterator iter = SHARED_RESOURCE::g_map[mapNo].m_SectorObjList[i*2][j*2][k].begin(); iter != SHARED_RESOURCE::g_map[mapNo].m_SectorObjList[i*2][j*2][k].end(); ++iter )
				{
					*mIter = new Object( **iter );
					++mIter;
				}
			}
		}
	}

	for ( int i = 0; i < NUM_OF_SECTOR * 2 - 1; ++i )
	{
		for ( int j = 0; j < NUM_OF_SECTOR * 2 - 1; ++j )
		{
			if ( ( i % 2 ) == 0 && ( j % 2 ) == 0 );
			else {
				for ( int k = 0; k < ID_COUNT - 1; ++k )	//above는 짝수 섹터에만 존재함 그래서 -1
				{
					size_t list_size = SHARED_RESOURCE::g_map[mapNo].m_SectorObjList[i][j][k].size();
					m_SectorObjList[i][j][k].resize( list_size );

					auto mIter = m_SectorObjList[i][j][k].begin();
					for ( std::list<Object*>::iterator iter = SHARED_RESOURCE::g_map[mapNo].m_SectorObjList[i][j][k].begin(); iter != SHARED_RESOURCE::g_map[mapNo].m_SectorObjList[i][j][k].end(); ++iter )
					{
						int uID = ( *iter )->GetUniqueID();

						for ( int m = i - 1; m <= i + 1; ++m )
						{
							for ( int n = j - 1; n <= j + 1; ++n )
							{
								if ( m < 0 || n < 0 || m >= ( NUM_OF_SECTOR * 2 - 1 ) || n >= ( NUM_OF_SECTOR * 2 - 1 ) );
								else if ( ( m & 1 ) == 0 && ( n & 1 ) == 0 )	//짝수 섹터에서 복사하자 짝수 섹터가 원본이라 생각!
								{
									for ( auto iter2 = m_SectorObjList[m][n][k].begin(); iter2 != m_SectorObjList[m][n][k].end(); ++iter2 )
									{
										if ( ( *iter2 )->GetUniqueID() == uID )
										{
											*mIter = *iter2;
											++mIter;
											goto FIND_MATCHING_OBJ;
										}
									}
								}
							}
						}
					FIND_MATCHING_OBJ:;
					}
				}
			}
		}
	}
}