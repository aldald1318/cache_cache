#include "pch.h"
#include "OutputMemoryStream.h"
#include "BattleServer.h"

#define PACKET_TYPE_BUFFERED 1

OutputMemoryStream::OutputMemoryStream()
{
	ReallocBuffer( 4500 );	// 이더넷 MTU - TCP/IP 헤더
	m_buffer[0] = PACKET_TYPE_BUFFERED;
}

OutputMemoryStream::~OutputMemoryStream()
{
	delete m_buffer;
}

void OutputMemoryStream::Write( const char* data, unsigned short dataSize )
{
	if( this == nullptr ) return;

	m_bufferLock.lock();
	int resultHead{ m_head + dataSize };
	if( resultHead > m_capacity ){
		std::cout << "Buffer size over!" << std::endl;
		// 메모리 재할당해야할 수 있음.
		while( 1 );
	}

	
	std::memcpy( m_buffer + m_head, data, dataSize );	// 여기를 락을 걸어야할듯
	m_head += dataSize;
	m_nWrite++;
	m_bufferLock.unlock();
}

void OutputMemoryStream::Flush(int destID)
{
	if( this == nullptr ) return;

	m_bufferLock.lock();
	char copiedBuffer[4500]{};
	copiedBuffer[0] = PACKET_TYPE_BUFFERED;
	memcpy( copiedBuffer + 1, &m_head, sizeof( USHORT ) );
	memcpy( copiedBuffer + 3, m_buffer, m_head );
	m_head = 0;
	m_nWrite = 0;
	m_bufferLock.unlock();

	//BattleServer::GetInstance()->SendPacketBuffer( destID, copiedBuffer );
}

void OutputMemoryStream::ReallocBuffer( unsigned int byteSize )
{
	m_bufferLock.lock();
	m_buffer = new char[byteSize]{}; // realloc으로 돌려야할 수도 있음.
	if( m_buffer == nullptr ){
		// realloc 실패 예외처리 필요.
		std::cout << "Memory Reallocate Failed!!" << std::endl;
		while( 1 );
	}
	m_capacity = byteSize;
	m_bufferLock.unlock();
}