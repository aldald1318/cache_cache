#pragma once
#include "../../Protocol.h"
#include "Player.h"
#include "Thunderbolt.h"
#include "Globals.h"

struct Sector {
	bool active = false;
	std::vector<int> playerIndex;
	std::vector<int> thunderboltIndex;
};

class Map;
class Physics;
class Room
{
	// 실제 게임을 시뮬레이션하는 클래스.
public:
	explicit Room( int room_no );
	explicit Room( int room_no, wstring room_name );
	~Room();

public:
	void Initialize( int room_no );
	void Reset();
	void Update();			// 게임 월드 업데이트
	void PushMsg( message& );
	void Collision();
	void SectorCollision();
	void TotalSectorCollision();	//게임 시작 전체 월드 모두 충돌 시 부하가 너무 커서 따로 만들음 짝수 섹터만 돌도록
	void WorldUpdate();
	void SectorWorldUpdate();

public:
	bool IsEmpty();	// 방에 인원있는지 확인
	bool EnterRoom( int id ,bool is_roomManager, int* _slot);	// 룸 입장
	void AnounceRoomEnter(int id, int slot);
	void LeaveRoom( int id );	// 룸 퇴장

	void LoadMap( int mapNo );	// 맵 불러오기
	void UpdateActiveSector();
	void UpdateActiveSector( float x, float z, int what, int index );
	void UpdateObjectSector( XMFLOAT3 prePos, XMFLOAT3 curPos, Object* iter, int a, int b );
	void MakeRoomManual();
	std::wstring GetRoomName();
	int GetcurPlayerNum();
	int GetRoomNo();

	void SetMapNo( int map_no );
	int GetMapNo();

	bool IsGameStarted();
	bool IsRoomEnterable() { return m_isEnterable; }
	bool IsRoundStarted();

	void SendLeftTimePacket();	//즉각즉각 보내자 - Send부를껀지 Flush부를껀지

	char& CharTypeIntToStr( int c_type );

	void CheckGameState();
	void GameStart();
	void RoundStart();
	void GameOver(int winner_role);

	void UpdateUserInfo_DB( int winner_role );


public:		//send msg
	//void WriteOutputStream( int id, void* buf );
	void PushSendMsg( int id, void* buff );
	void FlushSendMsg();

	void PushPlayerPositionMsg( int to, int from, PTC_Vector* position_info );
	void PushPlayerDirectionMsg( int to, int from, PTC_Vector lookVector );
	void PushObjectPositionMsg( int id, short type_id, int obj_id, PTC_Vector* position_info );
	void PushTransformMsg( int transformer, short obj_type );
	void PushThunderboltPutMsg(int to, int bolt_id, PTC_Vector boltPos );
	void PushThunderboltRemoveMsg( int bolt_id );
	void PushHitMsg( int hitman, float hpPct );
	void PushDieMsg( int id );

	void PushChatMsg( int id, void* chat );
	void PushReadyMsg( int id, bool ready_status );
	void PushGameStartAvailableMsg( int id, bool available );

	void PushNewRoomManagerMsg( int id );
	void PushMapSelectedMsg();

	void PushPenaltyWarningMsg( int id );
	void PushPenaltyWarningEndMsg( int id );
	void PushPenaltyBeginMsg( int id );
	void PushPenaltyEndMsg( int id );

	void PushAnimMsg( int id, int animType );

public:	// 컨텐츠 메서드
	bool CheckPenaltyEVAddable(int slot);	// EV_PENALTY_CHECK 등록 가능한지 검사하는 함수
	void CheckPenalty( int id );
	void ApplyPenalty( int id );
	void UnapplyPenalty( int id );
	void MakeMove( int id );		// id 플레이어 이동 가능 상태로
	void MakeNoMove( int id );		// id 플레이어 이동 불가 상태로
	void FullWorldUpdateEnd();		//sector 사용시 전체 월드 업데이트 끝내기
	void ThunderboltSetNotUse( int index );

private:	//recv msg
	void CopyRecvMsgs();		// m_msgs에 메세지 push하기 / 또는 큐 복사 
	void ClearCopyMsgs();
	void ProcMsg( message msg );	// msg 타입보고 처리.
	void ProcUnUsedThunderbolt();

	

private:
	bool m_isGameStarted;
	bool m_isEnterable{ true };
	bool m_isRoundStarted;
	bool m_isManualRoom;
	bool m_isSent{ false }; // 방장에게 bc_packet_gamestart_available 한 번만 보내게 하기 위함.
	int m_roomNo;
	std::wstring m_roomName;
	int m_mapNo;
	int m_RoomManager;

	unsigned char m_leftTime;
	std::atomic_int m_countFrame { 0 };

	std::atomic_int			m_curPlayerNum;
	short					m_aliveStudentNum;
	short					m_aliveMasterNum;
	std::array<bool, (int)CHARACTERS::COUNT> m_characterSlot{ false };
	std::array<Player*, MAX_PLAYER>	m_players;
	std::array<Thunderbolt, MAX_THUNDERBOLT_COUNT> m_thunderbolts;

	std::queue<message>		m_recvMsgQueue;				// 이 입력, 이벤트 쌓아놓음
	std::queue<message>		m_copiedRecvMsgs;
	std::mutex				m_recvMsgQueueLock;

	std::queue<SEND_INFO> m_sendMsgQueue;
	std::mutex m_sendMsgQueueLock;


	Physics* m_physics = nullptr;				// 충돌
	Map* m_map = nullptr;
	std::chrono::system_clock::time_point m_lastUpdate;
	std::chrono::system_clock::time_point m_curUpdate;
	std::chrono::duration<float> m_elapsedSec;

	Sector m_ActiveSector[NUM_OF_SECTOR * 2 - 1][NUM_OF_SECTOR * 2 - 1];
	std::atomic_bool m_UpdateFullWorldSector = true; 
	std::mutex m_unUsedThunderboltListLock;
	std::queue<int> m_unUsedThunderboltList;
};