#include "pch.h"
#include "Master.h"
#include "Student.h"
#include "Character.h"
#include "../../Protocol.h"

Master::Master()
{
	Initialize();
}

Master::Master( const Player& rhs ) noexcept : Player( rhs ){ Initialize(); }

Master& Master::operator=( const Player&& player ) noexcept
{
	return *this;
}

Master::~Master()
{
	delete m_CurForm;
	m_CurForm = nullptr;
}

void Master::Initialize()
{
	m_Role = ROLE_MASTER;
	m_isMovable = false;
	m_isDead = false;
}

void Master::Reset()
{
	m_CurForm = nullptr;
	m_isReady = false;
	m_isDead = false;
	m_CharacterType = ( int )CHARACTERS::NONE; // 이거 
	m_keyW = false;
	m_keyA = false;
	m_keyS = false;
	m_keyD = false;
	m_keySpace = false;
	m_animJump = false;
	m_isMovable = false;
	m_CurForm = nullptr;
	m_isReady = false;
	m_Role = ROLE_MASTER;
	m_fTimeAttack = 0.0f;
}

bool Master::Update( float elapsedTime )
{
	// 플레이어의 이동, 회전은 Object::Update 가 하게 해야함. (회전은 Room 에서 바로 처리한다??)
	// Player::Update는 Object::Update에 어느 방향으로 이동할 지, 회전할 지 알려주면 될 것 같음.
	if( m_CurForm == nullptr ) return false;

#ifdef GAME_RULE_ON
	if( !m_isMovable )
		return false;
#endif // GAME_RULE_ON

	if( m_animJump ){
		m_fTimeElapsedJump += elapsedTime;
		if( m_fTimeElapsedJump >= 700.0f ){
			m_animJump = false;
			m_fTimeElapsedJump = 0.0f;
		}
	}

	if( m_isShoot ){
		m_fTimeAttack += elapsedTime;
		if( m_fTimeAttack >= 583.0f ){
			m_isShoot = false;
			m_fTimeAttack = 0.0f;
		}
	}

	XMFLOAT3 force{};
	float forceScalar = 1.f;

	XMFLOAT3 look = m_CurForm->GetLook();
	XMFLOAT3 up = m_CurForm->GetUp();
	XMFLOAT3 right = m_CurForm->GetRight();

	if( m_keyW ) force = Vector3::Add( force, look, forceScalar );
	if( m_keyS ) force = Vector3::Add( force, look, -forceScalar );
	if( m_keyA ) force = Vector3::Add( force, right, -forceScalar );
	if( m_keyD ) force = Vector3::Add( force, right, forceScalar );
	if( m_keySpace ){ force = Vector3::Add( force, up, forceScalar ); m_animJump = true; m_keySpace = false; } // 점프는 좀 더 생각해보자.

	Vector3::Normalize( force );
	if( !Vector3::IsZero( force ) ){
		float curFormForceAmountXZ{ m_CurForm->GetForceAmountXZ() };
		float curFormForceAmountY{ m_CurForm->GetForceAmountY() };
		force.x *= curFormForceAmountXZ;
		force.y *= curFormForceAmountY;
		force.z *= curFormForceAmountXZ;

		m_CurForm->AddForce( force, elapsedTime , false);
	}
	return m_CurForm->Update( elapsedTime, true );
}

void Master::SetIsShoot( bool is_shoot )
{
	m_isShoot = is_shoot;
}

int Master::GetAnimType()
{
	// 이동하면서 발사하면 그냥 공격애니메이션만 나온다.(현재는)
	if( !m_isMovable ) return ANIM_IDLE;

	if( m_isShoot ){
		//m_isShoot = false;
		return ANIM_ATTACK;
	}

	if( m_animJump ){
		return ANIM_JUMP;
	}

	if( m_keyW )
		return ANIM_FORWARD;
	else if( m_keyS )
		return ANIM_BACKWARD;
	else if( m_keyA || (m_keyA && m_keyW) || (m_keyA && m_keyS) )
		return ANIM_LEFT_STRAFE;
	else if( m_keyD || (m_keyD && m_keyW) || (m_keyD && m_keyS) )
		return ANIM_RIGHT_STRAFE;
	else
		return ANIM_IDLE;
}
