#pragma once
#include "pch.h"
using namespace DirectX;

constexpr float EPSILON = 1.0e-10f;
constexpr float PI = 3.1415f;

inline bool IsZero( float fValue ) { return((fabsf( fValue ) < EPSILON)); }
inline bool IsEqual( float fA, float fB ) { return(::IsZero( fA - fB )); }
inline bool IsZero( float fValue, float fEpsilon ) { return((fabsf( fValue ) < fEpsilon)); }
inline bool IsEqual( float fA, float fB, float fEpsilon ) { return(::IsZero( fA - fB, fEpsilon )); }
inline float InverseSqrt( float fValue ) { return 1.0f / sqrtf( fValue ); }
inline void Swap( float* pfS, float* pfT ) { float fTemp = *pfS; *pfS = *pfT; *pfT = fTemp; }

namespace Vector3
{
	inline XMFLOAT3 XMVectorToFloat3( const XMVECTOR& xmvVector )
	{
		XMFLOAT3 xmf3Result;
		XMStoreFloat3( &xmf3Result, xmvVector );
		return(xmf3Result);
	}

	inline XMFLOAT3 ScalarProduct( const XMFLOAT3& xmf3Vector, float fScalar, bool bNormalize = true )
	{
		XMFLOAT3 xmf3Result;
		if( bNormalize )
			XMStoreFloat3( &xmf3Result, XMVector3Normalize( XMLoadFloat3( &xmf3Vector ) ) * fScalar );
		else
			XMStoreFloat3( &xmf3Result, XMLoadFloat3( &xmf3Vector ) * fScalar );
		return(xmf3Result);
	}

	inline XMFLOAT3 Multiply( const XMFLOAT3& xmf3Vector, float fScalar )
	{
		XMFLOAT3 xmf3Result;
		XMStoreFloat3( &xmf3Result, XMLoadFloat3( &xmf3Vector ) * fScalar );
		return( xmf3Result );
	}

	inline XMFLOAT3 Division( const XMFLOAT3& xmf3Vector, float fScalar )
	{
		XMFLOAT3 xmf3Result;
		XMStoreFloat3( &xmf3Result, XMLoadFloat3( &xmf3Vector ) / fScalar );
		return( xmf3Result );
	}

	inline XMFLOAT3 Division( const XMFLOAT3& xmf3Vector1, const XMFLOAT3& xmf3Vector2 )
	{
		XMFLOAT3 xmf3Result;
		XMStoreFloat3( &xmf3Result, XMLoadFloat3( &xmf3Vector1 ) / XMLoadFloat3( &xmf3Vector2 ) );
		return( xmf3Result );
	}

	inline XMFLOAT3 Add( const XMFLOAT3& xmf3Vector1, const XMFLOAT3& xmf3Vector2 )
	{
		XMFLOAT3 xmf3Result;
		XMStoreFloat3( &xmf3Result, XMLoadFloat3( &xmf3Vector1 ) + XMLoadFloat3( &xmf3Vector2 ) );
		return(xmf3Result);
	}

	inline XMFLOAT3 Add( const XMFLOAT3& xmf3Vector1, const XMFLOAT3& xmf3Vector2, float fScalar )
	{
		XMFLOAT3 xmf3Result;
		XMStoreFloat3( &xmf3Result, XMLoadFloat3( &xmf3Vector1 ) + (XMLoadFloat3( &xmf3Vector2 ) * fScalar) );
		return(xmf3Result);
	}

	inline XMFLOAT3 Subtract( const XMFLOAT3& xmf3Vector1, const XMFLOAT3& xmf3Vector2 )
	{
		XMFLOAT3 xmf3Result;
		XMStoreFloat3( &xmf3Result, XMLoadFloat3( &xmf3Vector1 ) - XMLoadFloat3( &xmf3Vector2 ) );
		return(xmf3Result);
	}

	inline float DotProduct( const XMFLOAT3& xmf3Vector1, const XMFLOAT3& xmf3Vector2 )
	{
		XMFLOAT3 xmf3Result;
		XMStoreFloat3( &xmf3Result, XMVector3Dot( XMLoadFloat3( &xmf3Vector1 ), XMLoadFloat3( &xmf3Vector2 ) ) );
		return(xmf3Result.x);
	}

	inline XMFLOAT3 CrossProduct( const XMFLOAT3& xmf3Vector1, const XMFLOAT3& xmf3Vector2, bool bNormalize = true )
	{
		XMFLOAT3 xmf3Result;
		if( bNormalize )
			XMStoreFloat3( &xmf3Result, XMVector3Normalize( XMVector3Cross( XMLoadFloat3( &xmf3Vector1 ), XMLoadFloat3( &xmf3Vector2 ) ) ) );
		else
			XMStoreFloat3( &xmf3Result, XMVector3Cross( XMLoadFloat3( &xmf3Vector1 ), XMLoadFloat3( &xmf3Vector2 ) ) );
		return(xmf3Result);
	}

	inline XMFLOAT3 Normalize( const XMFLOAT3& xmf3Vector )
	{
		XMFLOAT3 m_xmf3Normal;
		XMStoreFloat3( &m_xmf3Normal, XMVector3Normalize( XMLoadFloat3( &xmf3Vector ) ) );
		return(m_xmf3Normal);
	}

	inline float Length( const XMFLOAT3& xmf3Vector )
	{
		XMFLOAT3 xmf3Result;
		XMStoreFloat3( &xmf3Result, XMVector3Length( XMLoadFloat3( &xmf3Vector ) ) );
		return(xmf3Result.x);
	}

	inline bool IsZero( const XMFLOAT3& xmf3Vector )
	{
		XMFLOAT3 xmf3Result;
		XMStoreFloat3( &xmf3Result, XMVector3Length( XMLoadFloat3( &xmf3Vector ) ) );
		return(::IsZero( xmf3Result.x ));
	}

	inline float Angle( const XMVECTOR& xmvVector1, const XMVECTOR& xmvVector2 )
	{
		XMVECTOR xmvAngle = XMVector3AngleBetweenNormals( xmvVector1, xmvVector2 );
		return(XMConvertToDegrees( acosf( XMVectorGetX( xmvAngle ) ) ));
	}

	inline float Angle( const XMFLOAT3& xmf3Vector1, const XMFLOAT3& xmf3Vector2 )
	{
		XMVECTOR xmvVector1 = XMLoadFloat3( &xmf3Vector1 );
		XMVECTOR xmvVector2 = XMLoadFloat3( &xmf3Vector2 );
		return(Angle( xmvVector1, xmvVector2 ));
	}

	inline XMFLOAT3 Transform( const XMFLOAT3& xmf3Vector, const XMFLOAT4X4& xmmtxTransform )
	{
		XMFLOAT3 xmf3Result;
		XMStoreFloat3( &xmf3Result, XMVector3Transform( XMLoadFloat3( &xmf3Vector ), XMLoadFloat4x4( &xmmtxTransform ) ) );
		return( xmf3Result );
	}

	inline XMFLOAT3 TransformNormal( const XMFLOAT3& xmf3Vector, const XMFLOAT4X4& xmmtxTransform )
	{
		XMFLOAT3 xmf3Result;
		XMStoreFloat3( &xmf3Result, XMVector3TransformNormal( XMLoadFloat3( &xmf3Vector ), XMLoadFloat4x4( &xmmtxTransform ) ) );
		return(xmf3Result);
	}

	inline XMFLOAT3 TransformCoord( const XMFLOAT3& xmf3Vector, const XMMATRIX& xmmtxTransform )
	{
		XMFLOAT3 xmf3Result;
		XMStoreFloat3( &xmf3Result, XMVector3TransformCoord( XMLoadFloat3( &xmf3Vector ), xmmtxTransform ) );
		return(xmf3Result);
	}

	inline XMFLOAT3 TransformCoord( const XMFLOAT3& xmf3Vector, const XMFLOAT4X4& xmf4x4Matrix )
	{
		XMMATRIX xmmtxTransform = XMLoadFloat4x4( &xmf4x4Matrix );
		return(TransformCoord( xmf3Vector, xmmtxTransform ));
	}
}

inline void QuatToMatrix( XMFLOAT4* quat, XMFLOAT4X4* rot )
{
	float wx, wy, wz, xx, yy, yz, xy, xz, zz, x2, y2, z2;

	// ��� ���
	x2 = quat->x + quat->x; y2 = quat->y + quat->y;
	z2 = quat->z + quat->z;
	xx = quat->x * x2; xy = quat->x * y2; xz = quat->x * z2;
	yy = quat->y * y2; yz = quat->y * z2; zz = quat->z * z2;
	wx = quat->w * x2; wy = quat->w * y2; wz = quat->w * z2;

	rot->_11 = 1.0f - (yy + zz); rot->_21 = xy - wz;
	rot->_31 = xz + wy; rot->_41 = 0.0f;

	rot->_12 = xy + wz; rot->_22 = 1.0f - (xx + zz);
	rot->_32 = yz - wx; rot->_42 = 0.0f;


	rot->_13 = xz - wy; rot->_23 = yz + wx;
	rot->_33 = 1.0f - (xx + yy); rot->_43 = 0.0f;


	rot->_14 = 0; rot->_24 = 0;
	rot->_34 = 0; rot->_44 = 1;
}

inline XMFLOAT4 QuatFromTwoVectors( XMFLOAT3 u, XMFLOAT3 v )
{
	float cos_theta{ Vector3::DotProduct( u, v ) };
	std::cout << "cos theta - " << cos_theta << std::endl;
	float angle{ acos( cos_theta ) };
	std::cout << "angle - " << XMConvertToDegrees( angle ) << std::endl;

	XMFLOAT3 w = Vector3::Normalize( Vector3::CrossProduct( u, v ) );
	XMFLOAT4 quat{};

	if( angle == 0 )
		return quat;
	else
		XMStoreFloat4( &quat, XMQuaternionRotationAxis( XMLoadFloat3( &w ), angle) );
	return quat;
}

inline XMFLOAT4 QuatFromTwoVectors( XMFLOAT3 u, XMFLOAT3 uUP, XMFLOAT3 v )
{
	float angle = acos( Vector3::DotProduct( uUP, v ) );
	float first_half = sin( Vector3::DotProduct( v, Vector3::CrossProduct( uUP, u ) ) ) >= 0;
	angle = first_half ? angle : 2 * PI - angle;
	std::cout << XMConvertToDegrees( angle ) << std::endl;

	XMFLOAT3 w = Vector3::Normalize( Vector3::CrossProduct( u, v ) );
	XMFLOAT4 quat{};

	if( angle == 0 )
		return quat;
	else
		XMStoreFloat4( &quat, XMQuaternionRotationAxis( XMLoadFloat3( &w ), angle ) );
	return quat;
}

namespace Vector4
{
	inline XMFLOAT4 XMVectorToFloat4( const XMVECTOR& xmvVector )
	{
		XMFLOAT4 xmf4Result;
		XMStoreFloat4( &xmf4Result, xmvVector );
		return(xmf4Result);
	}

	inline XMFLOAT4 Add( const XMFLOAT4& xmf4Vector1, const XMFLOAT4& xmf4Vector2 )
	{
		XMFLOAT4 xmf4Result;
		XMStoreFloat4( &xmf4Result, XMLoadFloat4( &xmf4Vector1 ) + XMLoadFloat4( &xmf4Vector2 ) );
		return(xmf4Result);
	}

	inline XMFLOAT4 Multiply( const XMFLOAT4& xmf4Vector1, const XMFLOAT4& xmf4Vector2 )
	{
		XMFLOAT4 xmf4Result;
		XMStoreFloat4( &xmf4Result, XMLoadFloat4( &xmf4Vector1 ) * XMLoadFloat4( &xmf4Vector2 ) );
		return(xmf4Result);
	}

	inline XMFLOAT4 Multiply( float fScalar, const XMFLOAT4& xmf4Vector )
	{
		XMFLOAT4 xmf4Result;
		XMStoreFloat4( &xmf4Result, fScalar * XMLoadFloat4( &xmf4Vector ) );
		return(xmf4Result);
	}
}

namespace Matrix4x4
{
	inline XMFLOAT4X4 Identity()
	{
		XMFLOAT4X4 xmf4x4Result;
		XMStoreFloat4x4( &xmf4x4Result, XMMatrixIdentity() );
		return(xmf4x4Result);
	}

	inline XMFLOAT4X4 Zero()
	{
		XMFLOAT4X4 xmf4x4Result;
		XMStoreFloat4x4( &xmf4x4Result, XMMatrixSet( 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f ) );
		return(xmf4x4Result);
	}

	inline XMFLOAT4X4 Multiply( const XMFLOAT4X4& xmf4x4Matrix1, const XMFLOAT4X4& xmf4x4Matrix2 )
	{
		XMFLOAT4X4 xmf4x4Result;
		XMStoreFloat4x4( &xmf4x4Result, XMLoadFloat4x4( &xmf4x4Matrix1 ) * XMLoadFloat4x4( &xmf4x4Matrix2 ) );
		return(xmf4x4Result);
	}

	inline XMFLOAT4X4 Scale( const XMFLOAT4X4& xmf4x4Matrix, float fScale )
	{
		XMFLOAT4X4 xmf4x4Result;
		XMStoreFloat4x4( &xmf4x4Result, XMLoadFloat4x4( &xmf4x4Matrix ) * fScale );
		/*
				XMVECTOR S, R, T;
				XMMatrixDecompose(&S, &R, &T, XMLoadFloat4x4(&xmf4x4Matrix));
				S = XMVectorScale(S, fScale);
				T = XMVectorScale(T, fScale);
				R = XMVectorScale(R, fScale);
				//R = XMQuaternionMultiply(R, XMVectorSet(0, 0, 0, fScale));
				XMStoreFloat4x4(&xmf4x4Result, XMMatrixAffineTransformation(S, XMVectorZero(), R, T));
		*/
		return(xmf4x4Result);
	}

	inline XMFLOAT4X4 Add( const XMFLOAT4X4& xmf4x4Matrix1, const XMFLOAT4X4& xmf4x4Matrix2 )
	{
		XMFLOAT4X4 xmf4x4Result;
		XMStoreFloat4x4( &xmf4x4Result, XMLoadFloat4x4( &xmf4x4Matrix1 ) + XMLoadFloat4x4( &xmf4x4Matrix2 ) );
		return(xmf4x4Result);
	}

	inline XMFLOAT4X4 Multiply( const XMFLOAT4X4& xmf4x4Matrix1, const XMMATRIX& xmmtxMatrix2 )
	{
		XMFLOAT4X4 xmf4x4Result;
		XMStoreFloat4x4( &xmf4x4Result, XMLoadFloat4x4( &xmf4x4Matrix1 ) * xmmtxMatrix2 );
		return(xmf4x4Result);
	}

	inline XMFLOAT4X4 Multiply( const XMMATRIX& xmmtxMatrix1, const XMFLOAT4X4& xmf4x4Matrix2 )
	{
		XMFLOAT4X4 xmf4x4Result;
		XMStoreFloat4x4( &xmf4x4Result, xmmtxMatrix1 * XMLoadFloat4x4( &xmf4x4Matrix2 ) );
		return(xmf4x4Result);
	}

	inline XMFLOAT4X4 Interpolate( const XMFLOAT4X4& xmf4x4Matrix1, const XMFLOAT4X4& xmf4x4Matrix2, float t )
	{
		XMFLOAT4X4 xmf4x4Result;
		XMVECTOR S0, R0, T0, S1, R1, T1;
		XMMatrixDecompose( &S0, &R0, &T0, XMLoadFloat4x4( &xmf4x4Matrix1 ) );
		XMMatrixDecompose( &S1, &R1, &T1, XMLoadFloat4x4( &xmf4x4Matrix2 ) );
		XMVECTOR S = XMVectorLerp( S0, S1, t );
		XMVECTOR T = XMVectorLerp( T0, T1, t );
		XMVECTOR R = XMQuaternionSlerp( R0, R1, t );
		XMStoreFloat4x4( &xmf4x4Result, XMMatrixAffineTransformation( S, XMVectorZero(), R, T ) );
		return(xmf4x4Result);
	}

	inline XMFLOAT4X4 Inverse( const XMFLOAT4X4& xmf4x4Matrix )
	{
		XMFLOAT4X4 xmf4x4Result;
		XMStoreFloat4x4( &xmf4x4Result, XMMatrixInverse( NULL, XMLoadFloat4x4( &xmf4x4Matrix ) ) );
		return(xmf4x4Result);
	}

	inline XMFLOAT4X4 Transpose( const XMFLOAT4X4& xmf4x4Matrix )
	{
		XMFLOAT4X4 xmf4x4Result;
		XMStoreFloat4x4( &xmf4x4Result, XMMatrixTranspose( XMLoadFloat4x4( &xmf4x4Matrix ) ) );
		return(xmf4x4Result);
	}

	inline XMFLOAT4X4 PerspectiveFovLH( float FovAngleY, float AspectRatio, float NearZ, float FarZ )
	{
		XMFLOAT4X4 xmf4x4Result;
		XMStoreFloat4x4( &xmf4x4Result, XMMatrixPerspectiveFovLH( FovAngleY, AspectRatio, NearZ, FarZ ) );
		return(xmf4x4Result);
	}

	inline XMFLOAT4X4 LookAtLH( const XMFLOAT3& xmf3EyePosition, const XMFLOAT3& xmf3LookAtPosition, const XMFLOAT3& xmf3UpDirection )
	{
		XMFLOAT4X4 xmf4x4Result;
		XMStoreFloat4x4( &xmf4x4Result, XMMatrixLookAtLH( XMLoadFloat3( &xmf3EyePosition ), XMLoadFloat3( &xmf3LookAtPosition ), XMLoadFloat3( &xmf3UpDirection ) ) );
		return(xmf4x4Result);
	}

	inline XMFLOAT4X4 PerspectiveFovRH( float FovAngleY, float AspectRatio, float NearZ, float FarZ )
	{
		XMFLOAT4X4 xmf4x4Result;
		XMStoreFloat4x4( &xmf4x4Result, XMMatrixPerspectiveFovRH( FovAngleY, AspectRatio, NearZ, FarZ ) );
		return(xmf4x4Result);
	}

	inline XMFLOAT4X4 LookAtRH( const XMFLOAT3& xmf3EyePosition, const XMFLOAT3& xmf3LookAtPosition, const XMFLOAT3& xmf3UpDirection )
	{
		XMFLOAT4X4 xmf4x4Result;
		XMStoreFloat4x4( &xmf4x4Result, XMMatrixLookAtRH( XMLoadFloat3( &xmf3EyePosition ), XMLoadFloat3( &xmf3LookAtPosition ), XMLoadFloat3( &xmf3UpDirection ) ) );
		return(xmf4x4Result);
	}
}


inline XMVECTOR QuatFromMatrix( XMFLOAT4X4 matrix )
{
	XMMATRIX xmMatrix{ XMMatrixSet(
		matrix._11, matrix._12, matrix._13, matrix._14,
		matrix._21, matrix._22, matrix._23, matrix._24,
		matrix._31, matrix._32, matrix._33, matrix._34,
		matrix._41, matrix._42, matrix._43, matrix._44 ) };

	return XMQuaternionRotationMatrix( xmMatrix );
}



