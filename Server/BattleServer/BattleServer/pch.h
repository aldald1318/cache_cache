#define _CRT_SECURE_NO_WARNINGS
#pragma once

#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <string>
#include <sstream>
#include <random>
#include <map>
#include <vector>
#include <list>
#include <array>
#include <queue>
#include <algorithm>
#include <string>
#include <chrono>
#include <mutex>
#include <atomic>
#include <WinSock2.h>
#include <WS2tcpip.h>
#include <DirectXMath.h>
#include <cmath>

#include "SocketAddress.h"
#include "SocketUtil.h"
#include "TCPSocket.h"
#include "UDPSocket.h"
#include "OutputMemoryStream.h"

#pragma comment(lib, "Ws2_32.lib")

#define _USE_MATH_DEFINES

//#define TEST	// 1명도 게임 시작 가능.
#define OVER_2P	// 2명
//#define NORMAL	// 일반 게임 ( 3명, 4명 )

#define GAME_RULE_ON 
//#define GAME_RULE_OFF // 변신 페널티 X, 스승 30초 대기 X, 학생 죽지 않음

//#define LOG_ON

#define SECTOR_ON