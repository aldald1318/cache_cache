#include "pch.h"
#include "Globals.h"
#include "Object.h"
#include "Boundary.h"
#include "DX_Math.h"

Object::Object()
{
	m_UniqueID = -1;
	m_xmf4x4World = Matrix4x4::Identity();
	m_xmfVel = { 0.f, 0.f, 0.f };
	m_xmfAcc = { 0.f, 0.f, 0.f };
	m_xmf3PrePosition = { 0.f, 0.f, 0.f };
	m_forceAmountXZ = 0.0f;
	m_forceAmountY = 0.0f;
	m_mass = 0.f;
	m_fricCoef = 0.f;
	m_maxVel = 0.f;
	m_boundaries = new Boundary;
}

Object::Object( const Object& other ) noexcept
{
	m_UniqueID = other.m_UniqueID;

	m_xmf4x4World = other.m_xmf4x4World;
	m_xmf3PrePosition = other.m_xmf3PrePosition;
	m_xmfVel = other.m_xmfVel;
	m_xmfAcc = other.m_xmfAcc;
	m_forceAmountXZ = other.m_forceAmountXZ;
	m_forceAmountY = other.m_forceAmountY;
	m_mass = other.m_mass;
	m_fricCoef = other.m_fricCoef;
	m_maxVel = other.m_maxVel;

	m_boundaries = new Boundary( *other.m_boundaries );
}

Object& Object::operator=( const Object& other )
{
	if( this != &other ) {
		delete m_boundaries;

		m_xmf4x4World = other.m_xmf4x4World;
		m_xmfVel = other.m_xmfVel;
		m_xmfAcc = other.m_xmfAcc;
		m_forceAmountXZ = other.m_forceAmountXZ;
		m_forceAmountY = other.m_forceAmountY;
		m_mass = other.m_mass;
		m_fricCoef = other.m_fricCoef;
		m_maxVel = other.m_maxVel;

		m_boundaries = other.m_boundaries;
	}
	return *this;
}

//bool Object::Update( float elapsedTime, bool player )
//{
//	float nForce { GRAVITY * m_mass };
//	float frictionSize = nForce * m_fricCoef * 10.f;
//
//	float velSize { sqrtf( pow( m_xmfVel.x, 2 ) + pow( m_xmfVel.y, 2 ) + pow( m_xmfVel.z, 2 ) ) };
//	if ( velSize <= FLT_EPSILON )
//	{
//		return false;
//	}
//	else if ( velSize > FLT_EPSILON )
//	{
//		m_xmPrePos = GetPosition();
//
//		XMFLOAT3 normalizedVel { m_xmfVel.x / velSize, m_xmfVel.y / velSize, m_xmfVel.z / velSize }; // 방향?
//
//		float frictionX = -normalizedVel.x * frictionSize;
//		float frictionZ = -normalizedVel.z * frictionSize;
//
//		XMFLOAT3 fricAcc { frictionX / m_mass, -GRAVITY, frictionZ / m_mass };
//
//		XMFLOAT3 newVel { m_xmfVel.x + fricAcc.x * elapsedTime, m_xmfVel.y + fricAcc.y * elapsedTime, m_xmfVel.z + fricAcc.z * elapsedTime };
//
//		if ( newVel.x * m_xmfVel.x <= FLT_EPSILON )
//			m_xmfVel.x = 0.f;
//		else
//			m_xmfVel.x = newVel.x;
//
//		if ( newVel.z * m_xmfVel.z <= FLT_EPSILON )
//			m_xmfVel.z = 0.f;
//		else
//			m_xmfVel.z = newVel.z;
//	}
//	else if ( m_xmf4x4World._42 > FLT_EPSILON )
//	{
//		m_xmfVel.y -= GRAVITY * elapsedTime; // 이부분때문에 건물 쌓은거 문제있을 수 있음.
//	}
//	else
//	{
//		XMFLOAT3 setvel { 0.f,-GRAVITY,0.f };
//		SetVelocity( setvel );
//	}
//
//	m_xmf4x4World._41 += m_xmfVel.x * elapsedTime;
//	m_xmf4x4World._42 += m_xmfVel.y * elapsedTime;
//	m_xmf4x4World._43 += m_xmfVel.z * elapsedTime;
//
//	if ( m_xmf4x4World._42 < FLT_EPSILON ) {
//		m_xmf4x4World._42 = 0.f;
//		m_xmfVel.y = -GRAVITY;
//	}
//
//	GetRotation();
//	/*std::cout << "x - " << XMConvertToDegrees( GetRotation().x ) << std::endl;
//	std::cout << "y - " << XMConvertToDegrees( GetRotation().y ) << std::endl;
//	std::cout << "z - " << XMConvertToDegrees( GetRotation().z ) << std::endl;*/
//	
//	XMFLOAT3 change = Vector3::Subtract( GetPosition(), m_xmPrePos );
//	if ( Vector3::IsZero( change ) )
//		return false;
//	return true;
//}

bool Object::Update( float elapsedTime, bool is_player )	// 이게 진짜 업데이트
{
	m_xmf3PrePosition = GetPosition();

	float nForce{ GRAVITY * m_mass };			//수직항력
	float frictionSize = nForce * m_fricCoef;	//마찰력

	//direction
	XMFLOAT3 normalizedVel = Vector3::Normalize( m_xmfVel );

	//friction force
	float frictionX = -normalizedVel.x * frictionSize;
	float frictionZ = -normalizedVel.z * frictionSize;

	//friction acc
	XMFLOAT3 fricAcc{};
	if( m_isApplyGravity )
		fricAcc = { frictionX / m_mass, -GRAVITY / pow( elapsedTime, 2 ), frictionZ / m_mass };
	else
		fricAcc = { frictionX / m_mass, 0, frictionZ / m_mass };

	//update velocity by friction force
	XMFLOAT3 newVel{ m_xmfVel.x + fricAcc.x * elapsedTime, m_xmfVel.y - GRAVITY * elapsedTime, m_xmfVel.z + fricAcc.z * elapsedTime };


	if( newVel.x * m_xmfVel.x <= FLT_EPSILON )
		m_xmfVel.x = 0.f;
	else
		m_xmfVel.x = newVel.x;

	if( newVel.z * m_xmfVel.z <= FLT_EPSILON )
		m_xmfVel.z = 0.f;
	else
		m_xmfVel.z = newVel.z;

	m_xmfVel.y = newVel.y;

	if( is_player )
	{
		float velSize = sqrtf( pow( m_xmfVel.x, 2 ) + pow( m_xmfVel.z, 2 ) );
		//std::cout.flush();
		if( velSize > m_maxVel )
		{
			m_xmfVel.x = m_xmfVel.x / velSize * m_maxVel;
			m_xmfVel.z = m_xmfVel.z / velSize * m_maxVel;
		}
	}

	m_xmf4x4World._41 += m_xmfVel.x * elapsedTime;
	m_xmf4x4World._42 += m_xmfVel.y * elapsedTime;
	m_xmf4x4World._43 += m_xmfVel.z * elapsedTime;

	if( m_xmf4x4World._42 < FLT_EPSILON )
	{
		m_xmf4x4World._42 = 0.f;
		m_xmfVel.y = 0.f;
	}

	/*if( m_xmfVel.y != 0.0f && m_xmf4x4World._42 != 0.0f ){
		std::cout << "y velocity = " << m_xmfVel.y << ",	";
		std::cout << "y position = " << m_xmf4x4World._42 << std::endl;
	}
	float maxY{};
	if( m_xmfVel.y <= 0.f )
		maxY = m_xmf4x4World._42 + 120.0f;
	else
		maxY = 120.f;

	if( m_xmf4x4World._42 >= maxY )
		m_xmf4x4World._42 = maxY;*/

	XMFLOAT3 change = Vector3::Subtract( GetPosition(), m_xmf3PrePosition );
	if( Vector3::IsZero( change ) )
		return false;
	else
	{
		// rotation은 아직 안생기므로 패스!
		m_boundaries->BoundaryMove( change );
		return true;
	}
}

bool Object::BulletUpdate( float elapsedTime )
{
	m_xmf3PrePosition = GetPosition();

	float velSize = sqrtf( pow( m_xmfVel.x, 2 ) + pow( m_xmfVel.y, 2 ) + pow( m_xmfVel.z, 2 ) );
	if( velSize > m_maxVel )
	{
		m_xmfVel.x = m_xmfVel.x / velSize * m_maxVel;
		m_xmfVel.y = m_xmfVel.y / velSize * m_maxVel;
		m_xmfVel.z = m_xmfVel.z / velSize * m_maxVel;
	}

	m_xmf4x4World._41 += m_xmfVel.x * elapsedTime;
	m_xmf4x4World._42 += m_xmfVel.y * elapsedTime;
	m_xmf4x4World._43 += m_xmfVel.z * elapsedTime;

	return true;
}

void Object::AddForce( XMFLOAT3 force, float elapsedTime, bool isBullet )
{
	if( m_boundaries->GetObjType() != OBJ_TYPE_CHARACTER &&
		m_boundaries->GetObjType() != OBJ_TYPE_THUNDERBOLT )
	{
		// 프랍으로 변신했을 때 공중에 띄우기 위해 곱해줌.
		force.y = 0.35f * m_forceAmountY;	
	}

	XMFLOAT3 acc{ 0.f, 0.f, 0.f };

	acc = Vector3::Division( force, m_mass );

	m_xmfVel.x += acc.x * elapsedTime;
	m_xmfVel.z += acc.z * elapsedTime;
	if ( isBullet ) m_xmfVel.y += acc.y * elapsedTime;
	else m_xmfVel.y += acc.y * 16.0f;
}

XMFLOAT4X4 Object::GetMatrix() const
{
	return m_xmf4x4World;
}

float Object::GetMass() const
{
	return m_mass;
}

float Object::GetFricCoef() const
{
	return m_fricCoef;
}

float Object::GetMaxVel() const
{
	return m_maxVel;
}

float Object::GetForceAmountXZ() const
{
	return m_forceAmountXZ;
}

float Object::GetForceAmountY() const
{
	return m_forceAmountY;
}

void Object::SetMass( float mass )
{
	m_mass = mass;
}

void Object::SetFricCoef( float friction )
{
	m_fricCoef = friction;
}

void Object::SetMaxVel( float velocity )
{
	m_maxVel = velocity;
}

XMFLOAT3 Object::GetPosition() const
{
	return XMFLOAT3( m_xmf4x4World._41, m_xmf4x4World._42, m_xmf4x4World._43 );
}

XMFLOAT3 Object::GetRotation() const
{
	/*XMFLOAT3 eulerAngle;
	eulerAngle.x = atan2f( -m_xmf4x4World._23, m_xmf4x4World._33 );
	float cosYangle = sqrt( pow( m_xmf4x4World._11, 2 ) + pow( m_xmf4x4World._12, 2 ) );
	eulerAngle.y = atan2f( m_xmf4x4World._13, cosYangle );
	float sinXangle = sin( eulerAngle.x );
	float cosXangle = cos( eulerAngle.x );
	eulerAngle.z = atan2f( cosXangle * m_xmf4x4World._21 + sinXangle * m_xmf4x4World._31, cosXangle * m_xmf4x4World._22 + sinXangle * m_xmf4x4World._32 );
	eulerAngle.x = XMConvertToDegrees( eulerAngle.x );
	eulerAngle.y = XMConvertToDegrees( eulerAngle.y );
	eulerAngle.z = XMConvertToDegrees( eulerAngle.z );
	return eulerAngle;*/
	return XMFLOAT3{};
}

XMFLOAT4 Object::GetRotationQuaternion() const
{
	return QuatFromTwoVectors( m_PreLook, GetLook() );
}

void Object::SetPosition( float x, float y, float z )
{
	m_xmf4x4World._41 = x;
	m_xmf4x4World._42 = y;
	m_xmf4x4World._43 = z;
}

void Object::SetPosition( XMFLOAT3 xmfPosition )
{
	SetPosition( xmfPosition.x, xmfPosition.y, xmfPosition.z );
}

XMFLOAT3 Object::GetPrePosition()const
{
	return m_xmf3PrePosition;
}

void Object::SetPrePosition( XMFLOAT3 xmfPosition )
{
	m_xmf3PrePosition = xmfPosition;
}

XMFLOAT3 Object::GetLook() const
{
	return (Vector3::Normalize( XMFLOAT3( m_xmf4x4World._31, m_xmf4x4World._32, m_xmf4x4World._33 ) ));
}

void Object::SetLook( const float& x, const float& y, const float& z )
{
	m_xmf4x4World._31 = x;
	m_xmf4x4World._32 = y;
	m_xmf4x4World._33 = z;
}

XMFLOAT3 Object::GetPreLook() const
{
	return m_PreLook;
}

void Object::SetPreLook( XMFLOAT3 look )
{
	m_PreLook = look;
}

XMFLOAT3 Object::GetUp() const
{
	return (Vector3::Normalize( XMFLOAT3( m_xmf4x4World._21, m_xmf4x4World._22, m_xmf4x4World._23 ) ));
}

void Object::SetUp( const float& x, const float& y, const float& z )
{
	m_xmf4x4World._21 = x;
	m_xmf4x4World._22 = y;
	m_xmf4x4World._23 = z;
}

XMFLOAT3 Object::GetRight() const
{
	return (Vector3::Normalize( XMFLOAT3( m_xmf4x4World._11, m_xmf4x4World._12, m_xmf4x4World._13 ) ));
}

void Object::SetRight( const float& x, const float& y, const float& z )
{
	m_xmf4x4World._11 = x;
	m_xmf4x4World._12 = y;
	m_xmf4x4World._13 = z;
}

XMFLOAT3 Object::GetVelocity() const
{
	return m_xmfVel;
}

void Object::SetVelocity( XMFLOAT3 xmfVel )
{
	m_xmfVel = xmfVel;
}

void Object::AddVelocity( XMFLOAT3 xmfVel )
{
	m_xmfVel = Vector3::Add( m_xmfVel, xmfVel );
}

void Object::SubVelocity( XMFLOAT3 xmfVel )
{
	m_xmfVel = Vector3::Subtract( m_xmfVel, xmfVel );
}

void Object::InitYVelocity()
{
	m_xmfVel.y = -GRAVITY;
}

XMFLOAT3 Object::GetAccerleration() const
{
	return m_xmfAcc;
}

void Object::SetAcceleration( XMFLOAT3 xmfAcc )
{
	m_xmfAcc = xmfAcc;
}

void Object::MoveObject( XMFLOAT3 distance )
{
	SetPosition( Vector3::Add( GetPosition(), distance ) );
}

void Object::MoveObject( float x, float y, float z )
{
	XMFLOAT3 distance{ x,y,z };
	MoveObject( distance );
}

void Object::MoveStrafe( float distance )
{
	XMFLOAT3 xmf3Position = GetPosition();
	XMFLOAT3 xmf3Right = GetRight();
	xmf3Position = Vector3::Add( xmf3Position, xmf3Right, distance );
	SetPosition( xmf3Position );
}

void Object::MoveUp( float distance )
{
	XMFLOAT3 xmf3Position = GetPosition();
	XMFLOAT3 xmf3Up = GetUp();
	xmf3Position = Vector3::Add( xmf3Position, xmf3Up, distance );
	SetPosition( xmf3Position );
}

void Object::MoveForward( float distance )
{
	XMFLOAT3 xmf3Position = GetPosition();
	XMFLOAT3 xmf3Look = GetLook();
	xmf3Position = Vector3::Add( xmf3Position, xmf3Look, distance );
	SetPosition( xmf3Position );
}

void Object::Rotate( float pitch, float yaw, float roll )
{
	/*XMMATRIX mtxRotate = XMMatrixRotationRollPitchYaw( XMConvertToRadians( pitch ),
		XMConvertToRadians( yaw ), XMConvertToRadians( roll ) );
	m_xmf4x4World = Matrix4x4::Multiply( mtxRotate, m_xmf4x4World );*/
	XMFLOAT4 quat = Vector4::XMVectorToFloat4( XMQuaternionRotationRollPitchYaw( XMConvertToRadians( pitch ),
		XMConvertToRadians( yaw ), XMConvertToRadians( roll ) ) );
	XMFLOAT4X4 rot{};

	QuatToMatrix( &quat, &rot );
	m_xmf4x4World = Matrix4x4::Multiply( rot, m_xmf4x4World );
}

void Object::SetMatrixByLook( float x, float y, float z )
{
	XMFLOAT3 look{ x,y,z };
	XMFLOAT3 up = GetUp();
	XMFLOAT3 right = Vector3::CrossProduct( up, look );	//순서가 up look 아니면 look up 둘 중 하나

	m_xmf4x4World._11 = right.x;
	m_xmf4x4World._12 = right.y;
	m_xmf4x4World._13 = right.z;

	m_xmf4x4World._31 = x;
	m_xmf4x4World._32 = y;
	m_xmf4x4World._33 = z;
}

bool Object::IsStopped()
{
	XMFLOAT3 curPos = GetPosition();
	if( abs( m_xmf3PrePosition.x - curPos.x ) < 0.1f && abs( m_xmf3PrePosition.z - curPos.z ) < 0.1f )
		return true;
	return false;
}
