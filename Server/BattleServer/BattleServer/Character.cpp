#include "pch.h"
#include "Character.h"
#include "Boundary.h"
#include "extern.h"

Character::Character(const char& role, const int& map_no, const int& sp )
{
	Initialize();
	//SpawnPosition으로 위치 세트, 초기 회전값 세트.
	XMFLOAT4 spawninfo = SHARED_RESOURCE::g_spawnPos[map_no].m_spawnPos[sp];
	SetPosition( spawninfo.x, spawninfo.y, spawninfo.z );
	Rotate( 0.f, spawninfo.w, 0.f );

	// 바운더리 정보 불러와서 캐릭터 위치로 이동, 회전해야함
	m_boundaries = new Boundary(*SHARED_RESOURCE::g_boundaries[OBJ_TYPE_CHARACTER]); // 이게 문제
	m_boundaries->BoundaryMove( spawninfo.x, spawninfo.y, spawninfo.z );
}

Character::Character( Object& curForm, const Boundary* new_bb_info )
{
	Initialize();

	m_xmf4x4World = Matrix4x4::Identity();
	m_xmf4x4World = Matrix4x4::Multiply( m_xmf4x4World, curForm.GetMatrix() ); // 이러면 위치와 보는 방향은 유지가 됨.

	m_xmfAcc = curForm.GetAccerleration();
	m_xmfVel = curForm.GetVelocity();

	// 새로운 Bounding 정보를 적용한다.
	int numofBB = new_bb_info->GetNumOfBB();
	int numofBS = new_bb_info->GetNumOfBS();

	//std::mutex lk;
	//lk.lock();
	delete m_boundaries;

	m_boundaries = new Boundary( numofBB, numofBS );
	m_boundaries->SetObjType( -1 );
	//lk.unlock();

	FXMVECTOR objRotQuat{ QuatFromMatrix(GetMatrix()) }; // 오브젝트의 회전 쿼터니언이다.
	XMVECTOR resultQuat{};
	for( int i = 0; i < numofBB; ++i )
	{
		m_boundaries->SetBBPos( new_bb_info->GetBBPos( i ), i ); // 로컬 위치로 바운딩 박스 이동
		m_boundaries->SetBBRot( new_bb_info->GetBBRot( i ), i ); // 로컬 회전 설정

		XMFLOAT3 bbRot{ m_boundaries->GetBBRot( i ) };
		FXMVECTOR bbRotationQuat{	// BB의 회전 쿼터니언이다.
			XMQuaternionRotationRollPitchYaw(
				XMConvertToRadians( bbRot.x ),
				XMConvertToRadians( bbRot.y ),
				XMConvertToRadians( bbRot.z ) ) };


		// 쿼터니언 곱셈을 한다.
		// 결과 회전 쿼터니언을 행렬로 변환한다.
		XMFLOAT4X4 resultQuatToMat{};
		resultQuat = XMQuaternionMultiply( bbRotationQuat, objRotQuat );
		QuatToMatrix( &Vector4::XMVectorToFloat4( resultQuat ), &resultQuatToMat );

		XMFLOAT3 bbPos{ m_boundaries->GetBBPos( i ) };
		resultQuatToMat._41 += bbPos.x;
		resultQuatToMat._42 += bbPos.y;
		resultQuatToMat._43 += bbPos.z;

		m_boundaries->SetBBSize( new_bb_info->GetBBSize( i ), i ); // 바운딩 박스 크기 설정
		m_boundaries->SetWorldMatrix( resultQuatToMat, i );
		m_boundaries->SetBBPos( Vector3::Add( m_boundaries->GetBBPos( i ), GetPosition() ), i );
	}
	for( int i = 0; i < numofBS; ++i )
	{
		XMFLOAT4X4 rot{};
		QuatToMatrix( &Vector4::XMVectorToFloat4( resultQuat ), &rot );

		XMFLOAT4X4 mat = Matrix4x4::Identity(); // BS 로컬 행렬

		mat._41 = new_bb_info->GetBSPos( i ).x;
		mat._42 = new_bb_info->GetBSPos( i ).y;
		mat._43 = new_bb_info->GetBSPos( i ).z;
		mat = Matrix4x4::Multiply( mat, rot ); // 회전 적용

		mat = Matrix4x4::Multiply( mat, m_xmf4x4World );
		m_boundaries->SetBSPos( mat._41, mat._42, mat._43, i );
		m_boundaries->SetBSRad( new_bb_info->GetBSRad( i ), i );
	}
	delete& curForm;
}

Character::~Character()
{
	delete m_boundaries;
	m_boundaries = nullptr;
}

void Character::Initialize()
{
	m_xmf4x4World = Matrix4x4::Identity();
	m_xmfAcc = {};
	m_xmfVel = {};
	m_boundaries->SetObjType( OBJ_TYPE_CHARACTER );
	// 스승, 학생 마법사 별로 다르게 적용되야할 것임.
	m_forceAmountXZ = 10.0f;	// 임시값.
	m_forceAmountY = 5.0f;
	m_fricCoef = 0.3f;		// 임시값.
	m_maxVel = 0.612f;			// 최대 이동 속도 6m/sec
	m_mass = 50.f;
}


