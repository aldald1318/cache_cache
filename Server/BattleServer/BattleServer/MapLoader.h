#pragma once
#include "Globals.h"
#include "DX_Math.h"
class Map;
class SpawnPosition;
class Boundary;
class MapLoader
{
public:
	MapLoader();
	~MapLoader();

public:
	void Initialize();
	void LoadBBs( std::string filepath);
	void LoadMap( Map& map, SpawnPosition& spawn, std::string filepath );
	void LoadMapBySector( Map& map, SpawnPosition& spawn, std::string filepath );

private:
	obj_info m_info;
	Boundary* m_Binfo;
	int m_obj_type;
	int m_num_BB;
	XMFLOAT3 m_size;
	XMFLOAT3 m_pos;
	XMFLOAT3 m_look;
	XMFLOAT3 m_up;
	XMFLOAT3 m_right;
	XMFLOAT3 m_rot;

	int m_num_BS;
	XMFLOAT3 m_bs_pos;
	float m_bs_rad;
};

