#include "pch.h"
#include "BattleServer.h"
#include "Room.h"
#include "Map.h"
#include "SpawnPosition.h"
#include "MapLoader.h"
#include "Boundary.h"
#include "Globals.h"


namespace SHARED_RESOURCE {
	HANDLE g_iocp;
	std::map<int, Room*> g_rooms;
	std::array<int, MAX_ROOM> g_manualMatchRoomNo;
	std::array<int, MAX_AUTO_ROOM> g_autoMatchRoomNo;
	std::array<SOCKETINFO*, MAX_CLIENTS> g_clients;
	std::priority_queue<EVENT> g_timer_queue;

	Map g_map[2];
	std::map<int, Boundary*> g_boundaries;
	SpawnPosition g_spawnPos[2];
}

namespace ATOMIC {
	std::mutex g_timer_lock;
	std::mutex g_msg_lock;
	std::mutex g_autoMatchRoomNo_lock;
	std::mutex g_manualMatchRoomNo_lock;
	std::mutex g_dbInfo_lock;
	std::mutex g_clients_lock;
	std::mutex g_room_lock;
	std::atomic_int g_numOfRoom;
	std::atomic_int g_NumOfManualRoom;
	std::atomic_int g_numOfAutoRoom;
}

void CreatePlayerSlot(int max_slot);

int main()
{
	MapLoader map_loader;
	map_loader.LoadBBs( "BB.txt" );

#ifdef SECTOR_ON
	map_loader.LoadMapBySector( SHARED_RESOURCE::g_map[0], SHARED_RESOURCE::g_spawnPos[0], "fullMap.txt" );
#else
	map_loader.LoadMap( SHARED_RESOURCE::g_map[0], SHARED_RESOURCE::g_spawnPos[0], "testMap.txt" );
#endif


	// 여기서 서버 객체를 생성
	CreatePlayerSlot( MAX_CLIENTS );
	BattleServer* battleServer = BattleServer::GetInstance();
	battleServer->AcceptMMServer();
	battleServer->Run();
}

void CreatePlayerSlot( int max_slot ){
	std::cout << " Initializing g_clients\n";
	for( int user_id = 0; user_id < max_slot; ++user_id ){
		SOCKETINFO* player = new SOCKETINFO;
		player->recv_over.wsabuf[0].len = MAX_BUFFER;
		player->recv_over.wsabuf[0].buf = player->recv_over.net_buf;
		//player->socket->GetSocket() = -1;
		player->user_id = user_id;
		player->room_id = -1;
		ZeroMemory( player->packet_buf, MAX_BUFFER );
		ZeroMemory( player->id_str, sizeof( char ) * 10 );
		SHARED_RESOURCE::g_clients[user_id] = player;
	}
	std::cout << "Slot Initialization Finished\n";
}