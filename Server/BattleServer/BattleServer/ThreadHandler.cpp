#include "pch.h"
#include "ThreadHandler.h"
#include "MyThread.h"
#include "WorkerThread.h"
#include "TimerThread.h"

ThreadHandler::ThreadHandler()
{
}

ThreadHandler::~ThreadHandler()
{
	JoinThreads();
}


void ThreadHandler::AddThread( MyThread* mythread )
{
	mythread->InitThread();
	threads.emplace_back( mythread );
}


void ThreadHandler::CreateThreads()
{
	// WorkerThread ����
	std::cout << "Creating " << MAX_WORKERTHREAD << " WorkerThreads.." << std::endl;
	for( int i = 0; i < MAX_WORKERTHREAD; ++i )
		AddThread( new WorkerThread );
	std::cout << "WorkerThreads Created." << std::endl;

	// TimerThread ����
	std::cout << "Creating TimerThread.." << std::endl;
	AddThread( new TimerThread );
	std::cout << "TimerThread Created." << std::endl;
}

void ThreadHandler::JoinThreads()
{
	for( auto& thread : threads )
		thread->JoinThread();
}
