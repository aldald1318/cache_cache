#include "pch.h"

bool SocketUtil::StaticInit()
{
	WSADATA wsaData;
	int result = WSAStartup( MAKEWORD( 2, 2 ), &wsaData );
	if( result != NO_ERROR ){
		ReportError( "Starting Up" );
		return false;
	}
	return true;
}

void SocketUtil::CleanUp()
{
	WSACleanup();
}

void SocketUtil::ReportError( const char* operationDesc )
{
	WCHAR* lpMsgBuf;
	DWORD errorNumber = WSAGetLastError();

	FormatMessage(
				FORMAT_MESSAGE_ALLOCATE_BUFFER |
				FORMAT_MESSAGE_FROM_SYSTEM |
				FORMAT_MESSAGE_IGNORE_INSERTS,
				NULL,
				errorNumber,
				MAKELANGID( LANG_NEUTRAL, SUBLANG_DEFAULT ),
				(LPTSTR)&lpMsgBuf,
				0, NULL );

	std::wcout << L"Error " << errorNumber << " - " << operationDesc << ", " << lpMsgBuf << std::endl;
	
}

UDPSocketPtr SocketUtil::CreateUDPSocket( SocketAddressFamily family )
{
	SOCKET s = WSASocket( family, SOCK_DGRAM, IPPROTO_UDP, NULL, 0, WSA_FLAG_OVERLAPPED );

	if( s != INVALID_SOCKET )
	{
		std::cout << "UDP Socket Created" << std::endl;
		return UDPSocketPtr( new UDPSocket( s ) );
	}
	else
	{
		ReportError( "SocketUtil::CreateUDPSocket" );
		return nullptr;
	}
}

TCPSocketPtr SocketUtil::CreateTCPSocket( SocketAddressFamily family )
{
	SOCKET s = WSASocket( family, SOCK_STREAM, IPPROTO_TCP, NULL, 0, WSA_FLAG_OVERLAPPED );

	if( s != INVALID_SOCKET )
	{
		std::cout << "TCP Socket Created" << std::endl;
		return TCPSocketPtr( new TCPSocket( s ) );
	}
	else
	{
		ReportError( "SocketUtil::CreateTCPSocket" );
		return nullptr;
	}
}
