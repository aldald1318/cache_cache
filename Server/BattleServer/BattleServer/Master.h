#pragma once
#include"Player.h"
class Student;
class Master :
	public Player
{
public:
	Master();
	Master( const Player& rhs ) noexcept;
	Master& operator=( const Player&& player ) noexcept;
	~Master() override;
	void Initialize();
	void Reset() override;
	bool Update( float elapsedTime ) override;

public:
	void SetIsShoot( bool is_shoot );
	int GetAnimType() override;

private:
	bool m_isShoot{ false };
	float m_fTimeAttack{};
};