#include "pch.h"
#include "Thunderbolt.h"
#include "Boundary.h"

//Thunderbolt::Thunderbolt()
//{
//	Initialize();
//}

Thunderbolt::~Thunderbolt()
{
	delete m_boundaries;
	m_boundaries = nullptr;
}

void Thunderbolt::Initialize()
{
	// 아래 물리값은 일단 전부 임시값이다.
	m_forceAmountXZ = 3.0f;
	m_mass = 1.0f;
	m_fricCoef = 0.0f;
	m_maxVel = 3.0f;
	m_isUsed = false;
	m_isCollidable = false;
	m_damage = 20;
	m_isApplyGravity = false;
	m_xmf4x4World = Matrix4x4::Identity();
	SetVelocity( XMFLOAT3{ 0.f,0.f,0.f } );
	SetAcceleration( XMFLOAT3{ 0.f,0.f,0.f } );

	XMFLOAT3 size{};
	int numBB{ 1 };
	m_boundaries->SetBB( numBB );
	m_boundaries->SetBBPos( GetPosition(), numBB - 1 );
	m_boundaries->SetBBSize(size , numBB - 1 );
	m_boundaries->SetBS( numBB );
	m_boundaries->SetBSPos( GetPosition(), numBB - 1 );
	m_boundaries->SetBSRad( 0.f, 0 );
	m_boundaries->SetObjType( OBJ_TYPE_THUNDERBOLT );

}

void Thunderbolt::Shoot( const XMFLOAT3& pos, const XMFLOAT3& look, XMFLOAT3& dir, double eTime )
{
	// 스승마법사의 위치에서 look 벡터 방향으로 특정 값만큼 이동한 위치에서부터 발사되어야함.
	// **미완성 함수**
	// 쏠 방향 (x, z) * 스칼라 만큼 떨어진 곳, y는 일단 120.f
	XMFLOAT3 shootPos{ pos };
	float distanceScala{ 30.f };
	shootPos.x += look.x * distanceScala;
	shootPos.z += look.z * distanceScala;
	shootPos.y += 120.f;
	m_xmf4x4World = Matrix4x4::Identity();
	SetPosition( shootPos ); // 이러면 캐릭터 피벗에서 나감.(땅바닥)
	SetLook( dir.x, dir.y, dir.z );
	SetUse();
#ifdef SECTOR_ON
	SetCollidable();
#endif

	//
	XMFLOAT3 force = Vector3::Normalize( dir );
	force = Vector3::Multiply( force, m_forceAmountXZ );
	AddForce( force, 10.f , true);

	//XMFLOAT3 force{};
	//
	//force = Vector3::Add( force, look );
	//float fSize = sqrtf( pow( force.x, 2 ) + pow( force.z, 2 ) + pow( force.y, 2 ) );
	//if( fSize > FLT_EPSILON ){
	//	force = Vector3::Division( force, fSize );
	//
	//	force.x = look.x * m_forceAmount;
	//	force.y = look.y * m_forceAmount;
	//	force.z = look.z * m_forceAmount;
	//
	//	AddForce( force, 10.f ); // eTime 사용하지 말자 일단.
	//}
}

unsigned short Thunderbolt::GetDamage() const
{
	return m_damage;
}

void Thunderbolt::SetDamage( const unsigned short& damage )
{
	m_damage = damage;
}

void Thunderbolt::SetUse()
{
	m_isUsed = true;
}

void Thunderbolt::SetNotUse()
{
	m_isUsed = false;
}

bool Thunderbolt::isUsed()
{
	return m_isUsed;
}

void Thunderbolt::SetCollidable()
{
	m_isCollidable = true;
}

void Thunderbolt::SetNotCollidable()
{
	m_isCollidable = false;
}

bool Thunderbolt::isCollidable()
{
	return m_isCollidable;
}

bool Thunderbolt::CheckBulletInsideMap()
{
	XMFLOAT3 bulletPos = GetPosition();

	if( bulletPos.y <= 0 ){
		std::cout << "Thunderbolts Y < 0" << std::endl;
		return false;
	}
	else if( bulletPos.y >= 800 ){
		std::cout << "Thunderbolts Y > 300" << std::endl;
		return false;
	}
	else if ( bulletPos.x <= 0 || bulletPos.x >= 5000 )
		return false;
	else if ( bulletPos.z <= 0 || bulletPos.z >= 5000 )
		return false;

	return true;
}
