#pragma once
#include "DX_Math.h"
constexpr int MAX_BUFFER = 256;
constexpr float GRAVITY = 0.00980f;	//980 cm/s^2가 되어야 한다 아닌가
constexpr int NO_MSG = -1;
constexpr int MAX_PLAYER = 4;
constexpr int MAX_ROOM = 50;
constexpr short AUTO_ROOM_START = 50;
constexpr short MAX_AUTO_ROOM = 1250 + 50;

constexpr int EVENT_KEY = 10000;
constexpr int MATCH_MAKING_SERVER_KEY = 0;
constexpr int UDP_SOCKET_KEY = 1;

constexpr float ABOVE_1M = 300.f;

constexpr int NUM_OF_SECTOR = 5;
constexpr int SIZE_OF_SECTOR = 5000.f / NUM_OF_SECTOR;
constexpr int SIZE_OF_SECTOR_HALF = SIZE_OF_SECTOR / 2;

constexpr int SPAWN_PATTERN[6][3]{ {1, 2, 3}, {1, 3, 2}, {2, 1, 3}, {2, 3, 1}, {3, 1, 2}, {3, 2, 1} };
constexpr int MAX_CLIENTS = 5100;

constexpr short OBJ_TYPE_CHARACTER = -1;
constexpr short OBJ_TYPE_THUNDERBOLT = -2;

constexpr size_t INITIAL_LEFT_TIME = 240 + 1; // 초기 남은 시간.

constexpr size_t PENALTY_TIME = 8;	// 변신 패널티를 주기까지 시간??
constexpr size_t PENALTY_DURATION = 15; // 패널티 유지시간
constexpr float PENALTY_RADIUS = 200.f; // 변신 패널티 검사 반경??

constexpr size_t HOLT_DURATION = 30; // 정지시킬 시간

constexpr int THUNDERBOLT_DAMAGE = 20;

enum class CHARACTERS{NONE, WI, DR, BA, FP, MP, SO, COUNT };

constexpr int WINNER_MMR_INCREASEMENT = 50;
constexpr int LOSER_MMR_DECREASEMENT = -30;

enum EVENT_TYPE {
	EV_TCP_RECV, EV_UDP_RECV, EV_SEND, 
	EV_UPDATE, EV_TICK, EV_FLUSH_MSG,
	EV_PENALTY_CHECK, EV_PENALTY_BEGIN, EV_PENALTY_END, 
	EV_MOVE_ENABLE, EV_MAKE_MOVE_DISABLE,
	EV_UPDATE_DB,
	EV_RESET_ROOM,
	EV_FULL_WORLD_UPDATE_END,
	EV_THUNDERBOLT_SET_NOT_USE,
	EV_TYPE_COUNT
};

struct EVENT {
	int id;
	int target; // 업데이트면 룸 번호
	std::chrono::high_resolution_clock::time_point wakeup_time;
	EVENT_TYPE event_type;
	constexpr bool operator<( const EVENT& rhs ) const {
		return wakeup_time > rhs.wakeup_time;
	}
};

struct SEND_INFO {
	int to;
	char buff[256];
};

struct OVER_EX {
	WSAOVERLAPPED		over;
	WSABUF				wsabuf[1];
	char				net_buf[MAX_BUFFER];
	EVENT_TYPE			ev_type;
};

struct SOCKETINFO {
	OVER_EX			recv_over{};
	TCPSocketPtr	socket{};
	SocketAddress	udp_addr{};
	unsigned int	user_id{};
	unsigned int	room_id{};

	bool			isConnected = false;
	
	unsigned char packet_buf[MAX_BUFFER]{};
	int prev_packet_data{};
	int curr_packet_size{};

	char id_str[10]{};
	int mmr{};
	int killCnt{};

	int totalGameCnt{};
	int winningGameCnt{};

	float WRate{};
};

struct message {
	int id;		// 어떤 플레이어?
	char type;	// 어떤 행동? 어떤 키?
	XMFLOAT3 vec;
};


typedef struct OBJECT_INFO {	// map.txt 읽을 때 쓰는 구조체
	XMFLOAT3 pos;
	XMFLOAT3 rot;
	bool isChangable;
	bool isFixed;
	bool isCollidable;
	int obj_type;
	int obj_id;
}obj_info;