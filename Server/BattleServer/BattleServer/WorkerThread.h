#pragma once
#include "MyThread.h"
#include "Globals.h"
class DB_Handler;
class WorkerThread final :
	public MyThread
{
public:
	void InitThread() override;
	void ProcThread() override;
	void JoinThread() override;
	message ProcPacket(int id, void* buf);

public:
	bool GetPlayerInfo( const char* id_str, int& mmr, int& catchCnt,
		float& Wrate, int& totalCnt, int& WCnt );
	void DisconnectClient(int clientID, SOCKET client);
	void EraseRoom( int& roomID );
private:
	SOCKADDR_IN m_ServerAddr{};
	SOCKET m_UDPRecvSocket{};
	int m_AddrLen{};
	//DB_Handler* m_DBHandler{};

	SOCKADDR_IN m_UDPClientAddr{};
};
