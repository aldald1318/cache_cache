#include "pch.h"
#include "Globals.h"
#include "../../Protocol.h"
#include "extern.h"
#include "Boundary.h"
#include "Room.h"
#include "Map.h"
#include "BattleServer.h"
#include "Physics.h"
#include "Student.h"
#include "Master.h"
#include "Prop.h"

Room::Room( int room_no )
{
	Initialize( room_no );
}

Room::Room( int room_no, wstring room_name )
{
	Initialize( room_no );
	m_roomName = room_name;
}

Room::~Room()
{
	if( m_physics != nullptr ){
		delete m_physics;
		m_physics = nullptr;
	}
	if( m_map != nullptr ){
		delete m_map;
		m_map = nullptr;
	}

	for( auto& player : m_players ){
		if( player != nullptr ){
			delete player;
			player = nullptr;
		}
	}
}

void Room::Initialize( int room_no )
{
	m_roomNo = room_no;
	m_mapNo = MAP_TOWN;
	m_curPlayerNum = 0;
	m_aliveMasterNum = 0;
	m_aliveStudentNum = 0;
	m_isGameStarted = false;
	m_isRoundStarted = false;
	m_isManualRoom = false;
	m_leftTime = INITIAL_LEFT_TIME;	//4분
	m_physics = new Physics();
	m_map = new Map();
	m_lastUpdate = std::chrono::system_clock::now();

	for( auto& player : m_players )
		player = new Student();

	int thunderboltUniqueID{ OBJECT_START_INDEX_THUNDERBOLT };
	for( int i = 0; i < MAX_THUNDERBOLT_COUNT; ++i ){
		m_thunderbolts[i].SetuniqueID( thunderboltUniqueID++ );
	}

	for( int i = 0; i < NUM_OF_SECTOR * 2 - 1; ++i )
	{
		for( int j = 0; j < NUM_OF_SECTOR * 2 - 1; ++j )
		{
			m_ActiveSector[i][j].active = true;
		}
	}

	// ev 에 30ms 후에 다시 호출하도록 이벤트 걸어두니 elapsedTime을 30ms(60 frame ==> 17ms, 30 frame ==> 34ms 로... ) 넣어주면 될 것 같지??
	EVENT ev{ EVENT_KEY, m_roomNo, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds( 30 )  , EV_UPDATE };
	BattleServer::GetInstance()->AddTimer( ev );
}

void Room::Reset()
{
	if( this == nullptr ) return;

	m_isEnterable = true;
	m_isRoundStarted = false;
	m_aliveMasterNum = 0;
	m_aliveStudentNum = 0;
	m_leftTime = INITIAL_LEFT_TIME;	//4분
	m_lastUpdate = std::chrono::system_clock::now();
	m_UpdateFullWorldSector = true;

	for( int i = 0; i < NUM_OF_SECTOR * 2 - 1; ++i )
	{
		for( int j = 0; j < NUM_OF_SECTOR * 2 - 1; ++j )
		{
			m_ActiveSector[i][j].active = true;
		}
	}

	delete m_map;
	m_map = new Map();

	for( int i = 0; i < MAX_THUNDERBOLT_COUNT; ++i )
	{
		m_thunderbolts[i].SetNotUse();
#ifdef SECTOR_ON
		m_thunderbolts[i].SetNotCollidable();
#endif
	}

	for( auto& charSlot : m_characterSlot )
		charSlot = false;

	for( auto& player : m_players ){
		//if( player->GetID() != -1 ){
		if( player->GetRole() == ROLE_MASTER ){
			Student* temp = new Student( *player );
			delete player;
			player = temp;
		}
		else
			static_cast<Student*>(player)->Reset();
		//}
	}
}

void Room::Update()
{
	//std::cout << "Room::Update() called " << std::endl;
	if( this == nullptr )
		return;

	m_curUpdate = std::chrono::system_clock::now();
	m_elapsedSec = m_curUpdate - m_lastUpdate;
	m_lastUpdate = m_curUpdate;

	float elapsedMilliSec{ (m_elapsedSec.count() * 1000.f) };
	//std::cout << elapsedMilliSec << std::endl;

	ProcUnUsedThunderbolt();
	CopyRecvMsgs();
	message procMsg;
	while( !m_copiedRecvMsgs.empty() )
	{
		procMsg = m_copiedRecvMsgs.front();
		m_copiedRecvMsgs.pop();

		ProcMsg( procMsg );
	}
	//ProcCollisionInfo ret;

#ifdef TEST
	if( !m_isStarted )
	{
		PushGameStartAvailableMsg( m_RoomManager, true );
		m_isSent = true;
	}

#endif

#ifdef OVER_2P
	if( !m_isGameStarted )
	{
		int numOfReady{};

		for( int i = 0; i < MAX_PLAYER; ++i ) {
			if( !m_players[i]->GetSlotStatus() ) {
				if( m_players[i]->GetReady() )
					++numOfReady;
			}
		}

		if( m_roomNo >= 50 )
		{
			if( numOfReady == m_curPlayerNum && m_curPlayerNum != 1 && m_curPlayerNum != 0 && m_curPlayerNum == 4 ) {
				// 게임 스타트 가능하다고 방장 클라이언트에 알려줘야함.
				if( !m_isSent ) {
					PushGameStartAvailableMsg( m_RoomManager, true );
					m_isSent = true;
				}
			}
			else {
				if( m_isSent ) {
					m_isSent = false;
					PushGameStartAvailableMsg( m_RoomManager, false );
				}
			}

		}
		else
		{
			if( numOfReady == m_curPlayerNum && m_curPlayerNum != 1 && m_curPlayerNum != 0 ) {
				// 게임 스타트 가능하다고 방장 클라이언트에 알려줘야함.
				if( !m_isSent ) {
					PushGameStartAvailableMsg( m_RoomManager, true );
					m_isSent = true;
				}
			}
			else {
				if( m_isSent ) {
					m_isSent = false;
					PushGameStartAvailableMsg( m_RoomManager, false );
				}
			}
		}
	}
#endif
	if( m_isGameStarted )
	{
		for( int i = 0; i < MAX_PLAYER; ++i )
		{
			int id{ m_players[i]->GetID() };
			if( id != -1 )
			{
				m_players[i]->Update( elapsedMilliSec );
				if( m_players[i]->GetCurForm()->m_boundaries->GetObjType() == OBJECT_TYPE_CHARACTER ){
					int animType{ m_players[i]->GetAnimType() };

					if( animType != m_players[i]->GetPrevAnimType() ){
						PushAnimMsg( id, animType );
						m_players[i]->SetPrevAnimType( animType );
					}
				}
			}
		}
#ifdef SECTOR_ON
		if( !m_UpdateFullWorldSector )
			UpdateActiveSector();
		SectorWorldUpdate();
		if( !m_UpdateFullWorldSector )
			SectorCollision();
		else
			TotalSectorCollision();
#else
		WorldUpdate();
		Collision();
#endif
#ifdef GAME_RULE_ON
		for( int i = 0; i < MAX_PLAYER; ++i ){
			// 안 움직였으면, 패널티 검사 시작.
			if( m_players[i]->GetID() != -1 )
			{
				if( !m_players[i]->IsDead() )
				{
					if( CheckPenaltyEVAddable( i ) )
					{
						EVENT ev_penalty_check{ m_players[i]->GetID(), m_roomNo, std::chrono::high_resolution_clock::now() + std::chrono::seconds( 1 ), EV_PENALTY_CHECK };
						BattleServer::GetInstance()->AddTimer( ev_penalty_check );
					}
				}
			}
			// 여기까지 페널티 체크를 할 지 검사하는 부분입니다. 
			//매 프레임마다 검사하니 비효율적 다른 방법이 있는지 생각 해봐야합니다.
		}
#endif // GAME_RULE_ON


		XMFLOAT3 subDistance;
		for( int i = 0; i < MAX_PLAYER; ++i )
		{
			if( m_players[i]->GetID() != -1 )
			{
				if( !m_players[i]->IsDead() )
				{
					auto iterA = m_players[i]->GetCurForm();

					XMFLOAT3 curpos = iterA->GetPosition();
					XMFLOAT3 prepos = iterA->GetPrePosition();

					if( m_players[i]->GetRole() == ROLE_STUDENT && iterA->m_boundaries->GetObjType() != OBJECT_TYPE_CHARACTER )
						static_cast<Student*>(m_players[i])->isInRad( curpos );

					subDistance = Vector3::Subtract( curpos, prepos );

					if( fabs( subDistance.x ) >= 0.1f || fabs( subDistance.y ) >= 3.f || fabs( subDistance.z ) >= 0.1f )//움직였으면
					{
						PTC_Vector ptc_pos;

						ptc_pos.x = curpos.x;
						ptc_pos.y = curpos.y;
						ptc_pos.z = curpos.z;

						iterA->SetPrePosition( curpos );

						for( int j = 0; j < MAX_PLAYER; ++j )
						{
							if( m_players[j]->GetID() != -1 )
							{
								PushPlayerPositionMsg( m_players[j]->GetID(), m_players[i]->GetID(), &ptc_pos );
							}
						}
					}
				}
			}
		}

#ifdef SECTOR_ON
		for( int i = 0; i < NUM_OF_SECTOR; ++i )
		{
			for( int j = 0; j < NUM_OF_SECTOR; ++j )
			{
				for( auto iterA = m_map->m_SectorObjList[i * 2][j * 2][Map::NOTFIXED].begin(); iterA != m_map->m_SectorObjList[i * 2][j * 2][Map::NOTFIXED].end(); )
				{
					XMFLOAT3 curpos = (*iterA)->GetPosition();
					XMFLOAT3 prepos = (*iterA)->GetPrePosition();

					subDistance = Vector3::Subtract( curpos, prepos );

					if( fabs( subDistance.x ) >= 0.1f || fabs( subDistance.y ) >= 1.f || fabs( subDistance.z ) >= 0.1f )//움직였으면
					{
						short type_id = (*iterA)->m_boundaries->GetObjType();
						int obj_id = (*iterA)->GetUniqueID();
						PTC_Vector ptc_pos;

						ptc_pos.x = curpos.x;
						ptc_pos.y = curpos.y;
						ptc_pos.z = curpos.z;

						for( int i = 0; i < MAX_PLAYER; ++i )
						{
							if( m_players[i]->GetID() != -1 )
							{
								PushObjectPositionMsg( m_players[i]->GetID(), type_id, obj_id, &ptc_pos );	//쿨라이언트 오브젝트 움직임 완료되면
							}
						}
						UpdateObjectSector( prepos, curpos, *iterA++, i * 2, j * 2 );
					}
					else
					{
						++iterA;
					}
				}
			}
		}

#else
		for( auto iterA = m_map->m_ObjList[Map::NOTFIXED].begin(); iterA != m_map->m_ObjList[Map::NOTFIXED].end(); ++iterA )
		{
			XMFLOAT3 curpos = (*iterA)->GetPosition();
			XMFLOAT3 prepos = (*iterA)->GetPrePosition();

			subDistance = Vector3::Subtract( curpos, prepos );

			if( fabs( subDistance.x ) >= 0.1f || fabs( subDistance.y ) >= 1.f || fabs( subDistance.z ) >= 0.1f )//움직였으면
			{
				short type_id = (*iterA)->m_boundaries->GetObjType();
				int obj_id = (*iterA)->GetUniqueID();
				PTC_Vector ptc_pos;

				ptc_pos.x = curpos.x;
				ptc_pos.y = curpos.y;
				ptc_pos.z = curpos.z;

				for( int i = 0; i < MAX_PLAYER; ++i )
				{
					if( m_players[i]->GetID() != -1 )
					{
						PushObjectPositionMsg( m_players[i]->GetID(), type_id, obj_id, &ptc_pos );	//쿨라이언트 오브젝트 움직임 완료되면
					}
				}
			}
		}

#endif
		// 썬더볼트 움직임 전송
		for( int i = 0; i < m_thunderbolts.size(); ++i ){
			if( m_thunderbolts[i].isUsed() )
			{
#ifdef SECTOR_ON
				if( m_thunderbolts[i].isCollidable() )
#endif
				{
					XMFLOAT3 pos = m_thunderbolts[i].GetPosition();
					//std::cout << "thunderbolt - " << i << " pos x - " << pos.x << " pos - y" << pos.y << " pos.z - " << pos.z << std::endl;
					PTC_Vector ptc_pos;
					ptc_pos.x = pos.x;
					ptc_pos.y = pos.y;
					ptc_pos.z = pos.z;

					for( int j = 0; j < MAX_PLAYER; ++j )
						if( m_players[j]->GetID() != -1 )
							PushObjectPositionMsg( m_players[j]->GetID(), m_thunderbolts[i].m_boundaries->GetObjType(),
								m_thunderbolts[i].GetUniqueID(), &ptc_pos );
				}
			}
		}
		++m_countFrame;
	}

	std::chrono::system_clock::time_point updateEnd{ std::chrono::system_clock::now() };
	std::chrono::duration<float> updateTime{ updateEnd - m_curUpdate };

	//std::cout << "updateTime = " << updateTime.count() << std::endl;

	int nextUpdate{ static_cast<int>((0.016f - updateTime.count()) * 1000.0f) };
	if( nextUpdate <= 0 )
		nextUpdate = 0;

	//std::cout << "nextUpadte TimePoint = now() + " << nextUpdate << std::endl;


	EVENT ev_update{ EVENT_KEY, m_roomNo, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds( nextUpdate ) , EV_UPDATE };
	BattleServer::GetInstance()->AddTimer( ev_update );

	EVENT ev_flush{ EVENT_KEY, m_roomNo, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds( 1 )  , EV_FLUSH_MSG };
	BattleServer::GetInstance()->AddTimer( ev_flush );
}

void Room::WorldUpdate() {
	//not fixes object update
	for( auto iterA = m_map->m_ObjList[Map::NOTFIXED].begin(); iterA != m_map->m_ObjList[Map::NOTFIXED].end(); ++iterA )
		(*iterA)->Update( m_elapsedSec.count() * 1000.f, false );

	//thunderbolt update
	for( int i = 0; i < m_thunderbolts.size(); ++i )
	{
		if( m_thunderbolts[i].isUsed() )
		{
			m_thunderbolts[i].BulletUpdate( m_elapsedSec.count() * 1000.f );

			if( !m_thunderbolts[i].CheckBulletInsideMap() )
			{
				m_thunderbolts[i].SetNotUse();
				PushThunderboltRemoveMsg( i );
			}
		}
	}
}

void Room::SectorWorldUpdate() {
	for( int i = 0; i < NUM_OF_SECTOR; ++i )
	{
		for( int j = 0; j < NUM_OF_SECTOR; ++j )
		{
			if( m_ActiveSector[i * 2][j * 2].active == true )
			{
				//not fixes object update
				for( auto iterA = m_map->m_SectorObjList[i * 2][j * 2][Map::NOTFIXED].begin(); iterA != m_map->m_SectorObjList[i * 2][j * 2][Map::NOTFIXED].end(); ++iterA )
					(*iterA)->Update( m_elapsedSec.count() * 1000.f, false );

				//thunderbolt update
				if( !m_ActiveSector[i * 2][j * 2].thunderboltIndex.empty() )
					for( auto iter = m_ActiveSector[i * 2][j * 2].thunderboltIndex.begin(); iter != m_ActiveSector[i * 2][j * 2].thunderboltIndex.end(); ++iter )
					{
						if( m_thunderbolts[*iter].isCollidable() )
						{
							m_thunderbolts[*iter].BulletUpdate( m_elapsedSec.count() * 1000.f );

							if( !m_thunderbolts[*iter].CheckBulletInsideMap() )
							{
								m_thunderbolts[*iter].SetNotUse();
								m_thunderbolts[*iter].SetNotCollidable();
								PushThunderboltRemoveMsg( *iter );
							}
						}
					}
			}
		}
	}
}

void Room::SectorCollision() {

	ProcCollisionInfo ret;

	for( int i = 0; i < NUM_OF_SECTOR * 2 - 1; ++i )
	{
		for( int j = 0; j < NUM_OF_SECTOR * 2 - 1; ++j )
		{
			if( m_ActiveSector[i][j].active == true )
			{
				//not fixed object
				for( auto iterA = m_map->m_SectorObjList[i][j][Map::NOTFIXED].begin(); iterA != m_map->m_SectorObjList[i][j][Map::NOTFIXED].end(); ++iterA )
				{
					for( auto iterB = iterA; iterB != m_map->m_SectorObjList[i][j][Map::NOTFIXED].end(); ++iterB )
					{
						if( iterA != iterB ) {
							ret = m_physics->CheckCollision( *iterA, *iterB );
							if( ret.is_collision )
							{
								m_physics->SplitObject( *iterA, *iterB, ret.collisionNormalVector, ret.overlapDepth, ret.is_sequential );

								m_physics->ProcCollisionImpulse( *iterA, *iterB, ret.collisionNormalVector, ret.relativeVelocity, ret.is_sequential );
								(*iterA)->InitYVelocity();
								(*iterB)->InitYVelocity();
							}
						}
					}
				}

				//fixed object
				for( auto iterA = m_map->m_SectorObjList[i][j][Map::FIXED].begin(); iterA != m_map->m_SectorObjList[i][j][Map::FIXED].end(); ++iterA )
				{
					for( auto iterB = m_map->m_SectorObjList[i][j][Map::NOTFIXED].begin(); iterB != m_map->m_SectorObjList[i][j][Map::NOTFIXED].end(); ++iterB )
					{
						ret = m_physics->CheckCollision( *iterA, *iterB );
						if( ret.is_collision )
						{
							m_physics->SplitObject( *iterB, ret.collisionNormalVector, ret.overlapDepth, ret.is_sequential );    //무조건 b(not fixed)가 이동

							m_physics->ProcCollisionImpulse( *iterB, ret.collisionNormalVector, ret.relativeVelocity, ret.is_sequential );
							(*iterB)->InitYVelocity();
						}

					}
				}

				//player collision
				for( auto iterA = m_map->m_SectorObjList[i][j][Map::NOTFIXED].begin(); iterA != m_map->m_SectorObjList[i][j][Map::NOTFIXED].end(); ++iterA )
				{
					if( !m_ActiveSector[i][j].playerIndex.empty() )
						for( auto iter = m_ActiveSector[i][j].playerIndex.begin(); iter != m_ActiveSector[i][j].playerIndex.end(); ++iter )
						{
							auto iterB = m_players[*iter]->GetCurForm();
							ret = m_physics->CheckCollision( *iterA, iterB );
							if( ret.is_collision )
							{
								//m_physics->SplitObject( *iterA, ret.collisionNormalVector, ret.overlapDepth, !ret.is_sequential );
								m_physics->SplitObject( *iterA, iterB, ret.collisionNormalVector, ret.overlapDepth, ret.is_sequential, true );

								m_physics->ProcCollisionImpulse( *iterA, iterB, ret.collisionNormalVector, ret.relativeVelocity, ret.is_sequential );
								(*iterA)->InitYVelocity();
								iterB->InitYVelocity();
							}
						}
				}

				for( auto iterA = m_map->m_SectorObjList[i][j][Map::FIXED].begin(); iterA != m_map->m_SectorObjList[i][j][Map::FIXED].end(); ++iterA )
				{
					if( !m_ActiveSector[i][j].playerIndex.empty() )
						for( auto iter = m_ActiveSector[i][j].playerIndex.begin(); iter != m_ActiveSector[i][j].playerIndex.end(); ++iter )
						{
							auto iterB = m_players[*iter]->GetCurForm();
							ret = m_physics->CheckCollision( *iterA, iterB );
							if( ret.is_collision )
							{
								m_physics->SplitObject( iterB, ret.collisionNormalVector, ret.overlapDepth, ret.is_sequential );    //무조건 b(not fixed)가 이동

								m_physics->ProcCollisionImpulse( iterB, ret.collisionNormalVector, ret.relativeVelocity, ret.is_sequential );
								iterB->InitYVelocity();
							}
						}
				}

				if( !m_ActiveSector[i][j].playerIndex.empty() )
				{
					for( auto a = m_ActiveSector[i][j].playerIndex.begin(); a != m_ActiveSector[i][j].playerIndex.end(); ++a )
					{
						auto iterA = m_players[*a]->GetCurForm();
						for( auto b = a + 1; b != m_ActiveSector[i][j].playerIndex.end(); ++b )
						{
							auto iterB = m_players[*b]->GetCurForm();
							ret = m_physics->CheckCollision( iterA, iterB );
							if( ret.is_collision )
							{
								if( m_players[*a]->GetRole() == ROLE_MASTER && !m_players[*a]->IsMovable() )
								{
									m_physics->SplitObject( iterB, ret.collisionNormalVector, ret.overlapDepth, ret.is_sequential );

									m_physics->ProcCollisionImpulse( iterB, ret.collisionNormalVector, ret.relativeVelocity, ret.is_sequential );

									iterB->InitYVelocity();
								}
								else if( m_players[*b]->GetRole() == ROLE_MASTER && !m_players[*b]->IsMovable() )
								{
									m_physics->SplitObject( iterA, ret.collisionNormalVector, ret.overlapDepth, ret.is_sequential );

									m_physics->ProcCollisionImpulse( iterA, ret.collisionNormalVector, ret.relativeVelocity, ret.is_sequential );

									iterA->InitYVelocity();
								}
								else
								{
									m_physics->SplitObject( iterA, iterB, ret.collisionNormalVector, ret.overlapDepth, ret.is_sequential );

									m_physics->ProcCollisionImpulse( iterA, iterB, ret.collisionNormalVector, ret.relativeVelocity, ret.is_sequential );

									iterA->InitYVelocity();
									iterB->InitYVelocity();
								}
							}

						}
					}
				}

				//not fixed object
				for( auto iterA = m_map->m_SectorObjList[i][j][Map::NOTFIXED].begin(); iterA != m_map->m_SectorObjList[i][j][Map::NOTFIXED].end(); ++iterA )
				{
					for( auto iterB = iterA; iterB != m_map->m_SectorObjList[i][j][Map::NOTFIXED].end(); ++iterB )
					{
						if( iterA != iterB ) {
							ret = m_physics->CheckCollision( *iterA, *iterB );
							if( ret.is_collision )
							{
								m_physics->SplitObject( *iterA, *iterB, ret.collisionNormalVector, ret.overlapDepth, ret.is_sequential );
				
								m_physics->ProcCollisionImpulse( *iterA, *iterB, ret.collisionNormalVector, ret.relativeVelocity, ret.is_sequential );
								(*iterA)->InitYVelocity();
								(*iterB)->InitYVelocity();
							}
						}
					}
				}
				
				//fixed object
				for( auto iterA = m_map->m_SectorObjList[i][j][Map::FIXED].begin(); iterA != m_map->m_SectorObjList[i][j][Map::FIXED].end(); ++iterA )
				{
					for( auto iterB = m_map->m_SectorObjList[i][j][Map::NOTFIXED].begin(); iterB != m_map->m_SectorObjList[i][j][Map::NOTFIXED].end(); ++iterB )
					{
						ret = m_physics->CheckCollision( *iterA, *iterB );
						if( ret.is_collision )
						{
							m_physics->SplitObject( *iterB, ret.collisionNormalVector, ret.overlapDepth, ret.is_sequential );    //무조건 b(not fixed)가 이동
				
							m_physics->ProcCollisionImpulse( *iterB, ret.collisionNormalVector, ret.relativeVelocity, ret.is_sequential );
							(*iterB)->InitYVelocity();
						}
				
					}
				}

				//thunderbolt
				for( auto iterA = m_map->m_SectorObjList[i][j][Map::FIXED].begin(); iterA != m_map->m_SectorObjList[i][j][Map::FIXED].end(); ++iterA )
				{
					if( !m_ActiveSector[i][j].thunderboltIndex.empty() )
					{
						for( auto iter = m_ActiveSector[i][j].thunderboltIndex.begin(); iter != m_ActiveSector[i][j].thunderboltIndex.end(); ++iter )
						{
							if( m_thunderbolts[*iter].isCollidable() )
								if( m_physics->CheckBulletObjCollisionLine( m_thunderbolts[*iter].GetPrePosition(), m_thunderbolts[*iter].GetPosition(), *iterA ) )
								{
									m_thunderbolts[*iter].SetNotUse();
									m_thunderbolts[*iter].SetNotCollidable();
									PushThunderboltRemoveMsg( *iter );
								}
						}
					}
				}

				for( auto iterA = m_map->m_SectorObjList[i][j][Map::NOTFIXED].begin(); iterA != m_map->m_SectorObjList[i][j][Map::NOTFIXED].end(); ++iterA )
				{
					if( !m_ActiveSector[i][j].thunderboltIndex.empty() )
					{
						for( auto iter = m_ActiveSector[i][j].thunderboltIndex.begin(); iter != m_ActiveSector[i][j].thunderboltIndex.end(); ++iter )
						{
							if( m_thunderbolts[*iter].isCollidable() )
								if( m_physics->CheckBulletObjCollisionLine( m_thunderbolts[*iter].GetPrePosition(), m_thunderbolts[*iter].GetPosition(), *iterA ) )
								{
									m_thunderbolts[*iter].SetNotCollidable();
									PushThunderboltRemoveMsg( *iter );
									m_physics->ProcBulletImpulse( *iterA, m_thunderbolts[*iter].GetPrePosition(), m_thunderbolts[*iter].GetPosition() );


									short buf[2]{ (short)m_roomNo,(short)(*iter) };
									//int tmpbuf = *( int* )buf;

									EVENT ev_thunderbolt{ EVENT_KEY, *(int*)buf, std::chrono::high_resolution_clock::now() + std::chrono::seconds( 2 )  , EV_THUNDERBOLT_SET_NOT_USE };
									BattleServer::GetInstance()->AddTimer( ev_thunderbolt );
								}
						}
					}
				}

				if( !m_ActiveSector[i][j].playerIndex.empty() && !m_ActiveSector[i][j].thunderboltIndex.empty() )
				{
					int getID{};
					for( auto iterP = m_ActiveSector[i][j].playerIndex.begin(); iterP != m_ActiveSector[i][j].playerIndex.end(); ++iterP )
					{
						getID = m_players[*iterP]->GetID();
						if( m_players[*iterP]->GetRole() == ROLE_STUDENT )
						{
							auto iterA = m_players[*iterP]->GetCurForm();

							for( auto iterT = m_ActiveSector[i][j].thunderboltIndex.begin(); iterT != m_ActiveSector[i][j].thunderboltIndex.end(); ++iterT )
							{
								if( m_thunderbolts[*iterT].isCollidable() )
									if( m_physics->CheckBulletObjCollisionLine( m_thunderbolts[*iterT].GetPrePosition(), m_thunderbolts[*iterT].GetPosition(), iterA ) )
									{
										m_thunderbolts[*iterT].SetNotUse();
										m_thunderbolts[*iterT].SetNotCollidable();
										PushThunderboltRemoveMsg( *iterT );

										auto iterStudent = reinterpret_cast<Student*>(m_players[*iterP]);
										unsigned short hp = iterStudent->GetHP();

										if( hp <= THUNDERBOLT_DAMAGE )//플레이어 죽음 처리
										{
											iterStudent->SetHP( unsigned short( 0 ) );
											PushDieMsg( getID );
											m_aliveStudentNum--;
											m_players[*iterP]->SetDead();

											for( auto player : m_players ){
												if( player->GetRole() == ROLE_MASTER )	player->IncreaseKillCnt();
											}
#ifdef LOG_ON
											std::cout << "[LOG] Player - ID" << getID << "Dead!" << std::endl;
											std::cout << "[LOG] Left Student Count - " << m_aliveStudentNum << std::endl;
#endif
										}
										else
										{
											iterStudent->SetHP( unsigned short( hp - THUNDERBOLT_DAMAGE ) );
											PushHitMsg( getID, float( hp - THUNDERBOLT_DAMAGE ) / float( iterStudent->GetMaxHP() ) );
											m_physics->ProcBulletImpulse( iterA, m_thunderbolts[*iterT].GetPrePosition(), m_thunderbolts[*iterT].GetPosition() );
										}
									}
							}
						}
					}
				}
				//end
			}
		}
	}
}

void Room::TotalSectorCollision() {	//총알쏠 일 없다. 게임 시작 3초만 돌음

	ProcCollisionInfo ret;

	for( int i = 0; i < NUM_OF_SECTOR * 2 - 1; i += 2 )
	{
		for( int j = 0; j < NUM_OF_SECTOR * 2 - 1; j += 2 )
		{
			if( m_ActiveSector[i][j].active == true )
			{
				//not fixed object
				for( auto iterA = m_map->m_SectorObjList[i][j][Map::NOTFIXED].begin(); iterA != m_map->m_SectorObjList[i][j][Map::NOTFIXED].end(); ++iterA )
				{
					for( auto iterB = iterA; iterB != m_map->m_SectorObjList[i][j][Map::NOTFIXED].end(); ++iterB )
					{
						if( iterA != iterB ) {
							ret = m_physics->CheckCollision( *iterA, *iterB );
							if( ret.is_collision )
							{
								m_physics->SplitObject( *iterA, *iterB, ret.collisionNormalVector, ret.overlapDepth, ret.is_sequential );

								m_physics->ProcCollisionImpulse( *iterA, *iterB, ret.collisionNormalVector, ret.relativeVelocity, ret.is_sequential );
								(*iterA)->InitYVelocity();
								(*iterB)->InitYVelocity();
							}
						}
					}
				}

				//fixed object
				for( auto iterA = m_map->m_SectorObjList[i][j][Map::FIXED].begin(); iterA != m_map->m_SectorObjList[i][j][Map::FIXED].end(); ++iterA )
				{
					for( auto iterB = m_map->m_SectorObjList[i][j][Map::NOTFIXED].begin(); iterB != m_map->m_SectorObjList[i][j][Map::NOTFIXED].end(); ++iterB )
					{
						ret = m_physics->CheckCollision( *iterA, *iterB );
						if( ret.is_collision )
						{
							m_physics->SplitObject( *iterB, ret.collisionNormalVector, ret.overlapDepth, ret.is_sequential );    //무조건 b(not fixed)가 이동

							m_physics->ProcCollisionImpulse( *iterB, ret.collisionNormalVector, ret.relativeVelocity, ret.is_sequential );
							(*iterB)->InitYVelocity();
						}

					}
				}

				//player collision
				for( auto iterA = m_map->m_SectorObjList[i][j][Map::NOTFIXED].begin(); iterA != m_map->m_SectorObjList[i][j][Map::NOTFIXED].end(); ++iterA )
				{
					if( !m_ActiveSector[i][j].playerIndex.empty() )
						for( auto iter = m_ActiveSector[i][j].playerIndex.begin(); iter != m_ActiveSector[i][j].playerIndex.end(); ++iter )
						{
							auto iterB = m_players[*iter]->GetCurForm();
							ret = m_physics->CheckCollision( *iterA, iterB );
							if( ret.is_collision )
							{
								//m_physics->SplitObject( *iterA, ret.collisionNormalVector, ret.overlapDepth, !ret.is_sequential );
								m_physics->SplitObject( *iterA, iterB, ret.collisionNormalVector, ret.overlapDepth, ret.is_sequential, true );

								m_physics->ProcCollisionImpulse( *iterA, iterB, ret.collisionNormalVector, ret.relativeVelocity, ret.is_sequential );
								(*iterA)->InitYVelocity();
								iterB->InitYVelocity();
							}
						}
				}

				for( auto iterA = m_map->m_SectorObjList[i][j][Map::FIXED].begin(); iterA != m_map->m_SectorObjList[i][j][Map::FIXED].end(); ++iterA )
				{
					if( !m_ActiveSector[i][j].playerIndex.empty() )
						for( auto iter = m_ActiveSector[i][j].playerIndex.begin(); iter != m_ActiveSector[i][j].playerIndex.end(); ++iter )
						{
							auto iterB = m_players[*iter]->GetCurForm();
							ret = m_physics->CheckCollision( *iterA, iterB );
							if( ret.is_collision )
							{
								m_physics->SplitObject( iterB, ret.collisionNormalVector, ret.overlapDepth, ret.is_sequential );    //무조건 b(not fixed)가 이동

								m_physics->ProcCollisionImpulse( iterB, ret.collisionNormalVector, ret.relativeVelocity, ret.is_sequential );
								iterB->InitYVelocity();
							}
						}
				}

				if( !m_ActiveSector[i][j].playerIndex.empty() )
				{
					for( auto a = m_ActiveSector[i][j].playerIndex.begin(); a != m_ActiveSector[i][j].playerIndex.end(); ++a )
					{
						auto iterA = m_players[*a]->GetCurForm();
						for( auto b = a + 1; b != m_ActiveSector[i][j].playerIndex.end(); ++b )
						{
							auto iterB = m_players[*b]->GetCurForm();
							ret = m_physics->CheckCollision( iterA, iterB );
							if( ret.is_collision )
							{
								if( m_players[*a]->GetRole() == ROLE_MASTER && !m_players[*a]->IsMovable() )
								{
									m_physics->SplitObject( iterB, ret.collisionNormalVector, ret.overlapDepth, ret.is_sequential );

									m_physics->ProcCollisionImpulse( iterB, ret.collisionNormalVector, ret.relativeVelocity, ret.is_sequential );

									iterB->InitYVelocity();
								}
								else if( m_players[*b]->GetRole() == ROLE_MASTER && !m_players[*b]->IsMovable() )
								{
									m_physics->SplitObject( iterA, ret.collisionNormalVector, ret.overlapDepth, ret.is_sequential );

									m_physics->ProcCollisionImpulse( iterA, ret.collisionNormalVector, ret.relativeVelocity, ret.is_sequential );

									iterA->InitYVelocity();
								}
								else
								{
									m_physics->SplitObject( iterA, iterB, ret.collisionNormalVector, ret.overlapDepth, ret.is_sequential );

									m_physics->ProcCollisionImpulse( iterA, iterB, ret.collisionNormalVector, ret.relativeVelocity, ret.is_sequential );

									iterA->InitYVelocity();
									iterB->InitYVelocity();
								}
							}

						}
					}
				}

				//not fixed object
				for( auto iterA = m_map->m_SectorObjList[i][j][Map::NOTFIXED].begin(); iterA != m_map->m_SectorObjList[i][j][Map::NOTFIXED].end(); ++iterA )
				{
					for( auto iterB = iterA; iterB != m_map->m_SectorObjList[i][j][Map::NOTFIXED].end(); ++iterB )
					{
						if( iterA != iterB ) {
							ret = m_physics->CheckCollision( *iterA, *iterB );
							if( ret.is_collision )
							{
								m_physics->SplitObject( *iterA, *iterB, ret.collisionNormalVector, ret.overlapDepth, ret.is_sequential );

								m_physics->ProcCollisionImpulse( *iterA, *iterB, ret.collisionNormalVector, ret.relativeVelocity, ret.is_sequential );
								(*iterA)->InitYVelocity();
								(*iterB)->InitYVelocity();
							}
						}
					}
				}

				//fixed object
				for( auto iterA = m_map->m_SectorObjList[i][j][Map::FIXED].begin(); iterA != m_map->m_SectorObjList[i][j][Map::FIXED].end(); ++iterA )
				{
					for( auto iterB = m_map->m_SectorObjList[i][j][Map::NOTFIXED].begin(); iterB != m_map->m_SectorObjList[i][j][Map::NOTFIXED].end(); ++iterB )
					{
						ret = m_physics->CheckCollision( *iterA, *iterB );
						if( ret.is_collision )
						{
							m_physics->SplitObject( *iterB, ret.collisionNormalVector, ret.overlapDepth, ret.is_sequential );    //무조건 b(not fixed)가 이동

							m_physics->ProcCollisionImpulse( *iterB, ret.collisionNormalVector, ret.relativeVelocity, ret.is_sequential );
							(*iterB)->InitYVelocity();
						}

					}
				}
				//end
			}
		}
	}
}

void Room::Collision() {

	ProcCollisionInfo ret;
	//not fixes object
	for( auto iterA = m_map->m_ObjList[Map::NOTFIXED].begin(); iterA != m_map->m_ObjList[Map::NOTFIXED].end(); ++iterA )
	{
		for( auto iterB = iterA + 1; iterB != m_map->m_ObjList[Map::NOTFIXED].end(); ++iterB )
		{
			ret = m_physics->CheckCollision( *iterA, *iterB );
			if( ret.is_collision )
			{
				m_physics->SplitObject( *iterA, *iterB, ret.collisionNormalVector, ret.overlapDepth, ret.is_sequential );

				m_physics->ProcCollisionImpulse( *iterA, *iterB, ret.collisionNormalVector, ret.relativeVelocity, ret.is_sequential );
				(*iterA)->InitYVelocity();
				(*iterB)->InitYVelocity();
			}
		}
	}
	//fixed object
	for( auto iterA = m_map->m_ObjList[Map::FIXED].begin(); iterA != m_map->m_ObjList[Map::FIXED].end(); ++iterA )
	{
		for( auto iterB = m_map->m_ObjList[Map::NOTFIXED].begin(); iterB != m_map->m_ObjList[Map::NOTFIXED].end(); ++iterB )
		{
			ret = m_physics->CheckCollision( *iterA, *iterB );
			if( ret.is_collision )
			{
				m_physics->SplitObject( *iterB, ret.collisionNormalVector, ret.overlapDepth, ret.is_sequential );    //무조건 b(not fixed)가 이동

				m_physics->ProcCollisionImpulse( *iterB, ret.collisionNormalVector, ret.relativeVelocity, ret.is_sequential );
				(*iterB)->InitYVelocity();
			}

		}
	}

	//player collision
	for( auto iterA = m_map->m_ObjList[Map::NOTFIXED].begin(); iterA != m_map->m_ObjList[Map::NOTFIXED].end(); ++iterA )
	{
		for( int i = 0; i < MAX_PLAYER; ++i )
		{
			if( m_players[i]->GetID() != -1 )
			{
				if( !m_players[i]->IsDead() )
				{
					auto iterB = m_players[i]->GetCurForm();
					ret = m_physics->CheckCollision( *iterA, iterB );
					if( ret.is_collision )
					{
						//m_physics->SplitObject( *iterA, ret.collisionNormalVector, ret.overlapDepth, !ret.is_sequential );
						m_physics->SplitObject( *iterA, iterB, ret.collisionNormalVector, ret.overlapDepth, ret.is_sequential, true );

						m_physics->ProcCollisionImpulse( *iterA, iterB, ret.collisionNormalVector, ret.relativeVelocity, ret.is_sequential );
						(*iterA)->InitYVelocity();
						iterB->InitYVelocity();
					}
				}
			}
		}
	}
	for( auto iterA = m_map->m_ObjList[Map::FIXED].begin(); iterA != m_map->m_ObjList[Map::FIXED].end(); ++iterA )
	{
		for( int i = 0; i < MAX_PLAYER; ++i )
		{
			if( m_players[i]->GetID() != -1 )
			{
				if( !m_players[i]->IsDead() )
				{
					auto iterB = m_players[i]->GetCurForm();
					ret = m_physics->CheckCollision( *iterA, iterB );
					if( ret.is_collision )
					{
						m_physics->SplitObject( iterB, ret.collisionNormalVector, ret.overlapDepth, ret.is_sequential );    //무조건 b(not fixed)가 이동

						m_physics->ProcCollisionImpulse( iterB, ret.collisionNormalVector, ret.relativeVelocity, ret.is_sequential );
						iterB->InitYVelocity();
					}
				}
			}
		}
	}

	for( int i = 0; i < MAX_PLAYER; ++i )
	{
		if( m_players[i]->GetID() != -1 )
		{
			if( !m_players[i]->IsDead() )
			{
				auto iterA = m_players[i]->GetCurForm();
				for( int j = i + 1; j < MAX_PLAYER; ++j )
				{
					if( m_players[j]->GetID() != -1 )
					{
						if( !m_players[j]->IsDead() )
						{
							auto iterB = m_players[j]->GetCurForm();
							ret = m_physics->CheckCollision( iterA, iterB );
							if( ret.is_collision )
							{

								if( m_players[i]->GetRole() == ROLE_MASTER && !m_players[i]->IsMovable() )
								{
									m_physics->SplitObject( iterB, ret.collisionNormalVector, ret.overlapDepth, ret.is_sequential );

									m_physics->ProcCollisionImpulse( iterB, ret.collisionNormalVector, ret.relativeVelocity, ret.is_sequential );

									iterB->InitYVelocity();
								}
								else if( m_players[j]->GetRole() == ROLE_MASTER && !m_players[j]->IsMovable() )
								{
									m_physics->SplitObject( iterA, ret.collisionNormalVector, ret.overlapDepth, ret.is_sequential );

									m_physics->ProcCollisionImpulse( iterA, ret.collisionNormalVector, ret.relativeVelocity, ret.is_sequential );

									iterA->InitYVelocity();
								}
								else
								{
									m_physics->SplitObject( iterA, iterB, ret.collisionNormalVector, ret.overlapDepth, ret.is_sequential );

									m_physics->ProcCollisionImpulse( iterA, iterB, ret.collisionNormalVector, ret.relativeVelocity, ret.is_sequential );

									iterA->InitYVelocity();
									iterB->InitYVelocity();
								}
							}
						}
					}
				}
			}
		}
	}

	//not fixes object
	for( auto iterA = m_map->m_ObjList[Map::NOTFIXED].begin(); iterA != m_map->m_ObjList[Map::NOTFIXED].end(); ++iterA )
	{
		for( auto iterB = iterA + 1; iterB != m_map->m_ObjList[Map::NOTFIXED].end(); ++iterB )
		{
			ret = m_physics->CheckCollision( *iterA, *iterB );
			if( ret.is_collision )
			{
				m_physics->SplitObject( *iterA, *iterB, ret.collisionNormalVector, ret.overlapDepth, ret.is_sequential );
				m_physics->ProcCollisionImpulse( *iterA, *iterB, ret.collisionNormalVector, ret.relativeVelocity, ret.is_sequential );
				(*iterA)->InitYVelocity();
				(*iterB)->InitYVelocity();

			}
		}
	}


	//fixed object
	for( auto iterA = m_map->m_ObjList[Map::FIXED].begin(); iterA != m_map->m_ObjList[Map::FIXED].end(); ++iterA )
	{
		for( auto iterB = m_map->m_ObjList[Map::NOTFIXED].begin(); iterB != m_map->m_ObjList[Map::NOTFIXED].end(); ++iterB )
		{
			ret = m_physics->CheckCollision( *iterA, *iterB );
			if( ret.is_collision )
			{
				m_physics->SplitObject( *iterB, ret.collisionNormalVector, ret.overlapDepth, ret.is_sequential );    //무조건 b(not fixed)가 이동

				m_physics->ProcCollisionImpulse( *iterB, ret.collisionNormalVector, ret.relativeVelocity, ret.is_sequential );
				(*iterB)->InitYVelocity();
			}

		}
	}

	//thunderbolt
	for( auto iterA = m_map->m_ObjList[Map::FIXED].begin(); iterA != m_map->m_ObjList[Map::FIXED].end(); ++iterA )
	{
		for( int j = 0; j < MAX_THUNDERBOLT_COUNT; ++j ) {
			if( m_thunderbolts[j].isUsed() )
			{
				if( m_physics->CheckBulletObjCollisionLine( m_thunderbolts[j].GetPrePosition(), m_thunderbolts[j].GetPosition(), *iterA ) )
				{
					m_thunderbolts[j].SetNotUse();
					PushThunderboltRemoveMsg( j );
				}
			}
		}
	}
	for( auto iterA = m_map->m_ObjList[Map::NOTFIXED].begin(); iterA != m_map->m_ObjList[Map::NOTFIXED].end(); ++iterA )
	{
		for( int j = 0; j < MAX_THUNDERBOLT_COUNT; ++j ) {
			if( m_thunderbolts[j].isUsed() )
			{
				if( m_physics->CheckBulletObjCollisionLine( m_thunderbolts[j].GetPrePosition(), m_thunderbolts[j].GetPosition(), *iterA ) )
				{
					m_thunderbolts[j].SetNotUse();
					PushThunderboltRemoveMsg( j );
					m_physics->ProcBulletImpulse( *iterA, m_thunderbolts[j].GetPrePosition(), m_thunderbolts[j].GetPosition() );
				}
			}
		}
	}

	int getID{};
	for( int i = 0; i < MAX_PLAYER; ++i )
	{
		getID = m_players[i]->GetID();
		if( getID != -1 )
		{
			if( !m_players[i]->IsDead() )
			{
				if( m_players[i]->GetRole() == ROLE_STUDENT )
				{
					auto iterA = m_players[i]->GetCurForm();

					for( int j = 0; j < MAX_THUNDERBOLT_COUNT; ++j ) {
						if( m_thunderbolts[j].isUsed() )
						{
							if( m_physics->CheckBulletObjCollisionLine( m_thunderbolts[j].GetPrePosition(), m_thunderbolts[j].GetPosition(), iterA ) )
							{
								m_thunderbolts[j].SetNotUse();
								PushThunderboltRemoveMsg( j );

								auto iterStudent = reinterpret_cast<Student*>(m_players[i]);
								unsigned short hp = iterStudent->GetHP();

								if( hp <= THUNDERBOLT_DAMAGE )//플레이어 죽음 처리
								{
									iterStudent->SetHP( unsigned short( 0 ) );
									PushDieMsg( getID );
									m_aliveStudentNum--;
									m_players[i]->SetDead();
									static_cast<Student*>(m_players[i])->SetIsTransformable( false );
									for( auto player : m_players ){
										if( player->GetRole() == ROLE_MASTER )	player->IncreaseKillCnt();
									}
#ifdef LOG_ON
									std::cout << "[LOG] Player - ID" << getID << "Dead!" << std::endl;
									std::cout << "[LOG] Left Student Count - " << m_aliveStudentNum << std::endl;
#endif
								}
								else
								{
									iterStudent->SetHP( unsigned short( hp - THUNDERBOLT_DAMAGE ) );
									PushHitMsg( getID, float( hp - THUNDERBOLT_DAMAGE ) / float( iterStudent->GetMaxHP() ) );
									m_physics->ProcBulletImpulse( iterA, m_thunderbolts[j].GetPrePosition(), m_thunderbolts[j].GetPosition() );
								}
							}
						}
					}
				}
			}
		}
	}
}

void Room::PushMsg( message& msg )
{
	if( msg.id <= 1 ) return; // ID가 2부터 클라이언트.

	ATOMIC::g_msg_lock.lock();
	m_recvMsgQueue.push( msg );
	ATOMIC::g_msg_lock.unlock();
}

bool Room::IsEmpty()
{
	if( m_curPlayerNum <= 0 ) {
		return true;
	}
	return false;
}

bool Room::EnterRoom( int id, bool is_roomManager, int* _slot )
{
	if( m_curPlayerNum >= MAX_PLAYER )
		return false;
	for( int slot = 0; slot < MAX_PLAYER; ++slot )
	{
		if( m_players[slot]->GetSlotStatus() ) {
			m_curPlayerNum++;
			m_players[slot]->Enter();	// isEmpty = false 로 셋.
			m_players[slot]->SetID( id );
			m_players[slot]->SetID_STR( SHARED_RESOURCE::g_clients[id]->id_str );
			m_players[slot]->SetMMR( SHARED_RESOURCE::g_clients[id]->mmr );
			m_players[slot]->SetKillCnt( SHARED_RESOURCE::g_clients[id]->killCnt );
			m_players[slot]->SetWinRate( SHARED_RESOURCE::g_clients[id]->WRate );
			m_players[slot]->SetTotalCnt( SHARED_RESOURCE::g_clients[id]->totalGameCnt );
			m_players[slot]->SetWinCnt( SHARED_RESOURCE::g_clients[id]->winningGameCnt );

			ATOMIC::g_clients_lock.lock();
			SHARED_RESOURCE::g_clients[id]->room_id = m_roomNo;
			ATOMIC::g_clients_lock.unlock();

			if( is_roomManager )
				m_RoomManager = id;

			// 플레이어 입장 알리기.
			*_slot = slot;
			AnounceRoomEnter( id, slot );
			return true;
		}
	}
	return false;
}
void Room::AnounceRoomEnter( int id, int slot )
{
	bool isManager{};
	int enterer{ -1 };

	for( int i = 0; i < MAX_PLAYER; ++i ){
		if( id == m_players[i]->GetID() )
			enterer = i;
	}
	char enterer_cType[3];
	char enterer_id_str[10];
	bool enterer_isManager{ false };
	memcpy( enterer_id_str, m_players[enterer]->GetID_STR(), sizeof( char ) * 10 );
	memcpy( enterer_cType, &CharTypeIntToStr( m_players[enterer]->GetCharacterType() ), sizeof( char ) * 3 );
	if( m_RoomManager == m_players[enterer]->GetID() ) enterer_isManager = true;

	for( int i = 0; i < MAX_PLAYER; ++i ) {
		if( !m_players[i]->GetSlotStatus() ) {
			int tmpID = m_players[i]->GetID();
			bool tmpReady = m_players[i]->GetReady();
			int tmpMmr = m_players[i]->GetMMR();
			int tmpCnt = m_players[i]->GetKillCnt();
			float tmpWRate = m_players[i]->GetWinRate();
			int tmpTotalCnt = m_players[i]->GetTotalCnt();
			int tmpWinCnt = m_players[i]->GetWinCnt();

			char id_str[10]{};
			memcpy( id_str, m_players[i]->GetID_STR(), sizeof( char ) * 10 );

			if( m_RoomManager == m_players[i]->GetID() )
				isManager = TRUE;
			else
				isManager = FALSE;

			// 아래 블록 로직이 맞는지 검증해야한다.
			if( m_players[i]->GetID() != id ) {
				char c_type[3];
				memcpy( c_type, &CharTypeIntToStr( m_players[i]->GetCharacterType() ), sizeof( char ) * 3 );
				// 새로 들어온 유저에게 기존 유저 알려주기
				BattleServer::GetInstance()->SendRoomEnterPacket( id, tmpID, tmpReady, i,
					c_type, id_str, tmpMmr, tmpCnt, tmpWRate, tmpTotalCnt, tmpWinCnt, isManager );
				// 기존 유저에게 새로 들어온 유저 알려주기
				BattleServer::GetInstance()->SendRoomEnterPacket( tmpID, id, FALSE, slot,
					enterer_cType, enterer_id_str, tmpMmr, tmpCnt, tmpWRate, tmpTotalCnt, tmpWinCnt, enterer_isManager );
			}
		}
	}
}

void Room::LeaveRoom( int id )
{
	// is_roomManager == true 이면, 방장도 바꿔야 한다.
	ATOMIC::g_clients_lock.lock();
	SHARED_RESOURCE::g_clients[id]->room_id = -1;
	ATOMIC::g_clients_lock.unlock();

	int leaverRole{};
	int leaverIndex{ -1 };
	int getID{};

	// 퇴장하는 클라이언트의 룸에서의 역할과 인덱스 알아놓기.
	for( int i = 0; i < MAX_PLAYER; ++i ){
		getID = m_players[i]->GetID();
		if( getID == id ){
			leaverRole = m_players[i]->GetRole();
			leaverIndex = i;
		}
	}

	// 사용하던 데이터 정리하기
	int c_type{};	// 퇴장하는 클라이언트의 캐릭터 타입 번호
	char c_type_str[3]{};	// 캐릭터 타입 문자열(클라이언트 전송용)

	// 캐릭터 슬롯
	c_type = m_players[leaverIndex]->GetCharacterType();
	m_characterSlot[c_type] = false;
	memcpy( &c_type_str, &CharTypeIntToStr( c_type ), sizeof( char ) * 3 );

	// 나간 id 가 방장이면 새 방장 임명하기.
	if( id == m_RoomManager ) {
		for( int i = 0; i < MAX_PLAYER; ++i ) {
			getID = m_players[i]->GetID();
			if( getID != -1 && getID != id ) {
				m_RoomManager = m_players[i]->GetID();
				PushNewRoomManagerMsg( m_RoomManager );
				m_isSent = false;
				break;
			}
		}
	}

	// 게임 종료 검사에 사용하는 스승 수, 학생 수 감소
	--m_curPlayerNum;
	if( m_isGameStarted ){
		if( leaverRole == ROLE_MASTER )
			m_aliveMasterNum--;
		else{
			if( !m_players[leaverIndex]->IsDead() ){
				m_aliveStudentNum--;
				m_players[leaverIndex]->SetDead();
				PushDieMsg( id );
			}
		}
	}

	// 나간 슬롯의 플레이어 객체 초기화
	m_players[leaverIndex]->Initialize();

	// 캐릭터 선택 취소, 방 퇴장 패킷 전송
	for( const auto& player : m_players )
	{
		getID = player->GetID();
		if( getID != -1 && getID != id ){
			BattleServer::GetInstance()->SendCancleSelectCharacterPakcet( getID, id, c_type_str );
			BattleServer::GetInstance()->SendRoomLeavePacket( getID, id );
		}
	}

}


// 이 매서드 사용안하고 있음. new Map 하는 것 잘못되었음.
void Room::LoadMap( int mapNo )
{
	m_map = new Map();
	m_map->LoadMap( mapNo );
}

void Room::UpdateActiveSector() {

	for( int i = 0; i < NUM_OF_SECTOR * 2 - 1; ++i )
	{
		for( int j = 0; j < NUM_OF_SECTOR * 2 - 1; ++j )
		{
			m_ActiveSector[i][j].active = false;
			m_ActiveSector[i][j].playerIndex.clear();
			m_ActiveSector[i][j].thunderboltIndex.clear();
		}
	}

	XMFLOAT3 pos;
	for( int i = 0; i < m_thunderbolts.size(); ++i )
	{
		if( m_thunderbolts[i].isUsed() )
		{
			pos = m_thunderbolts[i].GetPosition();
			UpdateActiveSector( pos.x, pos.z, 1, i );
		}
	}
	for( int i = 0; i < MAX_PLAYER; ++i )
	{
		if( m_players[i]->GetID() != -1 )
		{
			if( !m_players[i]->IsDead() )
			{
				pos = m_players[i]->GetCurForm()->GetPosition();
				UpdateActiveSector( pos.x, pos.z, 0, i );
			}
		}
	}
}

void Room::UpdateActiveSector( float x, float z, int what, int index )
{
	//일단 what 0 ==> player , 1==> thunderbolt 로 합니다
	int sector[2] = { (int)floor( x / SIZE_OF_SECTOR ),(int)floor( z / SIZE_OF_SECTOR ) };
	int sector2[2] = { (int)floor( (x - SIZE_OF_SECTOR_HALF) / SIZE_OF_SECTOR ),(int)floor( (z - SIZE_OF_SECTOR_HALF) / SIZE_OF_SECTOR ) };
	for( int i = 0; i < 2; ++i )
	{
		if( sector[i] >= NUM_OF_SECTOR )sector[i] = (NUM_OF_SECTOR - 1) * 2;
		else if( sector[i] < 0 )sector[i] = 0;
		else sector[i] *= 2;

		if( sector2[i] >= NUM_OF_SECTOR - 1 ) sector2[i] = 0;
		else if( sector2[i] < 0 ) sector2[i] = 0;
		else sector2[i] = sector2[i] * 2 + 1;
	}

	m_ActiveSector[sector[0]][sector[1]].active = true;

	if( sector2[0] != 0 && sector2[1] != 0 )
		m_ActiveSector[sector2[0]][sector2[1]].active = true;

	if( sector2[0] != 0 )
		m_ActiveSector[sector2[0]][sector[1]].active = true;

	if( sector2[1] != 0 )
		m_ActiveSector[sector[0]][sector2[1]].active = true;

	if( what == 0 )
	{
		m_ActiveSector[sector[0]][sector[1]].playerIndex.push_back( index );
		if( sector2[0] != 0 && sector2[1] != 0 )
			m_ActiveSector[sector2[0]][sector2[1]].playerIndex.push_back( index );
		if( sector2[0] != 0 )
			m_ActiveSector[sector2[0]][sector[1]].playerIndex.push_back( index );
		if( sector2[1] != 0 )
			m_ActiveSector[sector[0]][sector2[1]].playerIndex.push_back( index );

	}
	else if( what == 1 )
	{
		m_ActiveSector[sector[0]][sector[1]].thunderboltIndex.push_back( index );
		if( sector2[0] != 0 && sector2[1] != 0 )
			m_ActiveSector[sector2[0]][sector2[1]].thunderboltIndex.push_back( index );
		if( sector2[0] != 0 )
			m_ActiveSector[sector2[0]][sector[1]].thunderboltIndex.push_back( index );
		if( sector2[1] != 0 )
			m_ActiveSector[sector[0]][sector2[1]].thunderboltIndex.push_back( index );
	}
}

void Room::UpdateObjectSector( XMFLOAT3 prePos, XMFLOAT3 curPos, Object* object, int a, int b )
{
	int preSector[2] = { a,b };
	int preSectorOdd[2] = { (int)floor( (prePos.x - SIZE_OF_SECTOR_HALF) / SIZE_OF_SECTOR ),(int)floor( (prePos.z - SIZE_OF_SECTOR_HALF) / SIZE_OF_SECTOR ) };

	int curSector[2] = { (int)floor( curPos.x / SIZE_OF_SECTOR ),(int)floor( curPos.z / SIZE_OF_SECTOR ) };
	int curSectorOdd[2] = { (int)floor( (curPos.x - SIZE_OF_SECTOR_HALF) / SIZE_OF_SECTOR ),(int)floor( (curPos.z - SIZE_OF_SECTOR_HALF) / SIZE_OF_SECTOR ) };

	for( int i = 0; i < 2; ++i )
	{
		if( curSector[i] >= NUM_OF_SECTOR )curSector[i] = (NUM_OF_SECTOR - 1) * 2;
		else if( curSector[i] < 0 )curSector[i] = 0;
		else curSector[i] *= 2;

		if( preSectorOdd[i] >= NUM_OF_SECTOR - 1 ) preSectorOdd[i] = 0;
		else if( preSectorOdd[i] < 0 ) preSectorOdd[i] = 0;
		else preSectorOdd[i] = preSectorOdd[i] * 2 + 1;

		if( curSectorOdd[i] >= NUM_OF_SECTOR - 1 ) curSectorOdd[i] = 0;
		else if( curSectorOdd[i] < 0 ) curSectorOdd[i] = 0;
		else curSectorOdd[i] = curSectorOdd[i] * 2 + 1;
	}

	if( preSector[0] == curSector[0] && preSector[1] == curSector[1] && preSectorOdd[0] == curSectorOdd[0] && preSectorOdd[1] == curSectorOdd[1] )
		return;


	int tmpID = object->GetUniqueID();

	if( !(preSector[0] == curSector[0] && preSector[1] == curSector[1]) )	//짝수 섹터에서도 옮김이 있으면
	{
		for( auto iter = m_map->m_SectorObjList[a][b][Map::NOTFIXED].begin(); iter != m_map->m_SectorObjList[a][b][Map::NOTFIXED].end(); ++iter )
		{
			if( (**iter).GetUniqueID() == tmpID )
			{
				m_map->m_SectorObjList[a][b][Map::NOTFIXED].erase( iter );
				break;
			}
		}
		m_map->m_SectorObjList[curSector[0]][curSector[1]][Map::NOTFIXED].emplace_back( object );
	}


	for( int m = a - 1; m <= a + 1; ++m )
	{
		for( int n = b - 1; n <= b + 1; ++n )
		{
			if( m < 0 || n < 0 || m >= (NUM_OF_SECTOR * 2 - 1) || n >= (NUM_OF_SECTOR * 2 - 1) );
			else if( !((m & 1) == 0 && (n & 1) == 0) )	//둘다 짝수는 안된다
			{
				for( auto iter = m_map->m_SectorObjList[m][n][Map::NOTFIXED].begin(); iter != m_map->m_SectorObjList[m][n][Map::NOTFIXED].end(); )
				{
					if( (*iter)->GetUniqueID() == tmpID )
					{
						iter = m_map->m_SectorObjList[m][n][Map::NOTFIXED].erase( iter );
					}
					else
					{
						++iter;
					}
				}
			}
		}
	}	//싹 삭제 odd

	if( curSectorOdd[0] != 0 && curSectorOdd[1] != 0 )
		m_map->m_SectorObjList[curSectorOdd[0]][curSectorOdd[1]][Map::NOTFIXED].emplace_back( object );
	if( curSectorOdd[0] != 0 )
		m_map->m_SectorObjList[curSectorOdd[0]][curSector[1]][Map::NOTFIXED].emplace_back( object );
	if( curSectorOdd[1] != 0 )
		m_map->m_SectorObjList[curSector[0]][curSectorOdd[1]][Map::NOTFIXED].emplace_back( object );

}

void Room::MakeRoomManual()
{
	m_isManualRoom = true;
}

std::wstring Room::GetRoomName()
{
	return m_roomName;	//일단 지금은 이거 리턴 나중에는 방 이름 string 리턴
}

int Room::GetcurPlayerNum()
{
	return m_curPlayerNum;
}

int Room::GetRoomNo()
{
	return m_roomNo;
}

int Room::GetMapNo()
{
	return m_mapNo;
}

bool Room::IsGameStarted()
{
	return m_isGameStarted;
}

bool Room::IsRoundStarted()
{
	return m_isRoundStarted;
}

void Room::SetMapNo( int map_no )
{
	m_mapNo = map_no;
}

char& Room::CharTypeIntToStr( int c_type )
{
	char char_type[3]{};
	if( c_type == (int)CHARACTERS::NONE )
		strcpy( char_type, CHARACTER_NONE );	// 선택하지 않은 상태임.
	else if( c_type == (int)CHARACTERS::DR )
		strcpy( char_type, CHARACTER_DRUID );
	else if( c_type == (int)CHARACTERS::BA )
		strcpy( char_type, CHARACTER_BAIRD );
	else if( c_type == (int)CHARACTERS::FP )
		strcpy( char_type, CHARACTER_FEMALE_PEASANT );
	else if( c_type == (int)CHARACTERS::MP )
		strcpy( char_type, CHARACTER_MALE_PEASANT );
	else if( c_type == (int)CHARACTERS::SO )
		strcpy( char_type, CHARACTER_SORCERER );
	else{
		std::cout << "Unknown char_type error" << std::endl;
		while( 1 );
	}
	return *char_type;
}

void Room::SendLeftTimePacket()
{
	if( this == nullptr )
		return;

	if( m_leftTime > 1 && m_isGameStarted ){
		EVENT ev{ EVENT_KEY, m_roomNo, std::chrono::high_resolution_clock::now() + std::chrono::seconds( 1 )  , EV_TICK };
		BattleServer::GetInstance()->AddTimer( ev );
		if( m_leftTime % 5 == 0 )
		{
			std::cout << "left time : " << (int)m_leftTime << " count : " << m_countFrame << std::endl;
		}
		m_countFrame = 0;

		for( int i = 0; i < MAX_PLAYER; ++i )
		{
			int getID = m_players[i]->GetID();
			if( getID != -1 )
			{
				BattleServer::GetInstance()->SendLeftTimePacket( getID, (m_leftTime - 1) );
			}
		}
	}
}

void Room::CheckGameState()
{
	if( this == nullptr ) return;

	m_leftTime--;
	// 라운드 시작
	if( m_leftTime <= INITIAL_LEFT_TIME - HOLT_DURATION ){
		if( !m_isRoundStarted ){
			m_isRoundStarted = true;
			std::cout << "============= Round start =============" << std::endl;
			RoundStart();
		}
	}

	if( m_isGameStarted ){
		// 타임 오버 => 학생 승리.
		if( m_leftTime <= 0 ){
			// Log
			std::cout << "============= Game over - STUDENTS WIN =============" << std::endl;
			std::cout << "Left Students Count : " << m_aliveStudentNum << std::endl;
			GameOver( ROLE_STUDENT );
		}

		// 학생이 모두 죽거나, 룸에서 퇴장함 => 스승 승리.
		else if( m_aliveStudentNum <= 0 ){
			// Log
			std::cout << "============= Game over - MASTER WINS =============" << std::endl;
			GameOver( ROLE_MASTER );
		}

		// 스승이 룸에서 퇴장함. => 학생 승리.
		else if( m_aliveMasterNum <= 0 ){
			// Log
			std::cout << "============= Game over - STUDENTS WIN =============" << std::endl;
			GameOver( ROLE_STUDENT );
		}
	}
}

void Room::GameStart()
{
	std::random_device rd;   //<- 몹시 무거워서 스레드당 한번만 생성하는게 좋다는데 멀티스레드에서 안전한지를 모르겠음
	std::uniform_int_distribution<> roleUid{ 0, (m_curPlayerNum - 1) };
	std::uniform_int_distribution<> spawnPosUid{ 0, 5 }; // 0번은 스승 마법사 포지션이어야 함!!

	// 맵 불러오기
#ifdef SECTOR_ON
	m_map->LoadSectorMap( m_mapNo );
#else
	m_map->LoadMap( m_mapNo );
#endif


	// 시작 정보 구조체 값 채우기.
	PTC_Start_Info startInfo[4];
	int spIdx; // spawn position index.
	spIdx = 0;

	int spPattern = spawnPosUid( rd );

	int roleMaster = 1; // { roleUid( rd ) };
	m_players[roleMaster]->SetRole( ROLE_MASTER );

	for( int i = 0; i < MAX_PLAYER; ++i )
	{
		if( -1 != m_players[i]->GetID() )
		{
			startInfo[i].id = m_players[i]->GetID();
			startInfo[i].role = m_players[i]->GetRole();
			if( i != roleMaster )
			{
				startInfo[i].spawn_pos = SPAWN_PATTERN[spPattern][spIdx++];
				m_aliveStudentNum++;
			}
			else //스승이면
			{
				// 아래 코드 Room에서 하면 안되는 건데...
				Master* temp = new Master{ *m_players[i] };
				delete m_players[i];
				m_players[i] = temp;
				startInfo[i].spawn_pos = 0; // 스승 마법사 스폰위치는 0!!
				m_aliveMasterNum++;
			}
			m_players[i]->SetCurForm( (char)m_players[i]->GetRole(), m_mapNo, startInfo[i].spawn_pos );
		}
	}

	// 모든 플레이어에게 BC_GAME_START 패킷 보내주기
	for( int i = 0; i < MAX_PLAYER; ++i )
	{
		if( -1 != m_players[i]->GetID() )
		{
			int id = m_players[i]->GetID();
			BattleServer::GetInstance()->SendGameStartPacket( id, m_mapNo, startInfo );
		}
	}

	EVENT ev{ EVENT_KEY, m_roomNo, std::chrono::high_resolution_clock::now() + std::chrono::seconds( 2 ), EV_FULL_WORLD_UPDATE_END };	//일단 3초동안
	BattleServer::GetInstance()->AddTimer( ev );

	m_isGameStarted = true;
	m_isEnterable = false;
}

void Room::RoundStart()
{
#ifdef GAME_RULE_ON
	int master{};
	for( const auto& player : m_players ){
		if( player->GetRole() == ROLE_MASTER )
			master = player->GetID();
	}
	EVENT ev{ master, m_roomNo, std::chrono::high_resolution_clock::now()  , EV_MOVE_ENABLE };
	BattleServer::GetInstance()->AddTimer( ev );
#endif // GAME_RULE_ON

	// 모든 플레이어에게 BC_ROUND_START 패킷 보내주기
	for( int i = 0; i < MAX_PLAYER; ++i )
	{
		if( -1 != m_players[i]->GetID() )
		{
			int id = m_players[i]->GetID();
			BattleServer::GetInstance()->SendRoundStartPacket( id );
		}
	}
}

void Room::GameOver( int winner_role )
{
#ifdef GAME_RULE_ON
	// DB에 업데이트할 정보들 g_clients에 갱신
	UpdateUserInfo_DB( winner_role );
	m_isGameStarted = false;

	// 리소스 정리
	//Reset();
	EVENT ev{ EVENT_KEY, m_roomNo, std::chrono::high_resolution_clock::now() + std::chrono::seconds( 5 ), EV_RESET_ROOM };
	BattleServer::GetInstance()->AddTimer( ev );

#endif // GAME_RULE_ON
}

void Room::UpdateUserInfo_DB( int winner_role )
{
	for( auto& player : m_players ){
		int mmr{};
		int killCnt{};
		float winRate{};
		int totalCnt{};
		int winCnt{};

		int id{ player->GetID() };
		if( id != -1 ){
			killCnt = player->GetKillCnt();
			// 승자 업데이트
			if( player->GetRole() == winner_role ){
				mmr = WINNER_MMR_INCREASEMENT;
				winCnt = 1;
			}

			// 패자 업데이트
			else{
				mmr = LOSER_MMR_DECREASEMENT;
				//if( mmr <= 0 ) mmr = 0;
			}
			winRate = roundf( winRate * 100 ) / 100;


			ATOMIC::g_dbInfo_lock.lock();
			// 승률을 DB에서 계산하기 위해서는 이긴 판수와 전체 판수가 필요함.
			SHARED_RESOURCE::g_clients[id]->mmr += mmr;
			SHARED_RESOURCE::g_clients[id]->killCnt = killCnt;
			SHARED_RESOURCE::g_clients[id]->totalGameCnt += 1;
			SHARED_RESOURCE::g_clients[id]->winningGameCnt += winCnt;
			SHARED_RESOURCE::g_clients[id]->WRate = (float)SHARED_RESOURCE::g_clients[id]->winningGameCnt / (float)SHARED_RESOURCE::g_clients[id]->totalGameCnt;

			ATOMIC::g_dbInfo_lock.unlock();

			// bc_packet_game_over 전송, EV_UPDATE_DB 등록

			std::cout << "[LOG] 데이터베이스 업데이트 이벤트 등록" << std::endl;
			EVENT ev{ id, m_roomNo, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds( 1 ), EV_UPDATE_DB };
			BattleServer::GetInstance()->AddTimer( ev );

			BattleServer::GetInstance()->SendGameOverPacket( id, winner_role );
		}
	}
}

bool Room::CheckPenaltyEVAddable( int slot )
{
	if( m_players[slot]->GetRole() == ROLE_STUDENT ) {
		// 캐릭터 타입이면 페널티 적용하지 않는다.
		if( m_players[slot]->GetCurForm()->m_boundaries->GetObjType() == OBJ_TYPE_CHARACTER )
			return false;

		if( static_cast<Student*>(m_players[slot])->IsStopped() ) {
			if( !static_cast<Student*>(m_players[slot])->IsPenaltyChecking() ){
				static_cast<Student*>(m_players[slot])->SetPenaltyPos( m_players[slot]->GetCurForm()->GetPosition() );
				static_cast<Student*>(m_players[slot])->SetPenaltyCheck( true );
				return true;
			}
		}
	}
	return false;
}


void Room::CopyRecvMsgs()
{
	m_recvMsgQueueLock.lock();
	m_copiedRecvMsgs = m_recvMsgQueue;
	m_recvMsgQueueLock.unlock();

	m_recvMsgQueueLock.lock();
	while( !m_recvMsgQueue.empty() )
		m_recvMsgQueue.pop();
	m_recvMsgQueueLock.unlock();
}

void Room::ClearCopyMsgs()
{
	while( !m_copiedRecvMsgs.empty() )
		m_copiedRecvMsgs.pop();
}

void Room::ProcMsg( message msg )
{
	switch( msg.type )
	{
	case CB_READY:
	{
		for( int i = 0; i < MAX_PLAYER; ++i )
		{
			if( m_players[i]->GetID() == msg.id )
			{
				m_players[i]->SetReady();

				// 다른 플레이어들에게 보내준다.
				char readyStatus = m_players[i]->GetReady();
				PushReadyMsg( msg.id, readyStatus );
			}
		}

#ifdef LOG_ON
		for( int i = 0; i < MAX_PLAYER; ++i ) {
			if( m_players[i]->GetSlotStatus() )
				std::cout << "player - " << i << " empty!\n";
			else {
				if( !m_players[i]->GetReady() )
					std::cout << "player - " << m_players[i]->GetID() << "is not ready\n";
				else
					std::cout << "player - " << m_players[i]->GetID() << "is ready\n";
			}
	}
#endif
		break;
	}

	case CB_START:
	{
		std::cout << "recv Game Start!" << std::endl;
		ClearCopyMsgs();
		if( !m_isGameStarted ){
			GameStart();
			SendLeftTimePacket();
		}
		break;
	}

	case CB_KEY_W_DOWN:
	{
		for( int i = 0; i < MAX_PLAYER; ++i ) {
			if( msg.id == m_players[i]->GetID() ) {
				m_players[i]->SetKeyW( true );
				break;
			}
		}
		break;
	}

	case CB_KEY_S_DOWN:
	{
		for( int i = 0; i < MAX_PLAYER; ++i ) {
			if( msg.id == m_players[i]->GetID() ) {
				m_players[i]->SetKeyS( true );
				break;
			}
		}
		break;
	}

	case CB_KEY_A_DOWN:
	{
		for( int i = 0; i < MAX_PLAYER; ++i ) {
			if( msg.id == m_players[i]->GetID() ) {
				m_players[i]->SetKeyA( true );
				break;
			}
		}
		break;
	}

	case CB_KEY_D_DOWN:
	{
		for( int i = 0; i < MAX_PLAYER; ++i ) {
			if( msg.id == m_players[i]->GetID() ) {
				m_players[i]->SetKeyD( true );
				break;
			}
		}
		break;
	}

	case CB_KEY_W_UP:
	{
		for( int i = 0; i < MAX_PLAYER; ++i ) {
			if( msg.id == m_players[i]->GetID() ) {
				m_players[i]->SetKeyW( false );
				break;
			}
		}
		break;
	}

	case CB_KEY_S_UP:
	{
		for( int i = 0; i < MAX_PLAYER; ++i ) {
			if( msg.id == m_players[i]->GetID() ) {
				m_players[i]->SetKeyS( false );
				break;
			}
		}
		break;
	}

	case CB_KEY_A_UP:
	{
		for( int i = 0; i < MAX_PLAYER; ++i ) {
			if( msg.id == m_players[i]->GetID() ) {
				m_players[i]->SetKeyA( false );
				break;
			}
		}
		break;
	}

	case CB_KEY_D_UP:
	{
		for( int i = 0; i < MAX_PLAYER; ++i ) {
			if( msg.id == m_players[i]->GetID() ) {
				m_players[i]->SetKeyD( false );
				break;
			}
		}
		break;
	}

	case CB_JUMP:
	{
		for( int i = 0; i < MAX_PLAYER; ++i ) {
			if( msg.id == m_players[i]->GetID() ) {
				m_players[i]->SetKeySpace( true );
				break;
			}
		}
		break;
	}

	case CB_THUNDER_BOLT:
	{
		if( !m_isRoundStarted ) break;

		int bolt_id{};
		for( int i = 0; i < MAX_PLAYER; ++i ) {
			if( m_players[i]->GetRole() == ROLE_MASTER ) {
				for( int j = 0; j < MAX_THUNDERBOLT_COUNT; ++j ) {
					if( !m_thunderbolts[j].isUsed() )
					{
						bolt_id = j;
						XMFLOAT3 look{ m_players[i]->GetCurForm()->GetLook() };
						m_thunderbolts[j].Shoot( m_players[i]->GetCurForm()->GetPosition(), look, msg.vec, 1.f );

						// 썬더볼트 발사 애니메이션 수행을 알리기 위해 m_isShoot = true로 세트.
						static_cast<Master*>(m_players[i])->SetIsShoot( true );
						std::cout << "true로 세트!" << std::endl;
						break;
					}
					else if( j + 1 == MAX_THUNDERBOLT_COUNT )
					{
						std::cout << "bullet full!" << std::endl;
					}
				}
			}
		}

		XMFLOAT3 boltPos{ m_thunderbolts[bolt_id].GetPosition() };
		PTC_Vector ptc_boltPos{};
		ptc_boltPos.x = boltPos.x;
		ptc_boltPos.y = boltPos.y;
		ptc_boltPos.z = boltPos.z;
		for( int i = 0; i < MAX_PLAYER; ++i ) {
			int id = m_players[i]->GetID();
			if( id != -1 ){
				PushThunderboltPutMsg( id, bolt_id, ptc_boltPos );
			}
		}
		break;
	}

	case CB_TRANSFORM:
	{
		if( !m_isGameStarted ) break;

		short obj_type{ (short)msg.vec.x };

		//std::cout << "recved ObjType - " << obj_type << std::endl;

		if( obj_type >= 300 || obj_type == 0 )
			break;

		// msg.id가 obj_type으로 변신하게 하기.
		for( int i = 0; i < MAX_PLAYER; ++i ) {
			if( m_players[i]->GetID() == msg.id ) {
				if( static_cast<Student*>(m_players[i])->IsTransformalble() && !m_players[i]->IsDead() ){
					static_cast<Student*>(m_players[i])->Transform( obj_type );

					// 프랍상태로 변신하면 페널티 체크 이벤트 등록
#ifdef GAME_RULE_ON
					if( obj_type != OBJECT_TYPE_CHARACTER && !static_cast<Student*>(m_players[i])->IsPenaltyChecking() ){
						// 위치 저장 
						XMFLOAT3 pos = m_players[i]->GetCurForm()->GetPosition();
						static_cast<Student*>(m_players[i])->SetPenaltyPos( pos );
						static_cast<Student*>(m_players[i])->SetPenaltyTime( PENALTY_TIME );

						EVENT ev_penalty_check{ m_players[i]->GetID(), m_roomNo, std::chrono::high_resolution_clock::now() + std::chrono::milliseconds( 1 )  , EV_PENALTY_CHECK };
						BattleServer::GetInstance()->AddTimer( ev_penalty_check );
						static_cast<Student*>(m_players[i])->SetPenaltyCheck( true );
					}
					else if( obj_type == OBJECT_TYPE_CHARACTER ){
						static_cast<Student*>(m_players[i])->SetPenaltyCheck( false );
						int pTime = static_cast<Student*>(m_players[i])->GetPenaltyTime();
						if( pTime <= 3 ) PushPenaltyWarningEndMsg( msg.id );
					}
#endif 
					// 클라이언트들에게 알려주기.
					PushTransformMsg( msg.id, obj_type );
				}
			}
		}
		break;
	}

	case CB_LOOK_VECTOR:
	{
		for( int i = 0; i < MAX_PLAYER; ++i )
		{
			int playerID = m_players[i]->GetID();
			if( -1 != playerID )
			{
				if( playerID == msg.id )
				{
					if( m_players[i]->GetCurForm() == nullptr ) return;
					// preLook 구하기
					XMFLOAT3 preLook{ m_players[i]->GetCurForm()->GetLook() };
					m_players[i]->GetCurForm()->SetPreLook( preLook );

					m_players[i]->GetCurForm()->SetMatrixByLook( msg.vec.x, msg.vec.y, msg.vec.z );
					//플레이어 BB회전 일단 이렇게 함 bb하나니껜
					m_players[i]->GetCurForm()->m_boundaries->SetBBLook( m_players[i]->GetCurForm()->GetLook(), 0 );
					m_players[i]->GetCurForm()->m_boundaries->SetBBRight( m_players[i]->GetCurForm()->GetRight(), 0 );
				}
				else
				{
					PTC_Vector recvLook{ msg.vec.x, msg.vec.y, msg.vec.z };
					BattleServer::GetInstance()->SendPlayerRotation( playerID, msg.id, recvLook );
					//PushPlayerDirectionMsg( playerID, msg.id, recvLook );
				}
			}
		}
		break;
	}

	case CB_MAP_TYPE:
	{
		m_mapNo = (int)msg.vec.x;
		PushMapSelectedMsg();
		break;
	}

	case CB_SELECT_CHARACTER:
	{
		// 선택된 캐릭터 슬롯을 확인한다.
		int idx = (int)msg.vec.x;

		char char_type[3];
		memcpy( char_type, &CharTypeIntToStr( idx ), sizeof( char ) * 3 );

		// false면 선택하지 않은 캐릭터 ==> true로 세트하고, 룸의 플레이어들에게 bc_packet_select_character 전송
		if( !m_characterSlot[idx] ){
			m_characterSlot[idx] = true;
			// 해당 플레이어 멤버 m_CharacterType도 갱신한다.
			for( const auto& player : m_players ){
				if( player->GetID() == msg.id )
					player->SetCharacterType( idx );

				// 20200318 임원택 예외처리 추가
				if( player->GetID() != -1 )
					BattleServer::GetInstance()->SendSelectCharacterPacket( player->GetID(), msg.id, char_type );
			}
		}
		// true면 선택된 캐릭터 ==> bc_packet_select_character_fail
		else
			BattleServer::GetInstance()->SendSelectCharacterFailPacket( msg.id );
		break;
	}

	case CB_CANCEL_SELECT_CHARACTER:
	{
		int idx = (int)msg.vec.x;
		m_characterSlot[idx] = false;
		char char_type[3];
		memcpy( char_type, &CharTypeIntToStr( idx ), sizeof( char ) * 3 );

		for( const auto& player : m_players ){
			if( player->GetID() == msg.id )
				player->SetCharacterType( (int)CHARACTERS::NONE );

			if( player->GetID() != -1 )
				BattleServer::GetInstance()->SendCancleSelectCharacterPakcet( player->GetID(), msg.id, char_type );
		}
		break;
	}

	case CB_TEST_MINUS_TIME:
	{
		if( (m_leftTime - 10) <= 0 )
			return;
		m_leftTime -= 10;
		break;
	}

	case CB_TEST_PLUS_TIME:
	{
		if( (m_leftTime + 10) >= INITIAL_LEFT_TIME )
			return;
		m_leftTime += 10;
		break;
	}

	case CB_TEST_ZERO_HP:
	{
		for( auto& player : m_players ){
			if( player->GetID() == msg.id ){
				if( player->GetRole() == ROLE_MASTER ) return;
				if( player->IsDead() ) return;

				PushDieMsg( player->GetID() );
				player->SetDead();
				m_aliveStudentNum--;
				std::cout << "[LOG] Player - ID" << player->GetID() << "Dead!" << std::endl;
				std::cout << "[LOG] Left Student Count - " << m_aliveStudentNum << std::endl;

			}
		}
		break;
	}

	default:
	{
		std::cout << "[LOG] UNKNOWN MSG TYPE ERROR! \n";
		while( true );
	}
}
}

void Room::ProcUnUsedThunderbolt()
{
	std::queue<int> tmpThunderboltQueue;
	m_unUsedThunderboltListLock.lock();
	tmpThunderboltQueue = m_unUsedThunderboltList;
	m_unUsedThunderboltListLock.unlock();

	m_unUsedThunderboltListLock.lock();
	while( !m_unUsedThunderboltList.empty() )
		m_unUsedThunderboltList.pop();
	m_unUsedThunderboltListLock.unlock();


	while( tmpThunderboltQueue.empty() != true )
	{
		m_thunderbolts[tmpThunderboltQueue.front()].SetNotUse();
		tmpThunderboltQueue.pop();
	}
}


void Room::PushSendMsg( int id, void* buff )
{
	char* packet = reinterpret_cast<char*>(buff);
	SEND_INFO newSendInfo{};
	newSendInfo.to = id;
	memcpy( &newSendInfo.buff, buff, (BYTE)packet[0] );

	m_sendMsgQueueLock.lock();
	m_sendMsgQueue.push( newSendInfo );
	m_sendMsgQueueLock.unlock();
}

void Room::FlushSendMsg()
{
	//std::cout << "FlushSendMsg Called" << std::endl;
	if( this == nullptr ) return;

	m_sendMsgQueueLock.lock();
	std::queue<SEND_INFO> tmpQueue{ m_sendMsgQueue };
	m_sendMsgQueueLock.unlock();

	m_sendMsgQueueLock.lock();
	while( !m_sendMsgQueue.empty() )
		m_sendMsgQueue.pop();
	m_sendMsgQueueLock.unlock();

	while( !tmpQueue.empty() )
	{
		SEND_INFO send_info = tmpQueue.front();
		tmpQueue.pop();

		BattleServer::GetInstance()->SendPacket( send_info.to, &send_info.buff );
	}
}

void Room::PushPlayerPositionMsg( int to, int from, PTC_Vector* position_info )
{
	bc_packet_player_pos packet;
	packet.size = sizeof( packet );
	packet.type = BC_PLAYER_POS;
	packet.id = from;
	packet.pos.x = position_info->x;
	packet.pos.y = position_info->y;
	packet.pos.z = position_info->z;
	PushSendMsg( to, &packet );
}

void Room::PushPlayerDirectionMsg( int to, int from, PTC_Vector lookVector )
{
	bc_packet_player_rot packet;
	packet.size = sizeof( packet );
	packet.type = BC_PLAYER_ROT;
	packet.id = from;
	packet.look = lookVector;
	PushSendMsg( to, &packet );
}

void Room::PushObjectPositionMsg( int id, short type_id, int obj_id, PTC_Vector* position_info )
{
	bc_packet_object_pos packet;
	packet.size = sizeof( packet );
	packet.type = BC_OBJECT_POS;
	packet.type_id = type_id;
	packet.obj_id = obj_id;
	packet.pos.x = position_info->x;
	packet.pos.y = position_info->y;
	packet.pos.z = position_info->z;
	PushSendMsg( id, &packet );
}

void Room::PushTransformMsg( int transformer, short obj_type )
{
	bc_packet_transform packet;
	packet.size = sizeof( packet );
	packet.type = BC_TRANSFORM;
	packet.id = transformer;
	packet.object_type = obj_type;

	for( int i = 0; i < MAX_PLAYER; ++i ) {
		int getID = m_players[i]->GetID();
		if( getID != -1 ) {
			PushSendMsg( getID, &packet );
		}
	}
}

void Room::PushThunderboltPutMsg( int to, int bolt_id, PTC_Vector boltPos )
{
	bc_packet_put_thunderbolt packet;
	packet.size = sizeof( packet );
	packet.type = BC_PUT_THUNDERBOLT;
	packet.bolt_id = bolt_id;
	packet.pos = boltPos;
	PushSendMsg( to, &packet );
}

void Room::PushThunderboltRemoveMsg( int bolt_id )
{
	bc_packet_remove_thunderbolt packet;
	packet.type = BC_REMOVE_THUNDERBOLT;
	packet.size = sizeof( packet );
	packet.bolt_id = bolt_id;

	for( const auto& player : m_players ) {
		int getID{ player->GetID() };
		if( getID != -1 ) {
			PushSendMsg( getID, &packet );
		}
	}
}

void Room::PushHitMsg( int hitman, float hpPct )
{
	bc_packet_hit packet;
	packet.type = BC_HIT;
	packet.size = sizeof( packet );
	packet.hp = hpPct;
	packet.id = hitman;

	int getID{};
	for( const auto& player : m_players ){
		getID = player->GetID();
		if( getID == -1 ) continue;
		PushSendMsg( getID, &packet );
	}
}
void Room::PushDieMsg( int id )
{
	bc_packet_die packet;
	packet.type = BC_DIE;
	packet.size = sizeof( packet );
	packet.id = id;

	for( const auto& player : m_players ) {
		int getID{ player->GetID() };
		if( getID != -1 ) {
			PushSendMsg( getID, &packet );
		}
	}
}

void Room::PushChatMsg( int id, void* chat )
{
	bc_packet_chat packet;
	packet.size = sizeof( packet );
	packet.type = BC_CHAT;
	packet.id = id;
	memcpy( &packet.chat, chat, sizeof( packet.chat ) );

	for( int i = 0; i < MAX_PLAYER; ++i )
	{
		int getID = m_players[i]->GetID();
		if( getID != -1 && getID != id )
		{
			PushSendMsg( getID, &packet );
		}
	}
}

void Room::PushReadyMsg( int id, bool ready_status )
{
	bc_packet_ready packet;
	packet.size = sizeof( packet );
	packet.type = BC_READY;
	packet.id = id;
	packet.ready = ready_status;

	for( int i = 0; i < MAX_PLAYER; ++i ) {
		int getID = m_players[i]->GetID();
		if( getID != -1 ) {
			PushSendMsg( getID, &packet );
		}
	}
}

void Room::PushGameStartAvailableMsg( int id, bool available )
{
	bc_packet_gamestart_available packet;
	packet.size = sizeof( packet );
	packet.type = BC_GAMESTART_AVAILABLE;
	packet.available = available;
	PushSendMsg( id, &packet );
}

void Room::PushNewRoomManagerMsg( int id )
{
	bc_packet_new_room_manager packet;
	packet.size = sizeof( packet );
	packet.type = BC_NEW_ROOM_MANAGER;
	packet.id = id;

	for( int i = 0; i < MAX_PLAYER; ++i ) {
		int getID = m_players[i]->GetID();
		if( getID != -1 ) {
			PushSendMsg( getID, &packet );
		}
	}
}

void Room::PushMapSelectedMsg()
{
	bc_packet_map_type packet;
	packet.size = sizeof( packet );
	packet.type = BC_MAP_TYPE;
	packet.mapType = GetMapNo();
	for( int i = 0; i < MAX_PLAYER; ++i ) {
		int getID = m_players[i]->GetID();
		if( getID != -1 ) {
			PushSendMsg( getID, &packet );
		}
	}
}

void Room::PushPenaltyWarningMsg( int id )
{
	bc_packet_penalty_warning packet;
	packet.size = sizeof( bc_packet_penalty_warning );
	packet.type = BC_PENALTY_WARNING;
	PushSendMsg( id, &packet );
}

void Room::PushPenaltyWarningEndMsg( int id )
{
	bc_packet_penalty_warning_end packet;
	packet.size = sizeof( packet );
	packet.type = BC_PENALTY_WARNING_END;
	PushSendMsg( id, &packet );
}



void Room::PushPenaltyBeginMsg( int id )
{
	bc_packet_penalty_begin packet;
	packet.size = sizeof( packet );
	packet.type = BC_PENALTY_BEGIN;
	packet.id = id;

	for( const auto& player : m_players ){
		int getID{ player->GetID() };
		if( getID == -1 ) continue;
		PushSendMsg( getID, &packet );
	}
}

void Room::PushPenaltyEndMsg( int id )
{
	bc_packet_penalty_end packet;
	packet.size = sizeof( packet );
	packet.type = BC_PENALTY_END;
	packet.id = id;

	for( const auto& player : m_players ){
		int getID{ player->GetID() };
		if( getID == -1 ) continue;
		PushSendMsg( getID, &packet );
	}
}

void Room::PushAnimMsg( int animator, int animType )
{
	bc_packet_anim_type packet;
	packet.size = sizeof( packet );
	packet.type = BC_ANIM;
	packet.id = animator;
	packet.anim_type = animType;

	for( const auto& player : m_players ){
		int getId = player->GetID();
		if( getId != -1 && getId != animator ){
			PushSendMsg( getId, &packet );
		}
	}
}

void Room::CheckPenalty( int id )
{
	if( this == nullptr ) return;
	if( !m_isGameStarted ) return; // 게임 종료되면 바로 리턴.

#ifdef LOG_ON
	std::cout << "[LOG] player - " << id << ", checkpenalty called" << std::endl;
#endif
	for( int i = 0; i < MAX_PLAYER; ++i ) {
		if( m_players[i]->GetID() == -1 )
			continue;

		if( m_players[i]->GetCurForm()->m_boundaries == nullptr )
			return;

		if( m_players[i]->IsDead() )
			continue;

		if( m_players[i]->GetCurForm()->m_boundaries->GetObjType() == OBJECT_TYPE_CHARACTER ) // 캐릭터가 BB정보가 없어 여기서 터짐.
			continue;	// 캐릭터 상태일 경우

		if( m_players[i]->GetID() == id ) {
			XMFLOAT3 curPos{ m_players[i]->GetCurForm()->GetPosition() };
			size_t pTime{};
			pTime = static_cast<Student*>(m_players[i])->GetPenaltyTime();
			if( static_cast<Student*>(m_players[i])->IsInRadious() ) {
				//std::cout << pTime << std::endl;
				if( pTime == 3 ){
					PushPenaltyWarningMsg( id );
				}

				if( pTime <= 0 ) {
#ifdef LOG_ON
					std::cout << "[LOG] ID - " << id << "패널티 적용 시작" << std::endl;
#endif
					EVENT ev_penalty{ id, m_roomNo, std::chrono::high_resolution_clock::now() + std::chrono::seconds( 1 ), EV_PENALTY_BEGIN };
					BattleServer::GetInstance()->AddTimer( ev_penalty );
				}
				else {
					static_cast<Student*>(m_players[i])->SetPenaltyTime( --pTime );
					EVENT ev_penalty_check{ id, m_roomNo, std::chrono::high_resolution_clock::now() + std::chrono::seconds( 1 ), EV_PENALTY_CHECK };
					BattleServer::GetInstance()->AddTimer( ev_penalty_check );
				}
			}

			else {
				// 반경을 벗어났음. 언제 다시 EV_PENALTY_CHECK 등록해야하지..?
				// 1. 가만히 있는지 확인한다.
				// 2. 다시 이벤트를 등록한다.
#ifdef LOG_ON
				std::cout << "[LOG] ID - " << id << "페널티 체크 비활성" << std::endl;
#endif
				if( pTime < 3 ) PushPenaltyWarningEndMsg( id ); 
				static_cast<Student*>(m_players[i])->SetPenaltyCheck( false );
				static_cast<Student*>(m_players[i])->SetPenaltyPos( curPos );
				static_cast<Student*>(m_players[i])->SetPenaltyTime( PENALTY_TIME );
			}
			break;
		}
	}
}

void Room::ApplyPenalty( int id )
{
	if( this == nullptr ) return;
	if( !m_isGameStarted ) return; // 게임 종료되면 바로 리턴.

#ifdef LOG_ON
	std::cout << "[LOG] ID - " << id << " 페널티 시작!" << std::endl;
#endif
	// EV_PENALTY_END (15초 후) 등록
	EVENT ev{ id, m_roomNo, std::chrono::high_resolution_clock::now() + std::chrono::seconds( PENALTY_DURATION ), EV_PENALTY_END };
	BattleServer::GetInstance()->AddTimer( ev );
	for( const auto& player : m_players ){
		if( player->GetID() == id ){
			// 학생 캐릭터로 강제 변신
			static_cast<Student*>(player)->Transform( OBJ_TYPE_CHARACTER );
			// 변신 불가능 상태로 세트
			static_cast<Student*>(player)->SetIsTransformable( false );
			PushPenaltyBeginMsg( id );
			// 강제변신 한 것 클라들에게 뿌리기.
			PushTransformMsg( id, OBJ_TYPE_CHARACTER );
		}
	}
}

void Room::UnapplyPenalty( int id )
{
	if( this == nullptr ) return;
	if( !m_isGameStarted ) return; // 게임 종료되면 바로 리턴.

	// 플레이어 변신 가능 상태로 세트
#ifdef LOG_ON
	std::cout << "[LOG] ID - " << id << " 페널티 끝!" << std::endl;
#endif
	for( const auto& player : m_players ){
		if( player->GetID() == id ){
			static_cast<Student*>(player)->SetIsTransformable( true );
			static_cast<Student*>(player)->SetPenaltyCheck( false );
			PushPenaltyEndMsg( id );
		}
	}
}

void Room::MakeMove( int id )
{
	if( this == nullptr ) return;
	if( !m_isGameStarted ) return; // 게임 종료되면 바로 리턴.

	for( auto& player : m_players ){
		if( player->GetID() == -1 ) continue;
		if( player->GetID() == id ){
			player->SetIsMovable( true );
			// Log
#ifdef LOG_ON
			std::cout << "Player - " << id << "now can move." << std::endl;
#endif
		}
	}
}

void Room::MakeNoMove( int id )
{
	if( this == nullptr ) return;
	if( !m_isGameStarted ) return; // 게임 종료되면 바로 리턴.

	for( auto& player : m_players ){
		if( player->GetID() == -1 ) continue;
		if( player->GetID() == id ){
			player->SetIsMovable( false );
			// Log
#ifdef LOG_ON
			std::cout << "Player - " << id << "now can not move." << std::endl;
#endif
		}
	}
}

void Room::FullWorldUpdateEnd() {
	m_UpdateFullWorldSector = false;
}

void Room::ThunderboltSetNotUse( int index ) {
	m_unUsedThunderboltListLock.lock();
	m_unUsedThunderboltList.push( index );
	m_unUsedThunderboltListLock.unlock();
}