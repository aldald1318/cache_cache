#pragma once
#include "pch.h"
#include "Globals.h"
#include "SpawnPosition.h"
#include "Map.h"

class Room;
namespace SHARED_RESOURCE {
	extern HANDLE g_iocp;
	extern std::map<int, Room*> g_rooms;
	extern std::array<int, MAX_ROOM> g_manualMatchRoomNo;
	extern std::array<int, MAX_AUTO_ROOM> g_autoMatchRoomNo;
	extern std::array<SOCKETINFO*, MAX_CLIENTS> g_clients;
	//extern std::map<int, SOCKETINFO*> g_clients;
	extern std::priority_queue<EVENT> g_timer_queue;

	extern Map g_map[2];
	extern 	std::map<int, Boundary*> g_boundaries;
	extern SpawnPosition g_spawnPos[2];
}

namespace ATOMIC {
	extern std::mutex g_timer_lock;
	extern std::mutex g_msg_lock;
	extern std::mutex g_autoMatchRoomNo_lock;
	extern std::mutex g_manualMatchRoomNo_lock;
	extern std::mutex g_dbInfo_lock;
	extern std::mutex g_clients_lock;
	extern std::mutex g_room_lock;
	extern std::atomic_int g_numOfRoom;
	extern std::atomic_int g_NumOfManualRoom;
	extern std::atomic_int g_numOfAutoRoom;
}