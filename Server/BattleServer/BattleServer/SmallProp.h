#pragma once
#include "Prop.h"
class SmallProp :
	public Prop
{

public:
	using Prop::Prop;
	SmallProp() = default;
	SmallProp( const obj_info& info, const Boundary* bb_info ) : Prop( info, bb_info ){ Initialize(); }
	SmallProp( Object& obj, const Boundary* bb_info ) : Prop( obj, bb_info ){ Initialize(); }
	void Initialize();

	SmallProp* clone() const { return new SmallProp( *this ); }
};

