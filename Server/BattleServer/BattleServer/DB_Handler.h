#pragma once
//pch에 넣으면 안됩니다 안돌아감
#include <iostream>
#include <string>
#include <windows.h>  
#include <sqlext.h>  
#include <codecvt>

constexpr auto NAMELEN = 11;

class DB_Handler
{
	// 데이터 베이스 연결, Room에서 게임 종료 시, user들 MMR 업데이트
public:
	DB_Handler();
	~DB_Handler();

	bool GetUserInfo( std::string id, int& mmr, int& killCnt, float& W_rate, int& totalCnt, int& WCnt );
	void UpdateUserInfo( std::string& id, int& mmr, int& killCnt, float& WRate, int& TCnt, int& WCnt );

	void HandleDiagnosticRecord( SQLHANDLE hHandle, SQLSMALLINT hType, RETCODE RetCode );
	void show_error();
	std::wstring StringToWstring( const std::string& str );

private:
	SQLHENV henv;
	SQLHDBC hdbc;
	SQLHSTMT hstmt = 0;
	SQLRETURN retcode;

	SQLINTEGER nMmr{}, nKillCnt{}, nTotalCnt{}, nWinCnt{};
	SQLREAL nWinRate{};
	SQLLEN cbMmr{}, cbKillCnt{}, cbWinRate{}, cbTotalCnt{}, cbWinCnt{};
};
