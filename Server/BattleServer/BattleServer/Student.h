#pragma once
#include"Player.h"
class Student :
	public Player
{
public:
	Student();
	Student( const Player& rhs ) : Player( rhs ) { Initialize(); }
	~Student() override;
	void Initialize();
	void Reset() override;
	bool Update( float elapsedTime ) override;
public:
	void SetHP( const int& type );
	void SetHP( const unsigned short& hp );
	unsigned short GetHP() const;

	void SetMaxHP( const int& type );
	unsigned short GetMaxHP() const;

	void SetPenaltyPos( const XMFLOAT3& pos );
	XMFLOAT3 GetPenaltyPos();

	void SetPenaltyTime( const size_t& tick );
	size_t GetPenaltyTime();
	void SetPenaltyCheck(bool isChecked);
	void isInRad( const XMFLOAT3 curPos );
	bool IsPenaltyChecking();
	bool IsStopped();

	bool IsTransformalble();
	void SetIsTransformable( bool );

	bool IsInRadious(){ return m_bInRad; }
	void SetInRadious( bool inRad ) { m_bInRad = inRad; }

public:
	void Transform(short obj_type);

public:
	int GetAnimType() override;

private:
	unsigned short m_Hp;
	unsigned short m_MaxHp;
	bool m_bTransformable = true;

	XMFLOAT3 m_PenaltyPos;	// 변신 위치, 학생 마법사의 현재 위치와의 거리를 측정하기 위함.
	size_t m_PenaltyTime;
	bool m_bPenaltyChecking;		// 변신 페널티 검사 체크하고 있는지
	bool m_bInRad{};
};