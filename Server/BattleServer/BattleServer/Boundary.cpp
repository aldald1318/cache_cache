#include "pch.h"
#include "Boundary.h"

Boundary::Boundary() noexcept
{
	m_xmf4x4_BB_World = nullptr;
	m_xmf3_BB_Size = nullptr;
	m_xmf3_BB_Rot = nullptr;
	m_xmf3_BS = nullptr;
	m_BS_Rad = nullptr;
	m_obj_type = NONETYPE;
	m_numOfBB = 0;
	m_numOfBS = 0;
}

Boundary::Boundary( int numBB, int numBS ) noexcept
{
	m_obj_type = NONETYPE;

	m_numOfBB = numBB;
	m_numOfBS = numBS;

	m_xmf4x4_BB_World = new XMFLOAT4X4[m_numOfBB];
	for ( int i = 0; i < m_numOfBB; ++i ) {
		m_xmf4x4_BB_World[i] = Matrix4x4::Identity();
	}
	m_xmf3_BB_Size = new XMFLOAT3[m_numOfBB];
	m_xmf3_BB_Rot = new XMFLOAT3[m_numOfBB];

	m_xmf3_BS = new XMFLOAT3[m_numOfBS];
	m_BS_Rad = new float[m_numOfBS];
}

Boundary::Boundary( const Boundary& other ) noexcept
{
	// 이 복사생성자는 맵 불러올 때 사용하면 되지 않을까?
	m_obj_type = other.m_obj_type;
	m_numOfBB = other.m_numOfBB;
	m_xmf4x4_BB_World = new XMFLOAT4X4[m_numOfBB];
	m_xmf3_BB_Size = new XMFLOAT3[m_numOfBB];
	m_xmf3_BB_Rot = new XMFLOAT3[m_numOfBB];

	memcpy( m_xmf4x4_BB_World, other.m_xmf4x4_BB_World, sizeof( XMFLOAT4X4 ) * m_numOfBB );
	memcpy( m_xmf3_BB_Size, other.m_xmf3_BB_Size, sizeof( XMFLOAT3 ) * m_numOfBB );
	memcpy( m_xmf3_BB_Rot, other.m_xmf3_BB_Rot, sizeof( XMFLOAT3 ) * m_numOfBB );

	m_numOfBS = other.m_numOfBS;
	m_xmf3_BS = new XMFLOAT3[m_numOfBS];
	m_BS_Rad = new float[m_numOfBS];

	memcpy( m_xmf3_BS, other.m_xmf3_BS, sizeof( XMFLOAT3 ) * m_numOfBS );
	memcpy( m_BS_Rad, other.m_BS_Rad, sizeof( float ) * m_numOfBS );
}

Boundary& Boundary::operator=( const Boundary& other )
{
	if( this != &other ){
		delete[] m_xmf4x4_BB_World;
		delete[] m_xmf3_BB_Size;
		delete[] m_xmf3_BB_Rot;
		delete[] m_xmf3_BS;
		delete[] m_BS_Rad;

		m_numOfBB = other.m_numOfBB;
		m_xmf4x4_BB_World = new XMFLOAT4X4[m_numOfBB];
		m_xmf3_BB_Size = new XMFLOAT3[m_numOfBB];
		m_xmf3_BB_Rot = new XMFLOAT3[m_numOfBB];

		memcpy( m_xmf4x4_BB_World, other.m_xmf4x4_BB_World, sizeof( XMFLOAT4X4 ) * m_numOfBB );
		memcpy( m_xmf3_BB_Size, other.m_xmf3_BB_Size, sizeof( XMFLOAT3 ) * m_numOfBB );
		memcpy( m_xmf3_BB_Rot, other.m_xmf3_BB_Rot, sizeof( XMFLOAT3 ) * m_numOfBB );

		m_numOfBS = other.m_numOfBS;
		m_xmf3_BS = new XMFLOAT3[m_numOfBS];
		m_BS_Rad = new float[m_numOfBS];

		memcpy( m_xmf3_BS, other.m_xmf3_BS, sizeof( XMFLOAT3 ) * m_numOfBS );
		memcpy( m_BS_Rad, other.m_BS_Rad, sizeof( float ) * m_numOfBS );
	}
	return *this;
}
Boundary::~Boundary()
{
	delete[] m_xmf4x4_BB_World;
	delete[] m_xmf3_BB_Size;
	delete[] m_xmf3_BB_Rot;
	delete[] m_xmf3_BS;
	delete[] m_BS_Rad;
}

void Boundary::SetBB( int numBB )
{
	if ( m_xmf4x4_BB_World != nullptr )delete[] m_xmf4x4_BB_World;
	if ( m_xmf3_BB_Size != nullptr )delete[] m_xmf3_BB_Size;
	if ( m_xmf3_BB_Rot != nullptr )delete[] m_xmf3_BB_Rot;

	m_numOfBB = numBB;
	m_xmf4x4_BB_World = new XMFLOAT4X4[numBB];
	for ( int i = 0; i < numBB; ++i ) {
		m_xmf4x4_BB_World[i] = Matrix4x4::Identity();
	}
	m_xmf3_BB_Size = new XMFLOAT3[numBB];
	m_xmf3_BB_Rot = new XMFLOAT3[numBB];
}
void Boundary::SetBS( int numBS )
{
	if ( m_xmf3_BS != nullptr )delete[] m_xmf3_BS;

	m_numOfBS = numBS;
	m_xmf3_BS = new XMFLOAT3[numBS];
	m_BS_Rad = new float[numBS];
}

int Boundary::GetObjType() const
{
	return m_obj_type;
}

void Boundary::SetObjType( int obj_type )
{
	m_obj_type = obj_type;
}

int Boundary::GetNumOfBB() const
{
	return m_numOfBB;
}

void Boundary::SetNumOfBB( int num_bb )
{
	m_numOfBB = num_bb;
}

int Boundary::GetNumOfBS() const
{
	return m_numOfBS;
}

void Boundary::SetNumOfBS( int num_bs )
{
	m_numOfBS = num_bs;
}


void Boundary::BoundaryMove( float x, float y, float z )
{
	XMFLOAT3 distance { x,y,z };
	BoundaryMove( distance );
}

void Boundary::BoundaryMove( XMFLOAT3 distance )
{
	for ( int i = 0; i < m_numOfBS; ++i )
	{
		XMFLOAT3 bsPos = GetBSPos( i );
		bsPos = Vector3::Add( bsPos, distance );
		SetBSPos( bsPos, i );

	}
	for ( int i = 0; i < m_numOfBB; ++i )
	{
		XMFLOAT3 bbPos = GetBBPos( i );
		bbPos = Vector3::Add( bbPos, distance );
		SetBBPos( bbPos, i );
	}
}

XMFLOAT4X4 Boundary::GetWorldMatrix( int idx ) const
{
	return m_xmf4x4_BB_World[idx];
}

void Boundary::SetWorldMatrix( const XMFLOAT4X4& mat, int idx )
{
	m_xmf4x4_BB_World[idx] = mat;
}

void Boundary::BBRotate( XMFLOAT3 eulerAngle, int idx )
{
	BBRotate( eulerAngle.x, eulerAngle.y, eulerAngle.z, idx );
}

void Boundary::BBRotate( XMFLOAT4 quaternion, int idx )
{
	XMFLOAT4X4 rot{};
	QuatToMatrix( &quaternion, &rot );
	m_xmf4x4_BB_World[idx] = Matrix4x4::Multiply( rot, m_xmf4x4_BB_World[idx] );
}

void Boundary::BBRotate( float pitch, float yaw, float roll,int idx )
{
	XMFLOAT4 quat = Vector4::XMVectorToFloat4( XMQuaternionRotationRollPitchYaw( XMConvertToRadians( pitch ),
		XMConvertToRadians( yaw ), XMConvertToRadians( roll ) ) );
	XMFLOAT4X4 rot{};

	QuatToMatrix( &quat, &rot );
	m_xmf4x4_BB_World[idx] = Matrix4x4::Multiply( rot, m_xmf4x4_BB_World[idx] );
}

XMFLOAT3 Boundary::GetBBRot( int idx ) const
{
	return m_xmf3_BB_Rot[idx];
}

void Boundary::SetBBRot( float x, float y, float z, int idx )
{
	if ( x >= 360.f )x -= 360;
	if ( y >= 360.f )y -= 360;
	if ( z >= 360.f )z -= 360;

	m_xmf3_BB_Rot[idx].x = x;
	m_xmf3_BB_Rot[idx].y = y;
	m_xmf3_BB_Rot[idx].z = z;
}

void Boundary::SetBBRot( XMFLOAT3 xmfRot, int idx )
{
	SetBBRot( xmfRot.x, xmfRot.y, xmfRot.z, idx );
}

XMFLOAT3 Boundary::GetBBSize( int idx ) const
{
	return m_xmf3_BB_Size[idx];
}

void Boundary::SetBBSize( float x, float y, float z, int idx )
{
	m_xmf3_BB_Size[idx].x = x;
	m_xmf3_BB_Size[idx].y = y;
	m_xmf3_BB_Size[idx].z = z;
}

void Boundary::SetBBSize( XMFLOAT3 xmfBBSize, int idx )
{
	SetBBSize( xmfBBSize.x, xmfBBSize.y, xmfBBSize.z, idx );
}

XMFLOAT3 Boundary::GetBBPos( int idx ) const
{
	return XMFLOAT3( m_xmf4x4_BB_World[idx]._41, m_xmf4x4_BB_World[idx]._42, m_xmf4x4_BB_World[idx]._43 );
}

void Boundary::SetBBPos( float x, float y, float z, int idx )
{
	m_xmf4x4_BB_World[idx]._41 = x;
	m_xmf4x4_BB_World[idx]._42 = y;
	m_xmf4x4_BB_World[idx]._43 = z;
}

void Boundary::SetBBPos( XMFLOAT3 xmfPos, int idx )
{
	SetBBPos( xmfPos.x, xmfPos.y, xmfPos.z, idx );
}

XMFLOAT3 Boundary::GetBBLook( int idx ) const
{
	return ( Vector3::Normalize( XMFLOAT3( m_xmf4x4_BB_World[idx]._31, m_xmf4x4_BB_World[idx]._32, m_xmf4x4_BB_World[idx]._33 ) ) );
}

void Boundary::SetBBLook( float x, float y, float z, int idx )
{
	m_xmf4x4_BB_World[idx]._31 = x;
	m_xmf4x4_BB_World[idx]._32 = y;
	m_xmf4x4_BB_World[idx]._33 = z;
}

void Boundary::SetBBLook( XMFLOAT3 xmfLook, int idx )
{
	SetBBLook( xmfLook.x, xmfLook.y, xmfLook.z, idx );
}

XMFLOAT3 Boundary::GetBBUp( int idx ) const
{
	return ( Vector3::Normalize( XMFLOAT3( m_xmf4x4_BB_World[idx]._21, m_xmf4x4_BB_World[idx]._22, m_xmf4x4_BB_World[idx]._23 ) ) );
}

void Boundary::SetBBUp( float x, float y, float z, int idx )
{
	m_xmf4x4_BB_World[idx]._21 = x;
	m_xmf4x4_BB_World[idx]._22 = y;
	m_xmf4x4_BB_World[idx]._23 = z;
}

void Boundary::SetBBUp( XMFLOAT3 xmfUp, int idx )
{
	SetBBUp( xmfUp.x, xmfUp.y, xmfUp.z, idx );
}

XMFLOAT3 Boundary::GetBBRight( int idx ) const
{
	return ( Vector3::Normalize( XMFLOAT3( m_xmf4x4_BB_World[idx]._11, m_xmf4x4_BB_World[idx]._12, m_xmf4x4_BB_World[idx]._13 ) ) );
}

void Boundary::SetBBRight( float x, float y, float z, int idx )
{
	m_xmf4x4_BB_World[idx]._11 = x;
	m_xmf4x4_BB_World[idx]._12 = y;
	m_xmf4x4_BB_World[idx]._13 = z;
}

void Boundary::SetBBRight( XMFLOAT3 xmfRight, int idx )
{
	SetBBRight( xmfRight.x, xmfRight.y, xmfRight.z, idx );
}

XMFLOAT3 Boundary::GetBSPos( int idx ) const
{
	return XMFLOAT3( m_xmf3_BS[idx] );
}

void Boundary::SetBSPos( float x, float y, float z, int idx )
{
	m_xmf3_BS[idx].x = x;
	m_xmf3_BS[idx].y = y;
	m_xmf3_BS[idx].z = z;
}

void Boundary::SetBSPos( XMFLOAT3 xmfPos, int idx )
{
	SetBSPos( xmfPos.x, xmfPos.y, xmfPos.z, idx );
}

float Boundary::GetBSRad( int idx ) const
{
	return m_BS_Rad[idx];
}

void Boundary::SetBSRad( float rad, int idx )
{
	m_BS_Rad[idx] = rad;
}
