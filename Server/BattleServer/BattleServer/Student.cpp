#include "pch.h"
#include "Student.h"
#include "SmallProp.h"
#include "MediumProp.h"
#include "LargeProp.h"
#include "Character.h"
#include "extern.h"
#include "../../Protocol.h"

Student::Student()
{
	Initialize();
}

Student::~Student()
{
	if( m_CurForm != nullptr )
		delete m_CurForm;
	m_CurForm = nullptr;
}

void Student::Initialize()
{
	m_Hp = 100;
	m_MaxHp = 100;
	m_PenaltyPos = {};
	m_PenaltyTime = {};
	m_bPenaltyChecking = false;
	m_bInRad = false;
	m_isMovable = true;
	m_isDead = false;
	m_CurForm = nullptr;
	if( m_CharacterType != (int)CHARACTERS::NONE )
		m_CharacterType = (int)CHARACTERS::NONE;
	if( m_isReady )
		m_isReady = false;
	m_Role = ROLE_STUDENT;
}

void Student::Reset()
{
	m_CurForm = nullptr;
	m_isReady = false;
	m_isDead = false;
	m_CharacterType = (int)CHARACTERS::NONE; // 이거 
	m_keyW = false;
	m_keyA = false;
	m_keyS = false;
	m_keyD = false;
	m_keySpace = false;
	m_animJump = false;
	m_Hp = 100;
	m_MaxHp = 100;
	m_PenaltyPos = {};
	m_PenaltyTime = {};
	m_bPenaltyChecking = false;
	m_bInRad = false;
	m_isMovable = true;
	m_isReady = false;
	m_bTransformable = true;
	m_Role = ROLE_STUDENT;
}

bool Student::Update( float elapsedTime )
{
	if( m_CurForm == nullptr ) return false;

	if( m_animJump ){
		m_fTimeElapsedJump += elapsedTime;
		if( m_fTimeElapsedJump >= 700.0f ){
			m_animJump = false;
			m_fTimeElapsedJump = 0.0f;
		}
	}
	
	XMFLOAT3 force{};
	float forceScalar = 1.f;
	int count{ 0 };
	int check{ 0 };

	XMFLOAT3 look = m_CurForm->GetLook();
	XMFLOAT3 up = m_CurForm->GetUp();

	if( m_keyW ) {
		++count;
		++check;
	}
	if( m_keyS ) {
		++count;
		++check;
	}
	if( m_keyA ) {
		++count;
		--check;
	}
	if( m_keyD ) {
		++count;
		--check;
	}
	if( m_keySpace ){
		force = Vector3::Add( force, up, forceScalar );
		m_animJump = true;
		m_keySpace = false;
	} // 점프는 좀 더 생각해보자.

	if( count == 0 || count == 4 );
	else if( count == 1 || count == 3 )
		force = Vector3::Add( force, look, forceScalar );
	else //2이다
		if( check == 0 )
			force = Vector3::Add( force, look, forceScalar );

	Vector3::Normalize( force );
	if( !Vector3::IsZero( force ) ){
		float curFormForceAmountXZ{ m_CurForm->GetForceAmountXZ() };
		float curFormForceAmountY{ m_CurForm->GetForceAmountY() };
		force.x *= curFormForceAmountXZ;
		force.y *= curFormForceAmountY;
		force.z *= curFormForceAmountXZ;

		m_CurForm->AddForce( force, elapsedTime , false);
	}
	return m_CurForm->Update( elapsedTime, true );
}

void Student::Transform( short obj_type )
{
	if( !m_bTransformable )	return;

	if( SHARED_RESOURCE::g_boundaries.find( obj_type ) == SHARED_RESOURCE::g_boundaries.end() ) return;

	if( obj_type == -1 ){
		SetHP( STUDENT );
		m_CurForm = new Character( *m_CurForm, SHARED_RESOURCE::g_boundaries[obj_type] ); // 아직 캐릭터 바운딩 정보가 없어용..
	}
	else if( 0 <= obj_type && obj_type < 100 ){
		SetHP( SMALL );
		m_CurForm = new SmallProp( *m_CurForm, SHARED_RESOURCE::g_boundaries[obj_type] ); // 어떤 바운더리 쓸 지 알아야함. 맵에서 가져와?
	}
	else if( 100 <= obj_type && obj_type < 200 ){
		SetHP( MEDIUM );
		m_CurForm = new MediumProp( *m_CurForm, SHARED_RESOURCE::g_boundaries[obj_type] );
	}
	else if( 200 <= obj_type && obj_type < 300 ){
		SetHP( LARGE );
		m_CurForm = new LargeProp( *m_CurForm, SHARED_RESOURCE::g_boundaries[obj_type] );
	}
}

int Student::GetAnimType()
{
	if( m_animJump ){
		return ANIM_JUMP;
	}

	if( !m_keyA && !m_keyW && !m_keyS && !m_keyD && !m_keySpace )
		return ANIM_IDLE;
	else
		return ANIM_RUNNING;
}

void Student::SetHP( const int& type )
{
	float curHp{ (float)m_Hp };
	float curMaxHp{ (float)m_MaxHp };
	float hpRate{curHp / m_MaxHp}; // 현재 체력 비율

	SetMaxHP( type );
	m_Hp = GetMaxHP() * hpRate;
}

void Student::SetHP( const unsigned short& hp )
{
	m_Hp = hp;
}

unsigned short Student::GetHP() const
{
	return m_Hp;
}

void Student::SetMaxHP( const int& type )
{
	switch( type ){
	case STUDENT:
	case MEDIUM:
		m_MaxHp = 100;
		break;
	case SMALL:
		m_MaxHp = 40;
		break;
	case LARGE:
		m_MaxHp = 200;
		break;
	default:
		std::cout << "Wrong Type Error!" << std::endl;
		while( 1 );
	}
}

unsigned short Student::GetMaxHP() const
{
	return m_MaxHp;
}

void Student::SetPenaltyPos( const XMFLOAT3& pos )
{
	m_PenaltyPos = pos;
}

XMFLOAT3 Student::GetPenaltyPos()
{
	return m_PenaltyPos;
}

void Student::SetPenaltyTime( const size_t& tick )
{
	m_PenaltyTime = tick;
}

size_t Student::GetPenaltyTime()
{
	return m_PenaltyTime;
}

void Student::SetPenaltyCheck( bool isChecking )
{
	m_bPenaltyChecking = isChecking;
}

void Student::isInRad( const XMFLOAT3 curPos )
{
	float distance{ sqrt( pow( m_PenaltyPos.x - curPos.x, 2 ) + pow( m_PenaltyPos.y - curPos.y, 2 ) + pow( m_PenaltyPos.z - curPos.z, 2 ) ) };
	/*std::cout << "Penalty Position = {" << m_PenaltyPos.x << ", " << m_PenaltyPos.y << ", " << m_PenaltyPos.z << "}" << std::endl;
	std::cout << "패널티 이동반경: " << distance << std::endl;*/
	if( PENALTY_RADIUS < distance )
		m_bInRad = false;
	else m_bInRad = true;
}

bool Student::IsPenaltyChecking()
{
	return m_bPenaltyChecking;
}

bool Student::IsStopped()
{
	if( m_CurForm->IsStopped() )
		return true;
	return false;
}

bool Student::IsTransformalble()
{
	return m_bTransformable;
}

void Student::SetIsTransformable( bool trans )
{
	m_bTransformable = trans;
}
