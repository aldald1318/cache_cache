#include "pch.h"
#include "UDPSocket.h"

UDPSocket::~UDPSocket()
{
	closesocket( m_socket );
}

int UDPSocket::Bind( const SocketAddress& address )
{
	int result = ::bind( m_socket, &address.m_sockAddr, address.GetSize() );
	if( result != 0 ){
		SocketUtil::ReportError( "UDPSocket::Bind" );
		return WSAGetLastError();
	}
	return NO_ERROR;
}

int UDPSocket::WSASendTo( LPWSABUF lpBuf, DWORD dwBufCnt, LPDWORD lpNumBytesSent, DWORD lpFlags, SocketAddress& lpTo, int lpTolen, LPWSAOVERLAPPED lpOverlapped )
{
	if( this == nullptr ){
		std::cout << "nullptr references." << std::endl;
		return -1;
	}

	int bytesSent = ::WSASendTo( m_socket, lpBuf, dwBufCnt, lpNumBytesSent, lpFlags, &lpTo.m_sockAddr, lpTolen, lpOverlapped, NULL );
	if( bytesSent == SOCKET_ERROR ){
		int errNum = WSAGetLastError();
		if( errNum != WSA_IO_PENDING ){
			SocketUtil::ReportError( "UDPSocket::WSASendTo" );
			return errNum;
		}
	}
	else return bytesSent;
}

int UDPSocket::WSAReceiveFrom( LPWSABUF lpBuf, DWORD dwBufCnt, LPDWORD lpNumBytesRecvd, LPDWORD lpFlags, SocketAddress& lpFrom, LPINT lpFromlen, LPWSAOVERLAPPED lpOverlapped )
{
	int bytesRecvd = ::WSARecvFrom( m_socket, lpBuf, dwBufCnt, lpNumBytesRecvd, lpFlags, &lpFrom.m_sockAddr, lpFromlen, lpOverlapped, NULL );
	if( bytesRecvd == SOCKET_ERROR ){
		int errNum = WSAGetLastError();
		if( errNum != WSA_IO_PENDING ){
			SocketUtil::ReportError( "UDPSocket::WSAReceiveFrom" );
			return errNum;
		}
	}
	return bytesRecvd;
}
