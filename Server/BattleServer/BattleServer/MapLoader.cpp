#include "pch.h"
#include "MapLoader.h"
#include "Map.h"
#include "SpawnPosition.h"
#include "SmallProp.h"
#include "MediumProp.h"
#include "LargeProp.h"
#include "Boundary.h"
#include "extern.h"

MapLoader::MapLoader()
{
	Initialize();
}
MapLoader::~MapLoader()
{
	delete m_Binfo;
}

void MapLoader::Initialize()
{
	m_Binfo = nullptr;
}

void MapLoader::LoadBBs( std::string filepath )
{
	std::cout << "Start Loading BBs...\n";
	std::ifstream in( filepath );

	while( !in.eof() ){
		// if( in.eof() )
		// 	return;
		m_Binfo = new Boundary;

		in >> m_obj_type >> m_num_BB;
		m_Binfo->SetBB( m_num_BB );
		m_Binfo->SetObjType( m_obj_type );
		m_Binfo->SetNumOfBB( m_num_BB );

		for( int i = 0; i < m_num_BB; ++i )
		{
			in >> m_size.x >> m_size.y >> m_size.z >>
				m_pos.x >> m_pos.y >> m_pos.z >>
				m_look.x >> m_look.y >> m_look.z >>
				m_up.x >> m_up.y >> m_up.z >>
				m_right.x >> m_right.y >> m_right.z >>
				m_rot.x >> m_rot.y >> m_rot.z;

		/*	m_rot.x *= (-1);
			m_rot.y *= (-1);
			m_rot.z *= (-1);*/

			m_Binfo->SetBBSize( m_size, i );
			m_Binfo->SetBBPos( m_pos, i );
			m_Binfo->SetBBLook( m_look, i );
			m_Binfo->SetBBUp( m_up, i );
			m_Binfo->SetBBRight( m_right, i );
			m_Binfo->SetBBRot( m_rot, i );
		}

		//for( int i = 0; i < m_num_BB; ++i ){
		//	std::cout << m_Binfo->GetBBPos( i ).x << std::endl;
		//	std::cout << m_Binfo->GetBBPos( i ).y << std::endl;
		//	std::cout << m_Binfo->GetBBPos( i ).z << std::endl;
		//}
		// BS 읽어 저장.
		in >> m_num_BS;
		m_Binfo->SetBS( m_num_BS );
		m_Binfo->SetNumOfBS( m_num_BS );
		for( int i = 0; i < m_num_BS; ++i )
		{
			in >> m_bs_pos.x >> m_bs_pos.y >> m_bs_pos.z >> m_bs_rad;
			m_Binfo->SetBSPos( m_bs_pos, i );
			m_Binfo->SetBSRad( m_bs_rad, i );
		}

		std::string tmp;
		in >> tmp;

		// g_boundaries에 넣기.
		SHARED_RESOURCE::g_boundaries[m_obj_type] = m_Binfo;
	}
	std::cout << "Loading BBs Finished.\n";
}

void MapLoader::LoadMap( Map& map, SpawnPosition& spawn, std::string filepath )
{
	// txt 읽어 전역변수 map에 저장하기
	std::cout << "Start Map Loading.." << std::endl;
	std::ifstream in( filepath );
	int studentNum = 1;
	for ( std::string line; std::getline( in, line );) {
		if ( line.size() == 0 ) continue;

		std::string name;
		std::stringstream lineParser( line );
		lineParser >> name;
		if ( name == "Masterpf" || name == "Studentpf" ) {
			XMFLOAT4 spawnpos;
			lineParser >> spawnpos.x >> spawnpos.y >> spawnpos.z >> spawnpos.w;

			if ( name == "Masterpf" )
				spawn.m_spawnPos[SpawnPosition::SPAWN_MASTER] = spawnpos;
			else
				spawn.m_spawnPos[studentNum++] = spawnpos;

			continue;
		}
		else {
			lineParser >> m_info.pos.x >> m_info.pos.y >> m_info.pos.z
				>> m_info.rot.x >> m_info.rot.y >> m_info.rot.z
				>> m_info.isChangable >> m_info.isFixed >> m_info.isCollidable
				>> m_info.obj_type >> m_info.obj_id;

			/*m_info.rot.x *= (-1);
			m_info.rot.y *= (-1);
			m_info.rot.z *= (-1);*/

			if (SHARED_RESOURCE::g_boundaries.find( m_info.obj_type ) == SHARED_RESOURCE::g_boundaries.end()) continue;

			if ( 300 <= m_info.obj_type && m_info.obj_type < 400 )
				continue; // 꽃이나 풀은 서버가 알 필요 없다.

			if ( 0 <= m_info.obj_type && m_info.obj_type < 100 ) {
				if ( m_info.pos.y > ABOVE_1M )
					map.m_ObjList[Map::ABOVE].emplace_back( new SmallProp( m_info, SHARED_RESOURCE::g_boundaries[m_info.obj_type] ) );
				else
					map.m_ObjList[Map::NOTFIXED].emplace_back( new SmallProp( m_info, SHARED_RESOURCE::g_boundaries[m_info.obj_type] ) );
			}

			else if ( 100 <= m_info.obj_type && m_info.obj_type < 200 ) {
				if ( m_info.pos.y > ABOVE_1M )
					map.m_ObjList[Map::ABOVE].emplace_back( new MediumProp( m_info, SHARED_RESOURCE::g_boundaries[m_info.obj_type] ) );
				else
					map.m_ObjList[Map::NOTFIXED].emplace_back( new MediumProp( m_info, SHARED_RESOURCE::g_boundaries[m_info.obj_type] ) );
			}

			else if ( 200 <= m_info.obj_type && m_info.obj_type < 300 ) {
				if ( m_info.pos.y > ABOVE_1M )
					map.m_ObjList[Map::ABOVE].emplace_back( new LargeProp( m_info, SHARED_RESOURCE::g_boundaries[m_info.obj_type] ) );
				else
					map.m_ObjList[Map::NOTFIXED].emplace_back( new LargeProp( m_info, SHARED_RESOURCE::g_boundaries[m_info.obj_type] ) );
			}

			else if ( 400 <= m_info.obj_type && m_info.obj_type < 700 ) {
				if ( m_info.pos.y > ABOVE_1M )
					map.m_ObjList[Map::ABOVE].emplace_back( new LargeProp( m_info, SHARED_RESOURCE::g_boundaries[m_info.obj_type] ) );
				else
					map.m_ObjList[Map::FIXED].emplace_back( new LargeProp( m_info, SHARED_RESOURCE::g_boundaries[m_info.obj_type] ) ); // 건물, 가판대 ( 변신 불가능하나 충돌함 )
			}
		}
	}
	std::cout << "Map Loading Finished! \n" << std::endl;
}

void MapLoader::LoadMapBySector( Map& map, SpawnPosition& spawn, std::string filepath )
{
	// txt 읽어 전역변수 map에 저장하기
	std::cout << "Start Map Loading.." << std::endl;
	std::ifstream in( filepath );
	int studentNum = 1;
	for ( std::string line; std::getline( in, line );) {
		if ( line.size() == 0 ) continue;

		std::string name;
		std::stringstream lineParser( line );
		lineParser >> name;
		if ( name == "Masterpf" || name == "Studentpf" ) {
			XMFLOAT4 spawnpos;
			lineParser >> spawnpos.x >> spawnpos.y >> spawnpos.z >> spawnpos.w;

			if ( name == "Masterpf" )
				spawn.m_spawnPos[SpawnPosition::SPAWN_MASTER] = spawnpos;
			else
				spawn.m_spawnPos[studentNum++] = spawnpos;

			continue;
		}
		else {
			lineParser >> m_info.pos.x >> m_info.pos.y >> m_info.pos.z
				>> m_info.rot.x >> m_info.rot.y >> m_info.rot.z
				>> m_info.isChangable >> m_info.isFixed >> m_info.isCollidable
				>> m_info.obj_type >> m_info.obj_id;
			
			if ( SHARED_RESOURCE::g_boundaries.find( m_info.obj_type ) == SHARED_RESOURCE::g_boundaries.end() ) continue;

			if ( 300 <= m_info.obj_type && m_info.obj_type < 400 )
				continue; // 꽃이나 풀은 서버가 알 필요 없다.

			int a = ( int )floor( m_info.pos.x / SIZE_OF_SECTOR );
			int b = ( int )floor( m_info.pos.z / SIZE_OF_SECTOR );
			if ( a >= NUM_OF_SECTOR ) a = (NUM_OF_SECTOR - 1) * 2;
			else if ( a < 0 ) a = 0;
			else a *= 2;

			if ( b >= NUM_OF_SECTOR ) b = (NUM_OF_SECTOR - 1) * 2;
			else if ( b < 0 ) b = 0;
			else b *= 2;

			int c = ( int )floor( ( m_info.pos.x - SIZE_OF_SECTOR_HALF ) / SIZE_OF_SECTOR );
			int d = ( int )floor( ( m_info.pos.z - SIZE_OF_SECTOR_HALF ) / SIZE_OF_SECTOR );
			if ( c >= NUM_OF_SECTOR - 1 ) c = 0;
			else if ( c < 0 ) c = 0;
			else c = c * 2 + 1;

			if ( d >= NUM_OF_SECTOR - 1 ) d = 0;
			else if ( d < 0 ) d = 0;
			else d = d * 2 + 1;


			if ( 0 <= m_info.obj_type && m_info.obj_type < 100 ) {
				if ( m_info.pos.y > ABOVE_1M )
					map.m_SectorObjList[a][b][Map::ABOVE].emplace_back( new SmallProp( m_info, SHARED_RESOURCE::g_boundaries[m_info.obj_type] ) );
				else
				{
					map.m_SectorObjList[a][b][Map::NOTFIXED].emplace_back( new SmallProp( m_info, SHARED_RESOURCE::g_boundaries[m_info.obj_type] ) );
					if ( c != 0 && d != 0 )
						map.m_SectorObjList[c][d][Map::NOTFIXED].emplace_back( map.m_SectorObjList[a][b][Map::NOTFIXED].back() );
					if ( c != 0 )
					{
						map.m_SectorObjList[c][b][Map::NOTFIXED].emplace_back( map.m_SectorObjList[a][b][Map::NOTFIXED].back() );
					}
					if ( d != 0 )
					{
						map.m_SectorObjList[a][d][Map::NOTFIXED].emplace_back( map.m_SectorObjList[a][b][Map::NOTFIXED].back() );
					}
				}
			}

			else if ( 100 <= m_info.obj_type && m_info.obj_type < 200 ) {
				if ( m_info.pos.y > ABOVE_1M )
					map.m_SectorObjList[a][b][Map::ABOVE].emplace_back( new MediumProp( m_info, SHARED_RESOURCE::g_boundaries[m_info.obj_type] ) );
				else
				{
					map.m_SectorObjList[a][b][Map::NOTFIXED].emplace_back( new MediumProp( m_info, SHARED_RESOURCE::g_boundaries[m_info.obj_type] ) );
					if ( c != 0 && d != 0 )
						map.m_SectorObjList[c][d][Map::NOTFIXED].emplace_back( map.m_SectorObjList[a][b][Map::NOTFIXED].back() );
					if ( c != 0 )
					{
						map.m_SectorObjList[c][b][Map::NOTFIXED].emplace_back( map.m_SectorObjList[a][b][Map::NOTFIXED].back() );
					}
					if ( d != 0 )
					{
						map.m_SectorObjList[a][d][Map::NOTFIXED].emplace_back( map.m_SectorObjList[a][b][Map::NOTFIXED].back() );
					}
				}
			}

			else if ( 200 <= m_info.obj_type && m_info.obj_type < 300 ) {
				if ( m_info.pos.y > ABOVE_1M )
					map.m_SectorObjList[a][b][Map::ABOVE].emplace_back( new LargeProp( m_info, SHARED_RESOURCE::g_boundaries[m_info.obj_type] ) );
				else
				{
					map.m_SectorObjList[a][b][Map::NOTFIXED].emplace_back( new LargeProp( m_info, SHARED_RESOURCE::g_boundaries[m_info.obj_type] ) );
					if ( c != 0 && d != 0 )
						map.m_SectorObjList[c][d][Map::NOTFIXED].emplace_back( map.m_SectorObjList[a][b][Map::NOTFIXED].back() );
					if ( c != 0 )
					{
						map.m_SectorObjList[c][b][Map::NOTFIXED].emplace_back( map.m_SectorObjList[a][b][Map::NOTFIXED].back() );
					}
					if ( d != 0 )
					{
						map.m_SectorObjList[a][d][Map::NOTFIXED].emplace_back( map.m_SectorObjList[a][b][Map::NOTFIXED].back() );
					}
				}
			}

			else if ( 400 <= m_info.obj_type && m_info.obj_type < 700 ) {
				if ( m_info.pos.y > ABOVE_1M )
					map.m_SectorObjList[a][b][Map::ABOVE].emplace_back( new LargeProp( m_info, SHARED_RESOURCE::g_boundaries[m_info.obj_type] ) );
				else
				{
					map.m_SectorObjList[a][b][Map::FIXED].emplace_back( new LargeProp( m_info, SHARED_RESOURCE::g_boundaries[m_info.obj_type] ) ); // 건물, 가판대 ( 변신 불가능하나 충돌함 )
					if ( c != 0 && d != 0 )
						map.m_SectorObjList[c][d][Map::FIXED].emplace_back( map.m_SectorObjList[a][b][Map::FIXED].back() );
					if ( c != 0 )
					{
						map.m_SectorObjList[c][b][Map::FIXED].emplace_back( map.m_SectorObjList[a][b][Map::FIXED].back() );
					}
					if ( d != 0 )
					{
						map.m_SectorObjList[a][d][Map::FIXED].emplace_back( map.m_SectorObjList[a][b][Map::FIXED].back() );
					}
				}
			}
		}
	}
	std::cout << "Map Loading Finished! \n" << std::endl;
}