#pragma once
#define DATA_START_LOCATION 3
class OutputMemoryStream
{
public:
	OutputMemoryStream();
	~OutputMemoryStream();
	
	void Write( const char* data, unsigned short dataSize );
	void Flush(int destID);
	unsigned int GetCapacity(){ return m_capacity; }
	unsigned int GetLength() { return m_head; }
	char* GetBufferPtr(){ return m_buffer; }
	int GetNWrite(){ return m_nWrite; }


private:
	char* m_buffer;
	ofstream m_pOutStream;
	int m_nWrite;
	unsigned short m_head{};
	unsigned short m_capacity;

	std::mutex m_bufferLock;

	void ReallocBuffer( unsigned int byteSize );
};

