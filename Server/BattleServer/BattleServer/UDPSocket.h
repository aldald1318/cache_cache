#pragma once
class SocketAddress;
class UDPSocket
{
public:
	~UDPSocket();

	int Bind( const SocketAddress& address );

	// WSASendTo 래핑 함수. 첫 번째 인자 SOCKET은 멤버 사용, 마지막 인자 COMPLETION_ROUTINE 사용안함.
	int WSASendTo( LPWSABUF lpBuf, DWORD dwBufCnt, LPDWORD lpNumBytesSent, DWORD lpFlags, SocketAddress& lpTo, int lpTolen, LPWSAOVERLAPPED lpOverlapped );

	// WSARecvFrom 래핑 함수. 첫 번째 인자 SOCKET은 멤버 사용, 마지막 인자 COMPLETION_ROUTINE 사용안함.
	int WSAReceiveFrom( LPWSABUF lpBuf, DWORD dwBufCnt, LPDWORD lpNumBytesRecvd, LPDWORD lpFlags, SocketAddress& lpFrom, LPINT lpFromlen, LPWSAOVERLAPPED lpOverlapped );


public:
	SOCKET GetSocket(){ return m_socket; }

private:
	friend class SocketUtil;
	UDPSocket( SOCKET socket ) : m_socket( socket ){}
	SOCKET m_socket;
};

typedef std::shared_ptr<UDPSocket> UDPSocketPtr;

