#include "pch.h"
#include "SmallProp.h"
#include "Boundary.h"

void SmallProp::Initialize()
{
	m_maxVel = 0.275f;
	m_mass = 10.f;
	m_fricCoef = 0.7f;
	m_forceAmountXZ = 1.f; // �ӽ�
	m_forceAmountY = 0.5f;
}
