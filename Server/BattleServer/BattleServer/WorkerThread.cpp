#include "pch.h"
#include "../../Protocol.h"
#include "WorkerThread.h"
#include "BattleServer.h"
#include "DB_Handler.h"
#include "Room.h"
#include "Globals.h"
#include "extern.h"

void WorkerThread::InitThread()
{
	m_ServerAddr = BattleServer::GetInstance()->GetServerAddr();
	m_AddrLen = sizeof( m_ServerAddr );
	m_UDPRecvSocket = BattleServer::GetInstance()->GetUDPSocket();
	//m_DBHandler = new DB_Handler;
	mythread = std::thread( [&]() {WorkerThread::ProcThread(); } );
}

void WorkerThread::ProcThread()
{
	while( true ) {
		DWORD num_byte;
		ULONG key;
		PULONG p_key = &key;
		WSAOVERLAPPED* p_over;

		BOOL bSuccessful = GetQueuedCompletionStatus( SHARED_RESOURCE::g_iocp, &num_byte, (PULONG_PTR)p_key, &p_over, INFINITE );
		if( !bSuccessful ){
			int err_no = GetLastError();
			BattleServer::GetInstance()->error_display( "GQCS Error!", err_no );
		}

		SOCKET clientSock;
		if( key == EVENT_KEY );
		else if( key == MATCH_MAKING_SERVER_KEY ){
			std::cout << "[LOG] MatchMakingServerKey Returned!" << std::endl;
			clientSock = SHARED_RESOURCE::g_clients[key]->socket->GetSocket();
		}
		else if( key != UDP_SOCKET_KEY )
		{
			if( SHARED_RESOURCE::g_clients[key]->socket == nullptr ){
#ifdef LOG_ON
				std::cout << "[LOG] " << key << " - socket was nullptr" << std::endl;
#endif // LOG_ON
				continue;
			}

			clientSock = SHARED_RESOURCE::g_clients[key]->socket->GetSocket();

			if( num_byte == 0 )
			{
				// 클라이언트와 연결끊김.
				DisconnectClient( key, clientSock );
				continue;
			}
		}
		else{


			clientSock = m_UDPRecvSocket;
		}
		OVER_EX* over_ex = reinterpret_cast<OVER_EX*>(p_over);

		if( EV_TCP_RECV == over_ex->ev_type )
		{
			char* buf = SHARED_RESOURCE::g_clients[key]->recv_over.net_buf;
			unsigned psize = SHARED_RESOURCE::g_clients[key]->curr_packet_size;
			unsigned pr_size = SHARED_RESOURCE::g_clients[key]->prev_packet_data;
			message msg{ -1 };
			while( num_byte > 0 )
			{
				if( 0 == psize ) psize = (BYTE)buf[0];
				if( num_byte + pr_size >= psize )
				{
					// 지금 패킷 완성 가능
					unsigned char packet[MAX_BUFFER];
					memcpy( packet, SHARED_RESOURCE::g_clients[key]->packet_buf, pr_size );
					memcpy( packet + pr_size, buf, psize - pr_size );
					msg = ProcPacket( key, packet );


					if( msg.type != NO_MSG ){
						int room_ID = SHARED_RESOURCE::g_clients[key]->room_id;
						SHARED_RESOURCE::g_rooms[room_ID]->PushMsg( msg );
					}

					num_byte -= psize - pr_size;
					buf += psize - pr_size;
					psize = 0; pr_size = 0;
				}
				else
				{
					ATOMIC::g_clients_lock.lock();
					memcpy( SHARED_RESOURCE::g_clients[key]->packet_buf + pr_size, buf, num_byte );
					ATOMIC::g_clients_lock.unlock();
					pr_size += num_byte;
					num_byte = 0;
				}
			}
			SHARED_RESOURCE::g_clients[key]->curr_packet_size = psize;
			SHARED_RESOURCE::g_clients[key]->prev_packet_data = pr_size;

			DWORD flags = 0;
			memset( &over_ex->over, 0x00, sizeof( WSAOVERLAPPED ) );
			memset( &msg, 0, sizeof( message ) );
			over_ex->ev_type = EV_TCP_RECV;

			int retval = WSARecv( clientSock, over_ex->wsabuf, 1, 0, &flags, &over_ex->over, 0 );
			if( retval == SOCKET_ERROR ) {
				if( WSAGetLastError() != ERROR_IO_PENDING )
				{
					std::cout << "Recv Error \n";
					BattleServer::GetInstance()->error_display( "WSARecv Error!", WSAGetLastError() );
					//while ( 1 );
				}
			}
		}

		else if( EV_UDP_RECV == over_ex->ev_type ) {
			message msg{ -1 };
			DWORD flags = 0;

			msg = ProcPacket( key, over_ex->net_buf );
			if( msg.type != NO_MSG )
			{
				unsigned int room_ID{ SHARED_RESOURCE::g_clients[msg.id]->room_id };
				SHARED_RESOURCE::g_rooms[room_ID]->PushMsg( msg );
			}

			memset( &over_ex->over, 0, sizeof( WSAOVERLAPPED ) );
			memset( &msg, 0, sizeof( message ) );

			over_ex->ev_type = EV_UDP_RECV;

			while( true ){
				int retval = WSARecvFrom( clientSock, over_ex->wsabuf, 1, 0, &flags, (SOCKADDR*)&m_UDPClientAddr, &m_AddrLen, &over_ex->over, 0 );
				if( retval == SOCKET_ERROR ){
					if( WSAGetLastError() != ERROR_IO_PENDING ){
						if( WSAGetLastError() != WSAECONNRESET )
							BattleServer::GetInstance()->error_display( "WSARecvFromError!", WSAGetLastError() );
					}
					else
						break;
				}
			}
		}

		else if( EV_SEND == over_ex->ev_type )
		{
			delete over_ex;
		}
		else if( EV_UPDATE == over_ex->ev_type )
		{
			//std::cout << this_thread::get_id() << std::endl;

			int room_ID = *(int*)over_ex->net_buf;
			SHARED_RESOURCE::g_rooms[room_ID]->Update();
			delete over_ex;
		}
		else if( EV_TICK == over_ex->ev_type )
		{
			int room_ID = *(int*)over_ex->net_buf;
			SHARED_RESOURCE::g_rooms[room_ID]->SendLeftTimePacket();
#ifdef GAME_RULE_ON
			SHARED_RESOURCE::g_rooms[room_ID]->CheckGameState();
#endif
			delete over_ex;
		}
		else if( EV_FLUSH_MSG == over_ex->ev_type )
		{
			int room_ID = *(int*)over_ex->net_buf;
			SHARED_RESOURCE::g_rooms[room_ID]->FlushSendMsg();
			delete over_ex;
		}
		else if( EV_PENALTY_CHECK == over_ex->ev_type ){
#ifdef LOG_ON
			std::cout << "[LOG] EV_PENALTY_CHECK CALLED ID - " << key << std::endl;
#endif
			int room_ID = *(int*)over_ex->net_buf;
			SHARED_RESOURCE::g_rooms[room_ID]->CheckPenalty( key );
			delete over_ex;
		}

		else if( EV_PENALTY_BEGIN == over_ex->ev_type ){
			std::cout << "[LOG] EV_PENALTY_BEGIN CALLED ID - " << key << std::endl;
			int room_ID = *(int*)over_ex->net_buf;
			SHARED_RESOURCE::g_rooms[room_ID]->ApplyPenalty( key );
			delete over_ex;
		}

		else if( EV_PENALTY_END == over_ex->ev_type ){
			std::cout << "[LOG] EV_PENALTY_END CALLED ID - " << key << std::endl;
			int room_ID = *(int*)over_ex->net_buf;
			SHARED_RESOURCE::g_rooms[room_ID]->UnapplyPenalty( key );
			delete over_ex;
		}

		else if( EV_MOVE_ENABLE == over_ex->ev_type ){
			std::cout << "[LOG] EV_MOVE_ENABLE CALLED ID - " << key << std::endl;
			int room_ID = *(int*)over_ex->net_buf;
			SHARED_RESOURCE::g_rooms[room_ID]->MakeMove( key ); // Make Player(key) can move
			delete over_ex;
		}

		else if( EV_MAKE_MOVE_DISABLE == over_ex->ev_type ){
			std::cout << "[LOG] EV_MAKE_MOVE_DISABLE CALLED ID - " << key << std::endl;
			int room_ID = *(int*)over_ex->net_buf;
			SHARED_RESOURCE::g_rooms[room_ID]->MakeNoMove( key ); // Make Player(key) cant move
			delete over_ex;
		}

		else if( EV_UPDATE_DB == over_ex->ev_type ){
			std::cout << "[LOG] EV_UPDATE_DATABASE" << std::endl;
			BattleServer::GetInstance()->SendUpdatedUserInfoPacket( key, SHARED_RESOURCE::g_clients[key]->mmr, SHARED_RESOURCE::g_clients[key]->killCnt,
				SHARED_RESOURCE::g_clients[key]->WRate, SHARED_RESOURCE::g_clients[key]->totalGameCnt, SHARED_RESOURCE::g_clients[key]->winningGameCnt );

			delete over_ex;
		}

		else if( EV_RESET_ROOM == over_ex->ev_type ){
			std::cout << "[LOG] EV_RESET_ROOM" << std::endl;
			int room_ID = *(int*)over_ex->net_buf;
			SHARED_RESOURCE::g_rooms[room_ID]->Reset();
			delete over_ex;
		}

		else if( EV_FULL_WORLD_UPDATE_END == over_ex->ev_type ) {
			std::cout << "[LOG] EV_FULL_WORLD_UPDATE_END" << std::endl;
			int room_ID = *(int*)over_ex->net_buf;
			SHARED_RESOURCE::g_rooms[room_ID]->FullWorldUpdateEnd();
			delete over_ex;
		}

		else if( EV_THUNDERBOLT_SET_NOT_USE == over_ex->ev_type ) {
			std::cout << "[LOG] EV_THUNDERBOLT_SET_NOT_USE" << std::endl;

			short buf[2];
			int room_ID = *(int*)over_ex->net_buf;
			memcpy( buf, &room_ID, sizeof( short ) * 2 );
			SHARED_RESOURCE::g_rooms[(int)buf[0]]->ThunderboltSetNotUse( (int)buf[1] );
			delete over_ex;
		}

		// ======== 다른 이벤트 추가 =========
		// 대기방에서 레디, 시작, 맵 선택 처리
		// 게임룸에서는? 

		else
		{
			std::cout << "[LOG] Unknown Event Type: " << over_ex->ev_type << std::endl;
		}
	}
}

void WorkerThread::JoinThread()
{
	//delete m_DBHandler;
	mythread.join();
}

message WorkerThread::ProcPacket( int id, void* buf )
{
	char* packet = reinterpret_cast<char*>(buf);
	message packet2msg;
	memset( &packet2msg, 0x00, sizeof( message ) );
	packet2msg.id = id;
	packet2msg.type = NO_MSG;

	switch( packet[1] )
	{
	case MB_REQUEST_ROOM:	//auto match
	{
		std::cout << "[LOG] recv mb_packet_request_room from Matchmaking Server." << std::endl;
		int roomNo{};
		for( int i = AUTO_ROOM_START; i < MAX_AUTO_ROOM; ++i ){
			ATOMIC::g_autoMatchRoomNo_lock.lock();
			if( SHARED_RESOURCE::g_autoMatchRoomNo[i] == -1 ){
				SHARED_RESOURCE::g_autoMatchRoomNo[i] = i + 1;
				roomNo = SHARED_RESOURCE::g_autoMatchRoomNo[i];
				ATOMIC::g_autoMatchRoomNo_lock.unlock();
				break;
			}
			ATOMIC::g_autoMatchRoomNo_lock.unlock();
		}

		Room* new_room = new Room( roomNo );
		SHARED_RESOURCE::g_rooms[roomNo] = new_room;
		++ATOMIC::g_numOfAutoRoom;
		++ATOMIC::g_numOfRoom;
		BattleServer::GetInstance()->SendAutoRoomReadyPacket( id, roomNo );
		break;
	}
	case CB_REQUSET_ROOM:	//manual match
	{
		std::cout << "[LOG] recv cb_packet_request_room from ID - " << id << std::endl;
		int roomNo{};
		for( int i = 0; i < MAX_ROOM; ++i ){
			ATOMIC::g_manualMatchRoomNo_lock.lock();
			if( SHARED_RESOURCE::g_manualMatchRoomNo[i] == -1 ){
				SHARED_RESOURCE::g_manualMatchRoomNo[i] = i + 1;
				roomNo = SHARED_RESOURCE::g_manualMatchRoomNo[i];
				ATOMIC::g_manualMatchRoomNo_lock.unlock();
				break;
			}
			ATOMIC::g_manualMatchRoomNo_lock.unlock();
		}
		wstring roomName{ reinterpret_cast<cb_packet_request_manual_room*>(packet)->room_title };
		Room* new_room = new Room( roomNo, roomName );
		new_room->MakeRoomManual();
		SHARED_RESOURCE::g_rooms[roomNo] = new_room;
		++ATOMIC::g_NumOfManualRoom;
		++ATOMIC::g_numOfRoom;
		BattleServer::GetInstance()->SendManualRoomReadyPacket( id, roomNo );
		break;
	}
	case CB_LOGIN:
	{
		//여기서 DB서 받아오자
		cb_packet_login* loginPacket = reinterpret_cast<cb_packet_login*>(packet);
		std::cout << "[LOG] recv cb_packet_login from ID - " << id << std::endl;

		/*char id_str[10]{};
		memcpy( &id_str, packet + 3, 10 );
		char match_type = packet[2];

		int mmr{}, catchCnt{};
		float winningRate{};
		int totalGameCnt{};
		int winningGameCnt{};*/

		/*if( false == GetPlayerInfo( id_str, mmr, catchCnt, winningRate, totalGameCnt, winningGameCnt ) ){
			std::cout << "[LOG] Login Denied - DataBase has no ID: " << id_str << "'s Informations.\n";
			BattleServer::GetInstance()->SendAccessDenyPacket( id );
		}*/

		// 배틀서버에 로그인 불가한 경우는?
		// 없는 아이디로 로그인 요청이 올 수 있는가?

		ATOMIC::g_dbInfo_lock.lock();
		memcpy( &SHARED_RESOURCE::g_clients[id]->id_str, &loginPacket->name, sizeof( char ) * 10 );
		SHARED_RESOURCE::g_clients[id]->mmr = loginPacket->mmr;
		SHARED_RESOURCE::g_clients[id]->killCnt = loginPacket->catched_student_count;
		SHARED_RESOURCE::g_clients[id]->WRate = loginPacket->winning_rate;
		SHARED_RESOURCE::g_clients[id]->totalGameCnt = loginPacket->total_game_cnt;
		SHARED_RESOURCE::g_clients[id]->winningGameCnt = loginPacket->winning_game_cnt;
		ATOMIC::g_dbInfo_lock.unlock(); 

		BattleServer::GetInstance()->SendAccessOKPacket( id, loginPacket->is_automatch );

		break;
	}
	case CB_JOIN:
	{
		// 클라이언트가 보낸 방 번호로 클라이언트 넣어줘야(생성?)함.
		std::cout << "[LOG] recv cb_packet_join from ID - " << id << std::endl;

		// 방 번호 알아내기
		int roomNo{};
		memcpy( &roomNo, packet + 2, sizeof( int ) );

		// 20200429 임원택
		// 없어진 방에 또 입장했을때, 튕기는 버그수정
		if( !SHARED_RESOURCE::g_rooms[roomNo] ) break;

		// 예외처리하기 없는 방번호에 대한 참여 요청시 BC_JOINDENIED 보내자
		if( SHARED_RESOURCE::g_rooms.find( roomNo ) == SHARED_RESOURCE::g_rooms.end() ){
			BattleServer::GetInstance()->SendRoomJoinDenied( id, ROOM_DENY_CODE_ROOM_DOSENT_EXIST );
			break;
		}

		// 게임 시작한 방도 못들어감
		if( !SHARED_RESOURCE::g_rooms[roomNo]->IsRoomEnterable() ){
			BattleServer::GetInstance()->SendRoomJoinDenied( id, ROOM_DENY_CODE_ROOM_HAS_STARTED );
			break;
		}

		// 방에 클라 연결하기
		bool isJoined{ false };
		int slot{};
		isJoined = SHARED_RESOURCE::g_rooms[roomNo]->EnterRoom( id, packet[6], &slot );
		if( isJoined ){
			BattleServer::GetInstance()->SendRoomJoinSuccess( id, slot ); // 몇 P인지 보내주자(요청한 클라한테 보내는 거다)
			int map_type{ SHARED_RESOURCE::g_rooms[roomNo]->GetMapNo() };
			BattleServer::GetInstance()->SendMapTypePacket( id, map_type );
		}
		// 풀방이어서 입장 불가
		else
			BattleServer::GetInstance()->SendRoomJoinDenied( id, ROOM_DENY_CODE_ROOM_IS_FULL );

		break;
	}

	case CB_ROOM_LEAVE:
	{
		std::cout << "[LOG] recv cb_packet_room_leave from ID - " << id << std::endl;
		// 방 퇴장 처리.(방장 나갈 때에 대한 처리도 해야함.)
		int roomNo = SHARED_RESOURCE::g_clients[id]->room_id;
		bool isManager = packet[2];
		SHARED_RESOURCE::g_rooms[roomNo]->LeaveRoom( id );

		// Room에 남은 인원이 없으면, 그 Room은 삭제함.
		// 클라에서 CB_REQUEST_ROOM_LIST 보내면, 갱신된 룸 리스트 보내짐.
		// 클라 룸 리스트 새로고침 기능은 CB_REQUEST_ROOM_LIST 보내는 것으로 해야함.
		if( SHARED_RESOURCE::g_rooms[roomNo]->IsEmpty() ){
			EraseRoom( roomNo );
		}
		break;
	}

	case CB_REQUEST_ROOM_LIST:
	{
		std::cout << "[LOG] recv cb_packet_request_room_list from ID - " << id << std::endl;
		PTC_Room rooms[5]{};
		size_t cnt{};
		if( ATOMIC::g_NumOfManualRoom != 0 )
		{
			for( int i = 0; i < MAX_ROOM; ++i )
			{
				ATOMIC::g_manualMatchRoomNo_lock.lock();
				int tempManualRoomNo = SHARED_RESOURCE::g_manualMatchRoomNo[i];
				ATOMIC::g_manualMatchRoomNo_lock.unlock();
				if( SHARED_RESOURCE::g_manualMatchRoomNo[i] != -1 )
				{
					// PTC_Room에 5개를 채울 때마다 룸리스트 패킷을 보냅니다.
					if( SHARED_RESOURCE::g_rooms[tempManualRoomNo] != nullptr )
					{
						rooms[cnt].id = SHARED_RESOURCE::g_rooms[tempManualRoomNo]->GetRoomNo(); +
							wcscpy( rooms[cnt].name, SHARED_RESOURCE::g_rooms[tempManualRoomNo]->GetRoomName().c_str() );
						rooms[cnt].participant = SHARED_RESOURCE::g_rooms[tempManualRoomNo]->GetcurPlayerNum();

						if( SHARED_RESOURCE::g_rooms[tempManualRoomNo]->IsGameStarted()
							|| SHARED_RESOURCE::g_rooms[tempManualRoomNo]->GetcurPlayerNum() >= MAX_PLAYER )
							rooms[cnt].joinable = false;
						else
							rooms[cnt].joinable = true;

						++cnt;
						if( cnt % 5 == 0 ){
							BattleServer::GetInstance()->SendRoomListPacket( id, ATOMIC::g_NumOfManualRoom, rooms );
							cnt = 0;
							ZeroMemory( &rooms, sizeof( rooms ) );
						}
					}
				}
			}
			// 5개를 다 못채웠으면, 빈 값을 의미하는 값들을 넣어 나머지를 채워 보냅니다.
			if( cnt % 5 != 0 ){
				for( int i = cnt % 5; i < 5; ++i ){
					rooms[i].id = -1;
					rooms[i].participant = 0;
				}
				BattleServer::GetInstance()->SendRoomListPacket( id, ATOMIC::g_NumOfManualRoom, rooms );
			}
		}
		else{	// 매뉴얼 룸이 없을 경우에는 PTC_Room 전체를 빈 값을 넣어 보냅니다.
			for( int i = 0; i < 5; ++i ){
				rooms[i].id = -1;
				rooms[i].participant = 0;
			}
			BattleServer::GetInstance()->SendRoomListPacket( id, ATOMIC::g_NumOfManualRoom, rooms );
		}
		break;
	}

	case CB_SELECT_CHARACTER:
	{
#ifdef LOG_ON
		std::cout << "[LOG] recv cb_packet_select_character from ID - " << id << std::endl;
#endif
		packet2msg.type = CB_SELECT_CHARACTER;
		// 2Bytes 캐릭터 배열을 msg에 넣어서 room에 전달해야함.
		// 워커스레드에서 문자열에서 int로 변경? 또는 enum class로 바꿔서 룸에 넘기자.

		/* 수정내용 20200318 임원택*/
		// 패킷중 int / char와 같이 자료형이 변경될때 (혹은 int형으로 인해)
		// 다음 포인터 이동이 안되는거로 판단됨
		// 그래서 다음과 같이 클라에서 패킷을 받는 방식으로 변경함
		cb_packet_select_character* select_packet = reinterpret_cast<cb_packet_select_character*>(packet);
		char character_type[3];
		strcpy_s( character_type, select_packet->character );

		if( !strcmp( character_type, CHARACTER_DRUID ) )
			packet2msg.vec.x = (int)CHARACTERS::DR;
		else if( !strcmp( character_type, CHARACTER_BAIRD ) )
			packet2msg.vec.x = (int)CHARACTERS::BA;
		else if( !strcmp( character_type, CHARACTER_FEMALE_PEASANT ) )
			packet2msg.vec.x = (int)CHARACTERS::FP;
		else if( !strcmp( character_type, CHARACTER_MALE_PEASANT ) )
			packet2msg.vec.x = (int)CHARACTERS::MP;
		else if( !strcmp( character_type, CHARACTER_SORCERER ) )
			packet2msg.vec.x = (int)CHARACTERS::SO;
		else{
			std::cout << "[LOG] Invalid Character Type Error!" << std::endl;
			while( 1 );
		}
		break;
	}

	case CB_CANCEL_SELECT_CHARACTER:
	{
#ifdef LOG_ON
		std::cout << "[LOG] recv cb_packet_cancel_select_character from ID - " << id << std::endl;
#endif
		packet2msg.type = CB_CANCEL_SELECT_CHARACTER;

		cb_packet_select_character* select_packet = reinterpret_cast<cb_packet_select_character*>(packet);
		char character_type[3];
		strcpy_s( character_type, select_packet->character );

		if( !strcmp( character_type, CHARACTER_DRUID ) )
			packet2msg.vec.x = (int)CHARACTERS::DR;
		else if( !strcmp( character_type, CHARACTER_BAIRD ) )
			packet2msg.vec.x = (int)CHARACTERS::BA;
		else if( !strcmp( character_type, CHARACTER_FEMALE_PEASANT ) )
			packet2msg.vec.x = (int)CHARACTERS::FP;
		else if( !strcmp( character_type, CHARACTER_MALE_PEASANT ) )
			packet2msg.vec.x = (int)CHARACTERS::MP;
		else if( !strcmp( character_type, CHARACTER_SORCERER ) )
			packet2msg.vec.x = (int)CHARACTERS::SO;
		else{
			std::cout << "[LOG] Invalid Character Type Error!" << std::endl;
			while( 1 );
		}
		break;
	}

	case CB_CHAT:
	{
		char chat[256];
		memcpy( &chat, packet + 2, (BYTE)packet[0] - 2 );

		SHARED_RESOURCE::g_rooms[SHARED_RESOURCE::g_clients[id]->room_id]->PushChatMsg( id, chat );
		break;
	}

	case CB_KEY_W_UP:
	{
		packet2msg.type = CB_KEY_W_UP;
		break;
	}
	case CB_KEY_W_DOWN:
	{
		packet2msg.type = CB_KEY_W_DOWN;
		break;
	}
	case CB_KEY_S_UP:
	{
		packet2msg.type = CB_KEY_S_UP;
		break;
	}
	case CB_KEY_S_DOWN:
	{
		packet2msg.type = CB_KEY_S_DOWN;
		break;
	}

	case CB_KEY_A_UP:
	{
		packet2msg.type = CB_KEY_A_UP;
		break;
	}
	case CB_KEY_A_DOWN:
	{
		packet2msg.type = CB_KEY_A_DOWN;
		break;
	}

	case CB_KEY_D_UP:
	{
		packet2msg.type = CB_KEY_D_UP;
		break;
	}
	case CB_KEY_D_DOWN:
	{
		packet2msg.type = CB_KEY_D_DOWN;
		break;
	}

	case CB_JUMP:
	{
#ifdef LOG_ON
		std::cout << "[LOG] " << id << " recv cb_packet_jump" << std::endl;
#endif
		packet2msg.type = CB_JUMP;
		break;
	}

	case CB_LOOK_VECTOR:
	{
		//std::cout << "look vector recved! " << std::endl;
		cb_packet_look_vector* look_packet = reinterpret_cast<cb_packet_look_vector*>(packet);

		if( look_packet == nullptr ){
			packet2msg.type = NO_MSG;
			break;
		}

		packet2msg.id = look_packet->id;
		packet2msg.type = CB_LOOK_VECTOR;
		packet2msg.vec.x = look_packet->look.x;
		packet2msg.vec.y = look_packet->look.y;
		packet2msg.vec.z = look_packet->look.z;
		break;
	}

	case CB_START:
	{
		std::cout << "[LOG] " << id << "start_packet received\n";
		packet2msg.type = CB_START;
		break;
	}

	case CB_READY:
	{
		std::cout << "[LOG] " << id << " - recv ready packet" << std::endl;
		packet2msg.type = CB_READY;
		break;
	}

	case CB_MAP_TYPE:
	{
		std::cout << "[LOG] " << id << " recv - map_type packet" << std::endl;
		packet2msg.type = CB_MAP_TYPE;
		packet2msg.vec.x = packet[2];
		break;
	}

	case CB_TRANSFORM:
	{
#ifdef LOG_ON
		std::cout << "[LOG] " << id << " recv cb_transform packet\n";
#endif
		packet2msg.type = CB_TRANSFORM;
		packet2msg.vec.x = MAKEWORD( packet[2], packet[3] );
		break;
	}

	case CB_THUNDER_BOLT:
	{
#ifdef LOG_ON
		std::cout << "[LOG] " << id << " recv CB_THUNDERBOLT" << std::endl;
#endif
		packet2msg.type = CB_THUNDER_BOLT;
		memcpy( &packet2msg.vec, &packet[2], sizeof( PTC_Vector ) );
		break;
	}

	case CB_TEST_MINUS_TIME:
	{
		packet2msg.type = CB_TEST_MINUS_TIME;
		break;
	}

	case CB_TEST_PLUS_TIME:
	{
		packet2msg.type = CB_TEST_PLUS_TIME;
		break;
	}

	case CB_TEST_ZERO_HP:
	{
		packet2msg.type = CB_TEST_ZERO_HP;
		break;
	}

	// packet2msg 내용 채워서 반환하자.
	default:
		std::cout << "[LOG] Invalid Packet Type Error!\n";
		while( true );
	}
	return packet2msg;
}

//bool WorkerThread::GetPlayerInfo( const char* id_str, int& mmr, int& catchCnt, float& Wrate, int& totalCnt, int& winCnt )
//{
//	if( m_DBHandler->GetUserInfo( id_str, mmr, catchCnt, Wrate, totalCnt, winCnt ) )
//		return true;
//	else
//		return false;
//}

void WorkerThread::DisconnectClient( int clientID, SOCKET client )
{
	// 소속된 룸 번호 확인, 룸 퇴장 처리.
	int roomID = SHARED_RESOURCE::g_clients[clientID]->room_id;

	if( roomID != -1 ){
		SHARED_RESOURCE::g_rooms[roomID]->LeaveRoom( clientID );

		if( SHARED_RESOURCE::g_rooms[roomID]->IsEmpty() )
		{
			EraseRoom( roomID );
		}
	}
	// 클라이언트와 연결 종료.
	ATOMIC::g_clients_lock.lock();
	SHARED_RESOURCE::g_clients[clientID]->isConnected = false;
	ZeroMemory( SHARED_RESOURCE::g_clients[clientID], sizeof( SOCKETINFO ) );

	SHARED_RESOURCE::g_clients[clientID]->socket = nullptr;
	ATOMIC::g_clients_lock.unlock();
	//closesocket( client );
	std::cout << "[LOG] Client ID - " << clientID << " disconnected. roomNO - " << roomID << std::endl;
}

void WorkerThread::EraseRoom( int& roomID )
{
	ATOMIC::g_room_lock.lock();
	SHARED_RESOURCE::g_rooms.erase( roomID );
	ATOMIC::g_room_lock.unlock();

	if( roomID < AUTO_ROOM_START ){
		for( int i = 0; i < MAX_ROOM; ++i ){
			ATOMIC::g_manualMatchRoomNo_lock.lock();
			if( SHARED_RESOURCE::g_manualMatchRoomNo[i] == roomID ){
				SHARED_RESOURCE::g_manualMatchRoomNo[i] = -1;
			}
			ATOMIC::g_manualMatchRoomNo_lock.unlock();
		}
		--ATOMIC::g_NumOfManualRoom;
		std::cout << "[LOG] ManualMatch RoomNo - " << roomID << " erased!" << std::endl;

	}
	else{
		for( int i = AUTO_ROOM_START; i < MAX_AUTO_ROOM; ++i ){
			ATOMIC::g_autoMatchRoomNo_lock.lock();
			if( SHARED_RESOURCE::g_autoMatchRoomNo[i] == roomID ){
				SHARED_RESOURCE::g_autoMatchRoomNo[i] = -1;
			}
			ATOMIC::g_autoMatchRoomNo_lock.unlock();
		}
		--ATOMIC::g_numOfAutoRoom;
		std::cout << "[LOG] AutoMatch RoomNo - " << roomID << " erased!" << std::endl;
	}
	--ATOMIC::g_numOfRoom;
}
