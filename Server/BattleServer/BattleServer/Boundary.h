#pragma once
#include "DX_Math.h"

constexpr int NONETYPE = -100;

class Boundary
{
public:
	Boundary()  noexcept;
	Boundary( int numBB, int numBS ) noexcept;
	Boundary( const Boundary& other ) noexcept;
	Boundary& operator=( const Boundary& other );
	~Boundary();

private:
	// BB_Tool 저장 순서
	// obj_type / num_BB / {size, pos, look, up, right, rot}*num_BB /
	// num_BS / {pos, rad}*num_BS
	int m_obj_type;
	int m_numOfBB;
	XMFLOAT4X4* m_xmf4x4_BB_World;
	XMFLOAT3* m_xmf3_BB_Size;
	XMFLOAT3* m_xmf3_BB_Rot;

	int m_numOfBS;
	XMFLOAT3* m_xmf3_BS;
	float* m_BS_Rad;	//반지름
public:

	void SetBB( int numBB );
	void SetBS( int numBS );

	void BBRotate( float pitch, float yaw, float roll, int idx );
	void BBRotate( XMFLOAT3 eulerAngle, int idx );
	void BBRotate( XMFLOAT4 quaternion, int idx );

	void BoundaryMove( float x, float y, float z );
	void BoundaryMove( XMFLOAT3 distance );

	XMFLOAT4X4 GetWorldMatrix(int idx) const;
	void SetWorldMatrix( const XMFLOAT4X4& mat, int idx );

	int GetObjType() const;
	void SetObjType( int obj_type );
	int GetNumOfBB() const;
	void SetNumOfBB( int num_bb );
	int GetNumOfBS() const;
	void SetNumOfBS( int num_bs );

	XMFLOAT3 GetBBRot( int idx ) const;
	void SetBBRot( float x, float y, float z, int idx );
	void SetBBRot( XMFLOAT3 xmfRot, int idx );

	XMFLOAT3 GetBBSize( int idx ) const;
	void SetBBSize( float x, float y, float z, int idx );
	void SetBBSize( XMFLOAT3 xmfBBSize, int idx );

	XMFLOAT3 GetBBPos(int idx) const;
	void SetBBPos( float x, float y, float z, int idx );
	void SetBBPos( XMFLOAT3 xmfPos, int idx );

	XMFLOAT3 GetBBLook(int idx) const;
	void SetBBLook( float x, float y, float z, int idx );
	void SetBBLook( XMFLOAT3 xmfLook, int idx );

	XMFLOAT3 GetBBUp( int idx ) const;
	void SetBBUp( float x, float y, float z, int idx );
	void SetBBUp( XMFLOAT3 xmfUp, int idx );

	XMFLOAT3 GetBBRight( int idx ) const;
	void SetBBRight( float x, float y, float z, int idx );
	void SetBBRight( XMFLOAT3 xmfRight, int idx );

	XMFLOAT3 GetBSPos( int idx ) const;
	void SetBSPos( float x, float y, float z, int idx );
	void SetBSPos( XMFLOAT3 xmfPos, int idx );

	float GetBSRad( int idx ) const;
	void SetBSRad( float rad, int idx );
};

