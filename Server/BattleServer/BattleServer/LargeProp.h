#pragma once
#include "Prop.h"
class LargeProp final :
	public Prop
{
public:
	using Prop::Prop;
	LargeProp() = default;
	LargeProp( const obj_info& info, const Boundary* bb_info ) : Prop( info, bb_info ){ Initialize(); };
	LargeProp( Object& obj, const Boundary* bb_info ) : Prop( obj, bb_info ){ Initialize(); };
	void Initialize();

	LargeProp* clone() const { return new LargeProp( *this ); }
};

