#pragma once
#include "Object.h"
#include "Globals.h"
class Prop :
	public Object
{
public:
	Prop() = default;
	Prop( const obj_info& info, const Boundary* bb_info );// noexcept;
	Prop( Object& curForm, const Boundary* new_bb_info );// noexcept;

public:
	bool Update( float elapsedTime, bool isPlayer ) override;

public:
	int GetType() const;
	void SetID( int obj_id );
	void SetType(int obj_type );

protected:
	int m_type;				// 무슨 오브젝트인지
	float m_preVelSize{};
};
