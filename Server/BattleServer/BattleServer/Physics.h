#pragma once
#include "Globals.h"
class Object;
class Boundary;

enum AXIS { AXIS_Y, AXIS_X, AXIS_Z, AXIS_NUM };

struct BBPoints {
	XMFLOAT3 point[8];
};

struct BBTwoPoint {
	XMFLOAT3 point[2];
};

struct ProcCollisionInfo {
	bool is_collision;
	XMFLOAT3 collisionNormalVector;
	XMFLOAT3 relativeVelocity;
	float overlapDepth;
	bool is_sequential;
};

class Physics
{
public:
	Physics() {};

public:
	ProcCollisionInfo CheckCollision( const Object* a, const Object* b )const;
	bool CheckBulletObjCollisionPoint( const XMFLOAT3 bullet_pos, const Object* obj )const;
	bool CheckBulletObjCollisionLine( const XMFLOAT3 bullet_pos_start, const XMFLOAT3 bullet_pos_end, const Object* obj )const;
	bool CheckBulletCharacterCollision( const XMFLOAT3 bullet_pos, const Object* obj )const;
	
	void ProcBulletImpulse( Object* a, XMFLOAT3 bullet_pos_start, XMFLOAT3 bullet_pos_end );

	// notfix notfix
	void ProcCollisionImpulse( Object* a, Object* b, XMFLOAT3 collisionNormal, XMFLOAT3 relativeVelocity, bool is_sequential );
	// notfix fixed
	void ProcCollisionImpulse( Object* a, XMFLOAT3 collisionNormal, XMFLOAT3 relativeVelocity, bool is_sequential );

	// player notfix
	void SplitObject( Object* a, Object* b, XMFLOAT3 collisionNormal, float overlapDepth, bool is_sequential, bool player );
	// notfix notfix
	void SplitObject( Object* a, Object* b, XMFLOAT3 collisionNormal, float overlapDepth ,bool is_sequential );
	// notfix fixed
	void SplitObject( Object* a, XMFLOAT3 collisionNormal, float overlapDepth, bool is_sequential );

	// N == not fix
	// F == fix
	void ProcCollisionNF( Object* a, Object* b );

private:
	//////////////////////////
	ProcCollisionInfo CheckCollisionOBB_OBB( const Object* a, const Object* b )const;
	bool CheckCollisionOBB_Point( const XMFLOAT3 bullet_pos, const Object* obj )const;
	bool CheckCollisionOBB_Ray( const XMFLOAT3 bullet_pos_start, const XMFLOAT3 bullet_pos_end, const Object* obj )const;
	bool CheckCollisionSphere( const XMFLOAT3& aPos, const XMFLOAT3& bPos, const float aRad, const float bRad ) const;
	bool CheckCollisionSphere_Ray( const XMFLOAT3& bullet_pos_start, const XMFLOAT3& bullet_pos_end, const XMFLOAT3& objPos, const float objRad ) const;

	void GetPoints( const Boundary* boundary, BBPoints* points, int num )const;
	const XMFLOAT3 GetPointTo( const XMFLOAT3 to, XMFLOAT3 pos, float distance )const;

	//점 두개, 투영될 벡터 --> 투영된 두 점 사이의 거리
	float GetProjectionPointDistance( const XMFLOAT3 a, const XMFLOAT3 b, const XMFLOAT3 vector )const;
	XMFLOAT3 GetProjectionPoint( const XMFLOAT3& point, const XMFLOAT3& vector )const;
};
