#pragma once
#include "DX_Math.h"
class Object;
class Master;
enum TYPE{ STUDENT, SMALL, MEDIUM, LARGE, END };
class Player
{
public:
	Player();
	Player( const Player& rhs );
	Player& operator=( const Player& rhs ) noexcept;
	virtual~Player();

	Master& GetMaster();
public:
	void Initialize();
	virtual void Reset();

	virtual bool Update( float elapsedTime ) = 0;

public:
	void SetCurForm(const char& role, const int& map_no, const int& sp);
	Object* GetCurForm();		
	
	void SetRole( char );
	char GetRole();
	
	void Enter();
	void Leave();
	
	bool GetSlotStatus();
	
	void SetID(const int& id);
	int GetID();

	void SetReady();
	bool GetReady();

	void SetID_STR( char* name );
	char* GetID_STR();

	void SetMMR( int mmr );
	int GetMMR();

	void SetKillCnt( int cnt );
	int GetKillCnt();

	void SetWinRate( float rate );
	float GetWinRate();
	void IncreaseKillCnt(){ m_killCnt++; }

	void SetTotalCnt( int totalCnt );
	int GetTotalCnt();

	void SetWinCnt( int winCnt );
	int GetWinCnt();

	void SetCharacterType( short c_type );
	short GetCharacterType();

	bool IsMovable();
	void SetIsMovable( bool movable );

	bool IsDead();
	void SetDead();

public:
	virtual int GetAnimType() = 0;
	int GetPrevAnimType();
	void SetPrevAnimType( int animType );

public:
	void SetKeyW( bool input );
	void SetKeyS( bool input );
	void SetKeyA( bool input );
	void SetKeyD( bool input );
	void SetKeySpace( bool input );

	bool GetKeyW();
	bool GetKeyS();
	bool GetKeyA();
	bool GetKeyD();
	bool GetKeySpace();

protected:
	int m_id;			// g_clients key값 그대로 쓰면 됨.
	Object* m_CurForm;	// 현재 모습
	char m_Role;		// 역할
	bool m_isEmpty;		// 빈 플레이어 슬롯
	bool m_isReady;
	bool m_isMovable;	// 이동 가능여부 ( 라운드 준비 시간 때 스승 못움직이게 )
	bool m_isDead;

	char m_idStr[10];
	int m_mmr;
	int m_killCnt;
	float m_winningRate;
	int m_totalGameCnt;
	int m_winningGameCnt;

	bool m_keyW = false;
	bool m_keyA = false;
	bool m_keyS = false;
	bool m_keyD = false;
	bool m_keySpace = false;
	bool m_animJump = false;

	short m_CharacterType;
	char m_prevAnimType;

	float m_fTimeElapsedJump{};
};

