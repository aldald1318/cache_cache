#include "pch.h"
#include "Prop.h"
#include "Boundary.h"

Prop::Prop( const obj_info& info, const Boundary* bb_info ) //noexcept
{
	m_xmf4x4World = Matrix4x4::Identity();
	m_preVelSize = 0.0f;

	Rotate( info.rot.x, info.rot.y, info.rot.z );
	SetPosition( info.pos );
	SetPrePosition( info.pos );
	m_UniqueID = info.obj_id;
	m_type = info.obj_type;
	m_xmfVel = XMFLOAT3{ 0.f, 0.f ,0.f };
	m_xmfAcc = XMFLOAT3{ 0.f, 0.f, 0.f };

	int numofBB = bb_info->GetNumOfBB();
	int numofBS = bb_info->GetNumOfBS();

	delete m_boundaries;
	m_boundaries = new Boundary( numofBB, numofBS );
	m_boundaries->SetObjType( bb_info->GetObjType() );

	// Object의 각도로 m_boudnadries 회전한다.
	// 1. Object의 pitch, yaw, roll로 쿼터니언을 구한다.
	XMVECTOR quat1 = XMQuaternionRotationRollPitchYaw( XMConvertToRadians( info.rot.x ),
		XMConvertToRadians( info.rot.y ), XMConvertToRadians( info.rot.z ) );

	for( int i = 0; i < numofBB; ++i )
	{
		// Bounding Box의 로컬 행렬을 만든다.
		XMFLOAT4X4 local = Matrix4x4::Identity();

		// bb_info 의 pos 로 이동, rot으로 회전한다.

		XMFLOAT3 rot = bb_info->GetBBRot( i );
		XMVECTOR quat2 = XMQuaternionRotationRollPitchYaw( XMConvertToRadians( rot.x ),
			XMConvertToRadians( rot.y ), XMConvertToRadians( rot.z ) );


		XMFLOAT4X4 tmp{};
		QuatToMatrix( &Vector4::XMVectorToFloat4( quat2 ), &tmp );

		local = Matrix4x4::Multiply( local, tmp );
		local._41 = bb_info->GetBBPos( i ).x;
		local._42 = bb_info->GetBBPos( i ).y;
		local._43 = bb_info->GetBBPos( i ).z;

		//m_boundaries->SetWorldMatrix( Matrix4x4::Multiply( local, m_xmf4x4World ), i );

		// quat1 으로 회전
		QuatToMatrix( &Vector4::XMVectorToFloat4( quat1 ), &tmp );
		local = Matrix4x4::Multiply( local, tmp );
		// OBJ 위치로 이동
		local._41 += GetPosition().x;
		local._42 += GetPosition().y;
		local._43 += GetPosition().z;

		m_boundaries->SetWorldMatrix( local, i );

		m_boundaries->SetBBSize( bb_info->GetBBSize( i ), i );
	}
	for( int i = 0; i < numofBS; ++i )
	{
		XMFLOAT4X4 q2m{};
		QuatToMatrix( &Vector4::XMVectorToFloat4( quat1 ), &q2m );

		XMFLOAT4X4 mat = Matrix4x4::Identity(); // BS 로컬 행렬

		mat._41 = bb_info->GetBSPos( i ).x;
		mat._42 = bb_info->GetBSPos( i ).y;
		mat._43 = bb_info->GetBSPos( i ).z;
		mat = Matrix4x4::Multiply( mat, q2m ); // 회전 적용

		mat = Matrix4x4::Multiply( mat, m_xmf4x4World );
		m_boundaries->SetBSPos( mat._41, mat._42, mat._43, i );
		m_boundaries->SetBSRad( bb_info->GetBSRad( i ), i );
	}
}

// 변신할 때 사용하려고 만든 생성자임.
Prop::Prop( Object& curForm, const Boundary* new_bb_info ) //noexcept
{
	m_xmf4x4World = Matrix4x4::Identity();
	m_xmf4x4World = Matrix4x4::Multiply( m_xmf4x4World, curForm.GetMatrix() ); // 이러면 위치와 보는 방향은 유지가 됨.
	m_preVelSize = sqrtf( pow( curForm.GetVelocity().x, 2 ) + pow( curForm.GetVelocity().z, 2 ) );

	m_xmfAcc = curForm.GetAccerleration();
	m_xmfVel = curForm.GetVelocity();
	m_UniqueID = NULL; // 플레이어 id넣기?
	m_type = new_bb_info->GetObjType();

	// 새로운 Bounding 정보를 적용한다.
	int numofBB = new_bb_info->GetNumOfBB();
	int numofBS = new_bb_info->GetNumOfBS();

	delete& curForm;
	delete m_boundaries;

	m_boundaries = new Boundary( numofBB, numofBS );
	m_boundaries->SetObjType( m_type );

	FXMVECTOR objRotQuat{ QuatFromMatrix( GetMatrix() ) }; // 오브젝트의 회전 쿼터니언이다.
	XMVECTOR resultQuat{};
	for( int i = 0; i < numofBB; ++i )
	{
		m_boundaries->SetBBPos( new_bb_info->GetBBPos( i ), i ); // 로컬 위치로 바운딩 박스 이동
		m_boundaries->SetBBRot( new_bb_info->GetBBRot( i ), i ); // 로컬 회전 설정

		XMFLOAT3 bbRot{ m_boundaries->GetBBRot( i ) };
		FXMVECTOR bbRotationQuat{	// BB의 회전 쿼터니언이다.
			XMQuaternionRotationRollPitchYaw(
				XMConvertToRadians( bbRot.x ),
				XMConvertToRadians( bbRot.y ),
				XMConvertToRadians( bbRot.z ) ) };


		// 쿼터니언 곱셈을 한다.
		// 결과 회전 쿼터니언을 행렬로 변환한다.
		XMFLOAT4X4 resultQuatToMat{};
		resultQuat = XMQuaternionMultiply( bbRotationQuat, objRotQuat );
		QuatToMatrix( &Vector4::XMVectorToFloat4( resultQuat ), &resultQuatToMat );

		XMFLOAT3 bbPos{ m_boundaries->GetBBPos( i ) };
		resultQuatToMat._41 += bbPos.x;
		resultQuatToMat._42 += bbPos.y;
		resultQuatToMat._43 += bbPos.z;

		m_boundaries->SetBBSize( new_bb_info->GetBBSize( i ), i ); // 바운딩 박스 크기 설정
		m_boundaries->SetWorldMatrix( resultQuatToMat, i );
		m_boundaries->SetBBPos( Vector3::Add( m_boundaries->GetBBPos( i ), GetPosition() ), i );
	}
	for( int i = 0; i < numofBS; ++i )
	{
		XMFLOAT4X4 rot{};
		QuatToMatrix( &Vector4::XMVectorToFloat4( resultQuat ), &rot );

		XMFLOAT4X4 mat = Matrix4x4::Identity(); // BS 로컬 행렬

		mat._41 = new_bb_info->GetBSPos( i ).x;
		mat._42 = new_bb_info->GetBSPos( i ).y;
		mat._43 = new_bb_info->GetBSPos( i ).z;
		mat = Matrix4x4::Multiply( mat, rot ); // 회전 적용

		mat = Matrix4x4::Multiply( mat, m_xmf4x4World );
		m_boundaries->SetBSPos( mat._41, mat._42, mat._43, i );
		m_boundaries->SetBSRad( new_bb_info->GetBSRad( i ), i );
	}
}

bool Prop::Update( float elapsedTime, bool isPlayer )
{
	m_xmf3PrePosition = GetPosition();
	m_preVelSize = sqrtf( pow( m_xmfVel.x, 2 ) + pow( m_xmfVel.z, 2 ) );

	float nForce{ GRAVITY * m_mass };			//수직항력
	float frictionSize = nForce * m_fricCoef;	//마찰력

	//direction
	XMFLOAT3 normalizedVel = Vector3::Normalize( m_xmfVel );

	//friction force
	float frictionX = -normalizedVel.x * frictionSize;
	float frictionZ = -normalizedVel.z * frictionSize;
	//float frictionY = -normalizedVel.y * 

	//friction acc
	XMFLOAT3 fricAcc{};
	if( m_isApplyGravity )
		fricAcc = { frictionX / m_mass, -GRAVITY / pow( elapsedTime, 2 ) , frictionZ / m_mass };
	else
		fricAcc = { frictionX / m_mass, 0, frictionZ / m_mass };

	//update velocity by friction force
	XMFLOAT3 newVel{ m_xmfVel.x + fricAcc.x * elapsedTime, m_xmfVel.y - GRAVITY * elapsedTime, m_xmfVel.z + fricAcc.z * elapsedTime };


	if( newVel.x * m_xmfVel.x <= FLT_EPSILON )
		m_xmfVel.x = 0.f;
	else
		m_xmfVel.x = newVel.x;

	if( newVel.z * m_xmfVel.z <= FLT_EPSILON )
		m_xmfVel.z = 0.f;
	else
		m_xmfVel.z = newVel.z;

	m_xmfVel.y = newVel.y;


	//std::cout << "Velocity - x = " << m_xmfVel.x << ", z = "<< m_xmfVel.z << ", y ="<< m_xmfVel.y << std::endl;


	float velSize = sqrtf( pow( m_xmfVel.x, 2 ) + pow( m_xmfVel.z, 2 ) );
	//std::cout.flush();
	if( velSize > m_maxVel )
	{
		m_xmfVel.x = m_xmfVel.x / velSize * m_maxVel;
		m_xmfVel.z = m_xmfVel.z / velSize * m_maxVel;
		m_xmfVel.y = m_xmfVel.y / velSize * m_maxVel;
		//velSize = m_maxVel;
	}

	float maxY = 50.0f;

	/*if( m_xmf4x4World._42 > FLT_EPSILON )
		m_xmfVel.y -= GRAVITY * elapsedTime;*/

	m_xmf4x4World._41 += m_xmfVel.x * elapsedTime;
	m_xmf4x4World._42 += m_xmfVel.y * elapsedTime;
	m_xmf4x4World._43 += m_xmfVel.z * elapsedTime;

	if( m_xmf4x4World._42 < FLT_EPSILON ){
		m_xmf4x4World._42 = 0.1f;
		m_xmfVel.y = 0.f;
	}

	if( isPlayer ){
	/*	XMVECTOR v = XMQuaternionRotationAxis( XMLoadFloat3( &GetRight() ), XMConvertToRadians( 30 ) );
		XMVECTOR u = QuatFromMatrix( GetMatrix() );
		v = XMQuaternionMultiply( v, u );
		XMFLOAT4X4 mat{};
		QuatToMatrix( &Vector4::XMVectorToFloat4( v ), &mat );
		mat._41 = m_xmf4x4World._41;
		mat._42 = m_xmf4x4World._42;
		mat._43 = m_xmf4x4World._43;
		m_xmf4x4World = mat;*/



		if( m_xmf4x4World._42 >= maxY )
			m_xmf4x4World._42 = maxY;
	}

	XMFLOAT3 change = Vector3::Subtract( GetPosition(), m_xmf3PrePosition );
	if( Vector3::IsZero( change ) )
		return false;
	else
	{
		// rotation은 아직 안생기므로 패스!
		m_boundaries->BoundaryMove( change );
		return true;
	}
}

int Prop::GetType() const
{
	return m_type;
}

void Prop::SetID( int obj_id )
{
	m_UniqueID = obj_id;
}

void Prop::SetType( int obj_type )
{
	m_type = obj_type;
}
