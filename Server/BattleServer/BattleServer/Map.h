#pragma once
#include "Object.h"

class Map
{
public:
	enum ObjListType { FIXED, NOTFIXED, ABOVE, ID_COUNT };

public:
	Map() = default;
	~Map();

	void LoadMap( int mapNo );
	void LoadSectorMap( int mapNo );

public:
	
	std::vector<Object*> m_ObjList[ID_COUNT];	//obj

	std::array<std::array<std::list<Object*>[ID_COUNT], NUM_OF_SECTOR * 2 - 1>, NUM_OF_SECTOR * 2 - 1> m_SectorObjList;
};
