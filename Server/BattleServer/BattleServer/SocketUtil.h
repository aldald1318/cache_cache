#pragma once
#include "TCPSocket.h"
#include "UDPSocket.h"

enum SocketAddressFamily{
	INET = AF_INET,
	INET6 = AF_INET6
};

class SocketUtil
{
public:
	static bool StaticInit();
	static void CleanUp();

	static void ReportError( const char* operationDesc );

	static UDPSocketPtr CreateUDPSocket( SocketAddressFamily family );
	static TCPSocketPtr CreateTCPSocket( SocketAddressFamily family );
};

