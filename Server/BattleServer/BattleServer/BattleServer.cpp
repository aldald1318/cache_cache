#include "pch.h"
#include "BattleServer.h"
#include "ThreadHandler.h"
#include "DB_Handler.h"
#include "extern.h"
#include "../../Protocol.h"

BattleServer::BattleServer()
{
	Initialize();
}

BattleServer::~BattleServer()
{
	m_ThreadHandler->JoinThreads();

	for( int i = 0; i < MAX_CLIENTS; ++i ){
		delete 	SHARED_RESOURCE::g_clients[i];
		SHARED_RESOURCE::g_clients[i] = nullptr;
	}
	m_sockUtil.CleanUp();
}

void BattleServer::Initialize()
{
	//m_UserID = 2;	///0번은 mm server, 1번은 udp 소켓

	// 매뉴얼, 오토 룸 슬롯 -1로 초기화
	for( int i = 0; i < MAX_ROOM; ++i )
		SHARED_RESOURCE::g_manualMatchRoomNo[i] = -1;

	for( int i = AUTO_ROOM_START; i < MAX_AUTO_ROOM; ++i )
		SHARED_RESOURCE::g_autoMatchRoomNo[i] = - 1;
	// 여기까지 룸 슬롯 초기화입니다.

	std::wcout.imbue( std::locale( "korean" ) );


	// refactor
	if( !m_sockUtil.StaticInit() ) exit( 1 );

	//생성은 여기서
	SHARED_RESOURCE::g_iocp = CreateIoCompletionPort( INVALID_HANDLE_VALUE, NULL, NULL, 0 );

	m_listenSocket = m_sockUtil.CreateTCPSocket( SocketAddressFamily::INET );

	SocketAddress serverAddr{ INADDR_ANY, BATTLE_SERVER_PORT };

	m_listenSocket->Bind( serverAddr );
	m_listenSocket->Listen();

	m_UDPSocket = m_sockUtil.CreateUDPSocket( SocketAddressFamily::INET );

	SocketAddress UDPAddr{ INADDR_ANY, BATTLE_SERVER_UDP_PORT };
	m_UDPSocket->Bind( UDPAddr );


	///UDP
	DWORD flags = 0;


	m_udpOver.wsabuf[0].len = MAX_BUFFER;
	m_udpOver.wsabuf[0].buf = m_udpOver.net_buf;
	m_udpOver.ev_type = EV_UDP_RECV;


	CreateIoCompletionPort( (HANDLE)m_UDPSocket->GetSocket(), SHARED_RESOURCE::g_iocp, UDP_SOCKET_KEY, 0 );


	memset( &m_udpOver.over, 0, sizeof( WSAOVERLAPPED ) );
	int addrlen{ m_UDPClientAddr.GetSize() };
	m_UDPSocket->WSAReceiveFrom( m_udpOver.wsabuf, 1, NULL, &flags, m_UDPClientAddr, &addrlen, &m_udpOver.over );
}

void BattleServer::AcceptMMServer()
{
	SocketAddress matchMakingServerAddr{};
	TCPSocketPtr clientSocket{};
	DWORD flags{};

	clientSocket = m_listenSocket->Accept( matchMakingServerAddr );

	SOCKETINFO* matchMakingServer = new SOCKETINFO;
	matchMakingServer->user_id = 0;	///mm server == 0
	matchMakingServer->socket = clientSocket;

	matchMakingServer->recv_over.wsabuf[0].len = MAX_BUFFER;
	matchMakingServer->recv_over.wsabuf[0].buf = matchMakingServer->recv_over.net_buf;

	matchMakingServer->recv_over.ev_type = EV_TCP_RECV;
	matchMakingServer->isConnected = true;
	matchMakingServer->curr_packet_size = 0;
	matchMakingServer->prev_packet_data = 0;

	SHARED_RESOURCE::g_clients[0] = matchMakingServer;

	CreateIoCompletionPort( reinterpret_cast<HANDLE>(clientSocket->GetSocket()), SHARED_RESOURCE::g_iocp, MATCH_MAKING_SERVER_KEY, 0 );

	memset( &SHARED_RESOURCE::g_clients[MATCH_MAKING_SERVER_KEY]->recv_over.over, 0, sizeof( WSAOVERLAPPED ) );

	clientSocket->WSAReceive( SHARED_RESOURCE::g_clients[MATCH_MAKING_SERVER_KEY]->recv_over.wsabuf, 1, NULL, &flags,
		&SHARED_RESOURCE::g_clients[MATCH_MAKING_SERVER_KEY]->recv_over.over );

	std::cout << "MMServer Connected" << std::endl;
}

void BattleServer::Run()
{
	m_ThreadHandler = new ThreadHandler;
	m_ThreadHandler->CreateThreads();

	SocketAddress clientAddress{};
	TCPSocketPtr clientSocket{};
	DWORD flags{};

	while( true ) {
		clientSocket = m_listenSocket->Accept( clientAddress );

		int user_id{};// = m_UserID++;
		for( int i = 2; i < MAX_CLIENTS; ++i ){
			if( !SHARED_RESOURCE::g_clients[i]->isConnected ){
				user_id = i;
				break;
			}
		}

		SOCKETINFO* new_player = new SOCKETINFO;
		new_player->user_id = user_id;
		new_player->room_id = -1; // -1 이면 방 소속 안되어있다.
		new_player->socket = clientSocket;

		new_player->recv_over.wsabuf[0].len = MAX_BUFFER;
		new_player->recv_over.wsabuf[0].buf = new_player->recv_over.net_buf;

		new_player->recv_over.ev_type = EV_TCP_RECV;

		new_player->isConnected = true;

		new_player->udp_addr = clientAddress;
		new_player->curr_packet_size = 0;
		new_player->prev_packet_data = 0;

		SHARED_RESOURCE::g_clients[user_id] = new_player;
		std::cout << user_id << " accept!" << std::endl;

		CreateIoCompletionPort( reinterpret_cast<HANDLE>(clientSocket->GetSocket()), SHARED_RESOURCE::g_iocp, user_id, 0 );

		memset( &SHARED_RESOURCE::g_clients[user_id]->recv_over.over, 0, sizeof( WSAOVERLAPPED ) );
		clientSocket->WSAReceive( SHARED_RESOURCE::g_clients[user_id]->recv_over.wsabuf, 1, NULL, &flags, &SHARED_RESOURCE::g_clients[user_id]->recv_over.over );
		clientSocket = nullptr;
	}
}

//void BattleServer::SendPacketBuffer( int id, void* buf )
//{
//	char* packets{ reinterpret_cast<char*>(buf) };
//	unsigned int packet_size{MAKEWORD( packets[1], packets[2] ) };
//	packet_size += 3;
//
//	// 오버EX 구조체 생성, 초기화.
//	OVER_EX* send_over = new OVER_EX;
//	memset( send_over, 0, sizeof( OVER_EX ) );
//	send_over->ev_type = EV_SEND;
//
//	//int packet_count = SHARED_RESOURCE::g_clients[id]->recv_over.stream->GetNWrite();
//	//m_OutFile << "ID: " << id << " | Packet Count: " << packet_count << " | Packet Size: " << packet_size << endl;
//
//	send_over->wsabuf[0].len = packet_size;
//	//send_over->wsabuf[0].buf = send_over->net_buf;
//	send_over->wsabuf[0].buf = packets;
//	
//	int retval = SHARED_RESOURCE::g_clients[id]->socket->WSASend( send_over->wsabuf, 1, 0, 0, &send_over->over );
//}

void BattleServer::SendPacket( int id, void* buf )
{
	char* packet = reinterpret_cast<char*>(buf);
	BYTE packet_size = (BYTE)packet[0];
	OVER_EX* send_over = new OVER_EX;
	memset( send_over, 0, sizeof( OVER_EX ) );
	send_over->ev_type = EV_SEND;
	memcpy( send_over->net_buf, packet, packet_size );
	send_over->wsabuf[0].len = packet_size;
	send_over->wsabuf[0].buf = send_over->net_buf;

	SHARED_RESOURCE::g_clients[id]->socket->WSASend( send_over->wsabuf, 1, 0, 0, &send_over->over );
}


void BattleServer::SendAccessOKPacket( int id, char match_type )
{
	bc_packet_accept_ok packet;
	packet.size = sizeof( packet );

	if ( match_type == MATCH_TYPE_AUTO )
	{
		packet.type = BC_AUTO_ACCEPT_OK;
	}
	else
	{
		packet.type = BC_MANUAL_ACCEPT_OK;
	}
	packet.id = id;
	
	SendPacket( id, &packet );
}

void BattleServer::SendAccessDenyPacket( int id )
{
	bc_packet_accept_deny packet;
	packet.size = sizeof( packet );
	packet.type = BC_ACCEPT_DEINED;
	SendPacket( id, &packet );
}

void BattleServer::SendAutoRoomReadyPacket( int id, int room_no )
{
	bm_packet_room_ready packet;
	packet.size = sizeof( packet );
	packet.type = BM_ROOMREADY;
	packet.roomNum = room_no;
	SendPacket( id, &packet );
}

void BattleServer::SendManualRoomReadyPacket( int id, int room_no )
{
	bc_packet_room_maked packet;
	packet.size = sizeof( packet );
	packet.type = BC_ROOM_MAKED;
	packet.roomNum = room_no;
	SendPacket( id, &packet );
}

void BattleServer::SendRoomJoinSuccess( int id, int slot )
{
	bc_packet_join_ok packet;
	packet.size = sizeof( packet );
	packet.type = BC_JOIN_OK;
	packet.playerNo = slot;
	std::cout << "send join ok \n";
	SendPacket( id, &packet );
}

void BattleServer::SendRoomJoinDenied( int id, int code )
{
	bc_packet_join_deny packet;
	packet.size = sizeof( packet );
	packet.type = BC_JOIN_DENY;
	packet.code = code;
	std::cout << "send join deny \n";
	SendPacket( id, &packet );
}

void BattleServer::SendMapTypePacket( int id, int maptype )
{
	bc_packet_map_type packet;
	packet.size = sizeof( packet );
	packet.type = BC_MAP_TYPE;
	packet.mapType = maptype;
	SendPacket( id, &packet );
}

void BattleServer::SendRoomEnterPacket( int to, int enterer, bool ready, char playerNo, const char* char_type,
	char* name, int mmr, int catchCnt, float w_rate, int totalCnt, int winCnt, bool isManager )
{
	bc_packet_room_entered packet;
	packet.size = sizeof( packet );
	packet.type = BC_ROOM_ENTERED;
	packet.id = enterer;
	packet.ready = ready;
	packet.playerNo = playerNo;
	strcpy( packet.char_type, char_type );
	strcpy( packet.name, name );
	packet.mmr = mmr;
	packet.catched_student_count = catchCnt;
	packet.winning_rate = w_rate;
	packet.total_game_cnt = totalCnt;
	packet.winning_game_cnt = winCnt;
	packet.isManager = isManager;
	SendPacket( to, &packet );
}

void BattleServer::SendRoomLeavePacket( int to, int leaver )
{
	bc_packet_room_leaved packet;
	packet.size = sizeof( packet );
	packet.type = BC_ROOM_LEAVED;
	packet.id = leaver;
	SendPacket( to, &packet );
}

void BattleServer::SendRoomListPacket( int id, int totalRoomNum ,PTC_Room* room_list )
{
	bc_packet_room_list packet;
	packet.size = sizeof( packet );
	packet.type = BC_ROOM_LIST;
	packet.totalRoomNum = totalRoomNum;
	memcpy( &packet.room_list, room_list, sizeof( packet.room_list ) );	//될런지 모르겠
	SendPacket( id, &packet );
}

void BattleServer::SendGameStartPacket( int id, int mapType, PTC_Start_Info* player_info )
{
	bc_packet_game_start packet;
	packet.size = sizeof( packet );
	packet.type = BC_GAME_START;
	packet.map_type = mapType;
	packet.penalty_duration = PENALTY_DURATION;
	memcpy( packet.info, player_info, sizeof( PTC_Start_Info ) * 4 );
	SendPacket( id, &packet );
}

void BattleServer::SendSelectCharacterPacket( int id, int selector, const char* char_type )
{
	bc_packet_select_character packet;
	packet.size = sizeof( packet );
	packet.type = BC_SELECT_CHARACTER;
	packet.id = selector;
	strcpy_s( packet.character, char_type );
	SendPacket( id, &packet );
}

void BattleServer::SendSelectCharacterFailPacket( int id )
{
	bc_packet_select_character_fail packet;
	packet.size = sizeof( packet );
	packet.type = BC_SELECT_CHARACTER_FAIL;
	SendPacket( id, &packet );
}

void BattleServer::SendCancleSelectCharacterPakcet( int to, int selector, const char* char_type )
{
	bc_packet_cancel_select_character packet;
	packet.size = sizeof( packet );
	packet.type = BC_CANCEL_SELECT_CHARACTER;
	packet.id = selector;
	strcpy_s( packet.character, char_type );
	SendPacket( to, &packet );
}

void BattleServer::SendRoundStartPacket( int id )
{
	bc_packet_round_start packet;
	packet.size = sizeof( packet );
	packet.type = BC_ROUND_START;
	SendPacket( id, &packet );
}

void BattleServer::SendPlayerRotation( int to, int from, PTC_Vector look )
{
	bc_packet_player_rot packet;
	packet.size = sizeof( packet );
	packet.type = BC_PLAYER_ROT;
	packet.id = from;
	packet.look = look;
	SendPacket( to, &packet );
}

void BattleServer::SendLeftTimePacket( int id, char left_time )
{
	bc_packet_left_time packet;
	packet.size = sizeof( packet );
	packet.type = BC_LEFT_TIME;
	packet.left_time = left_time;
	SendPacket( id, &packet );
}

void BattleServer::SendPutThunderBoltPacket( int id, int bolt_id, PTC_Vector pos )
{
	bc_packet_put_thunderbolt packet;
	packet.size = sizeof( packet );
	packet.type = BC_PUT_THUNDERBOLT;
	packet.bolt_id = bolt_id;
	packet.pos = pos;
	SendPacket( id, &packet );
}

void BattleServer::SendGameOverPacket( int id, char winner_role )
{
	bc_packet_game_over packet;
	packet.size = sizeof( packet );
	packet.type = BC_GAME_OVER;
	packet.win_team = winner_role;
	SendPacket( id, &packet );
}

void BattleServer::SendUpdatedUserInfoPacket(const int& id, const int& mmr, const int& catchCnt, const float& w_rate, const int& totalCnt, const int& winCnt )
{
	bc_packet_updated_user_info packet{};
	packet.size = sizeof( packet );
	packet.type = BC_UPDATED_USER_INFO;
	packet.mmr = mmr;
	packet.catched_student_count = catchCnt;
	packet.winning_rate = w_rate;
	packet.total_game_cnt = totalCnt;
	packet.winning_game_cnt = winCnt;
	SendPacket( id, &packet );
}

void BattleServer::AddTimer( EVENT& ev )
{
	ATOMIC::g_timer_lock.lock();
	SHARED_RESOURCE::g_timer_queue.push( ev );
	ATOMIC::g_timer_lock.unlock();
}

void BattleServer::error_display( const char* msg, int  err_no )
{
	WCHAR* lpMsgBuf;
	FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER |
		FORMAT_MESSAGE_FROM_SYSTEM,
		NULL, err_no,
		MAKELANGID( LANG_NEUTRAL, SUBLANG_DEFAULT ),
		(LPTSTR)&lpMsgBuf, 0, NULL );
	std::cout << msg;
	std::wcout << L"에러 - " << err_no << lpMsgBuf << std::endl;
	//while ( true );
	LocalFree( lpMsgBuf );
}

SOCKET BattleServer::GetUDPSocket()
{
	return m_UDPSocket->GetSocket();
}

SOCKADDR_IN BattleServer::GetServerAddr()
{
	return m_ServerAddr;
}

