#pragma once
#include "../../Protocol.h"
#include "Globals.h"
#include "Singleton.h"

class ThreadHandler;
class DB_Handler;
class BattleServer : public Singleton<BattleServer>
{
public:
	BattleServer();
	~BattleServer();

	void Initialize();
	void AcceptMMServer();
	void Run();

	void error_display( const char*, int );
	void AddTimer( EVENT& ev );

public:
	SOCKET GetUDPSocket();
	SOCKADDR_IN GetServerAddr();


public:
	//void SendPacketBuffer( int id, void* buff );
	void SendPacket( int id, void* buff );
	void SendAccessOKPacket( int id, char match_type );
	void SendAccessDenyPacket( int id );
	void SendAutoRoomReadyPacket( int id, int room_no );
	void SendManualRoomReadyPacket( int id, int room_no );
	void SendRoomJoinSuccess( int id, int slot );
	void SendRoomJoinDenied( int id, int code );
	void SendMapTypePacket( int id, int maptype );
	void SendRoomEnterPacket( int to, int enterer, bool ready, char playerNo,const char* char_type, char* name,
		int mmr, int catchCnt, float w_rate, int totalCnt, int winCnt, bool isManager );
	void SendRoomLeavePacket( int to, int from );
	void SendRoomListPacket( int id, int totalRoomNum, PTC_Room* room_list );
	void SendGameStartPacket( int id, int mapType, PTC_Start_Info* player_info );
	void SendSelectCharacterPacket( int to, int selector, const char* char_type );
	void SendSelectCharacterFailPacket( int id );
	void SendCancleSelectCharacterPakcet( int to, int selector, const char* char_type );

	// 인게임
	void SendRoundStartPacket( int id );
	void SendPlayerRotation(int to, int from, PTC_Vector look );
	void SendLeftTimePacket( int id, char left_time );
	void SendPutThunderBoltPacket( int id, int bolt_id, PTC_Vector pos);
	void SendGameOverPacket( int id, char winner_role );

	// 유저 정보 업데이트 패킷
	void SendUpdatedUserInfoPacket( const int& id, const int& mmr, const int& catchCnt, const float& w_rate, const int& totalCnt, const int& winCnt );

private:

	ThreadHandler* m_ThreadHandler;
	//std::atomic_int m_UserID;
	
	SocketUtil m_sockUtil;
	TCPSocketPtr m_listenSocket;
	UDPSocketPtr m_UDPSocket;

	SOCKET m_UDPRecvSock;
	SOCKADDR_IN m_ServerAddr{};

	OVER_EX m_udpOver{};
	SocketAddress m_UDPClientAddr{};


	ofstream m_OutFile{ "MemoryBufferOut.txt", ios::app };
};

