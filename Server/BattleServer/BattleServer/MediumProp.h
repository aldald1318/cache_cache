#pragma once
#include "Prop.h"
class MediumProp :
	public Prop
{
public:
	using Prop::Prop;
	MediumProp() = default;
	MediumProp( const obj_info& info, const Boundary* bb_info ) : Prop( info, bb_info ){ Initialize(); }
	MediumProp( Object& obj, const Boundary* bb_info ) : Prop( obj, bb_info ){ Initialize(); }
	void Initialize();

	MediumProp* clone() const { return new MediumProp( *this ); }
};

