#pragma once
#include "Globals.h"

class SpawnPosition
{
public:
	enum SpawnPos { SPAWN_MASTER, SPAWN_STUDENT_1, SPAWN_STUDENT_2, SPAWN_STUDENT_3, SPAWN_COUNT };

public:
	SpawnPosition() = default;

public:
	std::array < XMFLOAT4, SPAWN_COUNT> m_spawnPos;	//w == rot y
};

