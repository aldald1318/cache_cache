#include "pch.h"
#include "Player.h"
#include "Master.h"
#include "Character.h"
#include "Globals.h"
#include "extern.h"
#include "../../Protocol.h"

Player::Player()
{
	Initialize();
}

Player::Player( const Player& rhs )
{
	if( this != &rhs ){
		m_id = rhs.m_id;
		m_Role = rhs.m_Role;
		m_isEmpty = rhs.m_isEmpty;
		m_isReady = rhs.m_isReady;
		m_isDead = rhs.m_isDead;
		memcpy( m_idStr, rhs.m_idStr, sizeof( char ) * 10 );
		m_mmr = rhs.m_mmr;
		m_killCnt = rhs.m_killCnt;
		m_winningRate = rhs.m_winningRate;
		m_CurForm = rhs.m_CurForm;
		m_CharacterType = rhs.m_CharacterType;
	}
}

Player& Player::operator=( const Player& rhs ) noexcept
{
	if( this != &rhs ){
		delete m_CurForm;
		m_CurForm = rhs.m_CurForm;
		m_id = rhs.m_id;
		m_Role = rhs.m_Role;
		m_isEmpty = rhs.m_isEmpty;
		m_isReady = rhs.m_isReady;
		m_isDead = rhs.m_isDead;
		memcpy( m_idStr, rhs.m_idStr, sizeof( char ) * 10 );
		m_mmr = rhs.m_mmr;
		m_killCnt = rhs.m_killCnt;
		m_winningRate = rhs.m_winningRate;
		delete& rhs;
	}
	return *this;
}

Player::~Player()
{
	delete m_CurForm;
	m_CurForm = nullptr;
}

Master& Player::GetMaster()
{
	return reinterpret_cast<Master&>(*this);
}

void Player::Initialize()
{
	m_id = -1;
	m_CurForm = nullptr;
	m_Role = ROLE_STUDENT;		// 나중에 게임 시작할 때, 랜덤으로 Master로 지정해주기.
	m_isEmpty = true;
	m_isReady = false;
	m_isDead = false;
	ZeroMemory( &m_idStr, sizeof( char ) * 10 );
	m_mmr = 0;
	m_killCnt = 0;
	m_winningRate = 0.f;
	m_CharacterType = (int)CHARACTERS::NONE;
	m_prevAnimType = ANIM_IDLE;
	m_keyW = false;
	m_keyA = false;
	m_keyS = false;
	m_keyD = false;
	m_keySpace = false;
	m_animJump = false;
}

void Player::Reset()
{
	m_CurForm = nullptr;
	m_isReady = false;
	m_isDead = false;
	m_isMovable = false;
	m_CharacterType = (int)CHARACTERS::NONE; // 이거 
	m_keyW = false;
	m_keyA = false;
	m_keyS = false;
	m_keyD = false;
	m_keySpace = false;
}

void Player::SetCurForm( const char& role, const int& map_no, const int& sp )
{
	m_CurForm = new Character( role, map_no, sp );
}

Object* Player::GetCurForm()
{
	return m_CurForm;
}

void Player::SetRole( char role )
{
	m_Role = role;
}

char Player::GetRole()
{
	return m_Role;
}

void Player::Enter()
{
	m_isEmpty = false;
}

void Player::Leave()
{
	m_isEmpty = true;
}

bool Player::GetSlotStatus()
{
	return m_isEmpty;
}

void Player::SetID( const int& id )
{
	m_id = id;
}

int Player::GetID()
{
	return m_id;
}

void Player::SetReady()
{
	m_isReady = !m_isReady;
}

bool Player::GetReady()
{
	return m_isReady;
}

void Player::SetID_STR( char* name )
{
	memcpy( &m_idStr, name, sizeof( char ) * 10 );
}

char* Player::GetID_STR()
{
	return m_idStr;
}

void Player::SetMMR( int mmr )
{
	m_mmr = mmr;
}

int Player::GetMMR()
{
	return m_mmr;
}

void Player::SetKillCnt( int cnt )
{
	m_killCnt = cnt;
}

int Player::GetKillCnt()
{
	return m_killCnt;
}

void Player::SetWinRate( float rate )
{
	m_winningRate = rate;
}

float Player::GetWinRate()
{
	return m_winningRate;
}

void Player::SetTotalCnt( int totalCnt )
{
	m_totalGameCnt = totalCnt;
}

int Player::GetTotalCnt()
{
	return m_totalGameCnt;
}

void Player::SetWinCnt( int winCnt )
{
	m_winningGameCnt = winCnt;
}

int Player::GetWinCnt()
{
	return m_winningGameCnt;
}

void Player::SetCharacterType( short c_type )
{
	m_CharacterType = c_type;
}

short Player::GetCharacterType()
{
	return m_CharacterType;
}

bool Player::IsMovable()
{
	return m_isMovable;
}

void Player::SetIsMovable( bool movable )
{
	m_isMovable = movable;
}

int Player::GetPrevAnimType()
{
	return m_prevAnimType;
}

void Player::SetPrevAnimType( int animType )
{
	m_prevAnimType = animType;
}

void Player::SetKeyW( bool input )
{
	m_keyW = input;
}

void Player::SetKeyS( bool input )
{
	m_keyS = input;
}

void Player::SetKeyA( bool input )
{
	m_keyA = input;
}

void Player::SetKeyD( bool input )
{
	m_keyD = input;
}

void Player::SetKeySpace( bool input )
{
	m_keySpace = input;
}

bool Player::GetKeyW()
{
	return m_keyW;
}

bool Player::GetKeyS()
{
	return m_keyS;
}

bool Player::GetKeyA()
{
	return m_keyA;
}

bool Player::GetKeyD()
{
	return m_keyD;
}

bool Player::GetKeySpace()
{
	return m_keySpace;
}

bool Player::IsDead()
{
	return m_isDead;
}

void Player::SetDead()
{
	m_isDead = true;
}

