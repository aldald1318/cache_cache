#pragma once
#include "Object.h"
#include "SpawnPosition.h"

enum class ROLE {NONE, MASTER, STUDENT};
class Character :
	public Object
{
public:
	Character() = default;
	Character(const char& role, const int& map_no, const int& sp );
	Character( Object& curForm, const Boundary* new_bb_info );
	~Character();

public:
	void Initialize();

private:
};

