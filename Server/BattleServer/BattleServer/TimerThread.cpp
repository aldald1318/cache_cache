#include "pch.h"
#include "TimerThread.h"
#include "extern.h"
using namespace std;
void TimerThread::InitThread()
{
	mythread = std::thread( [&]() {TimerThread::ProcThread(); } );
}

void TimerThread::ProcThread()
{
	while( true )
	{
		ATOMIC::g_timer_lock.lock();
		while( true == SHARED_RESOURCE::g_timer_queue.empty() )
		{
			ATOMIC::g_timer_lock.unlock();
			this_thread::sleep_for(10ms);
			ATOMIC::g_timer_lock.lock();
		}

		const EVENT& ev = SHARED_RESOURCE::g_timer_queue.top();
		if( chrono::high_resolution_clock::now() < ev.wakeup_time )
		{
			ATOMIC::g_timer_lock.unlock();
			this_thread::sleep_for( 1ms );
			continue;
		}
		EVENT proc_ev = ev;
		SHARED_RESOURCE::g_timer_queue.pop();
		ATOMIC::g_timer_lock.unlock();

		OVER_EX* over_ex = new OVER_EX;

		if ( ( SHARED_RESOURCE::g_rooms[proc_ev.target] != NULL ) && ( proc_ev.event_type != EV_THUNDERBOLT_SET_NOT_USE ) ) {
			if ( proc_ev.event_type == EV_UPDATE )
			{
				over_ex->ev_type = EV_UPDATE;
				*( int* )over_ex->net_buf = proc_ev.target;
			}
			else if ( proc_ev.event_type == EV_FLUSH_MSG )
			{
				over_ex->ev_type = EV_FLUSH_MSG;
				*( int* )over_ex->net_buf = proc_ev.target;
			}
			else if ( proc_ev.event_type == EV_TICK )
			{
				over_ex->ev_type = EV_TICK;
				*( int* )over_ex->net_buf = proc_ev.target;
			}

			else if ( proc_ev.event_type == EV_PENALTY_CHECK )
			{
				over_ex->ev_type = EV_PENALTY_CHECK;
				*( int* )over_ex->net_buf = proc_ev.target;
			}

			else if ( proc_ev.event_type == EV_PENALTY_BEGIN ) {
				over_ex->ev_type = EV_PENALTY_BEGIN;
				*( int* )over_ex->net_buf = proc_ev.target;
			}

			else if ( proc_ev.event_type == EV_PENALTY_END ) {
				over_ex->ev_type = EV_PENALTY_END;
				*( int* )over_ex->net_buf = proc_ev.target;
			}

			else if ( proc_ev.event_type == EV_MOVE_ENABLE ) {
				over_ex->ev_type = EV_MOVE_ENABLE;
				*( int* )over_ex->net_buf = proc_ev.target;
			}

			else if ( proc_ev.event_type == EV_MAKE_MOVE_DISABLE ) {
				over_ex->ev_type = EV_MAKE_MOVE_DISABLE;
				*( int* )over_ex->net_buf = proc_ev.target;
			}

			else if ( proc_ev.event_type == EV_UPDATE_DB ) {
				over_ex->ev_type = EV_UPDATE_DB;
			}

			else if ( proc_ev.event_type == EV_RESET_ROOM ) {
				over_ex->ev_type = EV_RESET_ROOM;
				*( int* )over_ex->net_buf = proc_ev.target;
			}

			else if ( proc_ev.event_type == EV_FULL_WORLD_UPDATE_END ) {
				over_ex->ev_type = EV_FULL_WORLD_UPDATE_END;
				*( int* )over_ex->net_buf = proc_ev.target;
			}

			else if ( proc_ev.event_type == EV_THUNDERBOLT_SET_NOT_USE ) {
				over_ex->ev_type = EV_THUNDERBOLT_SET_NOT_USE;
				std::cout << "여기까지는 들어옵니다" << std::endl;
				*( int* )over_ex->net_buf = proc_ev.target;
			}

			else
			{
				// 이벤트가 없는데 왜 이 코드 수행?
				//over_ex->ev_type = proc_ev.event_type;
				//*( int* )over_ex->net_buf = proc_ev.target;
				std::cout << "Unknown Event Type Error! \n";
				while ( true );
			}

			PostQueuedCompletionStatus( SHARED_RESOURCE::g_iocp, 1, proc_ev.id, &over_ex->over );
		}
		else if ( proc_ev.event_type == EV_THUNDERBOLT_SET_NOT_USE )
		{
			short tmp[2];
			memcpy( tmp, &proc_ev.target, sizeof( short ) * 2 );
			if ( SHARED_RESOURCE::g_rooms[tmp[0]] != NULL )
			{
				over_ex->ev_type = EV_THUNDERBOLT_SET_NOT_USE;
				*( int* )over_ex->net_buf = proc_ev.target;
				PostQueuedCompletionStatus( SHARED_RESOURCE::g_iocp, 1, proc_ev.id, &over_ex->over );
			}
		}
	}
}

void TimerThread::JoinThread()
{
	mythread.join();
}
