#include "pch.h"
#include "Physics.h"
#include "Object.h"
#include "Boundary.h"

// room에서 호출하는 함수
// 오브젝트 2개 넘겨서 겹치면 ret의 is_collision 이 false
ProcCollisionInfo Physics::CheckCollision( const Object* a, const Object* b ) const
{
	if( a == nullptr || b == nullptr ){ 
		ProcCollisionInfo ret{};
		ret.is_collision = false;
		return ret; 
	}

	int aNumBS = a->m_boundaries->GetNumOfBS();
	int bNumBS = b->m_boundaries->GetNumOfBS();

	XMFLOAT3 aPos, bPos;
	float aRad, bRad;

	for ( int i = 0; i < aNumBS; ++i )
	{
		aPos = a->m_boundaries->GetBSPos( i );
		aRad = a->m_boundaries->GetBSRad( i );

		for ( int j = 0; j < bNumBS; ++j )
		{
			bPos = b->m_boundaries->GetBSPos( i );
			bRad = b->m_boundaries->GetBSRad( i );

			if ( true == CheckCollisionSphere( aPos, bPos, aRad, bRad ) )
			{
				return CheckCollisionOBB_OBB( a, b );
			}
		}
	}
	ProcCollisionInfo ret;
	ret.is_collision = false;
	return ret;
}

bool Physics::CheckBulletObjCollisionPoint( const XMFLOAT3 bullet_pos, const Object* obj )const
{
	int objNumBS = obj->m_boundaries->GetNumOfBS();

	XMFLOAT3 objPos;
	float objRad;

	for ( int i = 0; i < objNumBS; ++i )
	{
		objPos = obj->m_boundaries->GetBSPos( i );
		objRad = obj->m_boundaries->GetBSRad( i );

		if ( true == CheckCollisionSphere( bullet_pos, objPos, objRad, 0 ) )
			return CheckCollisionOBB_Point( bullet_pos, obj );
	}

	return false;
}

bool Physics::CheckBulletObjCollisionLine( const XMFLOAT3 bullet_pos_start, const XMFLOAT3 bullet_pos_end, const Object* obj )const
{
	int objNumBS = obj->m_boundaries->GetNumOfBS();

	XMFLOAT3 objPos;
	float objRad;

	for ( int i = 0; i < objNumBS; ++i )
	{
		objPos = obj->m_boundaries->GetBSPos( i );
		objRad = obj->m_boundaries->GetBSRad( i );

		if ( true == CheckCollisionSphere_Ray( bullet_pos_start, bullet_pos_end, objPos, objRad ) )
			return CheckCollisionOBB_Ray( bullet_pos_start, bullet_pos_end, obj );
	}

	return false;
}

bool Physics::CheckBulletCharacterCollision( const XMFLOAT3 bullet_pos, const Object* obj )const
{
	int objNumBS = obj->m_boundaries->GetNumOfBS();

	XMFLOAT3 objPos;
	float objRad;

	for ( int i = 0; i < objNumBS; ++i )
	{
		objPos = obj->m_boundaries->GetBSPos( i );
		objRad = obj->m_boundaries->GetBSRad( i );

		if ( true == CheckCollisionSphere( bullet_pos, objPos, objRad, 0 ) )
			return true; //client에게 Send
	}

	return false;
}

void Physics::ProcCollisionNF( Object* n, Object* f )
{
	XMFLOAT3 nVel = n->GetVelocity();
	float nMass = n->GetMass();

	float fMass = f->GetMass();

	XMFLOAT3 nVf;
	nVf.x = ( ( nMass - fMass ) / ( nMass + fMass ) ) * nVel.x;
	nVf.y = ( ( nMass - fMass ) / ( nMass + fMass ) ) * nVel.y;
	nVf.z = ( ( nMass - fMass ) / ( nMass + fMass ) ) * nVel.z;

	n->SetVelocity( nVf );
}


void Physics::SplitObject( Object* a, Object* b, XMFLOAT3 collisionNormal, float overlapDepth, bool is_sequential, bool player )
{
	float depth = overlapDepth;
	if ( depth >= 70.f )
	{
		//std::cout << "depth : " << depth << std::endl;
		depth = 5.f;
	}
	else if ( depth <= -70.f )
	{
		//std::cout << "depth : " << depth << std::endl;
		depth = -5.f;
	}

	XMFLOAT3 mtv = Vector3::Multiply( collisionNormal, depth );
	if ( is_sequential )//b가 a보다 멀리 -> b이동
	{
		b->MoveObject( mtv );
		b->m_boundaries->BoundaryMove( mtv );
	}
	else	//a가 b보다 멀리 -> a이동
	{
		a->MoveObject( mtv );
		a->m_boundaries->BoundaryMove( mtv );
	}

}

// notfixed notfixed
void Physics::SplitObject( Object* a, Object* b, XMFLOAT3 collisionNormal, float overlapDepth, bool is_sequential )
{
	XMFLOAT3 mtv = Vector3::Multiply( collisionNormal, overlapDepth );
	if ( is_sequential )//b가 a보다 멀리 -> b이동
	{
		b->MoveObject( mtv );
		b->m_boundaries->BoundaryMove( mtv );
	}
	else	//a가 b보다 멀리 -> a이동
	{
		a->MoveObject( mtv );
		a->m_boundaries->BoundaryMove( mtv );
	}
}

//fixed notfixed -> notfixed만 넘긴다
void Physics::SplitObject( Object* a, XMFLOAT3 collisionNormal, float overlapDepth, bool is_sequential )
{
	XMFLOAT3 mtv;
	float depth = overlapDepth;
	if ( depth >= 20.f )
		depth = 20.f;
	else if ( depth <= -20.f )
		depth = -20.f;
	if ( is_sequential )
	{
		mtv = Vector3::Multiply( collisionNormal, depth );
	}
	else
	{
		mtv = Vector3::Multiply( collisionNormal, -depth );
	}
	a->MoveObject( mtv );
	a->m_boundaries->BoundaryMove( mtv );
}

// bullet impulse
void Physics::ProcBulletImpulse( Object* a, XMFLOAT3 bullet_pos_start, XMFLOAT3 bullet_pos_end )
{

	XMFLOAT3 bulletDirection = Vector3::Subtract( bullet_pos_end, bullet_pos_start );
	float size = sqrtf( bulletDirection.x * bulletDirection.x + bulletDirection.z * bulletDirection.z );
	bulletDirection.x /= size;
	bulletDirection.z /= size;
	bulletDirection.y = 0.f;
	bulletDirection = Vector3::Multiply( bulletDirection, 1.f );

	a->AddVelocity( bulletDirection );
}

// notfixed notfixed
void Physics::ProcCollisionImpulse( Object* a, Object* b, XMFLOAT3 collisionNormal, XMFLOAT3 relativeVelocity, bool is_sequential )
{
	float j;
	j = ( -( 1.f + 0.f/*반발계수*/ ) * ( Vector3::DotProduct( relativeVelocity, collisionNormal ) ) / ( Vector3::DotProduct( collisionNormal, collisionNormal ) * ( 1.f / a->GetMass() + 1.f / b->GetMass() ) ) );

	if ( is_sequential )
	{
		a->SubVelocity( Vector3::Division( Vector3::Multiply( collisionNormal, j ), a->GetMass() ) );
		b->AddVelocity( Vector3::Division( Vector3::Multiply( collisionNormal, j ), b->GetMass() ) );
	}
	else
	{
		a->AddVelocity( Vector3::Division( Vector3::Multiply( collisionNormal, j ), a->GetMass() ) );
		b->SubVelocity( Vector3::Division( Vector3::Multiply( collisionNormal, j ), b->GetMass() ) );
	}
}

//fixed notfixed -> notfixed만 넘긴다
void Physics::ProcCollisionImpulse( Object* a, XMFLOAT3 collisionNormal, XMFLOAT3 relativeVelocity, bool is_sequential )
{
	float j;
	j = ( -( 1.f + 0.0f/*반발계수*/ ) * ( Vector3::DotProduct( relativeVelocity, collisionNormal ) ) / ( Vector3::DotProduct( collisionNormal, collisionNormal ) * ( 1.f / a->GetMass() + 1.f / 5000.f ) ) );

	if ( is_sequential )
	{
		a->AddVelocity( Vector3::Division( Vector3::Multiply( collisionNormal, j ), a->GetMass() ) );
	}
	else
	{
		a->SubVelocity( Vector3::Division( Vector3::Multiply( collisionNormal, j ), a->GetMass() ) );
	}
}

//private

// 원과 원 충돌 체크
bool Physics::CheckCollisionSphere( const XMFLOAT3& aPos, const XMFLOAT3& bPos, const float aRad, const float bRad ) const
{
	if ( pow( aRad + bRad, 2 ) > pow( aPos.x - bPos.x, 2 ) + pow( aPos.y - bPos.y, 2 ) + pow( aPos.z - bPos.z, 2 ) )
		return true;
	return false;
}

//원과 선분
bool Physics::CheckCollisionSphere_Ray( const XMFLOAT3& bullet_pos_start, const XMFLOAT3& bullet_pos_end, const XMFLOAT3& objPos, const float objRad ) const
{
	XMFLOAT3 Scenter2LStart = Vector3::Subtract( bullet_pos_start, objPos );
	float c = Vector3::DotProduct( Scenter2LStart, Scenter2LStart ) - ( objRad * objRad );

	if ( c <= 0.0f )
		return true;

	XMFLOAT3 lineDirection = Vector3::Subtract( bullet_pos_end, bullet_pos_start );
	float lineLenght = sqrtf( lineDirection.x * lineDirection.x + lineDirection.y * lineDirection.y + lineDirection.z * lineDirection.z );

	if ( lineLenght == 0.0f )
		return false;

	XMFLOAT3 normLine = Vector3::Division( lineDirection, lineLenght );
	float b_prime = Vector3::DotProduct( Scenter2LStart, normLine );

	if ( b_prime > 0.0f )
		return false;

	float discriminant = sqrt( b_prime * b_prime - c );

	float t1 = -b_prime + discriminant;
	if ( t1 >= 0.0f && t1 <= lineLenght )
		return true;

	float t2 = -b_prime - discriminant;
	if ( t1 >= 0.0f && t1 <= lineLenght )
		return true;

	return false;
}

// 오비비끼리 충돌체크
ProcCollisionInfo Physics::CheckCollisionOBB_OBB( const Object* a, const Object* b )const
{
	int aNumBB = a->m_boundaries->GetNumOfBB();
	int bNumBB = b->m_boundaries->GetNumOfBB();

	// 각 오브젝트 바운딩박스의 꼭짓점 좌표가 저장될것임
	BBPoints* aPoints = ( BBPoints* )malloc( sizeof( BBPoints ) * aNumBB );
	BBPoints* bPoints = ( BBPoints* )malloc( sizeof( BBPoints ) * bNumBB );

	// 바운딩박스 중심 좌표
	XMFLOAT3 aCenter;
	XMFLOAT3 bCenter;

	// 바운딩박스 크기 (절반 값)
	XMFLOAT3 aSize;
	XMFLOAT3 bSize;

	//각 바운딩박스의 룩업라이트
	XMFLOAT3 aAxis[AXIS_NUM];
	XMFLOAT3 bAxis[AXIS_NUM];

	float centerDistance;
	float aDistance;
	float bDistance;
	float aMaxDistance;
	float bMaxDistance;

	// impulse방식 충돌처리시 필요한 정보
	XMFLOAT3 collisionNormalVector;
	// 를 구하기 위한 임시 변수
	float maximunCenterDistance = FLT_EPSILON;
	// 침투 깊이
	float overlapDepth = 0.f;


	// a, b각 꼭짓점 구하기
	GetPoints( a->m_boundaries, aPoints, aNumBB );
	GetPoints( b->m_boundaries, bPoints, bNumBB );


	// y, x, z 순서로 진행
	for ( int i = 0; i < aNumBB; ++i )
	{

		aCenter = a->m_boundaries->GetBBPos( i );
		aSize = a->m_boundaries->GetBBSize( i );
		aAxis[AXIS_Z] = a->m_boundaries->GetBBLook( i );
		aAxis[AXIS_Y] = a->m_boundaries->GetBBUp( i );
		aAxis[AXIS_X] = a->m_boundaries->GetBBRight( i );

		for ( int j = 0; j < bNumBB; ++j )
		{
			maximunCenterDistance = FLT_EPSILON;


			bCenter = b->m_boundaries->GetBBPos( j );
			bSize = b->m_boundaries->GetBBSize( j );
			bAxis[AXIS_Z] = b->m_boundaries->GetBBLook( j );
			bAxis[AXIS_Y] = b->m_boundaries->GetBBUp( j );
			bAxis[AXIS_X] = b->m_boundaries->GetBBRight( j );

			//bb기준
			// 1 & 8
			//	max가 겹치면 겹치는 축
			//	모든 축이 겹치면 겹치는 bb return true

			//	안 겹치는 축이 하나라도 있으면 안 겹친다 -> 다음 bb로!

			//object기준
			//하나라도 겹치는 bb가 있으면 return true
			//겹치는 bb가 한개도 없으면 return false

			// a 에게 투영
			for ( int axis = AXIS_Y; axis < AXIS_NUM; ++axis )
			{
				// 각 축에 각각의 오비비 센터를 투영해 둘 사이 길이 알아내는 함수
				centerDistance = GetProjectionPointDistance( aCenter, bCenter, aAxis[axis] );

				switch ( axis )	//a 오브젝트의 축이므로 a의 점은 투영할 필요 없이 크기가 곧 길이
				{
				case AXIS_Y:
					aDistance = aSize.y;
					break;
				case AXIS_X:
					aDistance = aSize.x;
					break;
				case AXIS_Z:
					aDistance = aSize.z;
					break;
				}

				bMaxDistance = 0.f;
				for ( int b = 0; b < 8; ++b )
				{
					//b의 점 투영
					bDistance = GetProjectionPointDistance( bCenter, bPoints->point[b], aAxis[axis] );
					//b의 점 투영길이 중 가장 긴 길이가
					if ( bDistance > bMaxDistance )
						bMaxDistance = bDistance;
				}
				//aDistance 와의 합이 중심간의 길이보다 작으면 충돌하지 않는 오비비
				if ( aDistance + bMaxDistance < centerDistance )
					goto THIS_BB_NOT_COLLISION;

				if ( maximunCenterDistance < centerDistance )	// impulse방식에서 사용하기위해 저장
				{
					maximunCenterDistance = centerDistance;
					collisionNormalVector = aAxis[axis];
					overlapDepth = aDistance + bMaxDistance - centerDistance;
				}

			}// a 에게 투영

			// b 에게 투영
			for ( int axis = AXIS_Y; axis < AXIS_NUM; ++axis )
			{
				centerDistance = GetProjectionPointDistance( aCenter, bCenter, bAxis[axis] );

				switch ( axis )
				{
				case AXIS_Y:
					bDistance = bSize.y;
					break;
				case AXIS_X:
					bDistance = bSize.x;
					break;
				case AXIS_Z:
					bDistance = bSize.z;
					break;
				}

				aMaxDistance = 0;
				for ( int a = 0; a < 8; ++a )
				{
					aDistance = GetProjectionPointDistance( aCenter, aPoints->point[a], bAxis[axis] );
					if ( aDistance > aMaxDistance )
						aMaxDistance = aDistance;
				}
				if ( aMaxDistance + bDistance < centerDistance )
					goto THIS_BB_NOT_COLLISION;

				if ( maximunCenterDistance < centerDistance )
				{
					maximunCenterDistance = centerDistance;
					collisionNormalVector = bAxis[axis];
					overlapDepth = aMaxDistance + bDistance - centerDistance;
				}

			}// b 에게 투영

			// a X b 에게 투영
			for ( int aA = AXIS_Y; aA < AXIS_NUM; ++aA )
			{
				for ( int bA = AXIS_Y; bA < AXIS_NUM; ++bA )
				{
					XMFLOAT3 curAxis = Vector3::CrossProduct( aAxis[aA], bAxis[bA] );
					centerDistance = GetProjectionPointDistance( aCenter, bCenter, curAxis );

					aMaxDistance = 0.f;
					bMaxDistance = 0.f;

					for ( int a = 0; a < 8; ++a )
					{
						aDistance = GetProjectionPointDistance( aCenter, aPoints->point[a], curAxis );

						if ( aDistance > aMaxDistance )
							aMaxDistance = aDistance;
					}

					for ( int b = 0; b < 8; ++b )
					{
						bDistance = GetProjectionPointDistance( bCenter, bPoints->point[b], curAxis );

						if ( bDistance > bMaxDistance )
							bMaxDistance = bDistance;
					}

					if ( aMaxDistance + bMaxDistance < centerDistance )
						goto THIS_BB_NOT_COLLISION;

					if ( maximunCenterDistance < centerDistance )
					{
						maximunCenterDistance = centerDistance;
						collisionNormalVector = Vector3::Normalize( curAxis );

						if ( Vector3::IsZero( collisionNormalVector ) )	//->똑같은 벡터로 외적한 것임
							collisionNormalVector = aAxis[aA];

						overlapDepth = aMaxDistance + bMaxDistance - centerDistance;
					}
				}
			}// a X b 에게 투영
			//여기까지 나오면 모든 축이 겹친다 == 충돌했다

			ProcCollisionInfo ret;
			if ( Vector3::DotProduct( collisionNormalVector, aCenter ) > Vector3::DotProduct( collisionNormalVector, bCenter ) )	// a가 b보다 충돌 법센 벡터에서 멀리있다
			{
				ret.is_sequential = false;
				ret.relativeVelocity = Vector3::Subtract( a->GetVelocity(), b->GetVelocity() );
			}
			else
			{
				ret.is_sequential = true;
				ret.relativeVelocity = Vector3::Subtract( b->GetVelocity(), a->GetVelocity() );
			}


			if ( Vector3::DotProduct( collisionNormalVector, ret.relativeVelocity ) >= 0.0f ) // 서로 멀어지는 중이다
			{
				//std::cout << "멀어지는 중" << std::endl;
				goto THIS_BB_NOT_COLLISION;
			}

			ret.overlapDepth = overlapDepth;
			ret.is_collision = true;
			ret.collisionNormalVector = collisionNormalVector;


			free( aPoints );
			free( bPoints );
			return ret;


		THIS_BB_NOT_COLLISION:;	//충돌 안했으면 일로 온다 뽀문 마져 돌음
		}
	}

	free( aPoints );
	free( bPoints );
	ProcCollisionInfo ret;
	ret.is_collision = false;
	return ret;
}

bool Physics::CheckCollisionOBB_Ray( const XMFLOAT3 bullet_pos_start, const XMFLOAT3 bullet_pos_end, const Object* obj )const
{
	int objNumBB = obj->m_boundaries->GetNumOfBB();

	XMFLOAT3 bulletStart, bulletEnd;

	XMFLOAT3 sizeBB;
	XMFLOAT4X4 worldIn;
	
	float tmin { 0.f };
	float tmax { 0.f };
	float tymin{ 0.f };
	float tymax{ 0.f };
	float tzmin{ 0.f };
	float tzmax{ 0.f };

	for ( int i = 0; i < objNumBB; ++i )
	{
		sizeBB = obj->m_boundaries->GetBBSize( i );

		worldIn = obj->m_boundaries->GetWorldMatrix( i );
		worldIn = Matrix4x4::Inverse( worldIn );

		bulletStart = Vector3::Transform( bullet_pos_start, worldIn );
		bulletEnd = Vector3::Transform( bullet_pos_end, worldIn );

		XMFLOAT3 direction = Vector3::Subtract( bulletEnd, bulletStart );
	
		XMFLOAT3 inverseDirection = Vector3::Division( XMFLOAT3 { 1.0f,1.0f,1.0f }, direction );

		int sign[3] = { ( inverseDirection.x < 0 ),( inverseDirection.y < 0 ),( inverseDirection.z < 0 ) };

		//min max
		XMFLOAT3 bounds[2] = { {( -sizeBB.x ),( -sizeBB.y ) ,( -sizeBB.z )} ,{( sizeBB.x ) ,( sizeBB.y ) ,( sizeBB.z )} };

		tmin = ( bounds[sign[0]].x - bulletStart.x ) * inverseDirection.x;
		tmax = ( bounds[1 - sign[0]].x - bulletStart.x ) * inverseDirection.x;
		tzmin = ( bounds[sign[2]].z - bulletStart.z ) * inverseDirection.z;
		tzmax = ( bounds[1 - sign[2]].z - bulletStart.z ) * inverseDirection.z;

		if ( ( tmin > tzmax ) || ( tzmin > tmax ) ) {
			return false;
		}
		if ( tzmin > tmin ) {
			tmin = tzmin;
		}
		if ( tzmax < tmax ) {
			tmax = tzmax;
		}

		tymin = ( bounds[sign[1]].y - bulletStart.y ) * inverseDirection.y;
		tymax = ( bounds[1 - sign[1]].y - bulletStart.y ) * inverseDirection.y;

		if ( ( tmin > tymax ) || ( tymin > tmax ) ) {
			return false;
		}
		if ( tymin > tmin ) {
			tmin = tymin;
		}
		if ( tymax < tmax ) {
			tmax = tymax;
		}

		return !( tmax < 0.0f || tmin > 1.0f );

	}
	return false;
}

bool Physics::CheckCollisionOBB_Point( const XMFLOAT3 bullet_pos, const Object* obj )const
{
	int objNumBB = obj->m_boundaries->GetNumOfBB();

	XMFLOAT3 sizeBB;
	XMFLOAT3 AxisBB[AXIS_NUM];

	float disX;
	float disY;
	float disZ;

	for ( int i = 0; i < objNumBB; ++i )
	{
		sizeBB = Vector3::Multiply( obj->m_boundaries->GetBBSize( i ), 2 );
		// 저장된 길이가 절반이라 2 곱해줌

		AxisBB[AXIS_Y] = obj->m_boundaries->GetBBLook( i );
		AxisBB[AXIS_X] = obj->m_boundaries->GetBBRight( i );
		AxisBB[AXIS_Z] = obj->m_boundaries->GetBBUp( i );

		disX = Vector3::DotProduct( bullet_pos, AxisBB[AXIS_X] );
		disY = Vector3::DotProduct( bullet_pos, AxisBB[AXIS_Y] );
		disZ = Vector3::DotProduct( bullet_pos, AxisBB[AXIS_Z] );

		if ( ( 0 < disY && disY < sizeBB.y ) && ( 0 < disX && disX < sizeBB.x ) && ( 0 < disZ && disZ < sizeBB.z ) )
			return true;
	}
	return false;
}

float Physics::GetProjectionPointDistance( const XMFLOAT3 a, const XMFLOAT3 b, const XMFLOAT3 vector )const
{
	XMFLOAT3 aProj = GetProjectionPoint( a, vector );
	XMFLOAT3 bProj = GetProjectionPoint( b, vector );
	float distance = sqrt( pow( aProj.x - bProj.x, 2 ) + pow( aProj.y - bProj.y, 2 ) + pow( aProj.z - bProj.z, 2 ) );
	return distance;
}

XMFLOAT3 Physics::GetProjectionPoint( const XMFLOAT3& point, const XMFLOAT3& vector )const
{
	XMFLOAT3 ret = Vector3::Multiply( vector, Vector3::DotProduct( point, vector ) );
	return ret;
}

void Physics::GetPoints( const Boundary* boundary, BBPoints* points, int num )const
{
	XMFLOAT3 center;
	XMFLOAT3 look;
	XMFLOAT3 up;
	XMFLOAT3 right;
	XMFLOAT3 size;

	for ( int i = 0; i < num; ++i )
	{
		center = boundary->GetBBPos( i );
		look = boundary->GetBBLook( i );
		up = boundary->GetBBUp( i );
		right = boundary->GetBBRight( i );
		size = boundary->GetBBSize( i );

		points[i].point[0] = GetPointTo( look, GetPointTo( right, GetPointTo( up, center, size.y ), size.x ), size.z );
		points[i].point[1] = GetPointTo( look, GetPointTo( right, GetPointTo( up, center, size.y ), size.x ), -size.z );
		points[i].point[2] = GetPointTo( look, GetPointTo( right, GetPointTo( up, center, size.y ), -size.x ), size.z );
		points[i].point[3] = GetPointTo( look, GetPointTo( right, GetPointTo( up, center, size.y ), -size.x ), -size.z );

		points[i].point[4] = GetPointTo( look, GetPointTo( right, GetPointTo( up, center, -size.y ), size.x ), size.z );
		points[i].point[5] = GetPointTo( look, GetPointTo( right, GetPointTo( up, center, -size.y ), size.x ), -size.z );
		points[i].point[6] = GetPointTo( look, GetPointTo( right, GetPointTo( up, center, -size.y ), -size.x ), size.z );
		points[i].point[7] = GetPointTo( look, GetPointTo( right, GetPointTo( up, center, -size.y ), -size.x ), -size.z );
	}
}

const XMFLOAT3 Physics::GetPointTo( const XMFLOAT3 to, XMFLOAT3 pos, float distance )const
{
	XMFLOAT3 res = Vector3::Add( pos, to, distance );
	return res;
}
