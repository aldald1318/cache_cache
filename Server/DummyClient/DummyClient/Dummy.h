#pragma once
#include "pch.h"
#include "define.h"
#include "../../Protocol.h"

enum SERVER_TYPE { SV_BATTLE, SV_LOBBY, SV_BATTLE_UDP, SV_COUNT };

enum EVENT_TYPE { EV_RECV_TCP, EV_RECV_UDP, EV_SEND };

struct OverlappedEx {
	WSAOVERLAPPED over;
	WSABUF wsabuf;
	unsigned char IOCP_buf[MAX_BUF];
	EVENT_TYPE ev_type;
	SERVER_TYPE sv_type;
};

struct ConnectSocket {
	SOCKET socket;
	OverlappedEx recv_over;
	unsigned char packet_buf[MAX_PACKET_SIZE];
	int prev_packet_data;
	int curr_packet_size;
};

struct CLIENT {
	int id;
	int battle_id;
	short room_num;
	char is_host = 0;

	bool connect;
	bool loginok;
	bool battle_connect;

	ConnectSocket connectSocket[SV_COUNT];

	string name;
	int mmr = -1;
	int catched_student_count = 0;
	int total_game_count{};
	float winning_rate = 0.f;
	int winning_game_count{};
};

class Dummy
{
public:
	Dummy();
	~Dummy();

	void DoWorker();
	void ProcessPacket( int id, unsigned char packet[] );

	void ConnectLobbyServer( const boost::system::error_code&, boost::asio::deadline_timer* t );
	void ConnectBattleServer( int id);

	void DisconnectClient( int id );

	void SendLoginPacket( int id );
	void SendAutoMatchPacket( int id );
	void SendJoinPacket( int id, int roomNum );
	void SendReadyPacket( int id );
	void SendLookVectorPacket( int id );
	void SendBattleLoginPacket( int id );
	void SendSelectCharacterPacket( int id, int slot );
	void SendGameStartPacket( int id );
	void SendRequestUserInfo( int id );
	void SendUpdatedUserInfo(int id, int mmr, int catchCnt, float w_rate, int totalCnt, int winCnt );
	void SendPacket( int id, void* packet, SERVER_TYPE sv );

	void error_display( const char* msg, int err_no );

private:
	HANDLE	iocp;

	std::array<CLIENT, MAX_DUMMY> dummy;
	std::atomic_int dummy_count = 0;
	std::atomic_int num_connections;
};
