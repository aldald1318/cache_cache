#include "Dummy.h"

Dummy::Dummy()
{
	for ( int i = 0; i < MAX_DUMMY; ++i ) {
		dummy[i].connect = false;
		dummy[i].loginok = false;
		dummy[i].battle_connect = false;
		dummy[i].id = -1;
		dummy[i].battle_id = -1;
	}
	num_connections = 0;

	WSADATA	wsadata;
	WSAStartup( MAKEWORD( 2, 2 ), &wsadata );

	iocp = CreateIoCompletionPort( INVALID_HANDLE_VALUE, 0, NULL, 0 );
}
Dummy::~Dummy()
{

}

void Dummy::DoWorker()
{
	while ( true )
	{
		DWORD io_size;
		unsigned long long ci;
		OverlappedEx* over;
		BOOL ret = GetQueuedCompletionStatus( iocp, &io_size, ( PULONG_PTR )&ci, reinterpret_cast< LPWSAOVERLAPPED* >( &over ), INFINITE );

		int client_id = static_cast< int >( ci );
		if ( FALSE == ret )
		{
			int err_no = WSAGetLastError();
			if ( 64 == err_no ) DisconnectClient( client_id );
			else error_display( "GQCS : ", WSAGetLastError() );
		}
		if ( 0 == io_size )
		{
			DisconnectClient( client_id );
			continue;
		}
		SERVER_TYPE sv_type = over->sv_type;
		if ( EV_RECV_TCP == over->ev_type )
		{
			unsigned char* buf = over->IOCP_buf;// dummy[ci].connectSocket[sv_type].recv_over.IOCP_buf;
			unsigned psize = dummy[ci].connectSocket[sv_type].curr_packet_size;
			unsigned pr_size = dummy[ci].connectSocket[sv_type].prev_packet_data;

			while ( io_size > 0 )
			{
				if ( 0 == psize )
				{
					//std::cout << "recv size : " << ( int )buf[0] << ", " << io_size << std::endl;
					psize = buf[0];
				}

				if ( io_size + pr_size >= psize )
				{
					// 지금 패킷 완성 가능
					unsigned char packet[MAX_PACKET_SIZE];
					memcpy( packet, dummy[ci].connectSocket[sv_type].packet_buf, pr_size );
					memcpy( packet + pr_size, buf, psize - pr_size );
					//std::cout << "packet size : " << ( int )packet[0] << std::endl;
					ProcessPacket( static_cast< int >( ci ), packet );
					io_size -= psize - pr_size;
					buf += psize - pr_size;
					psize = 0;
					pr_size = 0;
				}
				else
				{
					memcpy( dummy[ci].connectSocket[sv_type].packet_buf + pr_size, buf, io_size );
					pr_size += io_size;
					io_size = 0;
				}
			}
			dummy[ci].connectSocket[sv_type].curr_packet_size = psize;
			dummy[ci].connectSocket[sv_type].prev_packet_data = pr_size;

			DWORD recv_flag = 0;

			int ret = WSARecv( dummy[ci].connectSocket[sv_type].socket, &dummy[ci].connectSocket[sv_type].recv_over.wsabuf, 1, NULL, &recv_flag, &dummy[ci].connectSocket[sv_type].recv_over.over, NULL );
			if ( SOCKET_ERROR == ret )
			{
				int err_no = WSAGetLastError();
				if ( err_no != WSA_IO_PENDING )
					error_display( "RECV ERROR", err_no );
			}
		}
		else if ( EV_RECV_UDP == over->ev_type )
		{
			ProcessPacket( client_id, over->IOCP_buf );

			DWORD recv_flag = 0;

			int ret = WSARecv( dummy[ci].connectSocket[sv_type].socket, &dummy[ci].connectSocket[sv_type].recv_over.wsabuf, 1, NULL, &recv_flag, &dummy[ci].connectSocket[sv_type].recv_over.over, NULL );
			if ( SOCKET_ERROR == ret )
			{
				int err_no = WSAGetLastError();
				if ( err_no != WSA_IO_PENDING )
					error_display( "RECV ERROR", err_no );
			}
		}
		else if ( EV_SEND == over->ev_type )
		{
			if ( io_size != over->wsabuf.len )
			{
				std::cout << "Send Incomplete Error!\n";
				closesocket( dummy[client_id].connectSocket[SV_LOBBY].socket );
				closesocket( dummy[client_id].connectSocket[SV_BATTLE].socket );
				dummy[client_id].connect = false;
				dummy[client_id].loginok = false;
			}
			delete over;
		}
		else
		{
			std::cout << "Unknown GQCS event!\n";
			while ( true );
		}
	}
}
void Dummy::ProcessPacket( int id, unsigned char _packet[] )
{
	switch ( _packet[1] )
	{
	case LC_LOGINOK:
	{
		lc_packet_login_ok* login_packet = reinterpret_cast< lc_packet_login_ok* >( _packet );
		int my_id = dummy_count++;
		dummy[my_id].id = login_packet->id;
		dummy[my_id].loginok = true;
		SendRequestUserInfo( id );
		break;
	}

	case LC_USERINFO:
	{
		lc_packet_userinfo* packet = reinterpret_cast<lc_packet_userinfo*>(_packet);
		dummy[id].mmr = packet->mmr;
		dummy[id].catched_student_count = packet->catched_student_count;
		dummy[id].total_game_count = packet->total_game_cnt;
		dummy[id].winning_rate = packet->winning_rate;
		dummy[id].winning_game_count = packet->winning_game_cnt;
		break;
	}

	case BC_AUTO_ACCEPT_OK:
	{
		//std::cout << "battle server accept ok\n";
		bc_packet_accept_ok* accept_packet = reinterpret_cast< bc_packet_accept_ok* >( _packet );
		dummy[id].battle_id = accept_packet->id;

		dummy[id].battle_connect = true;

		SendJoinPacket( id, dummy[id].room_num );
		break;
	}
	case LC_MATCH:
	{
		//std::cout << "recv match" << std::endl;
		lc_packet_match* match_packet = reinterpret_cast< lc_packet_match* >( _packet );
		dummy[id].room_num = match_packet->roomNum;
		dummy[id].is_host = match_packet->is_host;
		ConnectBattleServer( id );
		break;
	}
	case BC_PLAYER_ROT: break;
	case BC_JOIN_OK:
	{
		bc_packet_join_ok* recv_packet = reinterpret_cast< bc_packet_join_ok* >( _packet );
		SendSelectCharacterPacket( id, recv_packet->playerNo );
		//std::cout << "joinok\n";
		SendReadyPacket( id );
		break;
	}
	case BC_JOIN_DENY: std::cout << "join denied\n"; break;
	case LC_LOGINDENY: std::cout << "login deny\n"; break;
	case BC_ACCEPT_DEINED: std::cout << "accept deny\n"; break;
	case BC_ROOM_ENTERED: break;
	case BC_NEW_ROOM_MANAGER: std::cout << "new room manager\n"; break;
	case BC_SELECT_CHARACTER: break;
	case BC_LEFT_TIME: break;
	case BC_CANCEL_SELECT_CHARACTER: std::cout << "cancel select character\n"; break;
	case BC_SELECT_CHARACTER_FAIL: 
	{
		std::cout << "select character fail\n"; 
		break;
	}
	case BC_READY: break;
	case BC_GAME_START: break;
	case BC_GAMESTART_AVAILABLE:
	{
		//std::cout << "game start available\n";
		SendGameStartPacket( id );
		break;
	}
	case BC_MAP_TYPE: break;

	case BC_UPDATED_USER_INFO:
	{
		bc_packet_updated_user_info* packet = reinterpret_cast<bc_packet_updated_user_info*>(_packet);
		// 패킷을 바로 로비서버로 릴레이한다.
		SendUpdatedUserInfo(id,  packet->mmr, packet->catched_student_count, packet->winning_rate, packet->total_game_cnt, packet->winning_game_cnt );
		break;
	}
	default:
	{
		//std::cout << "unkown packet : " << ( int )_packet[1] << std::endl;
		//while( 1 );
	}
	}
}

std::uniform_int_distribution<int> uid { 0,10 };
std::default_random_engine dre;

//로비 서버에 연결
void Dummy::ConnectLobbyServer( const boost::system::error_code&, boost::asio::deadline_timer* t )
{
	if ( num_connections >= MAX_DUMMY ) return;

	dummy[num_connections].connectSocket[SV_LOBBY].socket = WSASocketW( AF_INET, SOCK_STREAM, IPPROTO_TCP, NULL, 0, WSA_FLAG_OVERLAPPED );

	SOCKADDR_IN ServerAddr;
	ZeroMemory( &ServerAddr, sizeof( SOCKADDR_IN ) );
	ServerAddr.sin_family = AF_INET;
	ServerAddr.sin_port = htons( LOBBY_SERVER_PORT );
	ServerAddr.sin_addr.s_addr = inet_addr( LOBBY_SERVER_IP_PUBLIC );


	int Result = WSAConnect( dummy[num_connections].connectSocket[SV_LOBBY].socket, ( sockaddr* )&ServerAddr, sizeof( ServerAddr ), NULL, NULL, NULL, NULL );
	if ( 0 != Result ) {
		error_display( "WSAConnect : ", GetLastError() );
	}

	dummy[num_connections].connectSocket[SV_LOBBY].curr_packet_size = 0;
	dummy[num_connections].connectSocket[SV_LOBBY].prev_packet_data = 0;
	ZeroMemory( &dummy[num_connections].connectSocket[SV_LOBBY].recv_over, sizeof( dummy[num_connections].connectSocket[SV_LOBBY].recv_over ) );
	dummy[num_connections].connectSocket[SV_LOBBY].recv_over.ev_type = EV_RECV_TCP;
	dummy[num_connections].connectSocket[SV_LOBBY].recv_over.sv_type = SV_LOBBY;

	dummy[num_connections].connectSocket[SV_LOBBY].recv_over.wsabuf.buf = reinterpret_cast< CHAR* >( dummy[num_connections].connectSocket[SV_LOBBY].recv_over.IOCP_buf );
	dummy[num_connections].connectSocket[SV_LOBBY].recv_over.wsabuf.len = sizeof( dummy[num_connections].connectSocket[SV_LOBBY].recv_over.IOCP_buf );

	DWORD recv_flag = 0;
	CreateIoCompletionPort( reinterpret_cast< HANDLE >( dummy[num_connections].connectSocket[SV_LOBBY].socket ), iocp, num_connections, 0 );
	int ret = WSARecv( dummy[num_connections].connectSocket[SV_LOBBY].socket, &dummy[num_connections].connectSocket[SV_LOBBY].recv_over.wsabuf, 1,
		NULL, &recv_flag, &dummy[num_connections].connectSocket[SV_LOBBY].recv_over.over, NULL );
	if ( SOCKET_ERROR == ret ) {
		int err_no = WSAGetLastError();
		if ( err_no != WSA_IO_PENDING )
		{
			error_display( "RECV ERROR", err_no );
		}
	}
	dummy[num_connections].connect = true;

	//여기서 send packet
	SendLoginPacket( num_connections );
	std::cout << "num connection : " << num_connections << std::endl;

	num_connections++;
	//std::cout << num_connections << std::endl;

	while ( !dummy[num_connections - 1].loginok ) {};

	SendAutoMatchPacket( num_connections - 1 );


	//for ( int i = 0; i < dummy_count; ++i )
	//{
	//	if ( i < 10 )
	//		if ( dummy[i].battle_connect == true )
	//		{
	//			SendReadyPacket( i );
	//		}
	//}

	t->expires_from_now( boost::posix_time::millisec( 50 ) );
	t->async_wait( boost::bind( &Dummy::ConnectLobbyServer, this, boost::asio::placeholders::error, t ) );
}

//배틀 서버에 연결
void Dummy::ConnectBattleServer( int id )
{
	dummy[id].connectSocket[SV_BATTLE].socket = WSASocketW( AF_INET, SOCK_STREAM, IPPROTO_TCP, NULL, 0, WSA_FLAG_OVERLAPPED );

	SOCKADDR_IN ServerAddr;
	ZeroMemory( &ServerAddr, sizeof( SOCKADDR_IN ) );
	ServerAddr.sin_family = AF_INET;
	ServerAddr.sin_port = htons( BATTLE_SERVER_PORT );
	ServerAddr.sin_addr.s_addr = inet_addr( BATTLE_SERVER_IP_PUBLIC );


	int Result = WSAConnect( dummy[id].connectSocket[SV_BATTLE].socket, ( sockaddr* )&ServerAddr, sizeof( ServerAddr ), NULL, NULL, NULL, NULL );
	if ( 0 != Result ) {
		error_display( "WSA TCP Connect : ", WSAGetLastError() );
	}

	dummy[id].connectSocket[SV_BATTLE].curr_packet_size = 0;
	dummy[id].connectSocket[SV_BATTLE].prev_packet_data = 0;
	ZeroMemory( &dummy[id].connectSocket[SV_BATTLE].recv_over, sizeof( dummy[id].connectSocket[SV_BATTLE].recv_over ) );
	dummy[id].connectSocket[SV_BATTLE].recv_over.ev_type = EV_RECV_TCP;
	dummy[id].connectSocket[SV_BATTLE].recv_over.sv_type = SV_BATTLE;

	dummy[id].connectSocket[SV_BATTLE].recv_over.wsabuf.buf = reinterpret_cast< CHAR* >( dummy[id].connectSocket[SV_BATTLE].recv_over.IOCP_buf );
	dummy[id].connectSocket[SV_BATTLE].recv_over.wsabuf.len = sizeof( dummy[id].connectSocket[SV_BATTLE].recv_over.IOCP_buf );

	DWORD recv_flag = 0;
	CreateIoCompletionPort( reinterpret_cast< HANDLE >( dummy[id].connectSocket[SV_BATTLE].socket ), iocp, id, 0 );
	int ret = WSARecv( dummy[id].connectSocket[SV_BATTLE].socket, &dummy[id].connectSocket[SV_BATTLE].recv_over.wsabuf, 1,
		NULL, &recv_flag, &dummy[id].connectSocket[SV_BATTLE].recv_over.over, NULL );
	if ( SOCKET_ERROR == ret ) {
		int err_no = WSAGetLastError();
		if ( err_no != WSA_IO_PENDING )
		{
			error_display( "RECV ERROR", err_no );
		}
	}
	//tcp connect
	//
	//udp connect
	dummy[id].connectSocket[SV_BATTLE_UDP].socket = WSASocketW( AF_INET, SOCK_DGRAM, IPPROTO_UDP, NULL, 0, WSA_FLAG_OVERLAPPED );

	ZeroMemory( &ServerAddr, sizeof( SOCKADDR_IN ) );
	ServerAddr.sin_family = AF_INET;
	ServerAddr.sin_port = htons( BATTLE_SERVER_UDP_PORT );
	ServerAddr.sin_addr.s_addr = inet_addr( BATTLE_SERVER_IP_PUBLIC );


	Result = WSAConnect( dummy[id].connectSocket[SV_BATTLE_UDP].socket, ( sockaddr* )&ServerAddr, sizeof( ServerAddr ), NULL, NULL, NULL, NULL );
	if ( 0 != Result ) {
		error_display( "WSA UDP Connect : ", GetLastError() );
	}

	dummy[id].connectSocket[SV_BATTLE_UDP].curr_packet_size = 0;
	dummy[id].connectSocket[SV_BATTLE_UDP].prev_packet_data = 0;
	ZeroMemory( &dummy[id].connectSocket[SV_BATTLE_UDP].recv_over, sizeof( dummy[id].connectSocket[SV_BATTLE_UDP].recv_over ) );
	dummy[id].connectSocket[SV_BATTLE_UDP].recv_over.ev_type = EV_RECV_UDP;
	dummy[id].connectSocket[SV_BATTLE_UDP].recv_over.sv_type = SV_BATTLE_UDP;

	dummy[id].connectSocket[SV_BATTLE_UDP].recv_over.wsabuf.buf = reinterpret_cast< CHAR* >( dummy[id].connectSocket[SV_BATTLE_UDP].recv_over.IOCP_buf );
	dummy[id].connectSocket[SV_BATTLE_UDP].recv_over.wsabuf.len = sizeof( dummy[id].connectSocket[SV_BATTLE_UDP].recv_over.IOCP_buf );

	recv_flag = 0;
	CreateIoCompletionPort( reinterpret_cast< HANDLE >( dummy[id].connectSocket[SV_BATTLE_UDP].socket ), iocp, id, 0 );
	ret = WSARecv( dummy[id].connectSocket[SV_BATTLE_UDP].socket, &dummy[id].connectSocket[SV_BATTLE_UDP].recv_over.wsabuf, 1,
		NULL, &recv_flag, &dummy[id].connectSocket[SV_BATTLE_UDP].recv_over.over, NULL );
	if ( SOCKET_ERROR == ret ) {
		int err_no = WSAGetLastError();
		if ( err_no != WSA_IO_PENDING )
		{
			error_display( "RECV ERROR", err_no );
		}
	}

	SendBattleLoginPacket( id );
}


void Dummy::DisconnectClient( int id )
{
	closesocket( dummy[id].connectSocket[SV_LOBBY].socket );
	closesocket( dummy[id].connectSocket[SV_BATTLE].socket );
	dummy[id].connect = false;
	std::cout << "Client [" << id << "] Disconnected!\n";
}

void Dummy::SendAutoMatchPacket( int id )
{
	cl_packet_automatch packet;
	packet.size = sizeof( packet );
	packet.type = CL_AUTOMATCH;

	SendPacket( id, &packet, SV_LOBBY );
}

void Dummy::SendLoginPacket( int id )
{
	//cl_packet_dummy_login packet;
	cl_packet_login packet;
	packet.size = sizeof( packet );
	packet.type = CL_LOGIN;
	std::string id_str { "Dummy" };
	id_str += std::to_string( id );
	std::string pw { "password" };
	memcpy( packet.id, id_str.c_str(), sizeof( char ) * 10 );
	memcpy( packet.password, pw.c_str(), sizeof( char ) * 10 );
	SendPacket( id, &packet, SV_LOBBY );
}

void Dummy::SendBattleLoginPacket( int id )
{
	cb_packet_login packet;
	packet.size = sizeof( packet );
	packet.type = CB_LOGIN;
	packet.is_automatch = MATCH_TYPE_AUTO;
	std::string id_str { "Dummy" };
	id_str += std::to_string( id );
	strcpy( packet.name, id_str.c_str() );
	packet.name[strlen( id_str.c_str() )] = '\0';
	packet.mmr = dummy[id].mmr;
	packet.catched_student_count = dummy[id].catched_student_count;
	packet.winning_rate = dummy[id].winning_rate;
	packet.winning_game_cnt = dummy[id].winning_game_count;
	packet.total_game_cnt = dummy[id].total_game_count;
	SendPacket( id, &packet, SV_BATTLE );
}

void Dummy::SendSelectCharacterPacket( int id, int slot )
{
	cb_packet_select_character packet;
	packet.size = sizeof( packet );
	packet.type = CB_SELECT_CHARACTER;
	std::string character;
	switch ( slot )
	{
	case 0:
		character = "DR";
		break;
	case 1:
		character = "BA";
		break;
	case 2:
		character = "FP";
		break;
	case 3:
		character = "MP";
		break;
	}
	strcpy_s( packet.character, character.c_str() );
	SendPacket( id, &packet, SV_BATTLE );
}

void Dummy::SendGameStartPacket( int id )
{
	cb_packet_start packet;
	packet.size = sizeof( packet );
	packet.type = CB_START;
	SendPacket( id, &packet, SV_BATTLE );
}

void Dummy::SendRequestUserInfo(int id)
{
	cl_packet_request_userinfo packet;
	packet.size = sizeof( packet );
	packet.type = CL_REQUEST_USERINFO;
	SendPacket(id, &packet, SV_LOBBY );
}

void Dummy::SendUpdatedUserInfo(int id, int mmr, int catchCnt, float w_rate, int totalCnt, int winCnt )
{
	cl_packet_updated_user_info packet;
	packet.size = sizeof( packet );
	packet.type = CL_UPDATED_USER_INFO;
	packet.mmr = mmr;
	packet.catched_student_count = catchCnt;
	packet.winning_rate = w_rate;
	packet.total_game_cnt = totalCnt;
	packet.winning_game_cnt = winCnt;
	SendPacket(id, &packet, SERVER_TYPE::SV_LOBBY );
}

void Dummy::SendReadyPacket( int id )
{
	cb_packet_ready packet;
	packet.size = sizeof( packet );
	packet.type = CB_READY;
	SendPacket( id, &packet, SV_BATTLE );
}

void Dummy::SendJoinPacket( int id, int roomNum )
{
	cb_packet_join packet;
	packet.size = sizeof( packet );
	packet.type = CB_JOIN;
	packet.roomNum = roomNum;
	packet.is_roomManager = dummy[id].is_host;
	SendPacket( id, &packet, SV_BATTLE );
}

void Dummy::SendLookVectorPacket( int id )
{
	cb_packet_look_vector packet;
	packet.size = sizeof( packet );
	packet.type = CB_LOOK_VECTOR;
	packet.id = dummy[id].battle_id;
	packet.look.x = 0.f;
	packet.look.y = 1.f;
	packet.look.z = 1.f;

	SendPacket( id, &packet, SV_BATTLE_UDP );
}

void Dummy::SendPacket( int id, void* packet, SERVER_TYPE sv )
{
	int psize = reinterpret_cast< unsigned char* >( packet )[0];
	int ptype = reinterpret_cast< unsigned char* >( packet )[1];
	OverlappedEx* over = new OverlappedEx;
	over->ev_type = EV_SEND;
	memcpy( over->IOCP_buf, packet, psize );
	ZeroMemory( &over->over, sizeof( over->over ) );
	over->wsabuf.buf = reinterpret_cast< CHAR* >( over->IOCP_buf );
	over->wsabuf.len = psize;

	int ret = WSASend( dummy[id].connectSocket[sv].socket, &over->wsabuf, 1, NULL, 0, &over->over, NULL );
	if ( 0 != ret ) {
		int err_no = WSAGetLastError();
		if ( WSA_IO_PENDING != err_no )
			error_display( "Error in SendPacket:", err_no );
	}
}

void Dummy::error_display( const char* msg, int err_no )
{
	WCHAR* lpMsgBuf;
	FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER |
		FORMAT_MESSAGE_FROM_SYSTEM,
		NULL, err_no,
		MAKELANGID( LANG_NEUTRAL, SUBLANG_DEFAULT ),
		( LPTSTR )&lpMsgBuf, 0, NULL );
	std::cout << msg;
	std::wcout << L"에러" << lpMsgBuf << std::endl;

	LocalFree( lpMsgBuf );
	while ( true );
}