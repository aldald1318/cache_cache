#pragma once

namespace EXInputHandler
{
	bool IsKeyDown(int key);
	bool IsKeyUp(int key);
	bool IsOverlap(int key);
}