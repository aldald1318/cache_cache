struct VS_OUTPUT
{
    float4 pos : SV_POSITION;
    float4 texC : TEXCOORD;
};

float4 main(VS_OUTPUT input) : SV_TARGET
{
    // return interpolated color
    return input.texC;
}