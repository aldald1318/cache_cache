#pragma once
#include "../CacheCache/framework.h"
#include "EXInputHandler.h"

namespace EXInputHandler
{
	static bool	s_CursorState = true;

	static bool	s_KeyFlag[256];

	// 현재 안쓰이는중 하지만 로비씬이나, 다른씬에서 쓰일수도있음
	POINT	g_LastMousePos = { 0.f,0.f };
	float	g_MouseChangebleX = 0.f;
	float	g_MouseChangebleY = 0.f;

	bool	g_MoveMouseCallback = false;
	bool	g_LeftMouseCallback = false;
	bool	g_RightMouseCallback = false;

	bool IsKeyDown(int key)
	{
		if (GetAsyncKeyState(key) & 0x8000) {
			if (!s_KeyFlag[key])
				return s_KeyFlag[key] = true;
		}
		return false;
	}

	bool IsKeyUp(int key)
	{
		if (GetAsyncKeyState(key) & 0x8000)
			s_KeyFlag[key] = true;
		else
			if (s_KeyFlag[key])
				return !(s_KeyFlag[key] = false);
		return false;
	}

	bool IsOverlap(int key)
	{
		return (GetAsyncKeyState(key) & 0x8000);
	}
}