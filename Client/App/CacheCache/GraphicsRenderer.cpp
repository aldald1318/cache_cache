#include "pch.h"
#include "GraphicsRenderer.h"
#include "GameTimer.h"
#include "GameCore.h"
#include "CommandContext.h"
#include "GameObject.h"
#include "GeometryMesh.h"
#include "DDSTextureLoader.h"

namespace Graphics
{
	Microsoft::WRL::ComPtr<ID3D12PipelineState> g_OpaquePSO;
	Microsoft::WRL::ComPtr<ID3D12PipelineState> g_OpacityPSO;
	Microsoft::WRL::ComPtr<ID3D12PipelineState> g_SkinnedPSO;
	Microsoft::WRL::ComPtr<ID3D12PipelineState> g_ContourPSO;
	Microsoft::WRL::ComPtr<ID3D12PipelineState> g_SkyPSO;
	Microsoft::WRL::ComPtr<ID3D12PipelineState> g_UIPSO;
	Microsoft::WRL::ComPtr<ID3D12PipelineState> g_HPPSO;
	Microsoft::WRL::ComPtr<ID3D12PipelineState> g_ShadowOpaquePSO;
	Microsoft::WRL::ComPtr<ID3D12PipelineState> g_SkinnedShadowOpaquePSO;
	Microsoft::WRL::ComPtr<ID3D12PipelineState> g_HorzBlurPSO;
	Microsoft::WRL::ComPtr<ID3D12PipelineState> g_VertBlurPSO;
	Microsoft::WRL::ComPtr<ID3D12PipelineState> g_ParticlePSO;
}

using namespace Core;
using namespace Graphics;

void GraphicsRenderer::Initialize()
{
	// Build Resources
	mShadowMap = std::make_unique<ShadowMap>(Core::g_Device.Get(), 3500, 3500);
	mBlurFilter = std::make_unique<BlurFilter>(Core::g_Device.Get(),
		Core::g_DisplayWidth, Core::g_DisplayHeight, DXGI_FORMAT_R8G8B8A8_UNORM);

	LoadTextures();
	BuildDescriptorHeaps();

	// Renderer
	BuildShaderAndInputLayout();
	BuildRootSignatures();
	BuildPostProcessRootSignature();
	BuildPipelineStateObjects();
}

void GraphicsRenderer::Shutdown()
{
}

void GraphicsRenderer::SetGraphicsDescriptorHeap()
{
	ID3D12DescriptorHeap* descriptorHeaps[] = { m_SrvDescriptorHeap.Get() };
	g_CommandList->SetDescriptorHeaps(_countof(descriptorHeaps), descriptorHeaps);
}

void GraphicsRenderer::RenderGraphics()
{
	/****************************** Render **********************************/
	g_CommandList->SetGraphicsRootSignature(m_RenderRS.Get());

	auto matBuffer = GraphicsContext::GetApp()->MaterialBuffer->Resource();
	g_CommandList->SetGraphicsRootShaderResourceView(1, matBuffer->GetGPUVirtualAddress());
	auto passCB = GraphicsContext::GetApp()->PassCB->Resource();
	g_CommandList->SetGraphicsRootConstantBufferView(2, passCB->GetGPUVirtualAddress());
	auto uiPassCB = GraphicsContext::GetApp()->UIPassCB->Resource();
	g_CommandList->SetGraphicsRootConstantBufferView(7, uiPassCB->GetGPUVirtualAddress());

	g_CommandList->SetGraphicsRootDescriptorTable(4, m_SrvDescriptorHeap->GetGPUDescriptorHandleForHeapStart());

	CD3DX12_GPU_DESCRIPTOR_HANDLE skyTexDescriptor(m_SrvDescriptorHeap->GetGPUDescriptorHandleForHeapStart());
	skyTexDescriptor.Offset(mSkyTexHeapIndex, GameCore::GetApp()->mCbvSrvUavDescriptorSize);
	g_CommandList->SetGraphicsRootDescriptorTable(3, skyTexDescriptor);

}

void GraphicsRenderer::RenderGraphicsShadow()
{
	g_CommandList->SetGraphicsRootSignature(m_RenderRS.Get());

	auto matBuffer = GraphicsContext::GetApp()->MaterialBuffer->Resource();
	g_CommandList->SetGraphicsRootShaderResourceView(1, matBuffer->GetGPUVirtualAddress());

	CD3DX12_GPU_DESCRIPTOR_HANDLE shadowTexDescriptor(m_SrvDescriptorHeap->GetGPUDescriptorHandleForHeapStart());
	shadowTexDescriptor.Offset(mShadowMapHeapIndex, GameCore::GetApp()->mCbvSrvUavDescriptorSize);
	g_CommandList->SetGraphicsRootDescriptorTable(6, shadowTexDescriptor);
}

void GraphicsRenderer::ExecuteBlurEffects()
{
	if (m_SwitchBlur) {
		mBlurFilter->Execute(Core::g_CommandList.Get(), m_PostProcessRS.Get(),
			g_HorzBlurPSO.Get(), g_VertBlurPSO.Get(), GameCore::GetApp()->CurrentBackBuffer(), 4);

		// Prepare to copy blurred output to the back buffer.
		Core::g_CommandList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(GameCore::GetApp()->CurrentBackBuffer(),
			D3D12_RESOURCE_STATE_COPY_SOURCE, D3D12_RESOURCE_STATE_COPY_DEST));

		Core::g_CommandList->CopyResource(GameCore::GetApp()->CurrentBackBuffer(), mBlurFilter->Output());
	}
}

void GraphicsRenderer::ExecuteResizeBlur()
{
	mBlurFilter->OnResize(Core::g_DisplayWidth, Core::g_DisplayHeight);
}

void GraphicsRenderer::LoadTextures()
{
	std::vector<std::string> texNames =
	{
		"grasscube1024",
		"PolyAdventureTexture_01",
		"Polygon_Fantasy_Characters_Texture_01_A",
		"Polygon_Fantasy_Characters_Texture_02_A",
		"Polygon_Fantasy_Characters_Texture_03_A",
		"Polygon_Fantasy_Characters_Texture_04_A",
		"Polygon_Fantasy_Characters_Texture_05_A",
		"UI_Board",
		"Thunderbolt",
		"UI_DU",
		"UI_BA",
		"UI_FP",
		"UI_MP",
		"UI_SO",
		"UI_BTN_START_RELEASED",
		"UI_IMAGEVIEW_MAP",
		"UI_BTN_READY_PRESSED",
		"UI_BTN_READY_RELEASED",
		"UI_BTN_READY_ACTIVATION",
		"UI_BTN_START_PRESSED",
		"UI_BTN_START_ACTIVATION",
		"UI_BTN_MAP_PREV_RELEASED",
		"UI_BTN_MAP_PREV_PRESSED",
		"UI_BTN_MAP_PREV_ACTIVATION",
		"UI_BTN_MAP_NEXT_RELEASED",
		"UI_BTN_MAP_NEXT_PRESSED",
		"UI_BTN_MAP_NEXT_ACTIVATION",
		"UI_BTN_EXIT_RELEASED",
		"UI_BTN_EXIT_PRESSED",
		"AIM",
		"Knights_Texture_01",
		"Character_Ready",
		"UI_Crown",
		"UI_HP",
		"UI_PRO_BACK",
		"UI_PRO_MASTER",
		"UI_PRO_POTION",
		"UI_PRO_BA",
		"UI_PRO_DU",
		"UI_PRO_FE",
		"UI_PRO_MA",
		"UI_PRO_SO",
		"UI_TIME_0",
		"UI_TIME_1",
		"UI_TIME_2",
		"UI_TIME_3",
		"UI_TIME_4",
		"UI_TIME_5",
		"UI_TIME_6",
		"UI_TIME_7",
		"UI_TIME_8",
		"UI_BTN_BACK_RELEASED",
		"UI_BTN_BACK_PRESSED",
		"UI_BTN_SING_IN_RELEASED",
		"UI_BTN_SING_IN_PRESSED",
		"UI_BTN_SING_UP_RELEASED",
		"UI_BTN_SING_UP_PRESSED",
		"UI_BTN_SING_UP_ACTIVATION",
		"UI_BTN_AUTOMATCH_RELEASED",
		"UI_BTN_AUTOMATCH_PRESSED",
		"UI_BTN_MANUALMATCH_RELEASED",
		"UI_BTN_MANUALMATCH_PRESSED",
		"UI_BTN_RULE_RELEASED",
		"UI_BTN_RULE_PRESSED",
		"TRANSPARENCY",
		"T_Smoke_Tiled_D",
		"P_PENALTY",
		"light_explosion",
		"LOGIN_BACK",
		"LOGIN_ID_INPUT",
		"LOGIN_PW_INPUT",
		"crush_mirror",
		"RULE_MANUAL",
		"X",
		"RENEWAL_BACK",
		"RENEWAL_ARROW",
		"LOBBY_PRO_BACK",
		"LOBBY_ROOMLIST_BACK",
		"LOBBY_ROOMLIST_R",
		"LOBBY_ROOMLIST_P",
		"LOBBY_MAKE_ROOM_R",
		"LOBBY_MAKE_ROOM_P",
		"LOBBY_MAKE_ROOM_A",
		"LOBBY_JOIN_ROOM_R",
		"LOBBY_JOIN_ROOM_P",
		"LOBBY_JOIN_ROOM_A",
		"GAME_EXIT_WINDOW",
		"BTN_YES_R",
		"BTN_YES_P",
		"BTN_NO_R",
		"BTN_NO_P",
		"MAKE_ROOM_WINDOW",
		"MAKE_ROOM_INPUT",
		"UI_TIME_ALARM_1",
		"UI_TIME_ALARM_2",
		"UI_TIME_ALARM_3",
		"UI_TIME_ALARM_4",
		"UI_TIME_ALARM_5",
		"UI_TIME_ALARM_6",
		"UI_TIME_ALARM_7",
		"UI_TIME_ALARM_8",
		"UI_TIME_ALARM_9",
		"UI_TIME_ALARM_10",
		"UI_PLAYBOARD_BACKGROUND",
		"UI_PLAYBOARD_DRU",
		"UI_PLAYBOARD_DRU_D",
		"UI_PLAYBOARD_FEA",
		"UI_PLAYBOARD_FEA_D",
		"UI_PLAYBOARD_KING",
		"UI_PLAYBOARD_KING_D",
		"UI_PLAYBOARD_LEE",
		"UI_PLAYBOARD_LEE_D",
		"UI_PLAYBOARD_MASTER",
		"UI_PLAYBOARD_RED",
		"UI_PLAYBOARD_RED_D",
		"UI_PLAYBOARD_SCROLL",
		"UI_CANCEL_R",
		"UI_CANCEL_P",
		"UI_CREATE_R",
		"UI_CREATE_P",
		"BTN_RENEWAL_R",
		"BTN_RENEWAL_P",
		"BTN_RENEWAL_U",
		"Mage_Room",
		"STAR_BACKGROUND",
		"WINNER_MASTER",
		"WINNER_STUDENT",
		"SIGNUP_WIN",
		"LOGO_CACHECACHE",
		"TIME_BACKGROUND",
		"UI_PRO_BACK_MASTER",
		"MAGIC_CIRCLE",
		"UI_BA_ORIGIN",
		"UI_DU_ORIGIN",
		"UI_SO_ORIGIN",
		"UI_FE_ORIGIN",
		"UI_MA_ORIGIN",
		"AUTOMATCH_LOADING_1",
		"AUTOMATCH_LOADING_2",
		"AUTOMATCH_LOADING_3",
		"AUTOMATCH_LOADING_4",
		"AUTOMATCH_LOADING_5",
		"AUTOMATCH_LOADING_6",
		"AUTOMATCH_LOADING_7",
		"AUTOMATCH_LOADING_8",
		"AUTOMATCH_ACTIVATION",
		"MANUALMATCH_ACTIVATION",
		"P_CHERRY_BLOSSOM",
		"MAGIC_CIRCLE_GREEN",
	};

	std::vector<std::wstring> texFilenames =
	{
		L"./Textures/grasscube1024.dds",
		L"./Textures/PolyAdventureTexture_01.dds",
		L"./Textures/Polygon_Fantasy_Characters_Texture_01_A.dds",
		L"./Textures/Polygon_Fantasy_Characters_Texture_02_A.dds",
		L"./Textures/Polygon_Fantasy_Characters_Texture_03_A.dds",
		L"./Textures/Polygon_Fantasy_Characters_Texture_04_A.dds",
		L"./Textures/Polygon_Fantasy_Characters_Texture_05_A.dds",
		L"./Textures/UI_Board.dds",
		L"./Textures/Thunderbolt.dds",
		L"./Textures/GameRoomUI/UI_DU.dds",
		L"./Textures/GameRoomUI/UI_BA.dds",
		L"./Textures/GameRoomUI/UI_FP.dds",
		L"./Textures/GameRoomUI/UI_MP.dds",
		L"./Textures/GameRoomUI/UI_SO.dds",
		L"./Textures/GameRoomUI/Start/btn_start_released.dds",
		L"./Textures/GameRoomUI/UI_MAP.dds",
		L"./Textures/GameRoomUI/Ready/btn_ready_pressed.dds",
		L"./Textures/GameRoomUI/Ready/btn_ready_released.dds",
		L"./Textures/GameRoomUI/Ready/btn_ready_activation.dds",
		L"./Textures/GameRoomUI/Start/btn_start_pressed.dds",
		L"./Textures/GameRoomUI/Start/btn_start_activation.dds",
		L"./Textures/GameRoomUI/MapButton/btn_mapPrev_released.dds",
		L"./Textures/GameRoomUI/MapButton/btn_mapPrev_pressed.dds",
		L"./Textures/GameRoomUI/MapButton/btn_mapPrev_activation.dds",
		L"./Textures/GameRoomUI/MapButton/btn_mapNext_released.dds",
		L"./Textures/GameRoomUI/MapButton/btn_mapNext_pressed.dds",
		L"./Textures/GameRoomUI/MapButton/btn_mapNext_activation.dds",
		L"./Textures/GameRoomUI/Exit/exit_released.dds",
		L"./Textures/GameRoomUI/Exit/exit_pressed.dds",
		L"./Textures/aim.dds",
		L"./Textures/Knights_Texture_01.dds",
		L"./Textures/GameRoomUI/Ready/Character_Ready.dds",
		L"./Textures/GameRoomUI/UI_Crown.dds",
		L"./Textures/Ingame/UI_HP.dds",
		L"./Textures/Ingame/UI_Profile_Back.dds",
		L"./Textures/Ingame/UI_PRO_MASTER.dds",
		L"./Textures/Ingame/UI_POTION.dds",
		L"./Textures/Ingame/UI_PRO_BA.dds",
		L"./Textures/Ingame/UI_PRO_DU.dds",
		L"./Textures/Ingame/UI_PRO_FE.dds",
		L"./Textures/Ingame/UI_PRO_MA.dds",
		L"./Textures/Ingame/UI_PRO_SO.dds",
		L"./Textures/Ingame/Timer/UI_0.dds",
		L"./Textures/Ingame/Timer/UI_1.dds",
		L"./Textures/Ingame/Timer/UI_2.dds",
		L"./Textures/Ingame/Timer/UI_3.dds",
		L"./Textures/Ingame/Timer/UI_4.dds",
		L"./Textures/Ingame/Timer/UI_5.dds",
		L"./Textures/Ingame/Timer/UI_6.dds",
		L"./Textures/Ingame/Timer/UI_7.dds",
		L"./Textures/Ingame/Timer/UI_8.dds",
		L"./Textures/GameResult/btn_back_released.dds",
		L"./Textures/GameResult/btn_back_pressed.dds",
		L"./Textures/Title/signin_a.dds",
		L"./Textures/Title/signin_P.dds",
		L"./Textures/Title/signUP_a.dds",
		L"./Textures/Title/signUP_P.dds",
		L"./Textures/Title/signUP_r.dds",
		L"./Textures/Lobby/automatch_a.dds",
		L"./Textures/Lobby/automatch_p.dds",
		L"./Textures/Lobby/manualmatch_a.dds",
		L"./Textures/Lobby/manualmatch_p.dds",
		L"./Textures/Lobby/rule_a.dds",
		L"./Textures/Lobby/rule_p.dds",
		L"./Textures/Lobby/RoomList/roomlistBlur.dds",
		L"./Textures/T_Smoke_Tiled_D.dds",
		L"./Textures/Ingame/Particle_Penalty.dds",
		L"./Textures/Ingame/light_explosion.dds",
		L"./Textures/Title/new_sign_in_win.dds",
		L"./Textures/Title/id_Input.dds",
		L"./Textures/Title/password_Input.dds",
		L"./Textures/Ingame/crush_mirror.dds",
		L"./Textures/Lobby/rule_manual.dds",
		L"./Textures/Lobby/x_p.dds",
		L"./Textures/Lobby/re_arrow_back.dds",
		L"./Textures/Lobby/re_arrow.dds",
		L"./Textures/Lobby/lobbyProBack.dds",
		L"./Textures/Lobby/RoomList/roomlistback.dds",
		L"./Textures/Lobby/RoomList/roomlist.dds",
		L"./Textures/Lobby/RoomList/roomlist_s.dds",
		L"./Textures/Lobby/RoomList/makeroom_a.dds",
		L"./Textures/Lobby/RoomList/makeroom_s.dds",
		L"./Textures/Lobby/RoomList/makeroom_un.dds",
		L"./Textures/Lobby/RoomList/joinroom_a.dds",
		L"./Textures/Lobby/RoomList/joinroom_s.dds",
		L"./Textures/Lobby/RoomList/joinroom_un.dds",
		L"./Textures/Lobby/exit_window.dds",
		L"./Textures/Button/yes_a.dds",
		L"./Textures/Button/yes_S.dds",
		L"./Textures/Button/no_a.dds",
		L"./Textures/Button/no_S.dds",
		L"./Textures/Lobby/RoomList/makeroom/makeroom_window.dds",
		L"./Textures/Lobby/RoomList/makeroom/makeroom_title.dds",
		L"./Textures/Ingame/Timer/UI_a1.dds",
		L"./Textures/Ingame/Timer/UI_a2.dds",
		L"./Textures/Ingame/Timer/UI_a3.dds",
		L"./Textures/Ingame/Timer/UI_a4.dds",
		L"./Textures/Ingame/Timer/UI_a5.dds",
		L"./Textures/Ingame/Timer/UI_a6.dds",
		L"./Textures/Ingame/Timer/UI_a7.dds",
		L"./Textures/Ingame/Timer/UI_a8.dds",
		L"./Textures/Ingame/Timer/UI_a9.dds",
		L"./Textures/Ingame/Timer/UI_a10.dds",
		L"./Textures/Ingame/playboard/playerboard_background.dds",
		L"./Textures/Ingame/playboard/playerboard_dru.dds",
		L"./Textures/Ingame/playboard/playerboard_dru_d.dds",
		L"./Textures/Ingame/playboard/playerboard_fea.dds",
		L"./Textures/Ingame/playboard/playerboard_fea_d.dds",
		L"./Textures/Ingame/playboard/playerboard_king.dds",
		L"./Textures/Ingame/playboard/playerboard_king_d.dds",
		L"./Textures/Ingame/playboard/playerboard_lee.dds",
		L"./Textures/Ingame/playboard/playerboard_lee_d.dds",
		L"./Textures/Ingame/playboard/playerboard_master.dds",
		L"./Textures/Ingame/playboard/playerboard_red.dds",
		L"./Textures/Ingame/playboard/playerboard_red_d.dds",
		L"./Textures/Ingame/playboard/playerboard_scroll.dds",
		L"./Textures/Lobby/RoomList/makeroom/CANCELa.dds",
		L"./Textures/Lobby/RoomList/makeroom/CANCELP.dds",
		L"./Textures/Lobby/RoomList/makeroom/CREATE_a.dds",
		L"./Textures/Lobby/RoomList/makeroom/CREATE_P.dds",
		L"./Textures/Lobby/RoomList/renewal_a.dds",
		L"./Textures/Lobby/RoomList/renewal_s.dds",
		L"./Textures/Lobby/RoomList/renewal_un.dds",
		L"./Textures/Lobby/Mage_Room.dds",
		L"./Textures/Title/starbackground.dds",
		L"./Textures/GameResult/winner_pro.dds",
		L"./Textures/GameResult/winner_stu.dds",
		L"./Textures/Title/new_sign_up_win.dds",
		L"./Textures/Title/cachecache_logo.dds",
		L"./Textures/Ingame/time_bg.dds",
		L"./Textures/Ingame/UI_Profile_Back_Master.dds",
		L"./Textures/magic_circle.dds",
		L"./Textures/GameRoomUI/UI_BA_ORIGIN.dds",
		L"./Textures/GameRoomUI/UI_DU_ORIGIN.dds",
		L"./Textures/GameRoomUI/UI_SO_ORIGIN.dds",
		L"./Textures/GameRoomUI/UI_FE_ORIGIN.dds",
		L"./Textures/GameRoomUI/UI_MA_ORIGIN.dds",
		L"./Textures/Lobby/Loading/1.dds",
		L"./Textures/Lobby/Loading/2.dds",
		L"./Textures/Lobby/Loading/3.dds",
		L"./Textures/Lobby/Loading/4.dds",
		L"./Textures/Lobby/Loading/5.dds",
		L"./Textures/Lobby/Loading/6.dds",
		L"./Textures/Lobby/Loading/7.dds",
		L"./Textures/Lobby/Loading/8.dds",
		L"./Textures/Lobby/automatch_un.dds",
		L"./Textures/Lobby/manualmatch_un.dds",
		L"./Textures/cherry_blossom.dds",
		L"./Textures/magic_circle_green.dds",
	};

	for (int i = 0; i < (int)texNames.size(); ++i)
	{
		auto texMap = std::make_unique<Texture>();
		texMap->Name = texNames[i];
		texMap->Filename = texFilenames[i];
		ThrowIfFailed(DirectX::CreateDDSTextureFromFile12(g_Device.Get(),
			g_CommandList.Get(), texMap->Filename.c_str(),
			texMap->Resource, texMap->UploadHeap));

		m_Textures[texMap->Name] = std::move(texMap);
	}
}

void GraphicsRenderer::BuildDescriptorHeaps()
{
	constexpr int blurDescriptorCount = 4;
	constexpr int skyboxDescriptorCount = 1;
	constexpr int shadowMapDescriptorCount = 1;

	//
	// Create the SRV heap.
	//
	D3D12_DESCRIPTOR_HEAP_DESC srvHeapDesc = {};
	srvHeapDesc.NumDescriptors = m_Textures.size() + skyboxDescriptorCount + shadowMapDescriptorCount + blurDescriptorCount;
	srvHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
	srvHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
	ThrowIfFailed(Core::g_Device->CreateDescriptorHeap(&srvHeapDesc, IID_PPV_ARGS(&m_SrvDescriptorHeap)));

	//
	// Fill out the heap with actual descriptors.
	//
	CD3DX12_CPU_DESCRIPTOR_HANDLE hDescriptor(m_SrvDescriptorHeap->GetCPUDescriptorHandleForHeapStart());

	std::vector<ComPtr<ID3D12Resource>> tex2DList =
	{
		m_Textures["PolyAdventureTexture_01"]->Resource,
		m_Textures[TEXTURE_STR_Polygon_Fantasy_Characters_Texture_01_A]->Resource,
		m_Textures[TEXTURE_STR_Polygon_Fantasy_Characters_Texture_02_A]->Resource,
		m_Textures[TEXTURE_STR_Polygon_Fantasy_Characters_Texture_03_A]->Resource,
		m_Textures[TEXTURE_STR_Polygon_Fantasy_Characters_Texture_04_A]->Resource,
		m_Textures[TEXTURE_STR_Polygon_Fantasy_Characters_Texture_05_A]->Resource,
		m_Textures[TEXTURE_STR_UI_BOARD]->Resource,
		m_Textures[TEXTURE_STR_Thunderbolt]->Resource,
		m_Textures[TEXTURE_STR_UI_IMAGEIVEW_DU]->Resource,
		m_Textures[TEXTURE_STR_UI_IMAGEIVEW_BA]->Resource,
		m_Textures[TEXTURE_STR_UI_IMAGEIVEW_FP]->Resource,
		m_Textures[TEXTURE_STR_UI_IMAGEIVEW_MP]->Resource,
		m_Textures[TEXTURE_STR_UI_IMAGEIVEW_SO]->Resource,
		m_Textures[TEXTURE_STR_UI_BUTTON_START_RELEASED]->Resource,
		m_Textures[TEXTURE_STR_UI_IMAGEVIEW_MAP]->Resource,
		m_Textures[TEXTURE_STR_UI_BUTTON_READY_PRESSED]->Resource,
		m_Textures[TEXTURE_STR_UI_BUTTON_READY_RELEASED]->Resource,
		m_Textures[TEXTURE_STR_UI_BUTTON_READY_ACTIVATION]->Resource,
		m_Textures[TEXTURE_STR_UI_BUTTON_START_PRESSED]->Resource,
		m_Textures[TEXTURE_STR_UI_BUTTON_START_ACTIVATION]->Resource,
		m_Textures[TEXTURE_STR_UI_BUTTON_MAP_PREV_RELEASED]->Resource,
		m_Textures[TEXTURE_STR_UI_BUTTON_MAP_PREV_PRESSED]->Resource,
		m_Textures[TEXTURE_STR_UI_BUTTON_MAP_PREV_ACTIVATION]->Resource,
		m_Textures[TEXTURE_STR_UI_BUTTON_MAP_NEXT_RELEASED]->Resource,
		m_Textures[TEXTURE_STR_UI_BUTTON_MAP_NEXT_PRESSED]->Resource,
		m_Textures[TEXTURE_STR_UI_BUTTON_MAP_NEXT_ACTIVATION]->Resource,
		m_Textures[TEXTURE_STR_UI_BUTTON_EXIT_RELEASED]->Resource,
		m_Textures[TEXTURE_STR_UI_BUTTON_EXIT_PRESSED]->Resource,
		m_Textures[TEXTURE_STR_AIM]->Resource,
		m_Textures[TEXTURE_STR_KNIGHTS_TEXTURE_01]->Resource,
		m_Textures[TEXTURE_STR_UI_CHARACTER_READY]->Resource,
		m_Textures[TEXTURE_STR_UI_CROWN]->Resource,
		m_Textures[TEXTURE_STR_UI_HP]->Resource,
		m_Textures[TEXTURE_STR_UI_PRO_BACK]->Resource,
		m_Textures[TEXTURE_STR_UI_PRO_MASTER]->Resource,
		m_Textures[TEXTURE_STR_UI_PRO_POTION]->Resource,
		m_Textures[TEXTURE_STR_UI_PRO_BA]->Resource,
		m_Textures[TEXTURE_STR_UI_PRO_DU]->Resource,
		m_Textures[TEXTURE_STR_UI_PRO_FE]->Resource,
		m_Textures[TEXTURE_STR_UI_PRO_MA]->Resource,
		m_Textures[TEXTURE_STR_UI_PRO_SO]->Resource,
		m_Textures[TEXTURE_STR_UI_TIME_0]->Resource,
		m_Textures[TEXTURE_STR_UI_TIME_1]->Resource,
		m_Textures[TEXTURE_STR_UI_TIME_2]->Resource,
		m_Textures[TEXTURE_STR_UI_TIME_3]->Resource,
		m_Textures[TEXTURE_STR_UI_TIME_4]->Resource,
		m_Textures[TEXTURE_STR_UI_TIME_5]->Resource,
		m_Textures[TEXTURE_STR_UI_TIME_6]->Resource,
		m_Textures[TEXTURE_STR_UI_TIME_7]->Resource,
		m_Textures[TEXTURE_STR_UI_TIME_8]->Resource,
		m_Textures[TEXTURE_STR_BUTTON_BACK_RELEASED]->Resource,
		m_Textures[TEXTURE_STR_BUTTON_BACK_PRESSED]->Resource,
		m_Textures[TEXTURE_STR_UI_BUTTON_TITLE_SING_IN_RELEASED]->Resource,
		m_Textures[TEXTURE_STR_UI_BUTTON_TITLE_SING_IN_PRESSED]->Resource,
		m_Textures[TEXTURE_STR_UI_BUTTON_TITLE_SING_UP_RELEASED]->Resource,
		m_Textures[TEXTURE_STR_UI_BUTTON_TITLE_SING_UP_PRESSED]->Resource,
		m_Textures[TEXTURE_STR_UI_BUTTON_TITLE_SING_UP_ACTIVATION]->Resource,
		m_Textures[TEXTURE_STR_UI_BUTTON_LOBBY_AUTOMATCH_RELEASED]->Resource,
		m_Textures[TEXTURE_STR_UI_BUTTON_LOBBY_AUTOMATCH_PRESSED]->Resource,
		m_Textures[TEXTURE_STR_UI_BUTTON_LOBBY_MANUALMATCH_RELEASED]->Resource,
		m_Textures[TEXTURE_STR_UI_BUTTON_LOBBY_MANUALMATCH_PRESSED]->Resource,
		m_Textures[TEXTURE_STR_UI_BUTTON_LOBBY_RULE_RELEASED]->Resource,
		m_Textures[TEXTURE_STR_UI_BUTTON_LOBBY_RULE_PRESSED]->Resource,
		m_Textures[TEXTURE_STR_TRANSPARENCY]->Resource,
		m_Textures[TEXTURE_STR_T_Smoke_Tiled_D]->Resource,
		m_Textures[TEXTURE_STR_P_PENALTY]->Resource,
		m_Textures[TEXTURE_STR_P_LIGHT_EXPLOSION]->Resource,
		m_Textures[TEXTURE_STR_UI_LOGIN_BACK]->Resource,
		m_Textures[TEXTURE_STR_UI_LOGIN_ID_INPUT]->Resource,
		m_Textures[TEXTURE_STR_UI_LOGIN_PW_INPUT]->Resource,
		m_Textures[TEXTURE_STR_CRUSH_MIRROR]->Resource,
		m_Textures[TEXTURE_STR_UI_RULE_MANUAL]->Resource,
		m_Textures[TEXTURE_STR_UI_X]->Resource,
		m_Textures[TEXTURE_STR_UI_RENEWAL_BACK]->Resource,
		m_Textures[TEXTURE_STR_UI_RENEWAL_ARROW]->Resource,
		m_Textures[TEXTURE_STR_UI_LOBBY_PRO_BACK]->Resource,
		m_Textures[TEXTURE_STR_UI_LOBBY_ROOMLIST_BACK]->Resource,
		m_Textures[TEXTURE_STR_UI_LOBBY_ROOMLIST_R]->Resource,
		m_Textures[TEXTURE_STR_UI_LOBBY_ROOMLIST_P]->Resource,
		m_Textures[TEXTURE_STR_UI_LOBBY_MAKE_ROOM_R]->Resource,
		m_Textures[TEXTURE_STR_UI_LOBBY_MAKE_ROOM_P]->Resource,
		m_Textures[TEXTURE_STR_UI_LOBBY_MAKE_ROOM_A]->Resource,
		m_Textures[TEXTURE_STR_UI_LOBBY_JOIN_ROOM_R]->Resource,
		m_Textures[TEXTURE_STR_UI_LOBBY_JOIN_ROOM_P]->Resource,
		m_Textures[TEXTURE_STR_UI_LOBBY_JOIN_ROOM_A]->Resource,
		m_Textures[TEXTURE_STR_UI_GAME_EXIT_WINDOW]->Resource,
		m_Textures[TEXTURE_STR_UI_BTN_YES_R]->Resource,
		m_Textures[TEXTURE_STR_UI_BTN_YES_P]->Resource,
		m_Textures[TEXTURE_STR_UI_BTN_NO_R]->Resource,
		m_Textures[TEXTURE_STR_UI_BTN_NO_P]->Resource,
		m_Textures[TEXTURE_STR_UI_MAKE_ROOM_WINDOW]->Resource,
		m_Textures[TEXTURE_STR_UI_MAKE_ROOM_INPUT]->Resource,
		m_Textures[TEXTURE_STR_UI_TIME_ALARM_1]->Resource,
		m_Textures[TEXTURE_STR_UI_TIME_ALARM_2]->Resource,
		m_Textures[TEXTURE_STR_UI_TIME_ALARM_3]->Resource,
		m_Textures[TEXTURE_STR_UI_TIME_ALARM_4]->Resource,
		m_Textures[TEXTURE_STR_UI_TIME_ALARM_5]->Resource,
		m_Textures[TEXTURE_STR_UI_TIME_ALARM_6]->Resource,
		m_Textures[TEXTURE_STR_UI_TIME_ALARM_7]->Resource,
		m_Textures[TEXTURE_STR_UI_TIME_ALARM_8]->Resource,
		m_Textures[TEXTURE_STR_UI_TIME_ALARM_9]->Resource,
		m_Textures[TEXTURE_STR_UI_TIME_ALARM_10]->Resource,
		m_Textures[TEXTURE_STR_UI_PLAYBOARD_BACKGROUND]->Resource,
		m_Textures[TEXTURE_STR_UI_PLAYBOARD_DRU]->Resource,
		m_Textures[TEXTURE_STR_UI_PLAYBOARD_DRU_D]->Resource,
		m_Textures[TEXTURE_STR_UI_PLAYBOARD_FEA]->Resource,
		m_Textures[TEXTURE_STR_UI_PLAYBOARD_FEA_D]->Resource,
		m_Textures[TEXTURE_STR_UI_PLAYBOARD_KING]->Resource,
		m_Textures[TEXTURE_STR_UI_PLAYBOARD_KING_D]->Resource,
		m_Textures[TEXTURE_STR_UI_PLAYBOARD_LEE]->Resource,
		m_Textures[TEXTURE_STR_UI_PLAYBOARD_LEE_D]->Resource,
		m_Textures[TEXTURE_STR_UI_PLAYBOARD_MASTER]->Resource,
		m_Textures[TEXTURE_STR_UI_PLAYBOARD_RED]->Resource,
		m_Textures[TEXTURE_STR_UI_PLAYBOARD_RED_D]->Resource,
		m_Textures[TEXTURE_STR_UI_PLAYBOARD_SCROLL]->Resource,
		m_Textures[TEXTURE_STR_UI_CANCEL_R]->Resource,
		m_Textures[TEXTURE_STR_UI_CANCEL_P]->Resource,
		m_Textures[TEXTURE_STR_UI_CREATE_R]->Resource,
		m_Textures[TEXTURE_STR_UI_CREATE_P]->Resource,
		m_Textures[TEXTURE_STR_UI_BTN_RENEWAL_R]->Resource,
		m_Textures[TEXTURE_STR_UI_BTN_RENEWAL_P]->Resource,
		m_Textures[TEXTURE_STR_UI_BTN_RENEWAL_U]->Resource,
		m_Textures[TEXTURE_STR_Mage_Room]->Resource,
		m_Textures[TEXTURE_STR_STAR_BACKGROUND]->Resource,
		m_Textures[TEXTURE_STR_WINNER_MASTER]->Resource,
		m_Textures[TEXTURE_STR_WINNER_STUDENT]->Resource,
		m_Textures[TEXTURE_STR_SIGNUP_WIN]->Resource,
		m_Textures[TEXTURE_STR_LOGO_CACHECACHE]->Resource,
		m_Textures[TEXTURE_STR_TIME_BACKGROUND]->Resource,
		m_Textures[TEXTURE_STR_PRO_BACK_MASTER]->Resource,
		m_Textures[TEXTURE_STR_MAGIC_CIRCLE]->Resource,
		m_Textures[TEXTURE_STR_BA_ORIGIN]->Resource,
		m_Textures[TEXTURE_STR_DU_ORIGIN]->Resource,
		m_Textures[TEXTURE_STR_SO_ORIGIN]->Resource,
		m_Textures[TEXTURE_STR_FE_ORIGIN]->Resource,
		m_Textures[TEXTURE_STR_MA_ORIGIN]->Resource,
		m_Textures[TEXTURE_STR_AUTOMATCH_LOADING_1]->Resource,
		m_Textures[TEXTURE_STR_AUTOMATCH_LOADING_2]->Resource,
		m_Textures[TEXTURE_STR_AUTOMATCH_LOADING_3]->Resource,
		m_Textures[TEXTURE_STR_AUTOMATCH_LOADING_4]->Resource,
		m_Textures[TEXTURE_STR_AUTOMATCH_LOADING_5]->Resource,
		m_Textures[TEXTURE_STR_AUTOMATCH_LOADING_6]->Resource,
		m_Textures[TEXTURE_STR_AUTOMATCH_LOADING_7]->Resource,
		m_Textures[TEXTURE_STR_AUTOMATCH_LOADING_8]->Resource,
		m_Textures[TEXTURE_STR_AUTOMATCH_ACTIVATION]->Resource,
		m_Textures[TEXTURE_STR_MANUALMATCH_ACTIVATION]->Resource,
		m_Textures[TEXTURE_STR_P_CHERRY_BLOSSOM]->Resource,
		m_Textures[TEXTURE_STR_MAGIC_CIRCLE_GREEN]->Resource,
	};

	auto grasscube1024 = m_Textures["grasscube1024"]->Resource;

	D3D12_SHADER_RESOURCE_VIEW_DESC srvDesc = {};
	srvDesc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
	// SkyCube
	srvDesc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURECUBE;
	srvDesc.TextureCube.MostDetailedMip = 0;
	srvDesc.TextureCube.MipLevels = grasscube1024->GetDesc().MipLevels;
	srvDesc.TextureCube.ResourceMinLODClamp = 0.0f;
	srvDesc.Format = grasscube1024->GetDesc().Format;
	Core::g_Device->CreateShaderResourceView(grasscube1024.Get(), &srvDesc, hDescriptor);
	//hDescriptor.Offset(1, GameCore::GetApp()->mCbvSrvUavDescriptorSize);

	// Textures
	srvDesc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE2D;
	srvDesc.Texture2D.MostDetailedMip = 0;
	srvDesc.Texture2D.ResourceMinLODClamp = 0.0f;

	for (UINT i = 0; i < (UINT)tex2DList.size(); ++i)
	{
		hDescriptor.Offset(1, GameCore::GetApp()->mCbvSrvUavDescriptorSize);

		srvDesc.Format = tex2DList[i]->GetDesc().Format;
		srvDesc.Texture2D.MipLevels = tex2DList[i]->GetDesc().MipLevels;
		Core::g_Device->CreateShaderResourceView(tex2DList[i].Get(), &srvDesc, hDescriptor);

		// next descriptor
	}

	mSkyTexHeapIndex = 0;
	mShadowMapHeapIndex = (UINT)tex2DList.size() + 1;
	mBlurHeapIndex = mShadowMapHeapIndex + 1;

	auto srvCpuStart = m_SrvDescriptorHeap->GetCPUDescriptorHandleForHeapStart();
	auto srvGpuStart = m_SrvDescriptorHeap->GetGPUDescriptorHandleForHeapStart();
	auto dsvCpuStart = GameCore::GetApp()->mDsvHeap->GetCPUDescriptorHandleForHeapStart();

	mShadowMap->BuildDescriptors(
		CD3DX12_CPU_DESCRIPTOR_HANDLE(srvCpuStart, mShadowMapHeapIndex, GameCore::GetApp()->mCbvSrvUavDescriptorSize),
		CD3DX12_GPU_DESCRIPTOR_HANDLE(srvGpuStart, mShadowMapHeapIndex, GameCore::GetApp()->mCbvSrvUavDescriptorSize),
		CD3DX12_CPU_DESCRIPTOR_HANDLE(dsvCpuStart, 1, GameCore::GetApp()->mDsvDescriptorSize));

	// 3개 할당
	mBlurFilter->BuildDescriptors(
		CD3DX12_CPU_DESCRIPTOR_HANDLE(m_SrvDescriptorHeap->GetCPUDescriptorHandleForHeapStart(), mBlurHeapIndex, GameCore::GetApp()->mCbvSrvUavDescriptorSize),
		CD3DX12_GPU_DESCRIPTOR_HANDLE(m_SrvDescriptorHeap->GetGPUDescriptorHandleForHeapStart(), mBlurHeapIndex, GameCore::GetApp()->mCbvSrvUavDescriptorSize),
		GameCore::GetApp()->mCbvSrvUavDescriptorSize);
}

void GraphicsRenderer::BuildShaderAndInputLayout()
{
	const D3D_SHADER_MACRO alphaTestDefines[] =
	{
		"ALPHA_TEST", "1",
		NULL, NULL
	};

	const D3D_SHADER_MACRO skinnedDefines[] =
	{
		"SKINNED", "1",
		NULL, NULL
	};

	const D3D_SHADER_MACRO hpDefines[] =
	{
		"HP", "1",
		NULL, NULL
	};

	m_Shaders["standardVS"] = d3dUtil::CompileShader(L"Shaders\\Default.hlsl", nullptr, "VS", "vs_5_1");
	m_Shaders["skinnedVS"] = d3dUtil::CompileShader(L"Shaders\\Default.hlsl", skinnedDefines, "VS", "vs_5_1");
	m_Shaders["opaquePS"] = d3dUtil::CompileShader(L"Shaders\\Default.hlsl", nullptr, "PS", "ps_5_1");

	m_Shaders["contourVS"] = d3dUtil::CompileShader(L"Shaders\\Contour.hlsl", nullptr, "VS", "vs_5_1");
	m_Shaders["contourPS"] = d3dUtil::CompileShader(L"Shaders\\Contour.hlsl", nullptr, "PS", "ps_5_1");

	m_Shaders["skyVS"] = d3dUtil::CompileShader(L"Shaders\\Sky.hlsl", nullptr, "VS", "vs_5_1");
	m_Shaders["skyPS"] = d3dUtil::CompileShader(L"Shaders\\Sky.hlsl", nullptr, "PS", "ps_5_1");

	m_Shaders["uiVS"] = d3dUtil::CompileShader(L"Shaders\\UI.hlsl", nullptr, "VS", "vs_5_1");
	m_Shaders["uiPS"] = d3dUtil::CompileShader(L"Shaders\\UI.hlsl", nullptr, "PS", "ps_5_1");
	m_Shaders["hpPS"] = d3dUtil::CompileShader(L"Shaders\\UI.hlsl", hpDefines, "PS", "ps_5_1");

	m_Shaders["shadowVS"] = d3dUtil::CompileShader(L"Shaders\\Shadows.hlsl", nullptr, "VS", "vs_5_1");
	m_Shaders["skinnedShadowVS"] = d3dUtil::CompileShader(L"Shaders\\Shadows.hlsl", skinnedDefines, "VS", "vs_5_1");
	m_Shaders["shadowOpaquePS"] = d3dUtil::CompileShader(L"Shaders\\Shadows.hlsl", nullptr, "PS", "ps_5_1");
	m_Shaders["shadowAlphaTestedPS"] = d3dUtil::CompileShader(L"Shaders\\Shadows.hlsl", alphaTestDefines, "PS", "ps_5_1");

	m_Shaders["horzBlurCS"] = d3dUtil::CompileShader(L"Shaders\\Blur.hlsl", nullptr, "HorzBlurCS", "cs_5_0");
	m_Shaders["vertBlurCS"] = d3dUtil::CompileShader(L"Shaders\\Blur.hlsl", nullptr, "VertBlurCS", "cs_5_0");

	// Billboards
	m_Shaders["particleVS"] = d3dUtil::CompileShader(L"Shaders\\Particle.hlsl", nullptr, "VS", "vs_5_1");
	m_Shaders["particleGS"] = d3dUtil::CompileShader(L"Shaders\\Particle.hlsl", nullptr, "GS", "gs_5_1");
	m_Shaders["particlePS"] = d3dUtil::CompileShader(L"Shaders\\Particle.hlsl", nullptr, "PS", "ps_5_1");

	m_Instancing_InputLayout =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 24, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
		{ "TANGENT", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 32, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
		{ "BINORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 44, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 }
	};

	m_Skinned_InputLayout =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 24, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
		{ "TANGENT", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 32, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
		{ "BINORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 44, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
		{ "WEIGHTS", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 56, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
		{ "BONEINDICES", 0, DXGI_FORMAT_R8G8B8A8_UINT, 0, 68, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 }
	};

	m_Billboard_InputLayout =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
		{ "SIZE", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 12, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
		{ "VELOCITY", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 20, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
		{ "STARTTIME", 0, DXGI_FORMAT_R32_FLOAT, 0, 32, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
		{ "LIFETIME", 0, DXGI_FORMAT_R32_FLOAT, 0, 36, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
		{ "PERIOD", 0, DXGI_FORMAT_R32_FLOAT, 0, 40, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
		{ "AMPLIFIER", 0, DXGI_FORMAT_R32_FLOAT, 0, 44, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
	};

	m_UI_InputLayout =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 24, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
	};
}

void GraphicsRenderer::BuildRootSignatures()
{
	CD3DX12_DESCRIPTOR_RANGE texTable0;
	texTable0.Init(D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 1, 0, 0);

	CD3DX12_DESCRIPTOR_RANGE texTable1;
	texTable1.Init(D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 148, 2, 0);
	CD3DX12_DESCRIPTOR_RANGE texTable2;
	texTable2.Init(D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 1, 1, 0);

	CD3DX12_ROOT_PARAMETER slotRootParameter[8];

	// 성능 팁 : 사용빈도가 높은것에서 낮은 것의 순서로 나열한다.
	/* Shader Register*/
	// Space 0
	// Texture(t1~8), passCB(b0), passCB(b1)
	// Space 1(구조체)
	// Instancing(t0), Material(t1)

	/* RootParameter slot*/
	// 0: Instancing / 1: Material / 2: passCB / 3: texMap / 4: texture / 5: SkinnedData(Bone)
	slotRootParameter[0].InitAsShaderResourceView(0, 1); // Instancing
	slotRootParameter[1].InitAsShaderResourceView(1, 1); // Material
	slotRootParameter[2].InitAsConstantBufferView(0); // PassCB
	slotRootParameter[3].InitAsDescriptorTable(1, &texTable0, D3D12_SHADER_VISIBILITY_PIXEL); // sky / shadow(nullSrv)
	slotRootParameter[4].InitAsDescriptorTable(1, &texTable1, D3D12_SHADER_VISIBILITY_PIXEL); // textures, blur
	slotRootParameter[5].InitAsConstantBufferView(1); // bones
	slotRootParameter[6].InitAsDescriptorTable(1, &texTable2, D3D12_SHADER_VISIBILITY_PIXEL); // shadow
	slotRootParameter[7].InitAsConstantBufferView(2); // uiPassCBParams
	// 8: particleVertex SRV
	// 9: particleStream SRV

	auto staticSamplers = GetStaticSamplers();

	// A root signature is an array of root parameters.
	CD3DX12_ROOT_SIGNATURE_DESC rootSigDesc(8, slotRootParameter,
		(UINT)staticSamplers.size(), staticSamplers.data(),
		D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT);

	// create a root signature with a single slot which points to a descriptor range consisting of a single constant buffer
	ComPtr<ID3DBlob> serializedRootSig = nullptr;
	ComPtr<ID3DBlob> errorBlob = nullptr;
	HRESULT hr = D3D12SerializeRootSignature(&rootSigDesc, D3D_ROOT_SIGNATURE_VERSION_1,
		serializedRootSig.GetAddressOf(), errorBlob.GetAddressOf());

	if (errorBlob != nullptr)
	{
		::OutputDebugStringA((char*)errorBlob->GetBufferPointer());
	}
	ThrowIfFailed(hr);

	ThrowIfFailed(g_Device->CreateRootSignature(
		0,
		serializedRootSig->GetBufferPointer(),
		serializedRootSig->GetBufferSize(),
		IID_PPV_ARGS(&m_RenderRS)));
}

void GraphicsRenderer::BuildPostProcessRootSignature()
{
	CD3DX12_DESCRIPTOR_RANGE srvTable;
	srvTable.Init(D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 1, 0);

	CD3DX12_DESCRIPTOR_RANGE uavTable;
	uavTable.Init(D3D12_DESCRIPTOR_RANGE_TYPE_UAV, 1, 0);

	// Root parameter can be a table, root descriptor or root constants.
	CD3DX12_ROOT_PARAMETER slotRootParameter[3];

	// Perfomance TIP: Order from most frequent to least frequent.
	slotRootParameter[0].InitAsConstants(12, 0);
	slotRootParameter[1].InitAsDescriptorTable(1, &srvTable);
	slotRootParameter[2].InitAsDescriptorTable(1, &uavTable);

	// A root signature is an array of root parameters.
	CD3DX12_ROOT_SIGNATURE_DESC rootSigDesc(3, slotRootParameter,
		0, nullptr,
		D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT);

	// create a root signature with a single slot which points to a descriptor range consisting of a single constant buffer
	ComPtr<ID3DBlob> serializedRootSig = nullptr;
	ComPtr<ID3DBlob> errorBlob = nullptr;
	HRESULT hr = D3D12SerializeRootSignature(&rootSigDesc, D3D_ROOT_SIGNATURE_VERSION_1,
		serializedRootSig.GetAddressOf(), errorBlob.GetAddressOf());

	if (errorBlob != nullptr)
	{
		::OutputDebugStringA((char*)errorBlob->GetBufferPointer());
	}
	ThrowIfFailed(hr);

	ThrowIfFailed(g_Device->CreateRootSignature(
		0,
		serializedRootSig->GetBufferPointer(),
		serializedRootSig->GetBufferSize(),
		IID_PPV_ARGS(&m_PostProcessRS)));
}

void GraphicsRenderer::BuildPipelineStateObjects()
{
	//
	// PSO for opaque objects.
	//
	D3D12_GRAPHICS_PIPELINE_STATE_DESC opaquePsoDesc;
	ZeroMemory(&opaquePsoDesc, sizeof(D3D12_GRAPHICS_PIPELINE_STATE_DESC));
	opaquePsoDesc.InputLayout = { m_Instancing_InputLayout.data(), (UINT)m_Instancing_InputLayout.size() };
	opaquePsoDesc.pRootSignature = m_RenderRS.Get();
	opaquePsoDesc.VS =
	{
		reinterpret_cast<BYTE*>(m_Shaders["standardVS"]->GetBufferPointer()),
		m_Shaders["standardVS"]->GetBufferSize()
	};
	opaquePsoDesc.PS =
	{
		reinterpret_cast<BYTE*>(m_Shaders["opaquePS"]->GetBufferPointer()),
		m_Shaders["opaquePS"]->GetBufferSize()
	};
	opaquePsoDesc.RasterizerState = CD3DX12_RASTERIZER_DESC(D3D12_DEFAULT);
	opaquePsoDesc.BlendState = CD3DX12_BLEND_DESC(D3D12_DEFAULT);
	opaquePsoDesc.DepthStencilState = CD3DX12_DEPTH_STENCIL_DESC(D3D12_DEFAULT);
	opaquePsoDesc.SampleMask = UINT_MAX;
	opaquePsoDesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
	opaquePsoDesc.NumRenderTargets = 1;
	opaquePsoDesc.RTVFormats[0] = g_BackBufferFormat;
	opaquePsoDesc.SampleDesc.Count = g_4xMsaaState ? 4 : 1;
	opaquePsoDesc.SampleDesc.Quality = g_4xMsaaState ? (g_4xMsaaQuality - 1) : 0;
	opaquePsoDesc.DSVFormat = g_DepthStencilFormat;
	ThrowIfFailed(g_Device->CreateGraphicsPipelineState(&opaquePsoDesc, IID_PPV_ARGS(&g_OpaquePSO)));

	// Alpha Blend State
	D3D12_BLEND_DESC blendDesc;
	ZeroMemory(&blendDesc, sizeof(D3D12_BLEND_DESC));
	blendDesc.AlphaToCoverageEnable = false;
	blendDesc.IndependentBlendEnable = false;
	blendDesc.RenderTarget[0].BlendEnable = TRUE;
	blendDesc.RenderTarget[0].SrcBlend = D3D12_BLEND_SRC_ALPHA;
	blendDesc.RenderTarget[0].DestBlend = D3D12_BLEND_INV_SRC_ALPHA;
	blendDesc.RenderTarget[0].BlendOp = D3D12_BLEND_OP_ADD;

	blendDesc.RenderTarget[0].SrcBlendAlpha = D3D12_BLEND_ONE;
	blendDesc.RenderTarget[0].DestBlendAlpha = D3D12_BLEND_ONE;
	blendDesc.RenderTarget[0].BlendOpAlpha = D3D12_BLEND_OP_ADD;
	blendDesc.RenderTarget[0].RenderTargetWriteMask = D3D12_COLOR_WRITE_ENABLE_ALL;
	blendDesc.AlphaToCoverageEnable = true;

	//
	// PSO for opacity objects.
	//
	D3D12_GRAPHICS_PIPELINE_STATE_DESC opacityPsoDesc = opaquePsoDesc;
	opacityPsoDesc.BlendState = blendDesc;
	ThrowIfFailed(g_Device->CreateGraphicsPipelineState(&opacityPsoDesc, IID_PPV_ARGS(&g_OpacityPSO)));

	//
	// PSO for contour pass.
	//
	D3D12_GRAPHICS_PIPELINE_STATE_DESC contourPsoDesc = opaquePsoDesc;
	contourPsoDesc.RasterizerState.CullMode = D3D12_CULL_MODE_FRONT;
	contourPsoDesc.VS =
	{
		reinterpret_cast<BYTE*>(m_Shaders["contourVS"]->GetBufferPointer()),
		m_Shaders["contourVS"]->GetBufferSize()
	};
	contourPsoDesc.PS =
	{
		reinterpret_cast<BYTE*>(m_Shaders["contourPS"]->GetBufferPointer()),
		m_Shaders["contourPS"]->GetBufferSize()
	};
	ThrowIfFailed(g_Device->CreateGraphicsPipelineState(&contourPsoDesc, IID_PPV_ARGS(&g_ContourPSO)));

	//
	// PSO for skinned pass.
	//
	D3D12_GRAPHICS_PIPELINE_STATE_DESC skinnedOpaquePsoDesc = opaquePsoDesc;
	skinnedOpaquePsoDesc.InputLayout = { m_Skinned_InputLayout.data(), (UINT)m_Skinned_InputLayout.size() };
	skinnedOpaquePsoDesc.VS =
	{
		reinterpret_cast<BYTE*>(m_Shaders["skinnedVS"]->GetBufferPointer()),
		m_Shaders["skinnedVS"]->GetBufferSize()
	};
	skinnedOpaquePsoDesc.PS =
	{
		reinterpret_cast<BYTE*>(m_Shaders["opaquePS"]->GetBufferPointer()),
		m_Shaders["opaquePS"]->GetBufferSize()
	};
	ThrowIfFailed(g_Device->CreateGraphicsPipelineState(&skinnedOpaquePsoDesc, IID_PPV_ARGS(&g_SkinnedPSO)));

	//
	// PSO for UI
	//
	D3D12_GRAPHICS_PIPELINE_STATE_DESC uiPsoDesc = opaquePsoDesc;
	uiPsoDesc.InputLayout = { m_UI_InputLayout.data(), (UINT)m_UI_InputLayout.size() };
	uiPsoDesc.BlendState = blendDesc;
	uiPsoDesc.RasterizerState.CullMode = D3D12_CULL_MODE_BACK;
	uiPsoDesc.DepthStencilState.DepthFunc = D3D12_COMPARISON_FUNC_ALWAYS;
	uiPsoDesc.pRootSignature = m_RenderRS.Get();
	uiPsoDesc.VS =
	{
		reinterpret_cast<BYTE*>(m_Shaders["uiVS"]->GetBufferPointer()),
		m_Shaders["uiVS"]->GetBufferSize()
	};
	uiPsoDesc.PS =
	{
		reinterpret_cast<BYTE*>(m_Shaders["uiPS"]->GetBufferPointer()),
		m_Shaders["uiPS"]->GetBufferSize()
	};
	ThrowIfFailed(g_Device->CreateGraphicsPipelineState(&uiPsoDesc, IID_PPV_ARGS(&g_UIPSO)));

	//
	// PSO for hp.
	//
	D3D12_GRAPHICS_PIPELINE_STATE_DESC hpPsoDesc = uiPsoDesc;
	hpPsoDesc.PS = {
		reinterpret_cast<BYTE*>(m_Shaders["hpPS"]->GetBufferPointer()),
		m_Shaders["hpPS"]->GetBufferSize()
	};
	ThrowIfFailed(g_Device->CreateGraphicsPipelineState(&hpPsoDesc, IID_PPV_ARGS(&g_HPPSO)));

	//
	// PSO for sky.
	//
	D3D12_GRAPHICS_PIPELINE_STATE_DESC skyPsoDesc = opaquePsoDesc;
	skyPsoDesc.RasterizerState.CullMode = D3D12_CULL_MODE_NONE;
	skyPsoDesc.DepthStencilState.DepthFunc = D3D12_COMPARISON_FUNC_LESS_EQUAL;
	skyPsoDesc.pRootSignature = m_RenderRS.Get();
	skyPsoDesc.VS =
	{
		reinterpret_cast<BYTE*>(m_Shaders["skyVS"]->GetBufferPointer()),
		m_Shaders["skyVS"]->GetBufferSize()
	};
	skyPsoDesc.PS =
	{
		reinterpret_cast<BYTE*>(m_Shaders["skyPS"]->GetBufferPointer()),
		m_Shaders["skyPS"]->GetBufferSize()
	};
	ThrowIfFailed(g_Device->CreateGraphicsPipelineState(&skyPsoDesc, IID_PPV_ARGS(&g_SkyPSO)));

	//
	// PSO for shadow map pass.
	//
	D3D12_GRAPHICS_PIPELINE_STATE_DESC smapPsoDesc = opaquePsoDesc;
	smapPsoDesc.RasterizerState.DepthBias = 10000;
	smapPsoDesc.RasterizerState.DepthBiasClamp = 0.0f;
	smapPsoDesc.RasterizerState.SlopeScaledDepthBias = 1.0f;
	smapPsoDesc.pRootSignature = m_RenderRS.Get();
	smapPsoDesc.VS =
	{
		reinterpret_cast<BYTE*>(m_Shaders["shadowVS"]->GetBufferPointer()),
		m_Shaders["shadowVS"]->GetBufferSize()
	};
	smapPsoDesc.PS =
	{
		reinterpret_cast<BYTE*>(m_Shaders["shadowOpaquePS"]->GetBufferPointer()),
		m_Shaders["shadowOpaquePS"]->GetBufferSize()
	};

	// Shadow map pass does not have a render target.
	smapPsoDesc.RTVFormats[0] = DXGI_FORMAT_UNKNOWN;
	smapPsoDesc.NumRenderTargets = 0;
	ThrowIfFailed(g_Device->CreateGraphicsPipelineState(&smapPsoDesc, IID_PPV_ARGS(&g_ShadowOpaquePSO)));

	D3D12_GRAPHICS_PIPELINE_STATE_DESC skinnedSmapPsoDesc = smapPsoDesc;
	skinnedSmapPsoDesc.InputLayout = { m_Skinned_InputLayout.data(), (UINT)m_Skinned_InputLayout.size() };
	skinnedSmapPsoDesc.VS =
	{
		reinterpret_cast<BYTE*>(m_Shaders["skinnedShadowVS"]->GetBufferPointer()),
		m_Shaders["skinnedShadowVS"]->GetBufferSize()
	};
	skinnedSmapPsoDesc.PS =
	{
		reinterpret_cast<BYTE*>(m_Shaders["shadowOpaquePS"]->GetBufferPointer()),
		m_Shaders["shadowOpaquePS"]->GetBufferSize()
	};
	ThrowIfFailed(g_Device->CreateGraphicsPipelineState(&skinnedSmapPsoDesc, IID_PPV_ARGS(&g_SkinnedShadowOpaquePSO)));

	//
	// PSO for horizontal blur
	//
	D3D12_COMPUTE_PIPELINE_STATE_DESC horzBlurPSO = {};
	horzBlurPSO.pRootSignature = m_PostProcessRS.Get();
	horzBlurPSO.CS =
	{
		reinterpret_cast<BYTE*>(m_Shaders["horzBlurCS"]->GetBufferPointer()),
		m_Shaders["horzBlurCS"]->GetBufferSize()
	};
	horzBlurPSO.Flags = D3D12_PIPELINE_STATE_FLAG_NONE;
	ThrowIfFailed(Core::g_Device->CreateComputePipelineState(&horzBlurPSO, IID_PPV_ARGS(&g_HorzBlurPSO)));

	//
	// PSO for vertical blur
	//
	D3D12_COMPUTE_PIPELINE_STATE_DESC vertBlurPSO = {};
	vertBlurPSO.pRootSignature = m_PostProcessRS.Get();
	vertBlurPSO.CS =
	{
		reinterpret_cast<BYTE*>(m_Shaders["vertBlurCS"]->GetBufferPointer()),
		m_Shaders["vertBlurCS"]->GetBufferSize()
	};
	vertBlurPSO.Flags = D3D12_PIPELINE_STATE_FLAG_NONE;
	ThrowIfFailed(Core::g_Device->CreateComputePipelineState(&vertBlurPSO, IID_PPV_ARGS(&g_VertBlurPSO)));

	//
	// PSO for billboards sprites
	//
	// 알파테스팅 안티엘리어싱
	// https://www.slideshare.net/jpcorp/5-10351002
	// https://m.blog.naver.com/PostView.nhn?blogId=sorkelf&logNo=40163123661&proxyReferer=https:%2F%2Fwww.google.com%2F
	// https://mrhook.co.kr/160
	D3D12_GRAPHICS_PIPELINE_STATE_DESC billboardsPsoDesc = opaquePsoDesc;
	billboardsPsoDesc.BlendState = blendDesc;
	billboardsPsoDesc.VS =
	{
		reinterpret_cast<BYTE*>(m_Shaders["particleVS"]->GetBufferPointer()),
		m_Shaders["particleVS"]->GetBufferSize()
	};
	billboardsPsoDesc.GS =
	{
		reinterpret_cast<BYTE*>(m_Shaders["particleGS"]->GetBufferPointer()),
		m_Shaders["particleGS"]->GetBufferSize()
	};
	billboardsPsoDesc.PS =
	{
		reinterpret_cast<BYTE*>(m_Shaders["particlePS"]->GetBufferPointer()),
		m_Shaders["particlePS"]->GetBufferSize()
	};
	billboardsPsoDesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_POINT;
	billboardsPsoDesc.InputLayout = { m_Billboard_InputLayout.data(), (UINT)m_Billboard_InputLayout.size() };
	billboardsPsoDesc.RasterizerState.CullMode = D3D12_CULL_MODE_BACK;

	ThrowIfFailed(Core::g_Device->CreateGraphicsPipelineState(&billboardsPsoDesc, IID_PPV_ARGS(&g_ParticlePSO)));
}

std::array<const CD3DX12_STATIC_SAMPLER_DESC, 7> GraphicsRenderer::GetStaticSamplers()
{
	// Applications usually only need a handful of samplers.  So just define them all up front
	// and keep them available as part of the root signature.  

	const CD3DX12_STATIC_SAMPLER_DESC pointWrap(
		0, // shaderRegister
		D3D12_FILTER_MIN_MAG_MIP_POINT, // filter
		D3D12_TEXTURE_ADDRESS_MODE_WRAP,  // addressU
		D3D12_TEXTURE_ADDRESS_MODE_WRAP,  // addressV
		D3D12_TEXTURE_ADDRESS_MODE_WRAP); // addressW

	const CD3DX12_STATIC_SAMPLER_DESC pointClamp(
		1, // shaderRegister
		D3D12_FILTER_MIN_MAG_MIP_POINT, // filter
		D3D12_TEXTURE_ADDRESS_MODE_CLAMP,  // addressU
		D3D12_TEXTURE_ADDRESS_MODE_CLAMP,  // addressV
		D3D12_TEXTURE_ADDRESS_MODE_CLAMP); // addressW

	const CD3DX12_STATIC_SAMPLER_DESC linearWrap(
		2, // shaderRegister
		D3D12_FILTER_MIN_MAG_MIP_LINEAR, // filter
		D3D12_TEXTURE_ADDRESS_MODE_WRAP,  // addressU
		D3D12_TEXTURE_ADDRESS_MODE_WRAP,  // addressV
		D3D12_TEXTURE_ADDRESS_MODE_WRAP); // addressW

	const CD3DX12_STATIC_SAMPLER_DESC linearClamp(
		3, // shaderRegister
		D3D12_FILTER_MIN_MAG_MIP_LINEAR, // filter
		D3D12_TEXTURE_ADDRESS_MODE_CLAMP,  // addressU
		D3D12_TEXTURE_ADDRESS_MODE_CLAMP,  // addressV
		D3D12_TEXTURE_ADDRESS_MODE_CLAMP); // addressW

	const CD3DX12_STATIC_SAMPLER_DESC anisotropicWrap(
		4, // shaderRegister
		D3D12_FILTER_ANISOTROPIC, // filter
		D3D12_TEXTURE_ADDRESS_MODE_WRAP,  // addressU
		D3D12_TEXTURE_ADDRESS_MODE_WRAP,  // addressV
		D3D12_TEXTURE_ADDRESS_MODE_WRAP,  // addressW
		0.0f,                             // mipLODBias
		8);                               // maxAnisotropy

	const CD3DX12_STATIC_SAMPLER_DESC anisotropicClamp(
		5, // shaderRegister
		D3D12_FILTER_ANISOTROPIC, // filter
		D3D12_TEXTURE_ADDRESS_MODE_CLAMP,  // addressU
		D3D12_TEXTURE_ADDRESS_MODE_CLAMP,  // addressV
		D3D12_TEXTURE_ADDRESS_MODE_CLAMP,  // addressW
		0.0f,                              // mipLODBias
		8);                                // maxAnisotropy

	const CD3DX12_STATIC_SAMPLER_DESC shadow(
		6, // shaderRegister
		D3D12_FILTER_COMPARISON_MIN_MAG_LINEAR_MIP_POINT, // filter
		D3D12_TEXTURE_ADDRESS_MODE_BORDER,  // addressU
		D3D12_TEXTURE_ADDRESS_MODE_BORDER,  // addressV
		D3D12_TEXTURE_ADDRESS_MODE_BORDER,  // addressW
		0.0f,                               // mipLODBias
		16,                                 // maxAnisotropy
		D3D12_COMPARISON_FUNC_LESS_EQUAL,
		D3D12_STATIC_BORDER_COLOR_OPAQUE_BLACK);

	return {
		pointWrap, pointClamp,
		linearWrap, linearClamp,
		anisotropicWrap, anisotropicClamp,
		shadow
	};
}