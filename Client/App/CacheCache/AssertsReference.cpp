#include "pch.h"
#include "AssertsReference.h"
#include "GeometryGenerator.h"
#include "Map.h"

void AssertsReference::LoadBB(const char* path)
{
	std::cout << "Start Loading BBs...\n";
	std::ifstream in(path);

	while (!in.eof()) 
	{
		auto bb = make_unique<BoundingBox>();
		string name;
		int bbNum = 0;
		string tmp;


		in >> name >> bbNum;
		for (int i = 0; i < bbNum; ++i)
		{
			in >> bb->Extents.x >> bb->Extents.y >> bb->Extents.z
				>> bb->Center.x >> bb->Center.y >> bb->Center.z
				>> tmp >> tmp >> tmp	// look
				>> tmp >> tmp >> tmp	// up
				>> tmp >> tmp >> tmp	// right
				>> tmp >> tmp >> tmp;	// rotation

			m_PropBoundingBox[name] = std::move(bb);
		}

		in >> tmp; // numBS --> 무조건 0이 나옴
		in >> tmp; // 구분자 C
	}
	std::cout << "Loading BBs Finished.\n";
}

Map* AssertsReference::LoadMapInfo(const char* fileName)
{
	Map* map = new Map;

	ifstream in(fileName);
	set<string> propTypeSet;

	string parserID;
	for (string line; getline(in, line);)
	{
		if (line.size() == 0) continue;

		string name;

		stringstream lineparser(line);
		lineparser >> name;
		if (name == "Masterpf" || name == "Studentpf")
		{
			MapTool::PlayerInfo prePlayerInfo;
			prePlayerInfo.playerName = name;
			lineparser >> prePlayerInfo.position.x >> prePlayerInfo.position.y >> prePlayerInfo.position.z >> prePlayerInfo.rotY
				>> prePlayerInfo.spawnPos;
			map->playerInfoVector.emplace_back(prePlayerInfo);
		}
		else if (name == "UI")
		{
			MapTool::UIInfo uiInfo;
			lineparser >> uiInfo.uiName >> uiInfo.meshName
				>> uiInfo.position.x >> uiInfo.position.y >> uiInfo.position.z
				>> uiInfo.rotation.x >> uiInfo.rotation.y >> uiInfo.rotation.z
				>> uiInfo.scale.x >> uiInfo.scale.y >> uiInfo.scale.z
				>> uiInfo.type;
			map->uiInfoVector.emplace_back(uiInfo);
		}
		else
		{
			
			MapTool::MapInfo preInfo;

			float tmp;
			preInfo.meshName = name;
			lineparser
				>> preInfo.position.x >> preInfo.position.y >> preInfo.position.z
				>> preInfo.rotation.x >> preInfo.rotation.y >> preInfo.rotation.z
				>> preInfo.changeable >> tmp >> tmp >> preInfo.proptype >> preInfo.typeID
				>> preInfo.textureName;

			map->mapInfoVector.emplace_back(preInfo);
			propTypeSet.insert(name);
		}
	}

	///////////////////////////////////////////////////////////////////////////
	// 특수 컨테이너들 제작
	// propTypeVector: 중복되는 것 없이 에셋이름만 모아놓은 vector
	// mapInfoDic: 서버타입아이디랑 클라타입아이디랑 매칭을 위한 map
	// mapInfoChangeableVector: 변신할수있는 타입정보만 모아놓은 vector

	for (auto iter = propTypeSet.begin(); iter != propTypeSet.end(); ++iter)
	{
		map->propTypeVector.emplace_back(*iter);
	}

	for (auto& p : map->mapInfoVector)
	{
		map->mapInfoDic[std::to_string(p.proptype)] = p.meshName;
		if (p.changeable) map->mapInfoChangeableVector.push_back(p);
	}

	return map;
}

void AssertsReference::BuildMaterials()
{
	auto sky = std::make_unique<Material>();
	sky->MatCBIndex = TEXTURE_INDEX_grasscube1024;
	sky->DiffuseSrvHeapIndex = TEXTURE_INDEX_grasscube1024;
	sky->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	sky->FresnelR0 = XMFLOAT3(0.1f, 0.1f, 0.1f);
	sky->Roughness = 1.0f;

	auto PolyAdventureTexture_01 = std::make_unique<Material>();
	PolyAdventureTexture_01->MatCBIndex = TEXTURE_INDEX_PolyAdventureTexture_01;
	PolyAdventureTexture_01->DiffuseSrvHeapIndex = TEXTURE_INDEX_PolyAdventureTexture_01;
	PolyAdventureTexture_01->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	PolyAdventureTexture_01->FresnelR0 = XMFLOAT3(0.02f, 0.02f, 0.02f);
	PolyAdventureTexture_01->Roughness = 0.967734;

	auto Polygon_Fantasy_Characters_Texture_01_A = std::make_unique<Material>();
	Polygon_Fantasy_Characters_Texture_01_A->MatCBIndex = TEXTURE_INDEX_Polygon_Fantasy_Characters_Texture_01_A;
	Polygon_Fantasy_Characters_Texture_01_A->DiffuseSrvHeapIndex = TEXTURE_INDEX_Polygon_Fantasy_Characters_Texture_01_A;
	Polygon_Fantasy_Characters_Texture_01_A->DiffuseAlbedo = XMFLOAT4(0.8f, 0.8f, 0.8f, 1.0f);
	Polygon_Fantasy_Characters_Texture_01_A->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	Polygon_Fantasy_Characters_Texture_01_A->Roughness = 0.717734;

	auto Polygon_Fantasy_Characters_Texture_02_A = std::make_unique<Material>();
	Polygon_Fantasy_Characters_Texture_02_A->MatCBIndex = TEXTURE_INDEX_Polygon_Fantasy_Characters_Texture_02_A;
	Polygon_Fantasy_Characters_Texture_02_A->DiffuseSrvHeapIndex = TEXTURE_INDEX_Polygon_Fantasy_Characters_Texture_02_A;
	Polygon_Fantasy_Characters_Texture_02_A->DiffuseAlbedo = XMFLOAT4(0.8f, 0.8f, 0.8f, 1.0f);
	Polygon_Fantasy_Characters_Texture_02_A->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	Polygon_Fantasy_Characters_Texture_02_A->Roughness = 0.717734;

	auto Polygon_Fantasy_Characters_Texture_03_A = std::make_unique<Material>();
	Polygon_Fantasy_Characters_Texture_03_A->MatCBIndex = TEXTURE_INDEX_Polygon_Fantasy_Characters_Texture_03_A;
	Polygon_Fantasy_Characters_Texture_03_A->DiffuseSrvHeapIndex = TEXTURE_INDEX_Polygon_Fantasy_Characters_Texture_03_A;
	Polygon_Fantasy_Characters_Texture_03_A->DiffuseAlbedo = XMFLOAT4(0.8f, 0.8f, 0.8f, 1.0f);
	Polygon_Fantasy_Characters_Texture_03_A->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	Polygon_Fantasy_Characters_Texture_03_A->Roughness = 0.717734;

	auto Polygon_Fantasy_Characters_Texture_04_A = std::make_unique<Material>();
	Polygon_Fantasy_Characters_Texture_04_A->MatCBIndex = TEXTURE_INDEX_Polygon_Fantasy_Characters_Texture_04_A;
	Polygon_Fantasy_Characters_Texture_04_A->DiffuseSrvHeapIndex = TEXTURE_INDEX_Polygon_Fantasy_Characters_Texture_04_A;
	Polygon_Fantasy_Characters_Texture_04_A->DiffuseAlbedo = XMFLOAT4(0.8f, 0.8f, 0.8f, 1.0f);
	Polygon_Fantasy_Characters_Texture_04_A->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	Polygon_Fantasy_Characters_Texture_04_A->Roughness = 0.717734;

	auto Polygon_Fantasy_Characters_Texture_05_A = std::make_unique<Material>();
	Polygon_Fantasy_Characters_Texture_05_A->MatCBIndex = TEXTURE_INDEX_Polygon_Fantasy_Characters_Texture_05_A;
	Polygon_Fantasy_Characters_Texture_05_A->DiffuseSrvHeapIndex = TEXTURE_INDEX_Polygon_Fantasy_Characters_Texture_05_A;
	Polygon_Fantasy_Characters_Texture_05_A->DiffuseAlbedo = XMFLOAT4(0.8f, 0.8f, 0.8f, 1.0f);
	Polygon_Fantasy_Characters_Texture_05_A->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	Polygon_Fantasy_Characters_Texture_05_A->Roughness = 0.717734;

	auto UI_Board = std::make_unique<Material>();
	UI_Board->MatCBIndex = TEXTURE_INDEX_UI_Board;
	UI_Board->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_Board;
	UI_Board->DiffuseAlbedo = XMFLOAT4(0.8f, 0.8f, 0.8f, 1.0f);
	UI_Board->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	UI_Board->Roughness = 0.517734;

	auto Thunderbolt = std::make_unique<Material>();
	Thunderbolt->MatCBIndex = TEXTURE_INDEX_Thunderbolt;
	Thunderbolt->DiffuseSrvHeapIndex = TEXTURE_INDEX_Thunderbolt;
	Thunderbolt->DiffuseAlbedo = XMFLOAT4(1.f, 1.f, 1.f, 1.0f);
	Thunderbolt->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	Thunderbolt->Roughness = 0.517734;

	auto UI_DU= std::make_unique<Material>();
	UI_DU->MatCBIndex = TEXTURE_INDEX_UI_DU;
	UI_DU->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_DU;
	UI_DU->DiffuseAlbedo = XMFLOAT4(0.7f, 0.7f, 0.7f, 1.f);
	UI_DU->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	UI_DU->Roughness = 0.517734;

	auto UI_BA = std::make_unique<Material>();
	UI_BA->MatCBIndex = TEXTURE_INDEX_UI_BA;
	UI_BA->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_BA;
	UI_BA->DiffuseAlbedo = XMFLOAT4(0.7f, 0.7f, 0.7f, 1.f);
	UI_BA->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	UI_BA->Roughness = 0.517734;

	auto UI_FP = std::make_unique<Material>();
	UI_FP->MatCBIndex = TEXTURE_INDEX_UI_FP;
	UI_FP->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_FP;
	UI_FP->DiffuseAlbedo = XMFLOAT4(0.7f, 0.7f, 0.7f, 1.f);
	UI_FP->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	UI_FP->Roughness = 0.517734;

	auto UI_MP = std::make_unique<Material>();
	UI_MP->MatCBIndex = TEXTURE_INDEX_UI_MP;
	UI_MP->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_MP;
	UI_MP->DiffuseAlbedo = XMFLOAT4(0.7f, 0.7f, 0.7f, 1.f);
	UI_MP->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	UI_MP->Roughness = 0.517734;

	auto UI_SO = std::make_unique<Material>();
	UI_SO->MatCBIndex = TEXTURE_INDEX_UI_SO;
	UI_SO->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_SO;
	UI_SO->DiffuseAlbedo = XMFLOAT4(0.7f, 0.7f, 0.7f, 1.f);
	UI_SO->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	UI_SO->Roughness = 0.517734;

	auto UI_START = std::make_unique<Material>();
	UI_START->MatCBIndex = TEXTURE_INDEX_UI_START;
	UI_START->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_START;
	UI_START->DiffuseAlbedo = XMFLOAT4(0.8f, 0.8f, 0.8f, 1.0f);
	UI_START->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	UI_START->Roughness = 0.517734;

	auto UI_MAP = std::make_unique<Material>();
	UI_MAP->MatCBIndex = TEXTURE_INDEX_UI_MAP;
	UI_MAP->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_MAP;
	UI_MAP->DiffuseAlbedo = XMFLOAT4(0.8f, 0.8f, 0.8f, 1.0f);
	UI_MAP->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	UI_MAP->Roughness = 0.517734;

	auto UI_READY = std::make_unique<Material>();
	UI_READY->MatCBIndex = TEXTURE_INDEX_UI_READY;
	UI_READY->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_READY;
	UI_READY->DiffuseAlbedo = XMFLOAT4(0.8f, 0.8f, 0.8f, 1.0f);
	UI_READY->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	UI_READY->Roughness = 0.517734;

	auto UI_READYOFF = std::make_unique<Material>();
	UI_READYOFF->MatCBIndex = TEXTURE_INDEX_UI_READY_OFF;
	UI_READYOFF->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_READY_OFF;
	UI_READYOFF->DiffuseAlbedo = XMFLOAT4(0.8f, 0.8f, 0.8f, 1.0f);
	UI_READYOFF->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	UI_READYOFF->Roughness = 0.517734;

	auto UI_READYINACTIVE = std::make_unique<Material>();
	UI_READYINACTIVE->MatCBIndex = TEXTURE_INDEX_UI_READY_INACTIVE;
	UI_READYINACTIVE->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_READY_INACTIVE;
	UI_READYINACTIVE->DiffuseAlbedo = XMFLOAT4(0.8f, 0.8f, 0.8f, 1.0f);
	UI_READYINACTIVE->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	UI_READYINACTIVE->Roughness = 0.517734;

	auto UI_START_OFF = std::make_unique<Material>();
	UI_START_OFF->MatCBIndex = TEXTURE_INDEX_UI_START_OFF;
	UI_START_OFF->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_START_OFF;
	UI_START_OFF->DiffuseAlbedo = XMFLOAT4(0.8f, 0.8f, 0.8f, 1.0f);
	UI_START_OFF->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	UI_START_OFF->Roughness = 0.517734;

	auto UI_START_INACTIVE = std::make_unique<Material>();
	UI_START_INACTIVE->MatCBIndex = TEXTURE_INDEX_UI_START_INACTIVE;
	UI_START_INACTIVE->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_START_INACTIVE;
	UI_START_INACTIVE->DiffuseAlbedo = XMFLOAT4(0.8f, 0.8f, 0.8f, 1.0f);
	UI_START_INACTIVE->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	UI_START_INACTIVE->Roughness = 0.517734;

	auto UI_MAP_PREV_REL = std::make_unique<Material>();
	UI_MAP_PREV_REL->MatCBIndex = TEXTURE_INDEX_UI_BUTTON_MAP_PREV_RELEASED;
	UI_MAP_PREV_REL->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_BUTTON_MAP_PREV_RELEASED;
	UI_MAP_PREV_REL->DiffuseAlbedo = XMFLOAT4(0.8f, 0.8f, 0.8f, 1.0f);
	UI_MAP_PREV_REL->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	UI_MAP_PREV_REL->Roughness = 0.517734;

	auto UI_MAP_PREV_PRES = std::make_unique<Material>();
	UI_MAP_PREV_PRES->MatCBIndex = TEXTURE_INDEX_UI_BUTTON_MAP_PREV_PRESSED;
	UI_MAP_PREV_PRES->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_BUTTON_MAP_PREV_PRESSED;
	UI_MAP_PREV_PRES->DiffuseAlbedo = XMFLOAT4(0.8f, 0.8f, 0.8f, 1.0f);
	UI_MAP_PREV_PRES->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	UI_MAP_PREV_PRES->Roughness = 0.517734;

	auto UI_MAP_PREV_ACT = std::make_unique<Material>();
	UI_MAP_PREV_ACT->MatCBIndex = TEXTURE_INDEX_UI_BUTTON_MAP_PREV_ACTIVATION;
	UI_MAP_PREV_ACT->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_BUTTON_MAP_PREV_ACTIVATION;
	UI_MAP_PREV_ACT->DiffuseAlbedo = XMFLOAT4(0.8f, 0.8f, 0.8f, 1.0f);
	UI_MAP_PREV_ACT->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	UI_MAP_PREV_ACT->Roughness = 0.517734;

	auto UI_MAP_NEXT_REL = std::make_unique<Material>();
	UI_MAP_NEXT_REL->MatCBIndex = TEXTURE_INDEX_UI_BUTTON_MAP_NEXT_RELEASED;
	UI_MAP_NEXT_REL->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_BUTTON_MAP_NEXT_RELEASED;
	UI_MAP_NEXT_REL->DiffuseAlbedo = XMFLOAT4(0.8f, 0.8f, 0.8f, 1.0f);
	UI_MAP_NEXT_REL->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	UI_MAP_NEXT_REL->Roughness = 0.517734;

	auto UI_MAP_NEXT_PRES = std::make_unique<Material>();
	UI_MAP_NEXT_PRES->MatCBIndex = TEXTURE_INDEX_UI_BUTTON_MAP_NEXT_PRESSED;
	UI_MAP_NEXT_PRES->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_BUTTON_MAP_NEXT_PRESSED;
	UI_MAP_NEXT_PRES->DiffuseAlbedo = XMFLOAT4(0.8f, 0.8f, 0.8f, 1.0f);
	UI_MAP_NEXT_PRES->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	UI_MAP_NEXT_PRES->Roughness = 0.517734;

	auto UI_MAP_NEXT_ACT = std::make_unique<Material>();
	UI_MAP_NEXT_ACT->MatCBIndex = TEXTURE_INDEX_UI_BUTTON_MAP_NEXT_ACTIVATION;
	UI_MAP_NEXT_ACT->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_BUTTON_MAP_NEXT_ACTIVATION;
	UI_MAP_NEXT_ACT->DiffuseAlbedo = XMFLOAT4(0.8f, 0.8f, 0.8f, 1.0f);
	UI_MAP_NEXT_ACT->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	UI_MAP_NEXT_ACT->Roughness = 0.517734;

	auto UI_EXIT_REL = std::make_unique<Material>();
	UI_EXIT_REL->MatCBIndex = TEXTURE_INDEX_UI_BUTTON_EXIT_RELEASED;
	UI_EXIT_REL->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_BUTTON_EXIT_RELEASED;
	UI_EXIT_REL->DiffuseAlbedo = XMFLOAT4(0.8f, 0.8f, 0.8f, 1.0f);
	UI_EXIT_REL->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	UI_EXIT_REL->Roughness = 0.517734;

	auto UI_EXIT_PRES = std::make_unique<Material>();
	UI_EXIT_PRES->MatCBIndex = TEXTURE_INDEX_UI_BUTTON_EXIT_PRESSED;
	UI_EXIT_PRES->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_BUTTON_EXIT_PRESSED;
	UI_EXIT_PRES->DiffuseAlbedo = XMFLOAT4(0.8f, 0.8f, 0.8f, 1.0f);
	UI_EXIT_PRES->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	UI_EXIT_PRES->Roughness = 0.517734;


	auto AIM = std::make_unique<Material>();
	AIM->MatCBIndex = TEXTURE_INDEX_AIM;
	AIM->DiffuseSrvHeapIndex = TEXTURE_INDEX_AIM;
	AIM->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	AIM->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	AIM->Roughness = 0.517734;

	auto Knights_Texture_01 = std::make_unique<Material>();
	Knights_Texture_01 ->MatCBIndex = TEXTURE_INDEX_KNIGHTS_TEXTURE_01;
	Knights_Texture_01 ->DiffuseSrvHeapIndex = TEXTURE_INDEX_KNIGHTS_TEXTURE_01;
	Knights_Texture_01 ->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	Knights_Texture_01 ->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	Knights_Texture_01 ->Roughness = 0.9;

	auto Character_Ready = std::make_unique<Material>();
	Character_Ready->MatCBIndex = TEXTURE_INDEX_UI_CHARACTER_READY;
	Character_Ready->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_CHARACTER_READY;
	Character_Ready->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	Character_Ready->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	Character_Ready->Roughness = 0.9;

	auto Crown = std::make_unique<Material>();
	Crown->MatCBIndex = TEXTURE_INDEX_UI_CROWN;
	Crown->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_CROWN;
	Crown->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	Crown->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	Crown->Roughness = 0.9;

	auto HP = std::make_unique<Material>();
	HP->MatCBIndex = TEXTURE_INDEX_UI_HP;
	HP->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_HP;
	HP->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	HP->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	HP->Roughness = 0.9;

	auto PRO_BACK = std::make_unique<Material>();
	PRO_BACK->MatCBIndex = TEXTURE_INDEX_UI_PRO_BACK;
	PRO_BACK->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_PRO_BACK;
	PRO_BACK->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	PRO_BACK->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	PRO_BACK->Roughness = 0.9;

	auto PRO_MASTER = std::make_unique<Material>();
	PRO_MASTER->MatCBIndex = TEXTURE_INDEX_UI_PRO_MASTER;
	PRO_MASTER->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_PRO_MASTER;
	PRO_MASTER->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	PRO_MASTER->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	PRO_MASTER->Roughness = 0.9;

	auto PRO_POTION = std::make_unique<Material>();
	PRO_POTION->MatCBIndex = TEXTURE_INDEX_UI_PRO_POTION;
	PRO_POTION->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_PRO_POTION;
	PRO_POTION->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	PRO_POTION->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	PRO_POTION->Roughness = 0.9;

	auto PRO_BA = std::make_unique<Material>();
	PRO_BA->MatCBIndex = TEXTURE_INDEX_UI_PRO_BA;
	PRO_BA->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_PRO_BA;
	PRO_BA->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	PRO_BA->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	PRO_BA->Roughness = 0.9;

	auto PRO_DU = std::make_unique<Material>();
	PRO_DU->MatCBIndex = TEXTURE_INDEX_UI_PRO_DU;
	PRO_DU->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_PRO_DU;
	PRO_DU->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	PRO_DU->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	PRO_DU->Roughness = 0.9;

	auto PRO_FE = std::make_unique<Material>();
	PRO_FE->MatCBIndex = TEXTURE_INDEX_UI_PRO_FE;
	PRO_FE->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_PRO_FE;
	PRO_FE->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	PRO_FE->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	PRO_FE->Roughness = 0.9;

	auto PRO_MA = std::make_unique<Material>();
	PRO_MA->MatCBIndex = TEXTURE_INDEX_UI_PRO_MA;
	PRO_MA->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_PRO_MA;
	PRO_MA->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	PRO_MA->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	PRO_MA->Roughness = 0.9;

	auto PRO_SO = std::make_unique<Material>();
	PRO_SO->MatCBIndex = TEXTURE_INDEX_UI_PRO_SO;
	PRO_SO->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_PRO_SO;
	PRO_SO->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	PRO_SO->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	PRO_SO->Roughness = 0.9;

	auto TIME_0 = std::make_unique<Material>();
	TIME_0->MatCBIndex = TEXTURE_INDEX_UI_TIME_0;
	TIME_0->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_TIME_0;
	TIME_0->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	TIME_0->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	TIME_0->Roughness = 0.9;

	auto TIME_1 = std::make_unique<Material>();
	TIME_1->MatCBIndex = TEXTURE_INDEX_UI_TIME_1;
	TIME_1->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_TIME_1;
	TIME_1->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	TIME_1->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	TIME_1->Roughness = 0.9;

	auto TIME_2 = std::make_unique<Material>();
	TIME_2->MatCBIndex = TEXTURE_INDEX_UI_TIME_2;
	TIME_2->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_TIME_2;
	TIME_2->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	TIME_2->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	TIME_2->Roughness = 0.9;

	auto TIME_3 = std::make_unique<Material>();
	TIME_3->MatCBIndex = TEXTURE_INDEX_UI_TIME_3;
	TIME_3->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_TIME_3;
	TIME_3->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	TIME_3->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	TIME_3->Roughness = 0.9;

	auto TIME_4 = std::make_unique<Material>();
	TIME_4->MatCBIndex = TEXTURE_INDEX_UI_TIME_4;
	TIME_4->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_TIME_4;
	TIME_4->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	TIME_4->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	TIME_4->Roughness = 0.9;

	auto TIME_5 = std::make_unique<Material>();
	TIME_5->MatCBIndex = TEXTURE_INDEX_UI_TIME_5;
	TIME_5->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_TIME_5;
	TIME_5->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	TIME_5->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	TIME_5->Roughness = 0.9;

	auto TIME_6 = std::make_unique<Material>();
	TIME_6->MatCBIndex = TEXTURE_INDEX_UI_TIME_6;
	TIME_6->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_TIME_6;
	TIME_6->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	TIME_6->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	TIME_6->Roughness = 0.9;

	auto TIME_7 = std::make_unique<Material>();
	TIME_7->MatCBIndex = TEXTURE_INDEX_UI_TIME_7;
	TIME_7->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_TIME_7;
	TIME_7->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	TIME_7->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	TIME_7->Roughness = 0.9;

	auto TIME_8 = std::make_unique<Material>();
	TIME_8->MatCBIndex = TEXTURE_INDEX_UI_TIME_8;
	TIME_8->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_TIME_8;
	TIME_8->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	TIME_8->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	TIME_8->Roughness = 0.9;

	auto Back_rel = std::make_unique<Material>();
	Back_rel->MatCBIndex = TEXTURE_INDEX_UI_BUTTON_BACK_RELEASED;
	Back_rel->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_BUTTON_BACK_RELEASED;
	Back_rel->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	Back_rel->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	Back_rel->Roughness = 0.9;

	auto Back_pres = std::make_unique<Material>();
	Back_pres->MatCBIndex = TEXTURE_INDEX_UI_BUTTON_BACK_PRESSED;
	Back_pres->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_BUTTON_BACK_PRESSED;
	Back_pres->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	Back_pres->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	Back_pres->Roughness = 0.9;

	auto SING_IN = std::make_unique<Material>();
	SING_IN->MatCBIndex = TEXTURE_INDEX_UI_BUTTON_TITLE_SING_IN_RELEASED;
	SING_IN->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_BUTTON_TITLE_SING_IN_RELEASED;
	SING_IN->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	SING_IN->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	SING_IN->Roughness = 0.9;

	auto SING_IN_P = std::make_unique<Material>();
	SING_IN_P->MatCBIndex = TEXTURE_INDEX_UI_BUTTON_TITLE_SING_IN_PRESSED;
	SING_IN_P->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_BUTTON_TITLE_SING_IN_PRESSED;
	SING_IN_P->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	SING_IN_P->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	SING_IN_P->Roughness = 0.9;

	auto SING_UP_R = std::make_unique<Material>();
	SING_UP_R->MatCBIndex = TEXTURE_INDEX_UI_BUTTON_TITLE_SING_UP_RELEASED;
	SING_UP_R->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_BUTTON_TITLE_SING_UP_RELEASED;
	SING_UP_R->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	SING_UP_R->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	SING_UP_R->Roughness = 0.9;

	auto SING_UP_P = std::make_unique<Material>();
	SING_UP_P->MatCBIndex = TEXTURE_INDEX_UI_BUTTON_TITLE_SING_UP_PRESSED;
	SING_UP_P->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_BUTTON_TITLE_SING_UP_PRESSED;
	SING_UP_P->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	SING_UP_P->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	SING_UP_P->Roughness = 0.9;

	auto SING_UP_A = std::make_unique<Material>();
	SING_UP_A->MatCBIndex = TEXTURE_INDEX_UI_BUTTON_TITLE_SING_UP_ACTIVATION;
	SING_UP_A->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_BUTTON_TITLE_SING_UP_ACTIVATION;
	SING_UP_A->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	SING_UP_A->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	SING_UP_A->Roughness = 0.9;

	auto AUTOMATCH_R = std::make_unique<Material>();
	AUTOMATCH_R->MatCBIndex = TEXTURE_INDEX_UI_BUTTON_LOBBY_AUTOMATCH_RELEASED;
	AUTOMATCH_R->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_BUTTON_LOBBY_AUTOMATCH_RELEASED;
	AUTOMATCH_R->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	AUTOMATCH_R->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	AUTOMATCH_R->Roughness = 0.9;

	auto AUTOMATCH_P = std::make_unique<Material>();
	AUTOMATCH_P->MatCBIndex = TEXTURE_INDEX_UI_BUTTON_LOBBY_AUTOMATCH_PRESSED;
	AUTOMATCH_P->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_BUTTON_LOBBY_AUTOMATCH_PRESSED;
	AUTOMATCH_P->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	AUTOMATCH_P->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	AUTOMATCH_P->Roughness = 0.9;

	auto MANUALMATCH_R = std::make_unique<Material>();
	MANUALMATCH_R->MatCBIndex = TEXTURE_INDEX_UI_BUTTON_LOBBY_MANUALMATCH_RELEASED;
	MANUALMATCH_R->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_BUTTON_LOBBY_MANUALMATCH_RELEASED;
	MANUALMATCH_R->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	MANUALMATCH_R->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	MANUALMATCH_R->Roughness = 0.9;

	auto MANUALMATCH_P = std::make_unique<Material>();
	MANUALMATCH_P->MatCBIndex = TEXTURE_INDEX_UI_BUTTON_LOBBY_MANUALMATCH_PRESSED;
	MANUALMATCH_P->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_BUTTON_LOBBY_MANUALMATCH_PRESSED;
	MANUALMATCH_P->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	MANUALMATCH_P->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	MANUALMATCH_P->Roughness = 0.9;

	auto RULE_R = std::make_unique<Material>();
	RULE_R->MatCBIndex =			TEXTURE_INDEX_UI_BUTTON_LOBBY_RULE_RELEASED;
	RULE_R->DiffuseSrvHeapIndex =	TEXTURE_INDEX_UI_BUTTON_LOBBY_RULE_RELEASED;
	RULE_R->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	RULE_R->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	RULE_R->Roughness = 0.9;

	auto RULE_P = std::make_unique<Material>();
	RULE_P->MatCBIndex =			TEXTURE_INDEX_UI_BUTTON_LOBBY_RULE_PRESSED;
	RULE_P->DiffuseSrvHeapIndex =	TEXTURE_INDEX_UI_BUTTON_LOBBY_RULE_PRESSED;
	RULE_P->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	RULE_P->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	RULE_P->Roughness = 0.9;

	auto TRANS = std::make_unique<Material>();
	TRANS->MatCBIndex = TEXTURE_INDEX_TRANSPARENCY;
	TRANS->DiffuseSrvHeapIndex = TEXTURE_INDEX_TRANSPARENCY;
	TRANS->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	TRANS->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	TRANS->Roughness = 0.9;

	auto T_Smoke_Tiled_D = std::make_unique<Material>();
	T_Smoke_Tiled_D->MatCBIndex = TEXTURE_INDEX_T_Smoke_Tiled_D;
	T_Smoke_Tiled_D->DiffuseSrvHeapIndex = TEXTURE_INDEX_T_Smoke_Tiled_D;
	T_Smoke_Tiled_D->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	T_Smoke_Tiled_D->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	T_Smoke_Tiled_D->Roughness = 0.9;

	auto P_PENALTY = std::make_unique<Material>();
	P_PENALTY->MatCBIndex = TEXTURE_INDEX_P_PENALTY;
	P_PENALTY->DiffuseSrvHeapIndex = TEXTURE_INDEX_P_PENALTY;
	P_PENALTY->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	P_PENALTY->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	P_PENALTY->Roughness = 0.9;

	auto P_LIGHT_EXPLOSION = std::make_unique<Material>();
	P_LIGHT_EXPLOSION->MatCBIndex = TEXTURE_INDEX_P_LIGHT_EXPLOSION;
	P_LIGHT_EXPLOSION->DiffuseSrvHeapIndex = TEXTURE_INDEX_P_LIGHT_EXPLOSION;
	P_LIGHT_EXPLOSION->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	P_LIGHT_EXPLOSION->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	P_LIGHT_EXPLOSION->Roughness = 0.9;

	auto LOGIN_BACK = std::make_unique<Material>();
	LOGIN_BACK->MatCBIndex = TEXTURE_INDEX_UI_LOGIN_BACK;
	LOGIN_BACK->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_LOGIN_BACK;
	LOGIN_BACK->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	LOGIN_BACK->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	LOGIN_BACK->Roughness = 0.9;

	auto LOGIN_ID_INPUT = std::make_unique<Material>();
	LOGIN_ID_INPUT->MatCBIndex = TEXTURE_INDEX_UI_LOGIN_ID_INPUT;
	LOGIN_ID_INPUT->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_LOGIN_ID_INPUT;
	LOGIN_ID_INPUT->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	LOGIN_ID_INPUT->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	LOGIN_ID_INPUT->Roughness = 0.9;

	auto LOGIN_PW_INPUT = std::make_unique<Material>();
	LOGIN_PW_INPUT->MatCBIndex = TEXTURE_INDEX_UI_LOGIN_PW_INPUT;
	LOGIN_PW_INPUT->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_LOGIN_PW_INPUT;
	LOGIN_PW_INPUT->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	LOGIN_PW_INPUT->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	LOGIN_PW_INPUT->Roughness = 0.9;

	auto CRUSH_MIRROR= std::make_unique<Material>();
	CRUSH_MIRROR->MatCBIndex = TEXTURE_INDEX_CRUSH_MIRROR;
	CRUSH_MIRROR->DiffuseSrvHeapIndex = TEXTURE_INDEX_CRUSH_MIRROR;
	CRUSH_MIRROR->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	CRUSH_MIRROR->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	CRUSH_MIRROR->Roughness = 0.9;

	auto RULE_MANUAL = std::make_unique<Material>();
	RULE_MANUAL->MatCBIndex = TEXTURE_INDEX_UI_RULE_MANUAL;
	RULE_MANUAL->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_RULE_MANUAL;
	RULE_MANUAL->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	RULE_MANUAL->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	RULE_MANUAL->Roughness = 0.9;

	auto X = std::make_unique<Material>();
	X->MatCBIndex = TEXTURE_INDEX_UI_X;
	X->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_X;
	X->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	X->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	X->Roughness = 0.9;

	auto RE_BACK = std::make_unique<Material>();
	RE_BACK->MatCBIndex = TEXTURE_INDEX_UI_RENEWAL_BACK;
	RE_BACK->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_RENEWAL_BACK;
	RE_BACK->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	RE_BACK->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	RE_BACK->Roughness = 0.9;

	auto RE_ARROW = std::make_unique<Material>();
	RE_ARROW->MatCBIndex = TEXTURE_INDEX_UI_RENEWAL_ARROW;
	RE_ARROW->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_RENEWAL_ARROW;
	RE_ARROW->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	RE_ARROW->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	RE_ARROW->Roughness = 0.9;

	auto LOBBY_PRO_BACK = std::make_unique<Material>();
	LOBBY_PRO_BACK->MatCBIndex = TEXTURE_INDEX_UI_LOBBY_PRO_BACK;
	LOBBY_PRO_BACK->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_LOBBY_PRO_BACK;
	LOBBY_PRO_BACK->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	LOBBY_PRO_BACK->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	LOBBY_PRO_BACK->Roughness = 0.9;

	auto LOBBY_ROOMLIST_BACK = std::make_unique<Material>();
	LOBBY_ROOMLIST_BACK->MatCBIndex = TEXTURE_INDEX_UI_LOBBY_ROOMLIST_BACK;
	LOBBY_ROOMLIST_BACK->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_LOBBY_ROOMLIST_BACK;
	LOBBY_ROOMLIST_BACK->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	LOBBY_ROOMLIST_BACK->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	LOBBY_ROOMLIST_BACK->Roughness = 0.9;

	auto LOBBY_ROOMLIST_R = std::make_unique<Material>();
	LOBBY_ROOMLIST_R->MatCBIndex = TEXTURE_INDEX_UI_LOBBY_ROOMLIST_R;
	LOBBY_ROOMLIST_R->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_LOBBY_ROOMLIST_R;
	LOBBY_ROOMLIST_R->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	LOBBY_ROOMLIST_R->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	LOBBY_ROOMLIST_R->Roughness = 0.9;

	auto LOBBY_ROOMLIST_P = std::make_unique<Material>();
	LOBBY_ROOMLIST_P->MatCBIndex = TEXTURE_INDEX_UI_LOBBY_ROOMLIST_P;
	LOBBY_ROOMLIST_P->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_LOBBY_ROOMLIST_P;
	LOBBY_ROOMLIST_P->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	LOBBY_ROOMLIST_P->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	LOBBY_ROOMLIST_P->Roughness = 0.9;

	auto LOBBY_MAKEROOM_R = std::make_unique<Material>();
	LOBBY_MAKEROOM_R->MatCBIndex = TEXTURE_INDEX_UI_LOBBY_MAKE_ROOM_R;
	LOBBY_MAKEROOM_R->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_LOBBY_MAKE_ROOM_R;
	LOBBY_MAKEROOM_R->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	LOBBY_MAKEROOM_R->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	LOBBY_MAKEROOM_R->Roughness = 0.9;

	auto LOBBY_MAKEROOM_P = std::make_unique<Material>();
	LOBBY_MAKEROOM_P->MatCBIndex = TEXTURE_INDEX_UI_LOBBY_MAKE_ROOM_P;
	LOBBY_MAKEROOM_P->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_LOBBY_MAKE_ROOM_P;
	LOBBY_MAKEROOM_P->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	LOBBY_MAKEROOM_P->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	LOBBY_MAKEROOM_P->Roughness = 0.9;

	auto LOBBY_MAKEROOM_A = std::make_unique<Material>();
	LOBBY_MAKEROOM_A->MatCBIndex = TEXTURE_INDEX_UI_LOBBY_MAKE_ROOM_A;
	LOBBY_MAKEROOM_A->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_LOBBY_MAKE_ROOM_A;
	LOBBY_MAKEROOM_A->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	LOBBY_MAKEROOM_A->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	LOBBY_MAKEROOM_A->Roughness = 0.9;


	auto LOBBY_JOIN_ROOM_R = std::make_unique<Material>();
	LOBBY_JOIN_ROOM_R->MatCBIndex = TEXTURE_INDEX_UI_LOBBY_JOIN_ROOM_R;
	LOBBY_JOIN_ROOM_R->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_LOBBY_JOIN_ROOM_R;
	LOBBY_JOIN_ROOM_R->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	LOBBY_JOIN_ROOM_R->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	LOBBY_JOIN_ROOM_R->Roughness = 0.9;

	auto LOBBY_JOIN_ROOM_P = std::make_unique<Material>();
	LOBBY_JOIN_ROOM_P->MatCBIndex = TEXTURE_INDEX_UI_LOBBY_JOIN_ROOM_P;
	LOBBY_JOIN_ROOM_P->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_LOBBY_JOIN_ROOM_P;
	LOBBY_JOIN_ROOM_P->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	LOBBY_JOIN_ROOM_P->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	LOBBY_JOIN_ROOM_P->Roughness = 0.9;

	auto LOBBY_JOIN_ROOM_A = std::make_unique<Material>();
	LOBBY_JOIN_ROOM_A->MatCBIndex = TEXTURE_INDEX_UI_LOBBY_JOIN_ROOM_A;
	LOBBY_JOIN_ROOM_A->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_LOBBY_JOIN_ROOM_A;
	LOBBY_JOIN_ROOM_A->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	LOBBY_JOIN_ROOM_A->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	LOBBY_JOIN_ROOM_A->Roughness = 0.9;

	auto EXIT_WINDOW = std::make_unique<Material>();
	EXIT_WINDOW->MatCBIndex = TEXTURE_INDEX_UI_GAME_EXIT_WINDOW;
	EXIT_WINDOW->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_GAME_EXIT_WINDOW;
	EXIT_WINDOW->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	EXIT_WINDOW->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	EXIT_WINDOW->Roughness = 0.9;

	auto BTN_YES_R = std::make_unique<Material>();
	BTN_YES_R->MatCBIndex = TEXTURE_INDEX_UI_BTN_YES_R;
	BTN_YES_R->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_BTN_YES_R;
	BTN_YES_R->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	BTN_YES_R->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	BTN_YES_R->Roughness = 0.9;

	auto BTN_YES_P = std::make_unique<Material>();
	BTN_YES_P->MatCBIndex = TEXTURE_INDEX_UI_BTN_YES_P;
	BTN_YES_P->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_BTN_YES_P;
	BTN_YES_P->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	BTN_YES_P->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	BTN_YES_P->Roughness = 0.9;

	auto BTN_NO_R = std::make_unique<Material>();
	BTN_NO_R->MatCBIndex = TEXTURE_INDEX_UI_BTN_NO_R;
	BTN_NO_R->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_BTN_NO_R;
	BTN_NO_R->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	BTN_NO_R->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	BTN_NO_R->Roughness = 0.9;

	auto BTN_NO_P = std::make_unique<Material>();
	BTN_NO_P->MatCBIndex = TEXTURE_INDEX_UI_BTN_NO_P;
	BTN_NO_P->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_BTN_NO_P;
	BTN_NO_P->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	BTN_NO_P->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	BTN_NO_P->Roughness = 0.9;

	auto MAKE_ROOM_WIN = std::make_unique<Material>();
	MAKE_ROOM_WIN->MatCBIndex = TEXTURE_INDEX_UI_MAKE_ROOM_WINDOW;
	MAKE_ROOM_WIN->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_MAKE_ROOM_WINDOW;
	MAKE_ROOM_WIN->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	MAKE_ROOM_WIN->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	MAKE_ROOM_WIN->Roughness = 0.9;

	auto MAKE_ROOM_INPUT = std::make_unique<Material>();
	MAKE_ROOM_INPUT->MatCBIndex = TEXTURE_INDEX_UI_MAKE_ROOM_INPUT;
	MAKE_ROOM_INPUT->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_MAKE_ROOM_INPUT;
	MAKE_ROOM_INPUT->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	MAKE_ROOM_INPUT->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	MAKE_ROOM_INPUT->Roughness = 0.9;

	auto TIME_AlARM_1 = std::make_unique<Material>();
	TIME_AlARM_1->MatCBIndex = TEXTURE_INDEX_UI_TIME_ALARM_1;
	TIME_AlARM_1->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_TIME_ALARM_1;
	TIME_AlARM_1->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	TIME_AlARM_1->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	TIME_AlARM_1->Roughness = 0.9;

	auto TIME_AlARM_2 = std::make_unique<Material>();
	TIME_AlARM_2->MatCBIndex = TEXTURE_INDEX_UI_TIME_ALARM_2;
	TIME_AlARM_2->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_TIME_ALARM_2;
	TIME_AlARM_2->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	TIME_AlARM_2->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	TIME_AlARM_2->Roughness = 0.9;

	auto TIME_AlARM_3 = std::make_unique<Material>();
	TIME_AlARM_3->MatCBIndex = TEXTURE_INDEX_UI_TIME_ALARM_3;
	TIME_AlARM_3->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_TIME_ALARM_3;
	TIME_AlARM_3->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	TIME_AlARM_3->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	TIME_AlARM_3->Roughness = 0.9;

	auto TIME_AlARM_4 = std::make_unique<Material>();
	TIME_AlARM_4->MatCBIndex = TEXTURE_INDEX_UI_TIME_ALARM_4;
	TIME_AlARM_4->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_TIME_ALARM_4;
	TIME_AlARM_4->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	TIME_AlARM_4->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	TIME_AlARM_4->Roughness = 0.9;

	auto TIME_AlARM_5 = std::make_unique<Material>();
	TIME_AlARM_5->MatCBIndex = TEXTURE_INDEX_UI_TIME_ALARM_5;
	TIME_AlARM_5->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_TIME_ALARM_5;
	TIME_AlARM_5->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	TIME_AlARM_5->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	TIME_AlARM_5->Roughness = 0.9;

	auto TIME_AlARM_6 = std::make_unique<Material>();
	TIME_AlARM_6->MatCBIndex = TEXTURE_INDEX_UI_TIME_ALARM_6;
	TIME_AlARM_6->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_TIME_ALARM_6;
	TIME_AlARM_6->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	TIME_AlARM_6->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	TIME_AlARM_6->Roughness = 0.9;

	auto TIME_AlARM_7 = std::make_unique<Material>();
	TIME_AlARM_7->MatCBIndex = TEXTURE_INDEX_UI_TIME_ALARM_7;
	TIME_AlARM_7->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_TIME_ALARM_7;
	TIME_AlARM_7->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	TIME_AlARM_7->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	TIME_AlARM_7->Roughness = 0.9;

	auto TIME_AlARM_8 = std::make_unique<Material>();
	TIME_AlARM_8->MatCBIndex = TEXTURE_INDEX_UI_TIME_ALARM_8;
	TIME_AlARM_8->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_TIME_ALARM_8;
	TIME_AlARM_8->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	TIME_AlARM_8->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	TIME_AlARM_8->Roughness = 0.9;

	auto TIME_AlARM_9 = std::make_unique<Material>();
	TIME_AlARM_9->MatCBIndex = TEXTURE_INDEX_UI_TIME_ALARM_9;
	TIME_AlARM_9->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_TIME_ALARM_9;
	TIME_AlARM_9->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	TIME_AlARM_9->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	TIME_AlARM_9->Roughness = 0.9;

	auto TIME_AlARM_10 = std::make_unique<Material>();
	TIME_AlARM_10->MatCBIndex = TEXTURE_INDEX_UI_TIME_ALARM_10;
	TIME_AlARM_10->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_TIME_ALARM_10;
	TIME_AlARM_10->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	TIME_AlARM_10->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	TIME_AlARM_10->Roughness = 0.9;

	auto PLAYBOARD_BACKGROUND = std::make_unique<Material>();
	PLAYBOARD_BACKGROUND->MatCBIndex = TEXTURE_INDEX_UI_PLAYBOARD_BACKGROUND;
	PLAYBOARD_BACKGROUND->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_PLAYBOARD_BACKGROUND;
	PLAYBOARD_BACKGROUND->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	PLAYBOARD_BACKGROUND->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	PLAYBOARD_BACKGROUND->Roughness = 0.9;

	auto PLAYBOARD_DRU = std::make_unique<Material>();
	PLAYBOARD_DRU->MatCBIndex = TEXTURE_INDEX_UI_PLAYBOARD_DRU;
	PLAYBOARD_DRU->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_PLAYBOARD_DRU;
	PLAYBOARD_DRU->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	PLAYBOARD_DRU->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	PLAYBOARD_DRU->Roughness = 0.9;

	auto PLAYBOARD_DRU_D = std::make_unique<Material>();
	PLAYBOARD_DRU_D->MatCBIndex = TEXTURE_INDEX_UI_PLAYBOARD_DRU_D;
	PLAYBOARD_DRU_D->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_PLAYBOARD_DRU_D;
	PLAYBOARD_DRU_D->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	PLAYBOARD_DRU_D->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	PLAYBOARD_DRU_D->Roughness = 0.9;

	auto PLAYBOARD_FEA = std::make_unique<Material>();
	PLAYBOARD_FEA->MatCBIndex = TEXTURE_INDEX_UI_PLAYBOARD_FEA;
	PLAYBOARD_FEA->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_PLAYBOARD_FEA;
	PLAYBOARD_FEA->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	PLAYBOARD_FEA->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	PLAYBOARD_FEA->Roughness = 0.9;

	auto PLAYBOARD_FEA_D = std::make_unique<Material>();
	PLAYBOARD_FEA_D->MatCBIndex = TEXTURE_INDEX_UI_PLAYBOARD_FEA_D;
	PLAYBOARD_FEA_D->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_PLAYBOARD_FEA_D;
	PLAYBOARD_FEA_D->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	PLAYBOARD_FEA_D->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	PLAYBOARD_FEA_D->Roughness = 0.9;

	auto PLAYBOARD_KING = std::make_unique<Material>();
	PLAYBOARD_KING->MatCBIndex = TEXTURE_INDEX_UI_PLAYBOARD_KING;
	PLAYBOARD_KING->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_PLAYBOARD_KING;
	PLAYBOARD_KING->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	PLAYBOARD_KING->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	PLAYBOARD_KING->Roughness = 0.9;

	auto PLAYBOARD_KING_D = std::make_unique<Material>();
	PLAYBOARD_KING_D->MatCBIndex = TEXTURE_INDEX_UI_PLAYBOARD_KING_D;
	PLAYBOARD_KING_D->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_PLAYBOARD_KING_D;
	PLAYBOARD_KING_D->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	PLAYBOARD_KING_D->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	PLAYBOARD_KING_D->Roughness = 0.9;

	auto PLAYBOARD_LEE = std::make_unique<Material>();
	PLAYBOARD_LEE->MatCBIndex = TEXTURE_INDEX_UI_PLAYBOARD_LEE;
	PLAYBOARD_LEE->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_PLAYBOARD_LEE;
	PLAYBOARD_LEE->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	PLAYBOARD_LEE->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	PLAYBOARD_LEE->Roughness = 0.9;

	auto PLAYBOARD_LEE_D = std::make_unique<Material>();
	PLAYBOARD_LEE_D->MatCBIndex = TEXTURE_INDEX_UI_PLAYBOARD_LEE_D;
	PLAYBOARD_LEE_D->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_PLAYBOARD_LEE_D;
	PLAYBOARD_LEE_D->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	PLAYBOARD_LEE_D->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	PLAYBOARD_LEE_D->Roughness = 0.9;

	auto PLAYBOARD_MASTER = std::make_unique<Material>();
	PLAYBOARD_MASTER->MatCBIndex = TEXTURE_INDEX_UI_PLAYBOARD_MASTER;
	PLAYBOARD_MASTER->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_PLAYBOARD_MASTER;
	PLAYBOARD_MASTER->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	PLAYBOARD_MASTER->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	PLAYBOARD_MASTER->Roughness = 0.9;

	auto PLAYBOARD_RED = std::make_unique<Material>();
	PLAYBOARD_RED->MatCBIndex = TEXTURE_INDEX_UI_PLAYBOARD_RED;
	PLAYBOARD_RED->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_PLAYBOARD_RED;
	PLAYBOARD_RED->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	PLAYBOARD_RED->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	PLAYBOARD_RED->Roughness = 0.9;

	auto PLAYBOARD_RED_D = std::make_unique<Material>();
	PLAYBOARD_RED_D->MatCBIndex = TEXTURE_INDEX_UI_PLAYBOARD_RED_D;
	PLAYBOARD_RED_D->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_PLAYBOARD_RED_D;
	PLAYBOARD_RED_D->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	PLAYBOARD_RED_D->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	PLAYBOARD_RED_D->Roughness = 0.9;

	auto PLAYBOARD_SCROLL = std::make_unique<Material>();
	PLAYBOARD_SCROLL->MatCBIndex = TEXTURE_INDEX_UI_PLAYBOARD_SCROLL;
	PLAYBOARD_SCROLL->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_PLAYBOARD_SCROLL;
	PLAYBOARD_SCROLL->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	PLAYBOARD_SCROLL->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	PLAYBOARD_SCROLL->Roughness = 0.9;

	auto BTN_CANCEL_R = std::make_unique<Material>();
	BTN_CANCEL_R->MatCBIndex = TEXTURE_INDEX_UI_CANCEL_R;
	BTN_CANCEL_R->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_CANCEL_R;
	BTN_CANCEL_R->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	BTN_CANCEL_R->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	BTN_CANCEL_R->Roughness = 0.9;

	auto BTN_CANCEL_P = std::make_unique<Material>();
	BTN_CANCEL_P->MatCBIndex = TEXTURE_INDEX_UI_CANCEL_P;
	BTN_CANCEL_P->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_CANCEL_P;
	BTN_CANCEL_P->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	BTN_CANCEL_P->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	BTN_CANCEL_P->Roughness = 0.9;

	auto BTN_CREATE_R = std::make_unique<Material>();
	BTN_CREATE_R->MatCBIndex = TEXTURE_INDEX_UI_CREATE_R;
	BTN_CREATE_R->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_CREATE_R;
	BTN_CREATE_R->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	BTN_CREATE_R->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	BTN_CREATE_R->Roughness = 0.9;

	auto BTN_CREATE_P = std::make_unique<Material>();
	BTN_CREATE_P->MatCBIndex = TEXTURE_INDEX_UI_CREATE_P;
	BTN_CREATE_P->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_CREATE_P;
	BTN_CREATE_P->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	BTN_CREATE_P->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	BTN_CREATE_P->Roughness = 0.9;

	auto BTN_RENEWAL_R = std::make_unique<Material>();
	BTN_RENEWAL_R->MatCBIndex = TEXTURE_INDEX_UI_BTN_RENEWAL_R;
	BTN_RENEWAL_R->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_BTN_RENEWAL_R;
	BTN_RENEWAL_R->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	BTN_RENEWAL_R->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	BTN_RENEWAL_R->Roughness = 0.9;

	auto BTN_RENEWAL_P = std::make_unique<Material>();
	BTN_RENEWAL_P->MatCBIndex = TEXTURE_INDEX_UI_BTN_RENEWAL_P;
	BTN_RENEWAL_P->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_BTN_RENEWAL_P;
	BTN_RENEWAL_P->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	BTN_RENEWAL_P->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	BTN_RENEWAL_P->Roughness = 0.9;

	auto BTN_RENEWAL_U = std::make_unique<Material>();
	BTN_RENEWAL_U->MatCBIndex = TEXTURE_INDEX_UI_BTN_RENEWAL_U;
	BTN_RENEWAL_U->DiffuseSrvHeapIndex = TEXTURE_INDEX_UI_BTN_RENEWAL_U;
	BTN_RENEWAL_U->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	BTN_RENEWAL_U->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	BTN_RENEWAL_U->Roughness = 0.9;

	auto MAGE_ROOM = std::make_unique<Material>();
	MAGE_ROOM->MatCBIndex = TEXTURE_INDEX_Mage_Room;
	MAGE_ROOM->DiffuseSrvHeapIndex = TEXTURE_INDEX_Mage_Room;
	MAGE_ROOM->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	MAGE_ROOM->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	MAGE_ROOM->Roughness = 0.9;

	auto STAR_BACKGROUND = std::make_unique<Material>();
	STAR_BACKGROUND->MatCBIndex = TEXTURE_INDEX_STAR_BACKGROUND;
	STAR_BACKGROUND->DiffuseSrvHeapIndex = TEXTURE_INDEX_STAR_BACKGROUND;
	STAR_BACKGROUND->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	STAR_BACKGROUND->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	STAR_BACKGROUND->Roughness = 0.9;

	auto WIN_MASTER = std::make_unique<Material>();
	WIN_MASTER->MatCBIndex = TEXTURE_INDEX_WINNER_MASTER;
	WIN_MASTER->DiffuseSrvHeapIndex = TEXTURE_INDEX_WINNER_MASTER;
	WIN_MASTER->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	WIN_MASTER->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	WIN_MASTER->Roughness = 0.9;

	auto WIN_STUDENT= std::make_unique<Material>();
	WIN_STUDENT->MatCBIndex = TEXTURE_INDEX_WINNER_STUDENT;
	WIN_STUDENT->DiffuseSrvHeapIndex = TEXTURE_INDEX_WINNER_STUDENT;
	WIN_STUDENT->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	WIN_STUDENT->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	WIN_STUDENT->Roughness = 0.9;

	auto SIGN_UP_WIN = std::make_unique<Material>();
	SIGN_UP_WIN->MatCBIndex = TEXTURE_INDEX_SIGNUP_WIN;
	SIGN_UP_WIN->DiffuseSrvHeapIndex = TEXTURE_INDEX_SIGNUP_WIN;
	SIGN_UP_WIN->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	SIGN_UP_WIN->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	SIGN_UP_WIN->Roughness = 0.9;

	auto LOGO_CACHECACHE = std::make_unique<Material>();
	LOGO_CACHECACHE->MatCBIndex = TEXTURE_INDEX_LOGO_CACHECACHE;
	LOGO_CACHECACHE->DiffuseSrvHeapIndex = TEXTURE_INDEX_LOGO_CACHECACHE;
	LOGO_CACHECACHE->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	LOGO_CACHECACHE->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	LOGO_CACHECACHE->Roughness = 0.9;

	auto TIME_BACKGROUND = std::make_unique<Material>();
	TIME_BACKGROUND->MatCBIndex = TEXTURE_INDEX_TIME_BACKGROUND;
	TIME_BACKGROUND->DiffuseSrvHeapIndex = TEXTURE_INDEX_TIME_BACKGROUND;
	TIME_BACKGROUND->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	TIME_BACKGROUND->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	TIME_BACKGROUND->Roughness = 0.9;

	auto UI_PRO_BACK_MASTER = std::make_unique<Material>();
	UI_PRO_BACK_MASTER->MatCBIndex = TEXTURE_INDEX_PRO_BACK_MASTER;
	UI_PRO_BACK_MASTER->DiffuseSrvHeapIndex = TEXTURE_INDEX_PRO_BACK_MASTER;
	UI_PRO_BACK_MASTER->DiffuseAlbedo = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	UI_PRO_BACK_MASTER->FresnelR0 = XMFLOAT3(0.0f, 0.0f, 0.0f);
	UI_PRO_BACK_MASTER->Roughness = 0.9;

	auto MAGIC_CIRCLE = std::make_unique<Material>();
	MAGIC_CIRCLE->MatCBIndex = TEXTURE_INDEX_MAGIC_CIRCLE;
	MAGIC_CIRCLE->DiffuseSrvHeapIndex = TEXTURE_INDEX_MAGIC_CIRCLE;
	MAGIC_CIRCLE->DiffuseAlbedo = XMFLOAT4(0.7f, 0.7f, 0.7f, 1.f);
	MAGIC_CIRCLE->FresnelR0 = XMFLOAT3(0.01f, 0.01f, 0.01f);
	MAGIC_CIRCLE->Roughness = 0.1;

	auto UI_BA_ORIGIN = std::make_unique<Material>();
	UI_BA_ORIGIN->MatCBIndex = TEXTURE_INDEX_BA_ORIGIN;
	UI_BA_ORIGIN->DiffuseSrvHeapIndex = TEXTURE_INDEX_BA_ORIGIN;
	UI_BA_ORIGIN->DiffuseAlbedo = XMFLOAT4(0.7f, 0.7f, 0.7f, 1.f);
	UI_BA_ORIGIN->FresnelR0 = XMFLOAT3(0.01f, 0.01f, 0.01f);
	UI_BA_ORIGIN->Roughness = 0.1;

	auto UI_DU_ORIGIN = std::make_unique<Material>();
	UI_DU_ORIGIN->MatCBIndex = TEXTURE_INDEX_DU_ORIGIN;
	UI_DU_ORIGIN->DiffuseSrvHeapIndex = TEXTURE_INDEX_DU_ORIGIN;
	UI_DU_ORIGIN->DiffuseAlbedo = XMFLOAT4(0.7f, 0.7f, 0.7f, 1.f);
	UI_DU_ORIGIN->FresnelR0 = XMFLOAT3(0.01f, 0.01f, 0.01f);
	UI_DU_ORIGIN->Roughness = 0.1;

	auto UI_SO_ORIGIN = std::make_unique<Material>();
	UI_SO_ORIGIN->MatCBIndex = TEXTURE_INDEX_SO_ORIGIN;
	UI_SO_ORIGIN->DiffuseSrvHeapIndex = TEXTURE_INDEX_SO_ORIGIN;
	UI_SO_ORIGIN->DiffuseAlbedo = XMFLOAT4(0.7f, 0.7f, 0.7f, 1.f);
	UI_SO_ORIGIN->FresnelR0 = XMFLOAT3(0.01f, 0.01f, 0.01f);
	UI_SO_ORIGIN->Roughness = 0.1;

	auto UI_FE_ORIGIN = std::make_unique<Material>();
	UI_FE_ORIGIN->MatCBIndex = TEXTURE_INDEX_FE_ORIGIN;
	UI_FE_ORIGIN->DiffuseSrvHeapIndex = TEXTURE_INDEX_FE_ORIGIN;
	UI_FE_ORIGIN->DiffuseAlbedo = XMFLOAT4(0.7f, 0.7f, 0.7f, 1.f);
	UI_FE_ORIGIN->FresnelR0 = XMFLOAT3(0.01f, 0.01f, 0.01f);
	UI_FE_ORIGIN->Roughness = 0.1;

	auto UI_MA_ORIGIN = std::make_unique<Material>();
	UI_MA_ORIGIN->MatCBIndex = TEXTURE_INDEX_MA_ORIGIN;
	UI_MA_ORIGIN->DiffuseSrvHeapIndex = TEXTURE_INDEX_MA_ORIGIN;
	UI_MA_ORIGIN->DiffuseAlbedo = XMFLOAT4(0.7f, 0.7f, 0.7f, 1.f);
	UI_MA_ORIGIN->FresnelR0 = XMFLOAT3(0.01f, 0.01f, 0.01f);
	UI_MA_ORIGIN->Roughness = 0.1;

	auto AUTOMATCH_LOADING_1 = std::make_unique<Material>();
	AUTOMATCH_LOADING_1->MatCBIndex = TEXTURE_INDEX_AUTOMATCH_LOADING_1;
	AUTOMATCH_LOADING_1->DiffuseSrvHeapIndex = TEXTURE_INDEX_AUTOMATCH_LOADING_1;
	AUTOMATCH_LOADING_1->DiffuseAlbedo = XMFLOAT4(0.7f, 0.7f, 0.7f, 1.f);
	AUTOMATCH_LOADING_1->FresnelR0 = XMFLOAT3(0.01f, 0.01f, 0.01f);
	AUTOMATCH_LOADING_1->Roughness = 0.1;

	auto AUTOMATCH_LOADING_2 = std::make_unique<Material>();
	AUTOMATCH_LOADING_2->MatCBIndex = TEXTURE_INDEX_AUTOMATCH_LOADING_2;
	AUTOMATCH_LOADING_2->DiffuseSrvHeapIndex = TEXTURE_INDEX_AUTOMATCH_LOADING_2;
	AUTOMATCH_LOADING_2->DiffuseAlbedo = XMFLOAT4(0.7f, 0.7f, 0.7f, 1.f);
	AUTOMATCH_LOADING_2->FresnelR0 = XMFLOAT3(0.01f, 0.01f, 0.01f);
	AUTOMATCH_LOADING_2->Roughness = 0.1;

	auto AUTOMATCH_LOADING_3 = std::make_unique<Material>();
	AUTOMATCH_LOADING_3->MatCBIndex = TEXTURE_INDEX_AUTOMATCH_LOADING_3;
	AUTOMATCH_LOADING_3->DiffuseSrvHeapIndex = TEXTURE_INDEX_AUTOMATCH_LOADING_3;
	AUTOMATCH_LOADING_3->DiffuseAlbedo = XMFLOAT4(0.7f, 0.7f, 0.7f, 1.f);
	AUTOMATCH_LOADING_3->FresnelR0 = XMFLOAT3(0.01f, 0.01f, 0.01f);
	AUTOMATCH_LOADING_3->Roughness = 0.1;

	auto AUTOMATCH_LOADING_4 = std::make_unique<Material>();
	AUTOMATCH_LOADING_4->MatCBIndex = TEXTURE_INDEX_AUTOMATCH_LOADING_4;
	AUTOMATCH_LOADING_4->DiffuseSrvHeapIndex = TEXTURE_INDEX_AUTOMATCH_LOADING_4;
	AUTOMATCH_LOADING_4->DiffuseAlbedo = XMFLOAT4(0.7f, 0.7f, 0.7f, 1.f);
	AUTOMATCH_LOADING_4->FresnelR0 = XMFLOAT3(0.01f, 0.01f, 0.01f);
	AUTOMATCH_LOADING_4->Roughness = 0.1;

	auto AUTOMATCH_LOADING_5 = std::make_unique<Material>();
	AUTOMATCH_LOADING_5->MatCBIndex = TEXTURE_INDEX_AUTOMATCH_LOADING_5;
	AUTOMATCH_LOADING_5->DiffuseSrvHeapIndex = TEXTURE_INDEX_AUTOMATCH_LOADING_5;
	AUTOMATCH_LOADING_5->DiffuseAlbedo = XMFLOAT4(0.7f, 0.7f, 0.7f, 1.f);
	AUTOMATCH_LOADING_5->FresnelR0 = XMFLOAT3(0.01f, 0.01f, 0.01f);
	AUTOMATCH_LOADING_5->Roughness = 0.1;

	auto AUTOMATCH_LOADING_6 = std::make_unique<Material>();
	AUTOMATCH_LOADING_6->MatCBIndex = TEXTURE_INDEX_AUTOMATCH_LOADING_6;
	AUTOMATCH_LOADING_6->DiffuseSrvHeapIndex = TEXTURE_INDEX_AUTOMATCH_LOADING_6;
	AUTOMATCH_LOADING_6->DiffuseAlbedo = XMFLOAT4(0.7f, 0.7f, 0.7f, 1.f);
	AUTOMATCH_LOADING_6->FresnelR0 = XMFLOAT3(0.01f, 0.01f, 0.01f);
	AUTOMATCH_LOADING_6->Roughness = 0.1;

	auto AUTOMATCH_LOADING_7 = std::make_unique<Material>();
	AUTOMATCH_LOADING_7->MatCBIndex = TEXTURE_INDEX_AUTOMATCH_LOADING_7;
	AUTOMATCH_LOADING_7->DiffuseSrvHeapIndex = TEXTURE_INDEX_AUTOMATCH_LOADING_7;
	AUTOMATCH_LOADING_7->DiffuseAlbedo = XMFLOAT4(0.7f, 0.7f, 0.7f, 1.f);
	AUTOMATCH_LOADING_7->FresnelR0 = XMFLOAT3(0.01f, 0.01f, 0.01f);
	AUTOMATCH_LOADING_7->Roughness = 0.1;

	auto AUTOMATCH_LOADING_8 = std::make_unique<Material>();
	AUTOMATCH_LOADING_8->MatCBIndex = TEXTURE_INDEX_AUTOMATCH_LOADING_8;
	AUTOMATCH_LOADING_8->DiffuseSrvHeapIndex = TEXTURE_INDEX_AUTOMATCH_LOADING_8;
	AUTOMATCH_LOADING_8->DiffuseAlbedo = XMFLOAT4(0.7f, 0.7f, 0.7f, 1.f);
	AUTOMATCH_LOADING_8->FresnelR0 = XMFLOAT3(0.01f, 0.01f, 0.01f);
	AUTOMATCH_LOADING_8->Roughness = 0.1;

	auto AUTOMATCH_ACTIVATION = std::make_unique<Material>();
	AUTOMATCH_ACTIVATION->MatCBIndex = TEXTURE_INDEX_AUTOMATCH_ACTIVATION;
	AUTOMATCH_ACTIVATION->DiffuseSrvHeapIndex = TEXTURE_INDEX_AUTOMATCH_ACTIVATION;
	AUTOMATCH_ACTIVATION->DiffuseAlbedo = XMFLOAT4(0.7f, 0.7f, 0.7f, 1.f);
	AUTOMATCH_ACTIVATION->FresnelR0 = XMFLOAT3(0.01f, 0.01f, 0.01f);
	AUTOMATCH_ACTIVATION->Roughness = 0.1;

	auto MANUALMATCH_ACTIVATION = std::make_unique<Material>();
	MANUALMATCH_ACTIVATION->MatCBIndex = TEXTURE_INDEX_MANUALMATCH_ACTIVATION;
	MANUALMATCH_ACTIVATION->DiffuseSrvHeapIndex = TEXTURE_INDEX_MANUALMATCH_ACTIVATION;
	MANUALMATCH_ACTIVATION->DiffuseAlbedo = XMFLOAT4(0.7f, 0.7f, 0.7f, 1.f);
	MANUALMATCH_ACTIVATION->FresnelR0 = XMFLOAT3(0.01f, 0.01f, 0.01f);
	MANUALMATCH_ACTIVATION->Roughness = 0.1;

	auto CHERRY_BLOSSOM = std::make_unique<Material>();
	CHERRY_BLOSSOM->MatCBIndex = TEXTURE_INDEX_P_CHERRY_BLOSSOM;
	CHERRY_BLOSSOM->DiffuseSrvHeapIndex = TEXTURE_INDEX_P_CHERRY_BLOSSOM;
	CHERRY_BLOSSOM->DiffuseAlbedo = XMFLOAT4(1.f, 1.f, 1.f, 1.f);
	CHERRY_BLOSSOM->FresnelR0 = XMFLOAT3(0.01f, 0.01f, 0.01f);
	CHERRY_BLOSSOM->Roughness = 0.9;

	auto MAGIC_CIRCLE_GREEN = std::make_unique<Material>();
	MAGIC_CIRCLE_GREEN->MatCBIndex = TEXTURE_INDEX_MAGIC_CIRCLE_GREEN;
	MAGIC_CIRCLE_GREEN->DiffuseSrvHeapIndex = TEXTURE_INDEX_MAGIC_CIRCLE_GREEN;
	MAGIC_CIRCLE_GREEN->DiffuseAlbedo = XMFLOAT4(1.f, 1.f, 1.f, 1.f);
	MAGIC_CIRCLE_GREEN->FresnelR0 = XMFLOAT3(0.01f, 0.01f, 0.01f);
	MAGIC_CIRCLE_GREEN->Roughness = 0.9;

	m_Materials["grasscube1024"] = std::move(sky);
	m_Materials["PolyAdventureTexture_01"] = std::move(PolyAdventureTexture_01);
	m_Materials[TEXTURE_STR_Polygon_Fantasy_Characters_Texture_01_A] = std::move(Polygon_Fantasy_Characters_Texture_01_A);
	m_Materials[TEXTURE_STR_Polygon_Fantasy_Characters_Texture_02_A] = std::move(Polygon_Fantasy_Characters_Texture_02_A);
	m_Materials[TEXTURE_STR_Polygon_Fantasy_Characters_Texture_03_A] = std::move(Polygon_Fantasy_Characters_Texture_03_A);
	m_Materials[TEXTURE_STR_Polygon_Fantasy_Characters_Texture_04_A] = std::move(Polygon_Fantasy_Characters_Texture_04_A);
	m_Materials[TEXTURE_STR_Polygon_Fantasy_Characters_Texture_05_A] = std::move(Polygon_Fantasy_Characters_Texture_05_A);
	m_Materials[TEXTURE_STR_UI_BOARD] = std::move(UI_Board);
	m_Materials[TEXTURE_STR_Thunderbolt] = std::move(Thunderbolt);
	m_Materials[TEXTURE_STR_UI_IMAGEIVEW_DU] = std::move(UI_DU);
	m_Materials[TEXTURE_STR_UI_IMAGEIVEW_BA] = std::move(UI_BA);
	m_Materials[TEXTURE_STR_UI_IMAGEIVEW_FP] = std::move(UI_FP);
	m_Materials[TEXTURE_STR_UI_IMAGEIVEW_MP] = std::move(UI_MP);
	m_Materials[TEXTURE_STR_UI_IMAGEIVEW_SO] = std::move(UI_SO);
	m_Materials[TEXTURE_STR_UI_BUTTON_START_RELEASED] = std::move(UI_START);
	m_Materials[TEXTURE_STR_UI_IMAGEVIEW_MAP] = std::move(UI_MAP);
	m_Materials[TEXTURE_STR_UI_BUTTON_READY_PRESSED] = std::move(UI_READY);
	m_Materials[TEXTURE_STR_UI_BUTTON_READY_RELEASED] = std::move(UI_READYOFF);
	m_Materials[TEXTURE_STR_UI_BUTTON_READY_ACTIVATION] = std::move(UI_READYINACTIVE);
	m_Materials[TEXTURE_STR_UI_BUTTON_START_PRESSED] = std::move(UI_START_OFF);
	m_Materials[TEXTURE_STR_UI_BUTTON_START_ACTIVATION] = std::move(UI_START_INACTIVE);
	m_Materials[TEXTURE_STR_UI_BUTTON_MAP_PREV_RELEASED] = std::move(UI_MAP_PREV_REL);
	m_Materials[TEXTURE_STR_UI_BUTTON_MAP_PREV_PRESSED] = std::move(UI_MAP_PREV_PRES);
	m_Materials[TEXTURE_STR_UI_BUTTON_MAP_PREV_ACTIVATION] = std::move(UI_MAP_PREV_ACT);
	m_Materials[TEXTURE_STR_UI_BUTTON_MAP_NEXT_RELEASED] = std::move(UI_MAP_NEXT_REL);
	m_Materials[TEXTURE_STR_UI_BUTTON_MAP_NEXT_PRESSED] = std::move(UI_MAP_NEXT_PRES);
	m_Materials[TEXTURE_STR_UI_BUTTON_MAP_NEXT_ACTIVATION] = std::move(UI_MAP_NEXT_ACT);
	m_Materials[TEXTURE_STR_UI_BUTTON_EXIT_RELEASED] =	std::move(UI_EXIT_REL);
	m_Materials[TEXTURE_STR_UI_BUTTON_EXIT_PRESSED] =	std::move(UI_EXIT_PRES);
	m_Materials[TEXTURE_STR_AIM] = std::move(AIM);
	m_Materials[TEXTURE_STR_KNIGHTS_TEXTURE_01] = std::move(Knights_Texture_01);
	m_Materials[TEXTURE_STR_UI_CHARACTER_READY] = std::move(Character_Ready);
	m_Materials[TEXTURE_STR_UI_CROWN] = std::move(Crown);
	m_Materials[TEXTURE_STR_UI_HP] = std::move(HP);
	m_Materials[TEXTURE_STR_UI_PRO_BACK] = std::move(PRO_BACK);
	m_Materials[TEXTURE_STR_UI_PRO_MASTER] = std::move(PRO_MASTER);
	m_Materials[TEXTURE_STR_UI_PRO_POTION] = std::move(PRO_POTION);
	m_Materials[TEXTURE_STR_UI_PRO_BA] = std::move(PRO_BA);
	m_Materials[TEXTURE_STR_UI_PRO_DU] = std::move(PRO_DU);
	m_Materials[TEXTURE_STR_UI_PRO_FE] = std::move(PRO_FE);
	m_Materials[TEXTURE_STR_UI_PRO_MA] = std::move(PRO_MA);
	m_Materials[TEXTURE_STR_UI_PRO_SO] = std::move(PRO_SO);
	m_Materials[TEXTURE_STR_UI_TIME_0] = std::move(TIME_0);
	m_Materials[TEXTURE_STR_UI_TIME_1] = std::move(TIME_1);
	m_Materials[TEXTURE_STR_UI_TIME_2] = std::move(TIME_2);
	m_Materials[TEXTURE_STR_UI_TIME_3] = std::move(TIME_3);
	m_Materials[TEXTURE_STR_UI_TIME_4] = std::move(TIME_4);
	m_Materials[TEXTURE_STR_UI_TIME_5] = std::move(TIME_5);
	m_Materials[TEXTURE_STR_UI_TIME_6] = std::move(TIME_6);
	m_Materials[TEXTURE_STR_UI_TIME_7] = std::move(TIME_7);
	m_Materials[TEXTURE_STR_UI_TIME_8] = std::move(TIME_8);
	m_Materials[TEXTURE_STR_BUTTON_BACK_RELEASED] = std::move(Back_rel);
	m_Materials[TEXTURE_STR_BUTTON_BACK_PRESSED] = std::move(Back_pres);
	m_Materials[TEXTURE_STR_UI_BUTTON_TITLE_SING_IN_RELEASED] = std::move(SING_IN);
	m_Materials[TEXTURE_STR_UI_BUTTON_TITLE_SING_IN_PRESSED] = std::move(SING_IN_P);
	m_Materials[TEXTURE_STR_UI_BUTTON_TITLE_SING_UP_RELEASED] = std::move(SING_UP_R);
	m_Materials[TEXTURE_STR_UI_BUTTON_TITLE_SING_UP_PRESSED] = std::move(SING_UP_P);
	m_Materials[TEXTURE_STR_UI_BUTTON_TITLE_SING_UP_ACTIVATION] = std::move(SING_UP_A);
	m_Materials[TEXTURE_STR_UI_BUTTON_LOBBY_AUTOMATCH_RELEASED] = std::move(AUTOMATCH_R);
	m_Materials[TEXTURE_STR_UI_BUTTON_LOBBY_AUTOMATCH_PRESSED] = std::move(AUTOMATCH_P);
	m_Materials[TEXTURE_STR_UI_BUTTON_LOBBY_MANUALMATCH_RELEASED] =	 std::move(MANUALMATCH_R);
	m_Materials[TEXTURE_STR_UI_BUTTON_LOBBY_MANUALMATCH_PRESSED] =	 std::move(MANUALMATCH_P);
	m_Materials[TEXTURE_STR_UI_BUTTON_LOBBY_RULE_RELEASED] =	std::move(RULE_R);
	m_Materials[TEXTURE_STR_UI_BUTTON_LOBBY_RULE_PRESSED] =		std::move(RULE_P);
	m_Materials[TEXTURE_STR_TRANSPARENCY] = std::move(TRANS);
	m_Materials[TEXTURE_STR_T_Smoke_Tiled_D] = std::move(T_Smoke_Tiled_D);
	m_Materials[TEXTURE_STR_P_PENALTY] = std::move(P_PENALTY);
	m_Materials[TEXTURE_STR_P_LIGHT_EXPLOSION] = std::move(P_LIGHT_EXPLOSION);
	m_Materials[TEXTURE_STR_UI_LOGIN_BACK] = std::move(LOGIN_BACK);
	m_Materials[TEXTURE_STR_UI_LOGIN_ID_INPUT] = std::move(LOGIN_ID_INPUT);
	m_Materials[TEXTURE_STR_UI_LOGIN_PW_INPUT] = std::move(LOGIN_PW_INPUT);
	m_Materials[TEXTURE_STR_CRUSH_MIRROR] = std::move(CRUSH_MIRROR);
	m_Materials[TEXTURE_STR_UI_RULE_MANUAL] = std::move(RULE_MANUAL);
	m_Materials[TEXTURE_STR_UI_X] = std::move(X);
	m_Materials[TEXTURE_STR_UI_RENEWAL_BACK] = std::move(RE_BACK);
	m_Materials[TEXTURE_STR_UI_RENEWAL_ARROW] = std::move(RE_ARROW);
	m_Materials[TEXTURE_STR_UI_LOBBY_PRO_BACK] = std::move(LOBBY_PRO_BACK);
	m_Materials[TEXTURE_STR_UI_LOBBY_ROOMLIST_BACK] = std::move(LOBBY_ROOMLIST_BACK);
	m_Materials[TEXTURE_STR_UI_LOBBY_ROOMLIST_R] = std::move(LOBBY_ROOMLIST_R);
	m_Materials[TEXTURE_STR_UI_LOBBY_ROOMLIST_P] = std::move(LOBBY_ROOMLIST_P);
	m_Materials[TEXTURE_STR_UI_LOBBY_MAKE_ROOM_R] = std::move(LOBBY_MAKEROOM_R);
	m_Materials[TEXTURE_STR_UI_LOBBY_MAKE_ROOM_P] = std::move(LOBBY_MAKEROOM_P);
	m_Materials[TEXTURE_STR_UI_LOBBY_MAKE_ROOM_A] = std::move(LOBBY_MAKEROOM_A);
	m_Materials[TEXTURE_STR_UI_LOBBY_JOIN_ROOM_R] = std::move(LOBBY_JOIN_ROOM_R);
	m_Materials[TEXTURE_STR_UI_LOBBY_JOIN_ROOM_P] = std::move(LOBBY_JOIN_ROOM_P);
	m_Materials[TEXTURE_STR_UI_LOBBY_JOIN_ROOM_A] = std::move(LOBBY_JOIN_ROOM_A);
	m_Materials[TEXTURE_STR_UI_GAME_EXIT_WINDOW] = std::move(EXIT_WINDOW);
	m_Materials[TEXTURE_STR_UI_BTN_YES_R] = std::move(BTN_YES_R);
	m_Materials[TEXTURE_STR_UI_BTN_YES_P] = std::move(BTN_YES_P);
	m_Materials[TEXTURE_STR_UI_BTN_NO_R] = std::move(BTN_NO_R);
	m_Materials[TEXTURE_STR_UI_BTN_NO_P] = std::move(BTN_NO_P);
	m_Materials[TEXTURE_STR_UI_MAKE_ROOM_WINDOW] = std::move(MAKE_ROOM_WIN);
	m_Materials[TEXTURE_STR_UI_MAKE_ROOM_INPUT] = std::move(MAKE_ROOM_INPUT);
	m_Materials[TEXTURE_STR_UI_TIME_ALARM_1] = std::move(TIME_AlARM_1);
	m_Materials[TEXTURE_STR_UI_TIME_ALARM_2] = std::move(TIME_AlARM_2);
	m_Materials[TEXTURE_STR_UI_TIME_ALARM_3] = std::move(TIME_AlARM_3);
	m_Materials[TEXTURE_STR_UI_TIME_ALARM_4] = std::move(TIME_AlARM_4);
	m_Materials[TEXTURE_STR_UI_TIME_ALARM_5] = std::move(TIME_AlARM_5);
	m_Materials[TEXTURE_STR_UI_TIME_ALARM_6] = std::move(TIME_AlARM_6);
	m_Materials[TEXTURE_STR_UI_TIME_ALARM_7] = std::move(TIME_AlARM_7);
	m_Materials[TEXTURE_STR_UI_TIME_ALARM_8] = std::move(TIME_AlARM_8);
	m_Materials[TEXTURE_STR_UI_TIME_ALARM_9] = std::move(TIME_AlARM_9);
	m_Materials[TEXTURE_STR_UI_TIME_ALARM_10] = std::move(TIME_AlARM_10);
	m_Materials[TEXTURE_STR_UI_PLAYBOARD_BACKGROUND] = std::move(PLAYBOARD_BACKGROUND);
	m_Materials[TEXTURE_STR_UI_PLAYBOARD_DRU] = std::move(PLAYBOARD_DRU);
	m_Materials[TEXTURE_STR_UI_PLAYBOARD_DRU_D] = std::move(PLAYBOARD_DRU_D);
	m_Materials[TEXTURE_STR_UI_PLAYBOARD_FEA] = std::move(PLAYBOARD_FEA);
	m_Materials[TEXTURE_STR_UI_PLAYBOARD_FEA_D] = std::move(PLAYBOARD_FEA_D);
	m_Materials[TEXTURE_STR_UI_PLAYBOARD_KING] = std::move(PLAYBOARD_KING);
	m_Materials[TEXTURE_STR_UI_PLAYBOARD_KING_D] = std::move(PLAYBOARD_KING_D);
	m_Materials[TEXTURE_STR_UI_PLAYBOARD_LEE] = std::move(PLAYBOARD_LEE);
	m_Materials[TEXTURE_STR_UI_PLAYBOARD_LEE_D] = std::move(PLAYBOARD_LEE_D);
	m_Materials[TEXTURE_STR_UI_PLAYBOARD_MASTER] = std::move(PLAYBOARD_MASTER);
	m_Materials[TEXTURE_STR_UI_PLAYBOARD_RED] = std::move(PLAYBOARD_RED);
	m_Materials[TEXTURE_STR_UI_PLAYBOARD_RED_D] = std::move(PLAYBOARD_RED_D);
	m_Materials[TEXTURE_STR_UI_PLAYBOARD_SCROLL] = std::move(PLAYBOARD_SCROLL);
	m_Materials[TEXTURE_STR_UI_CANCEL_R] = std::move(BTN_CANCEL_R);
	m_Materials[TEXTURE_STR_UI_CANCEL_P] = std::move(BTN_CANCEL_P);
	m_Materials[TEXTURE_STR_UI_CREATE_R] = std::move(BTN_CREATE_R);
	m_Materials[TEXTURE_STR_UI_CREATE_P] = std::move(BTN_CREATE_P);
	m_Materials[TEXTURE_STR_UI_BTN_RENEWAL_R] = std::move(BTN_RENEWAL_R);
	m_Materials[TEXTURE_STR_UI_BTN_RENEWAL_P] = std::move(BTN_RENEWAL_P);
	m_Materials[TEXTURE_STR_UI_BTN_RENEWAL_U] = std::move(BTN_RENEWAL_U);
	m_Materials[TEXTURE_STR_Mage_Room] = std::move(MAGE_ROOM);
	m_Materials[TEXTURE_STR_STAR_BACKGROUND] = std::move(STAR_BACKGROUND);
	m_Materials[TEXTURE_STR_WINNER_MASTER] = std::move(WIN_MASTER);
	m_Materials[TEXTURE_STR_WINNER_STUDENT] = std::move(WIN_STUDENT);
	m_Materials[TEXTURE_STR_SIGNUP_WIN] = std::move(SIGN_UP_WIN);
	m_Materials[TEXTURE_STR_LOGO_CACHECACHE] = std::move(LOGO_CACHECACHE);
	m_Materials[TEXTURE_STR_TIME_BACKGROUND] = std::move(TIME_BACKGROUND);
	m_Materials[TEXTURE_STR_PRO_BACK_MASTER] = std::move(UI_PRO_BACK_MASTER);
	m_Materials[TEXTURE_STR_MAGIC_CIRCLE] = std::move(MAGIC_CIRCLE);
	m_Materials[TEXTURE_STR_BA_ORIGIN] = std::move(UI_BA_ORIGIN);
	m_Materials[TEXTURE_STR_DU_ORIGIN] = std::move(UI_DU_ORIGIN);
	m_Materials[TEXTURE_STR_SO_ORIGIN] = std::move(UI_SO_ORIGIN);
	m_Materials[TEXTURE_STR_FE_ORIGIN] = std::move(UI_FE_ORIGIN);
	m_Materials[TEXTURE_STR_MA_ORIGIN] = std::move(UI_MA_ORIGIN);
	m_Materials[TEXTURE_STR_AUTOMATCH_LOADING_1] = std::move(AUTOMATCH_LOADING_1);
	m_Materials[TEXTURE_STR_AUTOMATCH_LOADING_2] = std::move(AUTOMATCH_LOADING_2);
	m_Materials[TEXTURE_STR_AUTOMATCH_LOADING_3] = std::move(AUTOMATCH_LOADING_3);
	m_Materials[TEXTURE_STR_AUTOMATCH_LOADING_4] = std::move(AUTOMATCH_LOADING_4);
	m_Materials[TEXTURE_STR_AUTOMATCH_LOADING_5] = std::move(AUTOMATCH_LOADING_5);
	m_Materials[TEXTURE_STR_AUTOMATCH_LOADING_6] = std::move(AUTOMATCH_LOADING_6);
	m_Materials[TEXTURE_STR_AUTOMATCH_LOADING_7] = std::move(AUTOMATCH_LOADING_7);
	m_Materials[TEXTURE_STR_AUTOMATCH_LOADING_8] = std::move(AUTOMATCH_LOADING_8);
	m_Materials[TEXTURE_STR_AUTOMATCH_ACTIVATION] = std::move(AUTOMATCH_ACTIVATION);
	m_Materials[TEXTURE_STR_MANUALMATCH_ACTIVATION] = std::move(MANUALMATCH_ACTIVATION);
	m_Materials[TEXTURE_STR_P_CHERRY_BLOSSOM] = std::move(CHERRY_BLOSSOM);
	m_Materials[TEXTURE_STR_MAGIC_CIRCLE_GREEN] = std::move(MAGIC_CIRCLE_GREEN);
}

void AssertsReference::BuildGeoMeshes(ID3D12Device* pDevice, ID3D12GraphicsCommandList* pCommandList)
{
	GeometryGenerator geoGen;
	GeometryGenerator::MeshData box = geoGen.CreateBox(1.0f, 1.0f, 1.0f, 2);
	GeometryGenerator::MeshData grid = geoGen.CreateGrid(0.1f, 0.1f, 2, 2);
	GeometryGenerator::MeshData sphere = geoGen.CreateSphere(0.5f, 20, 2);
	GeometryGenerator::MeshData cylinder = geoGen.CreateCylinder(0.5f, 0.3f, 3.0f, 20, 2);

	UINT boxVertexOffset = 0;
	UINT gridVertexOffset = (UINT)box.Vertices.size();
	UINT sphereVertexOffset = gridVertexOffset + (UINT)grid.Vertices.size();
	UINT cylinderVertexOffset = sphereVertexOffset + (UINT)sphere.Vertices.size();
	UINT quadVertexOffset = cylinderVertexOffset + (UINT)cylinder.Vertices.size();

	UINT boxIndexOffset = 0;
	UINT gridIndexOffset = (UINT)box.Indices32.size();
	UINT sphereIndexOffset = gridIndexOffset + (UINT)grid.Indices32.size();
	UINT cylinderIndexOffset = sphereIndexOffset + (UINT)sphere.Indices32.size();
	UINT quadIndexOffset = cylinderIndexOffset + (UINT)cylinder.Indices32.size();

	XMFLOAT3 vMinf3(+MathHelper::Infinity, +MathHelper::Infinity, +MathHelper::Infinity);
	XMFLOAT3 vMaxf3(-MathHelper::Infinity, -MathHelper::Infinity, -MathHelper::Infinity);
	XMVECTOR vMin = XMLoadFloat3(&vMinf3);
	XMVECTOR vMax = XMLoadFloat3(&vMaxf3);

	// Box Bounds
	for (auto& p : box.Vertices)
	{
		XMVECTOR P = XMLoadFloat3(&p.Pos);

		vMin = XMVectorMin(vMin, P);
		vMax = XMVectorMax(vMax, P);
	}

	BoundingBox boxbounds;
	XMStoreFloat3(&boxbounds.Center, 0.5f * (vMin + vMax));
	XMStoreFloat3(&boxbounds.Extents, 0.5f * (vMax - vMin));

	// Grid Bounds
	for (auto& p : grid.Vertices)
	{
		XMVECTOR P = XMLoadFloat3(&p.Pos);

		vMin = XMVectorMin(vMin, P);
		vMax = XMVectorMax(vMax, P);
	}

	BoundingBox gridbounds;
	XMStoreFloat3(&gridbounds.Center, 0.5f * (vMin + vMax));
	XMStoreFloat3(&gridbounds.Extents, 0.5f * (vMax - vMin));

	// Sphere Bounds
	for (auto& p : sphere.Vertices)
	{
		XMVECTOR P = XMLoadFloat3(&p.Pos);

		vMin = XMVectorMin(vMin, P);
		vMax = XMVectorMax(vMax, P);
	}

	BoundingBox spherebounds;
	XMStoreFloat3(&spherebounds.Center, 0.5f * (vMin + vMax));
	XMStoreFloat3(&spherebounds.Extents, 0.5f * (vMax - vMin));

	// Submesh
	SubmeshGeometry boxSubmesh;
	boxSubmesh.IndexCount = (UINT)box.Indices32.size();
	boxSubmesh.StartIndexLocation = boxIndexOffset;
	boxSubmesh.BaseVertexLocation = boxVertexOffset;
	boxSubmesh.Bounds = boxbounds;

	SubmeshGeometry gridSubmesh;
	gridSubmesh.IndexCount = (UINT)grid.Indices32.size();
	gridSubmesh.StartIndexLocation = gridIndexOffset;
	gridSubmesh.BaseVertexLocation = gridVertexOffset;
	gridSubmesh.Bounds = gridbounds;

	SubmeshGeometry sphereSubmesh;
	sphereSubmesh.IndexCount = (UINT)sphere.Indices32.size();
	sphereSubmesh.StartIndexLocation = sphereIndexOffset;
	sphereSubmesh.BaseVertexLocation = sphereVertexOffset;
	sphereSubmesh.Bounds = spherebounds;

	SubmeshGeometry cylinderSubmesh;
	cylinderSubmesh.IndexCount = (UINT)cylinder.Indices32.size();
	cylinderSubmesh.StartIndexLocation = cylinderIndexOffset;
	cylinderSubmesh.BaseVertexLocation = cylinderVertexOffset;

	auto totalVertexCount =
		box.Vertices.size() +
		grid.Vertices.size() +
		sphere.Vertices.size() +
		cylinder.Vertices.size();

	std::vector<Vertex> vertices(totalVertexCount);

	UINT k = 0;
	for (size_t i = 0; i < box.Vertices.size(); ++i, ++k)
	{
		vertices[k].Pos = box.Vertices[i].Pos;
		vertices[k].Normal = box.Vertices[i].Normal;
		vertices[k].TexC = box.Vertices[i].TexC;
	}

	for (size_t i = 0; i < grid.Vertices.size(); ++i, ++k)
	{
		vertices[k].Pos = grid.Vertices[i].Pos;
		vertices[k].Normal = grid.Vertices[i].Normal;
		vertices[k].TexC = grid.Vertices[i].TexC;
	}

	for (size_t i = 0; i < sphere.Vertices.size(); ++i, ++k)
	{
		vertices[k].Pos = sphere.Vertices[i].Pos;
		vertices[k].Normal = sphere.Vertices[i].Normal;
		vertices[k].TexC = sphere.Vertices[i].TexC;
	}

	for (size_t i = 0; i < cylinder.Vertices.size(); ++i, ++k)
	{
		vertices[k].Pos = cylinder.Vertices[i].Pos;
		vertices[k].Normal = cylinder.Vertices[i].Normal;
		vertices[k].TexC = cylinder.Vertices[i].TexC;
	}

	std::vector<std::uint16_t> indices;
	indices.insert(indices.end(), std::begin(box.GetIndices16()), std::end(box.GetIndices16()));
	indices.insert(indices.end(), std::begin(grid.GetIndices16()), std::end(grid.GetIndices16()));
	indices.insert(indices.end(), std::begin(sphere.GetIndices16()), std::end(sphere.GetIndices16()));
	indices.insert(indices.end(), std::begin(cylinder.GetIndices16()), std::end(cylinder.GetIndices16()));

	const UINT vbByteSize = (UINT)vertices.size() * sizeof(Vertex);
	const UINT ibByteSize = (UINT)indices.size() * sizeof(std::uint16_t);

	auto geo = std::make_unique<GeometryMesh>();

	ThrowIfFailed(D3DCreateBlob(vbByteSize, &geo->VertexBufferCPU));
	CopyMemory(geo->VertexBufferCPU->GetBufferPointer(), vertices.data(), vbByteSize);

	ThrowIfFailed(D3DCreateBlob(ibByteSize, &geo->IndexBufferCPU));
	CopyMemory(geo->IndexBufferCPU->GetBufferPointer(), indices.data(), ibByteSize);

	geo->VertexBufferGPU = d3dUtil::CreateDefaultBuffer(pDevice,
		pCommandList, vertices.data(), vbByteSize, geo->VertexBufferUploader);

	geo->IndexBufferGPU = d3dUtil::CreateDefaultBuffer(pDevice,
		pCommandList, indices.data(), ibByteSize, geo->IndexBufferUploader);

	geo->VertexByteStride = sizeof(Vertex);
	geo->VertexBufferByteSize = vbByteSize;
	geo->IndexFormat = DXGI_FORMAT_R16_UINT;
	geo->IndexBufferByteSize = ibByteSize;

	geo->DrawArgs[MESH_GEOID_RECT] = boxSubmesh;
	geo->DrawArgs[MESH_GEOID_GRID] = gridSubmesh;
	geo->DrawArgs[MESH_GEOID_SPHERE] = sphereSubmesh;
	geo->DrawArgs["cylinder"] = cylinderSubmesh;

	m_GeometryMesh[MESH_GEOID] = std::move(geo);
}

void AssertsReference::BuildModel(ID3D12Device* pDevice, ID3D12GraphicsCommandList* pCommandList, std::string meshName)
{
	// meshName으로 같은 메쉬가 있는지 체크
	if (m_GeometryMesh.count(meshName)) {
		cout << "이미 해당메쉬는 로드되었습니다." << endl;
	}
	else 
	{
		string path = "Models\\Model\\" + meshName;

		std::vector<Vertex> vertices;
		std::vector<std::uint32_t> indices;
		std::vector<Material> materials;

		LoadMeshFile(vertices, indices, &materials, path);

		const UINT vbByteSize = vertices.size() * sizeof(Vertex);
		const UINT ibByteSize = indices.size() * sizeof(std::int32_t);

		auto geo = std::make_unique<GeometryMesh>();
		geo->Name = meshName;

		ThrowIfFailed(D3DCreateBlob(vbByteSize, &geo->VertexBufferCPU));
		CopyMemory(geo->VertexBufferCPU->GetBufferPointer(), vertices.data(), vbByteSize);

		ThrowIfFailed(D3DCreateBlob(ibByteSize, &geo->IndexBufferCPU));
		CopyMemory(geo->IndexBufferCPU->GetBufferPointer(), indices.data(), ibByteSize);

		geo->VertexBufferGPU = d3dUtil::CreateDefaultBuffer(pDevice,
			pCommandList, vertices.data(), vbByteSize, geo->VertexBufferUploader);

		geo->IndexBufferGPU = d3dUtil::CreateDefaultBuffer(pDevice,
			pCommandList, indices.data(), ibByteSize, geo->IndexBufferUploader);

		geo->VertexByteStride = sizeof(Vertex);
		geo->VertexBufferByteSize = vbByteSize;
		geo->IndexFormat = DXGI_FORMAT_R32_UINT;
		geo->IndexBufferByteSize = ibByteSize;

		// Bounds
		BoundingBox bounds;

		auto iter = m_PropBoundingBox.find(meshName);
		if (iter != m_PropBoundingBox.end())
		{
			bounds = *(m_PropBoundingBox[meshName]);
		}
		else
		{
			XMFLOAT3 vMinf3(+MathHelper::Infinity, +MathHelper::Infinity, +MathHelper::Infinity);
			XMFLOAT3 vMaxf3(-MathHelper::Infinity, -MathHelper::Infinity, -MathHelper::Infinity);
			XMVECTOR vMin = XMLoadFloat3(&vMinf3);
			XMVECTOR vMax = XMLoadFloat3(&vMaxf3);

			for (auto& p : vertices)
			{
				XMVECTOR P = XMLoadFloat3(&p.Pos);

				vMin = XMVectorMin(vMin, P);
				vMax = XMVectorMax(vMax, P);
			}

			XMStoreFloat3(&bounds.Center, 0.5f * (vMin + vMax));
			XMStoreFloat3(&bounds.Extents, 0.5f * (vMax - vMin));
		}

		SubmeshGeometry submesh;
		submesh.IndexCount = indices.size();
		submesh.StartIndexLocation = 0;
		submesh.BaseVertexLocation = 0;
		submesh.Bounds = bounds;

		geo->DrawArgs[meshName] = submesh;
		m_GeometryMesh[meshName] = std::move(geo);
	}
}

void AssertsReference::BuildSkinnedModel(ID3D12Device* pDevice, ID3D12GraphicsCommandList* pCommandList, std::string meshName)
{
	if (m_GeometryMesh.count(meshName)) {
		cout << "이미 해당메쉬는 로드되었습니다." << endl;
	}
	else
	{
		string path = "Models\\SkinnedModel\\" + meshName + "\\" + meshName;

		std::vector<SkinnedVertex> skinnedVertices;
		std::vector<std::uint32_t> indices;
		std::vector<Material> materials;
		std::unique_ptr<SkinnedData> skinnedInfo = make_unique<SkinnedData>();

		LoadMeshFile(skinnedVertices, indices, &materials, path);
		LoadSkeletonFile(*skinnedInfo, path);

		// Geo CreateDefaultBuffer
		std::unique_ptr<SkinnedModelInstance> skinnedModelInst = std::make_unique<SkinnedModelInstance>();
		skinnedModelInst->SkinnedInfo = std::move(skinnedInfo);
		skinnedModelInst->FinalTransforms.resize(skinnedModelInst->SkinnedInfo->BoneCount());
		m_SkinnedModelInsts[meshName] = std::move(skinnedModelInst);

		const UINT vbByteSize = (UINT)skinnedVertices.size() * sizeof(SkinnedVertex);
		const UINT ibByteSize = (UINT)indices.size() * sizeof(std::uint32_t);

		auto geo = std::make_unique<GeometryMesh>();
		geo->Name = meshName;

		ThrowIfFailed(D3DCreateBlob(vbByteSize, &geo->VertexBufferCPU));
		CopyMemory(geo->VertexBufferCPU->GetBufferPointer(), skinnedVertices.data(), vbByteSize);

		ThrowIfFailed(D3DCreateBlob(ibByteSize, &geo->IndexBufferCPU));
		CopyMemory(geo->IndexBufferCPU->GetBufferPointer(), indices.data(), ibByteSize);

		geo->VertexBufferGPU = d3dUtil::CreateDefaultBuffer(pDevice,
			pCommandList, skinnedVertices.data(), vbByteSize, geo->VertexBufferUploader);

		geo->IndexBufferGPU = d3dUtil::CreateDefaultBuffer(pDevice,
			pCommandList, indices.data(), ibByteSize, geo->IndexBufferUploader);

		geo->VertexByteStride = sizeof(SkinnedVertex);
		geo->VertexBufferByteSize = vbByteSize;
		geo->IndexFormat = DXGI_FORMAT_R32_UINT;
		geo->IndexBufferByteSize = ibByteSize;

		// Bounds
		XMFLOAT3 vMinf3(+MathHelper::Infinity, +MathHelper::Infinity, +MathHelper::Infinity);
		XMFLOAT3 vMaxf3(-MathHelper::Infinity, -MathHelper::Infinity, -MathHelper::Infinity);
		XMVECTOR vMin = XMLoadFloat3(&vMinf3);
		XMVECTOR vMax = XMLoadFloat3(&vMaxf3);

		for (auto& p : skinnedVertices)
		{
			XMVECTOR P = XMLoadFloat3(&p.Pos);

			vMin = XMVectorMin(vMin, P);
			vMax = XMVectorMax(vMax, P);
		}

		BoundingBox bounds;
		XMStoreFloat3(&bounds.Center, 0.5f * (vMin + vMax));
		XMStoreFloat3(&bounds.Extents, 0.5f * (vMax - vMin));

		SubmeshGeometry submesh;
		submesh.IndexCount = indices.size();
		submesh.StartIndexLocation = 0;
		submesh.BaseVertexLocation = 0;
		submesh.Bounds = bounds;
		geo->DrawArgs[meshName] = submesh;

		m_GeometryMesh[meshName] = std::move(geo);
	}
}

void AssertsReference::BuildSkinnedModelAnimation(std::string meshName, const std::string clipName)
{
	if (!m_SkinnedModelInsts.count(meshName))
	{
		cout << "LoadModelAnimation: None MeshName" << endl;
	}
	else 
	{
		string path = "Models\\SkinnedModel\\" + meshName + "\\" + meshName;
		LoadAnimationFile(*m_SkinnedModelInsts[meshName]->SkinnedInfo, path, clipName);
	}
}

void AssertsReference::BuildSkinnedModelSubMesh(std::string meshName, const std::string submeshName)
{
	if (!m_SkinnedModelInsts.count(meshName))
	{
		cout << "BuildSkinnedModelSubMesh: None MeshName" << endl;
	}
	else
	{
		auto vSubmeshOffset = m_SkinnedModelInsts[meshName]->SkinnedInfo->GetSubmeshOffset();
		auto vBoneName = m_SkinnedModelInsts[meshName]->SkinnedInfo->GetBoneName();

		// submeshName의 객체가 있는지 예외처리
		bool checkIsSubmeshName = false;
		for (auto& boneName : vBoneName)
		{
			if (boneName == submeshName) checkIsSubmeshName = true;
		}

		// submesh 추가
		if (!checkIsSubmeshName)
		{
			cout << "BuildSkinnedModelSubMesh: None submeshName" << endl;
		}
		else
		{
			// caculate submesh offset
			UINT indexCount = 0;
			UINT startIndexLocation = 0;
			for (int i = 0; i < vBoneName.size(); ++i)
			{
				startIndexLocation = vSubmeshOffset[i];
				indexCount += startIndexLocation;

				if (vBoneName[i] == submeshName) break;
			}

			SubmeshGeometry submesh;
			submesh.IndexCount = indexCount;
			submesh.StartIndexLocation = startIndexLocation;
			submesh.BaseVertexLocation = 0;

			m_GeometryMesh[meshName]->DrawArgs[submeshName] = submesh;
		}
	}
}

void AssertsReference::BuildBasicParticle(ID3D12Device* pDevice, ID3D12GraphicsCommandList* pCommandList, 
	std::string particleName, int particleCount,
	DirectX::XMFLOAT2 particlePosX, DirectX::XMFLOAT2 particlePosY, DirectX::XMFLOAT2 particlePosZ,
	DirectX::XMFLOAT2 particleSize,
	DirectX::XMFLOAT2 particleVelX, DirectX::XMFLOAT2 particleVelY, DirectX::XMFLOAT2 particleVelZ,
	DirectX::XMFLOAT2 particleStartTime, DirectX::XMFLOAT2 particleLifeTime,
	DirectX::XMFLOAT2 particlePeriod, DirectX::XMFLOAT2 particleAmplifier)
{
	// 입력받아야 할 자료들
	// world, particle name
	// particle Param: pos, size,,,, 
	// build particle verticies
	std::vector<ParticleVertex> particleVertices;
	particleVertices.resize(particleCount);

	for (int i = 0; i < particleCount; ++i)
	{
		particleVertices[i].pos = XMFLOAT3(MathHelper::RandF(particlePosX.x, particlePosX.y), MathHelper::RandF(particlePosY.x, particlePosY.y), MathHelper::RandF(particlePosZ.x, particlePosZ.y));;
		particleVertices[i].size = particleSize;
		particleVertices[i].velocity = XMFLOAT3(MathHelper::RandF(particleVelX.x, particleVelX.y), MathHelper::RandF(particleVelY.x, particleVelY.y), MathHelper::RandF(particleVelZ.x, particleVelZ.y));
		particleVertices[i].startTime = MathHelper::RandF(particleStartTime.x, particleStartTime.y);
		particleVertices[i].lifeTime = MathHelper::RandF(particleLifeTime.x, particleLifeTime.y);
		particleVertices[i].period = MathHelper::RandF(particlePeriod.x, particlePeriod.y);
		particleVertices[i].amplifier = MathHelper::RandF(particleAmplifier.x, particleAmplifier.y);
	}

	// build paritcle indicies
	std::vector<std::uint16_t> indices;
	indices.resize(particleCount);

	for (int i = 0; i < particleCount; ++i)
	{
		indices[i] = i;
	}

	AllocateParticleBuffer(pDevice, pCommandList, particleName, particleVertices, indices);
}

void AssertsReference::BuildCircleParticle(ID3D12Device* pDevice, ID3D12GraphicsCommandList* pCommandList, 
	std::string particleName, int particleCount,
	DirectX::XMFLOAT3 particlePos, DirectX::XMFLOAT2 particleSize,
	DirectX::XMFLOAT2 particleVelX, DirectX::XMFLOAT2 particleVelY, DirectX::XMFLOAT2 particleVelZ,
	DirectX::XMFLOAT2 particleStartTime, DirectX::XMFLOAT2 particleLifeTime,
	DirectX::XMFLOAT2 particlePeriod, DirectX::XMFLOAT2 particleAmplifier,
	float radius)
{
	// 입력받아야 할 자료들
	// world, particle name
	// particle Param: pos, size,,,, 
	// build particle verticies
	std::vector<ParticleVertex> particleVertices;
	particleVertices.resize(particleCount);

	for (int i = 0; i < particleCount; ++i)
	{
		float randomRadius = MathHelper::RandF(0, 1);
		XMFLOAT3 initParametricPos = XMFLOAT3(radius *sin(randomRadius * 2.0 * XM_PI), 0, radius *cos(randomRadius * 2.0 * XM_PI));
		XMFLOAT3 newPos = MathHelper::Add(particlePos, initParametricPos);

		particleVertices[i].pos = newPos;
		particleVertices[i].size = particleSize;
		particleVertices[i].velocity = XMFLOAT3(MathHelper::RandF(particleVelX.x, particleVelX.y), MathHelper::RandF(particleVelY.x, particleVelY.y), MathHelper::RandF(particleVelZ.x, particleVelZ.y));
		particleVertices[i].startTime = MathHelper::RandF(particleStartTime.x, particleStartTime.y);
		particleVertices[i].lifeTime = MathHelper::RandF(particleLifeTime.x, particleLifeTime.y);
		particleVertices[i].period = MathHelper::RandF(particlePeriod.x, particlePeriod.y);
		particleVertices[i].amplifier = MathHelper::RandF(particleAmplifier.x, particleAmplifier.y);
	}

	///////////// 파티클 메시제작 동일
	// build paritcle indicies
	std::vector<std::uint16_t> indices;
	indices.resize(particleCount);

	for (int i = 0; i < particleCount; ++i)
	{
		indices[i] = i;
	}

	AllocateParticleBuffer(pDevice, pCommandList, particleName, particleVertices, indices);
}

bool AssertsReference::LoadMeshFile(std::vector<Vertex>& outVertexVector, std::vector<uint32_t>& outIndexVector, std::vector<Material>* outMaterial, std::string path)
{
	path += ".mesh";
	std::ifstream fileIn(path);

	uint32_t vertexSize, indexSize;
	uint32_t materialSize;

	std::string ignore;
	if (fileIn)
	{
		fileIn >> ignore >> vertexSize;
		fileIn >> ignore >> indexSize;
		fileIn >> ignore >> materialSize;

		if (vertexSize == 0 || indexSize == 0)
			return false;

		// Material Data
		fileIn >> ignore;
		for (uint32_t i = 0; i < materialSize; ++i)
		{
			Material tempMaterial;

			fileIn >> ignore >> tempMaterial.Name;
			fileIn >> ignore >> tempMaterial.Ambient.x >> tempMaterial.Ambient.y >> tempMaterial.Ambient.z;
			fileIn >> ignore >> tempMaterial.DiffuseAlbedo.x >> tempMaterial.DiffuseAlbedo.y >> tempMaterial.DiffuseAlbedo.z >> tempMaterial.DiffuseAlbedo.w;
			fileIn >> ignore >> tempMaterial.FresnelR0.x >> tempMaterial.FresnelR0.y >> tempMaterial.FresnelR0.z;
			fileIn >> ignore >> tempMaterial.Specular.x >> tempMaterial.Specular.y >> tempMaterial.Specular.z;
			fileIn >> ignore >> tempMaterial.Emissive.x >> tempMaterial.Emissive.y >> tempMaterial.Emissive.z;
			fileIn >> ignore >> tempMaterial.Roughness;
			fileIn >> ignore;
			for (int i = 0; i < 4; ++i)
			{
				for (int j = 0; j < 4; ++j)
				{
					fileIn >> tempMaterial.MatTransform.m[i][j];
				}
			}
			(*outMaterial).push_back(tempMaterial);
		}

		// Vertex Data
		for (uint32_t i = 0; i < vertexSize; ++i)
		{
			Vertex vertex;
			fileIn >> ignore >> vertex.Pos.x >> vertex.Pos.y >> vertex.Pos.z;
			fileIn >> ignore >> vertex.Normal.x >> vertex.Normal.y >> vertex.Normal.z;
			fileIn >> ignore >> vertex.TexC.x >> vertex.TexC.y;
			fileIn >> ignore >> vertex.Tangent.x >> vertex.Tangent.y >> vertex.Tangent.z;
			fileIn >> ignore >> vertex.Binormal.x >> vertex.Binormal.y >> vertex.Binormal.z;

			// push_back
			outVertexVector.push_back(vertex);
		}

		// Index Data
		fileIn >> ignore;
		for (uint32_t i = 0; i < indexSize; ++i)
		{
			uint32_t index;
			fileIn >> index;
			outIndexVector.push_back(index);
		}

		return true;
	}

	return false;
}

bool AssertsReference::LoadMeshFile(std::vector<SkinnedVertex>& outVertexVector, std::vector<uint32_t>& outIndexVector, std::vector<Material>* outMaterial, std::string path)
{
	path += ".mesh";
	std::ifstream fileIn(path);

	uint32_t vertexSize, indexSize;
	uint32_t materialSize;

	std::string ignore;
	if (fileIn)
	{
		fileIn >> ignore >> vertexSize;
		fileIn >> ignore >> indexSize;
		fileIn >> ignore >> materialSize;

		if (vertexSize == 0 || indexSize == 0)
			return false;

		if (outMaterial != nullptr)
		{
			// Material Data
			fileIn >> ignore;
			for (uint32_t i = 0; i < materialSize; ++i)
			{
				Material tempMaterial;

				fileIn >> ignore >> tempMaterial.Name;
				fileIn >> ignore >> tempMaterial.Ambient.x >> tempMaterial.Ambient.y >> tempMaterial.Ambient.z;
				fileIn >> ignore >> tempMaterial.DiffuseAlbedo.x >> tempMaterial.DiffuseAlbedo.y >> tempMaterial.DiffuseAlbedo.z >> tempMaterial.DiffuseAlbedo.w;
				fileIn >> ignore >> tempMaterial.FresnelR0.x >> tempMaterial.FresnelR0.y >> tempMaterial.FresnelR0.z;
				fileIn >> ignore >> tempMaterial.Specular.x >> tempMaterial.Specular.y >> tempMaterial.Specular.z;
				fileIn >> ignore >> tempMaterial.Emissive.x >> tempMaterial.Emissive.y >> tempMaterial.Emissive.z;
				fileIn >> ignore >> tempMaterial.Roughness;
				fileIn >> ignore;
				for (int i = 0; i < 4; ++i)
				{
					for (int j = 0; j < 4; ++j)
					{
						fileIn >> tempMaterial.MatTransform.m[i][j];
					}
				}
				(*outMaterial).push_back(tempMaterial);
			}
		}

		// Vertex Data
		for (uint32_t i = 0; i < vertexSize; ++i)
		{
			SkinnedVertex vertex;
			int temp[4];
			fileIn >> ignore >> vertex.Pos.x >> vertex.Pos.y >> vertex.Pos.z;
			fileIn >> ignore >> vertex.Normal.x >> vertex.Normal.y >> vertex.Normal.z;
			fileIn >> ignore >> vertex.TexC.x >> vertex.TexC.y;
			fileIn >> ignore >> vertex.Tangent.x >> vertex.Tangent.y >> vertex.Tangent.z;
			fileIn >> ignore >> vertex.Binormal.x >> vertex.Binormal.y >> vertex.Binormal.z;
			fileIn >> ignore >> vertex.BoneWeights.x >> vertex.BoneWeights.y >> vertex.BoneWeights.z;
			fileIn >> ignore >> temp[0] >> temp[1] >> temp[2] >> temp[3];

			for (int j = 0; j < 4; ++j)
			{
				vertex.BoneIndices[j] = temp[j];
			}
			// push_back
			outVertexVector.push_back(vertex);
		}

		// Index Data
		fileIn >> ignore;
		for (uint32_t i = 0; i < indexSize; ++i)
		{
			uint32_t index;
			fileIn >> index;
			outIndexVector.push_back(index);
		}

		return true;
	}

	return false;
}

bool AssertsReference::LoadSkeletonFile(SkinnedData& outSkinnedData, std::string path)
{
	path += ".skeleton";
	std::ifstream fileIn(path);

	uint32_t boneSize;

	std::string ignore;
	if (fileIn)
	{
		fileIn >> ignore >> boneSize;

		if (boneSize == 0)
			return false;

		// Bone Data
		// Bone Hierarchy
		fileIn >> ignore;
		std::vector<int> boneHierarchy;
		for (uint32_t i = 0; i < boneSize; ++i)
		{
			int tempBoneHierarchy;
			fileIn >> tempBoneHierarchy;
			boneHierarchy.push_back(tempBoneHierarchy);
		}

		fileIn >> ignore;
		for (uint32_t i = 0; i < boneSize; ++i)
		{
			std::string tempBoneName;
			fileIn >> tempBoneName;

			outSkinnedData.SetBoneName(tempBoneName);
		}
		// Bone Offset
		fileIn >> ignore;
		std::vector<DirectX::XMFLOAT4X4> boneOffsets;
		for (uint32_t i = 0; i < boneSize; ++i)
		{
			DirectX::XMFLOAT4X4 tempBoneOffset;
			for (int i = 0; i < 4; ++i)
			{
				for (int j = 0; j < 4; ++j)
				{
					fileIn >> tempBoneOffset.m[i][j];
				}
			}
			boneOffsets.push_back(tempBoneOffset);
		}
		// Bone Submesh Offset
		fileIn >> ignore;
		std::vector<int> boneSubmeshOffset;
		for (uint32_t i = 0; i < boneSize; ++i)
		{
			int tempBoneSubmeshOffset;
			fileIn >> tempBoneSubmeshOffset;
			outSkinnedData.SetSubmeshOffset(tempBoneSubmeshOffset);
		}

		outSkinnedData.Set(
			boneHierarchy,
			boneOffsets);

		return true;
	}

	return false;
}

bool AssertsReference::LoadAnimationFile(SkinnedData& outSkinnedData, std::string& path, const std::string clipName)
{
	path = path + "_" + clipName + ".anim";
	std::ifstream fileIn(path);

	AnimationClip animation;
	uint32_t boneAnimationSize, keyframeSize;

	std::string ignore;
	if (fileIn)
	{
		fileIn >> ignore >> boneAnimationSize;
		fileIn >> ignore >> keyframeSize;

		for (uint32_t i = 0; i < boneAnimationSize; ++i)
		{
			BoneAnimation boneAnim;
			for (uint32_t j = 0; j < keyframeSize; ++j)
			{
				Keyframe key;
				fileIn >> key.TimePos;
				fileIn >> key.Translation.x >> key.Translation.y >> key.Translation.z;
				fileIn >> key.Scale.x >> key.Scale.y >> key.Scale.z;
				fileIn >> key.RotationQuat.x >> key.RotationQuat.y >> key.RotationQuat.z >> key.RotationQuat.w;
				boneAnim.Keyframes.push_back(key);
			}
			animation.BoneAnimations.push_back(boneAnim);
		}

		outSkinnedData.SetAnimation(animation, clipName);
		return true;
	}

	return false;
}

void AssertsReference::AllocateParticleBuffer(ID3D12Device* pDevice, ID3D12GraphicsCommandList* pCommandList, std::string particleName, std::vector<ParticleVertex>& particleVertices, std::vector< uint16_t>& indices)
{
	const UINT vbByteSize = (UINT)particleVertices.size() * sizeof(ParticleVertex);
	const UINT ibByteSize = (UINT)indices.size() * sizeof(std::uint16_t);

	auto geo = std::make_unique<GeometryMesh>();
	geo->Name = particleName;

	// 파티클 메시(정점, 인덱스 버퍼) 리소스 할당
	ThrowIfFailed(D3DCreateBlob(vbByteSize, &geo->VertexBufferCPU));
	CopyMemory(geo->VertexBufferCPU->GetBufferPointer(), particleVertices.data(), vbByteSize);

	ThrowIfFailed(D3DCreateBlob(ibByteSize, &geo->IndexBufferCPU));
	CopyMemory(geo->IndexBufferCPU->GetBufferPointer(), indices.data(), ibByteSize);

	geo->VertexBufferGPU = d3dUtil::CreateDefaultBuffer(pDevice,
		pCommandList, particleVertices.data(), vbByteSize, geo->VertexBufferUploader);

	geo->IndexBufferGPU = d3dUtil::CreateDefaultBuffer(pDevice,
		pCommandList, indices.data(), ibByteSize, geo->IndexBufferUploader);

	geo->VertexByteStride = sizeof(ParticleVertex);
	geo->VertexBufferByteSize = vbByteSize;
	geo->IndexFormat = DXGI_FORMAT_R16_UINT;
	geo->IndexBufferByteSize = ibByteSize;

	SubmeshGeometry submesh;
	submesh.IndexCount = (UINT)indices.size();
	submesh.StartIndexLocation = 0;
	submesh.BaseVertexLocation = 0;

	geo->DrawArgs[particleName] = submesh;

	m_GeometryMesh[particleName] = std::move(geo);

	// 멤버변수 메모리 해제
	particleVertices.clear();
	indices.clear();
}
