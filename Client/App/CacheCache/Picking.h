#pragma once
#include "Camera.h"

// 추가할점
// z값 범위 조절

namespace Core
{
	extern int g_DisplayWidth;
	extern int g_DisplayHeight;
}

// 서버한테 보낼 정보 인데 필요 없는 값은 지워도 됩니다!
struct PickedObject
{
	// + UI Info
	float distance = 0.f;
	string objectName;	// type
	string instName;	// instName
	int serverMeshID;	// 같은 오브젝트면 같은 값
	int instanceID;		// 같은 오브젝트들 중에 몇번째인지

	bool isChangeable;
	UINT materialIndex;

	int zLayer = -1;
};

namespace Picking
{
	PickedObject RayIntersect(int screenX, int screenY, string MapName);
	PickedObject RayIntersect2D(int screenX, int screenY);
	PickedObject RayIntersect2D(int screenX, int screenY, std::string sceneName, std::string layer = OBJECT_TYPE_UI2D);
	PickedObject RayIntersect2DZLayer(int screenX, int screenY, std::string sceneName, std::string layer = OBJECT_TYPE_UI2D);

	PickedObject RayIntersectContour(int screenX, int screenY, string MapName);

	void RayAtViewSpace(XMVECTOR& original, XMVECTOR& direction, int screenX, int screenY);
}
