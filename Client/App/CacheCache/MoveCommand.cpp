#include "pch.h"
#include "MoveCommand.h"
#include "Character.h"

MoveCommand::MoveCommand(Character* owner) :
	ICommand(owner)
{
}

void MoveCommand::Idle()
{
	m_Owner->SetAnimationKeyState(AnimationController::PlayerState::STATE_IDLE);
}

void MoveCommand::Forward()
{
	m_Owner->SetAnimationKeyState(AnimationController::PlayerState::STATE_FORWARD);
}

void MoveCommand::Backward()
{
	switch (m_Owner->m_PlayerRole)
	{
	case ROLE_MASTER:
		m_Owner->SetAnimationKeyState(AnimationController::PlayerState::STATE_BACKWARD);
		break;
	case ROLE_STUDENT:
		m_Owner->SetAnimationKeyState(AnimationController::PlayerState::STATE_FORWARD);
		break;
	};
}

void MoveCommand::LeftStrafe()
{
	switch (m_Owner->m_PlayerRole)
	{
	case ROLE_MASTER:
		m_Owner->SetAnimationKeyState(AnimationController::PlayerState::STATE_LEFT_STRAFE);
		break;
	case ROLE_STUDENT:
		m_Owner->SetAnimationKeyState(AnimationController::PlayerState::STATE_FORWARD);
		break;
	};
}

void MoveCommand::RightStrafe()
{
	switch (m_Owner->m_PlayerRole)
	{
	case ROLE_MASTER:
		m_Owner->SetAnimationKeyState(AnimationController::PlayerState::STATE_RIGHT_STRAFE);
		break;
	case ROLE_STUDENT:
		m_Owner->SetAnimationKeyState(AnimationController::PlayerState::STATE_FORWARD);
		break;
	};
}

void MoveCommand::Attack()
{
	// Animation
	m_Owner->SetAnimationKeyState(AnimationController::PlayerState::STATE_ATTACK);
	// 공격을 한순간부터 쿨타임 시간시작
}

void MoveCommand::Jump()
{
	m_Owner->SetAnimationKeyState(AnimationController::PlayerState::STATE_JUMP);
}
