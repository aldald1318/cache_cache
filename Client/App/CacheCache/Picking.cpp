#include "pch.h"
#include "Picking.h"
#include "ApplicationContext.h"
#include "CACHE_CACHE.h"

#include "Map.h"
#include "GeometryMesh.h"

PickedObject Picking::RayIntersect(int screenX, int screenY, string MapName)
{
	PickedObject pickedObject;

	Map* originMap = AppContext->m_Maps[MapName];
	if (!originMap)
	{
		std::cout << "맵이름: " << MapName << endl << "오류! 맵이름이 잘못되었습니다." << endl;
		return {};
	}

	XMVECTOR original;
	XMVECTOR direction;

	// 프롭 체크
	for (auto& p : originMap->mapInfoVector)
	{
		GameObject* obj = AppContext->FindObject<GameObject>(p.meshName, std::to_string(p.typeID));	// proptype, typeid
		//if (obj->m_IsChangeable)
		{
			RayAtViewSpace(original, direction, screenX, screenY);

			XMFLOAT4X4 matView = CACHE_CACHE::GetApp()->m_Camera->GetView4x4f();
			XMMATRIX matInvView = XMMatrixInverse(&XMMatrixDeterminant(DirectX::XMLoadFloat4x4(&matView)), DirectX::XMLoadFloat4x4(&matView));

			XMMATRIX W = DirectX::XMLoadFloat4x4(&obj->m_World);
			XMMATRIX invWorld = XMMatrixInverse(&XMMatrixDeterminant(W), W);

			// Tranform ray to vi space of Mesh.
			XMMATRIX toLocal = XMMatrixMultiply(matInvView, invWorld);

			original = XMVector3TransformCoord(original, toLocal);
			direction = XMVector3TransformNormal(direction, toLocal);

			// Make the ray direction unit length for the intersection tests.
			direction = XMVector3Normalize(direction);

			float tmin = 0.0f;
			if (obj->m_Geo->DrawArgs[p.meshName].Bounds.Intersects(original, direction, tmin) && tmin > 0.f)
			{
				if (pickedObject.distance == 0.f)
				{
					pickedObject.distance = tmin;
					pickedObject.objectName = obj->GetMeshName();
					pickedObject.instName = obj->GetInstID();
					pickedObject.serverMeshID = obj->GetServerMeshID();
					pickedObject.instanceID = obj->GetIndex();
					pickedObject.materialIndex = obj->m_MaterialIndex;
					pickedObject.isChangeable = obj->m_IsChangeable;
				}
				else
				{
					if (pickedObject.distance > tmin)
					{
						pickedObject.distance = tmin;
						pickedObject.objectName = obj->GetMeshName();
						pickedObject.instName = obj->GetInstID();
						pickedObject.serverMeshID = obj->GetServerMeshID();
						pickedObject.instanceID = obj->GetIndex();
						pickedObject.materialIndex = obj->m_MaterialIndex;
						pickedObject.isChangeable = obj->m_IsChangeable;
					}
				}
			}
		}
	}

	// ui 체크
	for (auto& p : originMap->uiInfoVector)
	{
		GameObject* obj = AppContext->FindObject<GameObject>(p.meshName, p.uiName);	// proptype, typeid
		//if (obj->m_IsChangeable)
		{
			RayAtViewSpace(original, direction, screenX, screenY);

			XMFLOAT4X4 matView = CACHE_CACHE::GetApp()->m_Camera->GetView4x4f();
			XMMATRIX matInvView = XMMatrixInverse(&XMMatrixDeterminant(DirectX::XMLoadFloat4x4(&matView)), DirectX::XMLoadFloat4x4(&matView));

			XMMATRIX W = DirectX::XMLoadFloat4x4(&obj->m_World);
			XMMATRIX invWorld = XMMatrixInverse(&XMMatrixDeterminant(W), W);

			// Tranform ray to vi space of Mesh.
			XMMATRIX toLocal = XMMatrixMultiply(matInvView, invWorld);

			original = XMVector3TransformCoord(original, toLocal);
			direction = XMVector3TransformNormal(direction, toLocal);

			// Make the ray direction unit length for the intersection tests.
			direction = XMVector3Normalize(direction);

			float tmin = 0.0f;
			if (obj->m_Geo->DrawArgs[p.meshName].Bounds.Intersects(original, direction, tmin) && tmin > 0.f)
			{
				if (pickedObject.distance == 0.f)
				{
					pickedObject.distance = tmin;
					pickedObject.objectName = obj->GetMeshName();
					pickedObject.instName = obj->GetInstID();
					pickedObject.instanceID = obj->GetIndex();
					pickedObject.materialIndex = obj->m_MaterialIndex;
					pickedObject.isChangeable = obj->m_IsChangeable;
				}
				else
				{
					if (pickedObject.distance > tmin)
					{
						pickedObject.distance = tmin;
						pickedObject.objectName = obj->GetMeshName();
						pickedObject.instName = obj->GetInstID();
						pickedObject.instanceID = obj->GetIndex();
						pickedObject.materialIndex = obj->m_MaterialIndex;
						pickedObject.isChangeable = obj->m_IsChangeable;
					}
				}
			}
		}
	}

	//cout << "Collision " << pickedObject.objectName << ", " << pickedObject.instName << endl;
	//cout << "dist: " << pickedObject.distance << endl;
	//cout << "serverMeshID: " << pickedObject.serverMeshID << endl;
	//cout << "Active: " << pickedObject.isChangeable << endl;
	// cout << "================" << endl;

	if (pickedObject.isChangeable)
		return pickedObject;
	return {};
}

PickedObject Picking::RayIntersectContour(int screenX, int screenY, string MapName)
{
	PickedObject pickedObject;
	pickedObject.distance = INFINITE;

	Map* originMap = AppContext->m_Maps[MapName];
	if (!originMap)
	{
		std::cout << "맵이름: " << MapName << std::endl << "오류! 맵이름이 잘못되었습니다." << std::endl;
		return {};
	}

	XMVECTOR original;
	XMVECTOR direction;

	// 프롭 체크
	for (auto& p : originMap->mapInfoVector)
	{
		GameObject* obj = AppContext->FindObject<GameObject>(p.meshName, std::to_string(p.typeID));	// proptype, typeid
		if (obj->m_IsChangeable)
		{
			RayAtViewSpace(original, direction, screenX, screenY);

			XMFLOAT4X4 matView = CACHE_CACHE::GetApp()->m_Camera->GetView4x4f();
			XMMATRIX matInvView = XMMatrixInverse(&XMMatrixDeterminant(DirectX::XMLoadFloat4x4(&matView)), DirectX::XMLoadFloat4x4(&matView));

			XMMATRIX W = DirectX::XMLoadFloat4x4(&obj->m_World);
			XMMATRIX invWorld = XMMatrixInverse(&XMMatrixDeterminant(W), W);

			// Tranform ray to vi space of Mesh.
			XMMATRIX toLocal = XMMatrixMultiply(matInvView, invWorld);

			original = XMVector3TransformCoord(original, toLocal);
			direction = XMVector3TransformNormal(direction, toLocal);

			// Make the ray direction unit length for the intersection tests.
			direction = XMVector3Normalize(direction);

			float tmin = 0.f;
			if (obj->m_Geo->DrawArgs[p.meshName].Bounds.Intersects(original, direction, tmin) && tmin > 0.f)
			{
				if (pickedObject.distance >= tmin)
				{
					pickedObject.distance = tmin;
					pickedObject.objectName = obj->GetMeshName();
					pickedObject.instName = obj->GetInstID();
					pickedObject.serverMeshID = obj->GetServerMeshID();
					pickedObject.instanceID = obj->GetIndex();
				}
			}
		}
	}


	if (pickedObject.isChangeable)
		return pickedObject;
	return {};
}

PickedObject Picking::RayIntersect2D(int screenX, int screenY)
{
	const std::map<std::string, UINT>& info = AppContext->m_RItemsMap[OBJECT_TYPE_UI2D]->GetInstanceKeyMap();

	if (info.size() < 1) return {};

	PickedObject pickedObject;

	XMFLOAT3 scale{};
	XMFLOAT3 posLB{};
	XMFLOAT3 posRT{};


	for (auto& p : info)
	{
		auto ri = AppContext->m_RItemsVec[p.second];

		if (ri->m_IsVisible && ri->m_IsChangeable) // pickedObject.isChangeable)
		{
			scale = { ri->m_World._11 , ri->m_World._22, ri->m_World._33 };
			posLB = { ri->m_World._41 - ((scale.x / 20.f)), ri->m_World._42 + ((scale.y / 20.f)), ri->m_World._43 };
			posRT = { ri->m_World._41 + ((scale.x / 20.f)), ri->m_World._42 - ((scale.y / 20.f)) , ri->m_World._43 };

			posLB = { Core::g_DisplayWidth / 2.f + posLB.x,  -posLB.y + Core::g_DisplayHeight / 2.f, posLB.z };
			posRT = { Core::g_DisplayWidth / 2.f + posRT.x,  -posRT.y + Core::g_DisplayHeight / 2.f, posRT.z };

			if (screenX < posLB.x)
				continue;
			if (screenX > posRT.x)
				continue;
			if (screenY > posRT.y)
				continue;
			if (screenY < posLB.y)
				continue;

			pickedObject.distance = 0.0;
			pickedObject.instanceID = ri->GetIndex();
			pickedObject.objectName = ri->GetMeshName();
			pickedObject.instName = ri->GetInstID();
			pickedObject.materialIndex = ri->m_MaterialIndex;
			pickedObject.isChangeable = ri->m_IsChangeable;
			pickedObject.instName = ri->GetInstID();

			return pickedObject;
		}
	}

	return {};
}

PickedObject Picking::RayIntersect2D(int screenX, int screenY, std::string sceneName, std::string layer)
{
	const std::map<std::string, UINT>& info = AppContext->m_RItemsMap[layer + sceneName]->GetInstanceKeyMap();

	if (info.size() < 1) return {};

	PickedObject pickedObject;

	XMFLOAT3 scale{};
	XMFLOAT3 posLB{};
	XMFLOAT3 posRT{};


	for (auto& p : info)
	{
		auto ri = AppContext->m_RItemsVec[p.second];

		if (ri->m_IsVisible && ri->m_IsChangeable) // pickedObject.isChangeable)
		{
			scale = { ri->m_World._11 , ri->m_World._22, ri->m_World._33 };
			posLB = { ri->m_World._41 - ((scale.x / 20.f)), ri->m_World._42 + ((scale.y / 20.f)), ri->m_World._43 };
			posRT = { ri->m_World._41 + ((scale.x / 20.f)), ri->m_World._42 - ((scale.y / 20.f)) , ri->m_World._43 };

			posLB = { Core::g_DisplayWidth / 2.f + posLB.x,  -posLB.y + Core::g_DisplayHeight / 2.f, posLB.z };
			posRT = { Core::g_DisplayWidth / 2.f + posRT.x,  -posRT.y + Core::g_DisplayHeight / 2.f, posRT.z };
			// posLB = { Core::g_DisplayWidth / 2.f + posLB.x * Core::g_DisplayWidth / 2.f,  -posLB.y * Core::g_DisplayHeight / 2.f + Core::g_DisplayHeight / 2.f, posLB.z };
			// posRT = { Core::g_DisplayWidth / 2.f + posRT.x * Core::g_DisplayWidth / 2.f,  -posRT.y * Core::g_DisplayHeight / 2.f + Core::g_DisplayHeight / 2.f, posRT.z };

			if (screenX < posLB.x)
				continue;
			if (screenX > posRT.x)
				continue;
			if (screenY > posRT.y)
				continue;
			if (screenY < posLB.y)
				continue;


			pickedObject.distance = 0.0;
			pickedObject.instanceID = ri->GetIndex();
			pickedObject.objectName = ri->GetMeshName();
			pickedObject.instName = ri->GetInstID();
			pickedObject.materialIndex = ri->m_MaterialIndex;
			pickedObject.isChangeable = ri->m_IsChangeable;

			return pickedObject;
		}
	}

	return {};
}

PickedObject Picking::RayIntersect2DZLayer(int screenX, int screenY, std::string sceneName, std::string layer)
{
	const std::map<std::string, UINT>& info = AppContext->m_RItemsMap[layer + sceneName]->GetInstanceKeyMap();

	if (info.size() < 1) return {};

	PickedObject pickedObject;


	for (auto& p : info)
	{
		auto ri = AppContext->m_RItemsVec[p.second];
		if (ri->m_ZLayer == UI_LAYER_0)
		{
			pickedObject = RayIntersect2D(screenX, screenY, sceneName, layer);
		}
	}

	if (pickedObject.objectName != "")
		return pickedObject;

	for (auto& p : info)
	{
		auto ri = AppContext->m_RItemsVec[p.second];
		if (ri->m_ZLayer == UI_LAYER_1)
		{
			pickedObject = RayIntersect2D(screenX, screenY, sceneName, layer);
		}
	}

	if (pickedObject.objectName != "")
		return pickedObject;

	for (auto& p : info)
	{
		auto ri = AppContext->m_RItemsVec[p.second];
		if (ri->m_ZLayer == -1)
		{
			pickedObject = RayIntersect2D(screenX, screenY, sceneName, layer);
		}
	}

	if (pickedObject.objectName != "")
		return pickedObject;


	return {};
}

void Picking::RayAtViewSpace(XMVECTOR& original, XMVECTOR& direction, int screenX, int screenY)
{
	if (!CACHE_CACHE::GetApp()->m_Camera)
		return;

	XMFLOAT4X4 matProjection = CACHE_CACHE::GetApp()->m_Camera->GetProj4x4f();

	float vx = (+2.0f * screenX / Core::g_DisplayWidth - 1.0f) / matProjection._11;
	float vy = (-2.0f * screenY / Core::g_DisplayHeight + 1.0f) / matProjection._22;

	original = XMVectorSet(0.0f, 0.0f, 0.0f, 1.0f);
	direction = XMVectorSet(vx, vy, 1.0f, 0.0f);
}


