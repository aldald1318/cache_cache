#pragma once
#include "Singleton.h"
#include "GameObject.h"
#include "ObjectInfo.h"

namespace Core {
	extern int g_DisplayWidth;
	extern int g_DisplayHeight;
}

class Map;
class Character;
class ApplicationContext : public TemplateSingleton<ApplicationContext>
{
public:
	template <class TObject>
	TObject* CreateObject(std::string type, std::string instID)
	{
		// 꼭 해야할것들
		// 자신의 Index가 정해짐
		GameObject* obj = new TObject(type, instID);
		m_RItemsVec.push_back(obj);
		
		// Map 제작
		// Map 중복체크
		if (m_RItemsMap.find(type) != m_RItemsMap.end())
		{
			//cout << "Create Instance!" << endl;
			// Instancemap 조정
			m_RItemsMap[type]->AddInstance(instID, obj->GetIndex());
		}
		else
		{
			// 새로운 타입(메쉬)이 추가됨
			// -> ObjInfo 세팅
			ObjectInfo* objInfo = new ObjectInfo(instID, obj->GetIndex());
			objInfo->m_Type = type;
			m_RItemsMap[type] = objInfo;
		}

		return dynamic_cast<TObject*>(obj);
	}

	template <class TObject>
	TObject* FindObject(std::string type, std::string instID)
	{
		if (!m_RItemsMap.count(type)) {
			cout << "Error! None Type" << endl;
			return nullptr;
		}

		if (m_RItemsMap[type]->GetInstanceIndex(instID) == -1)
			return nullptr;
		return dynamic_cast<TObject*>(m_RItemsVec[m_RItemsMap[type]->GetInstanceIndex(instID)]);
	}

public:
	std::string FindMapName(int mapCode) const;
	int			FindAnimName(int playerRole, int animCode);
	XMFLOAT3	FindSpawnLocation(std::string mapName, int spawnLocation);
	std::string FindClientInstanceID(std::string mapName, std::string meshName);
	// UI를 선택했을때 텍스쳐 변경이 있을경우(Pressed / Disable)
	void		SetUITexture(std::string uiType, std::string meshName, std::string uiName, std::string uiTextureName, std::string uiPressedTextureName = "", std::string uiDisabledTextureName = "", std::string uiSelectedTextureName = "");

	void CreateSkycube(std::string skycubeName, std::string instID, std::string matName);
	void CreateProps(std::string mapName);
	void CreateContourProps(std::string mapName);
	void CreateCharacter(std::string meshName, std::string instID, std::string matName, int skinnedCBIndex /*Character 종류(역할)*/);
	void CreateUI3D(std::string mapName);
	void CreateUI2D(std::string ui2dLayer, std::string ui2dName,int matIndex, int uiPressedTextureIdx = -1, int uiDisabledTextureIdx = -1);
	void CreateThunderBolt();
	void CreateMagicCircle(std::string instName);
	void CreateWeapon(std::string weaponName, std::string subWeaponName);
	void CreateParticle(std::string particleName, std::string instID, std::string matName, bool isLoop, DirectX::XMFLOAT3 offset, float particlePlaySpeed = 1.f);

	void DisplayProps(std::string mapName, bool isScale = false, float scaleValue = 100.f);
	void HiddenProps(std::string mapName);

	void DisplayCharacter(std::string mapName, Character* user,int spawnLocation, bool isVisible = true);
	void DisplayCharacter(std::string mapName, std::string userName, int spawnLocation, bool isVisible = true);
	void HiddenCharacter(Character* user);
	void HiddenCharacter(std::string userName);

	void DisplayUI(std::string mapName);
	void HiddenUI(std::string mapName);

	void DisplayUI2D(std::string ui2dLayer, std::string ui2dName, XMFLOAT2 pos, XMFLOAT2 size, bool isChangable, AnchorType anchorType = AnchorType::NONE, int zLayer = -1, bool isText = false);
	void HiddenUI2D(std::string ui2dLayer, std::string ui2dName);
	void SetDisplayUI2D(std::string ui2dLayer, std::string ui2dName, bool isVisible);
	void SetPickingUI2D(std::string ui2dLayer, std::string ui2dName, bool isVisible);

	void DisplayMagicCircle(std::string instID, XMFLOAT3 pos);
	void DisplayMagicCircle(std::string instID, std::string mapName, int spawnLocation, bool isMe = false);
	void HiddenMagicCircle(std::string instID);

	void DisplayThunderBolt(int instID, float posX, float posY, float posZ);
	void HiddenThunderBolt(int instID, bool isVisible = false);

	void DisplayParticle(std::string particleName, std::string instID, DirectX::XMFLOAT3 pos, bool isVisible = false, bool isCapture = false);
	void HiddenParticle(std::string particleName, std::string instID);

public:
	void CreateDebugBoundingBox(std::string boundsName, std::string boundsInstName);

public:
	std::map<std::string, ObjectInfo*> m_RItemsMap;
	std::vector<GameObject*> m_RItemsVec;
	std::map<std::string, Map*> m_Maps;

};

