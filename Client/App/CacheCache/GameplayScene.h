#pragma once
#include "Scene.h"

namespace Graphics
{
	extern Microsoft::WRL::ComPtr<ID3D12PipelineState> g_OpaquePSO;
	extern Microsoft::WRL::ComPtr<ID3D12PipelineState> g_SkinnedPSO;
	extern Microsoft::WRL::ComPtr<ID3D12PipelineState> g_ContourPSO;
	extern Microsoft::WRL::ComPtr<ID3D12PipelineState> g_SkyPSO;
	extern Microsoft::WRL::ComPtr<ID3D12PipelineState> g_UIPSO;
	extern Microsoft::WRL::ComPtr<ID3D12PipelineState> g_HPPSO;
	extern Microsoft::WRL::ComPtr<ID3D12PipelineState> g_ShadowOpaquePSO;
	extern Microsoft::WRL::ComPtr<ID3D12PipelineState> g_SkinnedShadowOpaquePSO;
	extern Microsoft::WRL::ComPtr<ID3D12PipelineState> g_ParticlePSO;
}

namespace InputHandler
{
	extern bool g_CursorSwitch;
}

struct BoardInfo
{
	DirectX::XMFLOAT2 pos;
	wstring id;
};

class Character;
class GameplayScene : public Scene
{
	friend GameplayController;
private:
	virtual void ProcessEvent(int sEvent, int argsCount = 0, ...) override;

public:
	virtual void Initialize() override;
	virtual void OnResize() override;

public:
	virtual bool Enter() override;
	virtual void Exit() override;

	virtual void Update(const float& fDeltaTime) override;
	virtual void Render() override;
	virtual void RenderUI() override;
	virtual void WriteShadow() override;

	void ThreadEventHandler();

private:
	std::string ConvertCharacterNameToPlayboardImageName(std::string chrName);
	void SetUI2DProfilePicture(std::string ui2dName, std::string ui2dInstId, std::string playerName);
	void ChangeCameraNextPlayer();
	void ChangeFreeCamera();
	void DiePlayer();

private:
	/* ID: battleID */
	std::map<int, Character*> m_Users;
	int m_PlayerID;
	// 플레이어 서버 배틀클라이언트 아이디들
	std::vector<int> m_PlayerIDs;
	std::vector<std::string> m_PlayerNames;
	int m_IndexSaveForPlayerID = 1;

	/* 기타 이벤트 변수들 */
	std::string m_MapName;
	int			m_Timer;
	short		m_PenaltyDuration;
	
	// PlayerBoard 파라미터
	bool					m_IsTab = false;
	std::vector<BoardInfo>	m_PlayerBoardInfo;

	/* 내 캐릭터가 살아있는지 확인 */
	// 내 캐릭터가 죽었을시 씬컨트롤러 활성화를 위한 변수
	bool		m_IsArriveMyPlayer = true;
	int			m_IndexChangeOtherPlayerCamera = 0;

private:
	/* 쓰레드 이벤트동기화 변수들 */
	bool m_IsPenartyAlram = false;
	bool m_IsPenarty = false;
	bool m_IsGameOver = false;
	
	bool m_IsPlayerDieEventHandler = false;
	std::queue<int> m_DiedPlayerIDEventParams;

private:
	bool m_DebugNoUI = false;
};

