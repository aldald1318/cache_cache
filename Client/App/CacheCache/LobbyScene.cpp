#include "pch.h"
#include "LobbyScene.h"
#include "Service.h"
#include "CACHE_CACHE.h"
#include "CommandContext.h"
#include "InputHandler.h"
#include "AssertsReference.h"
#include "GameTimer.h"
#include "ApplicationContext.h"
#include "Character.h"
#include "UserInterface.h"
#include "Map.h"
#include "GameObject.h"
#include "ObjectInfo.h"

#define WIDTH_NORMALIZE_UI(x) (x + (Core::g_DisplayWidth / 2.f))
#define HEIGHT_NORMALIZE_UI(y) (-y + (Core::g_DisplayHeight / 2.f))

void LobbyScene::ProcessEvent(int sEvent, int argsCount, ...)
{
	va_list ap;
	string id_str;
	switch (sEvent)
	{
	case EVENT_LOBBY_AUTOMATCH_CANCEL_SUCCESS:
	{
		m_IsAutoMatching = false;
		UserInterface* autoMatching = AppContext->FindObject<UserInterface>(OBJECT_TYPE_UI2D + m_SceneName, OBJECT_NAME_RENEWAL_ARROW);
		autoMatching->InitSpriteAnimation();
		break;
	}
	case EVENT_LOBBY_MANUALMATCH_ROOM_LIST:
		Service::GetApp()->GetRoomList(m_RoomList);
		m_IsGetRoomList = true;

		UpdateRoomList();
		break;

	case EVENT_LOBBY_MANUALMATCH_ROOM_JOIN_ALLOW:
		SceneManager::GetApp()->ChangeScene();
		break;

	case EVENT_LOBBY_MANUALMATCH_ROOM_JOIN_DENY:
		int deny_code;
		// 2020.07.21 박태준: 여기서 UI 메시지 보여주기? 여기로 보내도 되는건지 모르겠음.
		va_start(ap, argsCount);
		deny_code = va_arg(ap, int);
		va_end(ap);

		switch (deny_code) {
		case ROOM_DENY_CODE_ROOM_DOSENT_EXIST:
			m_IsJoinDenyWin = true;
			m_JoinDenyWarning = L"This room does not exist :(";
			break;
		case ROOM_DENY_CODE_ROOM_HAS_STARTED:
			m_IsJoinDenyWin = true;
			m_JoinDenyWarning = L"This room has already started :(";
			break;
		case ROOM_DENY_CODE_ROOM_IS_FULL:
			m_IsJoinDenyWin = true;
			m_JoinDenyWarning = L"This room is full :0";
			break;
		default:
			cout << "ERROR: WRONG ROOM JOIN DENY CODE!!" << endl;
			break;
		}
		break;

	case EVENT_LOBBY_UPDATE_USERINFO:
	{
		va_start(ap, argsCount);
		id_str = va_arg(ap, string);
		m_MMRInfo.mmr = va_arg(ap, int);
		m_MMRInfo.catched_student_count = va_arg(ap, int);
		m_MMRInfo.total_game_count = va_arg(ap, int);
		m_MMRInfo.winning_rate = va_arg(ap, double);
		va_end(ap);

		WCHAR wc[256];
		int nLen = MultiByteToWideChar(CP_ACP, 0, id_str.c_str(), sizeof(char) * 10, NULL, NULL);
		MultiByteToWideChar(CP_ACP, 0, id_str.c_str(), sizeof(char) * 10, wc, nLen);
		m_MMRInfo.name = wc;
		break;
	}
	}
}

void LobbyScene::UIEvent(int sEvent, int argsCount, ...)
{
	va_list ap;
	switch (sEvent)
	{
	case CLIENT_EVENT_LOBBY_UI_PRESSED:
	{
		std::string uiName;
		bool onoff;
		string layer;
		va_start(ap, argsCount);
		uiName = va_arg(ap, std::string);
		onoff = va_arg(ap, bool);
		layer = va_arg(ap, string);
		va_end(ap);

		UpdateRoomList();
		AppContext->FindObject<UserInterface>(layer + m_SceneName, uiName)->Overlap(onoff);
		break;
	}
	case CLIENT_EVENT_LOBBY_UI_ROOMLIST_PAGE_PREV:
	{
		bool onoff;
		va_start(ap, argsCount);
		onoff = va_arg(ap, bool);
		va_end(ap);
		AppContext->FindObject<UserInterface>(OBJECT_TYPE_UI2D_ROOMLIST + m_SceneName, OBJECT_NAME_LOBBY_ROOMLIST_PREV)->OnActive(onoff);
		break;
	}
	case CLIENT_EVENT_LOBBY_UI_ROOMLIST_PAGE_NEXT:
	{
		bool onoff;
		va_start(ap, argsCount);
		onoff = va_arg(ap, bool);
		va_end(ap);
		AppContext->FindObject<UserInterface>(OBJECT_TYPE_UI2D_ROOMLIST + m_SceneName, OBJECT_NAME_LOBBY_ROOMLIST_NEXT)->OnActive(onoff);
		break;
	}
	case CLIENT_EVENT_LOBBY_UI_ROOMLIST_PRESSED:
	{
		string name;
		va_start(ap, argsCount);
		name = va_arg(ap, string);
		va_end(ap);

		string list = "LOBBY_ROOMLIST_LIST_";
		for (int i = 0; i < 7; ++i)
			if (list + to_string(i) != name)
				AppContext->FindObject<UserInterface>(OBJECT_TYPE_UI2D_ROOMLIST_LIST + m_SceneName, list + to_string(i))->Overlap(false);


		if (!AppContext->FindObject<UserInterface>(OBJECT_TYPE_UI2D_ROOMLIST_LIST + m_SceneName, name)->Overlap())
			m_SelectRoom = -1;

		break;
	}
	case CLIENT_EVENT_LOBBY_INPUT_STRING:
	{
		std::wstring wstr;
		va_start(ap, argsCount);
		wstr = va_arg(ap, std::wstring);
		va_end(ap);

		m_RoomNumber = wstr;
		break;
	}
	case CLIENT_EVENT_AUTOMATCH_START:
	{
		m_IsAutoMatching = true;
		// manualmatch non active
		AppContext->FindObject<UserInterface>(OBJECT_TYPE_UI2D + m_SceneName, OBJECT_NAME_MANUALMATCH)->OnActive(false);
		// display automatching loading sprite animation
		AppContext->DisplayUI2D(OBJECT_TYPE_UI2D + m_SceneName, OBJECT_NAME_RENEWAL_ARROW, XMFLOAT2(0.f, 470.f), XMFLOAT2(1000.f, 1000.f), false, AnchorType::Center, UI_LAYER_0);
		// display automatch cancel button
		AppContext->DisplayUI2D(OBJECT_TYPE_UI2D + m_SceneName, OBJECT_NAME_RENEWAL_BACK, XMFLOAT2(0.f, -470.f), XMFLOAT2(4000.f, 800.f), true, AnchorType::Center);

		break;
	}
	case CLIENT_EVENT_AUTOMATCH_CANCEL:
	{
		m_IsAutoMatching = false;
		// Manual Match button active
		AppContext->FindObject<UserInterface>(OBJECT_TYPE_UI2D + m_SceneName, OBJECT_NAME_MANUALMATCH)->OnActive(true);
		// hidden automatching loading image
		AppContext->HiddenUI2D(OBJECT_TYPE_UI2D + m_SceneName, OBJECT_NAME_RENEWAL_ARROW);
		// hidden automatch cancel button
		AppContext->HiddenUI2D(OBJECT_TYPE_UI2D + m_SceneName, OBJECT_NAME_RENEWAL_BACK);

		// init automatching sprite animation
		UserInterface* autoMatching = AppContext->FindObject<UserInterface>(OBJECT_TYPE_UI2D + m_SceneName, OBJECT_NAME_RENEWAL_ARROW);
		autoMatching->InitSpriteAnimation();

		break;
	}
	}
}

void LobbyScene::Initialize()
{
	m_SceneController = new LobbyController(this);
	m_SceneController->SetMapName(MAP_STR_LOBBY);

	m_RoomList.reserve(MAX_ROOM_COUNT);
	m_IsGetRoomList = false;

	/*Props*/
	AppContext->CreateProps(MAP_STR_LOBBY);

	/* UI2D */
	std::string ui2dName = OBJECT_TYPE_UI2D + m_SceneName;
	AppContext->CreateUI2D(ui2dName, OBJECT_NAME_AUTOMATCH, TEXTURE_INDEX_UI_BUTTON_LOBBY_AUTOMATCH_RELEASED, TEXTURE_INDEX_UI_BUTTON_LOBBY_AUTOMATCH_PRESSED, TEXTURE_INDEX_AUTOMATCH_ACTIVATION);
	AppContext->CreateUI2D(ui2dName, OBJECT_NAME_MANUALMATCH, TEXTURE_INDEX_UI_BUTTON_LOBBY_MANUALMATCH_RELEASED, TEXTURE_INDEX_UI_BUTTON_LOBBY_MANUALMATCH_PRESSED, TEXTURE_INDEX_MANUALMATCH_ACTIVATION);
	AppContext->CreateUI2D(ui2dName, OBJECT_NAME_RULE, TEXTURE_INDEX_UI_BUTTON_LOBBY_RULE_RELEASED, TEXTURE_INDEX_UI_BUTTON_LOBBY_RULE_PRESSED);
	AppContext->CreateUI2D(ui2dName, OBJECT_NAME_EXIT, TEXTURE_INDEX_UI_BUTTON_EXIT_RELEASED, TEXTURE_INDEX_UI_BUTTON_EXIT_PRESSED);
	AppContext->CreateUI2D(ui2dName, OBJECT_NAME_LOBBY_PRO_BACK, TEXTURE_INDEX_UI_LOBBY_PRO_BACK);

	AppContext->CreateUI2D(OBJECT_NAME_RULE_MANUAL + m_SceneName, OBJECT_NAME_BLUE_BLUR, TEXTURE_INDEX_TRANSPARENCY);
	AppContext->CreateUI2D(OBJECT_NAME_RULE_MANUAL + m_SceneName, OBJECT_NAME_X, TEXTURE_INDEX_UI_X);
	AppContext->CreateUI2D(OBJECT_NAME_RULE_MANUAL + m_SceneName, OBJECT_NAME_RULE_MANUAL, TEXTURE_INDEX_UI_RULE_MANUAL);

	AppContext->CreateUI2D(OBJECT_TYPE_UI2D_ROOMLIST + m_SceneName, OBJECT_NAME_LOBBY_ROOMLIST_BACK, TEXTURE_INDEX_UI_LOBBY_ROOMLIST_BACK);
	AppContext->CreateUI2D(OBJECT_TYPE_UI2D_ROOMLIST + m_SceneName, OBJECT_NAME_BACK_BLUR, TEXTURE_INDEX_TRANSPARENCY);
	AppContext->CreateUI2D(OBJECT_TYPE_UI2D_ROOMLIST + m_SceneName, OBJECT_NAME_BUTTON_BACK, TEXTURE_INDEX_UI_BUTTON_BACK_RELEASED, TEXTURE_INDEX_UI_BUTTON_BACK_PRESSED);
	AppContext->CreateUI2D(OBJECT_TYPE_UI2D_ROOMLIST + m_SceneName, OBJECT_NAME_LOBBY_MAKE_ROOM, TEXTURE_INDEX_UI_LOBBY_MAKE_ROOM_R, TEXTURE_INDEX_UI_LOBBY_MAKE_ROOM_P, TEXTURE_INDEX_UI_LOBBY_MAKE_ROOM_A);
	AppContext->CreateUI2D(OBJECT_TYPE_UI2D_ROOMLIST + m_SceneName, OBJECT_NAME_LOBBY_JOIN_ROOM, TEXTURE_INDEX_UI_LOBBY_JOIN_ROOM_R, TEXTURE_INDEX_UI_LOBBY_JOIN_ROOM_P, TEXTURE_INDEX_UI_LOBBY_JOIN_ROOM_A);
	AppContext->CreateUI2D(OBJECT_TYPE_UI2D_ROOMLIST + m_SceneName, OBJECT_NAME_LOBBY_ROOMLIST_PREV, TEXTURE_INDEX_UI_BUTTON_MAP_PREV_PRESSED, TEXTURE_INDEX_UI_BUTTON_MAP_PREV_RELEASED, TEXTURE_INDEX_UI_BUTTON_MAP_PREV_ACTIVATION);
	AppContext->CreateUI2D(OBJECT_TYPE_UI2D_ROOMLIST + m_SceneName, OBJECT_NAME_LOBBY_ROOMLIST_NEXT, TEXTURE_INDEX_UI_BUTTON_MAP_NEXT_PRESSED, TEXTURE_INDEX_UI_BUTTON_MAP_NEXT_RELEASED, TEXTURE_INDEX_UI_BUTTON_MAP_NEXT_ACTIVATION);
	AppContext->CreateUI2D(OBJECT_TYPE_UI2D_ROOMLIST + m_SceneName, OBJECT_NAME_LOBBY_ROOMLIST_BTN_RENEWAL, TEXTURE_INDEX_UI_BTN_RENEWAL_R, TEXTURE_INDEX_UI_BTN_RENEWAL_P, TEXTURE_INDEX_UI_BTN_RENEWAL_U);

	AppContext->CreateUI2D(OBJECT_TYPE_UI2D_ROOMLIST_LIST + m_SceneName, OBJECT_NAME_LOBBY_ROOMLIST_0, TEXTURE_INDEX_UI_LOBBY_ROOMLIST_R, TEXTURE_INDEX_UI_LOBBY_ROOMLIST_P);
	AppContext->CreateUI2D(OBJECT_TYPE_UI2D_ROOMLIST_LIST + m_SceneName, OBJECT_NAME_LOBBY_ROOMLIST_1, TEXTURE_INDEX_UI_LOBBY_ROOMLIST_R, TEXTURE_INDEX_UI_LOBBY_ROOMLIST_P);
	AppContext->CreateUI2D(OBJECT_TYPE_UI2D_ROOMLIST_LIST + m_SceneName, OBJECT_NAME_LOBBY_ROOMLIST_2, TEXTURE_INDEX_UI_LOBBY_ROOMLIST_R, TEXTURE_INDEX_UI_LOBBY_ROOMLIST_P);
	AppContext->CreateUI2D(OBJECT_TYPE_UI2D_ROOMLIST_LIST + m_SceneName, OBJECT_NAME_LOBBY_ROOMLIST_3, TEXTURE_INDEX_UI_LOBBY_ROOMLIST_R, TEXTURE_INDEX_UI_LOBBY_ROOMLIST_P);
	AppContext->CreateUI2D(OBJECT_TYPE_UI2D_ROOMLIST_LIST + m_SceneName, OBJECT_NAME_LOBBY_ROOMLIST_4, TEXTURE_INDEX_UI_LOBBY_ROOMLIST_R, TEXTURE_INDEX_UI_LOBBY_ROOMLIST_P);
	AppContext->CreateUI2D(OBJECT_TYPE_UI2D_ROOMLIST_LIST + m_SceneName, OBJECT_NAME_LOBBY_ROOMLIST_5, TEXTURE_INDEX_UI_LOBBY_ROOMLIST_R, TEXTURE_INDEX_UI_LOBBY_ROOMLIST_P);
	AppContext->CreateUI2D(OBJECT_TYPE_UI2D_ROOMLIST_LIST + m_SceneName, OBJECT_NAME_LOBBY_ROOMLIST_6, TEXTURE_INDEX_UI_LOBBY_ROOMLIST_R, TEXTURE_INDEX_UI_LOBBY_ROOMLIST_P);

	AppContext->CreateUI2D(OBJECT_TYPE_UI2D_MAKE_ROOM + m_SceneName, OBJECT_NAME_LOBBY_MAKE_ROOM_WINDOW, TEXTURE_INDEX_UI_MAKE_ROOM_WINDOW);
	AppContext->CreateUI2D(OBJECT_TYPE_UI2D_MAKE_ROOM + m_SceneName, OBJECT_NAME_LOBBY_MAKE_ROOM_INPUT, TEXTURE_INDEX_UI_MAKE_ROOM_INPUT);
	AppContext->CreateUI2D(OBJECT_TYPE_UI2D_MAKE_ROOM + m_SceneName, OBJECT_NAME_BTN_CANCEL, TEXTURE_INDEX_UI_CANCEL_R, TEXTURE_INDEX_UI_CANCEL_P);
	AppContext->CreateUI2D(OBJECT_TYPE_UI2D_MAKE_ROOM + m_SceneName, OBJECT_NAME_BTN_CREATE, TEXTURE_INDEX_UI_CREATE_R, TEXTURE_INDEX_UI_CREATE_P);

	AppContext->CreateUI2D(OBJECT_TYPE_UI2D_EXIT_GAME + m_SceneName, OBJECT_NAME_EXIT_WINDOW, TEXTURE_INDEX_UI_GAME_EXIT_WINDOW);
	AppContext->CreateUI2D(OBJECT_TYPE_UI2D_EXIT_GAME + m_SceneName, OBJECT_NAME_YES, TEXTURE_INDEX_UI_BTN_YES_R, TEXTURE_INDEX_UI_BTN_YES_P);
	AppContext->CreateUI2D(OBJECT_TYPE_UI2D_EXIT_GAME + m_SceneName, OBJECT_NAME_NO, TEXTURE_INDEX_UI_BTN_NO_R, TEXTURE_INDEX_UI_BTN_NO_P);

	AppContext->CreateUI2D(OBJECT_TYPE_UI2D_JOIN_DENY + m_SceneName, OBJECT_NAME_LOBBY_JOIN_DENY, TEXTURE_INDEX_UI_MAKE_ROOM_WINDOW);

	AppContext->CreateUI2D(OBJECT_TYPE_UI2D + m_SceneName, OBJECT_NAME_RENEWAL_BACK, TEXTURE_INDEX_UI_BUTTON_BACK_RELEASED, TEXTURE_INDEX_UI_BUTTON_BACK_PRESSED);
	AppContext->CreateUI2D(OBJECT_TYPE_UI2D + m_SceneName, OBJECT_NAME_RENEWAL_ARROW, TEXTURE_INDEX_AUTOMATCH_LOADING_1);
	UserInterface* automatchLoading = AppContext->FindObject<UserInterface>(OBJECT_TYPE_UI2D + m_SceneName, OBJECT_NAME_RENEWAL_ARROW);
	automatchLoading->SetSpriteAniamtion(SPRITE_NAME_AUTOMATCH_LOADING, 1.f);
	automatchLoading->AddFrame(SPRITE_NAME_AUTOMATCH_LOADING, TEXTURE_INDEX_AUTOMATCH_LOADING_1);
	automatchLoading->AddFrame(SPRITE_NAME_AUTOMATCH_LOADING, TEXTURE_INDEX_AUTOMATCH_LOADING_2);
	automatchLoading->AddFrame(SPRITE_NAME_AUTOMATCH_LOADING, TEXTURE_INDEX_AUTOMATCH_LOADING_3);
	automatchLoading->AddFrame(SPRITE_NAME_AUTOMATCH_LOADING, TEXTURE_INDEX_AUTOMATCH_LOADING_4);
	automatchLoading->AddFrame(SPRITE_NAME_AUTOMATCH_LOADING, TEXTURE_INDEX_AUTOMATCH_LOADING_5);
	automatchLoading->AddFrame(SPRITE_NAME_AUTOMATCH_LOADING, TEXTURE_INDEX_AUTOMATCH_LOADING_6);
	automatchLoading->AddFrame(SPRITE_NAME_AUTOMATCH_LOADING, TEXTURE_INDEX_AUTOMATCH_LOADING_7);
	automatchLoading->AddFrame(SPRITE_NAME_AUTOMATCH_LOADING, TEXTURE_INDEX_AUTOMATCH_LOADING_8);
}

void LobbyScene::OnResize()
{
}

bool LobbyScene::Enter()
{
	cout << "=============== Lobby Scene ===============" << endl;
	m_IsGetRoomList = false;

	Core::g_InputSwitch = true;

	m_IsRuleManual = false;
	m_IsExitGame = false;
	m_IsMakeRoom = false;

	m_RoomTitleWarning = 0;
	m_RoomListPage = 0;
	m_RoomListMaxPage = 0;
	m_RoomListCntLastPage = 0;
	m_SelectRoom = -1;

	if (m_IsManualMatchRoomLlist)
		Service::GetApp()->Notify(EVENT_LOBBY_MANUALMATCH_ROOMLIST_REQUEST);

	string name = "LOBBY_ROOMLIST_LIST_";
	for (int i = 0; i < 7; ++i)
		AppContext->FindObject<UserInterface>(OBJECT_TYPE_UI2D_ROOMLIST_LIST + m_SceneName, name + to_string(i))->Overlap(false);

	/* Create SceneBounds for Shadow */
	m_SceneBounds.Center = XMFLOAT3(1106.77, 0, 497.02);
	m_SceneBounds.Radius = sqrtf(2000.f * 2000.f + 2000.f * 2000.f);

	/* Light Setting */
	CACHE_CACHE::GetApp()->m_Lights[LIGHT_NAME_DIRECTIONAL]->Direction = { -0.40265, -1.43735, 0.04265 };
	CACHE_CACHE::GetApp()->m_Lights[LIGHT_NAME_DIRECTIONAL]->Strength = { 0.32,0.32,0.32 };

	// Props
	AppContext->DisplayProps(MAP_STR_LOBBY, true);

	// Character
	AppContext->DisplayCharacter(MAP_STR_LOBBY, CHARACTER_WIZARD, 0, true);
	Character* wizard = AppContext->FindObject<Character>(CHARACTER_WIZARD, CHARACTER_WIZARD);
	wizard->SetAnimationPlayerState(AnimationController::PlayerState::STATE_FIND);

	// Display UI
	AppContext->DisplayUI2D(OBJECT_TYPE_UI2D + m_SceneName, OBJECT_NAME_AUTOMATCH, XMFLOAT2(-750.f, 480.f), XMFLOAT2(4000.f, 800.f), true, AnchorType::LT);
	AppContext->DisplayUI2D(OBJECT_TYPE_UI2D + m_SceneName, OBJECT_NAME_MANUALMATCH, XMFLOAT2(-750.f, 380.f), XMFLOAT2(4000.f, 800.f), true, AnchorType::LT);
	AppContext->DisplayUI2D(OBJECT_TYPE_UI2D + m_SceneName, OBJECT_NAME_RULE, XMFLOAT2(-750.f, 280.f), XMFLOAT2(4000.f, 800.f), true, AnchorType::LT, UI_LAYER_BACK);
	AppContext->DisplayUI2D(OBJECT_TYPE_UI2D + m_SceneName, OBJECT_NAME_EXIT, XMFLOAT2(-750.f, 180.f), XMFLOAT2(4000.f, 800.f), true, AnchorType::LT);
	AppContext->DisplayUI2D(OBJECT_TYPE_UI2D + m_SceneName, OBJECT_NAME_LOBBY_PRO_BACK, XMFLOAT2(-730.f, -150.f), XMFLOAT2(5000.f, 5500.f), false, AnchorType::LT, UI_LAYER_1, true);

	AppContext->DisplayUI2D(OBJECT_NAME_RULE_MANUAL + m_SceneName, OBJECT_NAME_X, XMFLOAT2(675.f, 465.f), XMFLOAT2(1000.f, 1000.f), true, AnchorType::Center, UI_LAYER_0);
	AppContext->DisplayUI2D(OBJECT_NAME_RULE_MANUAL + m_SceneName, OBJECT_NAME_RULE_MANUAL, XMFLOAT2(0.f, 0.f), XMFLOAT2(15000.f, 8350.f), false, AnchorType::Center, UI_LAYER_0);
	AppContext->DisplayUI2D(OBJECT_NAME_RULE_MANUAL + m_SceneName, OBJECT_NAME_BLUE_BLUR, XMFLOAT2(0.f, 0.f), XMFLOAT2(1000000.f, 1000000.f), false, AnchorType::Center, UI_LAYER_1);
	AppContext->DisplayUI2D(OBJECT_TYPE_UI2D_ROOMLIST + m_SceneName, OBJECT_NAME_BACK_BLUR, XMFLOAT2(0.f, 0.f), XMFLOAT2(1000000.f, 1000000.f), false, AnchorType::Center);
	AppContext->DisplayUI2D(OBJECT_TYPE_UI2D_ROOMLIST + m_SceneName, OBJECT_NAME_LOBBY_ROOMLIST_BACK, XMFLOAT2(-200.f, 0.f), XMFLOAT2(13000.f, 9000.f), false, AnchorType::Center, UI_LAYER_1);
	AppContext->DisplayUI2D(OBJECT_TYPE_UI2D_ROOMLIST + m_SceneName, OBJECT_NAME_LOBBY_ROOMLIST_PREV, XMFLOAT2(-300.f, -400.f), XMFLOAT2(800.f, 800.f), true, AnchorType::Center, UI_LAYER_0);
	AppContext->DisplayUI2D(OBJECT_TYPE_UI2D_ROOMLIST + m_SceneName, OBJECT_NAME_LOBBY_ROOMLIST_NEXT, XMFLOAT2(-100.f, -400.f), XMFLOAT2(800.f, 800.f), true, AnchorType::Center, UI_LAYER_0);
	AppContext->DisplayUI2D(OBJECT_TYPE_UI2D_ROOMLIST + m_SceneName, OBJECT_NAME_LOBBY_ROOMLIST_BTN_RENEWAL, XMFLOAT2(-200.f, -400.f), XMFLOAT2(800.f, 800.f), true, AnchorType::Center, UI_LAYER_0);

	AppContext->DisplayUI2D(OBJECT_TYPE_UI2D_ROOMLIST_LIST + m_SceneName, OBJECT_NAME_LOBBY_ROOMLIST_0, XMFLOAT2(-200.f, 300.f), XMFLOAT2(12500.f, 900.f), true, AnchorType::Center, UI_LAYER_0, true);
	AppContext->DisplayUI2D(OBJECT_TYPE_UI2D_ROOMLIST_LIST + m_SceneName, OBJECT_NAME_LOBBY_ROOMLIST_1, XMFLOAT2(-200.f, 200.f), XMFLOAT2(12500.f, 900.f), true, AnchorType::Center, UI_LAYER_0, true);
	AppContext->DisplayUI2D(OBJECT_TYPE_UI2D_ROOMLIST_LIST + m_SceneName, OBJECT_NAME_LOBBY_ROOMLIST_2, XMFLOAT2(-200.f, 100.f), XMFLOAT2(12500.f, 900.f), true, AnchorType::Center, UI_LAYER_0, true);
	AppContext->DisplayUI2D(OBJECT_TYPE_UI2D_ROOMLIST_LIST + m_SceneName, OBJECT_NAME_LOBBY_ROOMLIST_3, XMFLOAT2(-200.f, 0.f), XMFLOAT2(12500.f, 900.f), true, AnchorType::Center, UI_LAYER_0, true);
	AppContext->DisplayUI2D(OBJECT_TYPE_UI2D_ROOMLIST_LIST + m_SceneName, OBJECT_NAME_LOBBY_ROOMLIST_4, XMFLOAT2(-200.f, -100.f), XMFLOAT2(12500.f, 900.f), true, AnchorType::Center, UI_LAYER_0, true);
	AppContext->DisplayUI2D(OBJECT_TYPE_UI2D_ROOMLIST_LIST + m_SceneName, OBJECT_NAME_LOBBY_ROOMLIST_5, XMFLOAT2(-200.f, -200.f), XMFLOAT2(12500.f, 900.f), true, AnchorType::Center, UI_LAYER_0, true);
	AppContext->DisplayUI2D(OBJECT_TYPE_UI2D_ROOMLIST_LIST + m_SceneName, OBJECT_NAME_LOBBY_ROOMLIST_6, XMFLOAT2(-200.f, -300.f), XMFLOAT2(12500.f, 900.f), true, AnchorType::Center, UI_LAYER_0, true);

	AppContext->DisplayUI2D(OBJECT_TYPE_UI2D_MAKE_ROOM + m_SceneName, OBJECT_NAME_LOBBY_MAKE_ROOM_WINDOW, XMFLOAT2(-170.f, 0.f), XMFLOAT2(6800.f, 3200.f), false, AnchorType::Center, UI_LAYER_1, true);
	AppContext->DisplayUI2D(OBJECT_TYPE_UI2D_MAKE_ROOM + m_SceneName, OBJECT_NAME_LOBBY_MAKE_ROOM_INPUT, XMFLOAT2(-170.f, 90.f), XMFLOAT2(6000.f, 900.f), true, AnchorType::Center, UI_LAYER_0, true);
	AppContext->DisplayUI2D(OBJECT_TYPE_UI2D_MAKE_ROOM + m_SceneName, OBJECT_NAME_BTN_CANCEL, XMFLOAT2(-15.f, -90.f), XMFLOAT2(2800.f, 750.f), true, AnchorType::Center, UI_LAYER_0);
	AppContext->DisplayUI2D(OBJECT_TYPE_UI2D_MAKE_ROOM + m_SceneName, OBJECT_NAME_BTN_CREATE, XMFLOAT2(-325.f, -90.f), XMFLOAT2(2800.f, 750.f), true, AnchorType::Center, UI_LAYER_0);

	AppContext->DisplayUI2D(OBJECT_TYPE_UI2D_ROOMLIST + m_SceneName, OBJECT_NAME_BUTTON_BACK, XMFLOAT2(700.f, 200.f), XMFLOAT2(4000.f, 800.f), true, AnchorType::Center, UI_LAYER_1);
	AppContext->DisplayUI2D(OBJECT_TYPE_UI2D_ROOMLIST + m_SceneName, OBJECT_NAME_LOBBY_MAKE_ROOM, XMFLOAT2(700.f, 400.f), XMFLOAT2(4000.f, 800.f), true, AnchorType::Center, UI_LAYER_1);
	AppContext->DisplayUI2D(OBJECT_TYPE_UI2D_ROOMLIST + m_SceneName, OBJECT_NAME_LOBBY_JOIN_ROOM, XMFLOAT2(700.f, 300.f), XMFLOAT2(4000.f, 800.f), true, AnchorType::Center, UI_LAYER_1);

	AppContext->DisplayUI2D(OBJECT_TYPE_UI2D_EXIT_GAME + m_SceneName, OBJECT_NAME_EXIT_WINDOW, XMFLOAT2(0.f, 0.f), XMFLOAT2(9000.f, 3000.f), false, AnchorType::Center, UI_LAYER_1);
	AppContext->DisplayUI2D(OBJECT_TYPE_UI2D_EXIT_GAME + m_SceneName, OBJECT_NAME_YES, XMFLOAT2(-240.f, -80.f), XMFLOAT2(3000.f, 780.f), true, AnchorType::Center, UI_LAYER_0);
	AppContext->DisplayUI2D(OBJECT_TYPE_UI2D_EXIT_GAME + m_SceneName, OBJECT_NAME_NO, XMFLOAT2(240.f, -80.f), XMFLOAT2(3000.f, 780.f), true, AnchorType::Center, UI_LAYER_0);

	AppContext->DisplayUI2D(OBJECT_TYPE_UI2D_JOIN_DENY + m_SceneName, OBJECT_NAME_LOBBY_JOIN_DENY, XMFLOAT2(-170.f, 0.f), XMFLOAT2(6800.f, 3000.f), true, AnchorType::Center, UI_LAYER_0, true);

	AppContext->FindObject<UserInterface>(OBJECT_TYPE_UI2D_ROOMLIST + m_SceneName, OBJECT_NAME_LOBBY_ROOMLIST_NEXT)->OnActive(false);
	AppContext->FindObject<UserInterface>(OBJECT_TYPE_UI2D_ROOMLIST + m_SceneName, OBJECT_NAME_LOBBY_ROOMLIST_PREV)->OnActive(false);

	// 여기서 Network::m_Client 정보 가져와서 정보창 채우기?
	Service::GetApp()->Notify(EVENT_LOBBY_REQUEST_USERINFO);

	// AppContext->DisplayUI2D(OBJECT_TYPE_UI2D_ROOMLIST + m_SceneName, OBJECT_NAME_LOBBY_ROOMLIST, XMFLOAT2(0.f, 300.f), XMFLOAT2(12500.f, 750.f), false, AnchorType::Center, UI_LAYER_1);

	// 카메라 뷰행렬 초기화
	CACHE_CACHE::GetApp()->m_Camera->CameraInitialize(SceneType::Lobby);

	// 사운드 시작
	SoundManager::GetApp()->PlayBGM(L"BGM_Lobby(Low).wav", 0.8f);

	return true;
}

void LobbyScene::Exit()
{
	Core::g_InputSwitch = false;

	m_IsRuleManual = false;
	m_IsExitGame = false;
	m_IsMakeRoom = false;

	m_RoomTitleWarning = 0;
	m_RoomListPage = 0;
	m_RoomListMaxPage = 0;
	m_RoomListCntLastPage = 0;
	m_SelectRoom = -1;

	if (m_IsAutoMatching)
	{
		m_IsAutoMatching = false;
		// Manual Match button active
		AppContext->FindObject<UserInterface>(OBJECT_TYPE_UI2D + m_SceneName, OBJECT_NAME_MANUALMATCH)->OnActive(true);
		// hidden automatching loading image
		AppContext->HiddenUI2D(OBJECT_TYPE_UI2D + m_SceneName, OBJECT_NAME_RENEWAL_ARROW);
		// hidden automatch cancel button
		AppContext->HiddenUI2D(OBJECT_TYPE_UI2D + m_SceneName, OBJECT_NAME_RENEWAL_BACK);

		// init automatching sprite animation
		UserInterface* autoMatching = AppContext->FindObject<UserInterface>(OBJECT_TYPE_UI2D + m_SceneName, OBJECT_NAME_RENEWAL_ARROW);
		autoMatching->InitSpriteAnimation();
	}

	// Hidden Props
	AppContext->HiddenProps(MAP_STR_LOBBY);

	//Hidden Character
	AppContext->HiddenCharacter(CHARACTER_WIZARD);

	// Hidden UIs
	AppContext->HiddenUI2D(OBJECT_TYPE_UI2D + m_SceneName, OBJECT_NAME_AUTOMATCH);
	AppContext->HiddenUI2D(OBJECT_TYPE_UI2D + m_SceneName, OBJECT_NAME_MANUALMATCH);
	AppContext->HiddenUI2D(OBJECT_TYPE_UI2D + m_SceneName, OBJECT_NAME_RULE);
	AppContext->HiddenUI2D(OBJECT_TYPE_UI2D + m_SceneName, OBJECT_NAME_EXIT);
	AppContext->HiddenUI2D(OBJECT_TYPE_UI2D + m_SceneName, OBJECT_NAME_LOBBY_PRO_BACK);

	AppContext->HiddenUI2D(OBJECT_NAME_RULE_MANUAL + m_SceneName, OBJECT_NAME_X);
	AppContext->HiddenUI2D(OBJECT_NAME_RULE_MANUAL + m_SceneName, OBJECT_NAME_RULE_MANUAL);
	AppContext->HiddenUI2D(OBJECT_NAME_RULE_MANUAL + m_SceneName, OBJECT_NAME_BLUE_BLUR);

	AppContext->HiddenUI2D(OBJECT_TYPE_UI2D + m_SceneName, OBJECT_NAME_RENEWAL_ARROW);

	// OBJECT_TYPE_UI2D_ROOMLIST
	AppContext->HiddenUI2D(OBJECT_TYPE_UI2D_ROOMLIST + m_SceneName, OBJECT_NAME_LOBBY_ROOMLIST_BACK);
	AppContext->HiddenUI2D(OBJECT_TYPE_UI2D_ROOMLIST + m_SceneName, OBJECT_NAME_BACK_BLUR);
	AppContext->HiddenUI2D(OBJECT_TYPE_UI2D_ROOMLIST + m_SceneName, OBJECT_NAME_BUTTON_BACK);
	AppContext->HiddenUI2D(OBJECT_TYPE_UI2D_ROOMLIST + m_SceneName, OBJECT_NAME_LOBBY_MAKE_ROOM);
	AppContext->HiddenUI2D(OBJECT_TYPE_UI2D_ROOMLIST + m_SceneName, OBJECT_NAME_LOBBY_JOIN_ROOM);
	AppContext->HiddenUI2D(OBJECT_TYPE_UI2D_ROOMLIST + m_SceneName, OBJECT_NAME_LOBBY_ROOMLIST_PREV);
	AppContext->HiddenUI2D(OBJECT_TYPE_UI2D_ROOMLIST + m_SceneName, OBJECT_NAME_LOBBY_ROOMLIST_NEXT);
	AppContext->HiddenUI2D(OBJECT_TYPE_UI2D_ROOMLIST + m_SceneName, OBJECT_NAME_LOBBY_ROOMLIST_BTN_RENEWAL);

	AppContext->HiddenUI2D(OBJECT_TYPE_UI2D_ROOMLIST_LIST + m_SceneName, OBJECT_NAME_LOBBY_ROOMLIST_0);
	AppContext->HiddenUI2D(OBJECT_TYPE_UI2D_ROOMLIST_LIST + m_SceneName, OBJECT_NAME_LOBBY_ROOMLIST_1);
	AppContext->HiddenUI2D(OBJECT_TYPE_UI2D_ROOMLIST_LIST + m_SceneName, OBJECT_NAME_LOBBY_ROOMLIST_2);
	AppContext->HiddenUI2D(OBJECT_TYPE_UI2D_ROOMLIST_LIST + m_SceneName, OBJECT_NAME_LOBBY_ROOMLIST_3);
	AppContext->HiddenUI2D(OBJECT_TYPE_UI2D_ROOMLIST_LIST + m_SceneName, OBJECT_NAME_LOBBY_ROOMLIST_4);
	AppContext->HiddenUI2D(OBJECT_TYPE_UI2D_ROOMLIST_LIST + m_SceneName, OBJECT_NAME_LOBBY_ROOMLIST_5);
	AppContext->HiddenUI2D(OBJECT_TYPE_UI2D_ROOMLIST_LIST + m_SceneName, OBJECT_NAME_LOBBY_ROOMLIST_6);

	AppContext->HiddenUI2D(OBJECT_TYPE_UI2D_MAKE_ROOM + m_SceneName, OBJECT_NAME_LOBBY_MAKE_ROOM_WINDOW);
	AppContext->HiddenUI2D(OBJECT_TYPE_UI2D_MAKE_ROOM + m_SceneName, OBJECT_NAME_LOBBY_MAKE_ROOM_INPUT);
	AppContext->HiddenUI2D(OBJECT_TYPE_UI2D_MAKE_ROOM + m_SceneName, OBJECT_NAME_BTN_CANCEL);
	AppContext->HiddenUI2D(OBJECT_TYPE_UI2D_MAKE_ROOM + m_SceneName, OBJECT_NAME_BTN_CREATE);

	AppContext->HiddenUI2D(OBJECT_TYPE_UI2D_EXIT_GAME + m_SceneName, OBJECT_NAME_EXIT_WINDOW);
	AppContext->HiddenUI2D(OBJECT_TYPE_UI2D_EXIT_GAME + m_SceneName, OBJECT_NAME_YES);
	AppContext->HiddenUI2D(OBJECT_TYPE_UI2D_EXIT_GAME + m_SceneName, OBJECT_NAME_NO);

	AppContext->HiddenUI2D(OBJECT_TYPE_UI2D_JOIN_DENY + m_SceneName, OBJECT_NAME_LOBBY_JOIN_DENY);

	cout << "===========================================" << endl << endl;
}

void LobbyScene::Update(const float& fDeltaTime)
{
	m_SceneController->Update(fDeltaTime);

	/*Props*/
	for (auto& prop : AppContext->m_Maps[MAP_STR_LOBBY]->propTypeVector)
	{
		GraphicsContext::GetApp()->UpdateInstanceData(AppContext->m_RItemsMap[prop], AppContext->m_RItemsVec);
	}

	/*Character*/
	GraphicsContext::GetApp()->UpdateInstanceData(AppContext->m_RItemsMap[CHARACTER_WIZARD], AppContext->m_RItemsVec);
	AppContext->FindObject<Character>(CHARACTER_WIZARD, CHARACTER_WIZARD)->Update(fDeltaTime);
	GraphicsContext::GetApp()->UpdateSkinnedCBs(BoneIndex::Wizard, AssertsReference::GetApp()->m_SkinnedModelInsts[CHARACTER_WIZARD].get());

	/*UIs*/
	if (m_IsRuleManual)
	{
		AppContext->SetDisplayUI2D(OBJECT_NAME_RULE_MANUAL + m_SceneName, OBJECT_NAME_X, true);
		AppContext->SetDisplayUI2D(OBJECT_NAME_RULE_MANUAL + m_SceneName, OBJECT_NAME_RULE_MANUAL, true);
		AppContext->SetDisplayUI2D(OBJECT_NAME_RULE_MANUAL + m_SceneName, OBJECT_NAME_BLUE_BLUR, true);
	}
	else
	{
		AppContext->SetDisplayUI2D(OBJECT_NAME_RULE_MANUAL + m_SceneName, OBJECT_NAME_X, false);
		AppContext->SetDisplayUI2D(OBJECT_NAME_RULE_MANUAL + m_SceneName, OBJECT_NAME_RULE_MANUAL, false);
		AppContext->SetDisplayUI2D(OBJECT_NAME_RULE_MANUAL + m_SceneName, OBJECT_NAME_BLUE_BLUR, false);
	}

	if (m_IsManualMatchRoomLlist)
	{
		AppContext->SetDisplayUI2D(OBJECT_TYPE_UI2D_ROOMLIST + m_SceneName, OBJECT_NAME_LOBBY_ROOMLIST_BACK, true);
		AppContext->SetDisplayUI2D(OBJECT_TYPE_UI2D_ROOMLIST + m_SceneName, OBJECT_NAME_BACK_BLUR, true);

		AppContext->SetDisplayUI2D(OBJECT_TYPE_UI2D_ROOMLIST_LIST + m_SceneName, OBJECT_NAME_LOBBY_ROOMLIST_0, true);
		AppContext->SetDisplayUI2D(OBJECT_TYPE_UI2D_ROOMLIST_LIST + m_SceneName, OBJECT_NAME_LOBBY_ROOMLIST_1, true);
		AppContext->SetDisplayUI2D(OBJECT_TYPE_UI2D_ROOMLIST_LIST + m_SceneName, OBJECT_NAME_LOBBY_ROOMLIST_2, true);
		AppContext->SetDisplayUI2D(OBJECT_TYPE_UI2D_ROOMLIST_LIST + m_SceneName, OBJECT_NAME_LOBBY_ROOMLIST_3, true);
		AppContext->SetDisplayUI2D(OBJECT_TYPE_UI2D_ROOMLIST_LIST + m_SceneName, OBJECT_NAME_LOBBY_ROOMLIST_4, true);
		AppContext->SetDisplayUI2D(OBJECT_TYPE_UI2D_ROOMLIST_LIST + m_SceneName, OBJECT_NAME_LOBBY_ROOMLIST_5, true);
		AppContext->SetDisplayUI2D(OBJECT_TYPE_UI2D_ROOMLIST_LIST + m_SceneName, OBJECT_NAME_LOBBY_ROOMLIST_6, true);

		AppContext->SetDisplayUI2D(OBJECT_TYPE_UI2D_ROOMLIST + m_SceneName, OBJECT_NAME_LOBBY_ROOMLIST_PREV, true);
		AppContext->SetDisplayUI2D(OBJECT_TYPE_UI2D_ROOMLIST + m_SceneName, OBJECT_NAME_LOBBY_ROOMLIST_NEXT, true);
		AppContext->SetDisplayUI2D(OBJECT_TYPE_UI2D_ROOMLIST + m_SceneName, OBJECT_NAME_LOBBY_ROOMLIST_BTN_RENEWAL, true);

		AppContext->SetDisplayUI2D(OBJECT_TYPE_UI2D_ROOMLIST + m_SceneName, OBJECT_NAME_BUTTON_BACK, true);
		AppContext->SetDisplayUI2D(OBJECT_TYPE_UI2D_ROOMLIST + m_SceneName, OBJECT_NAME_LOBBY_MAKE_ROOM, true);
		AppContext->SetDisplayUI2D(OBJECT_TYPE_UI2D_ROOMLIST + m_SceneName, OBJECT_NAME_LOBBY_JOIN_ROOM, true);
	}
	else
	{
		AppContext->SetDisplayUI2D(OBJECT_TYPE_UI2D_ROOMLIST_LIST + m_SceneName, OBJECT_NAME_LOBBY_ROOMLIST_0, false);
		AppContext->SetDisplayUI2D(OBJECT_TYPE_UI2D_ROOMLIST_LIST + m_SceneName, OBJECT_NAME_LOBBY_ROOMLIST_1, false);
		AppContext->SetDisplayUI2D(OBJECT_TYPE_UI2D_ROOMLIST_LIST + m_SceneName, OBJECT_NAME_LOBBY_ROOMLIST_2, false);
		AppContext->SetDisplayUI2D(OBJECT_TYPE_UI2D_ROOMLIST_LIST + m_SceneName, OBJECT_NAME_LOBBY_ROOMLIST_3, false);
		AppContext->SetDisplayUI2D(OBJECT_TYPE_UI2D_ROOMLIST_LIST + m_SceneName, OBJECT_NAME_LOBBY_ROOMLIST_4, false);
		AppContext->SetDisplayUI2D(OBJECT_TYPE_UI2D_ROOMLIST_LIST + m_SceneName, OBJECT_NAME_LOBBY_ROOMLIST_5, false);
		AppContext->SetDisplayUI2D(OBJECT_TYPE_UI2D_ROOMLIST_LIST + m_SceneName, OBJECT_NAME_LOBBY_ROOMLIST_6, false);

		AppContext->SetDisplayUI2D(OBJECT_TYPE_UI2D_ROOMLIST + m_SceneName, OBJECT_NAME_LOBBY_ROOMLIST_BACK, false);
		AppContext->SetDisplayUI2D(OBJECT_TYPE_UI2D_ROOMLIST + m_SceneName, OBJECT_NAME_LOBBY_ROOMLIST_PREV, false);
		AppContext->SetDisplayUI2D(OBJECT_TYPE_UI2D_ROOMLIST + m_SceneName, OBJECT_NAME_LOBBY_ROOMLIST_NEXT, false);
		AppContext->SetDisplayUI2D(OBJECT_TYPE_UI2D_ROOMLIST + m_SceneName, OBJECT_NAME_LOBBY_ROOMLIST_BTN_RENEWAL, false);
		AppContext->SetDisplayUI2D(OBJECT_TYPE_UI2D_ROOMLIST + m_SceneName, OBJECT_NAME_BACK_BLUR, false);
		AppContext->SetDisplayUI2D(OBJECT_TYPE_UI2D_ROOMLIST + m_SceneName, OBJECT_NAME_BUTTON_BACK, false);
		AppContext->SetDisplayUI2D(OBJECT_TYPE_UI2D_ROOMLIST + m_SceneName, OBJECT_NAME_LOBBY_MAKE_ROOM, false);
		AppContext->SetDisplayUI2D(OBJECT_TYPE_UI2D_ROOMLIST + m_SceneName, OBJECT_NAME_LOBBY_JOIN_ROOM, false);
	}


	if (m_IsExitGame)
	{
		AppContext->SetDisplayUI2D(OBJECT_TYPE_UI2D_EXIT_GAME + m_SceneName, OBJECT_NAME_EXIT_WINDOW, true);
		AppContext->SetDisplayUI2D(OBJECT_TYPE_UI2D_EXIT_GAME + m_SceneName, OBJECT_NAME_YES, true);
		AppContext->SetDisplayUI2D(OBJECT_TYPE_UI2D_EXIT_GAME + m_SceneName, OBJECT_NAME_NO, true);
	}
	else
	{
		AppContext->SetDisplayUI2D(OBJECT_TYPE_UI2D_EXIT_GAME + m_SceneName, OBJECT_NAME_EXIT_WINDOW, false);
		AppContext->SetDisplayUI2D(OBJECT_TYPE_UI2D_EXIT_GAME + m_SceneName, OBJECT_NAME_YES, false);
		AppContext->SetDisplayUI2D(OBJECT_TYPE_UI2D_EXIT_GAME + m_SceneName, OBJECT_NAME_NO, false);

	}
	if (m_IsMakeRoom)
	{
		AppContext->SetDisplayUI2D(OBJECT_TYPE_UI2D_MAKE_ROOM + m_SceneName, OBJECT_NAME_LOBBY_MAKE_ROOM_WINDOW, true);
		AppContext->SetDisplayUI2D(OBJECT_TYPE_UI2D_MAKE_ROOM + m_SceneName, OBJECT_NAME_LOBBY_MAKE_ROOM_INPUT, true);
		AppContext->SetDisplayUI2D(OBJECT_TYPE_UI2D_MAKE_ROOM + m_SceneName, OBJECT_NAME_BTN_CANCEL, true);
		AppContext->SetDisplayUI2D(OBJECT_TYPE_UI2D_MAKE_ROOM + m_SceneName, OBJECT_NAME_BTN_CREATE, true);
	}
	else
	{
		AppContext->SetDisplayUI2D(OBJECT_TYPE_UI2D_MAKE_ROOM + m_SceneName, OBJECT_NAME_LOBBY_MAKE_ROOM_WINDOW, false);
		AppContext->SetDisplayUI2D(OBJECT_TYPE_UI2D_MAKE_ROOM + m_SceneName, OBJECT_NAME_LOBBY_MAKE_ROOM_INPUT, false);
		AppContext->SetDisplayUI2D(OBJECT_TYPE_UI2D_MAKE_ROOM + m_SceneName, OBJECT_NAME_BTN_CANCEL, false);
		AppContext->SetDisplayUI2D(OBJECT_TYPE_UI2D_MAKE_ROOM + m_SceneName, OBJECT_NAME_BTN_CREATE, false);
	}
	if (m_IsJoinDenyWin)
	{
		AppContext->SetDisplayUI2D(OBJECT_TYPE_UI2D_JOIN_DENY + m_SceneName, OBJECT_NAME_LOBBY_JOIN_DENY, true);
	}
	else
	{
		AppContext->SetDisplayUI2D(OBJECT_TYPE_UI2D_JOIN_DENY + m_SceneName, OBJECT_NAME_LOBBY_JOIN_DENY, false);
	}

	GraphicsContext::GetApp()->Update2DPosition(AppContext->m_RItemsMap[OBJECT_TYPE_UI2D + m_SceneName], AppContext->m_RItemsVec);
	GraphicsContext::GetApp()->UpdateInstanceData(AppContext->m_RItemsMap[OBJECT_TYPE_UI2D + m_SceneName], AppContext->m_RItemsVec);

	GraphicsContext::GetApp()->Update2DPosition(AppContext->m_RItemsMap[OBJECT_NAME_RULE_MANUAL + m_SceneName], AppContext->m_RItemsVec);
	GraphicsContext::GetApp()->UpdateInstanceData(AppContext->m_RItemsMap[OBJECT_NAME_RULE_MANUAL + m_SceneName], AppContext->m_RItemsVec);

	GraphicsContext::GetApp()->Update2DPosition(AppContext->m_RItemsMap[OBJECT_TYPE_UI2D_ROOMLIST + m_SceneName], AppContext->m_RItemsVec);
	GraphicsContext::GetApp()->UpdateInstanceData(AppContext->m_RItemsMap[OBJECT_TYPE_UI2D_ROOMLIST + m_SceneName], AppContext->m_RItemsVec);

	GraphicsContext::GetApp()->Update2DPosition(AppContext->m_RItemsMap[OBJECT_TYPE_UI2D_ROOMLIST_LIST + m_SceneName], AppContext->m_RItemsVec);
	GraphicsContext::GetApp()->UpdateInstanceData(AppContext->m_RItemsMap[OBJECT_TYPE_UI2D_ROOMLIST_LIST + m_SceneName], AppContext->m_RItemsVec);


	GraphicsContext::GetApp()->Update2DPosition(AppContext->m_RItemsMap[OBJECT_TYPE_UI2D_EXIT_GAME + m_SceneName], AppContext->m_RItemsVec);
	GraphicsContext::GetApp()->UpdateInstanceData(AppContext->m_RItemsMap[OBJECT_TYPE_UI2D_EXIT_GAME + m_SceneName], AppContext->m_RItemsVec);

	GraphicsContext::GetApp()->Update2DPosition(AppContext->m_RItemsMap[OBJECT_TYPE_UI2D_MAKE_ROOM + m_SceneName], AppContext->m_RItemsVec);
	GraphicsContext::GetApp()->UpdateInstanceData(AppContext->m_RItemsMap[OBJECT_TYPE_UI2D_MAKE_ROOM + m_SceneName], AppContext->m_RItemsVec);

	GraphicsContext::GetApp()->Update2DPosition(AppContext->m_RItemsMap[OBJECT_TYPE_UI2D_JOIN_DENY + m_SceneName], AppContext->m_RItemsVec);
	GraphicsContext::GetApp()->UpdateInstanceData(AppContext->m_RItemsMap[OBJECT_TYPE_UI2D_JOIN_DENY + m_SceneName], AppContext->m_RItemsVec);

	//*Shadow*/
	GraphicsContext::GetApp()->UpdateShadowTransform(CACHE_CACHE::GetApp()->m_Lights[LIGHT_NAME_DIRECTIONAL].get(), m_SceneBounds);
	GraphicsContext::GetApp()->UpdateShadowPassCB();


	/*Materials*/
	GraphicsContext::GetApp()->UpdateMaterialBuffer(AssertsReference::GetApp()->m_Materials);
}

void LobbyScene::Render()
{
	/*Props*/
	for (auto& prop : AppContext->m_Maps[MAP_STR_LOBBY]->propTypeVector)
	{
		GraphicsContext::GetApp()->DrawRenderItem(AppContext->m_RItemsMap[prop], AppContext->m_RItemsVec);
	}

	/*Character*/
	GraphicsContext::GetApp()->SetPipelineState(Graphics::g_SkinnedPSO.Get());
	GraphicsContext::GetApp()->DrawRenderItem(AppContext->m_RItemsMap[CHARACTER_WIZARD], AppContext->m_RItemsVec);

	/*UIs*/
	GraphicsContext::GetApp()->SetPipelineState(Graphics::g_UIPSO.Get());

	GraphicsContext::GetApp()->DrawRenderItem(AppContext->m_RItemsMap[OBJECT_TYPE_UI2D + m_SceneName], AppContext->m_RItemsVec);
	if (m_IsRuleManual)
		GraphicsContext::GetApp()->DrawRenderItem(AppContext->m_RItemsMap[OBJECT_NAME_RULE_MANUAL + m_SceneName], AppContext->m_RItemsVec);
	if (m_IsManualMatchRoomLlist) {
		GraphicsContext::GetApp()->DrawRenderItem(AppContext->m_RItemsMap[OBJECT_TYPE_UI2D_ROOMLIST + m_SceneName], AppContext->m_RItemsVec);
		GraphicsContext::GetApp()->DrawRenderItem(AppContext->m_RItemsMap[OBJECT_TYPE_UI2D_ROOMLIST_LIST + m_SceneName], AppContext->m_RItemsVec);
	}

	GraphicsContext::GetApp()->DrawRenderItem(AppContext->m_RItemsMap[OBJECT_TYPE_UI2D_EXIT_GAME + m_SceneName], AppContext->m_RItemsVec);



	GraphicsContext::GetApp()->DrawRenderItem(AppContext->m_RItemsMap[OBJECT_TYPE_UI2D + m_SceneName], AppContext->m_RItemsVec, UI_LAYER_1);
	if (m_IsRuleManual)
		GraphicsContext::GetApp()->DrawRenderItem(AppContext->m_RItemsMap[OBJECT_NAME_RULE_MANUAL + m_SceneName], AppContext->m_RItemsVec, UI_LAYER_1);
	if (m_IsManualMatchRoomLlist) {
		GraphicsContext::GetApp()->DrawRenderItem(AppContext->m_RItemsMap[OBJECT_TYPE_UI2D_ROOMLIST + m_SceneName], AppContext->m_RItemsVec, UI_LAYER_1);
		GraphicsContext::GetApp()->DrawRenderItem(AppContext->m_RItemsMap[OBJECT_TYPE_UI2D_ROOMLIST_LIST + m_SceneName], AppContext->m_RItemsVec, UI_LAYER_1);
		GraphicsContext::GetApp()->DrawRenderItem(AppContext->m_RItemsMap[OBJECT_TYPE_UI2D_MAKE_ROOM + m_SceneName], AppContext->m_RItemsVec, UI_LAYER_1);
	}
	GraphicsContext::GetApp()->DrawRenderItem(AppContext->m_RItemsMap[OBJECT_TYPE_UI2D_EXIT_GAME + m_SceneName], AppContext->m_RItemsVec, UI_LAYER_1);


	GraphicsContext::GetApp()->DrawRenderItem(AppContext->m_RItemsMap[OBJECT_TYPE_UI2D + m_SceneName], AppContext->m_RItemsVec, UI_LAYER_0);
	if (m_IsRuleManual)
		GraphicsContext::GetApp()->DrawRenderItem(AppContext->m_RItemsMap[OBJECT_NAME_RULE_MANUAL + m_SceneName], AppContext->m_RItemsVec, UI_LAYER_0);
	if (m_IsManualMatchRoomLlist) {
		GraphicsContext::GetApp()->DrawRenderItem(AppContext->m_RItemsMap[OBJECT_TYPE_UI2D_ROOMLIST + m_SceneName], AppContext->m_RItemsVec, UI_LAYER_0);
		GraphicsContext::GetApp()->DrawRenderItem(AppContext->m_RItemsMap[OBJECT_TYPE_UI2D_ROOMLIST_LIST + m_SceneName], AppContext->m_RItemsVec, UI_LAYER_0);
		GraphicsContext::GetApp()->DrawRenderItem(AppContext->m_RItemsMap[OBJECT_TYPE_UI2D_MAKE_ROOM + m_SceneName], AppContext->m_RItemsVec, UI_LAYER_0);
	}
	GraphicsContext::GetApp()->DrawRenderItem(AppContext->m_RItemsMap[OBJECT_TYPE_UI2D_EXIT_GAME + m_SceneName], AppContext->m_RItemsVec, UI_LAYER_0);
	if (m_IsJoinDenyWin)
		GraphicsContext::GetApp()->DrawRenderItem(AppContext->m_RItemsMap[OBJECT_TYPE_UI2D_JOIN_DENY + m_SceneName], AppContext->m_RItemsVec, UI_LAYER_0);

	/*Shadow*/
	GraphicsContext::GetApp()->SetResourceShadowPassCB();
	GraphicsContext::GetApp()->SetPipelineState(Graphics::g_ShadowOpaquePSO.Get());

	/*Shadow Props*/
	for (auto& prop : AppContext->m_Maps[MAP_STR_LOBBY]->propTypeVector)
	{
		GraphicsContext::GetApp()->DrawRenderItem(AppContext->m_RItemsMap[prop], AppContext->m_RItemsVec);
	}

	/*Shadow Characters*/
	GraphicsContext::GetApp()->SetPipelineState(Graphics::g_SkinnedShadowOpaquePSO.Get());
	GraphicsContext::GetApp()->DrawRenderItem(AppContext->m_RItemsMap[CHARACTER_WIZARD], AppContext->m_RItemsVec);

	GraphicsContext::GetApp()->ShadowTransitionResourceBarrier();

	ThreadEventHandler();
}

void LobbyScene::RenderUI()
{
	// 프로필 정보
	if (!m_IsRuleManual & !m_IsManualMatchRoomLlist)
	{
		UITextInfo info = GraphicsContext::GetApp()->GetUIPosAndSize(AppContext->m_RItemsMap[OBJECT_TYPE_UI2D + m_SceneName], AppContext->m_RItemsVec, OBJECT_NAME_LOBBY_PRO_BACK);
		GraphicsContext::GetApp()->SetTextSize(info.size.y / 160.f, DWRITE_TEXT_ALIGNMENT_LEADING, D2D1::ColorF::Black);
		float defaultPosX = info.pos.x - (info.size.x / 20.f);

		// GraphicsContext::GetApp()->DrawD2DText(m_JoinDenyWarning, WIDTH_NORMALIZE_UI(defaultPosX + (info.size.x / 20.f) * 0.12f), HEIGHT_NORMALIZE_UI(info.pos.y), Core::g_DisplayWidth, Core::g_DisplayHeight, true);
		// GraphicsContext::GetApp()->DrawD2DText(m_JoinDenyWarning, WIDTH_NORMALIZE_UI(defaultPosX + (info.size.x / 20.f) * 0.12f), HEIGHT_NORMALIZE_UI(-info.pos.y), Core::g_DisplayWidth, Core::g_DisplayHeight, true);

		int winningRate = m_MMRInfo.winning_rate * 100.f;

		GraphicsContext::GetApp()->DrawD2DText(L"Wizrd. " + m_MMRInfo.name, WIDTH_NORMALIZE_UI(defaultPosX + (info.size.x / 20.f) * 0.3f), HEIGHT_NORMALIZE_UI(info.pos.y - (info.size.y / 20.f) * 0.5f), Core::g_DisplayWidth, Core::g_DisplayHeight, true);
		GraphicsContext::GetApp()->DrawD2DText(L"MMR. " + to_wstring(m_MMRInfo.mmr), WIDTH_NORMALIZE_UI(defaultPosX + (info.size.x / 20.f) * 0.3f), HEIGHT_NORMALIZE_UI(info.pos.y - (info.size.y / 20.f) * 0.3f), Core::g_DisplayWidth, Core::g_DisplayHeight, true);
		GraphicsContext::GetApp()->DrawD2DText(L"Catch Count. " + to_wstring(m_MMRInfo.catched_student_count), WIDTH_NORMALIZE_UI(defaultPosX + (info.size.x / 20.f) * 0.3f), HEIGHT_NORMALIZE_UI(info.pos.y - (info.size.y / 20.f) * 0.1f), Core::g_DisplayWidth, Core::g_DisplayHeight, true);
		GraphicsContext::GetApp()->DrawD2DText(L"Winning Rate. " + to_wstring(winningRate) + L"%", WIDTH_NORMALIZE_UI(defaultPosX + (info.size.x / 20.f) * 0.3f), HEIGHT_NORMALIZE_UI(info.pos.y + (info.size.y / 20.f) * 0.1f), Core::g_DisplayWidth, Core::g_DisplayHeight, true);
		// GraphicsContext::GetApp()->DrawD2DText(, WIDTH_NORMALIZE_UI(defaultPosX + (info.size.x / 20.f) * 1.f), HEIGHT_NORMALIZE_UI(info.pos.y + (info.size.y / 20.f) * 0.1f), Core::g_DisplayWidth, Core::g_DisplayHeight, true);
		GraphicsContext::GetApp()->DrawD2DText(L"Total Play Count. " + to_wstring(m_MMRInfo.total_game_count), WIDTH_NORMALIZE_UI(defaultPosX + (info.size.x / 20.f) * 0.28f), HEIGHT_NORMALIZE_UI(info.pos.y + (info.size.y / 20.f) * 0.3f), Core::g_DisplayWidth, Core::g_DisplayHeight, true);
	}

	if (m_IsManualMatchRoomLlist && m_IsGetRoomList)	// room list 보여주기
	{
		string uiName = "LOBBY_ROOMLIST_LIST_";
		int roomListCnt = m_RoomList.size();
		int endNum = 0;

		if (roomListCnt > 7)
		{
			if (m_RoomListPage == m_RoomListMaxPage)
				endNum = roomListCnt % 7; //  == 0 ? 7 : roomListCnt % 7;
			else
				endNum = 7;
		}
		else
			endNum = roomListCnt;
		if (endNum > 0)
		{
			for (int i = m_RoomListPage * 7; i < m_RoomListPage * 7 + endNum; ++i)
			{
				UITextInfo info = GraphicsContext::GetApp()->GetUIPosAndSize(AppContext->m_RItemsMap[OBJECT_TYPE_UI2D_ROOMLIST_LIST + m_SceneName], AppContext->m_RItemsVec, uiName + to_string(i % 7));
				GraphicsContext::GetApp()->SetTextSize(info.size.y / 15.f);
				float defaultPosX = info.pos.x - (info.size.x / 20.f);

				GraphicsContext::GetApp()->DrawD2DText(to_wstring(m_RoomList[i].id), defaultPosX + (info.size.x / 20.f) * 0.15f, -info.pos.y, Core::g_DisplayWidth); // info.pos.x- (info.size.x / 20.f) + (info.size.x / 20.f) * 0.15f

				if (!((m_IsMakeRoom || m_IsJoinDenyWin) && (i % 7 > 1 && i % 7 < 5)))
				{
					GraphicsContext::GetApp()->DrawD2DText(m_RoomList[i].name, defaultPosX + (info.size.x / 10.f) * 0.4f, -(info.pos.y), Core::g_DisplayWidth);
					GraphicsContext::GetApp()->DrawD2DText(L"TOWN", defaultPosX + (info.size.x / 10.f) * 0.725f, -(info.pos.y), Core::g_DisplayWidth);
				}
				GraphicsContext::GetApp()->DrawD2DText(to_wstring(m_RoomList[i].participant) + L"/4", defaultPosX + (info.size.x / 10.f) * 0.925f, -(info.pos.y), Core::g_DisplayWidth);
			}
		}
	}


	if (m_IsJoinDenyWin)
	{
		UITextInfo info = GraphicsContext::GetApp()->GetUIPosAndSize(AppContext->m_RItemsMap[OBJECT_TYPE_UI2D_JOIN_DENY + m_SceneName], AppContext->m_RItemsVec, OBJECT_NAME_LOBBY_JOIN_DENY);
		GraphicsContext::GetApp()->SetTextSize(info.size.y / 80.f, DWRITE_TEXT_ALIGNMENT_LEADING, D2D1::ColorF::LightYellow);
		float defaultPosX = info.pos.x - (info.size.x / 20.f);
		GraphicsContext::GetApp()->DrawD2DText(m_JoinDenyWarning, WIDTH_NORMALIZE_UI(defaultPosX + (info.size.x / 20.f) * 0.12f), HEIGHT_NORMALIZE_UI(info.pos.y), Core::g_DisplayWidth, Core::g_DisplayHeight, true);
	}

	if (m_IsMakeRoom)
	{
		wstring ws = L"";
		UITextInfo info = GraphicsContext::GetApp()->GetUIPosAndSize(AppContext->m_RItemsMap[OBJECT_TYPE_UI2D_MAKE_ROOM + m_SceneName], AppContext->m_RItemsVec, OBJECT_NAME_LOBBY_MAKE_ROOM_WINDOW);
		GraphicsContext::GetApp()->SetTextSize(info.size.y / 120.f, DWRITE_TEXT_ALIGNMENT_LEADING, D2D1::ColorF::LightYellow);

		float defaultPosX = info.pos.x - (info.size.x / 20.f);
		switch (m_RoomTitleWarning)
		{
		case 0:
			GraphicsContext::GetApp()->DrawD2DText(L"Please enter 10 characters or less", WIDTH_NORMALIZE_UI(defaultPosX + (info.size.x / 20.f) * 0.15f), HEIGHT_NORMALIZE_UI(-info.pos.y), Core::g_DisplayWidth, Core::g_DisplayHeight, true);
			break;
		case 1:
			GraphicsContext::GetApp()->DrawD2DText(L"Please enter text!", WIDTH_NORMALIZE_UI(defaultPosX + (info.size.x / 20.f) * 0.15f), HEIGHT_NORMALIZE_UI(-info.pos.y), Core::g_DisplayWidth, Core::g_DisplayHeight, true);
			break;
		case 2:
			GraphicsContext::GetApp()->DrawD2DText(L"More than 10 characters!", WIDTH_NORMALIZE_UI(defaultPosX + (info.size.x / 20.f) * 0.15f), HEIGHT_NORMALIZE_UI(-info.pos.y), Core::g_DisplayWidth, Core::g_DisplayHeight, true);
			break;
		default:
			break;
		}

		if (Core::g_Chating == 2)
		{
			ws = ws + Core::g_ChatBuf + Core::g_TempChatBuf;

			info = GraphicsContext::GetApp()->GetUIPosAndSize(AppContext->m_RItemsMap[OBJECT_TYPE_UI2D_MAKE_ROOM + m_SceneName], AppContext->m_RItemsVec, OBJECT_NAME_LOBBY_MAKE_ROOM_INPUT);
			GraphicsContext::GetApp()->SetTextSize(info.size.y / 20.f, DWRITE_TEXT_ALIGNMENT_LEADING, D2D1::ColorF::White);
			defaultPosX = info.pos.x - (info.size.x / 20.f);
			GraphicsContext::GetApp()->DrawD2DText(ws, WIDTH_NORMALIZE_UI(defaultPosX + (info.size.x / 20.f) * 0.15f), HEIGHT_NORMALIZE_UI(info.pos.y), Core::g_DisplayWidth, Core::g_DisplayHeight, true);
		}
		else // if (Core::g_Chating == 3)
		{
			info = GraphicsContext::GetApp()->GetUIPosAndSize(AppContext->m_RItemsMap[OBJECT_TYPE_UI2D_MAKE_ROOM + m_SceneName], AppContext->m_RItemsVec, OBJECT_NAME_LOBBY_MAKE_ROOM_INPUT);
			GraphicsContext::GetApp()->SetTextSize(info.size.y / 20.f, DWRITE_TEXT_ALIGNMENT_LEADING, D2D1::ColorF::White);
			defaultPosX = info.pos.x - (info.size.x / 20.f);
			GraphicsContext::GetApp()->DrawD2DText(m_RoomTitle, WIDTH_NORMALIZE_UI(defaultPosX + (info.size.x / 20.f) * 0.15f), HEIGHT_NORMALIZE_UI(info.pos.y), Core::g_DisplayWidth, Core::g_DisplayHeight, true);

		}
	}
}

void LobbyScene::ResetRoomListSelect()
{
	string list = "LOBBY_ROOMLIST_LIST_";
	for (int i = 0; i < 7; ++i)
		AppContext->FindObject<UserInterface>(OBJECT_TYPE_UI2D_ROOMLIST_LIST + m_SceneName, list + to_string(i))->Overlap(false);

}

void LobbyScene::ThreadEventHandler()
{
	if (m_IsAutoMatching)
	{
		UserInterface* autoMatching = AppContext->FindObject<UserInterface>(OBJECT_TYPE_UI2D + m_SceneName, OBJECT_NAME_RENEWAL_ARROW);
		autoMatching->PlaySpriteAnimation(GameTimer::GetApp()->DeltaTime(), SPRITE_NAME_AUTOMATCH_LOADING, true);
	}
}

int LobbyScene::GetRoomNumber() const
{
	std::string roomNumber;
	roomNumber.assign(m_RoomNumber.begin(), m_RoomNumber.end());

	return std::atoi(roomNumber.c_str());
}

void LobbyScene::UpdateRoomList()
{
	int roomCnt = m_RoomList.size();

	std::string uiName = "LOBBY_ROOMLIST_LIST_";

	m_RoomListMaxPage = roomCnt / 7;
	m_RoomListCntLastPage = roomCnt % 7;

	if (roomCnt < 8)
	{
		for (int i = 0; i < roomCnt; ++i)
		{
			AppContext->SetPickingUI2D(OBJECT_TYPE_UI2D_ROOMLIST_LIST + m_SceneName, uiName + to_string(i), true);
		}
		for (int i = roomCnt; i < 7; ++i)
		{
			AppContext->SetPickingUI2D(OBJECT_TYPE_UI2D_ROOMLIST_LIST + m_SceneName, uiName + to_string(i), false);
		}
	}
	else
	{
		if (m_RoomListPage == m_RoomListMaxPage)
		{
			if (m_RoomListCntLastPage == 0)
			{
				for (int i = 0; i < 7; ++i)
				{
					AppContext->SetPickingUI2D(OBJECT_TYPE_UI2D_ROOMLIST_LIST + m_SceneName, uiName + to_string(i), true);
				}
			}
			else
			{
				for (int i = 0; i < m_RoomListCntLastPage; ++i)
				{
					AppContext->SetPickingUI2D(OBJECT_TYPE_UI2D_ROOMLIST_LIST + m_SceneName, uiName + to_string(i), true);
				}
				for (int i = m_RoomListCntLastPage; i < 7; ++i)
				{
					AppContext->SetPickingUI2D(OBJECT_TYPE_UI2D_ROOMLIST_LIST + m_SceneName, uiName + to_string(i), false);
				}
			}
		}
		else if (m_RoomListPage < m_RoomListMaxPage)
		{
			// > 버튼 누를 수 있음
			AppContext->FindObject<UserInterface>(OBJECT_TYPE_UI2D_ROOMLIST + m_SceneName, OBJECT_NAME_LOBBY_ROOMLIST_NEXT)->OnActive(true);

			for (int i = 0; i < 7; ++i)
			{
				AppContext->SetPickingUI2D(OBJECT_TYPE_UI2D_ROOMLIST_LIST + m_SceneName, uiName + to_string(i), true);
			}
		}
	}
}


