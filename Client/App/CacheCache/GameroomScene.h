#pragma once
#include "Scene.h"
#include "Network.h"

#define CLIENT_EVENT_GAMEROOM_UI_PRESSED 0xFFFF10
#define GAMEPLAY_MAX_CHAT_COUNT 7

namespace Graphics
{
	extern Microsoft::WRL::ComPtr<ID3D12PipelineState> g_OpacityPSO;
	extern Microsoft::WRL::ComPtr<ID3D12PipelineState> g_SkinnedPSO;
	extern Microsoft::WRL::ComPtr<ID3D12PipelineState> g_SkyPSO;
	extern Microsoft::WRL::ComPtr<ID3D12PipelineState> g_UIPSO;
	extern Microsoft::WRL::ComPtr<ID3D12PipelineState> g_ShadowOpaquePSO;
	extern Microsoft::WRL::ComPtr<ID3D12PipelineState> g_SkinnedShadowOpaquePSO;
	extern Microsoft::WRL::ComPtr<ID3D12PipelineState> g_ParticlePSO;
}

class Character;
class ImageView;
class GameroomScene : public Scene
{
	friend class GameroomController;
	// 아래 구조체는 게임룸에서만 사용
	// BattleClients 쓰레드 동기화 오류를 막기위해
	// 구조체를 새로 생성
	struct SubBattleClient
	{
		SubBattleClient()
		{
			cout << "서브배틀클라이언트가 생성되었습니다." << endl;
		};
		~SubBattleClient()
		{
			cout << "서브배틀클라이언트가 소멸되었습니다." << endl;
		};

		// 플레이어 동기화정보
		int battleID;
		int playerNum; /*몇피인지*/
		int spawnLocation;
		std::string playerMeshName; /*무슨메쉬인지*/
		bool host;
		bool isReady;

		// 플레이어 DB정보
		std::string playerName;
		int mmr = -1;
		int catched_student_count = 0;
		float winning_rate = 0.f;
	};

	struct ServerData
	{
		ServerData() { 
			mapType = -1;
			mapName = "";
		};
		~ServerData() {
			subBattleClients.clear();
		};

		int playerID;

		int mapType;
		std::string mapName;

		// 배틀id / 서브배틀클라이언트
		std::map<int, SubBattleClient> subBattleClients;
	};

	struct ChatData
	{
		wstring name;
		wstring chat;
	};

private:
	virtual void ProcessEvent(int sEvent, int argsCount = 0, ...) override;
	virtual void UIEvent(int sEvent, int argsCount = 0, ...) override;

public:
	virtual void Initialize() override;
	virtual void OnResize() override;

public:
	virtual bool Enter() override;
	virtual void Exit() override;

	virtual void Update(const float& fDeltaTime) override;
	virtual void Render() override;
	virtual void RenderUI() override;

private:
	// BattleClient
	void BuildSubBattleClient(int bcID,bool isMy = false);
	void ResetSubBattleClient();

	// SelectedPlayer ImageView
	ImageView*	FindSelectPlayerImageView(std::string playerMeshName);
	ImageView*	FindReadyStateImageView(int playerNum);
	ImageView*	FindHostImageView(int playerNum);
	void		SetHostImageView();
	void		SetReadyStateImageView();

	void		OnActiveSelectedPlayerImageView(std::string playerMeshName, bool onoff);
	int			OnSelectSelectedPlayerImageView(std::string playerMeshName);
	void		ForcedUnSelectedPlayerImageView(std::string playerMeshName);
	void		SetOtherUnSelectedPlayerImageView(std::string playerMeshName, bool onoff);
	bool		GetOtherUnSelectedPlayerImageView(std::string playerMeshName);
	void		InitSelectPlayerImageView(std::string playerMeshName);

private:
	/* ServerData */
	std::unique_ptr<ServerData> m_ServerData;
	std::list<ChatData> m_ChatData;
};

