#pragma once
#include "GameObject.h"
#include "PlayerController.h"
#include "AnimationController.h"
#include "Picking.h"

/* Chararcter
@ Players (Master / Student)
@ Controller / Animation Parts(GameObject*)
@ Bone(ContantBuffer)가 필요함
@ Character는 CharacterParts를 관리하는 매니저 역할
*/
class Camera;
class CharacterParts;
class Particle;
class Character : public GameObject
{
	friend class PlayerController;
	friend class CharacterParts;

public:
	std::string m_MapName;				/* 캐릭터가 속해있는 맵이름 */
	UINT m_BattleServerID;				/* 배틀서버 아이디*/
	int m_PlayerRole;					/* 플레이어 역할 */
	int m_SpawnLoaction;				/* 스폰위치 */
	std::string m_UserPlayerMeshName;	/* 유저플레이어 메쉬이름 */

public:
	// 캐릭터 상태
	bool m_IsMoving = false;

public:
	explicit Character(std::string type, std::string id);
	virtual ~Character();
	
	virtual void InitializeTransform() override;

	// 업데이트 + 컴포넌트 업데이트
	void Update(const float deltaT);
	void CameraUpdate(const float delteT);
	void SoundUpdate(const float deltaT);
	void BlurUpdate(const float deltaT);

public:
	// 캐릭터 게임 컨텐츠 기능
	bool Transform(std::string meshName, std::string submeshName, UINT matIndex);
	bool ReleaseTransform();

	// 캐릭터 파티클 기능
	void TransformParticle(bool isCapture = false);
	void PenartyParticle(bool onoff);
	void ControlBlurEffect(bool onoff);

public:
	void SetMapName(std::string mapName);

	void SetCamera(Camera* myCamera, CameraType cameraType);
	void SetCamera(CameraType cameraType);
	void SetController(std::string mapName);
	void SetParts(CharacterParts* parts);
	void SetParticle(std::string particleName, std::string instID);
	void SetBlurLifeTime(float blurLifeTime);

	virtual bool	SetMesh(std::string meshName, std::string submeshName) override;
	void			SetMaterial(int materialIndex);

	void SetPosition(float posX, float posY, float posZ);
	void SetPosition(DirectX::XMFLOAT3 xmPos);

	void SetAnimationController(SkinnedModelInstance* skinnedModelInst);
	void SetAnimationPlayerState(AnimationController::PlayerState playerState);
	void SetAnimationKeyState(AnimationController::PlayerState keyState); 

public:
	// 이동 & 회전
	bool			Move(DWORD dwDirection, float fDistance);
	virtual void	Move(DWORD dwDirection, float fDistance, bool bUpdateVelocity) override;
	virtual void	Move(const XMFLOAT3& xmf3Shift, bool bVelocity = false) override;

	virtual void	Rotate(float pitch, float yaw, float roll) override;

private:
	XMFLOAT3	GetThunderBoltDirection(float screenX, float screenY);

	void		PropIntersect(float screenX, float screenY, XMVECTOR& orign, XMVECTOR& direction, string& objName, string& insID);
	float		CharacterIntersect(Character* character, std::string MeshName, float distance , float screenX, float screenY, XMVECTOR& orign, XMVECTOR& direction, string& objName, string& insID,float wizardDis);

	bool		RayMapTriangleIntersect(XMVECTOR orig, XMVECTOR dir, XMVECTOR v0, XMVECTOR v1, XMVECTOR v2, XMVECTOR& P);
	bool		RayObjTriangleIntersect(XMVECTOR orig, XMVECTOR dir, XMVECTOR v0, XMVECTOR v1, XMVECTOR v2, XMVECTOR& P);

	XMFLOAT3	CalculateThunderBoltDirectionUsingObj(XMVECTOR orign, XMVECTOR direction, string objName, string insID);
	XMFLOAT3	CalculateThunderBoltDirectionUsingMap(float screenX, float screenY);

public:
	// 캐릭터 컴포넌트들
	Camera* m_MyCamera;
	std::unique_ptr<PlayerController>		m_PlayerController;
	std::unique_ptr<AnimationController>	m_AnimationController;
	std::map<std::string, CharacterParts*>	m_Parts;
	std::map<std::string, Particle*>		m_Particles;

public:
	// 물체인지 캐릭터인지 판별하는 변수
	TransformType	m_TransformType;
	std::string		m_TransformMeshName;
	bool			m_IsThirdCamera = true;
	// 카툰렌더링 대상
	PickedObject m_ContourObj = {};
	float m_HP = 1.f;		// 유저 HP%

private:
	// 캐릭터전용 메쉬정보
	GeometryMesh*				m_CharacterGeo;
	D3D12_PRIMITIVE_TOPOLOGY	m_CharacterPrimitiveType;
	UINT						m_CharacterIndexCount;
	UINT						m_CharacterStartIndexLocation;
	int							m_CharacterBaseVertexLocation;
	BoundingBox					m_CharacterBounds;

	// 캐릭터전용 재질 인덱스
	UINT m_CharacterMaterialIndex;

	// 이동행렬
	DirectX::XMFLOAT3 m_Position;
	DirectX::XMFLOAT3 m_Right;
	DirectX::XMFLOAT3 m_Up;
	DirectX::XMFLOAT3 m_Look;

private:
	// 사운드 쿨타임 변수
	float m_RunningSoundCoolTime = 0.32f;
	float m_RunningSoundDeltaTime = 0.f;

	// Blur Control Param - 자기자신만 소유하고있다.
	bool		m_CharacterSwitchBlur = false;
	float		m_BlurTotalTime = 0.f;
	float		m_BlurLifeTime = 0.5f;
};

