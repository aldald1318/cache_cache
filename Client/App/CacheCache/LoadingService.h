#pragma once
#include "Singleton.h"

namespace Core
{
	// thread event param
	extern std::condition_variable g_Loading_cv;
	extern bool g_Loading_processed;

	// param for render
	extern Microsoft::WRL::ComPtr<ID2D1DeviceContext2> g_D2dDeviceContext;
	extern Microsoft::WRL::ComPtr<IDWriteFactory> g_DWriteFactory;

	extern int g_DisplayWidth;
	extern int g_DisplayHeight;
}

class LoadingService : public TemplateSingleton<LoadingService>
{
private:
	bool running = true;

public:
	void RunService();
	void StopService();

private:
	void Initialize();
	void Termiante();

private:
	void D2DLoadBitmap(std::string textureName, const wchar_t* path);
	void D2DLoadFont(std::wstring fontName, float fontSize);

	void D2DDrawText(const wstring wtext, float posX, float posY, float rectSize);
	void D2DDrawBitmap(std::string textureName, float posX, float posY, float rectWidth, float rectHeight);

private:
	void D2DDrawSpriteBitmap(float posX, float posY, float rectWidth, float rectHeight);

private:
	Microsoft::WRL::ComPtr<ID2D1SolidColorBrush>	m_TextBrush;
	Microsoft::WRL::ComPtr<IDWriteTextFormat>		m_TextFormat;

	// Textures
	std::map<std::string, Microsoft::WRL::ComPtr< ID2D1Bitmap>>	m_D2dTextures;

private:
	// SpriteAnimation
	std::vector<std::string> m_LoadingSprites;
	int m_FollowPointer = 0;

	float m_SpriteDeltaTime = 0.f;
	float m_SpriteDurationTime = 0.f;
	float m_SpriteAnimationSpeed = 0.f;
};

