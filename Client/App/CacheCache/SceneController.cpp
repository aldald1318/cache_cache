#include "pch.h"
#include "SceneController.h"
#include "InputHandler.h"
#include "SceneManager.h"
#include "Service.h"
#include "Picking.h"
#include "UserInterface.h"

#include "CACHE_CACHE.h"
#include "TitleScene.h"
#include "LobbyScene.h"
#include "GameroomScene.h"
#include "GameplayScene.h"
#include "GameresultScene.h"


TitleController::TitleController(TitleScene* myScene) :
	m_MyScene(myScene)
{
	m_IsInput = 0;
}

void TitleController::Update(const float deltaT)
{
	MouseCallback();
	HandleInput(deltaT);
}

void TitleController::HandleInput(const float deltaT)
{
	if (m_IsInput > 0 & wcslen(Core::g_ChatBuf) > 0)
	{
		if (Core::g_Chating == 3)
		{
			char tmp[256];
			WideCharToMultiByte(CP_ACP, 0, Core::g_ChatBuf, -1, tmp, sizeof(Core::g_ChatBuf), 0, 0);
			string stringChatBuf(tmp);
			wstring wstr = Core::g_ChatBuf;

			memset(Core::g_ChatBuf, 0, sizeof(WCHAR));
			memset(Core::g_TempChatBuf, 0, sizeof(WCHAR));
			Core::g_Chating = 0;

			if (m_IsInput == 1)
			{
				m_MyScene->_id.assign(wstr.begin(), wstr.end());
				m_MyScene->m_ID = L"" + wstr; // m_MyScene->m_ID + wstr;
			}
			else if (m_IsInput == 2)
			{
				m_MyScene->_password.assign(wstr.begin(), wstr.end());
				m_MyScene->m_Password = L"";
				if (m_MyScene->m_IsSignUp)
					m_MyScene->m_Password = m_MyScene->m_Password;
				else
				{
					wstring star;
					for (int i = 0; i < m_MyScene->_password.size(); ++i)
						star = star + L"*";
					m_MyScene->m_Password = m_MyScene->m_Password + star;
				}
			}

			m_IsInput = 0;
		}
	}
}

void TitleController::MouseCallback()
{
	if (InputHandler::g_LeftMouseCallback)
	{
		if (m_MyScene->m_IsMB)
		{
			auto po = Picking::RayIntersect2DZLayer(InputHandler::g_LastMousePos.x, InputHandler::g_LastMousePos.y, m_MyScene->m_SceneName, OBJECT_TYPE_UI2D_TITLE_MB);
			m_PickedUIName = po.instName;
			m_PickedUIObjectName = po.objectName;
		}
		else if (m_MyScene->m_IsSignUp)
		{
			auto po = Picking::RayIntersect2DZLayer(InputHandler::g_LastMousePos.x, InputHandler::g_LastMousePos.y, m_MyScene->m_SceneName, OBJECT_TYPE_UI2D_SIGN_UP);
			m_PickedUIName = po.instName;
			m_PickedUIObjectName = po.objectName;

			if (m_PickedUIName != "") m_MyScene->UIEvent(CLIENT_EVENT_TITLE_UI_PRESSED, 3, m_PickedUIName, true, string(OBJECT_TYPE_UI2D_SIGN_UP));
			else
			{
				auto po = Picking::RayIntersect2DZLayer(InputHandler::g_LastMousePos.x, InputHandler::g_LastMousePos.y, m_MyScene->m_SceneName, OBJECT_TYPE_UI2D_SIGN_UP_INPUT);
				m_PickedUIName = po.instName;
				m_PickedUIObjectName = po.objectName;
				// cout << "Pick 2D UI: " << po.objectName << ", " << po.instName << endl;
				if (m_PickedUIName != "") m_MyScene->UIEvent(CLIENT_EVENT_TITLE_UI_INPUT_PRESSED, 3, m_PickedUIName, true, string(OBJECT_TYPE_UI2D_SIGN_UP_INPUT));
			}
		}
		else
		{
			auto po = Picking::RayIntersect2DZLayer(InputHandler::g_LastMousePos.x, InputHandler::g_LastMousePos.y, m_MyScene->m_SceneName, OBJECT_TYPE_UI2D);
			m_PickedUIName = po.instName;
			m_PickedUIObjectName = po.objectName;
			// cout << "Pick 2D UI: " << po.objectName << ", " << po.instName << endl;

			if (m_PickedUIName != "") m_MyScene->UIEvent(CLIENT_EVENT_TITLE_UI_PRESSED, 3, m_PickedUIName, true, string(OBJECT_TYPE_UI2D));
		}

		InputHandler::g_LeftMouseCallback = false;
	}

	// 각 버튼들에 대한 기능들 UIEvent에서 진행가능함.
	if (InputHandler::g_LeftMouseOverlap == false && m_PickedUIName != "")
	{
		if (m_PickedUIObjectName == OBJECT_TYPE_UI2D + m_MyScene->m_SceneName)
		{
			if (m_PickedUIName == OBJECT_NAME_LOGIN_ID_INPUT)
			{
				m_MyScene->m_ID = L"";
				m_MyScene->m_ChatType = 1;
				if (!m_IsInput)
					Core::g_Chating = 2;
				else if (m_IsInput == 2)
				{
					if (CheckPW())
					{
						Core::g_Chating = 2;
					}
					else
					{
						Core::g_Chating = 2;
					}
				}
				else
				{
					Core::g_Chating = 2;
					m_MyScene->_id = "";
					m_MyScene->m_ID = L"";
					memset(Core::g_ChatBuf, 0, sizeof(WCHAR));
					memset(Core::g_TempChatBuf, 0, sizeof(WCHAR));
				}
				m_IsInput = 1;
			}
			else if (m_PickedUIName == OBJECT_NAME_LOGIN_PW_INPUT)
			{
				m_MyScene->m_Password = L"";
				m_MyScene->m_ChatType = 2;
				if (!m_IsInput)
					Core::g_Chating = 2;
				else if (m_IsInput == 1)
				{
					if (CheckID())
					{
						Core::g_Chating = 2;
					}
					else
					{
						Core::g_Chating = 2;
						m_MyScene->_password = "";
						m_MyScene->m_Password = L"";
						memset(Core::g_ChatBuf, 0, sizeof(WCHAR));
						memset(Core::g_TempChatBuf, 0, sizeof(WCHAR));

					}
				}
				else
				{
					Core::g_Chating = 2;
					m_MyScene->_password = "";
					m_MyScene->m_Password = L"";
					memset(Core::g_ChatBuf, 0, sizeof(WCHAR));
					memset(Core::g_TempChatBuf, 0, sizeof(WCHAR));
				}
				m_IsInput = 2;
			}
			else if (m_PickedUIName == OBJECT_NAME_SIGN_IN)
			{
				wstring wstr = Core::g_ChatBuf;

				memset(Core::g_ChatBuf, 0, sizeof(WCHAR));
				memset(Core::g_TempChatBuf, 0, sizeof(WCHAR));
				Core::g_Chating = 0;

				if (m_IsInput == 1)
				{
					m_MyScene->_id.assign(wstr.begin(), wstr.end());
					// cout << "id save: " << m_MyScene->_id << endl;
					m_MyScene->m_ID = L"" + wstr; // m_MyScene->m_ID + wstr;
				}
				else if (m_IsInput == 2)
				{
					m_MyScene->_password.assign(wstr.begin(), wstr.end());
					// cout << "password save: " << m_MyScene->_password << endl;
					m_MyScene->m_Password = wstr;
				}

				m_IsInput = 0;

				// cout << m_MyScene->_id << ", " << m_MyScene->_password << endl;
				Service::GetApp()->Notify(EVENT_TITLE_LOGIN_REQUEST, 2, m_MyScene->_id, m_MyScene->_password);
				m_MyScene->m_ID = L"";
				m_MyScene->m_Password = L"";
			}
			else if (m_PickedUIName == OBJECT_NAME_SIGN_UP)
			{
				memset(Core::g_ChatBuf, 0, sizeof(WCHAR));
				memset(Core::g_TempChatBuf, 0, sizeof(WCHAR));
				m_IsInput = 0;
				m_MyScene->m_IsSignUp = true;
				m_MyScene->m_ID = L"";
				m_MyScene->m_Password = L"";
				m_MyScene->_id = "";
				m_MyScene->_password = "";
			}
			if (m_PickedUIName != "")
				m_MyScene->UIEvent(CLIENT_EVENT_TITLE_UI_PRESSED, 3, m_PickedUIName, false, string(OBJECT_TYPE_UI2D));
		}
		else if (m_PickedUIObjectName == OBJECT_TYPE_UI2D_TITLE_MB + m_MyScene->m_SceneName)
		{
			if (m_PickedUIName == OBJECT_NAME_TITLE_MB)
			{
				m_MyScene->m_IsMB = false;
				m_MyScene->m_MBMainMessage = L"";
				m_MyScene->m_MBSubMessage = L"";
			}
		}
		else if (m_PickedUIObjectName == OBJECT_TYPE_UI2D_SIGN_UP + m_MyScene->m_SceneName)
		{
			if (m_PickedUIName == OBJECT_NAME_SIGN_UP_CANCEL)
			{
				memset(Core::g_ChatBuf, 0, sizeof(WCHAR));
				memset(Core::g_TempChatBuf, 0, sizeof(WCHAR));
				Core::g_Chating = 1;
				m_IsInput = 0;

				m_MyScene->UIEvent(CLIENT_EVENT_TITLE_UI_PRESSED, 3, string(OBJECT_NAME_SIGN_UP_ID_INPUT), false, string(OBJECT_TYPE_UI2D_SIGN_UP_INPUT));
				m_MyScene->UIEvent(CLIENT_EVENT_TITLE_UI_PRESSED, 3, string(OBJECT_NAME_SIGN_UP_PW_INPUT), false, string(OBJECT_TYPE_UI2D_SIGN_UP_INPUT));

				m_MyScene->m_IsSignUp = false;
				m_MyScene->m_ID = L"";
				m_MyScene->m_Password = L"";
				m_MyScene->_id = "";
				m_MyScene->_password = "";
			}
			else if (m_PickedUIName == OBJECT_NAME_SIGN_UP_CREATE)
			{
				wstring wstr = Core::g_ChatBuf;
				memset(Core::g_ChatBuf, 0, sizeof(WCHAR));
				memset(Core::g_TempChatBuf, 0, sizeof(WCHAR));
				Core::g_Chating = 1;

				if (m_IsInput == 1)
				{
					m_MyScene->_id.assign(wstr.begin(), wstr.end());
					// cout << "id save: " << m_MyScene->_id << endl;
				}
				else if (m_IsInput == 2)
				{
					m_MyScene->_password.assign(wstr.begin(), wstr.end());
					// cout << "password save: " << m_MyScene->_password << endl;
				}
				m_IsInput = 0;

				Service::GetApp()->Notify(EVENT_TITLE_SIGNUP_REQUEST, 2, m_MyScene->_id, m_MyScene->_password);

				Core::g_Chating = 0;
				m_MyScene->m_IsSignUp = false;
				m_MyScene->m_ID = L"";
				m_MyScene->m_Password = L"";
				m_MyScene->_id = "";
				m_MyScene->_password = "";
			}

			if (m_PickedUIName != "")
				m_MyScene->UIEvent(CLIENT_EVENT_TITLE_UI_PRESSED, 3, m_PickedUIName, false, string(OBJECT_TYPE_UI2D_SIGN_UP));
		}
		else if (m_PickedUIObjectName == OBJECT_TYPE_UI2D_SIGN_UP_INPUT + m_MyScene->m_SceneName)
		{
			if (m_PickedUIName == OBJECT_NAME_SIGN_UP_ID_INPUT)
			{
				m_MyScene->m_ID = L"";
				m_MyScene->m_ChatType = 1;
				if (!m_IsInput)
					Core::g_Chating = 2;
				else if (m_IsInput == 2)
				{
					if (CheckPW())
					{
						m_IsInput = 1;
						Core::g_Chating = 2;
					}
				}
				else
				{
					Core::g_Chating = 2;
					m_MyScene->_password = "";
					m_MyScene->m_Password = L"";
					memset(Core::g_ChatBuf, 0, sizeof(WCHAR));
					memset(Core::g_TempChatBuf, 0, sizeof(WCHAR));
				}
				m_IsInput = 1;

			}
			else if (m_PickedUIName == OBJECT_NAME_SIGN_UP_PW_INPUT)
			{
				m_MyScene->m_Password = L"";
				m_MyScene->m_ChatType = 2;
				if (!m_IsInput)
					Core::g_Chating = 2;
				else if (m_IsInput == 1)
				{
					if (CheckID())
					{
						m_IsInput = 2;
						Core::g_Chating = 2;
					}
				}
				else
				{
					Core::g_Chating = 2;
					m_MyScene->_password = "";
					m_MyScene->m_Password = L"";
					memset(Core::g_ChatBuf, 0, sizeof(WCHAR));
					memset(Core::g_TempChatBuf, 0, sizeof(WCHAR));
				}
				m_IsInput = 2;
			}

		}
		m_PickedUIName = "";
	}
}

bool TitleController::CheckID()
{
	char tmp[256];
	WideCharToMultiByte(CP_ACP, 0, Core::g_ChatBuf, -1, tmp, sizeof(Core::g_ChatBuf), 0, 0);
	string stringChatBuf(tmp);
	wstring wstr = Core::g_ChatBuf;

	memset(Core::g_ChatBuf, 0, sizeof(WCHAR));
	memset(Core::g_TempChatBuf, 0, sizeof(WCHAR));
	Core::g_Chating = 1;


	if (wstr.length() > 10)
	{
		// cout << "more 10 char" << endl;
		return false;
	}
	else if (wstr.length() < 1)
	{
		// cout << "less 0 char" << endl;
		return false;
	}

	m_MyScene->_id.assign(wstr.begin(), wstr.end());
	// cout << "id save: " << m_MyScene->_id << endl;
	m_MyScene->m_ID = L"" + wstr; // m_MyScene->m_ID + wstr;


	return true;
}

bool TitleController::CheckPW()
{
	char tmp[256];
	WideCharToMultiByte(CP_ACP, 0, Core::g_ChatBuf, -1, tmp, sizeof(Core::g_ChatBuf), 0, 0);
	string stringChatBuf(tmp);
	wstring wstr = Core::g_ChatBuf;

	memset(Core::g_ChatBuf, 0, sizeof(WCHAR));
	memset(Core::g_TempChatBuf, 0, sizeof(WCHAR));
	Core::g_Chating = 1;


	if (wstr.length() > 10)
	{
		// cout << "more 10 char" << endl;
		return false;
	}
	else if (wstr.length() < 1)
	{
		// cout << "less 0 char" << endl;
		return false;
	}

	m_MyScene->_password.assign(wstr.begin(), wstr.end());
	// cout << "password save: " << m_MyScene->_password << endl;
	m_MyScene->m_Password = L"";
	wstring star;
	for (int i = 0; i < m_MyScene->_password.size(); ++i)
		star = star + L"*";
	m_MyScene->m_Password = m_MyScene->m_Password + star;

	return true;
}

LobbyController::LobbyController(LobbyScene* myScene) :
	m_MyScene(myScene)
{
}

void LobbyController::Update(const float deltaT)
{
	MouseCallback();
	HandleInput(deltaT);
}

void LobbyController::HandleInput(const float deltaT)
{
	//////////////////////////////////////////////////////////////
	if (InputHandler::GetInputStringState())
	{
		m_MyScene->UIEvent(CLIENT_EVENT_LOBBY_INPUT_STRING, 1, InputHandler::GetWString());
	}

	// text input
	if (wcslen(Core::g_ChatBuf) > 0)
	{
		if (Core::g_Chating == 3)
		{
			wstring wstr = Core::g_ChatBuf;
			memset(Core::g_ChatBuf, 0, sizeof(WCHAR));
			memset(Core::g_TempChatBuf, 0, sizeof(WCHAR));
			Core::g_Chating = 0;

			char tmp[256];
			WideCharToMultiByte(CP_ACP, 0, wstr.c_str(), -1, tmp, sizeof(wstr.c_str()), 0, 0);
			string stringChatBuf(tmp);
			m_MyScene->m_RoomTitleString = stringChatBuf;
			m_MyScene->m_RoomTitle = wstr;
		}
	}
}

void LobbyController::MouseCallback()
{
	if (InputHandler::g_LeftMouseCallback)
	{
		PickedObject po;
		if (m_MyScene->m_IsRuleManual)
		{
			po = Picking::RayIntersect2DZLayer(InputHandler::g_LastMousePos.x, InputHandler::g_LastMousePos.y, m_MyScene->m_SceneName, OBJECT_NAME_RULE_MANUAL);
			m_PickedUIName = po.instName;
			m_PickedUIObjectName = po.objectName;

			if (m_PickedUIName != "") m_MyScene->UIEvent(CLIENT_EVENT_LOBBY_UI_PRESSED, 3, m_PickedUIName, true, string(OBJECT_NAME_RULE_MANUAL));
		}
		else if (m_MyScene->m_IsJoinDenyWin)
		{
			po = Picking::RayIntersect2DZLayer(InputHandler::g_LastMousePos.x, InputHandler::g_LastMousePos.y, m_MyScene->m_SceneName, OBJECT_TYPE_UI2D_JOIN_DENY);
			m_PickedUIName = po.instName;
			m_PickedUIObjectName = po.objectName;
		}
		else if (m_MyScene->m_IsMakeRoom)
		{
			po = Picking::RayIntersect2DZLayer(InputHandler::g_LastMousePos.x, InputHandler::g_LastMousePos.y, m_MyScene->m_SceneName, OBJECT_TYPE_UI2D_MAKE_ROOM);
			m_PickedUIName = po.instName;
			m_PickedUIObjectName = po.objectName;

			if (m_PickedUIName != "") m_MyScene->UIEvent(CLIENT_EVENT_LOBBY_UI_PRESSED, 3, m_PickedUIName, true, string(OBJECT_TYPE_UI2D_MAKE_ROOM));
		}
		else if (m_MyScene->m_IsManualMatchRoomLlist)
		{
			po = Picking::RayIntersect2DZLayer(InputHandler::g_LastMousePos.x, InputHandler::g_LastMousePos.y, m_MyScene->m_SceneName, OBJECT_TYPE_UI2D_ROOMLIST);
			m_PickedUIName = po.instName;
			m_PickedUIObjectName = po.objectName;

			if (m_PickedUIName != "")
				m_MyScene->UIEvent(CLIENT_EVENT_LOBBY_UI_PRESSED, 3, m_PickedUIName, true, string(OBJECT_TYPE_UI2D_ROOMLIST));
			//cout << "Pick 2D UI: " << po.objectName << ", " << po.instName << endl;

			if (m_PickedUIName == "")
			{
				po = Picking::RayIntersect2DZLayer(InputHandler::g_LastMousePos.x, InputHandler::g_LastMousePos.y, m_MyScene->m_SceneName, OBJECT_TYPE_UI2D_ROOMLIST_LIST);
				m_PickedUIName = po.instName;
				m_PickedUIObjectName = po.objectName;

				//cout << "Pick 2D UI: " << po.objectName << ", " << po.instName << endl;
			}
		}
		else if (m_MyScene->m_IsExitGame)
		{
			po = Picking::RayIntersect2DZLayer(InputHandler::g_LastMousePos.x, InputHandler::g_LastMousePos.y, m_MyScene->m_SceneName, OBJECT_TYPE_UI2D_EXIT_GAME);
			m_PickedUIName = po.instName;
			m_PickedUIObjectName = po.objectName;

			if (m_PickedUIName != "") m_MyScene->UIEvent(CLIENT_EVENT_LOBBY_UI_PRESSED, 3, m_PickedUIName, true, string(OBJECT_TYPE_UI2D_EXIT_GAME));
			//cout << "Pick 2D UI: " << po.objectName << ", " << po.instName << endl;
		}
		else
		{
			po = Picking::RayIntersect2DZLayer(InputHandler::g_LastMousePos.x, InputHandler::g_LastMousePos.y, m_MyScene->m_SceneName);
			m_PickedUIName = po.instName;
			m_PickedUIObjectName = po.objectName;

			if (m_PickedUIName != "") m_MyScene->UIEvent(CLIENT_EVENT_LOBBY_UI_PRESSED, 3, m_PickedUIName, true, string(OBJECT_TYPE_UI2D));
			//cout << "Pick 2D UI: " << po.objectName << ", " << po.instName << endl;
		}

		InputHandler::g_LeftMouseCallback = false;
	}

	if (InputHandler::g_LeftMouseOverlap == false)
	{
		if (m_PickedUIObjectName == OBJECT_TYPE_UI2D_ROOMLIST + m_MyScene->m_SceneName)
		{
			if (m_PickedUIName == OBJECT_NAME_BUTTON_BACK)
			{
				m_MyScene->m_IsManualMatchRoomLlist = false;
				m_MyScene->m_RoomListPage = 0;
				m_MyScene->m_RoomListMaxPage = 0;
				m_MyScene->m_RoomListCntLastPage = 0;
				m_MyScene->m_SelectRoom = -1;

				m_MyScene->ResetRoomListSelect();
			}
			else if (m_PickedUIName == OBJECT_NAME_LOBBY_JOIN_ROOM)
			{
				if (m_MyScene->m_SelectRoom != -1)
					Service::GetApp()->Notify(EVENT_LOBBY_MANUALMATCH_ROOM_JOIN, 1, m_MyScene->m_SelectRoom);
			}
			else if (m_PickedUIName == OBJECT_NAME_LOBBY_MAKE_ROOM)
			{
				m_MyScene->m_IsMakeRoom = true;
			}
			else if (m_PickedUIName == OBJECT_NAME_LOBBY_ROOMLIST_BTN_RENEWAL)
			{
				Service::GetApp()->Notify(EVENT_LOBBY_MANUALMATCH_ROOMLIST_REQUEST);
				m_MyScene->m_RoomListPage = 0;
				m_MyScene->m_RoomListMaxPage = 0;
				m_MyScene->m_RoomListCntLastPage = 0;
				m_MyScene->m_SelectRoom = -1;

				m_MyScene->ResetRoomListSelect();
				m_MyScene->UIEvent(CLIENT_EVENT_LOBBY_UI_ROOMLIST_PAGE_PREV, 1, false);
				if (m_MyScene->m_RoomListMaxPage > m_MyScene->m_RoomListPage)
					m_MyScene->UIEvent(CLIENT_EVENT_LOBBY_UI_ROOMLIST_PAGE_NEXT, 1, true);
				else
					m_MyScene->UIEvent(CLIENT_EVENT_LOBBY_UI_ROOMLIST_PAGE_NEXT, 1, false);
			}
			else if (m_PickedUIName == OBJECT_NAME_LOBBY_ROOMLIST_NEXT)
			{
				if (m_MyScene->m_RoomListMaxPage > m_MyScene->m_RoomListPage)
					m_MyScene->m_RoomListPage++;
				// cout << m_MyScene->m_RoomListMaxPage << ", " << m_MyScene->m_RoomListPage << endl;
				if (m_MyScene->m_RoomListMaxPage == m_MyScene->m_RoomListPage)
				{
					m_MyScene->UIEvent(CLIENT_EVENT_LOBBY_UI_ROOMLIST_PAGE_PREV, 1, true);
					m_MyScene->UIEvent(CLIENT_EVENT_LOBBY_UI_ROOMLIST_PAGE_NEXT, 1, false);
				}
				else
				{
					m_MyScene->UIEvent(CLIENT_EVENT_LOBBY_UI_ROOMLIST_PAGE_PREV, 1, true);
					m_MyScene->UIEvent(CLIENT_EVENT_LOBBY_UI_ROOMLIST_PAGE_NEXT, 1, true);
				}
			}
			else if (m_PickedUIName == OBJECT_NAME_LOBBY_ROOMLIST_PREV)
			{
				if (m_MyScene->m_RoomListPage > 0)
					m_MyScene->m_RoomListPage--;
				if (m_MyScene->m_RoomListPage == 0)
				{
					m_MyScene->UIEvent(CLIENT_EVENT_LOBBY_UI_ROOMLIST_PAGE_PREV, 1, false);
				}
				else if (m_MyScene->m_RoomListPage > 0)
				{
					m_MyScene->UIEvent(CLIENT_EVENT_LOBBY_UI_ROOMLIST_PAGE_PREV, 1, true);
				}
				if (m_MyScene->m_RoomListMaxPage > m_MyScene->m_RoomListPage)
				{
					m_MyScene->UIEvent(CLIENT_EVENT_LOBBY_UI_ROOMLIST_PAGE_NEXT, 1, true);
				}
			}

			if (m_PickedUIName != "")
				m_MyScene->UIEvent(CLIENT_EVENT_LOBBY_UI_PRESSED, 3, m_PickedUIName, false, string(OBJECT_TYPE_UI2D_ROOMLIST));

		}
		else if (m_PickedUIObjectName == OBJECT_TYPE_UI2D_JOIN_DENY + m_MyScene->m_SceneName)
		{
			if (m_PickedUIName == OBJECT_NAME_LOBBY_JOIN_DENY)
				m_MyScene->m_IsJoinDenyWin = false;
		}
		else if (m_PickedUIObjectName == OBJECT_TYPE_UI2D_ROOMLIST_LIST + m_MyScene->m_SceneName)
		{
			if (m_PickedUIName == OBJECT_NAME_LOBBY_ROOMLIST_0)
				m_MyScene->m_SelectRoom = m_MyScene->m_RoomList[m_MyScene->m_RoomListPage * 7 + 0].id;
			else if (m_PickedUIName == OBJECT_NAME_LOBBY_ROOMLIST_1)
				m_MyScene->m_SelectRoom = m_MyScene->m_RoomList[m_MyScene->m_RoomListPage * 7 + 1].id;
			else if (m_PickedUIName == OBJECT_NAME_LOBBY_ROOMLIST_2)
				m_MyScene->m_SelectRoom = m_MyScene->m_RoomList[m_MyScene->m_RoomListPage * 7 + 2].id;
			else if (m_PickedUIName == OBJECT_NAME_LOBBY_ROOMLIST_3)
				m_MyScene->m_SelectRoom = m_MyScene->m_RoomList[m_MyScene->m_RoomListPage * 7 + 3].id;
			else if (m_PickedUIName == OBJECT_NAME_LOBBY_ROOMLIST_4)
				m_MyScene->m_SelectRoom = m_MyScene->m_RoomList[m_MyScene->m_RoomListPage * 7 + 4].id;
			else if (m_PickedUIName == OBJECT_NAME_LOBBY_ROOMLIST_5)
				m_MyScene->m_SelectRoom = m_MyScene->m_RoomList[m_MyScene->m_RoomListPage * 7 + 5].id;
			else if (m_PickedUIName == OBJECT_NAME_LOBBY_ROOMLIST_6)
				m_MyScene->m_SelectRoom = m_MyScene->m_RoomList[m_MyScene->m_RoomListPage * 7 + 6].id;


			if (m_PickedUIName != "")
				m_MyScene->UIEvent(CLIENT_EVENT_LOBBY_UI_ROOMLIST_PRESSED, 1, m_PickedUIName);
		}
		else if (m_PickedUIObjectName == OBJECT_NAME_RULE_MANUAL + m_MyScene->m_SceneName)
		{
			if (m_PickedUIName == OBJECT_NAME_X)
				m_MyScene->m_IsRuleManual = false;

			if (m_PickedUIName != "")
				m_MyScene->UIEvent(CLIENT_EVENT_LOBBY_UI_PRESSED, 3, m_PickedUIName, false, string(OBJECT_NAME_RULE_MANUAL));
		}
		else if (m_PickedUIObjectName == OBJECT_TYPE_UI2D_EXIT_GAME + m_MyScene->m_SceneName)
		{
			if (m_PickedUIName == OBJECT_NAME_YES)
				CACHE_CACHE::GetApp()->Exit();
			if (m_PickedUIName == OBJECT_NAME_NO)
				m_MyScene->m_IsExitGame = false;
			if (m_PickedUIName != "")
				m_MyScene->UIEvent(CLIENT_EVENT_LOBBY_UI_PRESSED, 3, m_PickedUIName, false, string(OBJECT_TYPE_UI2D_EXIT_GAME));
		}
		else if (m_PickedUIObjectName == OBJECT_TYPE_UI2D_MAKE_ROOM + m_MyScene->m_SceneName)
		{
			if (m_PickedUIName == OBJECT_NAME_LOBBY_MAKE_ROOM_INPUT)
			{
				if (Core::g_InputSwitch) //  && (!Core::g_Chating))
				{
					// 채팅 입력을 할 수 있음
					// Core::g_Chating = 1;
					// cout << "g_Chating on" << endl;
					Core::g_Chating = 2;
					// cout << "채팅 입력 가능" << endl;
				}
			}
			else if (m_PickedUIName == OBJECT_NAME_BTN_CANCEL)
			{
				m_MyScene->m_IsMakeRoom = false;
				m_MyScene->m_RoomTitle = L"";
				m_MyScene->m_RoomTitleString = "";
				m_MyScene->m_RoomTitleWarning = 0;

				memset(Core::g_ChatBuf, 0, sizeof(WCHAR));
				memset(Core::g_TempChatBuf, 0, sizeof(WCHAR));
			}
			else if (m_PickedUIName == OBJECT_NAME_BTN_CREATE)
			{
				if (CheckRoomName())
				{
					Service::GetApp()->Notify(EVENT_LOBBY_MANUALMATCH_ROOM_MAKE, 1, m_MyScene->m_RoomTitle);
				}
			}

			if (m_PickedUIName != "")
			{
				m_MyScene->UIEvent(CLIENT_EVENT_LOBBY_UI_PRESSED, 3, m_PickedUIName, false, string(OBJECT_TYPE_UI2D_MAKE_ROOM));
				m_MyScene->m_RoomTitle = L"";
				m_MyScene->m_RoomTitleString = "";
			}
		}
		else
		{
			if (m_PickedUIName == OBJECT_NAME_MANUALMATCH)
			{
				m_MyScene->m_IsManualMatchRoomLlist = true;
#ifdef DEBUG_SERVER
				Service::GetApp()->Notify(EVENT_LOBBY_MANUALMATCH_REQUEST);
#endif // DEBUG_SERVER
			}
			else if (m_PickedUIName == OBJECT_NAME_AUTOMATCH)
			{
				// automatch 버튼을 눌렀을때 딱 한번만 호출되게
				m_MyScene->UIEvent(CLIENT_EVENT_AUTOMATCH_START);
				Service::GetApp()->Notify(EVENT_LOBBY_AUTOMATCH_REQUEST);
			}
			else if (m_PickedUIName == OBJECT_NAME_RENEWAL_BACK)
			{
				// cout << "AUTOMATCH CANCEL" << endl;
				m_MyScene->UIEvent(CLIENT_EVENT_AUTOMATCH_CANCEL);
				Service::GetApp()->Notify(EVENT_LOBBY_AUTOMATCH_CANCEL_REQUEST);
			}
			else if (m_PickedUIName == OBJECT_NAME_RULE)
			{
				if (!(m_MyScene->m_IsRuleManual))
					m_MyScene->m_IsRuleManual = true;
			}
			else if (m_PickedUIName == OBJECT_NAME_EXIT)
			{
				if (!(m_MyScene->m_IsExitGame))
					m_MyScene->m_IsExitGame = true;
			}

			if (m_PickedUIName != "")
				m_MyScene->UIEvent(CLIENT_EVENT_LOBBY_UI_PRESSED, 3, m_PickedUIName, false, string(OBJECT_TYPE_UI2D));
		}

		m_PickedUIName = "";
	}
}

bool LobbyController::CheckRoomName()
{
	// 멀티 바이트로 바꿔주는 부분
	if (wcslen(Core::g_ChatBuf) > 0)
	{
		if (Core::g_Chating == 2)
		{
			wstring wstr = Core::g_ChatBuf;
			memset(Core::g_ChatBuf, 0, sizeof(WCHAR));
			memset(Core::g_TempChatBuf, 0, sizeof(WCHAR));
			Core::g_Chating = 0;

			char tmp[256];
			WideCharToMultiByte(CP_ACP, 0, wstr.c_str(), -1, tmp, sizeof(wstr.c_str()), 0, 0);
			string stringChatBuf(tmp);
			m_MyScene->m_RoomTitleString = stringChatBuf;
			m_MyScene->m_RoomTitle = wstr;

		}
	}

	// 입력을 안했거나 10글자가 넘은 것을 걸러주는 부분
	wstring title = m_MyScene->m_RoomTitle;
	if (title.length() == 0)
	{
		// cout << "title len < 0" << endl;
		m_MyScene->m_RoomTitleWarning = 1;
		return false;
	}
	else if (title.length() > 10)
	{
		// cout << "title len > 10 " << endl;
		m_MyScene->m_RoomTitleWarning = 2;
		return false;
	}

	return true;
}

GameroomController::GameroomController(GameroomScene* myScene) :
	m_MyScene(myScene)
{
}

void GameroomController::Update(const float deltaT)
{
	MouseCallback();
	HandleInput(deltaT);
}

void GameroomController::HandleInput(const float deltaT)
{
	// TEST
	// if (InputHandler::IsOverlap(VK_UP))
	// {
	// 	CACHE_CACHE::GetApp()->m_Lights[LIGHT_NAME_DIRECTIONAL]->Direction.x += 0.01f;
	// 	cout << CACHE_CACHE::GetApp()->m_Lights[LIGHT_NAME_DIRECTIONAL]->Direction.x << endl;
	// }
	// if (InputHandler::IsOverlap(VK_DOWN))
	// {
	// 	CACHE_CACHE::GetApp()->m_Lights[LIGHT_NAME_DIRECTIONAL]->Direction.x -= 0.01f;
	// 	cout << CACHE_CACHE::GetApp()->m_Lights[LIGHT_NAME_DIRECTIONAL]->Direction.x << endl;
	// }

	if (InputHandler::IsKeyDown(VK_RETURN))
	{
		if (Core::g_InputSwitch && !Core::g_Chating)
		{
			// 채팅 입력을 할 수 있음
			Core::g_Chating = 1;
			// cout << "g_Chating on" << endl;
		}
	}

	else if (wcslen(Core::g_ChatBuf) > 0)
	{
		if (Core::g_Chating == 3)
		{
			// cout << "서버 전송" << endl;
			char tmp[256];
			WideCharToMultiByte(CP_ACP, 0, Core::g_ChatBuf, -1, tmp, sizeof(Core::g_ChatBuf), 0, 0);
			string stringChatBuf(tmp);
			Service::GetApp()->Notify(EVENT_GAMEROOM_SEND_CHAT, 1, Core::g_ChatBuf);
			memset(Core::g_ChatBuf, 0, sizeof(WCHAR));
			Core::g_Chating = 1;

			WCHAR wc[256];
			int nLen = MultiByteToWideChar(CP_ACP, 0, stringChatBuf.c_str(), stringChatBuf.size(), NULL, NULL);
			MultiByteToWideChar(CP_ACP, 0, stringChatBuf.c_str(), stringChatBuf.size(), wc, nLen);

			int myID = m_MyScene->m_ServerData->playerID;
			wc[nLen] = 0;
			m_MyScene->m_ChatData.push_back({ Service::GetApp()->GetBattleClient(myID)->GetName(), wc });
			if (m_MyScene->m_ChatData.size() > GAMEPLAY_MAX_CHAT_COUNT)
				m_MyScene->m_ChatData.pop_front();
		}
	}
}

void GameroomController::MouseCallback()
{
	if (InputHandler::g_LeftMouseCallback)
	{
		PickedObject po = Picking::RayIntersect(InputHandler::g_LastMousePos.x, InputHandler::g_LastMousePos.y, m_MapName);

		// UI Pressed true로 변경
		m_PickedUIName = po.instName;
		if (m_PickedUIName != "") m_MyScene->UIEvent(CLIENT_EVENT_GAMEROOM_UI_PRESSED, 2, m_PickedUIName, true);

		// UI Event
			// Scene에게 명령
			// Service에게 명령
		// Select Player (1p: Druid / 2p: Baird / 3p: Female_Peasant / 4p: Male_Peasant / 5p: Sorcerer
		if (po.instName == UI_IMAGEVIEW_SELECT_P1)
		{
			// 체크 됬는지 안됬는지 bool값을 리턴하는 함수제작
			// 이에 따라 SELECT_CHARACTER / UNSELECT 일지 결정
			std::string characterMeshType = CHARACTER_DRUID;
			m_MyScene->UIEvent(EVENT_GAMEROOM_INPUT_SELECT_CHARACTER, 1, characterMeshType);
		}
		else if (po.instName == UI_IMAGEVIEW_SELECT_P2)
		{
			std::string characterMeshType = CHARACTER_BAIRD;
			m_MyScene->UIEvent(EVENT_GAMEROOM_INPUT_SELECT_CHARACTER, 1, characterMeshType);
		}
		else if (po.instName == UI_IMAGEVIEW_SELECT_P3)
		{
			std::string characterMeshType = CHARACTER_FEMALE_PEASANT;
			m_MyScene->UIEvent(EVENT_GAMEROOM_INPUT_SELECT_CHARACTER, 1, characterMeshType);
		}
		else if (po.instName == UI_IMAGEVIEW_SELECT_P4)
		{
			std::string characterMeshType = CHARACTER_MALE_PEASANT;
			m_MyScene->UIEvent(EVENT_GAMEROOM_INPUT_SELECT_CHARACTER, 1, characterMeshType);
		}
		else if (po.instName == UI_IMAGEVIEW_SELECT_P5)
		{
			std::string characterMeshType = CHARACTER_SORCERER;
			m_MyScene->UIEvent(EVENT_GAMEROOM_INPUT_SELECT_CHARACTER, 1, characterMeshType);
		}
		else if (po.instName == UI_BUTTON_EXIT)
		{
			// 이전 씬으로 이동
			m_MyScene->ResetSubBattleClient();
			Service::GetApp()->Notify(EVENT_GAMEROOM_EXIT);
			SceneManager::GetApp()->ChangeScene(SceneType::Lobby);
		}
		else if (po.instName == UI_BUTTON_PREVMAP)
		{
			// cout << "PREV MAP" << endl;
			m_MyScene->UIEvent(EVENT_GAMEROOM_INPUT_MAPINFO, 1, EVENT_GAMEROOM_PREV_MAP);
		}
		else if (po.instName == UI_BUTTON_NEXTMAP)
		{
			// cout << "NEXT MAP" << endl;
			m_MyScene->UIEvent(EVENT_GAMEROOM_INPUT_MAPINFO, 1, EVENT_GAMEROOM_NEXT_MAP);
		}
		else if (po.instName == UI_BUTTON_READY)
		{
			Service::GetApp()->Notify(EVENT_GAMEROOM_PLAYER_INPUT_READY);
		}
		else if (po.instName == UI_BUTTON_GAMESTART)
		{
			Service::GetApp()->Notify(EVENT_GAMEROOM_GAME_START);
		}

		InputHandler::g_LeftMouseCallback = false;
	}

	if (InputHandler::g_LeftMouseOverlap == false && m_PickedUIName != "")
	{
		//PickedObject po = Picking::RayIntersect(InputHandler::g_LastMousePos.x, InputHandler::g_LastMousePos.y, m_MapName);
		// UI Pressed 변경해주기
		m_MyScene->UIEvent(CLIENT_EVENT_GAMEROOM_UI_PRESSED, 2, m_PickedUIName, false);
		m_PickedUIName = "";
	}
}

GameplayController::GameplayController(GameplayScene* myScene) :
	m_MyScene(myScene)
{
}

void GameplayController::Update(const float deltaT)
{
	MouseCallback();
	HandleInput(deltaT);
}

void GameplayController::HandleInput(const float deltaT)
{
	// if 플레이어가 죽었을때만 작동됨
	if (!m_MyScene->m_IsArriveMyPlayer)
	{
		Camera* camera = CACHE_CACHE::GetApp()->m_Camera;

		// 시점변경
		if (GetAsyncKeyState('T') & 0x8000) {
			m_MyScene->ChangeFreeCamera();
		}
		float speed = 300 * deltaT;
		if (camera->GetCameraType() == CameraType::Free)
		{
			// 이동
			if (GetAsyncKeyState('W') & 0x8000) {
				camera->Walk(speed);
			}
			if (GetAsyncKeyState('S') & 0x8000) {
				camera->Walk(-speed);
			}
			if (GetAsyncKeyState('A') & 0x8000) {
				camera->Strafe(-speed);
			}
			if (GetAsyncKeyState('D') & 0x8000) {
				camera->Strafe(speed);
			}
			if (GetAsyncKeyState(VK_SPACE) & 0x8000) {
				camera->Up(speed);
			}
			if (GetAsyncKeyState('C') & 0x8000) {
				camera->Up(-speed);
			}
		}
	}

	if (InputHandler::IsKeyDown(VK_TAB))
		m_MyScene->m_IsTab = true;
	else if (InputHandler::IsKeyUp(VK_TAB))
		m_MyScene->m_IsTab = false;

	// 디버깅용 치트키
	// F1: 시간 10초 증가
	// F2: 시간 10초 감소
	// F3: 캐릭터 HP 0 (자살하기)
#ifdef DEBUG_CHEATING
	if (InputHandler::IsKeyUp(VK_F1))
	{
		Network::GetApp()->SendTestPlusTimePacket();
	}
	else if (InputHandler::IsKeyUp(VK_F2))
	{
		Network::GetApp()->SendTestMinusTimePacket();
	}
	else if (InputHandler::IsKeyUp(VK_F3))
	{
		Network::GetApp()->SendTestZeroHpPacket();
	}
	else if (InputHandler::IsKeyUp(VK_F4))
	{
		m_MyScene->m_DebugNoUI = !m_MyScene->m_DebugNoUI;
	}
#endif
}

void GameplayController::MouseCallback()
{
	// if 플레이어가 죽었을때만 작동됨
	if (m_MyScene->m_IsArriveMyPlayer) return;

	// 마우스 이동
	if (InputHandler::g_MoveMouseCallback)
	{
		CameraType cameraType = CACHE_CACHE::GetApp()->m_Camera->GetCameraType();
		Camera* camera = CACHE_CACHE::GetApp()->m_Camera;

		// 1인칭 회전(자전)
		if (cameraType == CameraType::First)
		{
			if (InputHandler::g_MouseChangebleY != 0.0f)
			{
				camera->Rotate(InputHandler::g_MouseChangebleY, 0.f, 0.f);
			}
			if (InputHandler::g_MouseChangebleX != 0.f)
			{
				//if (m_Owner->m_PlayerRole == ROLE_MASTER)
				//	m_Owner->Rotate(0, InputHandler::g_MouseChangebleX, 0);
				camera->Rotate(0.f, InputHandler::g_MouseChangebleX, 0.f);
			}
		}
		// 3인칭 회전(공전)
		else if (cameraType == CameraType::Third)
		{
			if (InputHandler::g_MouseChangebleY != 0.0f) {
				camera->Rotate(InputHandler::g_MouseChangebleY, 0, 0);
			}

			if (InputHandler::g_MouseChangebleX != 0.0f)
			{
				//if (m_Owner->m_PlayerRole == ROLE_MASTER)
				//	m_Owner->Rotate(0, InputHandler::g_MouseChangebleX, 0);
				camera->Rotate(0, InputHandler::g_MouseChangebleX, 0);
			}
		}
		// 자유 시점
		else if (cameraType == CameraType::Free)
		{
			if (InputHandler::g_MouseChangebleY != 0.0f) {
				camera->Rotate(InputHandler::g_MouseChangebleY, 0, 0);
			}

			if (InputHandler::g_MouseChangebleX != 0.0f)
			{
				// m_Owner->Rotate(0, InputHandler::g_MouseChangebleX, 0);
				camera->Rotate(0, InputHandler::g_MouseChangebleX, 0);
			}
		}

		// 마우스이동 처리
		POINT ptMouse = { Core::g_DisplayWidth / 2,Core::g_DisplayHeight / 2 };
		ClientToScreen(Core::g_hMainWnd, &ptMouse);
		SetCursorPos(ptMouse.x, ptMouse.y);
		InputHandler::g_MoveMouseCallback = false;
	}

	//다른 캐릭터 카메라로 변경되기 (마우스 좌클릭)
	// 스승 -> m_Users[0] -> ,,, -> m_Users[3] 순서대로
	if (InputHandler::g_LeftMouseCallback)
	{
		m_MyScene->ChangeCameraNextPlayer();
		InputHandler::g_LeftMouseCallback = false;
	}
}

GameresultController::GameresultController(GameresultScene* myScene) :
	m_MyScene(myScene)
{
}

void GameresultController::Update(const float deltaT)
{
	MouseCallback();
	HandleInput(deltaT);
}

void GameresultController::HandleInput(const float deltaT)
{
}

void GameresultController::MouseCallback()
{
}
