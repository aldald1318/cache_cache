#pragma once

/* Map Container 설명 */
// mapInfoDic: 서버 타입아이디랑 클라 타입아이디 매칭을 위한 컨테이너
// mapInfoVevctor: 맵에 배치된 모든 프롭 정보들
// playerVector: 맵에 배치된 모든 플레이어 정보들
// propTypeVector: 중복되는 것 없이 에셋이름만 모아놓은 컨테이너
// mapInfoChangeableVector: 픽킹처리를 위한 변할 수 있는 오브젝트들만 모아놓은 컨테이너

class Map
{
public:
	Map() = default;
	~Map();

public:
	// 크기가 작을때는 map / 클때는 unordered_map이 유리하다. 
	// 맵 정보들
	std::map<std::string, std::string> mapInfoDic;
	std::vector<MapTool::MapInfo> mapInfoVector;
	std::vector<MapTool::PlayerInfo> playerInfoVector;
	std::vector<MapTool::UIInfo> uiInfoVector;

	// 기타 기능 컨테이너
	std::vector<std::string> propTypeVector;
	std::vector<MapTool::MapInfo> mapInfoChangeableVector;

	// 텍스쳐 이름 ( 프롭 / 캐릭터 4종류 )
	int propTexture;
	//array<int, TOTAL_USER_COUNT> characterTextures;
};

