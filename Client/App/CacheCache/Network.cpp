#include "pch.h"
#include "Network.h"
#include "Service.h"


Network::Network()
{
	Initialize();
}

Network::~Network()
{
	Cleanup();
}

void Network::Initialize()
{
	WSADATA wsadata;
	if (WSAStartup(MAKEWORD(2, 2), &wsadata) != 0) errorQuit("error wsa Init");

	m_SwapRoomList.reserve(MAX_ROOM_COUNT);
	m_PenaltyDuration = 0;
}

void Network::Cleanup() // 클린업 함수는 원택이가 네트워크 클래스 소멸할 때 잘 호출하도록 해~
{
	m_SwapRoomList.clear();
	m_BattleClients.clear();
	for (int i = 0; i < SV_COUNT; ++i)
		closesocket(m_Client.Sockets[i].socket);
	WSACleanup();
}

int Network::RecvN(SOCKET sock, char* buffer, int len, int flags)
{
	int receivedBytes{};
	char* ptr = buffer;
	int leftBytes{ len };

	while (leftBytes > 0) {
		receivedBytes = recv(sock, ptr, leftBytes, flags);
		if (receivedBytes == SOCKET_ERROR) {
			if (WSAGetLastError() != WSAEWOULDBLOCK) {
				errorDisplay("RECVN_ERROR - BATTLESERVER TCP SOCKET");
				if (receivedBytes == 0) { closesocket(sock); return -1; }
			}
		}
		else {
			leftBytes -= receivedBytes;
			ptr += receivedBytes;
		}
	}
	return len - leftBytes;
}

void Network::RecvBattleServerTCP()
{
	char buf[MAX_BUF]{};
	int retval = recv(m_Client.Sockets[SV_BATTLE_TCP].socket, buf, MAX_BUF, 0);	
	if (retval == SOCKET_ERROR) {
		if (WSAGetLastError() != WSAEWOULDBLOCK) {
			errorDisplay("BATTLESERVER RECV ERROR!");
			if (retval == 0) {
				closesocket(m_Client.Sockets[SV_BATTLE_TCP].socket);
				closesocket(m_Client.Sockets[SV_BATTLE_UDP].socket);
				
			}
		}
	}

	else
	{
		ProcessData( buf, retval );
	}
}

void Network::RecvLobbyServer()
{
	char buf[MAX_BUF];
	int retval = recv(m_Client.Sockets[SV_LOBBY].socket, buf, MAX_BUF, 0);
	if (retval == SOCKET_ERROR) {
		if (WSAGetLastError() != WSAEWOULDBLOCK)
			//errorDisplay( "RECV - BATTLESERVER_TCP ERROR!" );
			return;
	}
	else
		ProcessData(buf, retval);
}


void Network::ProcessData(char* buf, size_t io_byte)
{
	char* ptr = buf;
	static unsigned short in_packet_size = 0; // buf 안의 packet size
	static unsigned short saved_packet_size = 0;
	static char packet_buffer[MAX_BUF]; // 완전한 패킷, 일부 패킷

	/*if( saved_packet_size != 0 )
		cout << "Saved PacketSize = " << saved_packet_size << endl;*/

	static char tmpBuf[MAX_BUF]{};
	memcpy(tmpBuf, buf, io_byte);
	ZeroMemory(tmpBuf, io_byte);

	while (0 != io_byte) {
		if (0 == in_packet_size) in_packet_size = (BYTE)ptr[0]; // 처음받은 메시지 크기를 저장
		if (io_byte + saved_packet_size >= in_packet_size) {
			// 처음받은 메시지 크기보다 새로 받은 바이트 수 + 저장하고 있던 바이트 수가 크면		
			//processPacket 호출, 넘겨준 바이트 만큼 버퍼 포인터, 받은 바이트 수 갱신
			memcpy(packet_buffer + saved_packet_size, ptr, in_packet_size - saved_packet_size);
			ProcessPacket(packet_buffer);
			ZeroMemory(packet_buffer, MAX_BUF);
			ptr += in_packet_size - saved_packet_size;
			io_byte -= in_packet_size - saved_packet_size;
			in_packet_size = 0;
			saved_packet_size = 0;
		}
		else {
			memcpy(packet_buffer + saved_packet_size, ptr, io_byte);   // 받은 것 만큼만 임시 저장 버퍼에 담아두기.
			saved_packet_size += io_byte;
			io_byte = 0;

			cout << "Saved Packet Size = " << saved_packet_size << endl;
		}
	}
	return;
}


void Network::ProcessPacket(char* packet_buffer)
{
	switch ((BYTE)packet_buffer[1])
	{
	case LC_LOGINOK:
	{
		std::cout << "lobby server login ok" << std::endl;
		
		Service::GetApp()->Notify(EVENT_TITLE_LOGIN_ALLOW);
		break;
	}
	case LC_LOGINDENY:
	{
		std::cout << "lobby server login deny\n";
		Service::GetApp()->Notify(EVENT_TITLE_LOGIN_DENY);
		break;
	}
	case LC_SIGNUPOK:
	{
		std::cout << "sign up ok" << std::endl;
		Service::GetApp()->Notify( EVENT_TITLE_SIGNUP_SUCCESS );
		break;
	}
	case LC_SIGNUPDENY:
	{
		std::cout << "sign up deny" << std::endl;
		Service::GetApp()->Notify(EVENT_TITLE_SIGNUP_DENY);

		break;
	}

	case LC_USERINFO:
	{
		lc_packet_userinfo* packet = reinterpret_cast<lc_packet_userinfo*>(packet_buffer);
		m_Client.mmr = packet->mmr;
		m_Client.catchedStudentCnt = packet->catched_student_count;
		m_Client.totalGameCnt = packet->total_game_cnt;
		m_Client.winRate = packet->winning_rate;
		// 아래 안 씀.
		m_Client.winGameCnt = packet->winning_game_cnt;

		Service::GetApp()->Notify( EVENT_LOBBY_UPDATE_USERINFO, 5, m_Client.lobbyID, m_Client.mmr, m_Client.catchedStudentCnt, m_Client.totalGameCnt, m_Client.winRate );
		break;
	}

	//////////////////////////////////////////////////////////
	// Auto Match
	//////////////////////////////////////////////////////////
	case LC_MATCH:
	{
		cout << "lc_match" << endl;
		lc_packet_match* match_packet = reinterpret_cast<lc_packet_match*>(packet_buffer);
		m_AutoRoomNumberToJoin = match_packet->roomNum;
		m_bAutoIsHost = match_packet->is_host;

		Service::GetApp()->Notify( EVENT_LOBBY_AUTOMATCH_SUCCESS, 2, match_packet->is_host, match_packet->roomNum );
		break;
	}
	case LC_CANCEL_AUTOMATCH_SUCCESS:
	{
		Service::GetApp()->Notify(EVENT_LOBBY_AUTOMATCH_CANCEL_SUCCESS);
		break;
	}

	//////////////////////////////////////////////////////////
	// Manual Match
	//////////////////////////////////////////////////////////
	case BC_MANUAL_ACCEPT_OK:
	{
		std::cout << "recv manual accept ok" << std::endl;
		bc_packet_accept_ok* accept_ok_packet = reinterpret_cast<bc_packet_accept_ok*>(packet_buffer);
		m_Client.battleID = accept_ok_packet->id;
		if (m_Client.battleID == 0) {
			cout << "[LOG] BUFFER 내용이 0일 가능성이 있습니다." << endl;
			cout << "bc_packet_accept_ok.size = " << accept_ok_packet->size << endl;
			break;
		}
		m_Client.loginok = true;

		/////////////////////////////////////////////////////////////////////////
		// battleClient 생성
		std::unique_ptr< BattleClient> battleClient = std::make_unique<BattleClient>(m_Client.battleID);
		memcpy( battleClient->name , m_Client.lobbyID.c_str(), 10);
		battleClient->mmrInfo.mmr = m_Client.mmr;
		battleClient->mmrInfo.catched_student_count = m_Client.catchedStudentCnt;
		battleClient->mmrInfo.winning_rate = m_Client.winRate;
		battleClient->mmrInfo.total_game_count = m_Client.totalGameCnt;
		battleClient->mmrInfo.winning_game_count = m_Client.winGameCnt;
		m_BattleClients[m_Client.battleID] = std::move(battleClient);

		string tmpName = m_BattleClients[m_Client.battleID]->name;
		Service::GetApp()->Notify(EVENT_LOBBY_MANUALMATCH_ACCEPTOK);
		break;
	}
	case BC_AUTO_ACCEPT_OK:
	{
		std::cout << "recv auto accept ok" << std::endl;
		bc_packet_accept_ok* accept_ok_packet = reinterpret_cast< bc_packet_accept_ok* >( packet_buffer );
		m_Client.battleID = accept_ok_packet->id;
		if ( m_Client.battleID == 0 ) {
			cout << "[LOG] BUFFER 내용이 0일 가능성이 있습니다." << endl;
			cout << "bc_packet_accept_ok.size = " << accept_ok_packet->size << endl;
			break;
		}
		m_Client.loginok = true;

		/////////////////////////////////////////////////////////////////////////
		// battleClient 생성
		std::unique_ptr< BattleClient> battleClient = std::make_unique<BattleClient>( m_Client.battleID );
		memcpy( battleClient->name, m_Client.lobbyID.c_str(), 10 );
		battleClient->mmrInfo.mmr = m_Client.mmr;
		battleClient->mmrInfo.catched_student_count = m_Client.catchedStudentCnt;
		battleClient->mmrInfo.winning_rate = m_Client.winRate;
		battleClient->mmrInfo.total_game_count = m_Client.totalGameCnt;
		battleClient->mmrInfo.winning_game_count = m_Client.winGameCnt;
		m_BattleClients[m_Client.battleID] = std::move( battleClient );
		if( m_bAutoIsHost ) m_BattleClients[m_Client.battleID]->m_Host = true;

		Service::GetApp()->Notify( EVENT_LOBBY_AUTOMATCH_ACCEPTOK, 2, m_AutoRoomNumberToJoin, m_bAutoIsHost);
		break;
	}
	case BC_ACCEPT_DEINED:
	{
		std::cout << "recv accept denied \n";
		break;
	}

	case BC_ROOM_LIST:
	{
		std::cout << "recv room list" << std::endl;
		bc_packet_room_list* room_list_packet = reinterpret_cast<bc_packet_room_list*>(packet_buffer);

		for (int i = 0; i < MAX_ROOM_COUNT; ++i) {
			if (room_list_packet->room_list[i].id != -1) {
				m_RecvedRoomList.push_back(room_list_packet->room_list[i]);
			}
		}

		if (room_list_packet->totalRoomNum == m_RecvedRoomList.size()) {
			// 스왑 룸 리스트 벡터와 스왑.
			m_SwapRoomList.swap(m_RecvedRoomList);

			// 리시브드 룸 리스트 벡터 클리어.
			m_RecvedRoomList.clear();
		}

		//for( int i = 0; i < MAX_ROOM_COUNT; ++i )
		//{
		//	auto iter = find_if( m_SwapRoomList.begin(), m_SwapRoomList.end(), [room_list_packet, i]( const PTC_Room& room ) {
		//		return room.id == room_list_packet->room_list[i].id; } );
		//	if( iter == m_SwapRoomList.end() && room_list_packet->room_list[i].id != -1 ) {
		//		m_SwapRoomList.push_back( room_list_packet->room_list[i] );
		//	}
		//}

		Service::GetApp()->Notify(EVENT_LOBBY_MANUALMATCH_ROOM_LIST);
		break;
	}
	case BC_ROOM_MAKED:
	{
		bc_packet_room_maked* room_ready_packet = reinterpret_cast<bc_packet_room_maked*>(packet_buffer);
		m_BattleClients[m_Client.battleID]->m_Host = true;
		SendBattleRoomJoinPacket(room_ready_packet->roomNum);
		break;
	}

	case BC_JOIN_OK:
	{
		bc_packet_join_ok* join_ok_packet = reinterpret_cast<bc_packet_join_ok*>(packet_buffer);
		// 내가 몇번째 플레이어인지 저장
		m_BattleClients[m_Client.battleID]->m_PlayerNum = join_ok_packet->playerNo;
		m_BattleClients[m_Client.battleID]->m_SpawnLocation = join_ok_packet->playerNo;

		cout << "BC_JOIN_OK" << endl;
		cout << "m_PlayerNum: " << m_BattleClients[m_Client.battleID]->m_PlayerNum << endl;
		cout << "m_SpawnLocation: " << m_BattleClients[m_Client.battleID]->m_SpawnLocation << endl;

		Service::GetApp()->Notify(EVENT_LOBBY_MANUALMATCH_ROOM_JOIN_ALLOW);
		break;
	}

	case BC_JOIN_DENY:
	{
		bc_packet_join_deny* packet = reinterpret_cast<bc_packet_join_deny*>(packet_buffer);
		m_BattleClients[m_Client.battleID]->Init();
		Service::GetApp()->Notify(EVENT_LOBBY_MANUALMATCH_ROOM_JOIN_DENY, 1, packet->code);
		break;
	}

	case BC_ROOM_ENTERED:
	{
		bc_packet_room_entered* room_entered_packet = reinterpret_cast<bc_packet_room_entered*>(packet_buffer);

		// Create BattleClient
		std::unique_ptr<BattleClient> battleClient = std::make_unique<BattleClient>(room_entered_packet->id);
		memcpy(battleClient->name, room_entered_packet->name, sizeof(char) * 10);
		battleClient->mmrInfo.mmr = room_entered_packet->mmr;
		battleClient->mmrInfo.catched_student_count = room_entered_packet->catched_student_count;
		battleClient->mmrInfo.winning_rate = room_entered_packet->winning_rate;
		battleClient->mmrInfo.total_game_count = room_entered_packet->total_game_cnt;
		battleClient->mmrInfo.total_game_count = room_entered_packet->winning_game_cnt;
		battleClient->m_Host = static_cast<int>(room_entered_packet->isManager);
		battleClient->m_PlayerNum = room_entered_packet->playerNo;
		battleClient->m_SpawnLocation = room_entered_packet->playerNo;
		battleClient->m_Ready = static_cast<int>(room_entered_packet->ready);
		battleClient->m_PlayerMesh = room_entered_packet->char_type;
		m_BattleClients[room_entered_packet->id] = std::move(battleClient);

		cout << endl;
		cout << "BC_ENTER_ROOM CHECK" << endl;
		cout << "My Mesh: " << m_BattleClients[m_Client.battleID]->m_PlayerMesh << endl;
		cout << "My Num: " << m_BattleClients[m_Client.battleID]->m_PlayerNum << endl;
		cout << "My SpawnLocation: " << m_BattleClients[m_Client.battleID]->m_SpawnLocation << endl;
		cout << "My Ready: " << m_BattleClients[m_Client.battleID]->m_Ready << endl;
		cout << "My IsHost: " << m_BattleClients[m_Client.battleID]->m_Host << endl;
		cout << endl;

		cout << endl;
		cout << room_entered_packet->id << " Mesh: " << m_BattleClients[room_entered_packet->id]->m_PlayerMesh << endl;
		cout << room_entered_packet->id << " Num: " << m_BattleClients[room_entered_packet->id]->m_PlayerNum << endl;
		cout << room_entered_packet->id << " SpawnLocation: " << m_BattleClients[room_entered_packet->id]->m_SpawnLocation << endl;
		cout << room_entered_packet->id << " Ready: " << m_BattleClients[room_entered_packet->id]->m_Ready << endl;
		cout << room_entered_packet->id << " IsHost: " << m_BattleClients[room_entered_packet->id]->m_Host << endl;
		cout << endl;

		Service::GetApp()->Notify(EVENT_GAMEROOM_PLAYER_ENTER, 1, room_entered_packet->id);
		break;
	}

	case BC_ROOM_LEAVED:
	{
		//std::cout << "recv room leaved packet \n";
		bc_packet_room_leaved* room_leaved_packet = reinterpret_cast<bc_packet_room_leaved*>(packet_buffer);

		// 내 아이디인지 비교해주고, 다른 사람이 나갈때랑 다르게 동작해야할 것 같다.
		if (m_Client.battleID != room_leaved_packet->id) {
			m_BattleClients[room_leaved_packet->id].release();
			m_BattleClients.erase(room_leaved_packet->id);
		}
		Service::GetApp()->Notify(EVENT_GAMEROOM_PLAYER_LEAVE, 1, room_leaved_packet->id);
		break;
	}

	case BC_READY:
	{
		//std::cout << "recv ready packet \n";
		bc_packet_ready* ready_packet = reinterpret_cast<bc_packet_ready*>(packet_buffer);
		m_BattleClients[ready_packet->id]->m_Ready = ready_packet->ready;

		Service::GetApp()->Notify(EVENT_GAMEROOM_PLAYER_CALLBACK_READY, 2, ready_packet->id, ready_packet->ready);
		break;
	}

	case BC_SELECT_CHARACTER:
	{	// 캐릭터 선택
		bc_packet_select_character* select_packet = reinterpret_cast<bc_packet_select_character*>(packet_buffer);
		m_BattleClients[select_packet->id]->m_PlayerMesh = select_packet->character;

		std::string selectMesh = select_packet->character;
		Service::GetApp()->Notify(EVENT_GAMEROOM_CALLBACK_SELECT_CHARACTER, 2, select_packet->id, selectMesh);
		break;
	}

	case BC_SELECT_CHARACTER_FAIL:
	{	// 캐릭터 선택 실패
		bc_packet_select_character_fail* packet = reinterpret_cast<bc_packet_select_character_fail*>(packet_buffer);
		break;
	}

	case BC_CANCEL_SELECT_CHARACTER:
	{	// 캐릭터 선택 취소
		bc_packet_cancel_select_character* packet = reinterpret_cast<bc_packet_cancel_select_character*>(packet_buffer);

		std::string selectCancelMesh = packet->character;
		Service::GetApp()->Notify(EVENT_GAMEROOM_CALLBACK_SELECT_CANCEL_CHARACTER, 2, packet->id, selectCancelMesh);

		break;
	}

	case BC_NEW_ROOM_MANAGER:
	{
		// 새 방장
		std::cout << "recv new_room_manager packet \n";
		bc_packet_new_room_manager* packet = reinterpret_cast<bc_packet_new_room_manager*>(packet_buffer);
		int new_room_manager = packet->id;
		m_BattleClients[new_room_manager]->m_Host = true;

		Service::GetApp()->Notify(EVENT_GAMEROOM_PLAYER_HOST, 1, new_room_manager);
		break;
	}

	case BC_MAP_TYPE:
	{
		std::cout << "recv map_type packet \n";
		bc_packet_map_type* packet = reinterpret_cast<bc_packet_map_type*>(packet_buffer);
		m_MapInfo = packet->mapType;

		Service::GetApp()->Notify(EVENT_GAMEROOM_CALLBACK_MAPINFO, 1, m_MapInfo);
		break;
	}

	case BC_GAMESTART_AVAILABLE:
	{
		std::cout << "recv gamestart_available packet\n";
		bc_packet_gamestart_available* packet = reinterpret_cast<bc_packet_gamestart_available*>(packet_buffer);
		bool available = packet->available;
		Service::GetApp()->Notify(EVENT_GAMEROOM_GAMESTART_AVAILABLE, 1, available);
		// 게임 시작 가능하다고 알려줘야함. 방장이면 받음. ( 클라 입장에선 내가 방장이면 오는 거니까 게임 스타트 버튼 활성화 해주면 된다. )
		break;
	}
	case BC_CHAT:
	{
		bc_packet_chat* packet = reinterpret_cast<bc_packet_chat*>(packet_buffer);
		wchar_t* chat = packet->chat;
		int id = packet->id;
		Service::GetApp()->Notify(EVENT_GAMEROOM_RECV_CHAT, 2, chat, id);
		break;
	}

	//////////////////////////////////////////////////////////
	// Gameplay
	//////////////////////////////////////////////////////////
	case BC_GAME_START:
	{
		cout << "bc_game_start" << endl;
		bc_packet_game_start* game_start_packet = reinterpret_cast<bc_packet_game_start*>(packet_buffer);

		for (auto& i : game_start_packet->info)
		{
			if (m_BattleClients.count(i.id))
			{
				m_BattleClients[i.id]->m_PlayerType = i.role;
				m_BattleClients[i.id]->m_SpawnLocation = i.spawn_pos;
			}
		}

		// 페널티 지속시간 여기서 받아서 넘기세요 20200715
		short penaltyDuration{ game_start_packet->penalty_duration };
		m_PenaltyDuration = penaltyDuration;

		m_MapInfo = static_cast<int>(game_start_packet->map_type);
		cout << "Network Gamestart Packet MapType: " << m_MapInfo << endl;

		Service::GetApp()->Notify(EVENT_LOADING_GAMEPLAY_INFO);
		break;
	}

	case BC_ROUND_START:
	{
		cout << "Recv bc_packet_round_start" << endl;
		Service::GetApp()->Notify(EVENT_GAMEPLAY_ROUND_START);

		break;
	}

	case BC_PLAYER_POS:
	{
		bc_packet_player_pos* player_pos_packet = reinterpret_cast<bc_packet_player_pos*>(packet_buffer);

		XMFLOAT3 argPos;
		argPos.x = player_pos_packet->pos.x;
		argPos.y = player_pos_packet->pos.y;
		argPos.z = player_pos_packet->pos.z;

		Service::GetApp()->Notify(EVENT_GAMEPLAY_CALLBACK_MOVE, 2, player_pos_packet->id, argPos);

		break;
	}
	case BC_PLAYER_ROT:
	{
		bc_packet_player_rot* player_rot_packet = reinterpret_cast<bc_packet_player_rot*>(packet_buffer);

		XMFLOAT3 look;
		look.x = player_rot_packet->look.x;
		look.y = player_rot_packet->look.y;
		look.z = player_rot_packet->look.z;

		Service::GetApp()->Notify(EVENT_GAMEPLAY_CALLBACK_MOUSE, 2, player_rot_packet->id, look);
		break;
	}
	case BC_OBJECT_POS:
	{
		//std::cout << "recv obj pos change" << std::endl;
		bc_packet_object_pos* player_objPos_packet = reinterpret_cast<bc_packet_object_pos*>(packet_buffer);

		// meshName / instID / pos
		XMFLOAT3 recvPos;
		recvPos.x = player_objPos_packet->pos.x;
		recvPos.y = player_objPos_packet->pos.y;
		recvPos.z = player_objPos_packet->pos.z;

		std::string type_id = to_string(player_objPos_packet->type_id);
		std::string obj_id = to_string(player_objPos_packet->obj_id);

		Service::GetApp()->Notify(EVENT_GAMEPLAY_OBJECT_POS, 3, type_id, obj_id, recvPos);

		break;
	}
	case BC_TRANSFORM:
	{
		bc_packet_transform* transform_packet = reinterpret_cast<bc_packet_transform*>(packet_buffer);
		// 배틀클라이언트에 정보저장
		m_BattleClients[transform_packet->id]->m_TransformMeshID = transform_packet->object_type;

		// 앱으로 보낼 데이터 가공
		std::string type_id = to_string(transform_packet->object_type);
		Service::GetApp()->Notify(EVENT_GAMEPLAY_TRAMNSFORM_RECV, 2, transform_packet->id, type_id);

		break;
	}

	case BC_TRANSFORM_CHARACTER:
	{// 무슨 캐릭터 메쉬 쓰는지 정보있음.
		bc_packet_transform_character* packet = reinterpret_cast<bc_packet_transform_character*>(packet_buffer);
		break;
	}

	case BC_PENALTY_WARNING:
	{
		bc_packet_penalty_warning* packet{ reinterpret_cast<bc_packet_penalty_warning*>(packet_buffer) };
		Service::GetApp()->Notify(EVENT_GAMEPLAY_PENALTY_WARNING);
		break;
	}

	case BC_PENALTY_WARNING_END:
	{
		bc_packet_penalty_warning_end* packet{ reinterpret_cast<bc_packet_penalty_warning_end*>(packet_buffer) };
		Service::GetApp()->Notify( EVENT_GAMEPLAY_PENALTY_WARNING_END );
		break;
	}

	case BC_PENALTY_BEGIN:
	{// 변신 페널티 시작하면 옵니다.
		bc_packet_penalty_begin* packet = reinterpret_cast<bc_packet_penalty_begin*>(packet_buffer);
		Service::GetApp()->Notify(EVENT_GAMEPLAY_PENALTY, 2, packet->id, true);
		break;
	}

	case BC_PENALTY_END:
	{// 변신 페널티 종료시 오는 패킷.
		bc_packet_penalty_end* packet = reinterpret_cast<bc_packet_penalty_end*>(packet_buffer);
		Service::GetApp()->Notify(EVENT_GAMEPLAY_PENALTY, 2, packet->id, false);
		break;
	}

	case BC_PUT_THUNDERBOLT:
	{
		bc_packet_put_thunderbolt* packet = reinterpret_cast<bc_packet_put_thunderbolt*>(packet_buffer);
		XMFLOAT3 putPos;
		putPos.x = packet->pos.x;
		putPos.y = packet->pos.y;
		putPos.z = packet->pos.z;

		Service::GetApp()->Notify(EVENT_GAMEPLAY_PUT_THUNDERBOLT, 2, packet->bolt_id, putPos); // 구조체도 넘길수 있나..?
		break;
	}

	case BC_REMOVE_THUNDERBOLT:
	{
		bc_packet_remove_thunderbolt* packet = reinterpret_cast<bc_packet_remove_thunderbolt*>(packet_buffer);
		Service::GetApp()->Notify(EVENT_GAMEPLAY_REMOVE_THUNDERBOLT, 1, packet->bolt_id);
		break;
	}

	case BC_HIT:
	{
		bc_packet_hit* packet = reinterpret_cast<bc_packet_hit*>(packet_buffer);
		float hp = packet->hp;
		hp = floor(hp * 100) / 100;
		Service::GetApp()->Notify(EVENT_GAMEPLAY_HIT, 2, packet->id, hp);

		std::cout << "recv hit!" << std::endl;
		break;
	}

	case BC_DIE:
	{
		std::cout << "recv die!" << std::endl;
		bc_packet_die* packet = reinterpret_cast<bc_packet_die*>(packet_buffer);
		Service::GetApp()->Notify(EVENT_GAMEPLAY_PLAYER_DIE, 1, packet->id);

		break;
	}

	case BC_LEFT_TIME:
	{
		//std::cout << "left time : " << (int)(unsigned char)packet_buffer[2] << std::endl;
		Service::GetApp()->Notify(EVENT_GAMEPLAY_TIMER, 1, (int)(unsigned char)packet_buffer[2]);
		break;
	}

	case BC_ANIM:
	{
		//cout << "Recved Anim Packet" << endl;
		bc_packet_anim_type* packet = reinterpret_cast<bc_packet_anim_type*>(packet_buffer);
		Service::GetApp()->Notify(EVENT_GAMEPLAY_ANIMATE, 2, packet->id, packet->anim_type);
		break;
	}

	case BC_GAME_OVER:
	{
		std::cout << "Network::GameOver" << endl;
		bc_packet_game_over* packet = reinterpret_cast<bc_packet_game_over*>(packet_buffer);
		m_WinTeam = packet->win_team;
		Service::GetApp()->Notify(EVENT_GAMEPLAY_GAMEOVER);
		break;
	}

	case BC_UPDATED_USER_INFO:
	{
		bc_packet_updated_user_info* packet = reinterpret_cast<bc_packet_updated_user_info*>(packet_buffer);
		// 패킷을 바로 로비서버로 릴레이한다.
		SendUpdatedUserInfo( packet->mmr, packet->catched_student_count, packet->winning_rate, packet->total_game_cnt, packet->winning_game_cnt );
		break;
	}

	default:
		std::cout << "unkown packet" << std::endl;
		std::cout << (int)packet_buffer[1] << std::endl;
		break;
	}
}

void Network::CreateSocket(SERVER_TYPE serverType)
{
	BOOL optval = TRUE;

	SOCKADDR_IN ServerAddr;
	ZeroMemory(&ServerAddr, sizeof(SOCKADDR_IN));
	ServerAddr.sin_family = AF_INET;

	switch (serverType)
	{
	case SV_BATTLE_TCP:
	{
		m_Client.Sockets[serverType].socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
		setsockopt(m_Client.Sockets[serverType].socket, IPPROTO_TCP, TCP_NODELAY, (char*)&optval, sizeof(optval));

		ServerAddr.sin_addr.s_addr = inet_addr(BATTLE_SERVER_IP_PUBLIC);
		ServerAddr.sin_port = htons(BATTLE_SERVER_PORT);
		break;
	}
	case SV_BATTLE_UDP:
	{
		m_Client.Sockets[serverType].socket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
		setsockopt(m_Client.Sockets[serverType].socket, IPPROTO_UDP, NULL, (char*)&optval, sizeof(optval));

		ServerAddr.sin_addr.s_addr = inet_addr(BATTLE_SERVER_IP_PUBLIC);
		ServerAddr.sin_port = htons(BATTLE_SERVER_UDP_PORT);
		break;
	}
	case SV_LOBBY:
	{
		m_Client.Sockets[serverType].socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
		setsockopt(m_Client.Sockets[serverType].socket, IPPROTO_TCP, TCP_NODELAY, (char*)&optval, sizeof(optval));

		ServerAddr.sin_addr.s_addr = inet_addr(LOBBY_SERVER_IP_PUBLIC);
		ServerAddr.sin_port = htons(LOBBY_SERVER_PORT);
		break;
	}
	}
	m_Client.Sockets[serverType].serverAddr = ServerAddr;
	m_Client.Sockets[serverType].curr_packet_size = 0;
	m_Client.Sockets[serverType].prev_packet_data = 0;
	m_Client.Sockets[serverType].connect = false;
}

void Network::ConnectServer(SERVER_TYPE serverType)
{
	int retval = 0;
	ULONG on = 1;

	switch (serverType)
	{
	case SERVER_TYPE::SV_LOBBY:
		if (!Service::GetApp()->IsConnectLobby())
			retval = connect(m_Client.Sockets[serverType].socket, (SOCKADDR*)&m_Client.Sockets[serverType].serverAddr, sizeof(SOCKADDR_IN));
		if (retval == SOCKET_ERROR)
		{
			if (WSAGetLastError() != WSAEWOULDBLOCK)
				errorQuit("BATTLE SERVER CONNECT ERROR!");
			//Service::GetApp()->Notify(EVENT_LOBBY_CONNECT_DENY);
			//return;
		}

		Service::GetApp()->Notify(EVENT_TITLE_CONNECT_ALLOW);
		break;
	case SERVER_TYPE::SV_BATTLE_TCP:
		if (!Service::GetApp()->IsConnectBattleTCP())
			retval = connect(m_Client.Sockets[serverType].socket, (SOCKADDR*)&m_Client.Sockets[serverType].serverAddr, sizeof(SOCKADDR_IN));
		if (retval == SOCKET_ERROR)
		{
			if (WSAGetLastError() != WSAEWOULDBLOCK)
				errorQuit("BATTLE SERVER CONNECT ERROR!");
			//Service::GetApp()->Notify(EVENT_LOBBY_CONNECT_DENY);
			//return;
		}

		break;
	case SERVER_TYPE::SV_BATTLE_UDP:
		if (!Service::GetApp()->IsConnectBattleUDP())
			retval = connect(m_Client.Sockets[serverType].socket, (SOCKADDR*)&m_Client.Sockets[serverType].serverAddr, sizeof(SOCKADDR_IN));
		if (retval == SOCKET_ERROR)
		{
			if (WSAGetLastError() != WSAEWOULDBLOCK)
				errorQuit("BATTLE SERVER CONNECT ERROR!");
			//Service::GetApp()->Notify(EVENT_LOBBY_CONNECT_DENY);
			//return;
		}

		break;
	}

	ioctlsocket(m_Client.Sockets[serverType].socket, FIONBIO, &on);
	m_Client.Sockets[serverType].connect = true;
}

void Network::DisconnectServer(SERVER_TYPE serverType)
{
	LINGER linger{ 1, 0 };
	setsockopt(m_Client.Sockets[serverType].socket, SOL_SOCKET, SO_LINGER, (char*)&linger, sizeof(linger));
	shutdown(m_Client.Sockets[serverType].socket, SD_SEND);

	ZeroMemory(&m_Client.Sockets[serverType].serverAddr, sizeof(m_Client.Sockets[serverType].serverAddr));
	m_Client.Sockets[serverType].curr_packet_size = 0;
	m_Client.Sockets[serverType].prev_packet_data = 0;
	m_Client.Sockets[serverType].connect = false;
}

bool Network::SendPacket(void* buffer, SERVER_TYPE serverType)
{
	char* packet = reinterpret_cast<char*>(buffer);
	int packet_size = (BYTE)packet[0];
	int retval = send(m_Client.Sockets[serverType].socket, packet, packet_size, 0);
	if (retval == SOCKET_ERROR)
	{
		errorDisplay("Send Error! ");
		//std::cout << "Send Error \n";
		return false;
	}
	return true;
}

void Network::SendLobbyLoginPacket(std::string id, std::string password)
{
	m_Client.lobbyID = id;

	cl_packet_login packet;
	packet.size = sizeof( packet );
	packet.type = CL_LOGIN;
	memcpy( packet.id, id.c_str(), sizeof( char ) * 10 );
	memcpy( packet.password, password.c_str(), sizeof( char ) * 10 );
	SendPacket( &packet, SV_LOBBY );
}

void Network::SendLobbySingUpPacket(std::string id, std::string password)
{
	cl_packet_signup packet;
	packet.size = sizeof(packet);
	packet.type = CL_SIGNUP;
	strcpy(packet.id, id.c_str());
	strcpy(packet.password, password.c_str());
	SendPacket(&packet, SV_LOBBY);
}

void Network::SendAutoMatchPacket()
{
	cl_packet_automatch packet;
	packet.size = sizeof(packet);
	packet.type = CL_AUTOMATCH;
	SendPacket(&packet, SV_LOBBY);
}

void Network::SendAutoMatchCancelPacket()
{
	cl_packet_cancel_automatch auto_cancel_packet;
	auto_cancel_packet.size = sizeof(auto_cancel_packet);
	auto_cancel_packet.type = CL_CANCEL_AUTOMATCH;
	SendPacket(&auto_cancel_packet, SV_LOBBY);
}

void Network::SendRequestUserInfoPacket()
{
	cl_packet_request_userinfo packet;
	packet.size = sizeof( packet );
	packet.type = CL_REQUEST_USERINFO;
	SendPacket( &packet, SV_LOBBY );
}
int cnt{};

bool Network::SendBattleLoginPacket( char match_type )
{
	cout << "로그인 요청 횟수 = " << cnt++ << endl;
	cb_packet_login packet;
	packet.size = sizeof(packet);
	packet.type = CB_LOGIN;
	packet.is_automatch = match_type;
	memcpy( &packet.name, m_Client.lobbyID.c_str(), 10 );
	packet.mmr = m_Client.mmr;
	packet.catched_student_count = m_Client.catchedStudentCnt;
	packet.winning_rate = m_Client.winRate;
	packet.total_game_cnt = m_Client.totalGameCnt;
	packet.winning_game_cnt = m_Client.winGameCnt;
	if (!SendPacket(&packet, SV_BATTLE_TCP)) return false;
	return true;
}

void Network::SendRequestManualRoomListPacket()
{
	cb_packet_request_room packet;
	packet.size = sizeof(packet);
	packet.type = CB_REQUEST_ROOM_LIST;
	SendPacket(&packet, SV_BATTLE_TCP);
}

void Network::SendBattleRoomJoinPacket(int roomNum)
{
	std::cout << "Room Number = " << roomNum << std::endl;
	if (m_BattleClients.count(m_Client.battleID))
		m_BattleClients[m_Client.battleID]->m_RoomNumber = roomNum;

	cb_packet_join packet;
	packet.size = sizeof(packet);
	packet.type = CB_JOIN;
	packet.roomNum = m_BattleClients[m_Client.battleID]->m_RoomNumber;
	packet.is_roomManager = m_BattleClients[m_Client.battleID]->m_Host;
	SendPacket(&packet, SV_BATTLE_TCP);
}

void Network::SendBattleRoomMakePacket(wstring roomTitle)
{
	cb_packet_request_manual_room packet;
	packet.size = sizeof(packet);
	packet.type = CB_REQUSET_ROOM;
	if (roomTitle.size() > 10)
		roomTitle[10] = 0;
	wcscpy(packet.room_title, roomTitle.c_str());
	wcout << L"SendBattleRoomMakePacket() " << packet.room_title << endl;
	SendPacket(&packet, SV_BATTLE_TCP);
}

void Network::SendRoomLeavePacket()
{
	m_BattleClients[m_Client.battleID]->Init();

	cb_packet_room_leave packet;
	packet.size = sizeof(packet);
	packet.type = CB_ROOM_LEAVE;
	SendPacket(&packet, SERVER_TYPE::SV_BATTLE_TCP);
}

void Network::SendReadyPacket()
{
	cb_packet_ready packet;
	packet.size = sizeof(packet);
	packet.type = CB_READY;
	SendPacket(&packet, SV_BATTLE_TCP);
}

void Network::SendSelectPacket(std::string selected)
{
	cout << selected << "가 선택되었다고 네트워크에게 패킷전송" << endl;

	m_BattleClients[m_Client.battleID]->m_PlayerMesh = selected;
	//memcpy( (void*)m_BattleClients[m_Client.battleID]->m_PlayerMesh.c_str(), selected.c_str(), sizeof( selected ) );
	cb_packet_select_character packet;
	packet.size = sizeof(packet);
	packet.type = CB_SELECT_CHARACTER;
	packet.id = m_Client.battleID;
	strcpy_s(packet.character, selected.c_str());
	SendPacket(&packet, SV_BATTLE_TCP);
}

void Network::SendSelectCancelPacket(std::string selected)
{
	cout << selected << "가 선택취소되었다고 네트워크에게 패킷전송" << endl;

	m_BattleClients[m_Client.battleID]->m_PlayerMesh = CHARACTER_NONE;

	cb_packet_select_character packet;
	packet.size = sizeof(packet);
	packet.type = CB_CANCEL_SELECT_CHARACTER;
	packet.id = m_Client.battleID;
	strcpy_s(packet.character, selected.c_str());
	SendPacket(&packet, SV_BATTLE_TCP);
}

void Network::SendMapTypePacket(int mapType)
{
	m_MapInfo = mapType;

	cb_packet_map_type packet;
	packet.size = sizeof(packet);
	packet.type = CB_MAP_TYPE;
	packet.map_type = mapType;
	SendPacket(&packet, SV_BATTLE_TCP);
}

void Network::SendGameStartPacket()
{
	cb_packet_start packet;
	packet.size = sizeof(packet);
	packet.type = CB_START;
	SendPacket(&packet, SV_BATTLE_TCP);
}

void Network::SendMovePacket(char key_status)
{
	cb_packet_move_key_status packet;
	packet.size = sizeof(packet);
	packet.type = key_status;
	SendPacket(&packet, SV_BATTLE_TCP);
}

void Network::SendLookVectorPacket(XMFLOAT3& look)
{
	//static unsigned int cnt;
	//cout << cnt++ << endl;

	cb_packet_look_vector packet;
	packet.size = sizeof(packet);
	packet.type = CB_LOOK_VECTOR;
	packet.id = m_Client.battleID;
	packet.look.x = look.x;
	packet.look.y = look.y;
	packet.look.z = look.z;
	SendPacket(&packet, SV_BATTLE_UDP);
}


void Network::SendTransformPacket(short obj_type)
{
	if (m_BattleClients.count(m_Client.battleID))
		m_BattleClients[m_Client.battleID]->m_TransformMeshID = obj_type;

	cb_packet_transform packet;
	packet.size = sizeof(packet);
	packet.type = CB_TRANSFORM;
	packet.obj_type = obj_type;
	SendPacket(&packet, SV_BATTLE_TCP);
}

void Network::SendThunderboltPacket(XMFLOAT3& direction)
{
	cb_packet_thunder_bolt packet;
	packet.size = sizeof(packet);
	packet.type = CB_THUNDER_BOLT;
	packet.dir.x = direction.x;
	packet.dir.y = direction.y;
	packet.dir.z = direction.z;
	SendPacket(&packet, SERVER_TYPE::SV_BATTLE_TCP);
}

void Network::SendChat(wchar_t* chatBuf)
{
	cb_packet_chat packet;
	packet.size = sizeof(packet);
	packet.type = CB_CHAT;
	// sprintf(packet.chat, "%ws", chatBuf);
	memcpy(packet.chat, chatBuf, sizeof(wchar_t)*100);
	SendPacket(&packet, SERVER_TYPE::SV_BATTLE_TCP);
}

void Network::SendUpdatedUserInfo( int mmr, int catchCnt, float w_rate, int totalCnt, int winCnt )
{
	cl_packet_updated_user_info packet;
	packet.size = sizeof( packet );
	packet.type = CL_UPDATED_USER_INFO;
	packet.mmr = mmr;
	packet.catched_student_count = catchCnt;
	packet.winning_rate = w_rate;
	packet.total_game_cnt = totalCnt;
	packet.winning_game_cnt = winCnt;
	SendPacket( &packet, SERVER_TYPE::SV_LOBBY );
}

void Network::SendTestPlusTimePacket()
{
	cb_test_packet_plus_time packet;
	packet.size = sizeof(packet);
	packet.type = CB_TEST_PLUS_TIME;
	SendPacket(&packet, SV_BATTLE_TCP);
}

void Network::SendTestMinusTimePacket()
{
	cb_test_packet_minus_time packet;
	packet.size = sizeof(packet);
	packet.type = CB_TEST_MINUS_TIME;
	SendPacket(&packet, SV_BATTLE_TCP);
}

void Network::SendTestZeroHpPacket()
{
	cb_test_packet_zero_hp packet;
	packet.size = sizeof(packet);
	packet.type = CB_TEST_ZERO_HP;
	SendPacket(&packet, SV_BATTLE_TCP);
}


