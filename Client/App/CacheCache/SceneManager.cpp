#include "pch.h"
#include "SceneManager.h"
#include "Scene.h"

#include "LogoScene.h"
#include "TitleScene.h"
#include "LobbyScene.h"
#include "GameroomScene.h"
#include "GameplayScene.h"
#include "GameresultScene.h"

#include "Service.h"

// 인자 없을 때 사용.
void SceneManager::SendEvent(SceneType sceneType, int sEvent)
{
	m_Scenes[static_cast<int>(sceneType)]->ProcessEvent(sEvent);
}

// 인자가 있을 경우 사용
void SceneManager::SendEventArgs(SceneType sceneType, int sEvent, int argsCount, ...)
{
	va_list ap;
	MMRInfo mmrInfo = {};
	string id_str;
	switch (sEvent)
	{

		//////////////////////////////////////////////////////////////////////////////////////////
		// lOBBY Scene
		//////////////////////////////////////////////////////////////////////////////////////////
	case EVENT_LOBBY_MANUALMATCH_ROOM_JOIN_DENY:
		int deny_code;
		va_start(ap, argsCount);
		deny_code = va_arg(ap, int);
		va_end(ap);

		m_Scenes[static_cast<int>(sceneType)]->ProcessEvent(sEvent, argsCount, deny_code);
		break;

	
	case EVENT_LOBBY_UPDATE_USERINFO:
		va_start( ap, argsCount );
		id_str = va_arg( ap, string );
		mmrInfo.mmr = va_arg( ap, int );
		mmrInfo.catched_student_count = va_arg( ap, int );
		mmrInfo.total_game_count = va_arg( ap, int );
		mmrInfo.winning_rate = va_arg( ap, double );
		va_end( ap );

		m_Scenes[static_cast<int>(sceneType)]->ProcessEvent( sEvent, argsCount, id_str, mmrInfo.mmr, mmrInfo.catched_student_count, mmrInfo.total_game_count, mmrInfo.winning_rate );

		break;

//////////////////////////////////////////////////////////////////////////////////////////
// Gameroom Scene
//////////////////////////////////////////////////////////////////////////////////////////

	case EVENT_GAMEROOM_PLAYER_ENTER:
	{
		int argID;
		va_start(ap, argsCount);
		argID = va_arg(ap, int);
		va_end(ap);

		m_Scenes[static_cast<int>(sceneType)]->ProcessEvent(sEvent, argsCount, argID);
		break;
	}
	case EVENT_GAMEROOM_PLAYER_LEAVE:
	{
		int argID;
		va_start(ap, argsCount);
		argID = va_arg(ap, int);
		va_end(ap);

		m_Scenes[static_cast<int>(sceneType)]->ProcessEvent(sEvent, argsCount, argID);
		break;
	}
	case EVENT_GAMEROOM_PLAYER_HOST:
	{
		int argID;
		va_start(ap, argsCount);
		argID = va_arg(ap, int);
		va_end(ap);

		m_Scenes[static_cast<int>(sceneType)]->ProcessEvent(sEvent, argsCount, argID);
		break;
	}
	case EVENT_GAMEROOM_PLAYER_CALLBACK_READY:
	{
		int argID;
		char argReady;

		va_start(ap, argsCount);
		argID = va_arg(ap, int);
		argReady = va_arg(ap, char);
		va_end(ap);

		m_Scenes[static_cast<int>(sceneType)]->ProcessEvent(sEvent, argsCount, argID, argReady);
		break;
	}
	case EVENT_GAMEROOM_CALLBACK_MAPINFO:
	{
		int mapType = NULL;
		va_start(ap, argsCount);
		mapType = va_arg(ap, int);
		va_end(ap);

		m_Scenes[static_cast<int>(sceneType)]->ProcessEvent(sEvent, argsCount, mapType);
		break;
	}
	case EVENT_GAMEROOM_CALLBACK_SELECT_CHARACTER:
	{
		int argID;
		std::string argSelectMesh;
		va_start(ap, argsCount);
		argID = va_arg(ap, int);
		argSelectMesh = va_arg(ap, std::string);
		va_end(ap);

		m_Scenes[static_cast<int>(sceneType)]->ProcessEvent(sEvent, argsCount, argID, argSelectMesh);
		break;
	}
	case EVENT_GAMEROOM_CALLBACK_SELECT_CANCEL_CHARACTER:
	{
		int argID;
		std::string argSelectCancelMesh;
		va_start(ap, argsCount);
		argID = va_arg(ap, int);
		argSelectCancelMesh = va_arg(ap, std::string);
		va_end(ap);

		m_Scenes[static_cast<int>(sceneType)]->ProcessEvent(sEvent, argsCount, argID, argSelectCancelMesh);
		break;
	}

	case EVENT_GAMEROOM_GAMESTART_AVAILABLE:
	{
		bool available{};
		va_list ap;
		va_start(ap, argsCount);
		available = va_arg(ap, bool);
		va_end(ap);
		m_Scenes[static_cast<int>(sceneType)]->ProcessEvent(sEvent, argsCount, available);
		break;
	}

	case EVENT_LOADING_GAMEPLAY_INFO:
	{
		m_Scenes[static_cast<int>(sceneType)]->ProcessEvent(sEvent);
		break;
	}

	case EVENT_GAMEROOM_RECV_CHAT:
	{
		string chatBuf = "";
		int id;

		va_list ap;
		va_start(ap, argsCount);
		chatBuf = va_arg(ap, string);
		id = va_arg(ap, int);
		va_end(ap);
		m_Scenes[static_cast<int>(sceneType)]->ProcessEvent(sEvent, argsCount, chatBuf, id);
		break;
	}
	//////////////////////////////////////////////////////////////////////////////////////////
	// Gameplay Scene
	//////////////////////////////////////////////////////////////////////////////////////////
	case EVENT_GAMEPLAY_START:
	{
		int argBattleID;
		int argPlayerType;
		std::string argPlayerMesh;
		int argSpawnLocation;

		va_start(ap, argsCount);
		argBattleID = va_arg(ap, int);
		argPlayerType = va_arg(ap, int);
		argPlayerMesh = va_arg(ap, std::string);
		argSpawnLocation = va_arg(ap, int);
		va_end(ap);

		m_Scenes[static_cast<int>(sceneType)]->ProcessEvent(sEvent, argsCount, argBattleID, argPlayerType, argPlayerMesh, argSpawnLocation);
		break;
	}
	case EVENT_GAMEPLAY_CALLBACK_MOVE:
	{
		int argID;
		XMFLOAT3 argPos;
		va_start(ap, argsCount);
		argID = va_arg(ap, int);
		argPos = va_arg(ap, XMFLOAT3);
		va_end(ap);

		m_Scenes[static_cast<int>(sceneType)]->ProcessEvent(sEvent, argsCount, argID, argPos);
		break;
	}
	case EVENT_GAMEPLAY_CALLBACK_MOUSE:
	{
		int argID;
		va_start(ap, argsCount);
		argID = va_arg(ap, int);
		argLookCallback = va_arg(ap, XMFLOAT3);
		va_end(ap);

		m_Scenes[static_cast<int>(sceneType)]->ProcessEvent(sEvent, argsCount, argID, argLookCallback);
		break;
	}
	case EVENT_GAMEPLAY_OBJECT_POS:
	{
		std::string argMeshName;
		std::string argInstID;
		XMFLOAT3 argObjPos;

		va_start(ap, argsCount);
		argMeshName = va_arg(ap, std::string);
		argInstID = va_arg(ap, std::string);
		argObjPos = va_arg(ap, XMFLOAT3);
		va_end(ap);

		m_Scenes[static_cast<int>(sceneType)]->ProcessEvent(sEvent, argsCount, argMeshName, argInstID, argObjPos);

		break;
	}
	case EVENT_GAMEPLAY_TRAMNSFORM_RECV:
	{
		int argClientID;
		std::string serverMeshID;

		va_start(ap, argsCount);
		argClientID = va_arg(ap, int);
		serverMeshID = va_arg(ap, std::string);
		va_end(ap);

		m_Scenes[static_cast<int>(sceneType)]->ProcessEvent(sEvent, argsCount, argClientID, serverMeshID);
		break;
	}
	case EVENT_GAMEPLAY_PUT_THUNDERBOLT:
	{
		int boltID;
		XMFLOAT3 putPos;

		va_start(ap, argsCount);
		boltID = va_arg(ap, int);
		putPos = va_arg(ap, XMFLOAT3);
		va_end(ap);

		m_Scenes[static_cast<int>(sceneType)]->ProcessEvent(sEvent, argsCount, boltID, putPos);
		break;
	}
	case EVENT_GAMEPLAY_REMOVE_THUNDERBOLT:
	{
		int boltID;

		va_start(ap, argsCount);
		boltID = va_arg(ap, int);
		va_end(ap);

		m_Scenes[static_cast<int>(sceneType)]->ProcessEvent(sEvent, argsCount, boltID);
		break;
	}

	case EVENT_GAMEPLAY_HIT:
	{
		int id;
		double hp;

		va_start(ap, argsCount);
		id = va_arg(ap, int);
		hp = va_arg(ap, double);
		va_end(ap);

		m_Scenes[static_cast<int>(sceneType)]->ProcessEvent(sEvent, argsCount, id, hp);
		break;
	}

	case EVENT_GAMEPLAY_ANIMATE:
	{
		int id; // 애니메이션 수행할 배틀클라이언트
		char anim_type; // 수행할 애니메이션 타입

		va_list ap;
		va_start(ap, argsCount);
		id = va_arg(ap, int);
		anim_type = va_arg(ap, char);
		va_end(ap);

		m_Scenes[static_cast<int>(sceneType)]->ProcessEvent(sEvent, argsCount, id, anim_type);
		break;
	}
	case EVENT_GAMEPLAY_PLAYER_DIE:
	{
		int id; // 애니메이션 수행할 배틀클라이언트

		va_list ap;
		va_start(ap, argsCount);
		id = va_arg(ap, int);
		va_end(ap);

		m_Scenes[static_cast<int>(sceneType)]->ProcessEvent(sEvent, argsCount, id);
		break;
	}
	case EVENT_GAMEPLAY_TIMER:
	{
		int timer; // 애니메이션 수행할 배틀클라이언트

		va_list ap;
		va_start(ap, argsCount);
		timer = va_arg(ap, int);
		va_end(ap);

		m_Scenes[static_cast<int>(sceneType)]->ProcessEvent(sEvent, argsCount, timer);
		break;
	}
	case EVENT_GAMEPLAY_PENALTY:
	{
		int targetId;
		bool isPenarty;

		va_list ap;
		va_start(ap, argsCount);
		targetId = va_arg(ap, int);
		isPenarty = va_arg(ap, bool);
		va_end(ap);

		m_Scenes[static_cast<int>(sceneType)]->ProcessEvent(sEvent, argsCount, targetId, isPenarty);
		break;
	}
	case EVENT_LOADING_GAMERESULT_INFO:
	{
		string chrName;

		va_list ap;
		va_start(ap, argsCount);
		chrName = va_arg(ap, std::string);
		va_end(ap);

		m_Scenes[static_cast<int>(sceneType)]->ProcessEvent(sEvent, argsCount, chrName);
		break;
	}
	default:
	{
		std::cout << "Unknown Event recv \n";
		break;
	}
	}
}

SceneManager::SceneManager() :
	m_CurScene(0)
{
}

SceneManager::~SceneManager()
{
	for (auto& s : m_Scenes)
	{
		SAFE_DELETE_PTR(s->m_SceneController);
	}

	for (auto& s : m_Scenes) {
		s->Exit();
		SAFE_DELETE_PTR(s);
	}
}

void SceneManager::InitializeScenes()
{
	CreateScene<LogoScene>(SceneType::Logo, "Logo");
	CreateScene<TitleScene>(SceneType::Title, "Title");
	CreateScene<LobbyScene>(SceneType::Lobby, "Lobby");
	CreateScene<GameroomScene>(SceneType::GameRoom, "GameRoom");
	CreateScene<GameplayScene>(SceneType::GamePlay, "GamePlay");
	CreateScene<GameresultScene>(SceneType::GameResult, "GameResult");
}

void SceneManager::OnResize()
{
	m_Scenes[m_CurScene]->OnResize();
}

void SceneManager::ChangeScene()
{
	if (m_CurScene == -1)
		cout << "NullScene" << endl;

	m_MutexChangeScene.lock();
	m_Scenes[m_CurScene]->Exit();

	m_CurScene += 1;

	if (m_CurScene >= static_cast<int>(SceneType::Count))
		m_CurScene = 1;

	m_Scenes[m_CurScene]->Enter();
	m_MutexChangeScene.unlock();
}

void SceneManager::ChangeScene(SceneType sceneType)
{
	if (m_CurScene == -1)
		cout << "NullScene" << endl;

	m_MutexChangeScene.lock();
	m_Scenes[m_CurScene]->Exit();

	m_CurScene = static_cast<int>(sceneType);
	m_Scenes[m_CurScene]->Enter();
	m_MutexChangeScene.unlock();
}

void SceneManager::EnterScene(SceneType sceneType)
{
	m_CurScene = static_cast<UINT>(sceneType);
	m_Scenes[m_CurScene]->Enter();
}

void SceneManager::ExitScene()
{
	m_Scenes[m_CurScene]->Exit();
}

void SceneManager::UpdateScene(const float& deltaT)
{
	m_Scenes[m_CurScene]->Update(deltaT);
}

void SceneManager::RenderScene()
{
	m_Scenes[m_CurScene]->Render();
}

void SceneManager::RenderUI()
{
	m_Scenes[m_CurScene]->RenderUI();
}

void SceneManager::WriteShadow()
{
	m_Scenes[m_CurScene]->WriteShadow();
}

Scene* SceneManager::GetCurScene() const
{
	return m_Scenes[m_CurScene];
}

UINT SceneManager::GetCurSceneIndex() const
{
	return m_CurScene;
}

