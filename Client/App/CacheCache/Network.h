#pragma once
#include "Singleton.h"
#include "BattleClient.h"

#define MAX_BUF	8192
#define MAX_PACKET_SIZE	8192

enum SERVER_TYPE { SV_BATTLE_TCP, SV_BATTLE_UDP, SV_LOBBY, SV_COUNT };

struct SocketInfo {
	SOCKET socket;
	SOCKADDR_IN serverAddr;
	unsigned char packet_buf[MAX_PACKET_SIZE];
	int prev_packet_data;
	int curr_packet_size;

	bool connect;
};

struct CLIENT {
	std::string		lobbyID{};
	int				battleID = -1;
	bool			loginok{};

	// lobby , battle socket (tcp, udp)
	SocketInfo Sockets[SV_COUNT]{};

	// player infos
	char id_str[10]{};
	int mmr{};
	int catchedStudentCnt{};
	float winRate{};
	int totalGameCnt{};
	int winGameCnt{};
};

class Network : public TemplateSingleton<Network>
{
	friend class Service;
public:
	explicit Network();
	virtual ~Network();

	void Initialize();
	void Cleanup();

public:
	void ProcessData( char* buf, size_t io_byte );
	void ProcessPacket( char* packet_buffer );

	void CreateSocket(SERVER_TYPE serverType);
	void ConnectServer( SERVER_TYPE serverType);
	void DisconnectServer(SERVER_TYPE serverType);

	int RecvN( SOCKET sock, char* buffer, int len, int flags );
	void RecvBattleServerTCP();
	void RecvLobbyServer();

	bool SendPacket( void* buffer, SERVER_TYPE serverType );
	void SendLobbyLoginPacket(std::string id, std::string password);
	void SendLobbySingUpPacket( std::string id, std::string password );
	void SendAutoMatchPacket();
	void SendAutoMatchCancelPacket();
	void SendRequestUserInfoPacket();
	bool SendBattleLoginPacket( char match_type ); // BattleServer한테 보내는거
	void SendRequestManualRoomListPacket();
	void SendBattleRoomJoinPacket(int roomNum); // 방참여
	void SendBattleRoomMakePacket(wstring roomTitle); // 방생성
	void SendRoomLeavePacket();

	void SendReadyPacket();
	void SendSelectPacket(std::string selected);
	void SendSelectCancelPacket(std::string selected);
	void SendMapTypePacket(int mapType);
	void SendGameStartPacket();
	void SendMovePacket( char key_status );
	void SendLookVectorPacket(XMFLOAT3& look);
	void SendTransformPacket( short obj_type );
	void SendThunderboltPacket( XMFLOAT3& direction );
	void SendChat(wchar_t* chatBuf);

	void SendUpdatedUserInfo( int mmr, int catchCnt, float w_rate, int totalCnt, int winCnt );

public:
	/* Debug Cheating */
	// 시간 10초 증감
	// HP 0(자살하기)
	void SendTestPlusTimePacket();
	void SendTestMinusTimePacket();
	void SendTestZeroHpPacket();

private:
	// 씬 정보들
	std::vector<PTC_Room> m_RecvedRoomList{};
	std::vector<PTC_Room> m_SwapRoomList{};
	int m_MapInfo;
	int m_WinTeam;
	short m_PenaltyDuration;

	// battleclient의 접속하기 위한 정보뿐(id, mmr ,,,)
	CLIENT m_Client; 

	// Automatch 정보 저장
	int m_AutoRoomNumberToJoin {};
	char m_bAutoIsHost {};

	/*key: Client::battleID*/
	std::map<int, unique_ptr<BattleClient>> m_BattleClients;
};

