#pragma once

struct MMRInfo
{
	wstring name;
	int mmr = -1;
	int catched_student_count = 0;
	int total_game_count{};
	float winning_rate = 0.f;
	int winning_game_count{};
};

class BattleClient
{
	friend class Network;
	friend class Service;
	friend class GameroomScene;
	int		battleID;

	// DB Info
	char	name[10];
	MMRInfo mmrInfo;

public:
	BattleClient(int battleID);
	virtual ~BattleClient();

	void Init();
	void InitMMR();
	wstring GetName() {
		WCHAR wc[256];
		int nLen = MultiByteToWideChar(CP_ACP, 0, name, sizeof(name), NULL, NULL);
		MultiByteToWideChar(CP_ACP, 0, name, sizeof(name), wc, nLen);
		wstring wstr{ wc };
		return wstr;
	};

private:
	int			m_PlayerNum; /*몇번째 플레이어(PlayerID)*/
	int			m_PlayerType; /*플레이어 역할(PlayerType)*/
	int			m_SpawnLocation;
	// 나중에 define int값으로 변경할예정
	std::string m_PlayerMesh;
	int			m_TransformMeshID;

	int			m_RoomNumber;
	bool		m_Host;
	bool		m_Ready;
};

