#include "pch.h"
#include "LogoScene.h"
#include "AssertsReference.h"
#include "CommandContext.h"

void LogoScene::Initialize()
{
}

void LogoScene::OnResize()
{
}

bool LogoScene::Enter()
{
	cout << "Logo Scene" << endl;

	return false;
}

void LogoScene::Exit()
{
}

void LogoScene::Update(const float& fDeltaTime)
{
}

void LogoScene::Render()
{
}

void LogoScene::RenderUI()
{
}
