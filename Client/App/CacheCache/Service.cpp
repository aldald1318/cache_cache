#include "pch.h"
#include "Service.h"


void Service::Termiante()
{
	running = false;
}

void Service::RunService()
{
	// 로비(TCP) 소켓 생성 & 로비 서버 Connect
	Network::GetApp()->CreateSocket( SERVER_TYPE::SV_LOBBY );
	Network::GetApp()->CreateSocket( SERVER_TYPE::SV_BATTLE_TCP );
	Network::GetApp()->CreateSocket( SERVER_TYPE::SV_BATTLE_UDP );

#ifdef DEBUG_SERVER
	// 게임시작시 로비 커넥트 요청
	Network::GetApp()->ConnectServer( SERVER_TYPE::SV_LOBBY );
#endif

	while( running )
	{
		if( lobbyRunning )
			Network::GetApp()->RecvLobbyServer();
		if( battleTCPRunning )
		{
			Network::GetApp()->RecvBattleServerTCP();
		}
	}
}

//void Service::RunMainThread()
//{
//	if (m_Events.empty()) return;
//
//	m_MutexDequeue.lock();
//	std::function<void()> job = std::move(m_Events.front());
//	m_Events.pop();
//	m_MutexDequeue.unlock();
//
//	cout << "run mainThread" << endl;
//	job();
//}

void Service::Notify( int sEvent, int argsCount, ... )
{
	MMRInfo mmrInfo = {};

	switch( sEvent )
	{
		//////////////////////////////////////////////////////////////////////////////////////////
		// Title Scene
		//////////////////////////////////////////////////////////////////////////////////////////
	case EVENT_TITLE_CONNECT_ALLOW:
		cout << "s: connect allow lobby" << endl;
		lobbyRunning = true;
		break;
	case EVENT_TITLE_CONNECT_DENY:
		cout << "s: connect deny lobby" << endl;
		Network::GetApp()->DisconnectServer( SERVER_TYPE::SV_LOBBY );
		break;
	case EVENT_TITLE_LOGIN_REQUEST:
	{
		cout << "s: join request" << endl;

		std::string lobbyID = "";
		std::string password = "";

		va_list ap; // 가변인자 리스트에 저장해 놓은것?
		va_start( ap, argsCount );
		lobbyID = va_arg( ap, std::string );	// 순서대로 들어온다.
		password = va_arg( ap, std::string );
		va_end( ap );

		Network::GetApp()->SendLobbyLoginPacket( lobbyID, password );
		break;
	}
	case EVENT_TITLE_LOGIN_ALLOW:
		cout << "s: join allow" << endl;
		SceneManager::GetApp()->SendEvent( SceneType::Title, EVENT_TITLE_LOGIN_ALLOW );
		break;
	case EVENT_TITLE_LOGIN_DENY:
		cout << "s: join deny" << endl;
		SceneManager::GetApp()->SendEvent( SceneType::Title, EVENT_TITLE_LOGIN_DENY );
		break;

	case EVENT_TITLE_SIGNUP_SUCCESS:
		SceneManager::GetApp()->SendEvent( SceneType::Title, EVENT_TITLE_SIGNUP_SUCCESS );
		break;

	case EVENT_TITLE_SIGNUP_DENY:
		SceneManager::GetApp()->SendEvent( SceneType::Title, EVENT_TITLE_SIGNUP_DENY );
		break;

	case EVENT_TITLE_SIGNUP_REQUEST:
	{
		string id;
		string pw;

		va_list ap; // 가변인자 리스트에 저장해 놓은것?
		va_start( ap, argsCount );
		id = va_arg( ap, std::string );	// 순서대로 들어온다.
		pw = va_arg( ap, std::string );
		va_end( ap );
		Network::GetApp()->SendLobbySingUpPacket( id, pw );
		break;
	}

	//////////////////////////////////////////////////////////////////////////////////////////
	// Lobby Scene
	//////////////////////////////////////////////////////////////////////////////////////////
	case EVENT_LOBBY_AUTOMATCH_REQUEST:
		cout << "s: automatch request" << endl;
		// 로비서버에서 lc_match 패킷을 보내줌
		Network::GetApp()->SendAutoMatchPacket();
		break;
	case EVENT_LOBBY_AUTOMATCH_SUCCESS:
	{
		cout << "s: automatch success & connect battle server" << endl;
		Network::GetApp()->ConnectServer(SV_BATTLE_TCP);
		Network::GetApp()->ConnectServer(SV_BATTLE_UDP);

		//lobbyRunning = false;
		battleTCPRunning = true;
		battleUDPRunning = true;

		// 로비서버에서 lc_match 패킷을 보내줌
		// lc_match 패킷을 받으면 connect요청을함
		Network::GetApp()->SendBattleLoginPacket(MATCH_TYPE_AUTO);
		break;
	}
	case EVENT_LOBBY_AUTOMATCH_CANCEL_REQUEST:
	{
		Network::GetApp()->SendAutoMatchCancelPacket();
		break;
	}
	case EVENT_LOBBY_AUTOMATCH_CANCEL_SUCCESS:
	{
		SceneManager::GetApp()->SendEvent(SceneType::Lobby, EVENT_LOBBY_AUTOMATCH_CANCEL_SUCCESS);
		break;
	}
	case EVENT_LOBBY_MANUALMATCH_REQUEST:
		cout << "s: manualmatch request & connect battle server" << endl;
		// 1. Battle Server Connect
		if( Network::GetApp()->m_Client.battleID == -1 ) {
			Network::GetApp()->ConnectServer( SERVER_TYPE::SV_BATTLE_TCP );
			Network::GetApp()->ConnectServer( SERVER_TYPE::SV_BATTLE_UDP );
			//lobbyRunning = false;
			battleTCPRunning = true;
			battleUDPRunning = true;
		}
		// 2. CB_LOGIN을 보내줘야함(id)
		Network::GetApp()->SendBattleLoginPacket( MATCH_TYPE_MANUAL );
		break;

	case EVENT_LOBBY_MANUALMATCH_ACCEPTOK:
	{
		cout << "s: manualmatch accept ok" << endl;

		// 3. CB_REQUEST_ROOM을 보냄(룸 리스트를 받기위해) 
		Network::GetApp()->SendRequestManualRoomListPacket();
		break;
	}

	case EVENT_LOBBY_AUTOMATCH_ACCEPTOK:
	{
		cout << "s: automatch accept ok" << endl;

		int roomNum;
		bool isHost;

		va_list ap;
		va_start( ap, argsCount );
		roomNum = va_arg( ap, int );
		isHost = va_arg( ap, bool );
		va_end( ap );

		Network::GetApp()->SendBattleRoomJoinPacket( roomNum );
		break;
	}


	case EVENT_LOBBY_MANUALMATCH_ROOMLIST_REQUEST:
		Network::GetApp()->SendRequestManualRoomListPacket();
		break;
	case EVENT_LOBBY_MANUALMATCH_ROOM_LIST:
		// 4. Room list 도착 -> UI roomlist 띄워줌
		SceneManager::GetApp()->SendEvent( SceneType::Lobby, EVENT_LOBBY_MANUALMATCH_ROOM_LIST );
		break;
	case EVENT_LOBBY_MANUALMATCH_ROOM_JOIN:
	{
		// manual room join, 방번호 필요
		int joinRoomNum = NULL;
		va_list ap;
		va_start( ap, argsCount );
		joinRoomNum = va_arg( ap, int );
		va_end( ap );

		Network::GetApp()->SendBattleRoomJoinPacket( joinRoomNum );
		break;
	}
	case EVENT_LOBBY_MANUALMATCH_ROOM_MAKE:
	{
		cout << "s: manual room make" << endl;
		wstring title;
		va_list ap;
		va_start( ap, argsCount );
		title = va_arg( ap, wstring );
		va_end( ap );

		Network::GetApp()->SendBattleRoomMakePacket( title );
		break;
	}

	case EVENT_LOBBY_MANUALMATCH_ROOM_JOIN_ALLOW:
		SceneManager::GetApp()->SendEvent( SceneType::Lobby, EVENT_LOBBY_MANUALMATCH_ROOM_JOIN_ALLOW );
		break;

	case EVENT_LOBBY_MANUALMATCH_ROOM_JOIN_DENY:
	{
		cout << "s: manual room join deny" << endl;

		int deny_code;
		char dCode;
		va_list ap;
		va_start( ap, argsCount );
		dCode = va_arg( ap, char );
		va_end( ap );

		deny_code = (int)(dCode);

		SceneManager::GetApp()->SendEventArgs( SceneType::Lobby, EVENT_LOBBY_MANUALMATCH_ROOM_JOIN_DENY, 1, deny_code );
		break;
	}

	case EVENT_LOBBY_UPDATE_USERINFO:
	{
		string id_str;
		int mmr;
		int killCnt;
		int totalCnt;
		double winRate;

		va_list ap;
		va_start( ap, argsCount );
		id_str = va_arg( ap, string );
		mmr = va_arg( ap, int );
		killCnt = va_arg( ap, int );
		totalCnt = va_arg( ap, int );
		winRate = va_arg( ap, double );
		va_end( ap );

		SceneManager::GetApp()->SendEventArgs( SceneType::Lobby, EVENT_LOBBY_UPDATE_USERINFO, 5, id_str, mmr, killCnt, totalCnt, winRate );
		break;
	}

	case EVENT_LOBBY_REQUEST_USERINFO:
	{
		Network::GetApp()->SendRequestUserInfoPacket();
		break;
	}

	//////////////////////////////////////////////////////////////////////////////////////////
	// Gameroom Scene
	//////////////////////////////////////////////////////////////////////////////////////////
	case EVENT_GAMEROOM_PLAYER_INPUT_READY:
		//cout << "s: send ready_packet" << endl;
		Network::GetApp()->SendReadyPacket();
		break;

	case EVENT_GAMEROOM_PLAYER_CALLBACK_READY:
	{
		int argID;
		char argReady;

		va_list ap;
		va_start( ap, argsCount );
		argID = va_arg( ap, int );
		argReady = va_arg( ap, char );
		va_end( ap );

		SceneManager::GetApp()->SendEventArgs( SceneType::GameRoom, EVENT_GAMEROOM_PLAYER_CALLBACK_READY, 2, argID, argReady );
		break;
	}
	case EVENT_GAMEROOM_PLAYER_ENTER:
	{
		cout << "s: player enter" << endl;

		int argID;
		va_list ap;
		va_start( ap, argsCount );
		argID = va_arg( ap, int );
		va_end( ap );

		SceneManager::GetApp()->SendEventArgs( SceneType::GameRoom, EVENT_GAMEROOM_PLAYER_ENTER, argsCount, argID );
		break;
	}
	case EVENT_GAMEROOM_PLAYER_LEAVE:
	{
		cout << "s: player exit" << endl;

		int argID;
		va_list ap;
		va_start( ap, argsCount );
		argID = va_arg( ap, int );
		va_end( ap );

		SceneManager::GetApp()->SendEventArgs( SceneType::GameRoom, EVENT_GAMEROOM_PLAYER_LEAVE, argsCount, argID );
		break;
	}
	case EVENT_GAMEROOM_PLAYER_HOST:
	{
		int argID;
		va_list ap;
		va_start( ap, argsCount );
		argID = va_arg( ap, int );
		va_end( ap );

		SceneManager::GetApp()->SendEventArgs( SceneType::GameRoom, EVENT_GAMEROOM_PLAYER_HOST, argsCount, argID );

		break;
	}
	case EVENT_GAMEROOM_INPUT_SELECT_CHARACTER:
	{
		std::string selectedMesh;
		va_list ap;
		va_start( ap, argsCount );
		selectedMesh = va_arg( ap, std::string );
		va_end( ap );

		Network::GetApp()->SendSelectPacket( selectedMesh );
		break;
	}
	case EVENT_GAMEROOM_INPUT_SELECT_CANCEL_CHARACTER:
	{
		std::string selectedMesh;
		va_list ap;
		va_start( ap, argsCount );
		selectedMesh = va_arg( ap, std::string );
		va_end( ap );

		Network::GetApp()->SendSelectCancelPacket( selectedMesh );
		break;
	}
	case EVENT_GAMEROOM_CALLBACK_SELECT_CHARACTER:
	{
		int id;
		std::string selectMesh;
		va_list ap;
		va_start( ap, argsCount );
		id = va_arg( ap, int );
		selectMesh = va_arg( ap, std::string );
		va_end( ap );

		SceneManager::GetApp()->SendEventArgs( SceneType::GameRoom, EVENT_GAMEROOM_CALLBACK_SELECT_CHARACTER, 2, id, selectMesh );
		break;
	}
	case EVENT_GAMEROOM_CALLBACK_SELECT_CANCEL_CHARACTER:
	{
		int id;
		std::string selectCancelMesh;
		va_list ap;
		va_start( ap, argsCount );
		id = va_arg( ap, int );
		selectCancelMesh = va_arg( ap, std::string );
		va_end( ap );

		SceneManager::GetApp()->SendEventArgs( SceneType::GameRoom, EVENT_GAMEROOM_CALLBACK_SELECT_CANCEL_CHARACTER, 2, id, selectCancelMesh );
		break;
	}
	case EVENT_GAMEROOM_EXIT:
	{
		Network::GetApp()->SendRoomLeavePacket();
		break;
	}
	case EVENT_GAMEROOM_CALLBACK_MAPINFO:
	{
		int mapType = NULL;
		va_list ap;
		va_start( ap, argsCount );
		mapType = va_arg( ap, int );
		va_end( ap );

		SceneManager::GetApp()->SendEventArgs( SceneType::GameRoom, EVENT_GAMEROOM_CALLBACK_MAPINFO, 1, mapType );
		break;
	}
	case EVENT_GAMEROOM_INPUT_MAPINFO:
	{
		int mapType = NULL;
		va_list ap;
		va_start( ap, argsCount );
		mapType = va_arg( ap, int );
		va_end( ap );
		Network::GetApp()->SendMapTypePacket( mapType );
		break;
	}
	case EVENT_GAMEROOM_GAME_START:
	{
		cout << "s: send game_start_packet" << endl;
		Network::GetApp()->SendGameStartPacket();
		break;
	}

	case EVENT_GAMEROOM_GAMESTART_AVAILABLE:
	{
		cout << "s: gamestart is available" << endl;
		bool available{};
		va_list ap;
		va_start( ap, argsCount );
		available = va_arg( ap, bool );
		va_end( ap );
		SceneManager::GetApp()->SendEventArgs( SceneType::GameRoom, EVENT_GAMEROOM_GAMESTART_AVAILABLE, 1, available );
		break;
	}

	case EVENT_LOADING_GAMEPLAY_INFO:
	{
		cout << "s: loading gameplay info" << endl;

		SceneManager::GetApp()->SendEvent( SceneType::GameRoom, EVENT_LOADING_GAMEPLAY_INFO );
		break;
	}
	case EVENT_GAMEPLAY_ROUND_START:
	{
		SceneManager::GetApp()->SendEvent( SceneType::GamePlay, EVENT_GAMEPLAY_ROUND_START );
		break;
	}

	case EVENT_GAMEROOM_SEND_CHAT:
	{
		cout << "s: send chat buf" << endl;
		wchar_t* chat;
		va_list ap;
		va_start( ap, argsCount );
		chat = va_arg( ap, wchar_t* );
		va_end( ap );
		Network::GetApp()->SendChat( chat );
		break;
	}

	case EVENT_GAMEROOM_RECV_CHAT:
	{
		cout << "s: recv chat buf" << endl;
		wstring chatBuf;
		int id;
		va_list ap;
		va_start( ap, argsCount );
		chatBuf = va_arg( ap, wchar_t*);
		id = va_arg( ap, int );
		va_end( ap );

		char tmpbuf[400];
		WideCharToMultiByte(CP_ACP, 0, chatBuf.c_str(), -1, tmpbuf, chatBuf.size() * 2, 0, 0);
		int len = chatBuf.size() * 2;
		tmpbuf[len] = '\0';
		string strBuf{ tmpbuf };

		SceneManager::GetApp()->SendEventArgs( SceneType::GameRoom, EVENT_GAMEROOM_RECV_CHAT, 2, strBuf, id );
		break;
	}

	//////////////////////////////////////////////////////////////////////////////////////////
	// Gameplay Scene
	//////////////////////////////////////////////////////////////////////////////////////////
	case  EVENT_GAMEPLAY_START:
	{
		/* 게임 시작할때 필요한 정보 */
		// battleID: 배틀아이디
		// PlayerType: 플레이어 역할
		// PlayerMesh: 플레이어 메쉬
		// SpawnLocation: 스폰위치
		{
			std::lock_guard<std::mutex> lock( m_MutexEvent );
			for( auto& bc : Network::GetApp()->m_BattleClients ) {
				SceneManager::GetApp()->SendEventArgs( SceneType::GamePlay, EVENT_GAMEPLAY_START, 4, bc.second->battleID, bc.second->m_PlayerType, bc.second->m_PlayerMesh, bc.second->m_SpawnLocation );
				cout << "Service::EVENT_GAMEPLAY_START - 플레이어 생성" << endl;
			}
		}

		break;
	}
	case EVENT_GAMEPLAY_INPUT_MOVE:
	{
		int virtualKey = NULL;
		va_list ap;
		va_start( ap, argsCount );
		virtualKey = va_arg( ap, int );
		va_end( ap );

		Network::GetApp()->SendMovePacket( virtualKey );
		break;
	}
	case EVENT_GAMEPLAY_CALLBACK_MOVE:
	{
		// Temp Data
		int argID;
		XMFLOAT3 argPos;

		va_list ap;
		va_start( ap, argsCount );
		argID = va_arg( ap, int );
		argPos = va_arg( ap, XMFLOAT3 );
		va_end( ap );

		SceneManager::GetApp()->SendEventArgs( SceneType::GamePlay, EVENT_GAMEPLAY_CALLBACK_MOVE, 2, argID, argPos );
		break;
	}
	case EVENT_GAMEPLAY_INPUT_MOUSE:
	{
		va_list ap;
		va_start( ap, argsCount );
		argLook = va_arg( ap, XMFLOAT3 );
		va_end( ap );

		Network::GetApp()->SendLookVectorPacket( argLook );
		break;
	}
	case EVENT_GAMEPLAY_CALLBACK_MOUSE:
	{
		int argID = 0;

		va_list ap;
		va_start( ap, argsCount );
		argID = va_arg( ap, int );
		argLookCallback = va_arg( ap, XMFLOAT3 );
		va_end( ap );

		SceneManager::GetApp()->SendEventArgs( SceneType::GamePlay, EVENT_GAMEPLAY_CALLBACK_MOUSE, 2, argID, argLookCallback );
		break;
	}
	case EVENT_GAMEPLAY_OBJECT_POS:
	{
		std::string argMeshName;
		std::string argInstID;
		XMFLOAT3 argObjPos;

		va_list ap;
		va_start( ap, argsCount );
		argMeshName = va_arg( ap, std::string );
		argInstID = va_arg( ap, std::string );
		argObjPos = va_arg( ap, XMFLOAT3 );
		va_end( ap );

		SceneManager::GetApp()->SendEventArgs( SceneType::GamePlay, EVENT_GAMEPLAY_OBJECT_POS, 3, argMeshName, argInstID, argObjPos );
		break;
	}
	//case EVENT_GAMEPLAY_OBJECT_LOOK:
	//{
	//	break;
	//}
	case EVENT_GAMEPLAY_TRAMNSFORM_SEND:
	{
		int argServerMeshID;

		va_list ap;
		va_start( ap, argsCount );
		argServerMeshID = va_arg( ap, int );
		va_end( ap );

		Network::GetApp()->SendTransformPacket( argServerMeshID );
		break;
	}
	case EVENT_GAMEPLAY_TRAMNSFORM_RECV:
	{
		int argClientID;
		std::string serverMeshID;

		va_list ap;
		va_start( ap, argsCount );
		argClientID = va_arg( ap, int );
		serverMeshID = va_arg( ap, std::string );
		va_end( ap );

		SceneManager::GetApp()->SendEventArgs( SceneType::GamePlay, EVENT_GAMEPLAY_TRAMNSFORM_RECV, argsCount, argClientID, serverMeshID );
		break;
	}
	case EVENT_GAMEPLAY_PUT_THUNDERBOLT:
	{
		int boltID;
		XMFLOAT3 putPos;

		va_list ap;
		va_start( ap, argsCount );
		boltID = va_arg( ap, int );
		putPos = va_arg( ap, XMFLOAT3 );
		va_end( ap );

		SceneManager::GetApp()->SendEventArgs( SceneType::GamePlay, EVENT_GAMEPLAY_PUT_THUNDERBOLT, argsCount, boltID, putPos );
		break;
	}
	case EVENT_GAMEPLAY_REMOVE_THUNDERBOLT:
	{
		int boltID;

		va_list ap;
		va_start( ap, argsCount );
		boltID = va_arg( ap, int );
		va_end( ap );

		SceneManager::GetApp()->SendEventArgs( SceneType::GamePlay, EVENT_GAMEPLAY_REMOVE_THUNDERBOLT, argsCount, boltID );
		break;
	}

	case EVENT_GAMEPLAY_HIT:
	{
		int id;
		double hp;

		va_list ap;
		va_start( ap, argsCount );
		id = va_arg( ap, int );
		hp = va_arg( ap, double );
		va_end( ap );

		cout << "Service => " << hp << endl;

		SceneManager::GetApp()->SendEventArgs( SceneType::GamePlay, EVENT_GAMEPLAY_HIT, argsCount, id, hp );
		break;
	}

	case EVENT_GAMEPLAY_ANIMATE:
	{
		// 20.04.09 박태준 - 배틀서버에서 매 프레임? 마다 나를 제외한 다른 플레이어들이 수행할 애니메이션 타입을 전송함.
		int id; // 애니메이션 수행할 배틀클라이언트
		char anim_type; // 수행할 애니메이션 타입

		va_list ap;
		va_start( ap, argsCount );
		id = va_arg( ap, int );
		anim_type = va_arg( ap, char );
		va_end( ap );

		SceneManager::GetApp()->SendEventArgs( SceneType::GamePlay, EVENT_GAMEPLAY_ANIMATE, argsCount, id, anim_type );
		break;
	}
	case EVENT_GAMEPLAY_PLAYER_DIE:
	{
		int id; // 애니메이션 수행할 배틀클라이언트

		va_list ap;
		va_start( ap, argsCount );
		id = va_arg( ap, int );
		va_end( ap );

		SceneManager::GetApp()->SendEventArgs( SceneType::GamePlay, EVENT_GAMEPLAY_PLAYER_DIE, argsCount, id );
		break;
	}
	case EVENT_GAMEPLAY_TIMER:
	{
		int timer; // 애니메이션 수행할 배틀클라이언트

		va_list ap;
		va_start( ap, argsCount );
		timer = va_arg( ap, int );
		va_end( ap );

		SceneManager::GetApp()->SendEventArgs( SceneType::GamePlay, EVENT_GAMEPLAY_TIMER, argsCount, timer );
		break;
	}
	case EVENT_GAMEPLAY_GAMEOVER:
	{
		SceneManager::GetApp()->SendEvent( SceneType::GamePlay, EVENT_GAMEPLAY_GAMEOVER );
		break;
	}
	case EVENT_GAMEPLAY_PENALTY:
	{
		int targetId;
		bool isPenalty;

		va_list ap;
		va_start( ap, argsCount );
		targetId = va_arg( ap, int );
		isPenalty = va_arg( ap, bool );
		va_end( ap );

		SceneManager::GetApp()->SendEventArgs( SceneType::GamePlay, EVENT_GAMEPLAY_PENALTY, argsCount, targetId, isPenalty );
		break;
	}
	case EVENT_GAMEPLAY_PENALTY_WARNING:
	{
		SceneManager::GetApp()->SendEvent( SceneType::GamePlay, EVENT_GAMEPLAY_PENALTY_WARNING );
		break;
	}

	case EVENT_GAMEPLAY_PENALTY_WARNING_END:
	{
		SceneManager::GetApp()->SendEvent( SceneType::GamePlay, EVENT_GAMEPLAY_PENALTY_WARNING_END );
		break;
	}
	//////////////////////////////////////////////////////////////////////////////////////////
	// Gameresult Scene
	//////////////////////////////////////////////////////////////////////////////////////////
	case EVENT_LOADING_GAMERESULT_INFO:
	{
		string chrName;

		va_list ap;
		va_start( ap, argsCount );
		chrName = va_arg( ap, std::string );
		va_end( ap );

		SceneManager::GetApp()->SendEventArgs( SceneType::GameResult, EVENT_LOADING_GAMERESULT_INFO, argsCount, chrName );
		break;
	}
	}
}

// 배열 넘어오면 쓰시오.
void Service::GetRoomList( std::vector<PTC_Room>& ptcRoomlist )
{
	Network::GetApp()->m_SwapRoomList.swap( ptcRoomlist );
}

int Service::GetMyBattleID() const
{
	return Network::GetApp()->m_Client.battleID;
}

BattleClient* Service::GetBattleClient( int id ) const
{
	if( !Network::GetApp()->m_BattleClients.count( id ) )
		return nullptr;
	return Network::GetApp()->m_BattleClients[id].get();
}

int Service::GetBattleClientsCount() const
{
	return Network::GetApp()->m_BattleClients.size();
}

int Service::GetMyMapInfo() const
{
	return Network::GetApp()->m_MapInfo;
}

int Service::GetWinTeam() const
{
	return Network::GetApp()->m_WinTeam;
}

short Service::GetPenaltyDuration() const
{
	return Network::GetApp()->m_PenaltyDuration;
}

//void Service::EnqueueEvent(std::function<void()> fp)
//{
//	m_MutexEnqueue.lock();
//	m_Events.push(std::move(fp));
//	m_MutexEnqueue.unlock();
//}

