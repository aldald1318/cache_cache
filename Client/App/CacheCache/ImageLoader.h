#pragma once

namespace Core
{
	extern Microsoft::WRL::ComPtr<ID3D12Device> g_Device;
	extern Microsoft::WRL::ComPtr<ID3D12CommandQueue> g_CommandQueue;
	extern Microsoft::WRL::ComPtr<ID3D12CommandAllocator> g_DirectCmdListAlloc;
	extern Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList> g_CommandList;

	extern int g_DisplayWidth;
	extern int g_DisplayHeight;
}

namespace DirectX
{
	HRESULT CreateFontFromImage(std::string fileName, ID3D12DescriptorHeap* SrvDescriptorHeap);

	int LoadImageDataFromFile(BYTE** imageData, D3D12_RESOURCE_DESC& resourceDescription, LPCWSTR filename, int& bytesPerRow);
	Text LoadFont(std::string filename, int windowWidth, int windowHeight);
}