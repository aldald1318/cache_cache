#include "pch.h"
#include "Map.h"

Map::~Map()
{
	mapInfoDic.clear();
	mapInfoVector.clear();
	playerInfoVector.clear();
	uiInfoVector.clear();

	// 기타 기능 컨테이너
	propTypeVector.clear();
	mapInfoChangeableVector.clear();
}
