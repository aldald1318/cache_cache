#pragma once
#include "IGameApp.h"
#include "Singleton.h"
#include "Camera.h"

namespace Core
{
	extern HWND g_hMainWnd;
	extern HWND g_hVideoWnd;

	extern Microsoft::WRL::ComPtr<ID3D12Device> g_Device;
	extern Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList> g_CommandList;

	extern int g_DisplayWidth;
	extern int g_DisplayHeight;
}

class SceneManager;
class SoundManager;
class ApplicationContext;
class AssertsReference;
class CommandCenter;
class ParticleSystem;
class CACHE_CACHE : public IGameApp, public TemplateSingleton<CACHE_CACHE>
{
public:
	virtual void Startup(void) override;
	virtual void Cleanup(void) override;
	virtual void Update(float deltaT) override;
	virtual void RenderScene(void) override;
	virtual void RenderUI(void) override;
	virtual void WriteShadow(void) override;

	void OnResize();

private:
	void BuildAsserts();
	void BuildCharactersAndParticles();

public:
	Camera* m_Camera;
	std::map<std::string, std::unique_ptr<Light>> m_Lights;

private:
	SceneManager* m_SceneManager;
	ApplicationContext* m_AppContext;
	AssertsReference* m_AssetsRef;
	SoundManager* m_SoundManager;
	CommandCenter* m_CommandCenter;
	ParticleSystem* m_ParticleSystem;
};

