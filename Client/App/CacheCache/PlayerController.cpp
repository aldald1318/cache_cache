#include "pch.h"
#include "PlayerController.h"
#include "InputHandler.h"
#include "Picking.h"
#include "Service.h"
#include "Camera.h"
#include "GameTimer.h"

#include "Character.h"
#include "SkinnedModelInstance.h"
#include "CommandCenter.h"
#include "MoveCommand.h"

PlayerController::PlayerController(Character* player) :
	m_Owner(player)
{
	CommandCenter::GetApp()->PushCommand<MoveCommand>(static_cast<int>(MoveState::Idle), m_Owner);
}

void PlayerController::Update(const float deltaT)
{
	MouseCallback();
	HandleInput(deltaT);

	OnKeyPressed();
	OnKeyReleased();
}

void PlayerController::HandleInput(const float deltaT)
{
	if (!m_Owner) return;
	if (!m_Owner->m_MyCamera) return;

	float speed = 300 * deltaT;
	XMVECTOR direction = {};

	DWORD dir = 0;


#ifdef DEBUG_CLIENT
	switch (m_Owner->m_MyCamera->GetCameraType())
	{
	case CameraType::First:
	case CameraType::Third:
	{
		switch (m_Owner->m_PlayerRole)
		{
		case ROLE_STUDENT:	// ROLE_STUDENT
			if (GetAsyncKeyState('W') & 0x8000) dir |= DIR_FORWARD;
			if (GetAsyncKeyState('S') & 0x8000) dir |= DIR_BACKWARD;
			if (GetAsyncKeyState('A') & 0x8000) dir |= DIR_LEFT;
			if (GetAsyncKeyState('D') & 0x8000) dir |= DIR_RIGHT;

			if (dir != 0)
				m_Owner->Move(dir, speed);
			break;
		case ROLE_MASTER:	// ROLE_MASTER
			if (GetAsyncKeyState('W') & 0x8000) {
				m_Owner->Move(DIR_FORWARD, speed, true);
			}
			if (GetAsyncKeyState('S') & 0x8000) {
				m_Owner->Move(DIR_BACKWARD, speed, true);
			}
			if (GetAsyncKeyState('A') & 0x8000) {
				m_Owner->Move(DIR_LEFT, speed, true);
			}
			if (GetAsyncKeyState('D') & 0x8000) {
				m_Owner->Move(DIR_RIGHT, speed, true);
			}
			break;
		}
	}
	break;
	case CameraType::Free:
	{
		if (GetAsyncKeyState('W') & 0x8000) {
			m_Owner->m_MyCamera->Walk(speed);
		}
		if (GetAsyncKeyState('S') & 0x8000) {
			m_Owner->m_MyCamera->Walk(-speed);
		}
		if (GetAsyncKeyState('A') & 0x8000) {
			m_Owner->m_MyCamera->Strafe(-speed);
		}
		if (GetAsyncKeyState('D') & 0x8000) {
			m_Owner->m_MyCamera->Strafe(speed);
		}
		if (GetAsyncKeyState(VK_SPACE) & 0x8000) {
			m_Owner->m_MyCamera->Up(speed);
		}
		if (GetAsyncKeyState('C') & 0x8000) {
			m_Owner->m_MyCamera->Up(-speed);
		}
	}
	break;
	}
#elif DEBUG_SERVER
	switch (m_Owner->m_MyCamera->GetCameraType())
	{
	case CameraType::First:
	case CameraType::Third:
	{
		switch (m_Owner->m_PlayerRole)
		{
		case ROLE_STUDENT:	// ROLE_STUDENT
			if (GetAsyncKeyState('W') & 0x8000) dir |= DIR_FORWARD;
			if (GetAsyncKeyState('S') & 0x8000) dir |= DIR_BACKWARD;
			if (GetAsyncKeyState('A') & 0x8000) dir |= DIR_LEFT;
			if (GetAsyncKeyState('D') & 0x8000) dir |= DIR_RIGHT;

			if (dir != 0)
				if (m_Owner->Move(dir, speed))
				{
					Service::GetApp()->Notify(EVENT_GAMEPLAY_INPUT_MOUSE, 1, m_Owner->GetLook());
				};

			dir = 0;

			break;
		}
	}
	break;
	default:
		break;
	}
#endif
	if (InputHandler::IsKeyUp('T'))
	{
		if (m_Owner->m_IsThirdCamera)
		{
			m_Owner->SetCamera(CameraType::First);
			m_Owner->m_IsThirdCamera = !m_Owner->m_IsThirdCamera;
			return;
		}
		else if (!m_Owner->m_IsThirdCamera)
		{
			m_Owner->SetCamera(CameraType::Third);
			m_Owner->m_IsThirdCamera = !m_Owner->m_IsThirdCamera;
			return;
		}
	}
}

void PlayerController::MouseCallback()
{
	if (!m_Owner) return;
	if (!m_Owner->m_MyCamera) return;

	// 마우스 이동
	if (InputHandler::g_MoveMouseCallback)
	{
		if (m_Owner->m_PlayerRole == ROLE_STUDENT)
		{
			PickedObject po = Picking::RayIntersectContour(Core::g_DisplayWidth / 2.f, Core::g_DisplayHeight / 2.f, m_MapName);
			if (m_Owner->m_ContourObj.distance != po.distance)
				m_Owner->m_ContourObj = po;
			// cout << m_Owner->m_BattleServerID << ": " << m_Owner->m_ContourObj.objectName << endl;
		}

		// 1인칭 회전(자전)
		if (m_Owner->m_MyCamera->GetCameraType() == CameraType::First)
		{
			if (InputHandler::g_MouseChangebleY != 0.0f)
			{
				m_Owner->m_MyCamera->Rotate(InputHandler::g_MouseChangebleY, 0.f, 0.f);
			}
			if (InputHandler::g_MouseChangebleX != 0.f)
			{
				if (m_Owner->m_PlayerRole == ROLE_MASTER)
					m_Owner->Rotate(0, InputHandler::g_MouseChangebleX, 0);
				m_Owner->m_MyCamera->Rotate(0.f, InputHandler::g_MouseChangebleX, 0.f);
			}
		}
		// 3인칭 회전(공전)
		else if (m_Owner->m_MyCamera->GetCameraType() == CameraType::Third)
		{
			if (InputHandler::g_MouseChangebleY != 0.0f) {
				m_Owner->m_MyCamera->Rotate(InputHandler::g_MouseChangebleY, 0, 0);
			}

			if (InputHandler::g_MouseChangebleX != 0.0f)
			{
				if (m_Owner->m_PlayerRole == ROLE_MASTER)
					m_Owner->Rotate(0, InputHandler::g_MouseChangebleX, 0);
				m_Owner->m_MyCamera->Rotate(0, InputHandler::g_MouseChangebleX, 0);
			}
		}
		// 서버에게 자신의 룩벡터 보내기(UDP)
		if (m_Owner->m_PlayerRole == ROLE_MASTER)
			Service::GetApp()->Notify(EVENT_GAMEPLAY_INPUT_MOUSE, 1, m_Owner->GetLook());

		// 마우스이동 처리
		POINT ptMouse = { Core::g_DisplayWidth / 2,Core::g_DisplayHeight / 2 };
		ClientToScreen(Core::g_hMainWnd, &ptMouse);
		SetCursorPos(ptMouse.x, ptMouse.y);
		InputHandler::g_MoveMouseCallback = false;
	}

	// 스승일때 활성화(공격)
	if (InputHandler::g_LeftMouseCallback)
	{
		switch (m_Owner->m_PlayerRole)
		{
		case ROLE_MASTER:
		{
			if (CommandCenter::GetApp()->m_StartJumpAnim) break;

			CommandCenter::GetApp()->PushCommand<MoveCommand>(static_cast<int>(MoveState::Attack), m_Owner);
			CommandCenter::GetApp()->m_StartAttackAnim = true;

			// 공격애니메이션을 변경하던, 총알 발사에 쿨타임 걸어놓던 애니메이션과 투사체 발사가 어느정도 맞게 조정하자.
			if (CommandCenter::GetApp()->m_AttackCoolTimeSwitch)
			{
				// cout << "Shoot" << endl;

				XMFLOAT3 tmp = m_Owner->GetThunderBoltDirection(Core::g_DisplayWidth / 2.f, Core::g_DisplayHeight / 2.f);
				Network::GetApp()->SendThunderboltPacket(tmp);
				CommandCenter::GetApp()->m_AttackCoolTimeSwitch = false;
			}

			break;
		}
		case ROLE_STUDENT:
		{
			// 변신 로딩시간 추가 해야함
			PickedObject po = Picking::RayIntersect(Core::g_DisplayWidth / 2.f, Core::g_DisplayHeight / 2.f, m_MapName);
			if (po.objectName != "" && po.serverMeshID != 0 && po.serverMeshID < 300) {
				Service::GetApp()->Notify(EVENT_GAMEPLAY_TRAMNSFORM_SEND, 1, po.serverMeshID);
			}
			break;
		}
		}

		InputHandler::g_LeftMouseCallback = false;
	}

	// 변신푸는기능
	if (InputHandler::g_RightMouseOverlap && m_Owner->m_PlayerRole == ROLE_STUDENT)
	{
		if (m_Owner->m_TransformType == TransformType::Character) return;

		InputHandler::g_MouseClickTime += GameTimer::GetApp()->DeltaTime();
		if (InputHandler::g_MouseClickTime > RELEASE_TRANSFORM_COOLTIME)
		{
			// cout << "변신해제" << endl;
			Service::GetApp()->Notify(EVENT_GAMEPLAY_TRAMNSFORM_SEND, 1, OBJECT_TYPE_CHARACTER);

			InputHandler::g_MouseClickTime = 0;
			InputHandler::g_RightMouseOverlap = false;
		}
	}
}

void PlayerController::OnKeyPressed()
{
	if (!m_Owner) return;
	if (!m_Owner->m_MyCamera) return;

	if (CommandCenter::GetApp()->m_StartJumpAnim) return;

	switch (m_Owner->m_MyCamera->GetCameraType())
	{
	case CameraType::First:
	case CameraType::Third:
		if (InputHandler::IsKeyDown('W'))
		{
			CommandCenter::GetApp()->PushCommand<MoveCommand>(static_cast<int>(MoveState::Forward), m_Owner);
			Service::GetApp()->Notify(EVENT_GAMEPLAY_INPUT_MOVE, 1, CB_KEY_W_DOWN);
		}
		if (InputHandler::IsKeyDown('S'))
		{
			CommandCenter::GetApp()->PushCommand<MoveCommand>(static_cast<int>(MoveState::Backward), m_Owner);
			Service::GetApp()->Notify(EVENT_GAMEPLAY_INPUT_MOVE, 1, CB_KEY_S_DOWN);
		}
		if (InputHandler::IsKeyDown('A'))
		{
			CommandCenter::GetApp()->PushCommand<MoveCommand>(static_cast<int>(MoveState::LeftStrafe), m_Owner);
			Service::GetApp()->Notify(EVENT_GAMEPLAY_INPUT_MOVE, 1, CB_KEY_A_DOWN);
		}
		if (InputHandler::IsKeyDown('D'))
		{
			CommandCenter::GetApp()->PushCommand<MoveCommand>(static_cast<int>(MoveState::RightStrafe), m_Owner);
			Service::GetApp()->Notify(EVENT_GAMEPLAY_INPUT_MOVE, 1, CB_KEY_D_DOWN);
		}
		if (InputHandler::IsKeyDown(VK_SPACE))
		{
			CommandCenter::GetApp()->PushCommand<MoveCommand>(static_cast<int>(MoveState::Jump), m_Owner);
			CommandCenter::GetApp()->m_StartJumpAnim = true;
			Service::GetApp()->Notify(EVENT_GAMEPLAY_INPUT_MOVE, 1, CB_JUMP);
		}
		break;
	}
}

void PlayerController::OnKeyReleased()
{
	if (!m_Owner) return;
	if (!m_Owner->m_MyCamera) return;

	if (CommandCenter::GetApp()->m_StartJumpAnim) return;

	switch (m_Owner->m_MyCamera->GetCameraType())
	{
	case CameraType::First:
	case CameraType::Third:

		if (InputHandler::IsKeyUp('W'))
		{
			CommandCenter::GetApp()->PopCommand(static_cast<int>(MoveState::Forward));
			Service::GetApp()->Notify(EVENT_GAMEPLAY_INPUT_MOVE, 1, CB_KEY_W_UP);
		}
		if (InputHandler::IsKeyUp('S'))
		{
			CommandCenter::GetApp()->PopCommand(static_cast<int>(MoveState::Backward));
			Service::GetApp()->Notify(EVENT_GAMEPLAY_INPUT_MOVE, 1, CB_KEY_S_UP);
		}
		if (InputHandler::IsKeyUp('A'))
		{
			CommandCenter::GetApp()->PopCommand(static_cast<int>(MoveState::LeftStrafe));
			Service::GetApp()->Notify(EVENT_GAMEPLAY_INPUT_MOVE, 1, CB_KEY_A_UP);
		}
		if (InputHandler::IsKeyUp('D'))
		{
			CommandCenter::GetApp()->PopCommand(static_cast<int>(MoveState::RightStrafe));
			Service::GetApp()->Notify(EVENT_GAMEPLAY_INPUT_MOVE, 1, CB_KEY_D_UP);
		}
		if (InputHandler::IsKeyUp(VK_SPACE)) {}
	}
}
