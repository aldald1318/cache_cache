#include "pch.h"
#include "GameroomScene.h"
#include "Service.h"
#include "ApplicationContext.h"
#include "CommandContext.h"
#include "AssertsReference.h"
#include "CACHE_CACHE.h"

#include "Map.h"
#include "Character.h"
#include "BattleClient.h"
#include "ImageView.h"
#include "Button.h"

// 플레이어 선택창 순서
// p1:PE / p2:DR / p3:QU / p4: SO

void GameroomScene::ProcessEvent( int sEvent, int argsCount, ... )
{
	va_list ap;
	switch( sEvent )
	{
	case EVENT_GAMEROOM_PLAYER_ENTER:
	{
		// 플레이어 입장
		// 맨처음은 디폴트 메쉬
		// 하지만 플레이어가 들어왔다는 정보는 보여줘야함
		/// 플레이어 배틀아이디 
		/// 플레이어 이름 / 플레이어 MMR / 플레이어 번호(1p/2p,,) / 플레이어 메쉬이름

		int argID;
		va_start( ap, argsCount );
		argID = va_arg( ap, int );
		va_end( ap );

		// 배틀클라이언트 정보 복사
		BuildSubBattleClient( argID );

		// 이전에 들어와있는 클라이언트를 보여주고 선택한 캐릭터는 선택하지못하도록 이미지뷰 비활성화
		SubBattleClient sbClient = m_ServerData->subBattleClients[argID];

		// 들어온 사람 스폰위치에 마법진 그리기
		if( sbClient.spawnLocation == 0 )
			AppContext->DisplayMagicCircle( OBJECT_NAME_MAGIC_CIRCLE_P1, MAP_STR_GAMEROOM, sbClient.spawnLocation );
		else if( sbClient.spawnLocation == 1 )
			AppContext->DisplayMagicCircle( OBJECT_NAME_MAGIC_CIRCLE_P2, MAP_STR_GAMEROOM, sbClient.spawnLocation );
		else if( sbClient.spawnLocation == 2 )
			AppContext->DisplayMagicCircle( OBJECT_NAME_MAGIC_CIRCLE_P3, MAP_STR_GAMEROOM, sbClient.spawnLocation );
		else if( sbClient.spawnLocation == 3 )
			AppContext->DisplayMagicCircle( OBJECT_NAME_MAGIC_CIRCLE_P4, MAP_STR_GAMEROOM, sbClient.spawnLocation );

		// 이미 방에 들어와 있는 클라이언트 그려주기
		if( sbClient.playerMeshName != "" && sbClient.playerMeshName != CHARACTER_NONE )
		{
			AppContext->DisplayCharacter( MAP_STR_GAMEROOM, sbClient.playerMeshName, sbClient.spawnLocation, true );
			// 선택된 캐릭터가 있다면 캐릭터 선택 이미지뷰 UnSelected -> UnActive로 변경
			SetOtherUnSelectedPlayerImageView( sbClient.playerMeshName, true );
		}

		break;
	}
	case EVENT_GAMEROOM_PLAYER_LEAVE:
	{
		// 플레이어 퇴장
		// ID를 받아 플레이어를 그리지 않음
		int argID;
		va_start( ap, argsCount );
		argID = va_arg( ap, int );
		va_end( ap );

		if( argID != m_ServerData->playerID )
			cout << argID << "가 나갔다." << endl;

		// 마법진 지워주기
		if( m_ServerData->subBattleClients[argID].spawnLocation == 0 )
			AppContext->HiddenMagicCircle( OBJECT_NAME_MAGIC_CIRCLE_P1 );
		else if( m_ServerData->subBattleClients[argID].spawnLocation == 1 )
			AppContext->HiddenMagicCircle( OBJECT_NAME_MAGIC_CIRCLE_P2 );
		else if( m_ServerData->subBattleClients[argID].spawnLocation == 2 )
			AppContext->HiddenMagicCircle( OBJECT_NAME_MAGIC_CIRCLE_P3 );
		else if( m_ServerData->subBattleClients[argID].spawnLocation == 3 )
			AppContext->HiddenMagicCircle( OBJECT_NAME_MAGIC_CIRCLE_P4 );

		// 해당 ui정보도 초기화
		InitSelectPlayerImageView( m_ServerData->subBattleClients[argID].playerMeshName );
		AppContext->HiddenCharacter( m_ServerData->subBattleClients[argID].playerMeshName );

		// 나간 플레이어 레디상태 지워주기
		ImageView* iv = FindReadyStateImageView( m_ServerData->subBattleClients[argID].playerNum );
		if( iv ) iv->m_IsVisible = false;

		// 나간 플레이어 데이터 지워주기
		m_ServerData->subBattleClients.erase( argID );

		break;
	}
	case EVENT_GAMEROOM_PLAYER_HOST:
	{
		// 0709 현재 호스트 패킷이 안옴
		// 버그현상: 다른 클라에서 방장 표시 이미지뷰가 표시안됨(왕관이미지)

		int argID;
		va_start( ap, argsCount );
		argID = va_arg( ap, int );
		va_end( ap );

		if( !m_ServerData->subBattleClients.count( argID ) ) break;

		m_ServerData->subBattleClients[argID].host = true;
		cout << "이제 " << argID << "가 방장!" << endl;

		// 방장 상태 이미지뷰 활성화
		SetHostImageView();

		// 방장기능 업데이트 (prev / next / gamestart)
		if( m_ServerData->subBattleClients[m_ServerData->playerID].host )
		{
			//if(무슨맵인지에 따라서 활성화 비활성화 체크하기)
			switch( m_ServerData->mapType )
			{
			case MAP_TOWN:
				AppContext->FindObject<Button>( MESH_GEOID_RECT, UI_BUTTON_PREVMAP )->OnActive( false );
				AppContext->FindObject<Button>( MESH_GEOID_RECT, UI_BUTTON_NEXTMAP )->OnActive( true );
				break;
			case TEMP_SECOND_MAP:
				AppContext->FindObject<Button>( MESH_GEOID_RECT, UI_BUTTON_PREVMAP )->OnActive( true );
				AppContext->FindObject<Button>( MESH_GEOID_RECT, UI_BUTTON_NEXTMAP )->OnActive( false );
				break;
			}
		}

		break;
	}
	case EVENT_GAMEROOM_PLAYER_CALLBACK_READY:
	{
		int argID;
		char argReady;

		va_start( ap, argsCount );
		argID = va_arg( ap, int );
		argReady = va_arg( ap, char );
		va_end( ap );

		ImageView* iv = FindReadyStateImageView( m_ServerData->subBattleClients[argID].playerNum );

		if( argReady )
			iv->m_IsVisible = true;
		else
			iv->m_IsVisible = false;

		m_ServerData->subBattleClients[argID].isReady = argReady;

		break;
	}
	case EVENT_GAMEROOM_CALLBACK_MAPINFO:
	{
		int mapType{};
		va_start( ap, argsCount );
		mapType = va_arg( ap, int );
		va_end( ap );

		m_ServerData->mapType = mapType;
		m_ServerData->mapName = AppContext->FindMapName( mapType );

		cout << "현재맵은 " << m_ServerData->mapName << "입니다." << endl;

		break;
	}
	case EVENT_GAMEROOM_CALLBACK_SELECT_CHARACTER:
	{
		int argID;
		std::string argSelectMesh;
		va_start( ap, argsCount );
		argID = va_arg( ap, int );
		argSelectMesh = va_arg( ap, std::string );
		va_end( ap );

		// 다른사람이 선택한 캐릭터 잠구기
		if( argID != m_ServerData->playerID )
		{
			cout << argID << "가 " << argSelectMesh << "를 선택했다." << endl;
			SetOtherUnSelectedPlayerImageView( argSelectMesh, true );
		}

		// meshID로 isVisible true false
		SubBattleClient& sbClient = m_ServerData->subBattleClients[argID];
		sbClient.playerMeshName = argSelectMesh;
		AppContext->DisplayCharacter( MAP_STR_GAMEROOM, argSelectMesh, m_ServerData->subBattleClients[argID].spawnLocation, true );

		break;
	}
	case EVENT_GAMEROOM_CALLBACK_SELECT_CANCEL_CHARACTER:
	{
		int argID;
		std::string argSelectMesh;
		va_start( ap, argsCount );
		argID = va_arg( ap, int );
		argSelectMesh = va_arg( ap, std::string );
		va_end( ap );

		// 다른사람이 선택한 캐릭터 잠금해제
		if( argID != m_ServerData->playerID )
		{
			cout << argID << "가 " << argSelectMesh << "를 선택취소했다." << endl;
			SetOtherUnSelectedPlayerImageView( argSelectMesh, false );
		}

		SubBattleClient sbClient = m_ServerData->subBattleClients[argID];
		sbClient.playerMeshName = sbClient.playerMeshName;
		AppContext->HiddenCharacter( argSelectMesh );
		break;
	}

	case EVENT_GAMEROOM_GAMESTART_AVAILABLE:
	{
		// 여기서 스타트 버튼 활성화 되도록 작업하면 됩니다.
		bool available{};
		va_list ap;
		va_start( ap, argsCount );
		available = va_arg( ap, bool );
		va_end( ap );

		if( available && m_ServerData->subBattleClients[m_ServerData->playerID].host )
			AppContext->FindObject<Button>( MESH_GEOID_RECT, UI_BUTTON_GAMESTART )->OnActive( true );
		else
			AppContext->FindObject<Button>( MESH_GEOID_RECT, UI_BUTTON_GAMESTART )->OnActive( false );
		break;
	}

	case EVENT_LOADING_GAMEPLAY_INFO:
	{
		/* 로딩화면 */
		// 게임플레이 정보 세팅하기
		// 맵 / 플레이어 / 오브젝트
		//AppContext->FindObject<UserInterface>(MESH_GEOID_RECT, UI_BUTTON_GAMESTART)->Overlap(false);
		SceneManager::GetApp()->ChangeScene();
		break;
	}
	case EVENT_GAMEROOM_RECV_CHAT:
	{
		string chatBuf;
		int id;
		va_list ap;
		va_start( ap, argsCount );
		chatBuf = va_arg( ap, string );
		id = va_arg( ap, int );
		va_end( ap );

		cout << (id) << "에게 받은 채팅 - " << chatBuf << endl;

		WCHAR wc[256];
		int nLen = MultiByteToWideChar( CP_ACP, 0, chatBuf.c_str(), chatBuf.size(), NULL, NULL );
		MultiByteToWideChar( CP_ACP, 0, chatBuf.c_str(), chatBuf.size(), wc, nLen );
		wc[nLen] = 0;


		WCHAR wName[256];
		nLen = MultiByteToWideChar( CP_ACP, 0, Service::GetApp()->GetBattleClient( id )->name, sizeof( char ) * 10, NULL, NULL );
		MultiByteToWideChar( CP_ACP, 0, Service::GetApp()->GetBattleClient( id )->name, sizeof( char ) * 10, wName, nLen );
		wName[nLen] = 0;

		ChatData serverChat{ wName, wc };
		m_ChatData.push_back( serverChat );
		if( m_ChatData.size() > GAMEPLAY_MAX_CHAT_COUNT )
			m_ChatData.pop_front();
		break;
	}
	}
}

void GameroomScene::UIEvent( int sEvent, int argsCount, ... )
{
	// 자신의 버튼 이벤트는 Selected로 처리한다.

	/*공통*/
	// 캐릭터-이미지뷰 모두 활성화
	// 내가 캐릭터 선택시 선택한 캐릭터-이미지뷰 Selected(true)
	// 서버에서 전달된 선택된 캐릭터-이미지뷰 또한 비활성화(다른 클라 메쉬 지정)
	// 캐릭터 선택시 -> 레디버튼 활성화(내 클라 메쉬 지정)

	// 선택한 캐릭터-이미지뷰 선택시 Selected(true)로 변경(내 클라 메쉬 초기화)
	// 현재 선택한 캐릭터-이미지뷰가 존재하는데(Selected(true)), Selected(false)된 다른 캐릭터-이미지뷰를 선택시 
	// 현재 선택한 캐릭터-이미지뷰 Selected(false), 다시 선택한 캐릭터 이미지뷰 Selected(true)(메쉬변경)

	/*방장*/
	// 모든 클라이언트가 레디를 했을때 게임스타트버튼 활성화
	// 현재 방장도 레디버튼이 있어 레디를 안하고 게임스타트 버튼으로만 바로시작하고싶다면
	// 게임스타트 버튼이 활성화되었을때 레디패킷과 스타트패킷 둘다보내면 해결됨

	/*참가자*/
	// 게임스타트 버튼 그리지 않음
	va_list ap;
	switch( sEvent )
	{
	case CLIENT_EVENT_GAMEROOM_UI_PRESSED:
	{
		std::string uiName;
		bool onoff;
		va_start( ap, argsCount );
		uiName = va_arg( ap, std::string );
		onoff = va_arg( ap, bool );
		va_end( ap );

		AppContext->FindObject<UserInterface>( MESH_GEOID_RECT, uiName )->Overlap( onoff );

		break;
	}

	case EVENT_GAMEROOM_INPUT_SELECT_CHARACTER:
	{
		std::string chrMeshName;
		va_start( ap, argsCount );
		chrMeshName = va_arg( ap, std::string );
		va_end( ap );

		// 다른사람이 선택한 결과가 올경우
		if( GetOtherUnSelectedPlayerImageView( chrMeshName ) ) {
			ChatData serverChat{ L"", L"이미 선택된 캐릭터입니다." };
			m_ChatData.push_back( serverChat );
			if( m_ChatData.size() > GAMEPLAY_MAX_CHAT_COUNT )
				m_ChatData.pop_front();
			break;
		}
		else SetOtherUnSelectedPlayerImageView( chrMeshName, false );

		// 이전에 선택한 캐릭터와 현재 선택한 캐릭터가 동일한지 아닌지를 체크해야한다.
		SubBattleClient& sbClient = m_ServerData->subBattleClients[m_ServerData->playerID];
		if( chrMeshName != sbClient.playerMeshName )
		{
			if( sbClient.playerMeshName != "" && sbClient.playerMeshName != CHARACTER_NONE )
			{
				ForcedUnSelectedPlayerImageView( sbClient.playerMeshName );
				Service::GetApp()->Notify( EVENT_GAMEROOM_INPUT_SELECT_CANCEL_CHARACTER, 1, sbClient.playerMeshName );
			}
		}

		// 캐릭터 선택됨
		int isSelect = OnSelectSelectedPlayerImageView( chrMeshName );
		if( isSelect == 1 )
		{
			Service::GetApp()->Notify( EVENT_GAMEROOM_INPUT_SELECT_CHARACTER, 1, chrMeshName );
			AppContext->FindObject<Button>( MESH_GEOID_RECT, UI_BUTTON_READY )->OnActive( true );
		}
		// 캐릭터 선택취소됨
		else if( isSelect == 0 )
		{
			Service::GetApp()->Notify( EVENT_GAMEROOM_INPUT_SELECT_CANCEL_CHARACTER, 1, chrMeshName );

			// if ready가 on일때
			if( m_ServerData->subBattleClients[m_ServerData->playerID].isReady )
				Service::GetApp()->Notify( EVENT_GAMEROOM_PLAYER_INPUT_READY );
			AppContext->FindObject<Button>( MESH_GEOID_RECT, UI_BUTTON_READY )->OnActive( false );
		}
		else if( isSelect == -1 )
			cout << "다른 클라이언트가 이미 선택한 캐릭터입니다." << endl;

		break;
	}
	case EVENT_GAMEROOM_INPUT_MAPINFO:
	{
		int mapEvent;
		va_start( ap, argsCount );
		mapEvent = va_arg( ap, int );
		va_end( ap );

		int mapType = 0;
		if( mapEvent == EVENT_GAMEROOM_PREV_MAP )
		{
			mapType = m_ServerData->mapType - 1;
		}
		else if( mapEvent == EVENT_GAMEROOM_NEXT_MAP )
		{
			mapType = m_ServerData->mapType + 1;
		}

		if( mapType == MAP_TOWN )
		{
			AppContext->FindObject<Button>( MESH_GEOID_RECT, UI_BUTTON_PREVMAP )->OnActive( false );
			AppContext->FindObject<Button>( MESH_GEOID_RECT, UI_BUTTON_NEXTMAP )->OnActive( true );
		}
		else if( mapType == TEMP_SECOND_MAP )
		{
			AppContext->FindObject<Button>( MESH_GEOID_RECT, UI_BUTTON_PREVMAP )->OnActive( true );
			AppContext->FindObject<Button>( MESH_GEOID_RECT, UI_BUTTON_NEXTMAP )->OnActive( false );
		}

		Service::GetApp()->Notify( EVENT_GAMEROOM_INPUT_MAPINFO, 1, mapType );
		break;
	}
	}
}

void GameroomScene::Initialize()
{
	m_SceneController = new GameroomController( this );
	m_SceneController->SetMapName( MAP_STR_GAMEROOM );

	m_ServerData = std::make_unique< ServerData>();

	/*SkyCube*/
	AppContext->CreateSkycube( "gameroomSky", "gameroomSky", "grasscube1024" );

	/*Magic Circle*/
	AppContext->CreateMagicCircle( OBJECT_NAME_MAGIC_CIRCLE_P1 );
	AppContext->CreateMagicCircle( OBJECT_NAME_MAGIC_CIRCLE_P2 );
	AppContext->CreateMagicCircle( OBJECT_NAME_MAGIC_CIRCLE_P3 );
	AppContext->CreateMagicCircle( OBJECT_NAME_MAGIC_CIRCLE_P4 );

	/*Props*/
	// 다른맵의 메쉬이름과 고유아이디가 같을경우 Duplicatae라는 경고 문자가 나올수 있다.
	// 하지만 실제적으로 문제가 되진 않을거같다.
	AppContext->CreateProps( MAP_STR_GAMEROOM );
	AppContext->CreateUI3D( MAP_STR_GAMEROOM );

	// SetUITexture(텍스쳐순서: 키가 안눌려있는 상태(Released) / 키가 눌렸을때 (Pressed) / 키가 비활성화 상태일때(NoneActivation))
	// UI를 선택했을때 텍스쳐 변경이 있을경우(Pressed / Disable)
	AppContext->SetUITexture( UI_TYPE_BUTTON, MESH_GEOID_RECT, UI_BUTTON_GAMESTART, TEXTURE_STR_UI_BUTTON_START_RELEASED, TEXTURE_STR_UI_BUTTON_START_PRESSED, TEXTURE_STR_UI_BUTTON_START_ACTIVATION );
	AppContext->SetUITexture( UI_TYPE_BUTTON, MESH_GEOID_RECT, UI_BUTTON_READY, TEXTURE_STR_UI_BUTTON_READY_RELEASED, TEXTURE_STR_UI_BUTTON_READY_PRESSED, TEXTURE_STR_UI_BUTTON_READY_ACTIVATION );
	AppContext->SetUITexture( UI_TYPE_BUTTON, MESH_GEOID_RECT, UI_BUTTON_PREVMAP, TEXTURE_STR_UI_BUTTON_MAP_PREV_RELEASED, TEXTURE_STR_UI_BUTTON_MAP_PREV_PRESSED, TEXTURE_STR_UI_BUTTON_MAP_PREV_ACTIVATION );
	AppContext->SetUITexture( UI_TYPE_BUTTON, MESH_GEOID_RECT, UI_BUTTON_NEXTMAP, TEXTURE_STR_UI_BUTTON_MAP_NEXT_RELEASED, TEXTURE_STR_UI_BUTTON_MAP_NEXT_PRESSED, TEXTURE_STR_UI_BUTTON_MAP_NEXT_ACTIVATION );
	AppContext->SetUITexture( UI_TYPE_BUTTON, MESH_GEOID_RECT, UI_BUTTON_EXIT, TEXTURE_STR_UI_BUTTON_EXIT_PRESSED );

	AppContext->SetUITexture( UI_TYPE_IMAGEVIEW, MESH_GEOID_RECT, UI_IMAGEVIEW_MAP, TEXTURE_STR_UI_IMAGEVIEW_MAP );
	AppContext->SetUITexture( UI_TYPE_IMAGEVIEW, MESH_GEOID_RECT, UI_IMAGEVIEW_SELECT_P1, TEXTURE_STR_UI_IMAGEIVEW_DU, "", "", TEXTURE_STR_DU_ORIGIN );
	AppContext->SetUITexture( UI_TYPE_IMAGEVIEW, MESH_GEOID_RECT, UI_IMAGEVIEW_SELECT_P2, TEXTURE_STR_UI_IMAGEIVEW_BA, "", "", TEXTURE_STR_BA_ORIGIN );
	AppContext->SetUITexture( UI_TYPE_IMAGEVIEW, MESH_GEOID_RECT, UI_IMAGEVIEW_SELECT_P3, TEXTURE_STR_UI_IMAGEIVEW_FP, "", "", TEXTURE_STR_FE_ORIGIN );
	AppContext->SetUITexture( UI_TYPE_IMAGEVIEW, MESH_GEOID_RECT, UI_IMAGEVIEW_SELECT_P4, TEXTURE_STR_UI_IMAGEIVEW_MP, "", "", TEXTURE_STR_MA_ORIGIN );
	AppContext->SetUITexture( UI_TYPE_IMAGEVIEW, MESH_GEOID_RECT, UI_IMAGEVIEW_SELECT_P5, TEXTURE_STR_UI_IMAGEIVEW_SO, "", "", TEXTURE_STR_SO_ORIGIN );

	AppContext->SetUITexture( UI_TYPE_IMAGEVIEW, MESH_GEOID_RECT, UI_IMAGEVIEW_CHARACTER_READY_1, TEXTURE_STR_UI_CHARACTER_READY );
	AppContext->SetUITexture( UI_TYPE_IMAGEVIEW, MESH_GEOID_RECT, UI_IMAGEVIEW_CHARACTER_READY_2, TEXTURE_STR_UI_CHARACTER_READY );
	AppContext->SetUITexture( UI_TYPE_IMAGEVIEW, MESH_GEOID_RECT, UI_IMAGEVIEW_CHARACTER_READY_3, TEXTURE_STR_UI_CHARACTER_READY );
	AppContext->SetUITexture( UI_TYPE_IMAGEVIEW, MESH_GEOID_RECT, UI_IMAGEVIEW_CHARACTER_READY_4, TEXTURE_STR_UI_CHARACTER_READY );


	AppContext->SetUITexture( UI_TYPE_IMAGEVIEW, MESH_GEOID_RECT, UI_IMAGEVIEW_CROWN_1, TEXTURE_STR_UI_CROWN );
	AppContext->SetUITexture( UI_TYPE_IMAGEVIEW, MESH_GEOID_RECT, UI_IMAGEVIEW_CROWN_2, TEXTURE_STR_UI_CROWN );
	AppContext->SetUITexture( UI_TYPE_IMAGEVIEW, MESH_GEOID_RECT, UI_IMAGEVIEW_CROWN_3, TEXTURE_STR_UI_CROWN );
	AppContext->SetUITexture( UI_TYPE_IMAGEVIEW, MESH_GEOID_RECT, UI_IMAGEVIEW_CROWN_4, TEXTURE_STR_UI_CROWN );

	// IMAGEVIEW READY / HOST / ENTER PLAYER

}

void GameroomScene::OnResize()
{
}

bool GameroomScene::Enter()
{
	cout << "============= Gameroom Scene ==============" << endl;

	// 채팅을 할 수 있음
	Core::g_InputSwitch = true;
	Core::g_Chating = 1;

	/* Create SceneBounds for Shadow */
	m_SceneBounds.Center = XMFLOAT3( 2532.11, 0, 450 );
	m_SceneBounds.Radius = sqrtf( 2000.f * 2000.f + 2000.f * 2000.f );

	/* Light Setting */
	CACHE_CACHE::GetApp()->m_Lights[LIGHT_NAME_DIRECTIONAL]->Direction = { 0.59735, -1.43735, 1.24265 };
	CACHE_CACHE::GetApp()->m_Lights[LIGHT_NAME_DIRECTIONAL]->Strength = { 0.9f, 0.8f, 0.7f };

	// 첫번째 맵 세팅
	m_ServerData->mapType = MAP_TOWN;
	m_ServerData->mapName = AppContext->FindMapName( MAP_TOWN );

	AppContext->DisplayProps( MAP_STR_GAMEROOM );
	AppContext->DisplayUI( MAP_STR_GAMEROOM );

	// 자기 자신 서브배틀클라이언트 생성
	m_ServerData->playerID = Service::GetApp()->GetMyBattleID();
	BuildSubBattleClient( m_ServerData->playerID, true );

	// 자기 자신 마법진 보여주기
	SubBattleClient sbClient = m_ServerData->subBattleClients[m_ServerData->playerID];
	if( sbClient.spawnLocation == 0 )
		AppContext->DisplayMagicCircle( OBJECT_NAME_MAGIC_CIRCLE_P1, MAP_STR_GAMEROOM, sbClient.spawnLocation, true );
	else if( sbClient.spawnLocation == 1 )
		AppContext->DisplayMagicCircle( OBJECT_NAME_MAGIC_CIRCLE_P2, MAP_STR_GAMEROOM, sbClient.spawnLocation, true );
	else if( sbClient.spawnLocation == 2 )
		AppContext->DisplayMagicCircle( OBJECT_NAME_MAGIC_CIRCLE_P3, MAP_STR_GAMEROOM, sbClient.spawnLocation, true );
	else if( sbClient.spawnLocation == 3 )
		AppContext->DisplayMagicCircle( OBJECT_NAME_MAGIC_CIRCLE_P4, MAP_STR_GAMEROOM, sbClient.spawnLocation, true );

	// 자기 자신 파티클 보여주기
	XMFLOAT3 myPos = AppContext->FindSpawnLocation( MAP_STR_GAMEROOM, sbClient.spawnLocation );
	AppContext->DisplayParticle( PARTICLE_NAME_CHERRY_BLOSSOM_CHARACTER, PARTICLE_NAME_CHERRY_BLOSSOM_CHARACTER, myPos, true );


	// 게임 시작, 준비버튼 비활성화
	AppContext->FindObject<Button>( MESH_GEOID_RECT, UI_BUTTON_GAMESTART )->OnActive( false );
	AppContext->FindObject<Button>( MESH_GEOID_RECT, UI_BUTTON_READY )->OnActive( false );

	// 준비상태 비활성화
	AppContext->FindObject<ImageView>( MESH_GEOID_RECT, UI_IMAGEVIEW_CHARACTER_READY_1 )->m_IsVisible = false;
	AppContext->FindObject<ImageView>( MESH_GEOID_RECT, UI_IMAGEVIEW_CHARACTER_READY_2 )->m_IsVisible = false;
	AppContext->FindObject<ImageView>( MESH_GEOID_RECT, UI_IMAGEVIEW_CHARACTER_READY_3 )->m_IsVisible = false;
	AppContext->FindObject<ImageView>( MESH_GEOID_RECT, UI_IMAGEVIEW_CHARACTER_READY_4 )->m_IsVisible = false;

	// 방장 비활성화
	AppContext->FindObject<ImageView>( MESH_GEOID_RECT, UI_IMAGEVIEW_CROWN_1 )->m_IsVisible = false;
	AppContext->FindObject<ImageView>( MESH_GEOID_RECT, UI_IMAGEVIEW_CROWN_2 )->m_IsVisible = false;
	AppContext->FindObject<ImageView>( MESH_GEOID_RECT, UI_IMAGEVIEW_CROWN_3 )->m_IsVisible = false;
	AppContext->FindObject<ImageView>( MESH_GEOID_RECT, UI_IMAGEVIEW_CROWN_4 )->m_IsVisible = false;

	// 방장일경우
	// Prev 비활성 / Next 활성
	AppContext->FindObject<Button>( MESH_GEOID_RECT, UI_BUTTON_PREVMAP )->OnActive( false );
	if( !m_ServerData->subBattleClients[m_ServerData->playerID].host )
	{
		// 방장이 아닌경우
		AppContext->FindObject<Button>( MESH_GEOID_RECT, UI_BUTTON_NEXTMAP )->OnActive( false );
	}
	else
	{
		// 방장인 경우
		AppContext->FindObject<Button>( MESH_GEOID_RECT, UI_BUTTON_NEXTMAP )->OnActive( true );
	}

	SetHostImageView();
	SetReadyStateImageView();

	// 카메라 뷰행렬 초기화
	CACHE_CACHE::GetApp()->m_Camera->CameraInitialize( SceneType::GameRoom );

	return true;
}

void GameroomScene::Exit()
{
	SoundManager::GetApp()->StopAll();

	// Enter가 아닌 Exit할때 초기화
	AppContext->HiddenProps( MAP_STR_GAMEROOM );
	AppContext->HiddenUI( MAP_STR_GAMEROOM );

	AppContext->HiddenCharacter( CHARACTER_DRUID );
	AppContext->HiddenCharacter( CHARACTER_BAIRD );
	AppContext->HiddenCharacter( CHARACTER_FEMALE_PEASANT );
	AppContext->HiddenCharacter( CHARACTER_MALE_PEASANT );
	AppContext->HiddenCharacter( CHARACTER_SORCERER );

	// 자기 자신 파티클 감추기
	AppContext->HiddenParticle( PARTICLE_NAME_CHERRY_BLOSSOM_CHARACTER, PARTICLE_NAME_CHERRY_BLOSSOM_CHARACTER );

	/*UI 초기화*/
	// 선택한 캐릭터 이미지뷰 세팅 초기화
	AppContext->FindObject<ImageView>( MESH_GEOID_RECT, UI_IMAGEVIEW_SELECT_P1 )->InitializeSyncInfo();
	AppContext->FindObject<ImageView>( MESH_GEOID_RECT, UI_IMAGEVIEW_SELECT_P2 )->InitializeSyncInfo();
	AppContext->FindObject<ImageView>( MESH_GEOID_RECT, UI_IMAGEVIEW_SELECT_P3 )->InitializeSyncInfo();
	AppContext->FindObject<ImageView>( MESH_GEOID_RECT, UI_IMAGEVIEW_SELECT_P4 )->InitializeSyncInfo();
	AppContext->FindObject<ImageView>( MESH_GEOID_RECT, UI_IMAGEVIEW_SELECT_P5 )->InitializeSyncInfo();

	// m_ServerData 초기화
	for( auto& p : m_ServerData->subBattleClients )
	{
		// subBattleClilents가 존재할때
		// 게임레디 풀기
		p.second.playerMeshName = CHARACTER_NONE;
		p.second.isReady = false;
	}

	Core::g_InputSwitch = false;
	m_ChatData.clear();

	cout << "===========================================" << endl << endl;
}

void GameroomScene::Update( const float& fDeltaTime )
{
	m_SceneController->Update( fDeltaTime );

	/* SkyCube */
	GraphicsContext::GetApp()->UpdateInstanceData( AppContext->m_RItemsMap["gameroomSky"], AppContext->m_RItemsVec );

	/* Magic Circle */
	GraphicsContext::GetApp()->UpdateInstanceData( AppContext->m_RItemsMap[OBJECT_TYPE_MAGIC_CIRCLE], AppContext->m_RItemsVec );

	/*Props*/
	for( auto& prop : AppContext->m_Maps[MAP_STR_GAMEROOM]->propTypeVector )
	{
		GraphicsContext::GetApp()->UpdateInstanceData( AppContext->m_RItemsMap[prop], AppContext->m_RItemsVec );
	}

	/*UI*/
	GraphicsContext::GetApp()->UpdateInstanceData( AppContext->m_RItemsMap[MESH_GEOID_RECT], AppContext->m_RItemsVec );

	/*Characters*/
	GraphicsContext::GetApp()->UpdateInstanceData( AppContext->m_RItemsMap[CHARACTER_DRUID], AppContext->m_RItemsVec );
	GraphicsContext::GetApp()->UpdateInstanceData( AppContext->m_RItemsMap[CHARACTER_BAIRD], AppContext->m_RItemsVec );
	GraphicsContext::GetApp()->UpdateInstanceData( AppContext->m_RItemsMap[CHARACTER_FEMALE_PEASANT], AppContext->m_RItemsVec );
	GraphicsContext::GetApp()->UpdateInstanceData( AppContext->m_RItemsMap[CHARACTER_MALE_PEASANT], AppContext->m_RItemsVec );
	GraphicsContext::GetApp()->UpdateInstanceData( AppContext->m_RItemsMap[CHARACTER_SORCERER], AppContext->m_RItemsVec );

	AppContext->FindObject<Character>( CHARACTER_DRUID, CHARACTER_DRUID )->Update( fDeltaTime );
	AppContext->FindObject<Character>( CHARACTER_BAIRD, CHARACTER_BAIRD )->Update( fDeltaTime );
	AppContext->FindObject<Character>( CHARACTER_FEMALE_PEASANT, CHARACTER_FEMALE_PEASANT )->Update( fDeltaTime );
	AppContext->FindObject<Character>( CHARACTER_MALE_PEASANT, CHARACTER_MALE_PEASANT )->Update( fDeltaTime );
	AppContext->FindObject<Character>( CHARACTER_SORCERER, CHARACTER_SORCERER )->Update( fDeltaTime );

	GraphicsContext::GetApp()->UpdateSkinnedCBs( BoneIndex::Druid, AssertsReference::GetApp()->m_SkinnedModelInsts[CHARACTER_DRUID].get() );
	GraphicsContext::GetApp()->UpdateSkinnedCBs( BoneIndex::Baird, AssertsReference::GetApp()->m_SkinnedModelInsts[CHARACTER_BAIRD].get() );
	GraphicsContext::GetApp()->UpdateSkinnedCBs( BoneIndex::Female_Peasant, AssertsReference::GetApp()->m_SkinnedModelInsts[CHARACTER_FEMALE_PEASANT].get() );
	GraphicsContext::GetApp()->UpdateSkinnedCBs( BoneIndex::Male_Peasant, AssertsReference::GetApp()->m_SkinnedModelInsts[CHARACTER_MALE_PEASANT].get() );
	GraphicsContext::GetApp()->UpdateSkinnedCBs( BoneIndex::Sorcerer, AssertsReference::GetApp()->m_SkinnedModelInsts[CHARACTER_SORCERER].get() );

	/*Particle*/
	GraphicsContext::GetApp()->UpdateInstanceData( AppContext->m_RItemsMap[PARTICLE_NAME_CHERRY_BLOSSOM_CHARACTER], AppContext->m_RItemsVec, false, true );

	//*Shadow*/
	GraphicsContext::GetApp()->UpdateShadowTransform( CACHE_CACHE::GetApp()->m_Lights[LIGHT_NAME_DIRECTIONAL].get(), m_SceneBounds );
	GraphicsContext::GetApp()->UpdateShadowPassCB();

	/*Materials*/
	GraphicsContext::GetApp()->UpdateMaterialBuffer( AssertsReference::GetApp()->m_Materials );
}

void GameroomScene::Render()
{
	/*Props*/
	for( auto& prop : AppContext->m_Maps[MAP_STR_GAMEROOM]->propTypeVector )
	{
		GraphicsContext::GetApp()->DrawRenderItem( AppContext->m_RItemsMap[prop], AppContext->m_RItemsVec );
	}
	// 3D UI
	GraphicsContext::GetApp()->DrawRenderItem( AppContext->m_RItemsMap[MESH_GEOID_RECT], AppContext->m_RItemsVec );

	/* Magic Circle */
	GraphicsContext::GetApp()->SetPipelineState( Graphics::g_OpacityPSO.Get() );
	GraphicsContext::GetApp()->DrawRenderItem( AppContext->m_RItemsMap[OBJECT_TYPE_MAGIC_CIRCLE], AppContext->m_RItemsVec );

	/*SkyBox*/
	GraphicsContext::GetApp()->SetPipelineState( Graphics::g_SkyPSO.Get() );
	GraphicsContext::GetApp()->DrawRenderItem( AppContext->m_RItemsMap["gameroomSky"], AppContext->m_RItemsVec );

	/*Particle*/
	GraphicsContext::GetApp()->SetPipelineState( Graphics::g_ParticlePSO.Get() );
	GraphicsContext::GetApp()->DrawRenderItem( AppContext->m_RItemsMap[PARTICLE_NAME_CHERRY_BLOSSOM_CHARACTER], AppContext->m_RItemsVec );

	/*Characters*/
	GraphicsContext::GetApp()->SetPipelineState( Graphics::g_SkinnedPSO.Get() );
	GraphicsContext::GetApp()->DrawRenderItem( AppContext->m_RItemsMap[CHARACTER_DRUID], AppContext->m_RItemsVec );
	GraphicsContext::GetApp()->DrawRenderItem( AppContext->m_RItemsMap[CHARACTER_BAIRD], AppContext->m_RItemsVec );
	GraphicsContext::GetApp()->DrawRenderItem( AppContext->m_RItemsMap[CHARACTER_FEMALE_PEASANT], AppContext->m_RItemsVec );
	GraphicsContext::GetApp()->DrawRenderItem( AppContext->m_RItemsMap[CHARACTER_MALE_PEASANT], AppContext->m_RItemsVec );
	GraphicsContext::GetApp()->DrawRenderItem( AppContext->m_RItemsMap[CHARACTER_SORCERER], AppContext->m_RItemsVec );

	/*Shadow*/
	GraphicsContext::GetApp()->SetResourceShadowPassCB();
	GraphicsContext::GetApp()->SetPipelineState( Graphics::g_ShadowOpaquePSO.Get() );

	/*Shadow Props*/
	for( auto& prop : AppContext->m_Maps[MAP_STR_GAMEROOM]->propTypeVector )
	{
		GraphicsContext::GetApp()->DrawRenderItem( AppContext->m_RItemsMap[prop], AppContext->m_RItemsVec );
	}

	/*Shadow Characters*/
	GraphicsContext::GetApp()->SetPipelineState( Graphics::g_SkinnedShadowOpaquePSO.Get() );
	GraphicsContext::GetApp()->DrawRenderItem( AppContext->m_RItemsMap[CHARACTER_DRUID], AppContext->m_RItemsVec );
	GraphicsContext::GetApp()->DrawRenderItem( AppContext->m_RItemsMap[CHARACTER_BAIRD], AppContext->m_RItemsVec );
	GraphicsContext::GetApp()->DrawRenderItem( AppContext->m_RItemsMap[CHARACTER_FEMALE_PEASANT], AppContext->m_RItemsVec );
	GraphicsContext::GetApp()->DrawRenderItem( AppContext->m_RItemsMap[CHARACTER_MALE_PEASANT], AppContext->m_RItemsVec );
	GraphicsContext::GetApp()->DrawRenderItem( AppContext->m_RItemsMap[CHARACTER_SORCERER], AppContext->m_RItemsVec );

	GraphicsContext::GetApp()->ShadowTransitionResourceBarrier();

}

void GameroomScene::RenderUI()
{
	// 입력하고 있는 채팅 
	WCHAR ws[256] = L"";
	if( Core::g_Chating == 2 )	// 채팅 입력중일 때만 보여주기
	{
		wcscat( ws, Core::g_ChatBuf );
		wcscat( ws, Core::g_TempChatBuf );
		wcscat( ws, L"|" );

		GraphicsContext::GetApp()->SetTextSize( Core::g_DisplayWidth / 55, DWRITE_TEXT_ALIGNMENT_LEADING, D2D1::ColorF::White );
		GraphicsContext::GetApp()->DrawD2DText( ws, Core::g_DisplayWidth / 25, Core::g_DisplayHeight / 1.07, Core::g_DisplayWidth, Core::g_DisplayHeight, true );
	}

	// 서버에게 받은 채팅 보여주기
	wstring chatData;
	int index = 0;
	if( m_ChatData.size() <= 0 )
		return;

	for( auto p = m_ChatData.rbegin(); p != m_ChatData.rend(); ++p )
	{
		if( (*p).name != L"" )
			chatData = L"[" + (*p).name + L"] " + (*p).chat;
		else
			chatData = (*p).chat;

		GraphicsContext::GetApp()->SetTextSize( Core::g_DisplayWidth / 55, DWRITE_TEXT_ALIGNMENT_LEADING, D2D1::ColorF::White );
		GraphicsContext::GetApp()->DrawD2DText( chatData, Core::g_DisplayWidth / 25, Core::g_DisplayHeight / 1.07 - (Core::g_DisplayWidth / 45) - (Core::g_DisplayWidth / 45 * index), Core::g_DisplayWidth, Core::g_DisplayHeight, true );
		index++;
	}
}

void GameroomScene::BuildSubBattleClient( int bcID, bool isMy )
{
	// 이거땜에 오류있음
	if( m_ServerData->subBattleClients.count( bcID ) || bcID == -1 ) {
		cout << "게임룸 서브배틀클라이언트 " << bcID << "가 이미존재함." << endl;
		return;
	}

	BattleClient* bc = Service::GetApp()->GetBattleClient( bcID );
	if( !bc ) return;

	SubBattleClient subBc;
	subBc.battleID = bcID;
	subBc.playerNum = bc->m_PlayerNum;
	subBc.spawnLocation = bc->m_SpawnLocation;
	subBc.playerMeshName = bc->m_PlayerMesh;
	subBc.host = bc->m_Host;

	// 맨처음 들어왔을때는 무조건 isReady가 false여야한다. (ex) 나 자신)
	if( isMy )
		subBc.isReady = false;
	subBc.isReady = bc->m_Ready;
	cout << bcID << "의 레디상태: " << subBc.isReady << endl;

	subBc.playerName = bc->name;
	subBc.mmr = bc->mmrInfo.mmr;
	subBc.catched_student_count = bc->mmrInfo.catched_student_count;
	subBc.winning_rate = bc->mmrInfo.winning_rate;

	m_ServerData->subBattleClients[bcID] = std::move( subBc );
}

void GameroomScene::ResetSubBattleClient()
{
	// 모든 마법진 감추기
	AppContext->HiddenMagicCircle( OBJECT_NAME_MAGIC_CIRCLE_P1 );
	AppContext->HiddenMagicCircle( OBJECT_NAME_MAGIC_CIRCLE_P2 );
	AppContext->HiddenMagicCircle( OBJECT_NAME_MAGIC_CIRCLE_P3 );
	AppContext->HiddenMagicCircle( OBJECT_NAME_MAGIC_CIRCLE_P4 );

	m_ServerData->mapName = "";
	m_ServerData->mapType = -1;
	m_ServerData->playerID = -1;
	m_ServerData->subBattleClients.clear();
	cout << "서브배틀클라이언트 모두 클리어 됨" << endl << endl;
}

ImageView* GameroomScene::FindSelectPlayerImageView( std::string playerMeshName )
{
	if( playerMeshName == "" || playerMeshName == CHARACTER_NONE ) {
		cout << "캐릭터 메쉬이름이 존재하지 않습니다." << endl;
		return nullptr;
	}

	ImageView* iv = nullptr;

	if( playerMeshName == CHARACTER_DRUID )
		iv = AppContext->FindObject<ImageView>( MESH_GEOID_RECT, UI_IMAGEVIEW_SELECT_P1 );
	else if( playerMeshName == CHARACTER_BAIRD )
		iv = AppContext->FindObject<ImageView>( MESH_GEOID_RECT, UI_IMAGEVIEW_SELECT_P2 );
	else if( playerMeshName == CHARACTER_FEMALE_PEASANT )
		iv = AppContext->FindObject<ImageView>( MESH_GEOID_RECT, UI_IMAGEVIEW_SELECT_P3 );
	else if( playerMeshName == CHARACTER_MALE_PEASANT )
		iv = AppContext->FindObject<ImageView>( MESH_GEOID_RECT, UI_IMAGEVIEW_SELECT_P4 );
	else if( playerMeshName == CHARACTER_SORCERER )
		iv = AppContext->FindObject<ImageView>( MESH_GEOID_RECT, UI_IMAGEVIEW_SELECT_P5 );

	if( iv == nullptr )
	{
		cout << "찾은 플레이어 이미지뷰 포인터가 널포인터입니다." << endl;
		return nullptr;
	}

	return iv;
}

ImageView* GameroomScene::FindReadyStateImageView( int playerNum )
{
	if( playerNum == -1 ) {
		return nullptr;
	}

	ImageView* iv = nullptr;

	if( playerNum == 0 )
		iv = AppContext->FindObject<ImageView>( MESH_GEOID_RECT, UI_IMAGEVIEW_CHARACTER_READY_1 );
	else if( playerNum == 1 )
		iv = AppContext->FindObject<ImageView>( MESH_GEOID_RECT, UI_IMAGEVIEW_CHARACTER_READY_2 );
	else if( playerNum == 2 )
		iv = AppContext->FindObject<ImageView>( MESH_GEOID_RECT, UI_IMAGEVIEW_CHARACTER_READY_3 );
	else if( playerNum == 3 )
		iv = AppContext->FindObject<ImageView>( MESH_GEOID_RECT, UI_IMAGEVIEW_CHARACTER_READY_4 );

	if( iv == nullptr )
	{
		cout << "찾은 플레이어 레디 이미지뷰 포인터가 널포인터입니다." << endl;
		return nullptr;
	}

	return iv;
}

ImageView* GameroomScene::FindHostImageView( int playerNum )
{
	if( playerNum == -1 ) {
		return nullptr;
	}

	ImageView* iv = nullptr;

	if( playerNum == 0 )
		iv = AppContext->FindObject<ImageView>( MESH_GEOID_RECT, UI_IMAGEVIEW_CROWN_1 );
	else if( playerNum == 1 )
		iv = AppContext->FindObject<ImageView>( MESH_GEOID_RECT, UI_IMAGEVIEW_CROWN_2 );
	else if( playerNum == 2 )
		iv = AppContext->FindObject<ImageView>( MESH_GEOID_RECT, UI_IMAGEVIEW_CROWN_3 );
	else if( playerNum == 3 )
		iv = AppContext->FindObject<ImageView>( MESH_GEOID_RECT, UI_IMAGEVIEW_CROWN_4 );

	if( iv == nullptr )
	{
		cout << "찾은 왕관 이미지뷰 포인터가 널포인터입니다." << endl;
		return nullptr;
	}

	return iv;
}

void GameroomScene::SetHostImageView()
{
	int hostNum = -1;
	for( auto& p : m_ServerData->subBattleClients )
	{
		if( p.second.host )
		{
			hostNum = p.second.playerNum;
			break;
		}
	}

	for( int i = 0; i < TOTAL_USER_COUNT; ++i )
	{
		if( i == hostNum )
		{
			ImageView* iv = FindHostImageView( i );
			iv->m_IsVisible = true;
		}
		else
		{
			ImageView* iv = FindHostImageView( i );
			iv->m_IsVisible = false;
		}
	}

}

void GameroomScene::SetReadyStateImageView()
{
	for( auto& p : m_ServerData->subBattleClients )
	{
		if( p.second.isReady )
		{
			FindReadyStateImageView( p.second.playerNum )->m_IsVisible = true;
		}
		else
		{
			FindReadyStateImageView( p.second.playerNum )->m_IsVisible = false;
		}
	}
}

void GameroomScene::OnActiveSelectedPlayerImageView( std::string playerMeshName, bool onoff )
{
	ImageView* iv = FindSelectPlayerImageView( playerMeshName );

	if( !iv ) return;
	iv->OnActive( onoff );
}

int GameroomScene::OnSelectSelectedPlayerImageView( std::string playerMeshName )
{
	ImageView* iv = FindSelectPlayerImageView( playerMeshName );
	return iv->OnSelected();
}

void GameroomScene::ForcedUnSelectedPlayerImageView( std::string playerMeshName )
{
	ImageView* iv = FindSelectPlayerImageView( playerMeshName );

	if( !iv ) return;
	iv->ForcedUnSelected();
}

void GameroomScene::SetOtherUnSelectedPlayerImageView( std::string playerMeshName, bool onoff )
{
	ImageView* iv = FindSelectPlayerImageView( playerMeshName );

	if( !iv ) return;
	iv->SetOtherIsSelected( onoff );
}

bool GameroomScene::GetOtherUnSelectedPlayerImageView( std::string playerMeshName )
{
	ImageView* iv = FindSelectPlayerImageView( playerMeshName );

	return iv->GetOtherIsSelected();
}

void GameroomScene::InitSelectPlayerImageView( std::string playerMeshName )
{
	ImageView* iv = FindSelectPlayerImageView( playerMeshName );

	if( !iv ) return;
	return iv->InitializeSyncInfo();
}
