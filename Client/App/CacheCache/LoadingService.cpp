#include "pch.h"
#include "LoadingService.h"
#include "GameCore.h"
#include "GameTimer.h"
#include <wincodec.h>

#define LOADING_DELTA_TIME 0.0166

void LoadingService::RunService()
{
	Initialize();

	while (running)
	{
		GameCore::GetApp()->D3D11DevicePreparePresent();

		// RenderUI
		D2DDrawBitmap("loadingBackground", 0, 0, Core::g_DisplayWidth, Core::g_DisplayHeight);
		D2DDrawBitmap("loading", - Core::g_DisplayWidth/30, 0, Core::g_DisplayWidth/6, Core::g_DisplayHeight/14);
		D2DDrawSpriteBitmap(Core::g_DisplayWidth / 15, 0, Core::g_DisplayWidth / 15, Core::g_DisplayHeight / 12);

		///////////
		GameCore::GetApp()->D3D11DeviceExecuteCommandList();
		GameCore::GetApp()->MoveToNextFrame();
	}
	// MainThread -> Event 전송 시작해도된다.
	// Condition_Variable notifyone
	Termiante();

	std::mutex stop_mtx;
	stop_mtx.lock();
	Core::g_Loading_processed = true;
	stop_mtx.unlock();

	Core::g_Loading_cv.notify_one();
}

void LoadingService::StopService()
{
	running = false;
}

void LoadingService::Initialize()
{
	CoInitialize(NULL);

	D2DLoadFont(L"Verdana",30.f);

	D2DLoadBitmap("loadingBackground", L"./Textures/starbackground.png");
	// Loading Textures
	D2DLoadBitmap("loading", L"./Textures/Title/loading/loading.png");
	D2DLoadBitmap("loading_1", L"./Textures/Title/loading/1.png");
	D2DLoadBitmap("loading_2", L"./Textures/Title/loading/2.png");
	D2DLoadBitmap("loading_3", L"./Textures/Title/loading/3.png");
	D2DLoadBitmap("loading_4", L"./Textures/Title/loading/4.png");
	D2DLoadBitmap("loading_5", L"./Textures/Title/loading/5.png");
	D2DLoadBitmap("loading_6", L"./Textures/Title/loading/6.png");
	D2DLoadBitmap("loading_7", L"./Textures/Title/loading/7.png");
	D2DLoadBitmap("loading_8", L"./Textures/Title/loading/8.png");
	D2DLoadBitmap("loading_9", L"./Textures/Title/loading/9.png");
	D2DLoadBitmap("loading_10", L"./Textures/Title/loading/10.png");
	D2DLoadBitmap("loading_11", L"./Textures/Title/loading/11.png");
	D2DLoadBitmap("loading_12", L"./Textures/Title/loading/12.png");
	D2DLoadBitmap("loading_13", L"./Textures/Title/loading/13.png");
	D2DLoadBitmap("loading_14", L"./Textures/Title/loading/14.png");
	
	// Set Loading Sprite
	for (int i = 1; i <= 14; ++i)
	{
		m_LoadingSprites.emplace_back("loading_" + std::to_string(i));
	}
	
	m_SpriteDurationTime = 1.f;
	m_SpriteAnimationSpeed = 20.f;
}

void LoadingService::Termiante()
{
	m_D2dTextures.clear();
	m_LoadingSprites.clear();
	CoUninitialize();
}

void LoadingService::D2DLoadBitmap(std::string textureName, const wchar_t* path)
{
	if (m_D2dTextures.count(textureName))
	{
		m_D2dTextures[textureName]->Release();
		m_D2dTextures[textureName] = nullptr;
	}

	Microsoft::WRL::ComPtr<ID2D1Bitmap> pD2dBitmap;
	// WIC(Windows Imaging Component)관련 객체를 생성하기 위한 Factory 객체 선언
	IWICImagingFactory* p_wic_factory;
	// WIC 객체를 생성하기 위한 Factory 객체를 생성한다.
	CoCreateInstance(CLSID_WICImagingFactory, NULL, CLSCTX_INPROC_SERVER, IID_PPV_ARGS(&p_wic_factory));

	IWICBitmapDecoder* p_decoder;     // 압축된 이미지를 해제할 객체
	IWICBitmapFrameDecode* p_frame;   // 특정 그림을 선택한 객체
	IWICFormatConverter* p_converter; // 이미지 변환 객체
	int result = 0;  // 그림 파일을 읽은 결과 값 (0이면 그림 읽기 실패, 1이면 그림 읽기 성공)
	// WIC용 Factory 객체를 사용하여 이미지 압축 해제를 위한 객체를 생성
	if (S_OK == p_wic_factory->CreateDecoderFromFilename(path, NULL, GENERIC_READ, WICDecodeMetadataCacheOnDemand, &p_decoder)) {
		// 파일을 구성하는 이미지 중에서 첫번째 이미지를 선택한다.
		if (S_OK == p_decoder->GetFrame(0, &p_frame)) {
			// IWICBitmap형식의 비트맵을 ID2D1Bitmap. 형식으로 변환하기 위한 객체 생성
			if (S_OK == p_wic_factory->CreateFormatConverter(&p_converter)) {
				// 선택된 그림을 어떤 형식의 비트맵으로 변환할 것인지 설정한다.
				if (S_OK == p_converter->Initialize(p_frame, GUID_WICPixelFormat32bppPBGRA, WICBitmapDitherTypeNone, NULL, 0.0f, WICBitmapPaletteTypeCustom)) {
					// IWICBitmap 형식의 비트맵으로 ID2D1Bitmap 객체를 생성한다.
					if (S_OK == Core::g_D2dDeviceContext->CreateBitmapFromWicBitmap(p_converter, NULL, &pD2dBitmap)) result = 1;  // 성공적으로 생성한 경우
				}
				p_converter->Release();  // 이미지 변환 객체 제거
			}
			p_frame->Release();   // 그림파일에 있는 이미지를 선택하기 위해 사용한 객체 제거
		}
		p_decoder->Release();     // 압축을 해제하기 위해 생성한 객체 제거
	}
	p_wic_factory->Release();     // WIC를 사용하기 위해 만들었던 Factory 객체 제거

	m_D2dTextures[textureName] = std::move(pD2dBitmap);
}

void LoadingService::D2DLoadFont(std::wstring fontName, float fontSize)
{
	// Create D2D/DWrite objects for rendering text.
	ThrowIfFailed(Core::g_D2dDeviceContext->CreateSolidColorBrush(D2D1::ColorF(D2D1::ColorF::White), &m_TextBrush));
	ThrowIfFailed(Core::g_DWriteFactory->CreateTextFormat(
		fontName.c_str(),
		NULL,
		DWRITE_FONT_WEIGHT_NORMAL,
		DWRITE_FONT_STYLE_NORMAL,
		DWRITE_FONT_STRETCH_NORMAL,
		fontSize,
		L"en-us",
		&m_TextFormat
		));
	ThrowIfFailed(m_TextFormat->SetTextAlignment(DWRITE_TEXT_ALIGNMENT_CENTER));
	ThrowIfFailed(m_TextFormat->SetParagraphAlignment(DWRITE_PARAGRAPH_ALIGNMENT_CENTER));
}

void LoadingService::D2DDrawText(const wstring wtext, float posX, float posY, float rectSize)
{
	D2D1_RECT_F textRect;
	textRect.left = posX - rectSize/2 + Core::g_DisplayWidth / 2;
	textRect.top = posY - rectSize/2 + Core::g_DisplayHeight / 2;
	textRect.right = posX + rectSize/2 + Core::g_DisplayWidth / 2;
	textRect.bottom = posY + rectSize/2 + Core::g_DisplayHeight / 2;


	Core::g_D2dDeviceContext->DrawText(
		wtext.c_str(),
		wtext.size(),
		m_TextFormat.Get(),
		&textRect,
		m_TextBrush.Get()
		);
}

void LoadingService::D2DDrawBitmap(std::string textureName, float posX, float posY, float rectWidth, float rectHeight)
{
	D2D1_RECT_F rect;

	rect.left = posX	- rectWidth/2 +		Core::g_DisplayWidth / 2;
	rect.top = posY		- rectHeight/2 +	Core::g_DisplayHeight / 2;
	rect.right = posX	+ rectWidth/2 +		Core::g_DisplayWidth / 2;
	rect.bottom = posY	+ rectHeight/2 +	Core::g_DisplayHeight / 2;

	Core::g_D2dDeviceContext->DrawBitmap(m_D2dTextures[textureName].Get(),rect);
}

void LoadingService::D2DDrawSpriteBitmap(float posX, float posY, float rectWidth, float rectHeight)
{
	m_SpriteDeltaTime += LOADING_DELTA_TIME * m_SpriteAnimationSpeed;
	m_FollowPointer = std::floor(m_SpriteDeltaTime);

	if (m_FollowPointer >= m_LoadingSprites.size()) {
		m_SpriteDeltaTime = 0;
		m_FollowPointer = 0;
	}

	D2DDrawBitmap(m_LoadingSprites[m_FollowPointer] ,posX, posY, rectWidth, rectHeight);
}
