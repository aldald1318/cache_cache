#include "pch.h"
#include "GameresultScene.h"
#include "Service.h"
#include "GameTimer.h"
#include "InputHandler.h"
#include "CommandContext.h"
#include "ApplicationContext.h"
#include "AssertsReference.h"
#include "UserInterface.h"
#include "CACHE_CACHE.h"
#include "Map.h"
#include "Character.h"

void GameresultScene::ProcessEvent(int sEvent, int argsCount, ...)
{
	switch (sEvent)
	{
	case EVENT_LOADING_GAMERESULT_INFO:
	{
		string chrName;

		va_list ap;
		va_start(ap, argsCount);
		chrName = va_arg(ap, std::string);
		va_end(ap);

		m_PlayerNames.emplace_back(chrName);
		break;
	}
	}
}

void GameresultScene::Initialize()
{
	m_SceneController = new GameresultController(this);
	// ���п� ���� ���� �����.
	m_SceneController->SetMapName(MAP_STR_RESULT_MASTER);

	/*Props*/
	AppContext->CreateProps(MAP_STR_RESULT_MASTER);

	/* UI2D */
	AppContext->CreateUI2D(OBJECT_TYPE_UI2D + m_SceneName, OBJECT_NAME_WINNER, TEXTURE_INDEX_WINNER_MASTER, TEXTURE_INDEX_WINNER_STUDENT);
}

void GameresultScene::OnResize()
{
}

bool GameresultScene::Enter()
{
	cout << "============= GameResult Scene ==============" << endl;
	InputHandler::g_CursorSwitch = true;

	/* Create SceneBounds for Shadow */
	m_SceneBounds.Center = XMFLOAT3(1106.77, 0, 497.02);
	m_SceneBounds.Radius = sqrtf(2000.f * 2000.f + 2000.f * 2000.f);

	/* Light Setting */
	CACHE_CACHE::GetApp()->m_Lights[LIGHT_NAME_DIRECTIONAL]->Direction = { -0.40265, -1.43735, 0.04265 };
	CACHE_CACHE::GetApp()->m_Lights[LIGHT_NAME_DIRECTIONAL]->Strength = { 0.32,0.32,0.32 };

	// �¸��� ����
	m_WinTeam = Service::GetApp()->GetWinTeam();

	// Props & Player -> ���п� ���� �����
	if (m_WinTeam == ROLE_MASTER) m_MapName = MAP_STR_RESULT_MASTER;
	else if (m_WinTeam == ROLE_STUDENT)m_MapName = MAP_STR_RESULT_STUDENT;

	// Display Props
	AppContext->DisplayProps(m_MapName, true);

	// Display Characters
	AppContext->DisplayCharacter(m_MapName, CHARACTER_WIZARD, 0, true);
	Character* wizard = AppContext->FindObject<Character>(CHARACTER_WIZARD, CHARACTER_WIZARD);
	if(m_WinTeam == ROLE_MASTER)
		wizard->SetAnimationPlayerState(AnimationController::PlayerState::STATE_WIN);
	else
		wizard->SetAnimationPlayerState(AnimationController::PlayerState::STATE_LOSE);

	for (int i=0; i<m_PlayerNames.size(); ++i)
	{
		AppContext->DisplayCharacter(m_MapName, m_PlayerNames[i], i+1, true);
		Character* chr = AppContext->FindObject<Character>(m_PlayerNames[i], m_PlayerNames[i]);
		if (m_WinTeam == ROLE_MASTER)
			chr->SetAnimationPlayerState(AnimationController::PlayerState::STATE_LOSE);
		else
			chr->SetAnimationPlayerState(AnimationController::PlayerState::STATE_WIN);
	}

#ifdef DEBUG_CLIENT
	// CLIENT TEST
	AppContext->DisplayCharacter(m_MapName, CHARACTER_BAIRD,1, true);
	AppContext->DisplayCharacter(m_MapName, CHARACTER_DRUID, 2, true);
	AppContext->DisplayCharacter(m_MapName, CHARACTER_FEMALE_PEASANT, 3, true);
#endif

	// Display UI
	AppContext->DisplayUI2D(OBJECT_TYPE_UI2D + m_SceneName, OBJECT_NAME_WINNER, XMFLOAT2(0.f, -300.f), XMFLOAT2(8920.f, 3500.f), true, AnchorType::Center);
	if(m_WinTeam == ROLE_MASTER)
		AppContext->FindObject<UserInterface>(OBJECT_TYPE_UI2D + m_SceneName, OBJECT_NAME_WINNER)->Overlap(false);
	else if(m_WinTeam == ROLE_STUDENT)
		AppContext->FindObject<UserInterface>(OBJECT_TYPE_UI2D + m_SceneName, OBJECT_NAME_WINNER)->Overlap(true);

	// ī�޶� ����� �ʱ�ȭ 
	CACHE_CACHE::GetApp()->m_Camera->CameraInitialize(SceneType::GameResult);

	// ���� ����
	SoundManager::GetApp()->PlaySoundOnce(L"BGM_Result.wav", SoundManager::CHANNEL_ID::BGM);

	return false;
}

void GameresultScene::Exit()
{
	//  �� ��� ���� ����
	SoundManager::GetApp()->StopAll();

	// �κ� & ���ӷ� ���� ����
	SoundManager::GetApp()->PlayBGM(L"BGM_Lobby(Low).wav");

	// Hidden Props
	AppContext->HiddenProps(m_MapName);

	// Hidden Character
	AppContext->HiddenCharacter(CHARACTER_WIZARD);
	for (auto& p : m_PlayerNames)
	{
		AppContext->HiddenCharacter(p);
	}
	m_PlayerNames.clear();

	// �¸��� ����
	// �÷��̾� ���� ����
	// �÷��̾� ���߱�

	cout << "===========================================" << endl << endl;
}

void GameresultScene::Update(const float& fDeltaTime)
{
	m_SceneController->Update(fDeltaTime);

	/*Props*/
	for (auto& prop : AppContext->m_Maps[m_MapName]->propTypeVector)
	{
		GraphicsContext::GetApp()->UpdateInstanceData(AppContext->m_RItemsMap[prop], AppContext->m_RItemsVec);
	}

	/*Characters*/
	GraphicsContext::GetApp()->UpdateInstanceData(AppContext->m_RItemsMap[CHARACTER_WIZARD], AppContext->m_RItemsVec);
	GraphicsContext::GetApp()->UpdateInstanceData(AppContext->m_RItemsMap[CHARACTER_DRUID], AppContext->m_RItemsVec);
	GraphicsContext::GetApp()->UpdateInstanceData(AppContext->m_RItemsMap[CHARACTER_BAIRD], AppContext->m_RItemsVec);
	GraphicsContext::GetApp()->UpdateInstanceData(AppContext->m_RItemsMap[CHARACTER_FEMALE_PEASANT], AppContext->m_RItemsVec);
	GraphicsContext::GetApp()->UpdateInstanceData(AppContext->m_RItemsMap[CHARACTER_MALE_PEASANT], AppContext->m_RItemsVec);
	GraphicsContext::GetApp()->UpdateInstanceData(AppContext->m_RItemsMap[CHARACTER_SORCERER], AppContext->m_RItemsVec);

	AppContext->FindObject<Character>(CHARACTER_WIZARD, CHARACTER_WIZARD)->Update(fDeltaTime);
	AppContext->FindObject<Character>(CHARACTER_DRUID, CHARACTER_DRUID)->Update(fDeltaTime);
	AppContext->FindObject<Character>(CHARACTER_BAIRD, CHARACTER_BAIRD)->Update(fDeltaTime);
	AppContext->FindObject<Character>(CHARACTER_FEMALE_PEASANT, CHARACTER_FEMALE_PEASANT)->Update(fDeltaTime);
	AppContext->FindObject<Character>(CHARACTER_MALE_PEASANT, CHARACTER_MALE_PEASANT)->Update(fDeltaTime);
	AppContext->FindObject<Character>(CHARACTER_SORCERER, CHARACTER_SORCERER)->Update(fDeltaTime);

	GraphicsContext::GetApp()->UpdateSkinnedCBs(BoneIndex::Wizard, AssertsReference::GetApp()->m_SkinnedModelInsts[CHARACTER_WIZARD].get());
	GraphicsContext::GetApp()->UpdateSkinnedCBs(BoneIndex::Druid, AssertsReference::GetApp()->m_SkinnedModelInsts[CHARACTER_DRUID].get());
	GraphicsContext::GetApp()->UpdateSkinnedCBs(BoneIndex::Baird, AssertsReference::GetApp()->m_SkinnedModelInsts[CHARACTER_BAIRD].get());
	GraphicsContext::GetApp()->UpdateSkinnedCBs(BoneIndex::Female_Peasant, AssertsReference::GetApp()->m_SkinnedModelInsts[CHARACTER_FEMALE_PEASANT].get());
	GraphicsContext::GetApp()->UpdateSkinnedCBs(BoneIndex::Male_Peasant, AssertsReference::GetApp()->m_SkinnedModelInsts[CHARACTER_MALE_PEASANT].get());
	GraphicsContext::GetApp()->UpdateSkinnedCBs(BoneIndex::Sorcerer, AssertsReference::GetApp()->m_SkinnedModelInsts[CHARACTER_SORCERER].get());

	/*UI2D*/
	GraphicsContext::GetApp()->Update2DPosition(AppContext->m_RItemsMap[OBJECT_TYPE_UI2D + m_SceneName], AppContext->m_RItemsVec);
	GraphicsContext::GetApp()->UpdateInstanceData(AppContext->m_RItemsMap[OBJECT_TYPE_UI2D + m_SceneName], AppContext->m_RItemsVec);


	/*Shadow*/
	GraphicsContext::GetApp()->UpdateShadowTransform(CACHE_CACHE::GetApp()->m_Lights[LIGHT_NAME_DIRECTIONAL].get(), m_SceneBounds);
	GraphicsContext::GetApp()->UpdateShadowPassCB();

	/*Materials*/
	GraphicsContext::GetApp()->UpdateMaterialBuffer(AssertsReference::GetApp()->m_Materials);
}

void GameresultScene::Render()
{
	/*Props*/
	for (auto& prop : AppContext->m_Maps[m_MapName]->propTypeVector)
	{
		GraphicsContext::GetApp()->DrawRenderItem(AppContext->m_RItemsMap[prop], AppContext->m_RItemsVec);
	}

	/*Characters*/
	GraphicsContext::GetApp()->SetPipelineState(Graphics::g_SkinnedPSO.Get());
	GraphicsContext::GetApp()->DrawRenderItem(AppContext->m_RItemsMap[CHARACTER_WIZARD], AppContext->m_RItemsVec);
	GraphicsContext::GetApp()->DrawRenderItem(AppContext->m_RItemsMap[CHARACTER_DRUID], AppContext->m_RItemsVec);
	GraphicsContext::GetApp()->DrawRenderItem(AppContext->m_RItemsMap[CHARACTER_BAIRD], AppContext->m_RItemsVec);
	GraphicsContext::GetApp()->DrawRenderItem(AppContext->m_RItemsMap[CHARACTER_FEMALE_PEASANT], AppContext->m_RItemsVec);
	GraphicsContext::GetApp()->DrawRenderItem(AppContext->m_RItemsMap[CHARACTER_MALE_PEASANT], AppContext->m_RItemsVec);
	GraphicsContext::GetApp()->DrawRenderItem(AppContext->m_RItemsMap[CHARACTER_SORCERER], AppContext->m_RItemsVec);

	/*UI2D*/
	GraphicsContext::GetApp()->SetPipelineState(Graphics::g_UIPSO.Get());
	GraphicsContext::GetApp()->DrawRenderItem(AppContext->m_RItemsMap[OBJECT_TYPE_UI2D + m_SceneName], AppContext->m_RItemsVec);

	/*Shadow*/
	GraphicsContext::GetApp()->SetResourceShadowPassCB();
	GraphicsContext::GetApp()->SetPipelineState(Graphics::g_ShadowOpaquePSO.Get());

	/*Shadow Props*/
	for (auto& prop : AppContext->m_Maps[m_MapName]->propTypeVector)
	{
		GraphicsContext::GetApp()->DrawRenderItem(AppContext->m_RItemsMap[prop], AppContext->m_RItemsVec);
	}

	/*Shadow Characters*/
	GraphicsContext::GetApp()->SetPipelineState(Graphics::g_SkinnedShadowOpaquePSO.Get());
	GraphicsContext::GetApp()->DrawRenderItem(AppContext->m_RItemsMap[CHARACTER_WIZARD], AppContext->m_RItemsVec);
	GraphicsContext::GetApp()->DrawRenderItem(AppContext->m_RItemsMap[CHARACTER_DRUID], AppContext->m_RItemsVec);
	GraphicsContext::GetApp()->DrawRenderItem(AppContext->m_RItemsMap[CHARACTER_BAIRD], AppContext->m_RItemsVec);
	GraphicsContext::GetApp()->DrawRenderItem(AppContext->m_RItemsMap[CHARACTER_FEMALE_PEASANT], AppContext->m_RItemsVec);
	GraphicsContext::GetApp()->DrawRenderItem(AppContext->m_RItemsMap[CHARACTER_MALE_PEASANT], AppContext->m_RItemsVec);
	GraphicsContext::GetApp()->DrawRenderItem(AppContext->m_RItemsMap[CHARACTER_SORCERER], AppContext->m_RItemsVec);

	GraphicsContext::GetApp()->ShadowTransitionResourceBarrier();

	ThreadEventHandler();
}

void GameresultScene::RenderUI()
{
	// ����ȯ Ÿ�̸�
	GraphicsContext::GetApp()->SetTextSize(Core::g_DisplayWidth / 35);
	GraphicsContext::GetApp()->DrawD2DText(std::to_wstring( static_cast<int>(m_SceneDurationTime - std::floor(m_SceneDeltaTime)) ) + L" Sec...", Core::g_DisplayWidth/2 - Core::g_DisplayWidth / 15, Core::g_DisplayHeight/2 - Core::g_DisplayHeight / 18, Core::g_DisplayWidth/10);
}

void GameresultScene::ThreadEventHandler()
{
	m_SceneDeltaTime += GameTimer::GetApp()->DeltaTime();

	if (m_SceneDurationTime < m_SceneDeltaTime)
	{
		m_SceneDeltaTime = 0.f;
		SceneManager::GetApp()->ChangeScene(SceneType::GameRoom);
	}
}
