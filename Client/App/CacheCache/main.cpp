﻿// CacheCache.cpp : 애플리케이션에 대한 진입점을 정의합니다.
//

#include "pch.h"
#include "main.h"
#include "GameCore.h"
#include "CACHE_CACHE.h"

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
	_In_opt_ HINSTANCE hPrevInstance,
	_In_ LPWSTR    lpCmdLine,
	_In_ int       nCmdShow)
{
	// Enable run-time memory check for debug builds.
#if defined(DEBUG) | defined(_DEBUG)
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
#endif

	try
	{
		//_CrtSetBreakAlloc(1537535);
		CREATE_APPLICATION(CACHE_CACHE)
	}
	catch (DxException& e)
	{
		MessageBox(nullptr, e.ToString().c_str(), L"HR Failed", MB_OK);
		return 0;
	}
}