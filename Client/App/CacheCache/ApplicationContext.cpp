#include "pch.h"
#include "ApplicationContext.h"
#include "AssertsReference.h"
#include "CommandContext.h"
#include "Map.h"
#include "Character.h"
#include "CharacterParts.h"
#include "UserInterface.h"
#include "Button.h"
#include "ImageView.h"
#include "Particle.h"

#define WIDTH_NORMALIZE_LT(x) (x + (1980.f / 2.f))
#define HEIGHT_NORMALIZE_LT(y) (-y + (1080.f / 2.f))


std::string ApplicationContext::FindMapName(int mapCode) const
{
	std::string mapName;

	switch (mapCode)
	{
	case MAP_TOWN:
		mapName = MAP_STR_TOWN;
		break;
	case TEMP_SECOND_MAP:
		mapName = MAP_STR_SECONDMAP;
		break;
	}

	return mapName;
}

int ApplicationContext::FindAnimName(int playerRole, int animCode)
{
	int chrState;

	// 애니메이션 상수 string -> 디파인값으로 바꿀예정
	switch (animCode)
	{
	case ANIM_IDLE:
		chrState = AnimationController::PlayerState::STATE_IDLE;
		break;
	case ANIM_FORWARD:
		chrState = AnimationController::PlayerState::STATE_FORWARD;
		break;
	case ANIM_BACKWARD:
		chrState = AnimationController::PlayerState::STATE_BACKWARD;
		break;
	case ANIM_LEFT_STRAFE:
		switch (playerRole)
		{
		case ROLE_MASTER:
			chrState = AnimationController::PlayerState::STATE_LEFT_STRAFE;
			break;
		case ROLE_STUDENT:
			chrState = AnimationController::PlayerState::STATE_FORWARD;
			break;
		}
		break;
	case ANIM_RIGHT_STRAFE:
		switch (playerRole)
		{
		case ROLE_MASTER:
			chrState = AnimationController::PlayerState::STATE_RIGHT_STRAFE;
			break;
		case ROLE_STUDENT:
			chrState = AnimationController::PlayerState::STATE_FORWARD;
			break;
		}
		break;
	case ANIM_ATTACK:
		chrState = AnimationController::PlayerState::STATE_ATTACK;
		break;
	case ANIM_JUMP:
		chrState = AnimationController::PlayerState::STATE_JUMP;
		break;
	case ANIM_RUNNING:
		chrState = AnimationController::PlayerState::STATE_FORWARD;
		break;
	default:
		cout << "None Anim Code" << endl;
		break;
	}

	return chrState;
}

XMFLOAT3 ApplicationContext::FindSpawnLocation(std::string mapName, int spawnLocation)
{
	std::vector<MapTool::PlayerInfo> playerInfoVec = m_Maps[mapName]->playerInfoVector;

	for (auto& p : playerInfoVec)
	{
		if (spawnLocation == p.spawnPos)
		{
			return p.position;
		}
	}

	return XMFLOAT3(0, 0, 0);
}

std::string ApplicationContext::FindClientInstanceID(std::string mapName, std::string meshName)
{
	if (!m_Maps.count(mapName)) return "";

	std::string type = "";

	if (m_Maps[mapName]->mapInfoDic.count(meshName))
		type = m_Maps[mapName]->mapInfoDic[meshName];
	else if (meshName == std::to_string(OBJECT_TYPE_THUNDERBOLT))
		type = std::to_string(OBJECT_TYPE_THUNDERBOLT);

	return type;
}

void ApplicationContext::SetUITexture(std::string uiType, std::string meshName, std::string uiName, std::string uiTextureName, std::string uiPressedTextureName, std::string uiDisabledTextureName, std::string uiSelectedTextureName)
{
	if (uiType == UI_TYPE_IMAGEVIEW)
	{
		ImageView* iv = FindObject<ImageView>(meshName, uiName);
		iv->m_MaterialIndex = AssertsReference::GetApp()->m_Materials[uiTextureName]->DiffuseSrvHeapIndex;
		iv->SetReleasedTexture(AssertsReference::GetApp()->m_Materials[uiTextureName]->DiffuseSrvHeapIndex);

		if (uiPressedTextureName != "")
			iv->SetPressedTexture(AssertsReference::GetApp()->m_Materials[uiPressedTextureName]->DiffuseSrvHeapIndex);
		if (uiDisabledTextureName != "")
			iv->SetDisabledTexture(AssertsReference::GetApp()->m_Materials[uiDisabledTextureName]->DiffuseSrvHeapIndex);
		if(uiSelectedTextureName != "")
			iv->SetSelectedTexture(AssertsReference::GetApp()->m_Materials[uiSelectedTextureName]->DiffuseSrvHeapIndex);
	}
	else if (uiType == UI_TYPE_BUTTON)
	{
		Button* btn = FindObject<Button>(meshName, uiName);
		btn->m_MaterialIndex = AssertsReference::GetApp()->m_Materials[uiTextureName]->DiffuseSrvHeapIndex;
		btn->SetReleasedTexture(AssertsReference::GetApp()->m_Materials[uiTextureName]->DiffuseSrvHeapIndex);

		if (uiPressedTextureName != "")
			btn->SetPressedTexture(AssertsReference::GetApp()->m_Materials[uiPressedTextureName]->DiffuseSrvHeapIndex);
		if (uiDisabledTextureName != "")
			btn->SetDisabledTexture(AssertsReference::GetApp()->m_Materials[uiDisabledTextureName]->DiffuseSrvHeapIndex);
	}
}

void ApplicationContext::CreateSkycube(std::string skycubeName, std::string instID, std::string matName)
{
	/* SkyCube */
	GameObject* sky = CreateObject<GameObject>(skycubeName, instID);
	sky->SetMesh(MESH_GEOID, MESH_GEOID_SPHERE);
	sky->m_MaterialIndex = AssertsReference::GetApp()->m_Materials[matName]->DiffuseSrvHeapIndex;
	sky->m_IsVisible = true;
	sky->m_IsVisibleOnePassCheck = true;
	sky->m_IsChangeable = false;
	// 나중 충돌처리에서 문제가 된다면 변경
	sky->m_World = MathHelper::Identity4x4();
	sky->m_TexTransform = MathHelper::Identity4x4();

	sky->Scale(SKYBOX_SCALE, SKYBOX_SCALE, SKYBOX_SCALE);
}

void ApplicationContext::CreateProps(std::string mapName)
{
	std::vector<MapTool::MapInfo> mapInfoVector = m_Maps[mapName]->mapInfoVector;

	for (auto& itemInfo : mapInfoVector)
	{
		GameObject* item = CreateObject<GameObject>(itemInfo.meshName, std::to_string(itemInfo.typeID));
		item->SetServerMeshID(itemInfo.proptype);
		item->SetMesh(itemInfo.meshName, itemInfo.meshName);
		item->m_MaterialIndex = AssertsReference::GetApp()->m_Materials[itemInfo.textureName]->DiffuseSrvHeapIndex;
		item->m_IsVisible = false;
		item->m_IsVisibleOnePassCheck = false;
		item->m_IsChangeable = itemInfo.changeable;
	}
}

void ApplicationContext::CreateContourProps(std::string mapName)
{
	std::vector<MapTool::MapInfo> mapInfoVector = m_Maps[mapName]->mapInfoChangeableVector;

	for (auto& itemInfo : mapInfoVector)
	{
		GameObject* item = CreateObject<GameObject>(itemInfo.meshName + "Contour", std::to_string(itemInfo.typeID));
		item->SetServerMeshID(itemInfo.proptype);
		item->SetMesh(itemInfo.meshName, itemInfo.meshName);
		item->m_MaterialIndex = AssertsReference::GetApp()->m_Materials[itemInfo.textureName]->DiffuseSrvHeapIndex;
		item->m_IsVisible = false;
		item->m_IsChangeable = itemInfo.changeable;
		item->m_World = FindObject<GameObject>(itemInfo.meshName, to_string(itemInfo.typeID))->m_World;
	}

}

void ApplicationContext::CreateCharacter(std::string meshName, std::string instID, std::string matName, int skinnedCBIndex)
{
	Character* chr = CreateObject<Character>(meshName, instID);
	chr->SetMesh(meshName, meshName);
	chr->SetMaterial(AssertsReference::GetApp()->m_Materials[matName]->DiffuseSrvHeapIndex);
	chr->SetAnimationController(AssertsReference::GetApp()->m_SkinnedModelInsts[meshName].get());
	chr->m_SkinnedCBIndex = skinnedCBIndex;
	chr->m_IsVisible = false;
	chr->m_IsVisibleOnePassCheck = false;
	chr->m_IsChangeable = false;
	// 임시 스폰위치 지정
	chr->m_SpawnLoaction = skinnedCBIndex;
}

void ApplicationContext::CreateUI3D(std::string mapName)
{
	std::vector<MapTool::UIInfo> uiInfoVector = m_Maps[mapName]->uiInfoVector;

	// ui default image -> TEXTURE_STR_UI_BOARD
	for (auto& itemInfo : uiInfoVector)
	{
		if (itemInfo.type == UI_TYPE_BUTTON)
		{
			Button* item = CreateObject<Button>(itemInfo.meshName, itemInfo.uiName);
			item->SetMesh(MESH_GEOID, itemInfo.meshName);
			item->m_MaterialIndex = AssertsReference::GetApp()->m_Materials[TEXTURE_STR_UI_BOARD]->DiffuseSrvHeapIndex;
			item->m_UIName = itemInfo.uiName;
			item->m_IsVisible = false;
			item->m_IsVisibleOnePassCheck = false;
			item->m_IsChangeable = true;
		}
		else if (itemInfo.type == UI_TYPE_IMAGEVIEW)
		{
			ImageView* item = CreateObject<ImageView>(itemInfo.meshName, itemInfo.uiName);
			item->SetMesh(MESH_GEOID, itemInfo.meshName);
			item->m_MaterialIndex = AssertsReference::GetApp()->m_Materials[TEXTURE_STR_UI_BOARD]->DiffuseSrvHeapIndex;
			item->m_UIName = itemInfo.uiName;
			item->m_IsVisible = false;
			item->m_IsVisibleOnePassCheck = false;
			item->m_IsChangeable = true;
		}
	}
}

void ApplicationContext::CreateUI2D(std::string ui2dLayer, std::string ui2dName, int matIndex, int uiPressedTextureIdx, int uiDisabledTextureIdx)
{
	UserInterface* item = CreateObject<UserInterface>(ui2dLayer, ui2dName);
	item->SetMesh(MESH_GEOID, MESH_GEOID_GRID);
	item->m_MaterialIndex = matIndex;
	item->SetOriginMaterial(matIndex);
	item->m_IsVisible = false;
	item->m_IsVisibleOnePassCheck = false;
	item->m_IsChangeable = false;

	// 기본 & released
	item->SetReleasedTexture(matIndex);

	if (uiPressedTextureIdx != -1)
		item->SetPressedTexture(uiPressedTextureIdx);
	if (uiDisabledTextureIdx != -1)
		item->SetDisabledTexture(uiDisabledTextureIdx);

	//// 아래 내용은 Display에서
	//// 800, 600 -> display width / height 로 변경
	//item->m_PositionRatio = { (posX - (sizeX / 20.f)) / Core::g_DisplayWidth, (posY - (sizeY / 20.f))/ Core::g_DisplayHeight };
	//item->m_SizeRatio = { sizeX / sizeY, sizeY / Core::g_DisplayHeight };

	//item->Scale(sizeX, sizeY, 1);
	//item->SetPosition(posX, posY, 1.f);
}

void ApplicationContext::CreateThunderBolt()
{
	for (int i = 0; i < MAX_THUNDERBOLT_COUNT; ++i)
	{
		GameObject* thunderBolt = CreateObject<GameObject>(std::to_string(OBJECT_TYPE_THUNDERBOLT), std::to_string(OBJECT_START_INDEX_THUNDERBOLT + i));
		thunderBolt->SetServerMeshID(OBJECT_TYPE_THUNDERBOLT);
		thunderBolt->SetMesh(MESH_GEOID, MESH_GEOID_SPHERE);
		thunderBolt->m_MaterialIndex = TEXTURE_INDEX_Thunderbolt;
		thunderBolt->m_IsVisible = false;
		thunderBolt->m_IsVisibleOnePassCheck = false;
		thunderBolt->m_IsChangeable = false;
	}
}

void ApplicationContext::CreateMagicCircle(std::string instName)
{
	GameObject* magicCircle = CreateObject<GameObject>(OBJECT_TYPE_MAGIC_CIRCLE, instName);
	magicCircle->SetMesh(MESH_GEOID, MESH_GEOID_RECT);
	magicCircle->m_MaterialIndex = TEXTURE_INDEX_MAGIC_CIRCLE;
	magicCircle->m_OriginMatIndex = TEXTURE_INDEX_MAGIC_CIRCLE;
	magicCircle->m_IsVisible = false;
	magicCircle->m_IsVisibleOnePassCheck = false;
	magicCircle->m_IsChangeable = false;
}

void ApplicationContext::CreateWeapon(std::string weaponName, std::string subWeaponName)
{
	CharacterParts* weapon = CreateObject<CharacterParts>(weaponName, subWeaponName);
	// 서버메쉬아이디는 존재하지않음
	weapon->SetServerMeshID(-99);
	weapon->SetMesh(weaponName, subWeaponName);
	weapon->m_MaterialIndex = TEXTURE_INDEX_Polygon_Fantasy_Characters_Texture_01_A;
	weapon->m_IsVisible = true;
	weapon->m_IsVisibleOnePassCheck = true;
	weapon->m_IsChangeable = false;

	weapon->Rotate(45.f, 0, 0);
	weapon->SetPosition(0, 0, 0);
}

void ApplicationContext::CreateParticle(std::string particleName, std::string instID, std::string matName, bool isLoop, DirectX::XMFLOAT3 offset, float particlePlaySpeed)
{
	Particle* particle = CreateObject<Particle>(particleName, instID);
	particle->SetMesh(particleName, particleName);
	particle->m_MaterialIndex = AssertsReference::GetApp()->m_Materials[matName]->DiffuseSrvHeapIndex;
	particle->m_IsVisible = false;
	particle->m_IsVisibleOnePassCheck = false;
	particle->m_IsChangeable = false;
	particle->SetIsLoop(isLoop);
	particle->SetParticleOffset(offset);
	particle->SetParticlePlaySpeed(particlePlaySpeed);
}

void ApplicationContext::DisplayProps(std::string mapName, bool isScale, float scaleValue)
{
	if (!m_Maps.count(mapName))
		return;

	// Props
	std::vector<MapTool::MapInfo> mapInfoVector = m_Maps[mapName]->mapInfoVector;
	for (auto& itemInfo : mapInfoVector)
	{
		GameObject* obj = FindObject<GameObject>(itemInfo.meshName, std::to_string(itemInfo.typeID));

		obj->InitializeTransform();
		obj->m_IsVisible = true;
		obj->m_IsVisibleOnePassCheck = true;
		if (isScale) obj->Scale(scaleValue, scaleValue, scaleValue);
		obj->Rotate(itemInfo.rotation.x, itemInfo.rotation.y, itemInfo.rotation.z);
		obj->SetPosition(itemInfo.position);
	}
}

void ApplicationContext::HiddenProps(std::string mapName)
{
	if (!m_Maps.count(mapName))
		return;

	// Props
	std::vector<MapTool::MapInfo> mapInfoVector = m_Maps[mapName]->mapInfoVector;
	for (auto& itemInfo : mapInfoVector)
	{
		GameObject* obj = FindObject<GameObject>(itemInfo.meshName, std::to_string(itemInfo.typeID));

		ZeroMemory(&obj->m_World, sizeof(obj->m_World));
		ZeroMemory(&obj->m_TexTransform, sizeof(obj->m_TexTransform));
	}

	// Update InstanceData
	for (auto& prop : m_Maps[mapName]->propTypeVector)
	{
		GraphicsContext::GetApp()->UpdateInstanceData(m_RItemsMap[prop], m_RItemsVec);
	}

	// Visible Off
	for (auto& itemInfo : mapInfoVector)
	{
		GameObject* obj = FindObject<GameObject>(itemInfo.meshName, std::to_string(itemInfo.typeID));
		obj->m_IsVisible = false;
		obj->m_IsVisibleOnePassCheck = false;
	}
}

void ApplicationContext::DisplayCharacter(std::string mapName, Character* user, int spawnLocation, bool isVisible)
{
	if (!m_Maps.count(mapName)) return;
	if (!user) return;

	std::vector<MapTool::PlayerInfo> playerInfoVec = m_Maps[mapName]->playerInfoVector;

	for (auto& p : playerInfoVec)
	{
		if (spawnLocation == p.spawnPos)
		{
			user->InitializeTransform();
			user->SetAnimationKeyState(AnimationController::PlayerState::STATE_IDLE);
			user->SetAnimationPlayerState(AnimationController::PlayerState::STATE_IDLE);

			user->m_IsVisible = isVisible;
			user->m_IsVisibleOnePassCheck = isVisible;
			user->m_HP = 1.f;
			user->Rotate(0, XMConvertToRadians(p.rotY), 0);
			user->SetPosition(p.position);
		}
	}
}

void ApplicationContext::DisplayCharacter(std::string mapName, std::string userName, int spawnLocation, bool isVisible)
{
	if (!m_Maps.count(mapName)) return;
	Character* user = FindObject<Character>(userName, userName);
	if (!user) return;

	std::vector<MapTool::PlayerInfo> playerInfoVec = m_Maps[mapName]->playerInfoVector;

	for (auto& p : playerInfoVec)
	{
		if (spawnLocation == p.spawnPos)
		{
			user->InitializeTransform();
			user->SetAnimationKeyState(AnimationController::PlayerState::STATE_IDLE);
			user->SetAnimationPlayerState(AnimationController::PlayerState::STATE_IDLE);

			user->m_IsVisible = isVisible;
			user->m_IsVisibleOnePassCheck = isVisible;
			user->Rotate(0, XMConvertToRadians(p.rotY), 0);
			user->SetPosition(p.position);

			return;
		}
	}
}

void ApplicationContext::HiddenCharacter(Character* user)
{
	if (!user) return;

	user->m_MyCamera = nullptr;
	user->SetAnimationKeyState(AnimationController::PlayerState::STATE_IDLE);
	user->SetAnimationPlayerState(AnimationController::PlayerState::STATE_IDLE);
	user->m_PlayerController.release();
	user->ControlBlurEffect(false);

	ZeroMemory(&user->m_World, sizeof(user->m_World));
	ZeroMemory(&user->m_TexTransform, sizeof(user->m_TexTransform));

	user->ReleaseTransform();

	// Update InstanceData
	GraphicsContext::GetApp()->UpdateInstanceData(m_RItemsMap[user->GetMeshName()], m_RItemsVec);

	// InstanceData 업데이트 후에 visible 꺼주기!
	user->m_IsVisible = false;
	user->m_IsVisibleOnePassCheck = false;
}

void ApplicationContext::HiddenCharacter(std::string userName)
{
	Character* user = FindObject<Character>(userName, userName);
	if (!user) return;

	user->m_MyCamera = nullptr;
	user->SetAnimationKeyState(AnimationController::PlayerState::STATE_IDLE);
	user->SetAnimationPlayerState(AnimationController::PlayerState::STATE_IDLE);
	user->m_PlayerController.release();
	user->ControlBlurEffect(false);

	ZeroMemory(&user->m_World, sizeof(user->m_World));
	ZeroMemory(&user->m_TexTransform, sizeof(user->m_TexTransform));

	user->ReleaseTransform();

	// Update InstanceData
	GraphicsContext::GetApp()->UpdateInstanceData(m_RItemsMap[userName], m_RItemsVec);

	user->m_IsVisible = false;
	user->m_IsVisibleOnePassCheck = false;
}

void ApplicationContext::DisplayUI(std::string mapName)
{
	if (!m_Maps.count(mapName))
		return;

	// UI
	std::vector<MapTool::UIInfo> uiInfoVector = m_Maps[mapName]->uiInfoVector;
	for (auto& itemInfo : uiInfoVector)
	{
		UserInterface* obj = FindObject<UserInterface>(itemInfo.meshName, itemInfo.uiName);

		obj->InitializeTransform();
		obj->m_IsVisible = true;
		obj->m_IsVisibleOnePassCheck = true;
		obj->Scale(itemInfo.scale.x, itemInfo.scale.y, itemInfo.scale.z);
		obj->Rotate(itemInfo.rotation.x, itemInfo.rotation.y, itemInfo.rotation.z);
		obj->SetPosition(itemInfo.position);
	}
}

void ApplicationContext::HiddenUI(std::string mapName)
{
	if (!m_Maps.count(mapName))
		return;

	// UI
	std::vector<MapTool::UIInfo> uiInfoVector = m_Maps[mapName]->uiInfoVector;

	for (auto& itemInfo : uiInfoVector)
	{
		UserInterface* obj = FindObject<UserInterface>(itemInfo.meshName, itemInfo.uiName);

		ZeroMemory(&obj->m_World, sizeof(obj->m_World));
		ZeroMemory(&obj->m_TexTransform, sizeof(obj->m_TexTransform));
	}

	// Update InstanceData
	GraphicsContext::GetApp()->UpdateInstanceData(m_RItemsMap[MESH_GEOID_RECT], m_RItemsVec);
	//GraphicsContext::GetApp()->UpdateInstanceData(AppContext->m_RItemsMap[UI_MESH_SPHERE], AppContext->m_RItemsVec);

	// Visible Off
	for (auto& itemInfo : uiInfoVector)
	{
		UserInterface* obj = FindObject<UserInterface>(itemInfo.meshName, itemInfo.uiName);
		obj->m_IsVisible = false;
		obj->m_IsVisibleOnePassCheck = false;
	}
}

void ApplicationContext::DisplayUI2D(std::string ui2dLayer, std::string ui2dName, XMFLOAT2 pos, XMFLOAT2 size, bool isChangable, AnchorType anchorType, int zLayer, bool isText)
{
	UserInterface* ui = FindObject<UserInterface>(ui2dLayer, ui2dName);
	if (!ui) return;

	ui->InitializeTransform();
	ui->m_IsVisible = true;
	ui->m_IsVisibleOnePassCheck = true;
	ui->m_IsChangeable = isChangable;
	ui->m_ZLayer = zLayer;
	ui->m_IsText = isText;

	switch (anchorType)
	{
	case AnchorType::LT:
		ui->SetAnchorType(AnchorType::LT);
		ui->m_PositionRatio = { WIDTH_NORMALIZE_LT((pos.x - (size.x / 20.f))), HEIGHT_NORMALIZE_LT(pos.y + (size.y / 20.f)) };
		ui->m_SizeRatio = { size.x / size.y, size.y };

		break;
	case AnchorType::Center:
		ui->SetAnchorType(AnchorType::Center);
		ui->m_PositionRatio = { ((pos.x - (size.x / 20.f))), -(pos.y + (size.y / 20.f)) };
		ui->m_SizeRatio = { size.x / size.y, size.y / 1080.f };

		break;
	default:
		break;
	}
	// ui->m_PositionRatio = { (pos.x - (size.x / 20.f)) / 800.f, (pos.y - (size.y / 20.f)) / 600.f };
	// ui->m_SizeRatio = { size.x / size.y, size.y / 1080.f };

	ui->Scale(size.x, size.y, 1);
	ui->SetPosition(pos.x, pos.y, 1.f);

	ui->m_UIPos = XMFLOAT2(pos.x, pos.y);
	ui->m_UISize = XMFLOAT2(size.x * 0.1f, size.y * 0.1f);
}

void ApplicationContext::SetDisplayUI2D(std::string ui2dLayer, std::string ui2dName, bool isVisible)
{
	UserInterface* ui = FindObject<UserInterface>(ui2dLayer, ui2dName);
	if (!ui) return;

	ui->m_IsVisible = isVisible;
	// ui->m_IsVisibleOnePassCheck = isVisible;
}

void ApplicationContext::SetPickingUI2D(std::string ui2dLayer, std::string ui2dName, bool isVisible)
{
	UserInterface* ui = FindObject<UserInterface>(ui2dLayer, ui2dName);
	if (!ui) return;

	ui->m_IsChangeable = isVisible;
}

void ApplicationContext::DisplayMagicCircle(std::string instID, XMFLOAT3 pos)
{
	GameObject* obj = FindObject<GameObject>(OBJECT_TYPE_MAGIC_CIRCLE, instID);
	if (!obj) return;

	obj->m_IsVisible = true;
	obj->m_IsVisibleOnePassCheck = true;
	obj->InitializeTransform();
	obj->Scale(250, 0.2, 250);
	obj->SetPosition(pos);
}

void ApplicationContext::DisplayMagicCircle(std::string instID, std::string mapName, int spawnLocation, bool isMe)
{
	GameObject* obj = FindObject<GameObject>(OBJECT_TYPE_MAGIC_CIRCLE,instID);
	if (!obj) return;

	std::vector<MapTool::PlayerInfo> playerInfoVec = m_Maps[mapName]->playerInfoVector;

	for (auto& p : playerInfoVec)
	{
		if (spawnLocation == p.spawnPos)
		{
			obj->m_IsVisible = true;
			obj->m_IsVisibleOnePassCheck = true;
			obj->InitializeTransform();
			obj->Scale(250, 0.2, 250);
			obj->SetPosition(p.position);

			if (isMe) obj->m_MaterialIndex = TEXTURE_INDEX_MAGIC_CIRCLE_GREEN;
			else obj->m_MaterialIndex = TEXTURE_INDEX_MAGIC_CIRCLE;

			return;
		}
	}
}

void ApplicationContext::HiddenMagicCircle(std::string instID)
{
	GameObject* obj = FindObject<GameObject>(OBJECT_TYPE_MAGIC_CIRCLE,instID);
	if (!obj) return;

	ZeroMemory(&obj->m_World, sizeof(obj->m_World));
	ZeroMemory(&obj->m_TexTransform, sizeof(obj->m_TexTransform));
	obj->m_MaterialIndex = obj->m_OriginMatIndex;

	GraphicsContext::GetApp()->UpdateInstanceData(m_RItemsMap[OBJECT_TYPE_MAGIC_CIRCLE], m_RItemsVec);

	//obj->m_IsVisible = false;
	//obj->m_IsVisibleOnePassCheck = false;
}

void ApplicationContext::HiddenUI2D(std::string ui2dLayer, std::string ui2dName)
{

	UserInterface* ui = FindObject<UserInterface>(ui2dLayer, ui2dName);
	if (!ui) return;

	ZeroMemory(&ui->m_World, sizeof(ui->m_World));
	ZeroMemory(&ui->m_TexTransform, sizeof(ui->m_TexTransform));

	GraphicsContext::GetApp()->UpdateInstanceData(m_RItemsMap[ui2dLayer], m_RItemsVec);

	ui->m_IsChangeable = false;
	ui->m_IsVisible = false;
	ui->m_IsVisibleOnePassCheck = false;
}

void ApplicationContext::DisplayThunderBolt(int instID, float posX, float posY, float posZ)
{
	GameObject* obj = FindObject<GameObject>(std::to_string(OBJECT_TYPE_THUNDERBOLT), std::to_string(OBJECT_START_INDEX_THUNDERBOLT + instID));
	if (!obj) return;

	obj->m_IsVisible = true;
	obj->m_IsVisibleOnePassCheck = true;
	obj->InitializeTransform();
	obj->Scale(30, 30, 30);
	obj->SetPosition(posX, posY, posZ);
}

void ApplicationContext::HiddenThunderBolt(int instID, bool isVisible)
{
	GameObject* obj = FindObject<GameObject>(std::to_string(OBJECT_TYPE_THUNDERBOLT), std::to_string(OBJECT_START_INDEX_THUNDERBOLT + instID));
	if (!obj) {
		cout << "HiddenThunderBolt: cant find obj" << endl;
		return;
	}

	ZeroMemory(&obj->m_World, sizeof(obj->m_World));
	ZeroMemory(&obj->m_TexTransform, sizeof(obj->m_TexTransform));

	// 썬더볼트 visible 관리 수정하기
	if (isVisible)
	{
		GraphicsContext::GetApp()->UpdateInstanceData(m_RItemsMap[std::to_string(OBJECT_TYPE_THUNDERBOLT)], m_RItemsVec);
		obj->m_IsVisibleOnePassCheck = false;
	}
}

void ApplicationContext::DisplayParticle(std::string particleName, std::string instID, DirectX::XMFLOAT3 pos, bool isVisible, bool isCapture)
{
	Particle* ptc = FindObject<Particle>(particleName, instID);
	if (!ptc) return;

	ptc->m_IsVisible = isVisible;
	ptc->m_IsVisibleOnePassCheck = isVisible;
	ptc->InitializeTransform();

	if (isCapture)
	{
		ptc->CapturePosition();
	}
	else
		ptc->SetPosition(pos);
	ptc->PlayParticle();
	// rotate
}

void ApplicationContext::HiddenParticle(std::string particleName, std::string instID)
{
	Particle* ptc = FindObject<Particle>(particleName, instID);
	if (!ptc) return;

	ZeroMemory(&ptc->m_World, sizeof(ptc->m_World));
	ZeroMemory(&ptc->m_TexTransform, sizeof(ptc->m_TexTransform));
	ptc->StopParticle();

	// Update InstanceData
	GraphicsContext::GetApp()->UpdateInstanceData(m_RItemsMap[particleName], m_RItemsVec);

	ptc->m_IsVisible = false;
	ptc->m_IsVisibleOnePassCheck = false;
}

void ApplicationContext::CreateDebugBoundingBox(std::string boundsName, std::string boundsInstName)
{
	GameObject* item = CreateObject<GameObject>(boundsName, boundsInstName);
	item->SetMesh(MESH_GEOID, MESH_GEOID_RECT);
	item->m_PrimitiveType = D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
	item->m_MaterialIndex = 1;
	item->m_IsVisible = true;
	item->m_IsChangeable = true;
}
