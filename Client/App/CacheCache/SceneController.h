#pragma once
#include "Controller.h"

class TitleScene;
class LobbyScene;
class GameroomScene;
class GameplayScene;
class GameresultScene;

// �� ������ Controller ����
class TitleController final : public Controller
{
public:
	explicit TitleController(TitleScene* myScene);
	virtual ~TitleController() = default;

public:
	virtual void Update(const float deltaT) override;

private:
	virtual void HandleInput(const float deltaT) override;
	virtual void MouseCallback() override;
	
	bool CheckID();
	bool CheckPW();

private:
	TitleScene* m_MyScene;
	std::string m_PickedUIName;
	std::string m_PickedUIObjectName;

	int m_IsInput;
};

class LobbyController final : public Controller
{
public:
	explicit LobbyController(LobbyScene* myScene);
	virtual ~LobbyController() = default;

public:
	virtual void Update(const float deltaT) override;

private:
	virtual void HandleInput(const float deltaT) override;
	virtual void MouseCallback() override;

	bool CheckRoomName();

private:
	LobbyScene* m_MyScene;
	std::string m_PickedUIName;
	std::string m_PickedUIObjectName;


};

class GameroomController final : public Controller
{
public:
	explicit GameroomController(GameroomScene* myScene);
	virtual ~GameroomController() = default;

public:
	virtual void Update(const float deltaT) override;

private:
	virtual void HandleInput(const float deltaT) override;
	virtual void MouseCallback() override;


private:
	GameroomScene* m_MyScene;
	bool m_IsSelected = false;
	std::string m_PickedUIName;
};

class GameplayController final : public Controller
{
public:
	explicit GameplayController(GameplayScene* myScene);
	virtual ~GameplayController() = default;

public:
	virtual void Update(const float deltaT) override;

private:
	virtual void HandleInput(const float deltaT) override;
	virtual void MouseCallback() override;

private:
	GameplayScene* m_MyScene;
};

class GameresultController final : public Controller
{
public:
	explicit GameresultController(GameresultScene* myScene);
	virtual ~GameresultController() = default;

public:
	virtual void Update(const float deltaT) override;

private:
	virtual void HandleInput(const float deltaT) override;
	virtual void MouseCallback() override;

private:
	GameresultScene* m_MyScene;
};