#pragma once
#include "Singleton.h"
#include "SceneManager.h"
#include "Network.h"
#include <functional>

//#define EVENT_TITLE_CONNECT_REQUEST 0xA1
#define EVENT_TITLE_CONNECT_ALLOW 0xA2
#define EVENT_TITLE_CONNECT_DENY 0xA3
#define EVENT_TITLE_LOGIN_REQUEST 0xA4
#define EVENT_TITLE_LOGIN_ALLOW 0xA5
#define EVENT_TITLE_LOGIN_DENY 0xA6
#define EVENT_TITLE_SIGNUP_SUCCESS 0xA7
#define EVENT_TITLE_SIGNUP_DENY 0XA8
#define EVENT_TITLE_SIGNUP_REQUEST  0XA9

#define EVENT_LOBBY_AUTOMATCH_REQUEST 0xB1
#define EVENT_LOBBY_AUTOMATCH_SUCCESS 0xB2
#define EVENT_LOBBY_AUTOMATCH_ACCEPTOK	0xB3
#define EVENT_LOBBY_AUTOMATCH_CANCEL_REQUEST	0xB4
#define EVENT_LOBBY_AUTOMATCH_CANCEL_SUCCESS	0xB5
#define EVENT_LOBBY_MANUALMATCH_REQUEST 0xB6
#define EVENT_LOBBY_MANUALMATCH_ACCEPTOK 0xB7
#define EVENT_LOBBY_MANUALMATCH_ROOM_LIST 0xB8
/* ROOM_JOIN: 방참가 / ROOM_MAKE: 방만들기 */
#define EVENT_LOBBY_MANUALMATCH_ROOM_JOIN 0xB9
#define EVENT_LOBBY_MANUALMATCH_ROOM_MAKE 0xBA
#define EVENT_LOBBY_MANUALMATCH_ROOM_JOIN_ALLOW 0xBB
#define EVENT_LOBBY_MANUALMATCH_ROOM_JOIN_DENY 0xBC
#define EVENT_LOBBY_CLIENT_MMRINFO 0xBD
#define EVENT_LOBBY_MANUALMATCH_ROOMLIST_REQUEST 0xBE
#define EVENT_LOBBY_UPDATE_USERINFO 0xBF
#define EVENT_LOBBY_REQUEST_USERINFO 0xB11

#define EVENT_GAMEROOM_PLAYER_ENTER 0xC1
#define EVENT_GAMEROOM_PLAYER_INPUT_READY 0xC2
#define EVENT_GAMEROOM_PLAYER_CALLBACK_READY 0xC3
#define EVENT_GAMEROOM_PLAYER_LEAVE 0xC4
#define EVENT_GAMEROOM_PLAYER_HOST 0xC5
#define EVENT_GAMEROOM_INPUT_SELECT_CHARACTER 0xC6
#define EVENT_GAMEROOM_INPUT_SELECT_CANCEL_CHARACTER 0xC7
#define EVENT_GAMEROOM_CALLBACK_SELECT_CHARACTER 0xC8
#define EVENT_GAMEROOM_CALLBACK_SELECT_CANCEL_CHARACTER 0xC9
#define EVENT_GAMEROOM_EXIT 0xCA
#define EVENT_GAMEROOM_INPUT_MAPINFO 0xCB
#define EVENT_GAMEROOM_CALLBACK_MAPINFO 0xCC
#define EVENT_GAMEROOM_GAME_START 0xCD
#define EVENT_GAMEROOM_PREV_MAP 0xCE
#define EVENT_GAMEROOM_NEXT_MAP 0xCF
#define EVENT_GAMEROOM_GAMESTART_AVAILABLE 0xD0
#define EVENT_GAMEROOM_SEND_CHAT	0xD1
#define EVENT_GAMEROOM_RECV_CHAT	0xD2

#define EVENT_LOADING_GAMEPLAY_INFO 0xD3

#define EVENT_GAMEPLAY_START 0xE1
#define EVENT_GAMEPLAY_INPUT_MOVE 0xE2
#define EVENT_GAMEPLAY_INPUT_MOUSE 0xE3
#define EVENT_GAMEPLAY_CALLBACK_MOVE 0xE4
#define EVENT_GAMEPLAY_CALLBACK_MOUSE 0xE5
#define EVENT_GAMEPLAY_OBJECT_POS 0xE6
#define EVENT_GAMEPLAY_ROUND_START 0xE7
#define EVENT_GAMEPLAY_TRAMNSFORM_SEND 0xE8
#define EVENT_GAMEPLAY_TRAMNSFORM_RECV 0xE9
#define EVENT_GAMEPLAY_PUT_THUNDERBOLT 0xEA1
#define EVENT_GAMEPLAY_PLAYER_DIE 0xEA2
#define EVENT_GAMEPLAY_TIMER 0xEA3
#define EVENT_GAMEPLAY_REMOVE_THUNDERBOLT 0xEA4
#define EVENT_GAMEPLAY_ANIMATE	0xEA5
#define EVENT_GAMEPLAY_GAMEOVER 0xEA6
#define EVENT_GAMEPLAY_HIT 0xEA7
#define EVENT_GAMEPLAY_PENALTY 0xEA8
#define EVENT_GAMEPLAY_PENALTY_WARNING 0xEA9
#define EVENT_GAMEPLAY_PENALTY_WARNING_END 0XEB0

#define EVENT_LOADING_GAMERESULT_INFO 0xF1
#define EVENT_GAMERESULT 0xF2

class Service : public TemplateSingleton<Service>
{
private:
	// io thraed param
	bool running = true;
	bool lobbyRunning = false;
	bool battleTCPRunning = false;
	bool battleUDPRunning = false;

private:
	XMFLOAT3 argLook = { 0.f,1.f,0.f };
	XMFLOAT3 argLookCallback = { 0.f,1.f,0.f };

public:
	void Termiante();
	void RunService();
	//void RunMainThread();

public:
	void Notify(int sEvent, int argsCount = 0, ...);

	bool IsConnectLobby() const { return lobbyRunning; }
	bool IsConnectBattleTCP() const { return battleTCPRunning; }
	bool IsConnectBattleUDP() const { return battleUDPRunning; }

public:
	void										GetRoomList(std::vector<PTC_Room>& ptcRoomlist);
	int											GetMyBattleID() const;
	BattleClient*								GetBattleClient(int id) const;
	int											GetBattleClientsCount() const;
	int											GetMyMapInfo() const;
	int											GetWinTeam() const;
	short										GetPenaltyDuration() const;

private:
	std::mutex m_MutexEvent;

	//// 사용안함.
	//std::queue<std::function<void()>> m_Events;
	//std::condition_variable m_CVEvent;
	//std::mutex m_MutexEnqueue;
	//std::mutex m_MutexDequeue;

	//// 예시
	//// 계속해서 패킷이 올경우 rock이 연속으로 걸려 프레임이 떨어지거나, 오류가 발생
	////EnqueueEvent([&]() { SceneManager::GetApp()->SendEventArgs(SceneType::GamePlay, EVENT_GAMEPLAY_REMOVE_THUNDERBOLT, argsCount, boltID); });
	//void EnqueueEvent(std::function<void()> fp);
};

