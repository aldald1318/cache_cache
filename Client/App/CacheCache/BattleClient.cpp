#include "pch.h"
#include "BattleClient.h"

size_t meshCount = 0;

BattleClient::BattleClient(int bID) :
	battleID(bID), m_PlayerNum(-1)
{
	InitMMR();
	Init();
}

BattleClient::~BattleClient()
{
}

void BattleClient::Init()
{
	m_PlayerMesh = CHARACTER_NONE;
	m_PlayerNum = -1;
	m_RoomNumber = -1;
	m_SpawnLocation = -1;
	m_Host = false;
	m_Ready = false;
}

void BattleClient::InitMMR()
{
	mmrInfo.mmr = -1;
	mmrInfo.catched_student_count = 0;
	mmrInfo.winning_rate = 0.f;
	mmrInfo.total_game_count = 0;
	mmrInfo.winning_game_count = 0;
}