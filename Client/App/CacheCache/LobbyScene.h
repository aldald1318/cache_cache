#pragma once
#include "Scene.h"
#include "BattleClient.h"

#define CLIENT_EVENT_LOBBY_UI_PRESSED 0xFFFF10
#define CLIENT_EVENT_LOBBY_INPUT_STRING 0xFFFA10
#define CLIENT_EVENT_LOBBY_UI_ROOMLIST_PRESSED 0xFFFA11
#define CLIENT_EVENT_LOBBY_UI_ROOMLIST_PAGE_PREV 0xFFFA12
#define CLIENT_EVENT_LOBBY_UI_ROOMLIST_PAGE_NEXT 0xFFFA13
#define CLIENT_EVENT_AUTOMATCH_START 0xFFFA14
#define CLIENT_EVENT_AUTOMATCH_CANCEL 0xFFFA15

namespace Graphics
{
	extern Microsoft::WRL::ComPtr<ID3D12PipelineState> g_SkinnedPSO;
	extern Microsoft::WRL::ComPtr<ID3D12PipelineState> g_ShadowOpaquePSO;
	extern Microsoft::WRL::ComPtr<ID3D12PipelineState> g_SkinnedShadowOpaquePSO;
	extern Microsoft::WRL::ComPtr<ID3D12PipelineState> g_UIPSO;
}


class GameObject;
class ObjectInfo;

class LobbyScene : public Scene
{
	friend class LobbyController;
private:
	virtual void ProcessEvent(int sEvent, int argsCount = 0, ...) override;
	virtual void UIEvent(int sEvent, int argsCount = 0, ...) override;

public:
	virtual void Initialize() override;
	virtual void OnResize() override;

public:
	virtual bool Enter() override;
	virtual void Exit() override;

	virtual void Update(const float& fDeltaTime) override;
	virtual void Render() override;
	virtual void RenderUI() override;

	void ResetRoomListSelect();

	void ThreadEventHandler();

private:
	int GetRoomNumber() const;
	void UpdateRoomList();

private:
	std::vector<PTC_Room> m_RoomList;
	bool m_IsGetRoomList;
	int m_RoomListPage = 0;
	int m_RoomListMaxPage = 0;
	int m_RoomListCntLastPage = 0;
	int m_SelectRoom = -1;

	std::wstring m_RoomNumber;

	bool m_IsAutoMatching = false;

	bool m_IsRuleManual = false;
	bool m_IsManualMatchRoomLlist = false;
	bool m_IsExitGame = false;
	bool m_IsMakeRoom = false;
	bool m_IsJoinDenyWin = false;
	std::wstring m_JoinDenyWarning = L"";

	wstring m_RoomTitle;
	string m_RoomTitleString;
	UINT m_RoomTitleWarning = 0;

	std::map<std::string, UITextInfo> m_RoomListInfo;

	MMRInfo m_MMRInfo;
};

