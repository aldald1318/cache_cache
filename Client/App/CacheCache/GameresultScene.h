#pragma once
#include "Scene.h"

namespace Graphics
{
	extern Microsoft::WRL::ComPtr<ID3D12PipelineState> g_SkinnedPSO;
	extern Microsoft::WRL::ComPtr<ID3D12PipelineState> g_ShadowOpaquePSO;
	extern Microsoft::WRL::ComPtr<ID3D12PipelineState> g_SkinnedShadowOpaquePSO;
	extern Microsoft::WRL::ComPtr<ID3D12PipelineState> g_UIPSO;
}

namespace InputHandler
{
	extern bool g_CursorSwitch;
}

class GameresultScene : public Scene
{
	friend class GameresultController;

private:
	virtual void ProcessEvent(int sEvent, int argsCount = 0, ...) override;

public:
	virtual void Initialize() override;
	virtual void OnResize() override;

public:
	virtual bool Enter() override;
	virtual void Exit() override;

	virtual void Update(const float& fDeltaTime) override;
	virtual void Render() override;
	virtual void RenderUI() override;

	void ThreadEventHandler();

private:
	std::string					m_MapName;
	int							m_WinTeam;
	std::vector<std::string>	m_PlayerNames;

	// 씬전환 시간 관련 파라미터
	// 게임결과씬은 DurationTime 후에 자동으로 씬전환된다.
	float m_SceneDurationTime = 5.f;
	float m_SceneDeltaTime = 0.f;
};

