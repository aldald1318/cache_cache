#include "pch.h"
#include "CACHE_CACHE.h"
#include "CommandContext.h"
#include "InputHandler.h"
#include "SceneManager.h"
#include "SoundManager.h"
#include "ApplicationContext.h"
#include "AssertsReference.h"
#include "Map.h"
#include "CommandCenter.h"
#include "ParticleSystem.h"

using namespace Core;

void CACHE_CACHE::Startup(void)
{
	// Camera
	m_Camera = new Camera;

	// Lights
	m_Lights[LIGHT_NAME_DIRECTIONAL] = std::make_unique<Light>();
	m_Lights[LIGHT_NAME_DIRECTIONAL]->Direction = { 0.57735f, -0.81735f, -1.07735 };
	m_Lights[LIGHT_NAME_DIRECTIONAL]->Strength = { 0.9f, 0.8f, 0.7f };

	// Create Tools
	m_SceneManager = SceneManager::GetApp();
	m_SoundManager = SoundManager::GetApp();
	m_AppContext = ApplicationContext::GetApp();
	m_AssetsRef = AssertsReference::GetApp();
	m_CommandCenter = CommandCenter::GetApp();
	m_ParticleSystem = ParticleSystem::GetApp();

	// Build Sounds
	SoundManager::GetApp()->ReadySoundManager("./Sounds/");

	// LoadFont
	GraphicsContext::GetApp()->LoadFont(L"Verdana",20);

	// Build Asserts / Render Items
	BuildAsserts();
	BuildCharactersAndParticles();

	SceneManager::GetApp()->InitializeScenes();
#ifdef DEBUG_CLIENT
	SceneManager::GetApp()->EnterScene(SceneType::GamePlay);
#elif DEBUG_SERVER
	SceneManager::GetApp()->EnterScene(SceneType::Title);
#endif

	// Build GpuResources
	GraphicsContext::GetApp()->passCount = 2;
	GraphicsContext::GetApp()->skinnedObjectCount = BoneIndex::Count;
	GraphicsContext::GetApp()->materialCount = AssertsReference::GetApp()->m_Materials.size();

	for (auto& p : AppContext->m_RItemsMap)
	{
		GraphicsContext::GetApp()->BuildInstanceBuffer(p.second);
	}
}

void CACHE_CACHE::Cleanup(void)
{
	/* Clear Cameras */
	SAFE_DELETE_PTR(m_Camera);

	/* Clear Maps */
	for (auto& p : AppContext->m_Maps)
	{
		SAFE_DELETE_PTR(p.second);
	}
	AppContext->m_Maps.clear();

	/* Clear GameObjects */
	for (auto& p : AppContext->m_RItemsMap)
	{
		SAFE_DELETE_PTR(p.second);
	}
	for (auto& p : AppContext->m_RItemsVec)
		SAFE_DELETE_PTR(p);
	AppContext->m_RItemsVec.clear();
	AppContext->m_RItemsMap.clear();

	/* Release Commands */
	CommandCenter::GetApp()->Release();

	/* Sounds Clear */
	SoundManager::GetApp()->DestroyAll();

	/* Release References & Manager */
	SceneManager::DestroyApp();
	CommandCenter::DestroyApp();
	SoundManager::DestroyApp();
	AssertsReference::DestroyApp();
	ApplicationContext::DestroyApp();
}

void CACHE_CACHE::Update(float deltaT)
{
	/*Cursor*/
	InputHandler::ShowMouseCursor();

	/*CommandCenter*/
	CommandCenter::GetApp()->Order(deltaT);

	/*SoundManager*/
	SoundManager::GetApp()->UpdateSoundManager();

	/*ParticleSystem*/
	if (m_ParticleSystem)
		ParticleSystem::GetApp()->Update(deltaT);

	/*SceneManager*/
	if (m_SceneManager)
		SceneManager::GetApp()->UpdateScene(deltaT);

	// PassCB
	m_Camera->UpdateViewMatrix();
	GraphicsContext::GetApp()->UpdateMainPassCB(*m_Camera, m_Lights[LIGHT_NAME_DIRECTIONAL].get());
}

void CACHE_CACHE::RenderScene(void)
{
	if (m_SceneManager)
		SceneManager::GetApp()->RenderScene();
}

void CACHE_CACHE::RenderUI(void)
{
	if (m_SceneManager)
		SceneManager::GetApp()->RenderUI();
}

void CACHE_CACHE::WriteShadow(void)
{
	if (m_SceneManager)
		m_SceneManager->WriteShadow();
}

void CACHE_CACHE::OnResize()
{
	if(m_Camera)
		m_Camera->OnResize();

	if (m_SceneManager)
		m_SceneManager->OnResize();
}

void CACHE_CACHE::BuildAsserts()
{
	// Build Maps
	// 맵에 지정된 텍스쳐 세팅
	AppContext->m_Maps[MAP_STR_TOWN] = AssertsReference::GetApp()->LoadMapInfo("./Map/fullMap.txt");
	AppContext->m_Maps[MAP_STR_TOWN]->propTexture = TEXTURE_INDEX_PolyAdventureTexture_01;
	AppContext->m_Maps[MAP_STR_GAMEROOM] = AssertsReference::GetApp()->LoadMapInfo("./Map/gameroomMap.txt");
	AppContext->m_Maps[MAP_STR_GAMEROOM]->propTexture = TEXTURE_INDEX_PolyAdventureTexture_01;
	AppContext->m_Maps[MAP_STR_LOBBY] = AssertsReference::GetApp()->LoadMapInfo("./Map/lobbyMap.txt");
	AppContext->m_Maps[MAP_STR_LOBBY]->propTexture = TEXTURE_INDEX_Mage_Room;
	AppContext->m_Maps[MAP_STR_RESULT_MASTER] = AssertsReference::GetApp()->LoadMapInfo("./Map/gameresultMap_master.txt");
	AppContext->m_Maps[MAP_STR_RESULT_MASTER]->propTexture = TEXTURE_INDEX_Mage_Room;
	AppContext->m_Maps[MAP_STR_RESULT_STUDENT] = AssertsReference::GetApp()->LoadMapInfo("./Map/gameresultMap_student.txt");
	AppContext->m_Maps[MAP_STR_RESULT_STUDENT]->propTexture = TEXTURE_INDEX_Mage_Room;

	// BB정보 읽어오기
	AssertsReference::GetApp()->LoadBB("./BB/ClientBB.txt");

	// Build Map Models
	// GameResult씬과 Lobby씬 에셋이 동일하기 때문에 Lobby만 메시를 로드하고, 결과씬은 로드하지 않음.
	int loadMeshCount = AppContext->m_Maps[MAP_STR_TOWN]->propTypeVector.size();
	for (int i = 0; i < loadMeshCount; ++i)
	{
		
		cout << AppContext->m_Maps[MAP_STR_TOWN]->propTypeVector[i] << endl;
 		AssertsReference::GetApp()->BuildModel(g_Device.Get(), g_CommandList.Get(), AppContext->m_Maps[MAP_STR_TOWN]->propTypeVector[i]);
	}

	loadMeshCount = AppContext->m_Maps[MAP_STR_GAMEROOM]->propTypeVector.size();
	for (int i = 0; i < loadMeshCount; ++i)
	{
		cout << AppContext->m_Maps[MAP_STR_GAMEROOM]->propTypeVector[i] << endl;
		AssertsReference::GetApp()->BuildModel(g_Device.Get(), g_CommandList.Get(), AppContext->m_Maps[MAP_STR_GAMEROOM]->propTypeVector[i]);
	}

	loadMeshCount = AppContext->m_Maps[MAP_STR_LOBBY]->propTypeVector.size();
	for (int i = 0; i < loadMeshCount; ++i)
	{
		cout << AppContext->m_Maps[MAP_STR_LOBBY]->propTypeVector[i] << endl;
		AssertsReference::GetApp()->BuildModel(g_Device.Get(), g_CommandList.Get(), AppContext->m_Maps[MAP_STR_LOBBY]->propTypeVector[i]);
	}

	// Build GeoMeshes
	AssertsReference::GetApp()->BuildGeoMeshes(g_Device.Get(), g_CommandList.Get());

	// Build SkinnedModel
	AssertsReference::GetApp()->BuildSkinnedModel(g_Device.Get(), g_CommandList.Get(), CHARACTER_WIZARD);
	AssertsReference::GetApp()->BuildSkinnedModelAnimation(CHARACTER_WIZARD, "Idle");
	AssertsReference::GetApp()->BuildSkinnedModelAnimation(CHARACTER_WIZARD, "Running");
	AssertsReference::GetApp()->BuildSkinnedModelAnimation(CHARACTER_WIZARD, "Backward");
	AssertsReference::GetApp()->BuildSkinnedModelAnimation(CHARACTER_WIZARD, "Left_Strafe");
	AssertsReference::GetApp()->BuildSkinnedModelAnimation(CHARACTER_WIZARD, "Right_Strafe");
	AssertsReference::GetApp()->BuildSkinnedModelAnimation(CHARACTER_WIZARD, "Attack");
	AssertsReference::GetApp()->BuildSkinnedModelAnimation(CHARACTER_WIZARD, "Jump");
	AssertsReference::GetApp()->BuildSkinnedModelAnimation(CHARACTER_WIZARD, "Find");
	AssertsReference::GetApp()->BuildSkinnedModelAnimation(CHARACTER_WIZARD, "Win");
	AssertsReference::GetApp()->BuildSkinnedModelAnimation(CHARACTER_WIZARD, "Lose");

	AssertsReference::GetApp()->BuildSkinnedModel(g_Device.Get(), g_CommandList.Get(), CHARACTER_DRUID);
	AssertsReference::GetApp()->BuildSkinnedModelAnimation(CHARACTER_DRUID, "Idle");
	AssertsReference::GetApp()->BuildSkinnedModelAnimation(CHARACTER_DRUID, "Running");
	AssertsReference::GetApp()->BuildSkinnedModelAnimation(CHARACTER_DRUID, "Jump");
	AssertsReference::GetApp()->BuildSkinnedModelAnimation(CHARACTER_DRUID, "Win");
	AssertsReference::GetApp()->BuildSkinnedModelAnimation(CHARACTER_DRUID, "Lose");

	AssertsReference::GetApp()->BuildSkinnedModel(g_Device.Get(), g_CommandList.Get(), CHARACTER_BAIRD);
	AssertsReference::GetApp()->BuildSkinnedModelAnimation(CHARACTER_BAIRD, "Idle");
	AssertsReference::GetApp()->BuildSkinnedModelAnimation(CHARACTER_BAIRD, "Running");
	AssertsReference::GetApp()->BuildSkinnedModelAnimation(CHARACTER_BAIRD, "Jump");
	AssertsReference::GetApp()->BuildSkinnedModelAnimation(CHARACTER_BAIRD, "Win");
	AssertsReference::GetApp()->BuildSkinnedModelAnimation(CHARACTER_BAIRD, "Lose");

	AssertsReference::GetApp()->BuildSkinnedModel(g_Device.Get(), g_CommandList.Get(), CHARACTER_FEMALE_PEASANT);
	AssertsReference::GetApp()->BuildSkinnedModelAnimation(CHARACTER_FEMALE_PEASANT, "Idle");
	AssertsReference::GetApp()->BuildSkinnedModelAnimation(CHARACTER_FEMALE_PEASANT, "Running");
	AssertsReference::GetApp()->BuildSkinnedModelAnimation(CHARACTER_FEMALE_PEASANT, "Jump");
	AssertsReference::GetApp()->BuildSkinnedModelAnimation(CHARACTER_FEMALE_PEASANT, "Win");
	AssertsReference::GetApp()->BuildSkinnedModelAnimation(CHARACTER_FEMALE_PEASANT, "Lose");

	AssertsReference::GetApp()->BuildSkinnedModel(g_Device.Get(), g_CommandList.Get(), CHARACTER_MALE_PEASANT);
	AssertsReference::GetApp()->BuildSkinnedModelAnimation(CHARACTER_MALE_PEASANT, "Idle");
	AssertsReference::GetApp()->BuildSkinnedModelAnimation(CHARACTER_MALE_PEASANT, "Running");
	AssertsReference::GetApp()->BuildSkinnedModelAnimation(CHARACTER_MALE_PEASANT, "Jump");
	AssertsReference::GetApp()->BuildSkinnedModelAnimation(CHARACTER_MALE_PEASANT, "Win");
	AssertsReference::GetApp()->BuildSkinnedModelAnimation(CHARACTER_MALE_PEASANT, "Lose");

	AssertsReference::GetApp()->BuildSkinnedModel(g_Device.Get(), g_CommandList.Get(), CHARACTER_SORCERER);
	AssertsReference::GetApp()->BuildSkinnedModelAnimation(CHARACTER_SORCERER, "Idle");
	AssertsReference::GetApp()->BuildSkinnedModelAnimation(CHARACTER_SORCERER, "Running");
	AssertsReference::GetApp()->BuildSkinnedModelAnimation(CHARACTER_SORCERER, "Jump");
	AssertsReference::GetApp()->BuildSkinnedModelAnimation(CHARACTER_SORCERER, "Win");
	AssertsReference::GetApp()->BuildSkinnedModelAnimation(CHARACTER_SORCERER, "Lose");

	// Particles
	AssertsReference::GetApp()->BuildBasicParticle(g_Device.Get(), g_CommandList.Get(), 
		PARTICLE_NAME_SMOKE, 400,
		DirectX::XMFLOAT2(0, 0), DirectX::XMFLOAT2(0, 0), DirectX::XMFLOAT2(0, 0),
		DirectX::XMFLOAT2(50,40),
		DirectX::XMFLOAT2(-45, 45), DirectX::XMFLOAT2(-45, 45), DirectX::XMFLOAT2(-45, 45),
		DirectX::XMFLOAT2(0.f, 1.f), 
		DirectX::XMFLOAT2(5.f, 5.f),
		DirectX::XMFLOAT2(1.f, 2.f),
		DirectX::XMFLOAT2(1.f,1.f));

	AssertsReference::GetApp()->BuildCircleParticle(g_Device.Get(), g_CommandList.Get(),
		PARTICLE_NAME_PENALTY, 30,
		DirectX::XMFLOAT3(0, 0, 0),
		DirectX::XMFLOAT2(15, 15),
		DirectX::XMFLOAT2(0, 0), DirectX::XMFLOAT2(55, 60), DirectX::XMFLOAT2(0, 0),
		DirectX::XMFLOAT2(0.f, 6.f),
		DirectX::XMFLOAT2(4.5f, 4.5f),
		DirectX::XMFLOAT2(1.f, 2.f),
		DirectX::XMFLOAT2(18.f, 20.f),
		40.f);

	AssertsReference::GetApp()->BuildBasicParticle(g_Device.Get(), g_CommandList.Get(),
		PARTICLE_NAME_THUNDER, 500,
		DirectX::XMFLOAT2(0, 0), DirectX::XMFLOAT2(0, 0), DirectX::XMFLOAT2(0, 0),
		DirectX::XMFLOAT2(50, 40),
		DirectX::XMFLOAT2(-50, 50), DirectX::XMFLOAT2(-50, 50), DirectX::XMFLOAT2(-50, 50),
		DirectX::XMFLOAT2(0.f, 0.5f),
		DirectX::XMFLOAT2(0.5f, 0.5f),
		DirectX::XMFLOAT2(1.f, 2.f),
		DirectX::XMFLOAT2(1.f, 1.f));

	AssertsReference::GetApp()->BuildBasicParticle(g_Device.Get(), g_CommandList.Get(),
		PARTICLE_NAME_LIGHT_EXPLOSION, 300,
		DirectX::XMFLOAT2(0, 0), DirectX::XMFLOAT2(0, 0), DirectX::XMFLOAT2(0, 0),
		DirectX::XMFLOAT2(80, 80),
		DirectX::XMFLOAT2(-45, 45), DirectX::XMFLOAT2(-45, 45), DirectX::XMFLOAT2(-45, 45),
		DirectX::XMFLOAT2(0.f, 0.3f),
		DirectX::XMFLOAT2(1.5f, 1.5f),
		DirectX::XMFLOAT2(1.f, 2.f),
		DirectX::XMFLOAT2(1.f, 1.f));

	AssertsReference::GetApp()->BuildCircleParticle(g_Device.Get(), g_CommandList.Get(),
		PARTICLE_NAME_CHERRY_BLOSSOM_CHARACTER, 70,
		DirectX::XMFLOAT3(0, 0, 0),
		DirectX::XMFLOAT2(15, 15),
		DirectX::XMFLOAT2(-5, 5), DirectX::XMFLOAT2(-65, -70), DirectX::XMFLOAT2(-5, 5),
		DirectX::XMFLOAT2(0.f, 6.f),
		DirectX::XMFLOAT2(5.5f, 7.5f),
		DirectX::XMFLOAT2(1.f, 2.f),
		DirectX::XMFLOAT2(18.f, 20.f),
		50.f);

	// Build Materials
	AssertsReference::GetApp()->BuildMaterials();
}

void CACHE_CACHE::BuildCharactersAndParticles()
{
	/*Particles*/
	// 변신 파티클 - Smoke
	AppContext->CreateParticle(PARTICLE_NAME_SMOKE, CHARACTER_DRUID, TEXTURE_STR_T_Smoke_Tiled_D, false, XMFLOAT3(0,50,0), 3.f);
	AppContext->CreateParticle(PARTICLE_NAME_SMOKE, CHARACTER_BAIRD, TEXTURE_STR_T_Smoke_Tiled_D, false, XMFLOAT3(0, 50, 0), 3.f);
	AppContext->CreateParticle(PARTICLE_NAME_SMOKE, CHARACTER_FEMALE_PEASANT, TEXTURE_STR_T_Smoke_Tiled_D, false, XMFLOAT3(0, 50, 0), 3.f);
	AppContext->CreateParticle(PARTICLE_NAME_SMOKE, CHARACTER_MALE_PEASANT, TEXTURE_STR_T_Smoke_Tiled_D, false, XMFLOAT3(0, 50, 0), 3.f);
	AppContext->CreateParticle(PARTICLE_NAME_SMOKE, CHARACTER_SORCERER, TEXTURE_STR_T_Smoke_Tiled_D, false, XMFLOAT3(0, 50, 0), 3.f);

	// 변신 패널티 파티클 - Penarty
	AppContext->CreateParticle(PARTICLE_NAME_PENALTY, CHARACTER_DRUID, TEXTURE_STR_P_PENALTY, true, XMFLOAT3(0,110,0), 0.5f);
	AppContext->CreateParticle(PARTICLE_NAME_PENALTY, CHARACTER_BAIRD, TEXTURE_STR_P_PENALTY, true, XMFLOAT3(0, 110, 0), 0.5f);
	AppContext->CreateParticle(PARTICLE_NAME_PENALTY, CHARACTER_FEMALE_PEASANT, TEXTURE_STR_P_PENALTY, true, XMFLOAT3(0, 110, 0), 0.5f);
	AppContext->CreateParticle(PARTICLE_NAME_PENALTY, CHARACTER_MALE_PEASANT, TEXTURE_STR_P_PENALTY, true, XMFLOAT3(0, 110, 0), 0.5f);
	AppContext->CreateParticle(PARTICLE_NAME_PENALTY, CHARACTER_SORCERER, TEXTURE_STR_P_PENALTY, true, XMFLOAT3(0, 110, 0), 0.5f);

	// 매직 아우라 파티클
	AppContext->CreateParticle(PARTICLE_NAME_CHERRY_BLOSSOM_CHARACTER, PARTICLE_NAME_CHERRY_BLOSSOM_CHARACTER, TEXTURE_STR_P_CHERRY_BLOSSOM, true, XMFLOAT3(0, 250, 0), 0.5f);

	/*Characters*/
	// 캐릭터 배치 순서
	// Wizard / Druid / Baird / Female_Peasant / Male_Peasant / Sorcerer
	AppContext->CreateCharacter(CHARACTER_WIZARD, CHARACTER_WIZARD, TEXTURE_STR_Polygon_Fantasy_Characters_Texture_03_A, BoneIndex::Wizard);
	AppContext->CreateCharacter(CHARACTER_DRUID, CHARACTER_DRUID, TEXTURE_STR_Polygon_Fantasy_Characters_Texture_05_A, BoneIndex::Druid);
	AppContext->CreateCharacter(CHARACTER_BAIRD, CHARACTER_BAIRD, TEXTURE_STR_Polygon_Fantasy_Characters_Texture_01_A, BoneIndex::Baird);
	AppContext->CreateCharacter(CHARACTER_FEMALE_PEASANT, CHARACTER_FEMALE_PEASANT, TEXTURE_STR_Polygon_Fantasy_Characters_Texture_02_A, BoneIndex::Female_Peasant);
	AppContext->CreateCharacter(CHARACTER_MALE_PEASANT, CHARACTER_MALE_PEASANT, TEXTURE_STR_Polygon_Fantasy_Characters_Texture_04_A, BoneIndex::Male_Peasant);
	AppContext->CreateCharacter(CHARACTER_SORCERER, CHARACTER_SORCERER, TEXTURE_STR_Polygon_Fantasy_Characters_Texture_01_A, BoneIndex::Sorcerer);

	/* 각 캐릭터 파티클 세팅 */
	ParticleSystem::GetApp()->SetCharacterParticle(CHARACTER_DRUID, CHARACTER_DRUID, PARTICLE_NAME_SMOKE, CHARACTER_DRUID);
	ParticleSystem::GetApp()->SetCharacterParticle(CHARACTER_BAIRD, CHARACTER_BAIRD, PARTICLE_NAME_SMOKE, CHARACTER_BAIRD);
	ParticleSystem::GetApp()->SetCharacterParticle(CHARACTER_FEMALE_PEASANT, CHARACTER_FEMALE_PEASANT, PARTICLE_NAME_SMOKE, CHARACTER_FEMALE_PEASANT);
	ParticleSystem::GetApp()->SetCharacterParticle(CHARACTER_MALE_PEASANT, CHARACTER_MALE_PEASANT, PARTICLE_NAME_SMOKE, CHARACTER_MALE_PEASANT);
	ParticleSystem::GetApp()->SetCharacterParticle(CHARACTER_SORCERER, CHARACTER_SORCERER, PARTICLE_NAME_SMOKE, CHARACTER_SORCERER);

	ParticleSystem::GetApp()->SetCharacterParticle(CHARACTER_DRUID, CHARACTER_DRUID, PARTICLE_NAME_PENALTY, CHARACTER_DRUID);
	ParticleSystem::GetApp()->SetCharacterParticle(CHARACTER_BAIRD, CHARACTER_BAIRD, PARTICLE_NAME_PENALTY, CHARACTER_BAIRD);
	ParticleSystem::GetApp()->SetCharacterParticle(CHARACTER_FEMALE_PEASANT, CHARACTER_FEMALE_PEASANT, PARTICLE_NAME_PENALTY, CHARACTER_FEMALE_PEASANT);
	ParticleSystem::GetApp()->SetCharacterParticle(CHARACTER_MALE_PEASANT, CHARACTER_MALE_PEASANT, PARTICLE_NAME_PENALTY, CHARACTER_MALE_PEASANT);
	ParticleSystem::GetApp()->SetCharacterParticle(CHARACTER_SORCERER, CHARACTER_SORCERER, PARTICLE_NAME_PENALTY, CHARACTER_SORCERER);

	// 매직 아우라 파티클
	ParticleSystem::GetApp()->SetParticle(PARTICLE_NAME_CHERRY_BLOSSOM_CHARACTER, PARTICLE_NAME_CHERRY_BLOSSOM_CHARACTER);

}