// 0 -> SMALL
#define SM_Item_Canteen_01				0
#define SM_Item_Fruit_01				1
#define SM_Item_Fruit_02				2
#define SM_Item_Fruit_03				3
#define SM_Item_Gourd_01				4
#define SM_Item_Lantern_01				5
#define SM_Item_Lantern_02				6
#define SM_Item_Potion_01				7
#define SM_Item_Potion_02				8
#define SM_Item_Potion_03				9
#define SM_Item_Potion_04				10
#define SM_Item_Potion_05				11
#define SM_Item_Potion_06				12
#define SM_Item_Pouch_01				13
#define SM_Item_Waterskin_01			14
#define SM_Item_Wine_01					15
#define SM_Item_Wine_02					16
#define SM_Prop_Basket_01				17
#define SM_Prop_Basket_02				18
#define SM_Prop_Basket_03				19
#define SM_Prop_Basket_04				20
#define SM_Prop_Book_01					21
#define SM_Prop_Book_02					22
#define SM_Prop_Book_03					23
#define SM_Prop_Cheese_01				24
#define SM_Prop_Cheese_02				25
#define SM_Prop_Cheese_03				26
#define SM_Prop_Meat_01					27
#define SM_Prop_Meat_02					28
#define SM_Prop_Meat_03					29
#define SM_Prop_Pot_01					30
#define SM_Prop_Pot_02					31
#define SM_Prop_Pot_03					32
#define SM_Prop_Pumpkin_01				33
#define SM_Prop_Pumpkin_02				34
#define SM_Prop_Scroll_01				35
#define SM_Prop_Scroll_02				36
#define SM_Prop_Sack_01					37
#define SM_Prop_Sack_02					38
#define SM_Prop_Sack_03					39
#define SM_Prop_StoneBlock_01			40


// 1 -> MIDUEM
#define SM_Env_Bush_01					104
#define SM_Env_Bush_02					105
#define SM_Env_Bush_03					106
#define SM_Env_Bush_04					107
#define SM_Env_TreeLog_01				110
#define SM_Prop_Barrel_01				112
#define SM_Prop_Barrel_02				113
#define SM_Prop_Cart_Wheel_01			114
//#define SM_Prop_Chest_01				115 �𵨹����� ��
#define SM_Prop_Crate_01				116
#define SM_Prop_Crate_02				117
#define SM_Prop_Loghalf_01				118
#define SM_Prop_Loghalf_02				119
//#define SM_Prop_Logpile_01				120 small�� ũ�� ����
//#define SM_Prop_Sack_01					122
//#define SM_Prop_Sack_02					123
//#define SM_Prop_Sack_03					124
//#define SM_Prop_Sack_04					125
//#define SM_Prop_StoneBlock_01			126
#define Knights_SM_Prop_Crate_01		127
#define Knights_SM_Prop_Gravestone_01	128
#define Knights_SM_Prop_Gravestone_02	129
#define Knights_SM_Prop_WaterWheel_Support_01	130


// 2 -> LARGE
//#define SM_Env_Rock_02					202
//#define SM_Env_Rock_03					203
//#define SM_Env_Rock_04					204
//#define SM_Env_Rock_05					205
//#define SM_Env_Rock_011					206
//#define SM_Env_Rock_012					207
//#define SM_Env_Rock_013					208
#define SM_Prop_Cart_01					234
//#define SM_Prop_Cart_02					235
//#define SM_Prop_Cart_03					236
#define Knights_SM_Prop_CartHay_01		235
#define Knights_SM_Prop_Guillotine_01	236
#define Knights_SM_Prop_Plinth_01		237
#define Knights_SM_Prop_Plinth_02		238
#define Knights_SM_Prop_Statue_01		239


// collide X (ex)flower
#define SM_Env_Flower_01				300
#define SM_Env_Flower_02				301
#define SM_Env_Flower_03				302
#define SM_Env_Flower_04				303
#define SM_Env_Flower_05				304
#define SM_Env_Flower_06				305
#define SM_Env_Flower_07				306
#define SM_Env_Flower_08				307
#define SM_Env_Grass_01					308
#define SM_Env_Grass_02					309
#define SM_Env_Mushroom_01				310
#define SM_Env_Pebble_01				311
#define SM_Env_Pebble_02				312
#define SM_Env_Pebble_03				313
#define SM_Env_Pebble_04				314
#define SM_Env_Pebble_05				315
#define SM_Env_Pebble_06				316
#define SM_Env_Pebble_07				317
#define SM_Env_Plant_01					318
#define SM_Env_Plant_02					319
#define SM_Env_Plant_03					320
#define SM_Env_Plant_04					321
#define SM_Env_Plant_05					322
#define SM_Env_Reeds_01					323
#define SM_Env_Reeds_02					324
#define SM_Env_Reeds_03					325
#define SM_Prop_Washingline_01			326
#define SM_Prop_Washingline_02			327
#define SM_Prop_Washingline_03			328
#define Knights_SM_Prop_Banner_01		329
#define Knights_SM_Prop_Banner_02		330
#define Knights_SM_Prop_Banner_03		331
#define Knights_SM_Prop_ShopSign_01		332
#define Knights_SM_Bld_Castle_Flag_01	333
#define Knights_SM_Bld_Castle_Flagpole_01	334


// collide O (ex)Village
#define SM_Bld_Hut_01					400
#define SM_Bld_Stall_01					402
#define SM_Bld_Stall_02					403
#define SM_Bld_Stall_03					404
#define SM_Bld_Stall_04					405
#define SM_Bld_Stall_Cover_01			406
#define SM_Bld_Stall_Cover_02			407
#define SM_Bld_Stall_Cover_03			408
#define SM_Bld_Stall_Cover_04			409
#define SM_Bld_Stall_Cover_05			410
#define SM_Bld_Village_01				411
#define SM_Bld_Village_02				412
#define SM_Bld_Village_03				413
#define SM_Bld_Village_04				414
#define SM_Bld_Village_05				415
#define SM_Bld_Village_06				416
#define SM_Bld_Village_07				417
#define SM_Bld_Village_Top_01			418
#define SM_Bld_Village_WindowDrapes_01	419
#define SM_Prop_Stall_Table_01			420	//237 -> 420
#define SM_Bld_Fence_01					421 //100 -> 421
#define SM_Bld_Fence_02					422 //101 -> 422
#define SM_Bld_Village_HangingCloth_01	423 //102 -> 423
#define SM_Bld_Wall_02					424 //103 -> 424
#define SM_Env_CampFire_01				425 //108 -> 425
#define SM_Env_Hedge_01					426 //109 -> 426
#define SM_Env_TreeStump_01				427 //111 -> 427
#define SM_Prop_RoadSign_01				428 //121 -> 428
#define SM_Bld_Wall_01					429 //200 -> 429
#define SM_Bld_Well_01					430 //201 -> 430
#define SM_Env_Tree_01					431 //209 -> 431
#define SM_Env_Tree_02					432 //210 -> 432
#define SM_Env_Tree_03					433 //211 -> 433
#define SM_Env_Tree_04					434 //212 -> 434
#define SM_Env_Tree_05					435 //213 -> 435
#define SM_Env_Tree_06					436 //214 -> 436
#define SM_Env_Tree_07					437 //215 -> 437
#define SM_Env_Tree_08					438 //216 -> 438
#define SM_Env_Tree_09					439 //217 -> 439
#define SM_Env_Tree_010					440 //218 -> 440
#define SM_Env_Tree_011					441 //219 -> 441
#define SM_Env_Tree_012					442 //220 -> 442
#define SM_Env_Tree_013					443 //221 -> 443
#define SM_Env_Tree_014					444 //222 -> 444
#define SM_Env_Tree_015					445 //223 -> 445
#define SM_Env_Tree_016					446 //224 -> 446
#define SM_Env_TreeBirch_01				448 //225 -> 448
#define SM_Env_TreeBirch_02				449 //226 -> 449
#define SM_Env_TreeBirch_03				450 //227 -> 450
#define SM_Env_TreeDead_01				451 //228 -> 451
#define SM_Env_TreeDead_02				452 //229 -> 452
#define SM_Env_TreePine_01				453 //230 -> 453
#define SM_Env_TreePine_02				454 //231 -> 454
#define SM_Env_TreePine_03				455 //232 -> 455
#define SM_Env_TreePine_04				456 //233 -> 456
#define Knights_SM_Prop_Brazier_01		457
#define Knights_SM_Prop_CampFire_01		458
#define Knights_SM_Prop_Fence_01		459
#define Knights_SM_Prop_Lampost_01		460
#define Knights_SM_Prop_WaterWheel_01	461
#define Knights_SM_Prop_Statue_Base_01	462
#define Knights_SM_Bld_ArrowSlit_01		463
#define Knights_SM_Bld_Castle_Arrow_Slit_01	464
#define Knights_SM_Bld_Castle_Door_01	465
#define Knights_SM_Bld_Castle_Iron_Gate_01	466
#define Knights_SM_Bld_Castle_Pillar_01	467
#define Knights_SM_Bld_Castle_Roof_Spire_01	468
#define Knights_SM_Bld_Castle_Tower_01	469
#define Knights_SM_Bld_Castle_Tower_01_Lite	470
#define Knights_SM_Bld_Castle_Tower_02	471
#define Knights_SM_Bld_Castle_Tower_02_Lite	472
#define Knights_SM_Bld_Castle_Tower_03	473
#define Knights_SM_Bld_Castle_Tower_03_Lite	474
#define Knights_SM_Bld_Castle_Tower_04	475
#define Knights_SM_Bld_Castle_Tower_04_Lite	476
#define Knights_SM_Bld_Castle_Tower_Base_01	477
#define Knights_SM_Bld_Castle_Tower_Base_02	478
#define Knights_SM_Bld_Castle_Tower_Mini_01	479
#define Knights_SM_Bld_Castle_Tower_Mini_02	480
#define Knights_SM_Bld_Castle_Tower_Round_01	481
#define Knights_SM_Bld_Castle_Tower_Round_01_Lite	482
#define Knights_SM_Bld_Castle_Tower_Round_02	483
#define Knights_SM_Bld_Castle_Tower_Round_02_Lite	484
#define Knights_SM_Bld_Castle_Tower_Round_Top_01	485
#define Knights_SM_Bld_Castle_Tower_Round_Top_01_Lite	486
#define Knights_SM_Bld_Castle_Tower_Top_01	487
#define Knights_SM_Bld_Castle_Tower_Top_01_Lite	488
#define Knights_SM_Bld_Castle_Tower_Wall_Top_01 489
#define Knights_SM_Bld_Castle_Tower_Wall_Top_01_Lite	490
#define Knights_SM_Bld_Castle_Wall_01	491
#define Knights_SM_Bld_Castle_Wall_01_Lite	492
#define Knights_SM_Bld_Castle_Wall_Gate_01	493
#define Knights_SM_Bld_Castle_Wall_Gate_01_Lite	494
#define Knights_SM_Bld_Castle_Wood_Battlement_01	495
#define Knights_SM_Bld_Castle_Wood_Battlement_02	496
#define Knights_SM_Bld_Castle_Wood_Battlement_03	497
#define Knights_SM_Bld_Castle_Wood_Battlement_04	498
#define Knights_SM_Bld_Castle_Wood_Detal_01		499
#define Knights_SM_Bld_Church_Door_01	500
#define Knights_SM_Bld_Church_Extension_01	501
#define Knights_SM_Bld_Church_Extension_01_Lite	502
#define Knights_SM_Bld_Church_Room_01	503
#define Knights_SM_Bld_Church_Room_01_Lite	504
#define Knights_SM_Bld_Church_Room_02	505
#define Knights_SM_Bld_Church_Room_02_Lite	506
#define Knights_SM_Bld_Church_Room_03	507
#define Knights_SM_Bld_Church_Room_03_Lite	508
#define Knights_SM_Bld_Church_Tower_01	509
#define Knights_SM_Bld_Church_Tower_01_Lite	510
#define Knights_SM_Bld_Church_Tower_02	511
#define Knights_SM_Bld_Church_Tower_02_Lite	512
#define Knights_SM_Bld_Church_Tower_03	513
#define Knights_SM_Bld_Church_Tower_03_Lite 514
#define Knights_SM_Bld_Church_TowerBase_01	515
#define Knights_SM_Bld_Church_TowerBase_01_Lite	516
#define Knights_SM_Bld_House_Door_02	517
#define Knights_SM_Bld_House_Door_03	518
#define Knights_SM_Bld_House_Door_04	519
#define Knights_SM_Bld_House_Extension_01	520
#define Knights_SM_Bld_House_Extension_02	521
#define Knights_SM_Bld_House_Foundation_01	522
#define Knights_SM_Bld_House_Foundation_02	523
#define Knights_SM_Bld_House_Foundation_04	524
#define Knights_SM_Bld_House_Foundation_05	525
#define Knights_SM_Bld_House_Foundation_06	526
#define Knights_SM_Bld_House_Foundation_07	527
#define Knights_SM_Bld_House_Foundation_Beams_01	528
#define Knights_SM_Bld_House_FoundationRock_01	529
#define Knights_SM_Bld_House_Room_01	530
#define Knights_SM_Bld_House_Room_02	531
#define Knights_SM_Bld_House_Room_03	532
#define Knights_SM_Bld_House_Room_04	533
#define Knights_SM_Bld_House_Room_05	534
#define Knights_SM_Bld_House_Room_06	535
#define Knights_SM_Bld_House_Room_07	536
#define Knights_SM_Bld_House_RoomRound_01	537
#define Knights_SM_Bld_House_RoomRound_02	538
#define Knights_SM_Bld_House_RoomTall_01	539
#define Knights_SM_Bld_House_RoomTall_02	540
#define Knights_SM_Bld_House_RoomTall_03	541
#define Knights_SM_Bld_House_RoomTall_04	542
#define Knights_SM_Bld_House_RoomTall_05	543
#define Knights_SM_Bld_House_RoomTall_06	544
#define Knights_SM_Bld_House_RoomTop_01	545
#define Knights_SM_Bld_House_RoomTop_02	546
#define Knights_SM_Bld_House_RoomTop_03	547
#define Knights_SM_Bld_House_RoomTop_04	548
#define Knights_SM_Bld_House_RoomTop_05	549
#define Knights_SM_Bld_House_RoomTop_06	550
#define Knights_SM_Bld_House_RoomTop_07	551
#define Knights_SM_Bld_House_TopRoomSmall_01	552
#define Knights_SM_Bld_House_TopRoomSmall_02	553
#define Knights_SM_Bld_House_TopRoomSmall_03	554
#define Knights_SM_Bld_House_TopRoomSmall_04	555
#define Knights_SM_Bld_House_TopRoomSmall_05	556
#define Knights_SM_Bld_House_TopRoomSmall_06	557
#define Knights_SM_Bld_House_TopRoomSmall_07	558
#define Knights_SM_Bld_House_Tower_01	559
#define Knights_SM_Bld_House_Window_01	560
#define Knights_SM_Bld_House_Window_02	561
#define Knights_SM_Bld_House_Window_03	562
#define Knights_SM_Bld_Rockwall_Archway_01	563
#define Knights_SM_Bld_Rockwall_Straight_01	564
#define Knights_SM_Bld_Tent_01	565
#define Knights_SM_Bld_Tent_02	566
#define Knights_SM_Bld_Tent_03	567
#define Knights_SM_Bld_Village_Well_01	568


//tile&cloud&door&hill
#define SM_Env_FloorTile_01				700
#define SM_Env_FloorTile_02				701
#define SM_Env_FloorTile_03				702
#define SM_Env_FloorTile_04				703
#define SM_Env_FloorTile_05				704
#define SM_Env_FloorTile_06				705
#define SM_Env_FloorTile_07				706
#define SM_Env_Cloud_01					710
#define SM_Env_Cloud_02					711
#define SM_Env_Cloud_03					712
#define SM_Env_Cloud_04					713
#define SM_Env_Cloud_05					714
#define SM_Env_Cloud_06					715
#define SM_Env_Cloud_07					716
#define SM_Bld_HutDoor_01				717 //401 -> 517
#define SM_Env_Hill_01					718
#define SM_Env_Hill_02					719
#define SM_Env_Hill_03					720
#define SM_Env_Hill_04					721
#define Knights_SM_Env_Path_Cobble_01	722
#define Knights_SM_Env_Path_Cobble_02	723
#define Knights_SM_Env_Path_Stone_01	724
#define Knights_SM_Env_Path_Stone_02	725
#define Knights_SM_Env_Path_Stone_03	726
#define Knights_SM_Env_Path_Tile_01		727
#define Knights_SM_Env_Path_Tile_02		728
#define Knights_SM_Env_Path_Tile_Corner_01	729
#define Knights_SM_Env_Tile_Dirt_01		730
#define Knights_SM_Env_Tile_Grass_01	731
#define Knights_SM_Env_Tile_Water_01	732

//lobby & game result scene object
#define Dungeon_Amphora					800
#define Dungeon_Amphora_Stand			801
#define Dungeon_Bag						802
#define Dungeon_Barrel_Big				803
#define Dungeon_Barrel_Closed			804
#define Dungeon_Barrel_Open				805
#define Dungeon_Bench					806
#define Dungeon_Bucket					807
#define Dungeon_Carpet_Green			808
#define Dungeon_Carpet_Red				809
#define Dungeon_Cauldron_Empty			810
#define Dungeon_Cauldron_Full			811
#define Dungeon_Chair					812
#define Dungeon_Chest_Closed			813
#define Dungeon_Chest_Open				814
#define Dungeon_ChestSmall_Closed		815
#define Dungeon_ChestSmall_Open			816
#define Dungeon_CoatHook_Full			817
#define Dungeon_Desk					818
#define Dungeon_Fireplace				819
#define Dungeon_Shelf					820
#define Dungeon_Shelf_Wall				821
#define Dungeon_Steps_Small				822
#define Dungeon_Stool_round				823
#define Dungeon_Stool_square			824
#define Dungeon_Table_Big				825
#define Dungeon_Table_Small				826
#define Dungeon_WeaponRack_Big			827
#define Dungeon_WeaponRack_Small		828
#define Dungeon_Candle_Wall				829
#define Dungeon_Chandelier_Ceiling		830
#define Dungeon_Chandelier_Single		831
#define Dungeon_Chandelier_Triple		832
#define Dungeon_Torch					833
#define Dungeon_Torch_Wall				834
#define Dungeon_Beaker_Empty			835
#define Dungeon_Beaker_Full_Var1		836
#define Dungeon_Beaker_Full_Var2		837
#define Dungeon_Beaker_Small			838
#define Dungeon_Bone					839
#define Dungeon_Book_Big_Brown			840
#define Dungeon_Book_Big_DeepRed		841
#define Dungeon_Book_Big_Green			842
#define Dungeon_Book_Big_Red			843
#define Dungeon_Book_Brown				844
#define Dungeon_Book_DeepRed			845
#define Dungeon_Book_Green				846
#define Dungeon_Book_Open				847
#define Dungeon_Book_Red				848
#define Dungeon_Book_Small_Brown		849
#define Dungeon_Book_Small_DeepRed		850
#define Dungeon_Book_Small_Green		851
#define Dungeon_Book_Small_Red			852
#define Dungeon_Cauldron_Small			853
#define Dungeon_Cauldron_Small_Stand	854
#define Dungeon_Chandelier_Small		855
#define Dungeon_CrystalBall				856
#define Dungeon_CrystalBall_Stand		857
#define Dungeon_Fork					858
#define Dungeon_Inkwell					859
#define Dungeon_Jar_Big					860
#define Dungeon_Jar_Empty				861
#define Dungeon_Jar_Full				862
#define Dungeon_Key_Golden				863
#define Dungeon_Key_Rusty				864
#define Dungeon_Key_Silver				865
#define Dungeon_Knife					866
#define Dungeon_Plate					867
#define Dungeon_Plate_Small				868
#define Dungeon_Potion_Blue				869
#define Dungeon_Potion_Green			870
#define Dungeon_Potion_Red				871
#define Dungeon_Scroll_Big				872
#define Dungeon_Scroll_Elder			873
#define Dungeon_Scroll_Open				874
#define Dungeon_Scroll_Small			875
#define Dungeon_Scroll_Stack			876
#define Dungeon_Skull					877
#define Dungeon_Basement_Door_Left		878
#define Dungeon_Basement_Door_Right		879
#define Dungeon_Basement_Passage		880
#define Dungeon_Basement_Passage_Mix	881
#define Dungeon_Basement_Passage_Prison	882
#define Dungeon_Basement_Var1			883
#define Dungeon_Basement_Var2			884
#define Dungeon_Basement_Var3			885
#define Dungeon_Basement_Var4			886
#define Dungeon_Basement_Wall_Prison	887
#define Dungeon_Basement_Wall_Prison_Broken	888
#define Dungeon_Basement_Wall_Var1		889
#define Dungeon_Basement_Wall_Var2		890
#define Dungeon_Basement_Wall_Var3		891
#define Dungeon_Door_Iron_Left			892
#define Dungeon_Door_Wooden_Left		893
#define Dungeon_Door_Wooden_Round_Left	894
#define Dungeon_Dungeon_Big				895
#define Dungeon_Dungeon_Big_Passage		896
#define Dungeon_Dungeon_Big_to_Custom	897
#define Dungeon_Dungeon_Big_Wall		898
#define Dungeon_Dungeon_Big_Window		899
#define Dungeon_Dungeon_CornerCrossing	900
#define Dungeon_Dungeon_Custom_Border_Arched	901
#define Dungeon_Dungeon_Custom_Border_Flat		902
#define Dungeon_Dungeon_Custom_Center			903
#define Dungeon_Dungeon_Custom_Corner_Arched	904
#define Dungeon_Dungeon_Custom_Corner_Flat		905
#define Dungeon_Dungeon_Custom_Corner_Inverted	906
#define Dungeon_Dungeon_Wall_Var1		907
#define Dungeon_Dungeon_Wall_Var2		908
#define Dungeon_Dungeon_Wall_Var3		909
#define Dungeon_Dungeon_Wall_Window		910
#define Dungeon_Pillar					911
#define Dungeon_Balcony					912
